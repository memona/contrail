package org.ow2.contrail.federation.federationcli.resources;

import java.util.Map;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;
import org.ow2.contrail.federation.federationcli.exceptions.NotAvailableMethodException;

public class UserRole extends ResourceBase {

	@Override
	public Response get(Map<String, String> args) throws Exception {
		String[] requiredParams = {"roleId", "userId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/roles/%s", args.get("userId"), args.get("roleId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response getAll(Map<String, String> args) throws Exception {
		String[] requiredParams = { "userId" };
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/roles", args.get("userId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response add(Map<String, String> args) throws Exception {
		String[] requiredParams = {"roleId", "userId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/roles", args.get("userId"));
		String data = String.format("{\"roleId\": %s }", args.get("roleId"));
		return sendPOSTRequest(uri, data);
	}
	
	@Override
	public Response delete(Map<String, String> args) throws Exception {
		String[] requiredParams = {"roleId", "userId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/roles/%s", args.get("userId"), args.get("roleId"));
		return sendDELETERequest(uri);
	}

	@Override
	public Response update(Map<String, String> args) throws Exception {
		throw new NotAvailableMethodException();
	}

}
