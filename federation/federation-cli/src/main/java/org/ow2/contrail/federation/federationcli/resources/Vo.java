package org.ow2.contrail.federation.federationcli.resources;

import org.json.JSONObject;
import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;

import java.util.Map;

public class Vo extends ResourceBase {

    @Override
    public Response get(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "voId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/vos/%s", args.get("providerId"), args.get("voId"));
        return sendGETRequest(uri);
    }

    @Override
    public Response getAll(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/vos", args.get("providerId"));
        return sendGETRequest(uri);
    }

    @Override
    public Response add(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "data"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/vos", args.get("providerId"));
        return sendPOSTRequest(uri, args.get("data"));
    }

    @Override
    public Response update(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "voId", "data"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/vos/%s", args.get("providerId"), args.get("voId"));
        return sendPUTRequest(uri, args.get("data"));
    }

    @Override
    public Response delete(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "voId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/vos/%s", args.get("providerId"), args.get("voId"));
        return sendDELETERequest(uri);
    }

    public Response getLinkedClusters(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "voId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/vos/%s/clusters", args.get("providerId"), args.get("voId"));
        return sendGETRequest(uri);
    }

    public Response getLinkedCluster(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "voId", "clusterId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/vos/%s/clusters/%s",
                args.get("providerId"), args.get("voId"), args.get("clusterId"));
        return sendGETRequest(uri);
    }

    public Response linkCluster(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "voId", "clusterId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/vos/%s/clusters", args.get("providerId"), args.get("voId"));
        JSONObject o = new JSONObject();
        o.put("clusterURI", String.format("/providers/%s/clusters/%s", args.get("providerId"), args.get("clusterId")));
        return sendPOSTRequest(uri, o.toString());
    }

    public Response unlinkCluster(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "voId", "clusterId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/vos/%s/clusters/%s",
                args.get("providerId"), args.get("voId"), args.get("clusterId"));
        return sendDELETERequest(uri);
    }
}
