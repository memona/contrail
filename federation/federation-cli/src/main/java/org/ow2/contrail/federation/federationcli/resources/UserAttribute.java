package org.ow2.contrail.federation.federationcli.resources;

import java.util.Map;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;
import org.ow2.contrail.federation.federationcli.exceptions.NotAvailableMethodException;

public class UserAttribute extends ResourceBase {

	@Override
	public Response get(Map<String, String> args) throws Exception {
		String[] requiredParams = {"attributeId", "userId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/attributes/%s", args.get("userId"), args.get("attributeId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response getAll(Map<String, String> args) throws Exception {
		String[] requiredParams = { "userId" };
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/attributes", args.get("userId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response add(Map<String, String> args) throws Exception {
		String[] requiredParams = {"userId", "attributeId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/attributes", args.get("userId"));
		String data = String.format("{\"attributeId\": %s }", args.get("attributeId"));
		return sendPOSTRequest(uri, data);
	}
	
	@Override
	public Response delete(Map<String, String> args) throws Exception {
		String[] requiredParams = {"userId", "attributeId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/users/%s/attributes/%s", args.get("userId"), args.get("attributeId"));
		return sendDELETERequest(uri);
	}

	@Override
	public Response update(Map<String, String> args) throws Exception {
        String[] requiredParams = {"userId", "attributeId", "data"};
        checkArguments(args, requiredParams);
        String uri = String.format("/usera/%s/attributes/%s", args.get("userId"), args.get("attributeId"));
        return sendPUTRequest(uri, args.get("data"));
	}

}
