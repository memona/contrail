package org.ow2.contrail.federation.federationcli.resources;

import org.json.JSONObject;
import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;

import java.util.Map;

public class Cluster extends ResourceBase {

    @Override
    public Response get(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "clusterId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters/%s", args.get("providerId"), args.get("clusterId"));
        return sendGETRequest(uri);
    }

    @Override
    public Response getAll(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters", args.get("providerId"));
        return sendGETRequest(uri);
    }

    @Override
    public Response add(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "data"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters", args.get("providerId"));
        return sendPOSTRequest(uri, args.get("data"));
    }

    @Override
    public Response update(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "clusterId", "data"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters/%s", args.get("providerId"), args.get("clusterId"));
        return sendPUTRequest(uri, args.get("data"));
    }

    @Override
    public Response delete(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "clusterId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters/%s", args.get("providerId"), args.get("clusterId"));
        return sendDELETERequest(uri);
    }

    public Response getLinkedServers(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "clusterId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters/%s/servers", args.get("providerId"), args.get("clusterId"));
        return sendGETRequest(uri);
    }

    public Response getLinkedServer(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "clusterId", "serverId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters/%s/servers/%s",
                args.get("providerId"), args.get("clusterId"), args.get("serverId"));
        return sendGETRequest(uri);
    }

    public Response linkServer(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "clusterId", "serverId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters/%s/servers", args.get("providerId"), args.get("clusterId"));
        JSONObject o = new JSONObject();
        o.put("serverURI", String.format("/providers/%s/servers/%s", args.get("providerId"), args.get("serverId")));
        return sendPOSTRequest(uri, o.toString());
    }

    public Response unlinkServer(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "clusterId", "serverId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters/%s/servers/%s",
                args.get("providerId"), args.get("clusterId"), args.get("serverId"));
        return sendDELETERequest(uri);
    }

    public Response getLinkedVms(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "clusterId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters/%s/vms", args.get("providerId"), args.get("clusterId"));
        return sendGETRequest(uri);
    }

    public Response getLinkedVm(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "clusterId", "vmId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters/%s/vms/%s",
                args.get("providerId"), args.get("clusterId"), args.get("vmId"));
        return sendGETRequest(uri);
    }

    public Response linkVm(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "clusterId", "vmId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters/%s/vms", args.get("providerId"), args.get("clusterId"));
        JSONObject o = new JSONObject();
        o.put("vmURI", String.format("/providers/%s/vms/%s", args.get("providerId"), args.get("vmId")));
        return sendPOSTRequest(uri, o.toString());
    }

    public Response unlinkVm(Map<String, String> args) throws Exception {
        String[] requiredParams = {"providerId", "clusterId", "vmId"};
        checkArguments(args, requiredParams);

        String uri = String.format("/providers/%s/clusters/%s/vms/%s",
                args.get("providerId"), args.get("clusterId"), args.get("vmId"));
        return sendDELETERequest(uri);
    }

}
