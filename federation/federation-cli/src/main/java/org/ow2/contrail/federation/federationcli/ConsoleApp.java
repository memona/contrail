package org.ow2.contrail.federation.federationcli;

import org.ow2.contrail.federation.federationcli.exceptions.InvalidSyntaxException;
import org.ow2.contrail.federation.federationcli.exceptions.NotAvailableMethodException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ConsoleApp {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage:\n" +
                    "java -jar federation-cli.jar command [command_arguments] [-url <federation-api URL>] " +
                    "[-cacert <JKS file path:password>] [-clientcert <JKS file path:password>] " +
                    "[-accesstoken <token value>]" +
                    "[-rawResponse] [-debug]\n\n" +
                    "Parameters url, cacert and clientcert can be specified also by the environment variables " +
                    "FEDERATION_CLI_URL, FEDERATION_CLI_CACERT and FEDERATION_CLI_CLIENTCERT.");
            return;
        }

        String operation = null;
        String resourceName = null;
        try {
            String command = args[0];
            String[] parts = command.split("-");
            if (parts.length != 2) {
                throw new InvalidSyntaxException(String.format("Invalid command: %s.", command));
            }
            operation = parts[0];
            resourceName = parts[1].substring(0, 1).toUpperCase() + parts[1].substring(1);

            Map<String, String> commandArgs = new HashMap<String, String>();
            int i = 1;
            while (i < args.length) {
                String name = args[i];
                if (name.startsWith("-")) {
                    name = name.substring(1);
                }
                else {
                    throw new InvalidSyntaxException(String.format("Invalid argument encountered: %s.", name));
                }

                String value;
                if (i + 1 < args.length) {
                    if (args[i + 1].startsWith("-")) {
                        value = null;
                        i++;
                    }
                    else {
                        value = args[i + 1];
                        i += 2;
                    }
                }
                else {
                    value = null;
                    i++;
                }

                commandArgs.put(name, value);
            }

            // create instance of the specified resource class
            String fqClassName = "org.ow2.contrail.federation.federationcli.resources." + resourceName;
            ResourceBase resource = null;
            Class resourceClass;
            try {
                resourceClass = Class.forName(fqClassName);
                resource = (ResourceBase) resourceClass.newInstance();
            }
            catch (Exception e) {
                throw new InvalidSyntaxException(
                        String.format("The command '%s' is invalid. Unknown resource '%s'.", command, resourceName));
            }

            // get URL and certificates
            String restServiceURL = getEnvVariable(commandArgs, "url", "FEDERATION_CLI_URL", true);
            String cacert = getEnvVariable(commandArgs, "cacert", "FEDERATION_CLI_CACERT", false);
            String clientcert = getEnvVariable(commandArgs, "clientcert", "FEDERATION_CLI_CLIENTCERT", false);
            String accessToken = getEnvVariable(commandArgs, "accesstoken", "FEDERATION_CLI_ACCESS_TOKEN", false);
            RESTClient restClient = null;
            if (restServiceURL.startsWith("https")) {
                if (cacert == null || clientcert == null) {
                    throw new InvalidSyntaxException("cacert and clientcert arguments are required to use https " +
                            "protocol.");
                }
                restClient = new RESTClient(restServiceURL, cacert, clientcert, accessToken);
            }
            else {
                restClient = new RESTClient(restServiceURL);
            }

            resource.setRestClient(restClient);

            // check if rawResponse parameter is given
            if (commandArgs.containsKey("rawResponse")) {
                commandArgs.remove("rawResponse");
                resource.setRawResponse(true);
            }

            if (commandArgs.containsKey("debug")) {
                commandArgs.remove("debug");
                resource.setDebugMode(true);
            }

            Method method = null;
            try {
                Class[] parameterTypes = new Class[]{Map.class};
                method = resourceClass.getDeclaredMethod(operation, parameterTypes);
            }
            catch (Exception e) {
                throw new InvalidSyntaxException(
                        String.format("The command '%s' is invalid. Unknown operation '%s'.", command, operation));
            }

            Response result = null;
            try {
                Object[] methodArgs = new Object[]{commandArgs};
                result = (Response) method.invoke(resource, methodArgs);
            }
            catch (InvocationTargetException e) {
                throw e.getTargetException();
            }

            if (result.getDebugData() != null) {
                System.out.println(result.getDebugData());
            }
            System.out.println(result.toJSONString());
        }
        catch (NotAvailableMethodException e){
        	System.out.println("Sorry, the command is not available.");
            System.exit(1);
        }
        catch (Throwable e) {
            System.out.println("Failed to execute command: " + e.getMessage());
            System.exit(1);
        }
    }

    private static String getEnvVariable(Map<String, String> args, String paramName,
                                         String envVariableName, boolean required) throws InvalidSyntaxException {
        if (args.containsKey(paramName)) {
            return args.remove(paramName);
        }
        else if (System.getenv(envVariableName) != null) {
            return System.getenv(envVariableName);
        }
        else if (required) {
            throw new InvalidSyntaxException(String.format("Missing argument %s.", paramName));
        }
        else {
            return null;
        }
    }
}
