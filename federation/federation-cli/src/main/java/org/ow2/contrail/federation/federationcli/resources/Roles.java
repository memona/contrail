package org.ow2.contrail.federation.federationcli.resources;

import java.util.Map;

import org.ow2.contrail.federation.federationcli.ResourceBase;
import org.ow2.contrail.federation.federationcli.Response;

public class Roles extends ResourceBase {

	@Override
	public Response get(Map<String, String> args) throws Exception {
		String[] requiredParams = {"roleId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/roles/%s", args.get("roleId"));
		return sendGETRequest(uri);
	}

	@Override
	public Response getAll(Map<String, String> args) throws Exception {
		String[] requiredParams = {};
		checkArguments(args, requiredParams);
		return sendGETRequest("/roles");
	}

	@Override
	public Response add(Map<String, String> args) throws Exception {
		String[] requiredParams = {"data"};
		checkArguments(args, requiredParams);
		return sendPOSTRequest("/roles", args.get("data"));
	}

	@Override
	public Response update(Map<String, String> args) throws Exception {
		String[] requiredParams = {"roleId", "data"};
		checkArguments(args, requiredParams);
		String uri = String.format("/roles/%s", args.get("roleId"));
		return sendPUTRequest(uri, args.get("data"));
	}

	@Override
	public Response delete(Map<String, String> args) throws Exception {
		String[] requiredParams = {"roleId"};
		checkArguments(args, requiredParams);
		String uri = String.format("/roles/%s", args.get("roleId"));
		return sendDELETERequest(uri);
	}

}
