package org.ow2.contrail.federation.federationcli;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.ws.rs.core.MediaType;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RESTClient {
    private Client client;
    protected String rootURL;
    private String accessToken;

    public RESTClient(String rootURL) {
        if (rootURL.endsWith("/")) {
            rootURL = rootURL.substring(0, rootURL.length() - 1);
        }
        this.rootURL = rootURL;
        client = Client.create();
    }

    public RESTClient(String rootURL, String cacert, String clientcert, String accessToken) throws Exception {
        this.accessToken = accessToken;
        if (rootURL.endsWith("/")) {
            rootURL = rootURL.substring(0, rootURL.length() - 1);
        }
        this.rootURL = rootURL;

        Pattern pattern = Pattern.compile("^(.+):([^:]+)$");
        Matcher m = pattern.matcher(cacert);
        if (!m.find()) {
            throw new Exception(String.format("Invalid cacert parameter value: %s", cacert));
        }
        String cacertPath = m.group(1);
        String cacertPass = m.group(2);

        m = pattern.matcher(clientcert);
        if (!m.find()) {
            throw new Exception(String.format("Invalid clientcert parameter value: %s", clientcert));
        }
        String clientcertPath = m.group(1);
        String clientcertPass = m.group(2);

        // TrustManager
        KeyStore trustStore = KeyStore.getInstance("JKS");
        try {
            trustStore.load(new FileInputStream(cacertPath), cacertPass.toCharArray());
        }
        catch (Exception e) {
            throw new Exception(String.format("Failed to load CA certificate from %s.", cacertPath));
        }
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(trustStore);

        // KeyManager
        KeyStore keyStore = KeyStore.getInstance("JKS");
        try {
            keyStore.load(new FileInputStream(clientcertPath), clientcertPass.toCharArray());
        }
        catch (Exception e) {
            throw new Exception(String.format("Failed to load client certificate from %s.", clientcertPath));
        }
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(keyStore, clientcertPass.toCharArray());

        SSLContext ctx = SSLContext.getInstance("SSL");
        ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

        ClientConfig config = new DefaultClientConfig();
        config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(null, ctx));
        client = Client.create(config);
    }

    public String getRootURL() {
        return rootURL;
    }

    public WebResource.Builder getWebResource(String uri) {
        WebResource webResource = client.resource(rootURL + uri);
        WebResource.Builder builder = webResource.accept(MediaType.APPLICATION_JSON_TYPE);
        if (accessToken != null) {
            builder = builder.header("Authorization", "Bearer " + accessToken);
        }

        return builder;
    }
}
