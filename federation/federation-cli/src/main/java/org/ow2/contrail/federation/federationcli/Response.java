package org.ow2.contrail.federation.federationcli;

import com.sun.jersey.api.client.ClientResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Response {
    private int responseCode;
    private String content;
    private HashMap<String, String> headers;
    private Status status;
    private String detailedMessage;
    private boolean rawResponse = false;
    private DebugData debugData;

    public Response(int responseCode, Status status) {
        this.responseCode = responseCode;
        this.status = status;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public void addHeader(String name, String value) {
        if (headers == null) {
            headers = new HashMap<String, String>();
        }
        headers.put(name, value);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDetailedMessage() {
        return detailedMessage;
    }

    public void setDetailedMessage(String detailedMessage) {
        this.detailedMessage = detailedMessage;
    }

    public boolean isRawResponse() {
        return rawResponse;
    }

    public void setRawResponse(boolean rawResponse) {
        this.rawResponse = rawResponse;
    }

    public DebugData getDebugData() {
        return debugData;
    }

    public void setDebugData(DebugData debugData) {
        this.debugData = debugData;
    }

    public String toJSONString() throws JSONException {
        if (rawResponse) {
            return content;
        }
        else {
            if (content != null) {
                content = content.trim();
            }

            JSONObject o = new JSONObject();
            o.put("responseCode", responseCode);
            o.put("status", status);

            // content
            if (content == null || content.equals("")) {
                //o.put("content", "");
            }
            else if (content.startsWith("{")) {
                o.put("content", new JSONObject(content));
            }
            else if (content.startsWith("[")) {
                o.put("content", new JSONArray(content));
            }
            else {
                throw new JSONException("Unexpected response received. Failed to convert to JSON: " + content);
            }

            // headers
            if (headers != null && headers.size() > 0) {
                o.put("headers", new JSONObject(headers));
            }

            // detailedMessage
            o.put("detailedMessage", detailedMessage);

            return o.toString(2);
        }
    }

    public static enum Status {
        SUCCESS,
        ERROR
    }

    public static class DebugData {
        private Map<String, String> requestHeaders = new HashMap<String, String>();
        private String method;
        private String url;
        private String requestContent;
        private ClientResponse response;
        private String responseContent;

        public DebugData(String method, String url, String requestContent,
                         ClientResponse response, String responseContent) {
            this.method = method;
            this.url = url;
            this.requestContent = requestContent;
            this.response = response;
            this.responseContent = responseContent;
        }

        public Map<String, String> getRequestHeaders() {
            return requestHeaders;
        }

        public void setRequestHeaders(Map<String, String> requestHeaders) {
            this.requestHeaders = requestHeaders;
        }

        public void addRequestHeader(String name, String value) {
            requestHeaders.put(name, value);
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String uri) {
            this.url = uri;
        }

        public ClientResponse getResponse() {
            return response;
        }

        public void setResponse(ClientResponse response) {
            this.response = response;
        }

        public String getResponseContent() {
            return responseContent;
        }

        public void setResponseContent(String responseContent) {
            this.responseContent = responseContent;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Request:\n");
            sb.append(method).append(" ").append(url).append("\n");
            for (String headerName : requestHeaders.keySet()) {
                String headerValue = requestHeaders.get(headerName);
                sb.append(headerName).append(": ").append(headerValue).append("\n");
            }
            if (requestContent != null) {
                sb.append(requestContent).append("\n");
            }

            sb.append("\n");
            sb.append("Response:\n");
            sb.append(response.getClientResponseStatus().getStatusCode()).append(" ");
            sb.append(response.getClientResponseStatus().getReasonPhrase()).append("\n");
            for (String headerName : response.getHeaders().keySet()) {
                String headerValue = response.getHeaders().getFirst(headerName);
                sb.append(headerName).append(": ").append(headerValue).append("\n");
            }
            if (responseContent != null) {
                sb.append(responseContent).append("\n");
            }
            return sb.toString();
        }
    }
}
