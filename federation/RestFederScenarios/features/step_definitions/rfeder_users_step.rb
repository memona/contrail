Given /^username "([^"]*)"$/ do |arg1|
  #pending # express the regexp above with the code you wish you had
  @username = arg1
end

Given /^first name "([^"]*)" and last name "([^"]*)"$/ do |arg1, arg2|
  #pending # express the regexp above with the code you wish you had
  @firstname = arg1
  @lastname = arg2
end

Given /^md password "([^"]*)"$/ do |arg1|
  #pending # express the regexp above with the code you wish you had
  @password = arg1
end

Given /^email "([^"]*)"$/ do |arg1|
  #pending # express the regexp above with the code you wish you had
  @email = arg1
end

When /^get users request is made$/ do
  #pending # express the regexp above with the code you wish you had
  @res = makeGetRequest(@url+"/users")
  puts "result for @res"
  pp @res
  #assert_equal(@res.empty?, false, "Should get some results!")  
end

When /^user registration data is posted$/ do
  #pending # express the regexp above with the code you wish you had
  count = 1
  count = @res[:response].size + 1 if not @res.empty?
  params = {
   "username" => @username, 
   "firstName" => @firstname,
   "lastName" => @lastname,
   "password" => @password,
   "email" => @email
  }.to_json  
  @res = makePostRequest(@url+"/users", params)  
end

Given /^user (\d+) for sla template$/ do |arg1|
  @userid = arg1
end

Given /^user "([^"]*)"$/ do |arg1|
  @username = arg1
  puts "username: #{arg1}"
  puts "param1: #{@url}/users"
  @userurl = getUrlForResourceNameFrom(@url+"/users", arg1, "username")
  @userurl = getUrlForResourceNameFrom("http://10.31.1.3:8080/federation-api/users", "rcucumber", "username")
  puts "user url: #{@userurl}"
end

Given /^sla template url "([^"]*)"$/ do |arg1|
  @slatemplateurl = arg1
end

Given /^provider (\d+) with sla template (\d+)$/ do |arg1, arg2|
  @slatemplateurl = @url+"/providers/#{arg1}/slats/#{arg2}"
end

When /^SLAT (\d+) URI retrieved$/ do |arg1|
  @slatemplateurl = @url+@providerUrl+"/slats/#{arg1}"
  puts "slatemplateURL: #{@slatemplateurl}"
end

When /^get user with id request is made$/ do
  @res = makeGetRequest(@url+@userurl)#@url+"/users/#{@userid}")
  assert_equal(@res.empty?, false, "Should get some results!")
end

When /^user is added sla template$/ do
  params = {
    "url" => @slatemplateurl
  }.to_json
  @res = makePostRequest(@url+"#{@userurl}/slats", params)
end
