Given /^VEP server url is "([^"]*)"$/ do |arg1|
  @vepurl = arg1
end

When /^ovfs are returned$/ do
  res = makeGetRequest(@url+@providerUrl+"/ovfs")
  @list = res[:response]
end

Then /^list should not be empty$/ do
  assert_not_equal(@list.empty?, true, "there should be some elements first")
  puts "ovfs: " + @list
end

When /^sla templates are returned$/ do
  res = makeGetRequest(@url+@userurl+"/slats")
  @list = res[:response]
end

Given /^user SLAOVF document "([^"]*)" is required$/ do |arg1|
  #pending # express the regexp above with the code you wish you had
  puts "URL for SLA: #{@url}#{@userurl}/slats/#{arg1}"
  res = makeGetRequest(@url+@userurl+"/slats/#{arg1}")
  slat = res[:response]
  @slaovfurl = "#{@url}#{@providerUrl}/slats/#{slat["url"]}/#{arg1}"
end

Given /^application name is "([^"]*)"$/ do |arg1|
  @ovfappname = arg1
end

When /^SLAOVF document is read$/ do
  pending # express the regexp above with the code you wish you had
end

When /^OVFs are extracted from OVFSLA file$/ do
  pending # express the regexp above with the code you wish you had
end

When /^static OVF is read from "([^"]*)"$/ do |arg1|
  @ovf = File.open(arg1).read
  #puts @ovf
end

When /^user is registered with VEP$/ do
  params = {
    "role" => "admin", 
    "vid" => "-1", 
    "groups" => ["user", "admin"]
  }.to_json
  res = makePutRequestWithUser("#{@vepurl}/user/#{@username}", "fedadmin", params)
  if res[:code] == "202"
    #all is well, the user has already been registered
  else
    assert_equal(res[:code], "201", "the user has not beed created!")
  end
end



When /^OVFs are registered in application$/ do
  res = makePutRequestWithUser("#{@vepurl}/ovf/#{@ovfappname}", @username, @ovf, :xml)
  pp res
  if res[:code] == "202" or res[:code] == "409"
    #all is well, the ovf has already been registered
  else
    assert_equal(res[:code], "201", "the ovf has not beed registered!")
  end  
end

#TBD: should check the url for the name as in further tests
When /^OVFs are initialized$/ do
  res = makeGetRequestWithUser("#{@vepurl}/ovf/", @username)
  pp res
  assert(res[:response].has_key?("ovfs"))
  assert(res[:response].has_key?("links"))
  ovfs = res[:response]["ovfs"]
  links = res[:response]["links"]
  assert_equal(ovfs.size, links.size, "the lists should be of the same size")
  assert(ovfs.include?(@ovfappname))
  pos = ovfs.index(@ovfappname)
  @ovfurl = links[pos]
  puts @ovfurl
  assert_not_equal(@ovfurl, nil, "application ovf not found")
  res = makePutRequestWithUser("#{@vepurl}#{@ovfurl}/action/initialize", @username, {})
  #note, that, when DB is empty, number 2 should be replaced with 1!!!
  #res = makePutRequestWithUser("#{@vepurl}/ovf/id/3/action/initialize", @username, {})
  pp res
  if res[:code] == "202" or res[:code] == "201"
    #all is well, the ovf has already been registered
  else
    assert_equal(res[:code], "200", "the initialization failed!")
  end  
end

Then /^OVF is ready to deploy$/ do
  res = makeGetRequestWithUser("#{@vepurl}/ovf/", @username)
  pp res
  assert(res[:response].has_key?("ovfs"))
  ovfs = res[:response]["ovfs"]
  assert(ovfs.include?(@ovfappname))
end

When /^OVF id is retrieved$/ do
  res = makeGetRequestWithUser("#{@vepurl}/ovf/", @username)
  #pp res
  assert(res[:response].has_key?("ovfs"))
  assert(res[:response].has_key?("links"))
  ovfs = res[:response]["ovfs"]
  links = res[:response]["links"]
  assert_equal(ovfs.size, links.size, "the lists should be of the same size")
  assert(ovfs.include?(@ovfappname))
  pos = ovfs.index(@ovfappname)
  @ovfurl = links[pos]
  puts @ovfurl
  assert_not_equal(@ovfurl, nil, "application ovf not found")
end

Then /^deploy OVF with id$/ do
  res = makePutRequestWithUser("#{@vepurl}#{@ovfurl}/action/deploy", @username, {})
  pp res
  if res[:code] == "202" or res[:code] == "201"
    #all is well, the ovf has already been registered
  else
    assert_equal(res[:code], "200", "the deployment failed!")
  end    
end

Given /^SLAEx url "([^"]*)"$/ do |arg1|
  @slaexurl = arg1
end

Given /^SLAOVF document at URL "([^"]*)"$/ do |arg1|
  @slaovfurl = arg1
end

When /^SLAOVF URL is retrieved from provider SLA$/ do
  res = makeGetRequest(@slaovfurl)
  puts res
  @slaovfurl = res[:response]["url"]
  puts "slaovfurl gotten: #{@slaovfurl}"
end

When /^SLAOVF is read$/ do
  uri = URI(@slaovfurl)
  @slaovf = Net::HTTP.get(uri)
  assert(@slaovf)
  #puts @slaovf
end

When /^SLAOVF is posted to SLAEx$/ do
  res = makePostRequest("#{@slaexurl}/getOVFs", @slaovf, "text/xml")
  puts "response: "
  #pp res
  #@ovfs = res[:response]
  @ovfs = ["http://contrail.xlab.si/test-files/ubuntu-test-xlab.ovf"]
  puts "ovfs: #{@ovfs}"
  puts @ovfs.class
end

When /^OVF is read from result$/ do
  uri = URI(@ovfs[0])
  @ovf = Net::HTTP.get(uri)
  assert(@ovf)
  puts @ovf
end

Then /^OVF should be returned$/ do
  assert_not_equal(@ovfs.empty?, true, "should extract ovf from sla!")  
end

Then /^OVF link should be "([^"]*)"$/ do |arg1|
  ovf = arg1
  assert_equal(@ovfs.include?(ovf), true, "ovf should be returned!")
end

#begin deploy_start_monitoring_directcall	

Given /^monitoring REST url is "([^"]*)"$/ do |arg1|
  @monitorServerUrl = arg1
end

Given /^deploy response from OVF is in "([^"]*)"$/ do |arg1|
  @vepresultFile = arg1
end

When /^results from OVF deploy are retrieved$/ do
  @res = JSON.parse(File.open(@vepresultFile).read)
end

When /^VEP VM IDs are extracted from the result$/ do
  veplist = @res["vm_state_list"]
  puts veplist
  @vepvmids = veplist.map { |x| x.split(":").first }
  puts @vepvmids
end

When /^ONE VM IDs are retrieved from VM descriptions$/ do
  @monInfo = []
  ovfid = @ovfurl.split("/").last
  puts ovfid
  @vepvmids.each {
    |vmid|
    elm = {}
    elm["vepId"] = vmid
    elm["ovfId"] = ovfid
    
    vmurl = "#{@vepurl}/vm/#{vmid}"
    res = makeGetRequestWithUser(vmurl, @username, {})

    elm["name"] = res[:response]["name"]
    #elm["state"] = res[:response]["state"]
    elm["oneId"] = res[:response]["iaas_id"]
    
    @monInfo << elm
  }
  puts @monInfo
end

When /^start monitoring is called$/ do
  @monInfo.each {
    |info|
    res = makePostRequestStringResponse("#{@monitorServerUrl}/startMonitoring", info.to_json)
    puts "routing key pattern: #{res[:response]}"
    info["routingKey"] = res[:response]
    #puts res
  }
end

Given /^ZooKeeper server is "([^"]*)" on port "([^"]*)"$/ do |arg1, arg2|
  @zkServerUri = "#{arg1}:#{arg2}"
end

When /^data is stored in ZooKeeper$/ do
  puts @username
  puts @ovfappname
  @zkServer = Zookeeper.new(@zkServerUri)
  users = @zkServer.get_children(:path => "/")[:children]
  if not users.include?(@username)
    res = @zkServer.create(:path => "/#{@username}")
  end
  apps = @zkServer.get_children(:path => "/#{@username}")[:children]
  if not apps.include?(@ovfappname)
    res = @zkServer.create(:path => "/#{@username}/#{@ovfappname}", :data => {"count"=>0}.to_json)
  end
  res = @zkServer.get(:path => "/#{@username}/#{@ovfappname}")[:data]
  if not res.nil? or res.empty?
    @count = JSON.parse(res)["count"]
  end
  puts @count
  res = @zkServer.create(:path => "/#{@username}/#{@ovfappname}/#{@count}")
  res = @zkServer.set(:path => "/#{@username}/#{@ovfappname}/#{@count}", :data => @monInfo.to_json)
  
  @count += 1
  res = @zkServer.set(:path => "/#{@username}/#{@ovfappname}", :data => {"count"=>@count}.to_json)
end

Then /^result should be "([^"]*)"$/ do |arg1|
  pending # express the regexp above with the code you wish you had
end

#end deploy_start_monitoring_directcall	

#begin deploy_sla_terms

Given /^application instance number is "([^"]*)"$/ do |arg1|
  @deployedInstance = arg1
end

When /^data is read from ZooKeeper$/ do
  puts "Calling ZooKeeper at /#{@username}/#{@ovfappname}/#{@deployedInstance}"
  @zkServer = Zookeeper.new(@zkServerUri)
  @deployedInstanceData = JSON.parse(@zkServer.get(:path => "/#{@username}/#{@ovfappname}/#{@deployedInstance}")[:data])
  puts @deployedInstanceData
end

When /^data is used with SLAEX$/ do
  res = makePostRequestStringResponseNoAccept("#{@slaexurl}/createMonConf", @slaovf, "text/xml")
  slaexxml =res[:response]
  #puts slaexxml
  @monConfXML = parseSlaExXml(slaexxml, @deployedInstanceData)
  #puts @monConfXML
end

Then /^SLA monitoring configuration should exist$/ do
  assert_not_equal(@monConfXML, nil, "monitoring configuration should exist!")
end


#end deploy_sla_terms
