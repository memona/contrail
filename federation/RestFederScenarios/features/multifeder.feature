Feature: test multiple federation nodes
The idea of this feature is to check how MySQL in a cluster configuration 
over VPN works in a multi-federation environment. 

@multifeder_add_new_provider		
Scenario: register new provider
	#Given rest server url "http://172.16.93.143:8080/federation-api"
	Given rest server url "http://10.31.1.3:8080/federation-api"
	When get providers request is made
		And new provider with name prefix "XLAB-test" and email postfix "test.info@xlab.si" is posted
	Then result code should be "201"
		And response should be empty
		And new url location should be created


@multifeder_step_1
Scenario: producer-consumer test - register SLAT on provider
	Given standard multi-federation configuration with "2" nodes
		And node "fn-producer" is federation node "0"
		And node "fn-consumer" is federation node "1"
	When get providers on node "fn-producer" is called
		And the first provider is selected from the list
		And UUID of the provider is stored
	When new SLAT "prod-con-slat" at URL "http://contrail.xlab.si/test-files/ubuntu-test-xlab-SLA.xml" is registered on selected provider on node "fn-producer"
	Then get SLATs on selected provider on node "fn-consumer" should return the same list as on node "fn-producer"
		
@multifeder_step_2
Scenario: federation nodes are all producers - registering SLATs on the provider
Given standard multi-federation configuration with "2" nodes
	And node "fn-prod1" is federation node "0"
	And node "fn-prod2" is federation node "1"
	When get providers on node "fn-prod1" is called
		And the first provider is selected from the list
		And UUID of the provider is stored
	When new SLAT "prod-prod-slats" at URL "http://contrail.xlab.si/test-files/ubuntu-test-xlab-SLA.xml" is registered "100" times concurrently on random federation node
	Then get SLATs on selected provider on node "fn-prod1" should return the same list as on node "fn-prod2"
	
@multifeder_step_del
Scenario: federation nodes are all producers - deleting registered SLATs on the provider		
Given standard multi-federation configuration with "2" nodes
	And node "fn-prod1" is federation node "0"
	And node "fn-prod2" is federation node "1"
	When get providers on node "fn-prod1" is called
		And the first provider is selected from the list
		And UUID of the provider is stored
	When random SLATs are deleted "100" times concurrently on random federation node
	Then get SLATs on selected provider on node "fn-prod1" should return the same list as on node "fn-prod2"
	
@multifeder_step_cadd
Scenario: federation nodes are all producers - adding slats on all nodes concurrently
	Given standard multi-federation configuration with "2" nodes
		And node "fn-prod1" is federation node "0"
		And node "fn-prod2" is federation node "1"
	When get providers on node "fn-prod1" is called
		And the first provider is selected from the list
		And UUID of the provider is stored
	When random SLATs with name "concurr-slats" at URL "http://contrail.xlab.si/test-files/ubuntu-test-xlab-SLA.xml" are added "100" times concurrently on random federation node
	Then get SLATs on selected provider on node "fn-prod1" should return the same list as on node "fn-prod2"
	
@multifeder_step_5
Scenario: producer registers N SLATs then deletes some, consumer accesses deleted SLATs

@multifeder_step_nonclusteradd
Scenario: federation nodes are all producers - adding slats on all nodes concurrently
	Given standard multi-federation configuration with "2" nodes
		And node "fn-prod1" is federation node "1"
	When get providers on node "fn-prod1" is called
		And the first provider is selected from the list
		And UUID of the provider is stored
	When random SLATs with name "concurr-slats" at URL "http://contrail.xlab.si/test-files/ubuntu-test-xlab-SLA.xml" are added "500" times concurrently on random federation node
	Then get SLATs on selected provider on node "fn-prod1" should return the same list as on node "fn-prod2"

@multifeder_step_nonclusterdel
Scenario: federation nodes are all producers - deleting registered SLATs on the provider		
Given standard multi-federation configuration with "2" nodes
	And node "fn-prod1" is federation node "1"
	When get providers on node "fn-prod1" is called
		And the first provider is selected from the list
		And UUID of the provider is stored
	When random SLATs are deleted "100" times concurrently on random federation node
	Then get SLATs on selected provider on node "fn-prod1" should return the same list as on node "fn-prod2"
