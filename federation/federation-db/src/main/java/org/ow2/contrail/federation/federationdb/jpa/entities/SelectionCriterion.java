package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "SelectionCriterion", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
@NamedQueries({
        @NamedQuery(name = "SelectionCriterion.findByName", query = "SELECT c FROM SelectionCriterion c WHERE c.name = :name"),
        @NamedQuery(name = "SelectionCriterion.findAll", query = "SELECT c FROM SelectionCriterion c")
})
public class SelectionCriterion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer scId;
    @Basic(optional = false)
    @Column(nullable = false, length = 45)
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(precision = 22)
    private Double defaultValue;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "selectionCriterion")
    private List<UserSelectionCriterion> userSelectionCriterionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "selectionCriterion")
    private List<ApplicationSelectionCriterion> applicationSelectionCriterionList;

    public SelectionCriterion() {
    }

    public SelectionCriterion(Integer scId) {
        this.scId = scId;
    }

    public SelectionCriterion(Integer scId, String name) {
        this.scId = scId;
        this.name = name;
    }

    public Integer getScId() {
        return scId;
    }

    public void setScId(Integer scId) {
        this.scId = scId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Double defaultValue) {
        this.defaultValue = defaultValue;
    }

    public List<UserSelectionCriterion> getUserSelectionCriterionList() {
        return userSelectionCriterionList;
    }

    public void setUserSelectionCriterionList(List<UserSelectionCriterion> userSelectionCriterionList) {
        this.userSelectionCriterionList = userSelectionCriterionList;
    }

    public List<ApplicationSelectionCriterion> getApplicationSelectionCriterionList() {
        return applicationSelectionCriterionList;
    }

    public void setApplicationSelectionCriterionList(List<ApplicationSelectionCriterion> applicationSelectionCriterionList) {
        this.applicationSelectionCriterionList = applicationSelectionCriterionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (scId != null ? scId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SelectionCriterion)) {
            return false;
        }
        SelectionCriterion other = (SelectionCriterion) object;
        if ((this.scId == null && other.scId != null) || (this.scId != null && !this.scId.equals(other.scId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.SelectionCriterion[ scId=" + scId + " ]";
    }
    
}
