package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.Vm;

import javax.persistence.EntityManager;

public class VmDAO extends BaseDAO {

    public VmDAO() {
    }

    public VmDAO(EntityManager em) {
        super(em);
    }

    public Vm findById(int providerId, int vmId) {
        try {
            Vm vm = em.find(Vm.class, vmId);
            if (vm == null || !vm.getProviderId().getProviderId().equals(providerId)) {
                return null;
            }
            else {
                return vm;
            }
        }
        finally {
            closeEntityManager();
        }
    }

    public Vm findById(String providerUuid, int vmId) {
        try {
            Vm vm = em.find(Vm.class, vmId);
            if (vm == null || !vm.getProviderId().getUuid().equals(providerUuid)) {
                return null;
            }
            else {
                return vm;
            }
        }
        finally {
            closeEntityManager();
        }
    }
}
