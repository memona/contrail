/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * @author ales
 */
@Entity
@Table(name = "Provider", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
@NamedQueries({
    @NamedQuery(name = "Provider.findAll", query = "SELECT p FROM Provider p"),
    @NamedQuery(name = "Provider.findByProviderId", query = "SELECT p FROM Provider p WHERE p.providerId = :providerId"),
    @NamedQuery(name = "Provider.findByUuid", query = "SELECT p FROM Provider p WHERE p.uuid = :uuid"),
    @NamedQuery(name = "Provider.findByName", query = "SELECT p FROM Provider p WHERE p.name = :name"),
    @NamedQuery(name = "Provider.findByProviderUri", query = "SELECT p FROM Provider p WHERE p.providerUri = :providerUri"),
    @NamedQuery(name = "Provider.findByTypeId", query = "SELECT p FROM Provider p WHERE p.typeId = :typeId")})
public class Provider implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "providerId")
    private Integer providerId;
    @Basic(optional = false)
    @Column(name = "uuid", unique = true)
    private String uuid;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @Basic(optional = false)
    @Column(name = "providerUri")
    private String providerUri;
    @Basic(optional = false)
    @Column(name = "typeId")
    private int typeId;
    @ManyToMany(mappedBy = "providerList")
    private List<User> userList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "providerId")
    private List<Vo> voList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "providerId")
    private List<Vm> vmList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "providerId")
    private List<Cluster> clusterList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "providerId")
    private List<Server> serverList;
    @OneToMany(mappedBy = "providerproviderId")
    private List<DataCenter> dataCenterList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "providerId")
    private List<Ovf> ovfList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "providerId")
    private List<SLATemplate> slaTemplateList;
    @JoinTable(name = "Provider_has_Application", joinColumns = {
        @JoinColumn(name = "providerId", referencedColumnName = "providerId")}, inverseJoinColumns = {
        @JoinColumn(name = "applicationId", referencedColumnName = "applicationId")})
    @ManyToMany
    private List<Application> applicationList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "provider", fetch = FetchType.LAZY)
    private List<Service> serviceList;

    public Provider() {
    }

    public Provider(JSONObject json) throws JSONException {
        uuid = json.has("uuid") ? json.getString("uuid") : UUID.randomUUID().toString();
        name = json.getString("name");
        providerUri = json.getString("providerUri");
        typeId = json.getInt("typeId");
        attributes = "{}";
    }

    public Provider(Integer providerId) {
        this.providerId = providerId;
        this.uuid = UUID.randomUUID().toString();
    }

    public Provider(Integer providerId, String providerUri, int typeId) {
        this.providerId = providerId;
        this.uuid = UUID.randomUUID().toString();
        this.providerUri = providerUri;
        this.typeId = typeId;
    }

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getProviderUri() {
        return providerUri;
    }

    public void setProviderUri(String providerUri) {
        this.providerUri = providerUri;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public List<Vo> getVoList() {
        return voList;
    }

    public void setVoList(List<Vo> voList) {
        this.voList = voList;
    }

    public List<Vm> getVmList() {
        return vmList;
    }

    public void setVmList(List<Vm> vmList) {
        this.vmList = vmList;
    }

    public List<Cluster> getClusterList() {
        return clusterList;
    }

    public void setClusterList(List<Cluster> clusterList) {
        this.clusterList = clusterList;
    }

    public List<Server> getServerList() {
        return serverList;
    }

    public void setServerList(List<Server> serverList) {
        this.serverList = serverList;
    }

    public List<DataCenter> getDataCenterList() {
        return dataCenterList;
    }

    public void setDataCenterList(List<DataCenter> dataCenterList) {
        this.dataCenterList = dataCenterList;
    }

    public List<Ovf> getOvfList() {
        return ovfList;
    }

    public void setOvfList(List<Ovf> ovfList) {
        this.ovfList = ovfList;
    }

    public List<SLATemplate> getSLATemplateList() {
        return slaTemplateList;
    }

    public void setSLATemplateList(List<SLATemplate> slaTemplateList) {
        this.slaTemplateList = slaTemplateList;
    }

    public List<Application> getApplicationList() {
        return applicationList;
    }

    public void setApplicationList(List<Application> applicationList) {
        this.applicationList = applicationList;
    }

    public List<Service> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (providerId != null ? providerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Provider)) {
            return false;
        }
        Provider other = (Provider) object;
        if ((this.providerId == null && other.providerId != null) || (this.providerId != null && !this.providerId.equals(other.providerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Provider[ providerId=" + providerId + " ]";
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject(attributes);
        json.put("uuid", uuid);
        json.put("name", name);
        json.put("providerUri", providerUri);
        json.put("typeId", typeId);
        return json;
    }

    public void update(JSONObject json) throws JSONException {
        if (json.has("name")) {
            name = (String)json.remove("name");
        }
        if (json.has("providerUri")) {
            providerUri = (String)json.remove("providerUri");
        }
        if (json.has("typeId")) {
            typeId = (Integer) json.remove("typeId");
        }

        JSONObject attrs = new JSONObject(attributes);
        Iterator it = json.keys();
        while (it.hasNext()) {
            String key = (String) it.next();
            Object value = json.get(key);
            attrs.put(key, value);
        }
        attributes = attrs.toString();
    }
}
