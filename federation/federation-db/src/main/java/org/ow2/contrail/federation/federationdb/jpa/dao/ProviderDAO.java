package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class ProviderDAO extends BaseDAO {

    public ProviderDAO() {
    }

    public ProviderDAO(EntityManager em) {
        super(em);
    }

    public Provider findById(int providerId) {
        try {
            return em.find(Provider.class, providerId);
        }
        finally {
            closeEntityManager();
        }
    }

    public Provider findByUuid(String uuid) {
        try {
            Query q = em.createNamedQuery("Provider.findByUuid");
            q.setParameter("uuid", uuid);
            return (Provider) q.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            closeEntityManager();
        }
    }
}
