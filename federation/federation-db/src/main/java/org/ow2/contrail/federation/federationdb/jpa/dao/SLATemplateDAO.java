package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.SLATemplate;

import javax.persistence.EntityManager;

public class SLATemplateDAO extends BaseDAO {

    public SLATemplateDAO() {
    }

    public SLATemplateDAO(EntityManager em) {
        super(em);
    }

    public SLATemplate findById(int slatId) {
        try {
            return em.find(SLATemplate.class, slatId);
        }
        finally {
            closeEntityManager();
        }
    }

    public SLATemplate findById(String providerUuid, int slatId) {
        try {
            SLATemplate slaTemplate = em.find(SLATemplate.class, slatId);
            if (slaTemplate == null || !slaTemplate.getProviderId().getUuid().equals(providerUuid)) {
                return null;
            }
            else {
                return slaTemplate;
            }
        }
        finally {
            closeEntityManager();
        }
    }
}
