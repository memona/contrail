/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "VO", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
@NamedQueries({
    @NamedQuery(name = "Vo.findAll", query = "SELECT v FROM Vo v"),
    @NamedQuery(name = "Vo.findByVoId", query = "SELECT v FROM Vo v WHERE v.voId = :voId"),
    @NamedQuery(name = "Vo.findByName", query = "SELECT v FROM Vo v WHERE v.name = :name")})
public class Vo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "voId")
    private Integer voId;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @JoinTable(name = "VO_has_Cluster", joinColumns = {
        @JoinColumn(name = "voId", referencedColumnName = "voId")}, inverseJoinColumns = {
        @JoinColumn(name = "clusterId", referencedColumnName = "clusterId")})
    @ManyToMany
    private List<Cluster> clusterList;
    @JoinTable(name = "VO_has_EDC", joinColumns = {
        @JoinColumn(name = "voId", referencedColumnName = "voId")}, inverseJoinColumns = {
        @JoinColumn(name = "edcId", referencedColumnName = "edcId")})
    @ManyToMany
    private List<Edc> edcList;
    @JoinColumn(name = "providerId", referencedColumnName = "providerId")
    @ManyToOne(optional = false)
    private Provider providerId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "voId")
    private List<Cee> ceeList;

    public Vo() {
    }

    public Vo(JSONObject json) throws JSONException {
        String name = (String) json.remove("name");
        this.setName(name);
        this.setAttributes(json.toString());
    }


    public Vo(Integer voId) {
        this.voId = voId;
    }

    public Integer getVoId() {
        return voId;
    }

    public void setVoId(Integer voId) {
        this.voId = voId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public List<Cluster> getClusterList() {
        return clusterList;
    }

    public void setClusterList(List<Cluster> clusterList) {
        this.clusterList = clusterList;
    }

    public List<Edc> getEdcList() {
        return edcList;
    }

    public void setEdcList(List<Edc> edcList) {
        this.edcList = edcList;
    }

    public Provider getProviderId() {
        return providerId;
    }

    public void setProviderId(Provider providerId) {
        this.providerId = providerId;
    }

    public List<Cee> getCeeList() {
        return ceeList;
    }

    public void setCeeList(List<Cee> ceeList) {
        this.ceeList = ceeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (voId != null ? voId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vo)) {
            return false;
        }
        Vo other = (Vo) object;
        if ((this.voId == null && other.voId != null) || (this.voId != null && !this.voId.equals(other.voId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Vo[ voId=" + voId + " ]";
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject(attributes);
        json.put("name", name);
        return json;
    }

    public void update(JSONObject json) throws JSONException {
        if (json.has("name")) {
            name = (String)json.remove("name");
        }

        JSONObject attrs = new JSONObject(attributes);
        Iterator it = json.keys();
        while (it.hasNext()) {
            String key = (String) it.next();
            Object value = json.get(key);
            attrs.put(key, value);
        }
        attributes = attrs.toString();
    }
}
