/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "Attribute")
@NamedQueries({
    @NamedQuery(name = "Attribute.findAll", query = "SELECT a FROM Attribute a"),
    @NamedQuery(name = "Attribute.findByAttributeUuid", query = "SELECT a FROM Attribute a WHERE a.attributeUuid = :attributeUuid"),
    @NamedQuery(name = "Attribute.findByName", query = "SELECT a FROM Attribute a WHERE a.name = :name"),
    @NamedQuery(name = "Attribute.findByUri", query = "SELECT a FROM Attribute a WHERE a.uri = :uri"),
    @NamedQuery(name = "Attribute.findByDefaultValue", query = "SELECT a FROM Attribute a WHERE a.defaultValue = :defaultValue"),
    @NamedQuery(name = "Attribute.findByReference", query = "SELECT a FROM Attribute a WHERE a.reference = :reference"),
    @NamedQuery(name = "Attribute.findByDescription", query = "SELECT a FROM Attribute a WHERE a.description = :description")})
public class Attribute implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "attributeUuid")
    private String attributeUuid;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "uri")
    private String uri;
    @Column(name = "defaultValue")
    private String defaultValue;
    @Column(name = "reference")
    private String reference;
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "attribute")
    private List<UserhasAttribute> userhasAttributeList;

    public Attribute() {
    }

    public Attribute(String attributeUuid) {
        this.attributeUuid = attributeUuid;
    }

    public Attribute(String attributeUuid, String name) {
        this.attributeUuid = attributeUuid;
        this.name = name;
    }

    public String getAttributeUuid() {
        return attributeUuid;
    }

    public void setAttributeUuid(String attributeUuid) {
        this.attributeUuid = attributeUuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<UserhasAttribute> getUserhasAttributeList() {
        return userhasAttributeList;
    }

    public void setUserhasAttributeList(List<UserhasAttribute> userhasAttributeList) {
        this.userhasAttributeList = userhasAttributeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (attributeUuid != null ? attributeUuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Attribute)) {
            return false;
        }
        Attribute other = (Attribute) object;
        if ((this.attributeUuid == null && other.attributeUuid != null) || (this.attributeUuid != null && !this.attributeUuid.equals(other.attributeUuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.Attribute[ attributeUuid=" + attributeUuid + " ]";
    }
    
}
