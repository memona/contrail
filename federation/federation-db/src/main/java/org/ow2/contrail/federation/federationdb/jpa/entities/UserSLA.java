/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "UserSLA")
@NamedQueries({
    @NamedQuery(name = "UserSLA.findAll", query = "SELECT u FROM UserSLA u"),
    @NamedQuery(name = "UserSLA.findBySLAId", query = "SELECT u FROM UserSLA u WHERE u.sLAId = :sLAId"),
    @NamedQuery(name = "UserSLA.findByTemplateURL", query = "SELECT u FROM UserSLA u WHERE u.templateURL = :templateURL")})
public class UserSLA implements Serializable {
    @Column(name = "name")
    private String name;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SLAId")
    private Integer sLAId;
    @Lob
    @Column(name = "sla")
    private String sla;
    @Column(name = "templateURL")
    private String templateURL;
    @Lob
    @Column(name = "content")
    private String content;
    @JoinColumn(name = "SLATemplateId", referencedColumnName = "SLATemplateId")
    @ManyToOne
    private UserSLATemplate sLATemplateId;
    @JoinColumn(name = "userId", referencedColumnName = "userId")
    @ManyToOne(optional = false)
    private User userId;

    public UserSLA() {
    }

    public UserSLA(Integer sLAId) {
        this.sLAId = sLAId;
    }

    public Integer getSLAId() {
        return sLAId;
    }

    public void setSLAId(Integer sLAId) {
        this.sLAId = sLAId;
    }

    public String getSla() {
        return sla;
    }

    public void setSla(String sla) {
        this.sla = sla;
    }

    public String getTemplateURL() {
        return templateURL;
    }

    public void setTemplateURL(String templateURL) {
        this.templateURL = templateURL;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public UserSLATemplate getSLATemplateId() {
        return sLATemplateId;
    }

    public void setSLATemplateId(UserSLATemplate sLATemplateId) {
        this.sLATemplateId = sLATemplateId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sLAId != null ? sLAId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserSLA)) {
            return false;
        }
        UserSLA other = (UserSLA) object;
        if ((this.sLAId == null && other.sLAId != null) || (this.sLAId != null && !this.sLAId.equals(other.sLAId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.UserSLA[ sLAId=" + sLAId + " ]";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
