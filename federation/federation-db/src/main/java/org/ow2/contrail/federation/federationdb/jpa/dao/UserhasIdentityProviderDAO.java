/**
 *
 */
package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.UserhasidentityProvider;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserhasidentityProviderPK;

import javax.persistence.EntityManager;

/**
 * @author ales
 */
public class UserhasIdentityProviderDAO extends BaseDAO {

    public UserhasIdentityProviderDAO() {
    }

    public UserhasIdentityProviderDAO(EntityManager em) {
        super(em);
    }

    public UserhasidentityProvider findById(int usrId, String idpUuid) {
        try {
            return em.find(UserhasidentityProvider.class, new UserhasidentityProviderPK(usrId, idpUuid));
        }
        finally {
            closeEntityManager();
        }
    }
}
