package org.ow2.contrail.federation.federationdb.jpa.dao;

import org.ow2.contrail.federation.federationdb.jpa.entities.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

public class ServiceDAO extends BaseDAO {

    public ServiceDAO() {
    }

    public ServiceDAO(EntityManager em) {
        super(em);
    }

    public Service findById(String providerUuid, int serviceId) {
        try {
            Service service = em.find(Service.class, serviceId);
            if (service == null || !service.getProvider().getUuid().equals(providerUuid)) {
                return null;
            }
            else {
                return service;
            }
        }
        finally {
            closeEntityManager();
        }
    }

    public Service findByName(String providerUuid, String name) {
        try {
            TypedQuery<Service> q = em.createNamedQuery("Service.findByServiceName", Service.class);
            q.setParameter("name", name);
            Service service = q.getSingleResult();
            if (service == null || !service.getProvider().getUuid().equals(providerUuid)) {
                return null;
            }
            else {
                return service;
            }
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            closeEntityManager();
        }
    }
}
