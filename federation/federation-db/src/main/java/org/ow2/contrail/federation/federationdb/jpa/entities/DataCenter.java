/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.federation.federationdb.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author ales
 */
@Entity
@Table(name = "DataCenter")
@NamedQueries({
    @NamedQuery(name = "DataCenter.findAll", query = "SELECT d FROM DataCenter d"),
    @NamedQuery(name = "DataCenter.findByDcId", query = "SELECT d FROM DataCenter d WHERE d.dcId = :dcId"),
    @NamedQuery(name = "DataCenter.findByName", query = "SELECT d FROM DataCenter d WHERE d.name = :name")})
public class DataCenter implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dcId")
    private Integer dcId;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "attributes")
    private String attributes;
    @JoinTable(name = "DataCenter_has_Storage", joinColumns = {
        @JoinColumn(name = "dcId", referencedColumnName = "dcId")}, inverseJoinColumns = {
        @JoinColumn(name = "storageId", referencedColumnName = "storageId")})
    @ManyToMany
    private List<Storage> storageList;
    @JoinTable(name = "DataCenter_has_Cluster", joinColumns = {
        @JoinColumn(name = "dcId", referencedColumnName = "dcId")}, inverseJoinColumns = {
        @JoinColumn(name = "clusterId", referencedColumnName = "clusterId")})
    @ManyToMany
    private List<Cluster> clusterList;
    @JoinTable(name = "DataCenter_has_Network", joinColumns = {
        @JoinColumn(name = "dcId", referencedColumnName = "dcId")}, inverseJoinColumns = {
        @JoinColumn(name = "networkId", referencedColumnName = "networkId")})
    @ManyToMany
    private List<Network> networkList;
    @JoinColumn(name = "Provider_providerId", referencedColumnName = "providerId")
    @ManyToOne
    private Provider providerproviderId;

    public DataCenter() {
    }

    public DataCenter(Integer dcId) {
        this.dcId = dcId;
    }

    public Integer getDcId() {
        return dcId;
    }

    public void setDcId(Integer dcId) {
        this.dcId = dcId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public List<Storage> getStorageList() {
        return storageList;
    }

    public void setStorageList(List<Storage> storageList) {
        this.storageList = storageList;
    }

    public List<Cluster> getClusterList() {
        return clusterList;
    }

    public void setClusterList(List<Cluster> clusterList) {
        this.clusterList = clusterList;
    }

    public List<Network> getNetworkList() {
        return networkList;
    }

    public void setNetworkList(List<Network> networkList) {
        this.networkList = networkList;
    }

    public Provider getProviderproviderId() {
        return providerproviderId;
    }

    public void setProviderproviderId(Provider providerproviderId) {
        this.providerproviderId = providerproviderId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dcId != null ? dcId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DataCenter)) {
            return false;
        }
        DataCenter other = (DataCenter) object;
        if ((this.dcId == null && other.dcId != null) || (this.dcId != null && !this.dcId.equals(other.dcId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.DataCenter[ dcId=" + dcId + " ]";
    }
    
}
