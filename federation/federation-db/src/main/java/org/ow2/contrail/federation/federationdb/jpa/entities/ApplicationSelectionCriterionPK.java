package org.ow2.contrail.federation.federationdb.jpa.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ApplicationSelectionCriterionPK implements Serializable {
    @Basic(optional = false)
    @Column(nullable = false)
    private int applicationId;
    @Basic(optional = false)
    @Column(nullable = false)
    private int scId;

    public ApplicationSelectionCriterionPK() {
    }

    public ApplicationSelectionCriterionPK(int applicationId, int scId) {
        this.applicationId = applicationId;
        this.scId = scId;
    }

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }

    public int getScId() {
        return scId;
    }

    public void setScId(int scId) {
        this.scId = scId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) applicationId;
        hash += (int) scId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApplicationSelectionCriterionPK)) {
            return false;
        }
        ApplicationSelectionCriterionPK other = (ApplicationSelectionCriterionPK) object;
        if (this.applicationId != other.applicationId) {
            return false;
        }
        if (this.scId != other.scId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ow2.contrail.federation.federationdb.jpa.entities.ApplicationSelectionCriterionPK[ applicationId=" + applicationId + ", scId=" + scId + " ]";
    }
    
}
