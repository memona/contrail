package org.ow2.contrail.federation.federationcore.monitoring;

public abstract class BaseMonitoringThread extends Thread
{
	protected boolean execution;
	
	public BaseMonitoringThread()
	{
		super();
	}
	
	public void stopExecution()
	{
		execution = false;
	}
}
