package org.ow2.contrail.federation.federationcore.test.junit;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.util.Collection;
import java.util.Iterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.common.ParserManager;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.common.implementation.application.ApplicationDescriptor;
import org.ow2.contrail.common.implementation.ovf.Disk;
import org.ow2.contrail.common.implementation.ovf.OVFParser;
import org.ow2.contrail.common.implementation.ovf.OVFVirtualNetwork;
import org.ow2.contrail.common.implementation.ovf.SharedDisk;
import org.ow2.contrail.federation.federationcore.aem.mapping.ApplicationGraph;
import org.ow2.contrail.federation.federationcore.aem.mapping.ContrailEdge;
import org.ow2.contrail.federation.federationcore.utils.GraphUtils;

public class Ovf2Graph_SharedDisk {

	ApplicationGraph<String, ContrailEdge> appGraphDesc;
	Collection<ApplianceDescriptor> appl;
	ParserManager pm;
	final String ovfResource = "ovf-2vms-vep2.0_shared.xml";
	
	@Before
	public void setUp() throws Exception {
		URI uri = new URI("src/main/resources/" + ovfResource );
		ApplicationDescriptor ap = OVFParser.ParseOVF(uri);

		appGraphDesc = GraphUtils.MakeGraph(-1, ap);
		appl = appGraphDesc.getAppliances();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	public void testNumber() {
		final int expectedAppliances = 2;
		int size = appl.size();
		assertEquals(size,expectedAppliances);
	}
	
	@Test
	public void testAppliances() {
		Iterator<ApplianceDescriptor> i = appl.iterator();
		testNumber();
		ApplianceDescriptor d1 = i.next();
		assertEquals(d1.getID(), "VirtualSystem1");
		assertEquals(d1.getDefaultVmNumber(), 1);
		
		ApplianceDescriptor d2 = i.next();
		assertEquals(d2.getID(), "VirtualSystem2");
		assertEquals(d1.getDefaultVmNumber(), 1);
	}
	
	@Test
	public void testDisk() {
		Iterator<ApplianceDescriptor> it = appl.iterator();
		
		//virtualSystem1
		Collection<Disk> disks = it.next().getDisks();
		Iterator<Disk> id = disks.iterator();
		assertEquals(disks.size(), 1);
		assertEquals(id.next().getId(), "ubuntu1");
		
		//virtualSystem2
		Collection<Disk> disks2 = it.next().getDisks();
		Iterator<Disk> id2 = disks2.iterator();
		assertEquals(disks2.size(), 1);
		assertEquals(id2.next().getId(), "ubuntu2");
	}
	
	@Test
	public void testSharedDisk() {
		Iterator<ApplianceDescriptor> it = appl.iterator();
		
		//virtualSystem1
		Collection<SharedDisk> disks = it.next().getSharedDisks();
		Iterator<SharedDisk> id = disks.iterator();
		assertEquals(disks.size(), 1);
		SharedDisk d = id.next(); 
		assertEquals(d.getId(), "shared1");
		assertEquals(d.getCapacity(), "8589934592");
		
		// following two express same condition
		assertEquals(d.isTemporary(), true);
		assertEquals(d.getFormat(), "http://www.xtreemfs.org/specifications/format.html#temporary");
		
		//virtualSystem2
		Collection<SharedDisk> disks2 = it.next().getSharedDisks();
		Iterator<SharedDisk> id2 = disks2.iterator();
		assertEquals(disks2.size(), 1);
		SharedDisk d2 = id2.next(); 
		assertEquals(d2.getId(), "shared2");
		assertEquals(d2.getCapacity(), "8589934592");
		
		// following two express same condition
		assertEquals(d2.isVirtualNetworked(), true);
		assertEquals(d2.getFormat(), "http://www.xtreemfs.org/specifications/format.html#virtualNetworked");
		
	}
	
	@Test
	public void testNet() {
		Iterator<ApplianceDescriptor> it = appl.iterator();
		
		//virtualSystemColection
		// Collection<String> nets = pm.getApplianceNetworks(i.next());
		Collection<OVFVirtualNetwork> nets = it.next().getAssociatedVirtualNetworks();
		Iterator<OVFVirtualNetwork> id = nets.iterator();
		
		assertEquals(id.next().getName(), "conero-public-ranged-network");
		
		//virtualSystem1
		nets = it.next().getAssociatedVirtualNetworks();
		id = nets.iterator();
		assertEquals(id.next().getName(), "conero-public-ranged-network");
		assertEquals(id.next().getName(), "conero-private-ranged-network");
	}

}
