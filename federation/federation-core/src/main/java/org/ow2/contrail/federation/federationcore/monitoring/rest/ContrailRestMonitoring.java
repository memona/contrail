package org.ow2.contrail.federation.federationcore.monitoring.rest;

import org.ow2.contrail.federation.federationcore.adapter.providermanager.BaseProviderManager;
import org.ow2.contrail.federation.federationcore.monitoring.BaseProviderMonitoring;
import org.ow2.contrail.federation.federationcore.monitoring.HostMetrics;
import org.ow2.contrail.federation.federationcore.monitoring.VMMetrics;
import org.ow2.contrail.federation.federationcore.monitoring.message.BaseMessage;
import org.ow2.contrail.federation.federationcore.monitoring.watcher.ProviderWatcherManager;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class ContrailRestMonitoring extends BaseProviderMonitoring
{
	private WebResource service;
	
	public ContrailRestMonitoring(ProviderWatcherManager manager,
			BaseProviderManager provider) 
	{
		super(manager, provider);
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		this.service = client.resource(provider.getProviderAddress());
	}

	@Override
	public void start() 
	{
		/*
		for (HostMetrics metric: HostMetrics.values())
		{
			// TODO subscription
			String host = "???????";
			String substring = 
					this.provider.getProviderAddress()+"/offers/subscribe/hub:offer:"+"#"+":"+host+metric;
			service.path(substring).put();
			
			// TODO handling
			String request = "";
			
			// String resturl, long interval, WebResource service, BaseMessage.MessageType mtype
			
			RESTMetricFetcher fetcher = new RESTMetricFetcher(request, 5000, service, BaseMessage.MessageType.PROVIDER);
			fetcher.start();
			this.pool.add(fetcher);
		}
		*/
	}
	
}
