package org.ow2.contrail.federation.federationcore.sla.soap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.federation.federationcore.aem.application.CoreSelectionCriterion;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.InvalidNegotiationIDExceptionException;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.OperationInProgressExceptionException;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.OperationNotPossibleExceptionException;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.CancelNegotiation;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.CancelNegotiationResponse;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.CreateAgreement;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.CreateAgreementResponse;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.InitiateNegotiation;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.InitiateNegotiationResponse;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.Negotiate;
import org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.NegotiateResponse;
import org.ow2.contrail.federation.federationcore.sla.translation.SlaTranslator;
import org.ow2.contrail.federation.federationcore.sla.translation.SlaTranslatorImplNoOsgi;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

public class SoapSlamInterface {
	
	private static Logger logger = Logger.getLogger(SoapSlamInterface.class);
	private final String def_endpoint = "http://146.48.81.248:8080/services/federationNegotiation?wsdl";
	private SlaTranslator slaTranslator = new SlaTranslatorImplNoOsgi();
	private String endpoint;
	 
	public SoapSlamInterface(){	
		String epoint = getFSLAMFromFedSettings();
		//String epoint = def_endpoint;
		if (epoint.equals("") | epoint.equals("?wsdl")) {
			epoint = def_endpoint;
			logger.debug("[FSLAM Adapter] Endpoint set with default " + epoint);
		}
		else {
			logger.debug("[FSLAM Adapter] Endpoint set with " + epoint);
        }
		this.endpoint = epoint;
	}
	
	private static String getFSLAMFromFedSettings() {
		String fslam = null;
		fslam = FederationProperties.getInstance().getProperty("sla.negotiation.endpoint"); // as defined by fed-API
		
		if (fslam.equals(""))
			fslam = FederationProperties.getInstance().getProperty("fslam-endpoint");
		else
			fslam+="?wsdl";
	
		return fslam;
	}
	
	public SLATemplate parseSLATemplate(String slaTemplate) throws Exception{	
		SLATemplate slaT = slaTranslator.parseSlaTemplate(slaTemplate);
		return slaT;
		
	}
	
	public String renderSLATemplate(SLATemplate slaTemplate) throws Exception{
		String slaTemplateString = slaTranslator.renderSlaTemplate(slaTemplate);
		return slaTemplateString;
	}
	
	public String renderSLA(SLA sla) throws Exception{
		String slaRendered = slaTranslator.renderSla(sla);
		return slaRendered;
		
	}
	
	public SLA parseSLA(String slaContent) throws Exception{
		SLA sla = slaTranslator.parseSla(slaContent);
		return sla;
	}
	
	public SLATemplate addProviderList(SLATemplate slat, Provider[] providerList){
		JSONObject jsonProviderList = new JSONObject(); 
		JSONArray listProvider = new JSONArray();
		try {
			for(int i=0; i<providerList.length; i++){
				JSONObject jsonProvider = new JSONObject();
				jsonProvider.put("provider-uuid", providerList[i].getProviderId()+"");	
				logger.trace("provider-uuid: " + providerList[i].getProviderId() );
				try {
					JSONObject providerPSLAMUri = new JSONObject(providerList[i].getAttributes());
					logger.trace("Provider PSALM-URL: "+ providerPSLAMUri.get("p-slam-url"));
					jsonProvider.put("p-slam-url", providerPSLAMUri.get("p-slam-url"));
					//jsonProvider.put("p-slam-url", "http://192.168.57.12:8080/services/contrailNegotiation?wsdl"); //TODO CHECK if the url is correct	
					logger.trace("adding provider: " + jsonProvider.getString("p-slam-url") +" to SLATemplate");
					listProvider.put(jsonProvider);
					jsonProviderList.put("ProvidersList", listProvider);
					//logger.info(jsonProviderList);
					STND property = new STND("ProvidersList");
					slat.setPropertyValue(property, jsonProviderList.toString());
					
				} catch (Exception ex) {
					logger.error("[FSLAM Adapter] Provider "+providerList[i].getProviderId() +" missing p-slam-url attributes .. skipping Provider");
				}
				//String providerPSLAMUri = providerPSLAMUriSplitted[0] + providerPSLAMUriSplitted[1] + ":8080/services/contrailNegotiation?wsdl";
				//logger.info(providerPSLAMUri.get("p-slam-url"));
			}
			logger.info(jsonProviderList.toString());
		} catch (JSONException e) {
			e.printStackTrace();
			logger.error("[FSLAM Adapter] Error adding ProviderList in SLATemplate");
		}
		return slat;
	}
	
	public SLATemplate addCriteria(SLATemplate slat, CoreSelectionCriterion criteria[]){
		if (criteria != null){
			JSONObject jsonCriteria = new JSONObject(); 
			try {
				for(int i=0; i<criteria.length; i++){
					jsonCriteria.put(criteria[i].getName(), criteria[i].getValue()+"");
				}
				STND property = new STND("Criteria");
				slat.setPropertyValue(property, jsonCriteria.toString());
				logger.info("Criteria added to SLATemplate:");
				logger.info(jsonCriteria.toString());
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("[FSLAM Adapter] Error adding Criteria in SLATemplate");
			}
		}
		return slat;
	}
	
	public SLATemplate addUserUuid(SLATemplate slat, String uuid){
		try {
			STND property = new STND("UserUUID");
			slat.setPropertyValue(property, uuid);
			logger.info("UserUuid added to SLATemplate:");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[FSLAM Adapter] Error adding UserUuid in SLATemplate");
		}
		return slat;
	}
	
	public SLATemplate addAppUuid(SLATemplate slat, String appUuid){
		try {
			STND property = new STND("AppUUID");
			slat.setPropertyValue(property, appUuid);
			logger.info("AppUUID added to SLATemplate:");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[FSLAM Adapter] Error adding AppUUID in SLATemplate");
		}
		return slat;
	}
	
	public SLATemplate updateFslamEndpoint(SLATemplate slaTemplate, String userUuid){
		Party slaTemplateParty = getProviderParty(slaTemplate);
	    Party slaTemplateParty2 = getCustomerParty(slaTemplate);
	    
	    slaTemplateParty.setPropertyValue(org.slasoi.slamodel.vocab.sla.gslam_epr, endpoint);
	    //slaTemplateParty2.setId(new ID(userUuid));
	    Party[] slaTemplateParties = new Party[]{slaTemplateParty, slaTemplateParty2};
	    slaTemplate.setParties(slaTemplateParties);
		return slaTemplate;
	}
	
	private Party getProviderParty(SLATemplate slaTemplate) {
		Party[] parties = slaTemplate.getParties();
		for (Party party : parties) {
			STND partyRole = (STND) party.getAgreementRole();
			if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.provider)) {
				return party;
			}
		}
		return null;
	}
		
	private Party getCustomerParty(SLATemplate slaTemplate) {
		Party[] parties = slaTemplate.getParties();
		for (Party party : parties) {
			STND partyRole = (STND) party.getAgreementRole();
			if (partyRole != null && partyRole.equals(org.slasoi.slamodel.vocab.sla.customer)) {
				return party;
			}
		}
		return null;
	}
	
	
	public String initiateNegotiation(SLATemplate slaTemplate) throws Exception{
		FederationNegotiationStub stub = new FederationNegotiationStub(this.endpoint);
		InitiateNegotiation doc = getInitiateNegotiationDoc(slaTemplate);
	    InitiateNegotiationResponse resp = stub.initiateNegotiation(doc);
	    return resp.get_return();
	    
	}
	
	private InitiateNegotiation getInitiateNegotiationDoc(SLATemplate slat) throws java.lang.Exception {
		InitiateNegotiation doc = new InitiateNegotiation();
		String xmlSlat = slaTranslator.renderSlaTemplate(slat);
		doc.setSlaTemplate(xmlSlat);
		return doc;
	}
	
	public SLATemplate[] negotiate(SLATemplate slaTemplate, String negotiationId) {
		
		List<SLATemplate> slats = new ArrayList<SLATemplate>(); 
		//logger.info("[FSLAM Adapter] Negotiation : endpoint: " + this.endpoint);
		try { 
			FederationNegotiationStub stub = new FederationNegotiationStub(this.endpoint);
			Negotiate doc = getNegotiationDoc(negotiationId, slaTemplate);
			//logger.info("[FSLAM Adapter] Negotiation Id in doc: "+doc.getNegotiationID().toString());
			//logger.info("[FSLAM Adapter] " + doc.getSlaTemplate().toString());
			NegotiateResponse resp = stub.negotiate(doc);
			
			String[] xmlSlats = resp.get_return();
			
			for(String xmlSlat : xmlSlats) {
				SLATemplate slat = (xmlSlat == null) ? null : slaTranslator.parseSlaTemplate(xmlSlat);
				slats.add(slat);
			}
			
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}
		return (slats.toArray(new SLATemplate[slats.size()]));
	}
	
	private Negotiate getNegotiationDoc(String negotiationId, SLATemplate slat) throws java.lang.Exception {

		Negotiate doc = new Negotiate();
		String xmlSlat = slaTranslator.renderSlaTemplate(slat);
		doc.setNegotiationID(negotiationId);
		doc.setSlaTemplate(xmlSlat);
		return doc;
	}
	
	public SLA createAgreement(SLATemplate slaTemplate, String negotiationId) {
		
		SLA sla = null;
		try { 
			FederationNegotiationStub stub = new FederationNegotiationStub(this.endpoint);
			CreateAgreement doc = getAgreementDoc(negotiationId, slaTemplate);
			CreateAgreementResponse resp = stub.createAgreement(doc);
			String xmlSla = resp.get_return();
			//logger.info("[FSLAM Adapter] CreateAgreement sla agreed: "+ xmlSla);
			sla = (xmlSla == null) ? null : slaTranslator.parseSla(xmlSla);
			
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}
		
		return sla;
	}
	
	private CreateAgreement getAgreementDoc(String negotiationId, SLATemplate slat) throws java.lang.Exception {

		CreateAgreement doc = new CreateAgreement();
		String xmlSlat = slaTranslator.renderSlaTemplate(slat);
		doc.setNegotiationID(negotiationId);
		doc.setSlaTemplate(xmlSlat);
		return doc;
	}
	
	public boolean cancelNegotiation(String negotiationId) throws RemoteException, OperationInProgressExceptionException, OperationNotPossibleExceptionException, InvalidNegotiationIDExceptionException{
		FederationNegotiationStub stub = new FederationNegotiationStub(endpoint);
		CancelNegotiation cancelDoc = new CancelNegotiation();
		cancelDoc.setNegotiationID(negotiationId);
		CancelNegotiationResponse cancelResponse = stub.cancelNegotiation(cancelDoc);
		return cancelResponse.get_return();
	}
	
	public static void writeIntoFile(String xmlSource, String path)  {
	    try {
	    	logger.debug("[FSLAM Adapter] storing SLATemplate into file: "+path);
	    	
	    	File file= new File(path);
	    	if (!file.exists()) {
				file.createNewFile();
			}
			java.io.FileWriter fw = new java.io.FileWriter(file);
			logger.debug("[FSLAM Adapter] writing SLATemplate into: "+ file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(xmlSource);
			bw.close();
		} catch (IOException e) {
			logger.error("[FSLAM Adapter] Error storing SLATemplate into file");
			String stackTrace ="";
			for(StackTraceElement st: e.getStackTrace())
				stackTrace += st +"\n";
			logger.error(stackTrace);
		}
	}

	public static void main (String[] args) throws Exception{
		new SoapSlamInterface();
	}
	
}
