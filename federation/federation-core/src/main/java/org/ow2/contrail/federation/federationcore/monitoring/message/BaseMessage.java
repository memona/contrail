package org.ow2.contrail.federation.federationcore.monitoring.message;

/**
 * Abstract class for a monitoring or application-level event.
 */
public abstract class BaseMessage 
{
	protected boolean isInterrupt = false;
	
	public void setInterrupt(boolean value)
	{
		isInterrupt = value;
	}
	
	/**
	 * True is the message has to routed to the federation runtime manager.
	 */
	public boolean isInterrupt()
	{
		return isInterrupt;
	}
	
	public enum MessageType
	{
		APPLICATION,
		PROVIDER
	}
}

