package org.ow2.contrail.federation.federationcore.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import javax.persistence.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationcore.adapter.providermanager.CloudProviderId;
import org.ow2.contrail.federation.federationcore.aem.mapping.*;
import org.ow2.contrail.federation.federationcore.exception.ProviderTypeNotYetSupportedException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderTypeException;
import org.ow2.contrail.federation.federationcore.utils.GraphUtils;
import org.ow2.contrail.federation.federationdb.jpa.entities.*;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;
import org.ow2.contrail.common.ParserManager;
import org.ow2.contrail.common.implementation.application.*;
import org.ow2.contrail.common.implementation.ovf.*;
import org.ow2.contrail.common.implementation.ovf.virtualhardware.*;


public class ProvisioningAdapter {
	private static Logger logger = Logger.getLogger(ProvisioningAdapter.class);
	User u = new User();

	public ProvisioningAdapter() throws UnknownProviderTypeException, ProviderTypeNotYetSupportedException{
		u.setUsername("barabba");
	}
	
	public User getUser(){ return u;}
		
	public void printApplicationGraph(ApplicationGraph<String, ContrailEdge> ag){
		Collection<ApplianceDescriptor> adc = ag.getAppliances();
		for (ApplianceDescriptor appl : adc){
			System.out.println("Appliance (VirtualSystem): " + appl.getID());
			Set<ContrailEdge> egs = ag.edgesOf(appl.getID());
			System.out.println("Edges of " + appl.getID() + " :");
			for (ContrailEdge e : egs){
				System.out.println(e.getEdgeName());
			}
			System.out.println("___________________________________");
		}	
	}
	
	public static void printVirtualHardware(ApplicationGraph<String, ContrailEdge> ag){
		Collection<ApplianceDescriptor> adc = ag.getAppliances();
		for (ApplianceDescriptor appl : adc){
			System.out.println("VirtualSystem: " + appl.getID());
			Collection<OVFVirtualSystem> vsc = appl.getVirtualSystems();
			for (OVFVirtualSystem vs: vsc){
				Collection<OVFVirtualHardware> vh_c = vs.getRequiredHardware();
				Iterator<OVFVirtualHardware> i = vh_c.iterator();
				
				OVFVirtualHwCpu cpu = (OVFVirtualHwCpu) i.next();
				System.out.println("CPU " + cpu.getVirtualQuantity());
				
				OVFVirtualHwMemory mem = (OVFVirtualHwMemory) i.next();
				System.out.println("Mem " + mem.getVirtualQuantity());
			}
		}
	}
	
	/*
	 * @return An array of integers representing the virtual memory 
	 * 	       required by each VM contained in the appliance
	 */
	
	/*
	public static int[] getVirtualMemoryRequired(ApplianceDescriptor appl){
		Collection<OVFVirtualSystem> vsc = appl.getVirtualSystems();
		int[] virtualMem = new int[vsc.size()];
		int j=0;
		for (OVFVirtualSystem vs: vsc){
			Collection<OVFVirtualHardware> vh_c = vs.getRequiredHardware();
			Iterator<OVFVirtualHardware> i = vh_c.iterator();
			OVFVirtualHwCpu cpu = (OVFVirtualHwCpu) i.next();
			OVFVirtualHwMemory mem = (OVFVirtualHwMemory) i.next();
			virtualMem[j] = (int) mem.getVirtualQuantity();
			j++;
		}
		return virtualMem;
	}
	
	public static int retrieveServerRamFreeFromDB(Server s){
		int ram_free = -1;
		String att = s.getAttributes();
		JSONObject obj;
		try {
			obj = new JSONObject(att);
			ram_free = obj.getInt("ram_free");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ram_free;
	}
	*/
	
	@SuppressWarnings("unchecked")
	public static Collection<Provider> retrieveProvidersFromDB(){
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		List<Provider> providerList = null;
		Query query = (Query) em.createNamedQuery("Provider.findAll");
		providerList = query.getResultList();

		return providerList;
			/*
			for (Provider provider : providerList) {
				String att = provider.getAttributes();
				JSONObject obj = new JSONObject (att);
				System.out.println(provider.getName() + " " + obj.get("country"));
			}
			
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		*/
	}
	
	private static void setUpTest(Client c) throws UnknownProviderTypeException, ProviderTypeNotYetSupportedException{
		Server s1 = new Server();
		s1.setAttributes("{\"cpu_speed\":\"2494.276\",\"cpu_cores\":\"4\",\"ram_total\":\"3915\",\"ram_free\":\"512\",\"cpu_load_one\":\"0.09\",\"cpu_load_five\":\"0.04\",\"ram_used\":\"1152\"}");
		List<Server> ls1 = new ArrayList<Server>();
		ls1.add(s1);
		

		Server s2 = new Server();
		s2.setAttributes("{\"cpu_speed\":\"2494.276\",\"cpu_cores\":\"4\",\"ram_total\":\"3915\",\"ram_free\":\"1024\",\"cpu_load_one\":\"0.09\",\"cpu_load_five\":\"0.04\",\"ram_used\":\"1152\"}");
		List<Server> ls2 = new ArrayList<Server>();
		ls2.add(s2);
		
		Provider provider1 = new Provider();
		provider1.setName("Provider1");
		provider1.setProviderId(22);
		provider1.setProviderUri("http://10.0.0.1:4242");
		provider1.setTypeId(0);
		provider1.setUserList(new ArrayList<User>());
		provider1.setServerList(ls1);
		
		Provider provider2 = new Provider();
		provider2.setName("Provider2");
		provider2.setProviderId(1);
		provider2.setProviderUri("http://sangiovese:4242");
		provider2.setTypeId(0);
		provider2.setUserList(new ArrayList<User>());
		provider2.setServerList(ls2);
		
		c.demoProviderRegistration(provider1);
		c.demoProviderRegistration(provider2);
	}
	
	public static void main(String[] args) throws Exception {
		Client c = new Client();
		setUpTest(c);
		
		String appPath = System.getProperty("user.dir");
		String ovf = Client.loadOvf(appPath + "/src/main/resources/ovf-2vms-vep2.0.xml");
		
		ParserManager pm = new ParserManager(ovf, false);
		Application app = new Application();
		app.setApplicationId(1);
		app.setName(pm.getApplication().getName());
		app.setApplicationOvf(ovf);
		List<Application> appList = new ArrayList<Application>();
		appList.add(app);
		
		// here appliances are not yet constructed
		/*
		Collection<OVFProperty> ops = pm.getAllProperty(app.getName());
		for (OVFProperty op : ops){
			System.out.println(app.getName() + "- " + op.getKey());
		}
		*/
		
		ApplicationDescriptor appDesc = OVFParser.ParseOVF(ovf);
		ApplicationGraph<String, ContrailEdge> appGraph = GraphUtils.MakeGraph(-1, appDesc);
		// adapter.printApplicationGraph(appGraph);
		
		User u = Client.createDemoUser("barabba", "10125", appList);
		c.demoSubmitApplication(u, app);
		
		Collection<MappingPlan> cmpl = Mapping.computeMappingPlan(u, appGraph);
		
		System.out.println("We have " + cmpl.size() + " plans");
		for (MappingPlan mp : cmpl){
			Set<CloudProviderId> s_id = mp.getMapping().keySet();
			for (CloudProviderId i : s_id){
				Collection<ApplianceDescriptor> ap = mp.getMapping().get(i);
				for (ApplianceDescriptor a : ap)
					System.out.println(i + " " +  a.getID());
			}
		}
		
		
		/*
		Collection<ApplianceDescriptor> ap = appGraph.getAppliances();
		Iterator<ApplianceDescriptor> i = ap.iterator();
		int[] virtualMem = getVirtualMemoryRequired(i.next());
		System.out.println(Integer.toString(virtualMem[0]));
		
		*/
		
		
		/*
		//printVirtualHardware(appGraph);
		System.out.println("Retrieving providers from db");
		Collection<Provider> providers = retrieveProvidersFromDB();
		for (Provider p : providers){
			System.out.println(p.getName() + " | " + p.getUuid() + " | " + p.getProviderUri());
			Collection<Server> cs = p.getServerList();
			for (Server s : cs)
				System.out.println(s.getAttributes());
		}
		*/
	}
	
}
