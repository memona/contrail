package org.ow2.contrail.federation.federationcore.exception;

public class ExistingFederationPropertiesException extends Exception
{

	public ExistingFederationPropertiesException()
	{
		// TODO Auto-generated constructor stub
	}

	public ExistingFederationPropertiesException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ExistingFederationPropertiesException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ExistingFederationPropertiesException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
