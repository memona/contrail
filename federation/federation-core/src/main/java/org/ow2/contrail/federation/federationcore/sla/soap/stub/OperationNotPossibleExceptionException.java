
/**
 * OperationNotPossibleExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

package org.ow2.contrail.federation.federationcore.sla.soap.stub;

public class OperationNotPossibleExceptionException extends java.lang.Exception{
    
    private org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.OperationNotPossibleExceptionE faultMessage;

    
        public OperationNotPossibleExceptionException() {
            super("OperationNotPossibleExceptionException");
        }

        public OperationNotPossibleExceptionException(java.lang.String s) {
           super(s);
        }

        public OperationNotPossibleExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public OperationNotPossibleExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.OperationNotPossibleExceptionE msg){
       faultMessage = msg;
    }
    
    public org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.OperationNotPossibleExceptionE getFaultMessage(){
       return faultMessage;
    }
}
    