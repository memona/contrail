package org.ow2.contrail.federation.federationcore.monitoring;

import java.util.List;

import org.ow2.contrail.federation.federationcore.monitoring.watcher.ProviderWatcherManager;

public abstract class BaseMonitoring 
{
	protected List<BaseMonitoringThread> pool;
	
	public BaseMonitoring(ProviderWatcherManager manager)
	{
		// TODO: add this instance inside the manager
		// smt like: manager.add(this);
	}
	
	public abstract void start();
	
	public void stop()
	{
		for (BaseMonitoringThread thread : pool)
			thread.stopExecution();
	}
	
}
