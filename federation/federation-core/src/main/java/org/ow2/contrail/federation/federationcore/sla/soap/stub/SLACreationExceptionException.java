
/**
 * SLACreationExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

package org.ow2.contrail.federation.federationcore.sla.soap.stub;

public class SLACreationExceptionException extends java.lang.Exception{
    
    private org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.SLACreationExceptionE faultMessage;

    
        public SLACreationExceptionException() {
            super("SLACreationExceptionException");
        }

        public SLACreationExceptionException(java.lang.String s) {
           super(s);
        }

        public SLACreationExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public SLACreationExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.SLACreationExceptionE msg){
       faultMessage = msg;
    }
    
    public org.ow2.contrail.federation.federationcore.sla.soap.stub.FederationNegotiationStub.SLACreationExceptionE getFaultMessage(){
       return faultMessage;
    }
}
    