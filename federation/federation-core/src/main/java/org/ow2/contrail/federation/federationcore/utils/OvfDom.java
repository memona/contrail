package org.ow2.contrail.federation.federationcore.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class OvfDom {
	Document ovfD = null;

	public OvfDom(String ovf){
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance(); 
		//domFactory.setIgnoringComments(true);
		DocumentBuilder builder = null;
		try {
			builder = domFactory.newDocumentBuilder();
			InputStream is = new ByteArrayInputStream(ovf.getBytes("UTF-8"));
			ovfD = (Document) builder.parse(is);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	private Element createPropertyNode(Document doc, String key, String value){
		Element p = doc.createElement("Property"); 
		p.setAttribute("ovf:key", key);
		p.setAttribute("ovf:type", "string");
		p.setAttribute("ovf:value", value);
		return p;
	}

	public void addVINProperty(String key, String value){
		NodeList nodes = ovfD.getElementsByTagName("ProductSection");
		
		for (int i=0; i < nodes.getLength(); i++){
			Node parent = nodes.item(i).getParentNode();
			if (parent.getNodeName().equals("VirtualSystem")){
				Element p = createPropertyNode(ovfD, key, value);
				nodes.item(i).appendChild(p);
			}
		}	
	}

	public String toString(){
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = factory.newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		StringWriter writer = new StringWriter();

		Result result = new StreamResult(writer);
		Source source = new DOMSource(ovfD);
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return writer.toString();
	}
}
