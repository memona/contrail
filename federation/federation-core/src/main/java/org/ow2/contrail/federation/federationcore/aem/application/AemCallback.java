package org.ow2.contrail.federation.federationcore.aem.application;


import javax.persistence.EntityManager;
import javax.persistence.Query;


import org.apache.log4j.Logger;
import org.apache.tuscany.sca.host.embedded.SCADomain;
import org.ow2.contrail.authorization.cnr.pep.PEP;
import org.ow2.contrail.authorization.cnr.pep.PepCallback;
import org.ow2.contrail.federation.federationcore.FederationCore;
import org.ow2.contrail.federation.federationcore.authz.AuthzHub;
import org.ow2.contrail.federation.federationcore.usermgmt.IUserManagement;
import org.ow2.contrail.federation.federationcore.usermgmt.UserManagement;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

public class AemCallback extends PepCallback {
	private static Logger logger = Logger.getLogger(AuthzHub.class);
	private static String loggerTag = "[CORE.AEM.CALLBACK] ";

	String appFedId;
	String userId;
	boolean handled = false;
	
	public AemCallback(PEP pep, String appId, String uuid) {
		super(pep);
		logger.info(loggerTag + "Creating callback ");
		appFedId = appId;
		userId = uuid;
	}
	
	public String getAppFedId() {
		return appFedId;
	}

	public AemCallback(PEP fed_pep) {
		super(fed_pep);
	}

	private User getUserFromUuid(String uuid){
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		User u = null;
		Query query = (Query) em.createNamedQuery("User.findByUuid").setParameter("uuid", uuid);
		u = (User) query.getSingleResult();
		return u;
	}


	private Application getApplicationFromUuid(String id){
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();
		Application a = null;
		Query query = (Query) em.createNamedQuery("Application.findByUuid").setParameter("uuid", id);
		a = (Application) query.getSingleResult();
		return a;
	}

	public void onRevokeAccess() {
		if (handled){
			logger.error(loggerTag + "Revoke already handled !!!");
		}
		else {
			// logger.error(loggerTag + "Authorization revoked for the user!!!");
			// User u = getUserFromUuid(userId);
			logger.error(loggerTag + "Application is going to be stopped for user ");
		}
	}

	public void setHandled() {
		handled = true;
	}

}
