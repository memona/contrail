package org.ow2.contrail.federation.federationcore.adapter.vin;

import javax.persistence.*;


@Entity
@NamedQueries({
    @NamedQuery(name = "VinVm.findAll", query = "SELECT t FROM VinVm t"),
    @NamedQuery(name = "VinVm.findById", query = "SELECT t FROM VinVm t WHERE t.vinVmId = :vinID"),
    @NamedQuery(name = "VinVm.findIdByAppliance", query = "SELECT t.vinVmId FROM VinVm t WHERE t.appId = :applicationID AND t.applianceId = :applianceID"),
    @NamedQuery(name = "VinVm.findIdByApplication", query = "SELECT t.vinVmId, t.applianceId FROM VinVm t WHERE t.appId = :applicationID")})
public class VinVm {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String appId;
	private String applianceId;
	private String vinVmId;
	private String vinAddress;
	
	public String getVinAddress() {
		return vinAddress;
	}

	public void setVinAddress(String vinAddress) {
		this.vinAddress = vinAddress;
	}

	public VinVm(String appID, String applID, String vinvmID){
		super();
		this.appId = appID;
		this.applianceId = applID;
		this.vinVmId = vinvmID;
		this.vinAddress = null;
	}
	
	public VinVm(){}
	
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getApplianceId() {
		return applianceId;
	}
	public void setApplianceId(String applianceId) {
		this.applianceId = applianceId;
	}
	public String getVinVmId() {
		return vinVmId;
	}
	public void setVinVmId(String vinVmId) {
		this.vinVmId = vinVmId;
	}
	public String toString(){
		String str = this.getAppId() + " | " + this.getApplianceId() + " | " + this.getVinVmId() ;
		if (this.getVinAddress() != null)
			str += " | " + this.getVinAddress();
		return str;
	}
}
