package org.ow2.contrail.federation.federationcore.adapter.gafs;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = "GafsVolume.findAll", query = "SELECT t FROM GafsVolume t"),
	@NamedQuery(name = "GafsVolume.findIdByAppAndVolName", query = "SELECT t.volumeId FROM GafsVolume t WHERE t.appId = :appId AND t.volumeName = :volumeName")
})
public class GafsVolume {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long volumeId;
	
	private String appId;
	private String volumeName;

	public Long getId() {
		return volumeId;
	}

	public void setId(Long id) {
		this.volumeId = id;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getVolumeName() {
		return volumeName;
	}

	public void setVolumeName(String volumeName) {
		this.volumeName = volumeName;
	}

	
}
