package org.ow2.contrail.federation.federationcore.exception;

public class ProviderComunicationException extends Exception
{

	public ProviderComunicationException()
	{
		// TODO Auto-generated constructor stub
	}

	public ProviderComunicationException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ProviderComunicationException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ProviderComunicationException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
