package org.ow2.contrail.federation.federationcore.adapter.vin;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * @author Gaetano F. Anastasi (ISTI-CNR)
 * 
 * VirtualNetworkRegistry allows for storing and accessing data needed for setting up a Virtual Network 
 * between all the VMs of appliances composing an application. 
 */
public class VirtualNetworkRegistry {
	private static final VirtualNetworkRegistry INSTANCE = new VirtualNetworkRegistry();
	
	private static final String PERSISTENCE_UNIT_NAME = "vinRegistry";
	private static EntityManagerFactory factory;
	  
	private VirtualNetworkRegistry(){
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
	}
	
	public static final VirtualNetworkRegistry getInstance() {
        return INSTANCE;
    }
	
	public void addVmId(String appName, String applId, String vmVinId){
		 EntityManager em = factory.createEntityManager();
		 em.getTransaction().begin();
		 VinVm vid = new VinVm();
		 vid.setAppId(appName);
		 vid.setApplianceId(applId);
		 vid.setVinVmId(vmVinId);
		 em.persist(vid);
		 em.getTransaction().commit();
		 em.close();
	}
	
	public void updateVmAddress(String vmVinId, String address){
		EntityManager em1 = factory.createEntityManager();
		VinVm vm = (VinVm) em1.createNamedQuery("VinVm.findById").setParameter("vinID", vmVinId).getSingleResult();
		em1.close();
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		vm.setVinAddress(address);
		em.merge(vm);
		em.getTransaction().commit();
		em.close();
	}
	
	public void addHostId(String appName, String vmHostId){
		 EntityManager em = factory.createEntityManager();
		 em.getTransaction().begin();
		 VinHost vid = new VinHost();
		 vid.setAppId(appName);
		 vid.setVinHostId(vmHostId);
		 em.persist(vid);
		 em.getTransaction().commit();
		 em.close();
	}
	
	
	@SuppressWarnings("unchecked")
	protected Set<String> getVmIds(){
		Set<String> ids = new HashSet<String>();
		EntityManager em = factory.createEntityManager();
		List<VinVm> todoList = em.createNamedQuery("VinVm.findAll").getResultList();
		em.close();
	    for (VinVm o: todoList){
	    	ids.add(o.getVinVmId());
	    }
	    return ids;
	}
	
	protected Set<String> getHostIdsByApp(String appId){
		Set<String> ids = new HashSet<String>();
		EntityManager em = factory.createEntityManager();
		@SuppressWarnings("unchecked")
		List<VinHost> todoList = em.createNamedQuery("VinHost.findIdByApplication").getResultList();
		em.close();
	    for (VinHost o: todoList){
	    	String id = o.getVinHostId();
	    	ids.add(id);
	    }
	    return ids;
	}
	
	@SuppressWarnings("unchecked")
	/*
	 * This method returns the VM ids assigned by the VIN controller to an application. For each
	 * VIN VM id, the corresponding appliance id is also returned. In principle, multiple VM ids 
	 * can refer to the same appliance id. Here appliance corresponds to the VirtualSystem term.
	 * 
	 * @param appName The name of the application 
	 * 
	 * @return A map object that maps VM Ids to Appliance Ids
	 */
	public Map<String,String> getVmIdsByApplication(String appName){
		Map<String,String> m = new HashMap<String,String>();
		
		EntityManager em = factory.createEntityManager();
		Query q = em.createNamedQuery("VinVm.findIdByApplication")
			        .setParameter("applicationID", appName);
		List<Object[]> results = q.getResultList();
		for (Object[] result : results) {
			m.put((String) result[0], (String) result[1]);
			//System.out.println("vmId: " + result[0] + ", applId: " + result[1]);
		}
		em.close();
		return m;
	}
	
	@SuppressWarnings("unchecked")
	Set<String> getVmIdsByAppliance(String appName, String applId){
		EntityManager em = factory.createEntityManager();
		Query q = em.createNamedQuery("VinVm.findIdByAppliance")
			        .setParameter("applicationID", appName)
					.setParameter("applianceID", applId);
		List<String> cs = (List<String>) q.getResultList();
		Set<String> vmSet = new HashSet<String>(cs);
		em.close();
		return vmSet;
	}
	
	public void printVmIds(){
		EntityManager em = factory.createEntityManager();
	    Query q = em.createNamedQuery("VinVm.findAll");
	    @SuppressWarnings("unchecked") List<VinVm> todoList = q.getResultList();
	    for (VinVm o: todoList){
	    	System.out.println("[Registry] " + o.toString());
	    }
	    em.close();
	}
	
	public void addNet(String appName, String netId, String vinNetId){
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		VinNet vid = new VinNet();
		vid.setAppId(appName);
		vid.setAppNetId(netId);
		vid.setVinNetId(vinNetId);
		em.persist(vid);
		em.getTransaction().commit();
		em.close();
	}
	
	String getNetIdByEdge(String appName, String appNet){
		EntityManager em = factory.createEntityManager();
		Query q = em.createNamedQuery("VinNet.findIdByEdge")
			        .setParameter("applicationID", appName)
					.setParameter("edgeID", appNet);
		String s = null;
		try {
			@SuppressWarnings("unchecked")
			List<String> ls = q.getResultList();
			if (ls.size() > 1) throw new Exception("Should there be only one netId");
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		try {
		 s = (String) q.getSingleResult(); 
		}
		catch(NoResultException e){
			e.getMessage();
			return null;
		}
		return s;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getNetIdsByApp(String appName){
		EntityManager em = factory.createEntityManager();
		Query q = em.createNamedQuery("VinNet.findIdByApp")
			        .setParameter("applicationID", appName);
		List<String> ls = null;
		try {
			ls = q.getResultList();
		}
		catch(NoResultException e){
			e.getMessage();
			return null;
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		return ls;
	}
	
	public void printNetIds(){
		EntityManager em = factory.createEntityManager();
	    Query q = em.createNamedQuery("VinNet.findAll");
	    @SuppressWarnings("unchecked")
	    List<VinNet> todoList = q.getResultList();
	    for (VinNet o: todoList){
	    	System.out.println("[Registry] " + o.toString());
	    }
	    em.close();
	}
	
	public List<VinNet> getNetIds(){
		EntityManager em = factory.createEntityManager();
	    Query q = em.createNamedQuery("VinNet.findAll");
	    @SuppressWarnings("unchecked")
	    List<VinNet> todoList = q.getResultList();
	    em.close();
	    return todoList;
	}
	
	public void addNetworkConn(String cid, String vmVinId, String netVinId){
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		VinConn vcon = new VinConn();
		vcon.setVinConnId(cid);
		vcon.setVinNetId(netVinId);
		vcon.setVinVmId(vmVinId);
		em.persist(vcon);
		em.getTransaction().commit();
		em.close();
	}
	
	public String getConnId(String vinId, String netVinId){
		EntityManager em = factory.createEntityManager();
	    Query q = em.createNamedQuery("VinConn.findVinConnId")
	    		.setParameter("vinNetworkID", netVinId)
	    		.setParameter("vinNodeID", vinId);
	    String id = (String) q.getSingleResult();
	    em.close();
	    return id;
	}
	
	public void printConnections(){
		EntityManager em = factory.createEntityManager();
	    Query q = em.createNamedQuery("VinConn.findAll");
	    @SuppressWarnings("unchecked")
	    List<VinConn> todoList = q.getResultList();
	    for (VinConn c: todoList){
	    	System.out.println(c);
	    }
	    em.close();
	}
	
	public void addApplication(String user, String appName, String appDeployID){
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		VinApps vapp = new VinApps();
		vapp.setAppIdFedCore(appName);
		vapp.setAppIdProvider(Integer.parseInt(appDeployID));
		vapp.setUserId(user);
		em.persist(vapp);
		em.getTransaction().commit();
		em.close();
	}
	
	public void removeApplication(String user, String appName, String appDeployID){
		EntityManager em = factory.createEntityManager();
		Query q = em.createNamedQuery("VinApps.findDbIdByDeployId")
		        .setParameter("appDeployId", Integer.parseInt(appDeployID))
				.setParameter("userName", user);
		long id = (Long) q.getSingleResult();
		VinApps app = em.find(VinApps.class, id);
		em.getTransaction().begin();
		em.remove(app);
		em.getTransaction().commit();
		em.close();
	}
	
	public String getAppDeployIdByName(String appName, String user){
		EntityManager em = factory.createEntityManager();
		Query q = em.createNamedQuery("VinApps.findDeployIdByName")
			        .setParameter("appName", appName)
					.setParameter("userName", user);
		Integer s = (Integer) q.getSingleResult();
		if ( s != null) return s.toString();
		return null;
	}

	public String getAppNameByDeployId(String appDeployId, String user) {
		EntityManager em = factory.createEntityManager();
		Query q = em.createNamedQuery("VinApps.findNameByDeployId")
			        .setParameter("appDeployId", Integer.parseInt(appDeployId))
					.setParameter("userName", user);
		String appName = (String) q.getSingleResult();
		if (appName != null) return appName;
		return null;
	}

	public String getVmAddressByVmId(String vmId) {
		EntityManager em1 = factory.createEntityManager();
		VinVm vm = null;
		try {
			vm = (VinVm) em1.createNamedQuery("VinVm.findById").setParameter("vinID", vmId).getSingleResult();
		}
		catch (javax.persistence.NonUniqueResultException e){
			vm = (VinVm) em1.createNamedQuery("VinVm.findById").setParameter("vinID", vmId).getResultList().get(0);
		}
		em1.close();
		if (vm != null)
			return vm.getVinAddress();
		else
			return null;
	}
	
	
}
