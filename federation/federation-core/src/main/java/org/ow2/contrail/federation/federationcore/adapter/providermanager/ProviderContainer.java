package org.ow2.contrail.federation.federationcore.adapter.providermanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationcore.adapter.providermanager.BaseProviderManager.ProviderType;
import org.ow2.contrail.federation.federationcore.exception.ProviderTypeNotYetSupportedException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderTypeException;
import org.ow2.contrail.federation.federationcore.monitoring.watcher.ProviderWatcherManager;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;

/**
 * This class instantiates and stores concrete classes for provider management.
 * @author Emanuele Carlini, Gaetano Anastasi 
 *
 */

public class ProviderContainer
{
	private static Logger logger = Logger.getLogger(ProviderContainer.class);
	private static ArrayList<CloudProviderId> providerList = new ArrayList<CloudProviderId>();

	// the registry of singletons, one ProviderManager for each registered provider.
	private static final Map<CloudProviderId, ICloudProviderManager> instances = new HashMap<CloudProviderId, ICloudProviderManager>();

	public static ICloudProviderManager getInstance(CloudProviderId key) throws UnknownProviderErrorException {
		synchronized (instances) {
			ICloudProviderManager instance = instances.get(key);
			if (instance == null) {
				String error = "Provider " + key + "has not been registered through the registerProvider() method";
				logger.error(error);
				throw new UnknownProviderErrorException(error);
			}
			return instance;
		}
	}

	public static ArrayList<CloudProviderId> getProviderList(){
		return providerList;
	}

	public static void addProvider(Provider provider) throws UnknownProviderTypeException, ProviderTypeNotYetSupportedException{
		// logger.debug("Adding provider to Core handler ");
		
		CloudProviderId padd = new CloudProviderId(provider.getUuid());
		if (providerList.contains(padd)){
			return;
		}
		
		ICloudProviderManager iProvider = null;
		switch (provider.getTypeId())
		{
		case 0:{
			synchronized (instances) {
				CloudProviderId pnew = new CloudProviderId(provider.getUuid());
				iProvider = new ContrailProviderManager(ProviderWatcherManager.getInstance(), pnew, provider, ProviderType.CONTRAIL);
				instances.put(pnew, iProvider);
				providerList.add(pnew);
				iProvider.notifyFederationConnection();
				break;
			}
		}
		case 1:{
			throw new ProviderTypeNotYetSupportedException("The type " + provider.getTypeId() + " is not supported yet");
		}
		/**
		 * Add case TypeId=2 for setting secure (https) connection to a ContrailProvider
		 * @author Marco Distefano
		 */
		case 2:{
			/*
			synchronized (instances) {
				System.out.println("provider.getUuid() "+ provider.getUuid());

				CloudProviderId pnew = new CloudProviderId(provider.getUuid());

				System.out.println("pnew " + pnew);
				// FIXME: substitute it!
				// iProvider = new ContrailProviderManagerSecure(ProviderWatcherManager.getInstance(), pnew, provider, ProviderType.CONTRAIL);
				iProvider = new ContrailProviderManagerOld(ProviderWatcherManager.getInstance(), pnew, provider, ProviderType.CONTRAIL);

				instances.put(pnew, iProvider);
				providerList.add(pnew);
				iProvider.notifyFederationConnection();
				break;
			}
			 */
		}	
		case 42:{ // this type was in the federationDB - does it have any sense?
			synchronized (instances) {
				CloudProviderId pnew = new CloudProviderId(provider.getUuid());
				iProvider = new ContrailProviderManager(ProviderWatcherManager.getInstance(), pnew, provider, ProviderType.CONTRAIL);
				instances.put(pnew, iProvider);
				providerList.add(pnew);
				iProvider.notifyFederationConnection();
				break;
			}
		}

		default:
		{
			logger.error("[core.adapter.providermanager] (PC) provider \"" + provider.getName() + "\" not added");
			throw new UnknownProviderTypeException("The type " + provider.getTypeId() + " is unknown.");
		}
		}

		logger.info("[core.adapter.providermanager] Provider \"" + provider.getName() + "\" at " + provider.getProviderUri() + " is available");
	}
}
