package org.ow2.contrail.federation.federationcore.exception;

public class ProviderTypeNotYetSupportedException extends Exception
{

	public ProviderTypeNotYetSupportedException()
	{
		// TODO Auto-generated constructor stub
	}

	public ProviderTypeNotYetSupportedException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ProviderTypeNotYetSupportedException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ProviderTypeNotYetSupportedException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
