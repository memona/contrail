package org.ow2.contrail.federation.federationcore.adapter.vin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.federation.federationcore.adapter.gafs.GlobalFileSystemRegistry;
import org.ow2.contrail.federation.federationcore.adapter.gafs.GlobalAutonomousFileSystem;
import org.ow2.contrail.federation.federationcore.aem.mapping.*;
import org.ow2.contrail.resource.vin.common.*;
import org.ow2.contrail.resource.vin.controller.VinAPI;

/**
 * VirtualNetworkRegistrator
 * @author Gaetano F. Anastasi (ISTI-CNR)
 * 
 * VirtualNetworkRegistrator is in charge of analyzing an application (represented by @ApplicationGraph) and 
 * contacting the VIN for registering VMs and networks to be added to the virtual network.
 */
public class VirtualNetworkRegistrator {
	private static Logger logger = Logger.getLogger(VirtualNetworkRegistrator.class);
	private static String loggerTag = "[core.adapter.vin] (Registrator) ";
	
	ApplicationGraph<String, ContrailEdge> app = null;
	static VinAPI vinController;
	VirtualNetworkRegistry registry;
	String appName = null;
	
	private void registerApplication(ApplicationGraph<String, ContrailEdge> application, String appUUid) throws VinError{
		app = application;
		if (app != null) {
			appName = appUUid;
		}
		else {
			app = new ApplicationGraph<String, ContrailEdge>(null,"Fake");
			logger.error(loggerTag + "No application found");
		}
		vinController = VirtualInfrastructureNetwork.getController();
		/*
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		*/
		registry = VirtualNetworkRegistry.getInstance();
		this.registerVMs();
		
		try {
			this.registerNetworks();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		this.registerConnections();
		//registry.printConnections();
	}
	
	/**
     * Constructor for the VirtualNetworkRegistrator class. 
     *
     * @param  application  the application graph
     */
	public VirtualNetworkRegistrator(ApplicationGraph<String, ContrailEdge> application, String appUuid) throws VinError{
		registerApplication(application, appUuid);
	}
	
	/**
     * Constructor for the VirtualNetworkRegistrator class. 
     *
     * @param  application  the application graph
	 * @throws ConfigurationError 
     */
	public VirtualNetworkRegistrator(ApplicationGraph<String, ContrailEdge> application, String appUuid, String gafsVolume) throws VinError{
		registerApplication(application, appUuid);
		
		GlobalAutonomousFileSystem gafs_driver = GlobalAutonomousFileSystem.getInstance();
		List<String> servers = gafs_driver.getServers(gafsVolume);
		if (servers != null){
			// getting all the VIN ids of the GAFS servers
			List<String> vids = this.registerPhysicalMachines(servers.size());
			
			if (vids != null){	
				List<String> vnets = null;
				for (String vid : vids){	
					// adding each PhysicalMachine to all the VINs of the application
					vnets = registry.getNetIdsByApp(appName);
				    logger.info(loggerTag + "Adding Physical Machine with vid " + vid + "to " + vnets.size() + " network" );
					for (String vnet : vnets){
						this.addingVinNodeToNetwork(vid, vnet);
					}
					// logger.info(loggerTag + registry.getConnId(vid, vnets.get(0)));
				}
				
				List<String> joined = gafs_driver.startVinAgents(servers, vids, vinController.getIbisPoolName(), vinController.getIbisServerAddress());
				logger.info(loggerTag + "The following GAFS servers joined the VIN: " + joined);
				
				// getting address of the DIR
				if (vnets != null){
					String vNetId_gafsDIR = vnets.get(0); // strong assumption, think about it (specifications tells about a single VIN network)
					String vinId_gafsDIR = vids.get(0); // the gafs DIR server is always the first of this list (for construction)
					String connId_gafsDIR = registry.getConnId(vinId_gafsDIR, vNetId_gafsDIR );
					String ipAddr = vinController.getAddress(vNetId_gafsDIR, connId_gafsDIR);
					
					GlobalFileSystemRegistry gafsRegistry = GlobalFileSystemRegistry.getInstance();
					String appId = application.getName();
					gafsRegistry.addVolume(appId, gafsVolume);
					long volId = gafsRegistry.getVolumeId(appId, gafsVolume);
					logger.info("Id " + volId + " of volume " + gafsVolume);//FIXME
					gafsRegistry.add_DIR_Server(volId, ipAddr);
					logger.info("The vinAddress of the GAFS DIR is " + ipAddr);
					logger.info("The vinAddress of the GAFS DIR saved is " + gafsRegistry.get_DIR_Server(volId));
				}
				
			}
		}
		
	}
	
	
	private List<String> registerPhysicalMachines(int size) throws VinError {
		List<String> result = new ArrayList<String>();
		logger.info(loggerTag + "Registering " + size + " physical machines");
		for (int i=0; i < size; i++){
			String vid = vinController.registerPhysicalMachine(new Properties());
			result.add(vid);
			registry.addHostId(appName, vid);
		}
		return result;
	}

	static public String getIBisPoolName(){
		if (vinController != null){
			return vinController.getIbisPoolName();
		}
		else 
			return "";
	}
	
	static public String getIbisServerAddress(){
		String serverAddress = "";
		if (vinController != null)
			serverAddress =  vinController.getIbisServerAddress();
		return serverAddress;
	}
		
	static public List<String> getVinConnParams(){
		List<String> l = new ArrayList<String>();
		l.add(vinController.getIbisPoolName());
		l.add(vinController.getIbisServerAddress());
		return l;
	}
	
	private void registerVMs() throws VinError{
		try{
			Collection<ApplianceDescriptor> adc = app.getAppliances();
			for (ApplianceDescriptor appl : adc){
				for (int i=0; i<appl.getDefaultVmNumber(); i++){
					String vid = vinController.registerVirtualMachine(new Properties());
					registry.addVmId(appName, appl.getID(), vid);
					logger.debug(loggerTag + "registering vm " + vid + " in application " + appName);
				}
			}
			// registry.printVmIds();
		}
		catch (NullPointerException e){
			logger.error("[VinRegistrator] Error, No application found!");
		}
	}
	
	private Set<String> networkSet(){
		Set<String> networks = new HashSet<String>();
		try {
			Collection<ApplianceDescriptor> adc = app.getAppliances();
			for (ApplianceDescriptor appl : adc){
				Set<ContrailEdge> egs = app.edgesOf(appl.getID());
				for (ContrailEdge e : egs){
					networks.add(e.getEdgeName());
				}
			}
		}
		catch (NullPointerException e){
			logger.error("[VinRegistrator] Error, No application found!");
		}
		return networks;
	}
	
	private void registerNetworks() throws VinError{
		logger.debug(loggerTag + " registerNetworks enter..");
		Properties netPro = new Properties();
		netPro.setProperty("maximumSize", "5");
		netPro.setProperty("verbose", "true");
		netPro.setProperty("debug", "true");
		Set<String> networks = networkSet();
		for (String appN : networks){
			String nid = null;
			try {
				nid = vinController.addNetwork("gre", netPro);
			} catch (ConfigurationError e) {
				logger.error(loggerTag + e.getMessage());
				continue;
			} catch (OutOfEntriesError e) {
				logger.error(loggerTag + e.getMessage());
				continue;
			}
			registry.addNet(appName, appN, nid);
			// if (i==1) break; //just to test it, please remove it
		}
		logger.debug(loggerTag + " registerNetworks exit..");
		// registry.printNetIds();
	}
	
	private void registerConnections() throws VinError{
		logger.debug(loggerTag + " registerConnections enter..");
		Collection<ApplianceDescriptor> adc = app.getAppliances();
		if (adc == null || adc.size()==0){
			logger.error("[VinRegistrator] Error, No application found!");
			return;
		}
		for (ApplianceDescriptor appl : adc){

			// retrieving VIN VM IDs of the appliance
			Set<String> vmIds = registry.getVmIdsByAppliance(appName, appl.getID());
			String[] vmIdsArray = vmIds.toArray(new String[vmIds.size()]); // we assume one VM for appliance
			
			// retrieving VIN Network IDs of the appliance
			Set<ContrailEdge> egs = app.edgesOf(appl.getID());
			Set<String> edge_names = new HashSet<String>();
			if(egs.isEmpty()) logger.error(loggerTag + " egs is empty"); 
			// Edges are point-to-point connections, instead we only need the network
			for (ContrailEdge e : egs)
				edge_names.add(e.getEdgeName());
			
			for (String n : edge_names){
				String netId = registry.getNetIdByEdge(appName, n);
				if (netId == null){
					logger.warn(loggerTag + "No VIN network ID registered for edge named " + n);
					continue;
				}
				// adding connection for each network edge
				logger.info(loggerTag + "Adding VM "+ vmIdsArray[0] + " to VNet " + netId);
				addingVinNodeToNetwork(vmIdsArray[0], netId);
			}	
		}
		logger.debug(loggerTag + " registerConnections exit..");
	}
	
	/**
	 * This method calls the addConnection from the VIn Controller and 
	 * store information of the connection on the internal database
	 * @param vinNodeId
	 * @param vinNetId
	 * @throws VinError
	 */
	private void addingVinNodeToNetwork(String vinNodeId, String vinNetId) throws VinError{
		try {
			String cid = vinController.addConnection(vinNetId, vinNodeId, new Properties());
			logger.info(loggerTag + "Adding " + "connection " + cid + " from vId " + vinNodeId + " to vNet " + vinNetId);
			registry.addNetworkConn(cid, vinNodeId, vinNetId);
			String netAddress = vinController.getAddress(vinNetId, cid);
			
			logger.debug(loggerTag + "Vm " + vinNodeId + " has address " + netAddress);
			registry.updateVmAddress(vinNodeId, netAddress);
			
		} catch (ConfigurationError e1) {
			logger.error(e1.getMessage());
			e1.printStackTrace();
		}
	}
}
