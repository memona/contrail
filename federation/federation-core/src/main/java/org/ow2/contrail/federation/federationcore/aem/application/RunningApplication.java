package org.ow2.contrail.federation.federationcore.aem.application;

import java.util.*;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.federation.federationcore.adapter.providermanager.CloudProviderId;
import org.ow2.contrail.federation.federationcore.aem.mapping.MappingPlan;

public class RunningApplication
{
	private MappingPlan mappingPlan;
	private Collection<ApplianceStartupDescriptor> startupDesc;
	private String name;
	private String id;
	private String deployId;
	
	public RunningApplication(MappingPlan mappingPlan, Collection<ApplianceStartupDescriptor> startupDesc, String name, String fedId, String deployID)
	{
		this.setMappingPlan(mappingPlan);
		this.startupDesc = startupDesc;
		this.name = name;
		this.id = fedId;
		this.deployId = deployID;
	}

	public MappingPlan getMappingPlan() { return mappingPlan; }

	public void setMappingPlan(MappingPlan plan) { mappingPlan = plan; }

	public String getId() { return id; }

	public String getName() { return name; }

	public String getDeployId() { return deployId; }
}
