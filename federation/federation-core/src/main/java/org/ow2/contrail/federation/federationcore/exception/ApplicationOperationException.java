package org.ow2.contrail.federation.federationcore.exception;

public class ApplicationOperationException extends Exception
{

	public ApplicationOperationException()
	{
		// TODO Auto-generated constructor stub
	}

	public ApplicationOperationException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ApplicationOperationException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ApplicationOperationException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
