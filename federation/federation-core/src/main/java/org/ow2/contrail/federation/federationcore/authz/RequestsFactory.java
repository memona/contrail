package org.ow2.contrail.federation.federationcore.authz;

import java.util.ArrayList;
import java.util.List;

import org.ow2.contrail.authorization.cnr.utils.UconConstants;
import org.ow2.contrail.authorization.cnr.utils.UconConstants.Category;
import org.ow2.contrail.authorization.cnr.utils.pep.PepRequestAttribute;

public class RequestsFactory 
{
	final static String RESOURCE_ATTR_ID = "urn:oasis:names:tc:xacml:1.0:resource:resource-id";
	final static String ACTION_ATTR_ID = "urn:contrail:fed:action:id";
	final static String action_value = "submit";
	final static String type = UconConstants.XML_STRING;
	final static String issuer = "CNR-Federation";
	
	public static List<PepRequestAttribute> getFakeRequest()
	{
		final String resource_value = "VMTemplate";
		// example values of attributes
		String SUBJECT_ATTR_ID = "urn:contrail:names:federation:subject:name";
		String subject_value = "fed-user";
		
		// prepare a list of attribute for the request
		List<PepRequestAttribute> req = new ArrayList<PepRequestAttribute>();
		PepRequestAttribute sub = new PepRequestAttribute(SUBJECT_ATTR_ID, type, subject_value, issuer, Category.SUBJECT);
		req.add(sub);
		PepRequestAttribute res = new PepRequestAttribute(RESOURCE_ATTR_ID, type, resource_value, issuer, Category.RESOURCE);
		req.add(res);
		PepRequestAttribute act = new PepRequestAttribute(ACTION_ATTR_ID, type, action_value, issuer, Category.ACTION);
		req.add(act);	
		return req;
	}
}
