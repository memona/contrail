package org.ow2.contrail.federation.federationcore.aem.runtime;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.osoa.sca.annotations.Init;
import org.ow2.contrail.federation.federationcore.monitoring.message.BaseMessage;

public class RuntimeManagement
{
	static ConcurrentLinkedQueue<BaseMessage> interruptQueue;
	private InterruptQueueHandler interruptHandler;
	
	@Init
	public void init()
	{
		// FIXME: add some stuff to thread
		interruptQueue = new ConcurrentLinkedQueue<BaseMessage>();
		new Thread(new InterruptQueueHandler()).start();
	}
	
	// FIXME: if thread is sleeping it need to wake up
	public static void addInterrupt(BaseMessage message)
	{
		interruptQueue.add(message);
	}
}
