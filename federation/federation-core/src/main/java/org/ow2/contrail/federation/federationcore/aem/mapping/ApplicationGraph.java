package org.ow2.contrail.federation.federationcore.aem.mapping;

import java.util.ArrayList;
import java.util.Collection;

import org.jgrapht.graph.Multigraph;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;

@SuppressWarnings("serial")
public class ApplicationGraph<V, E> extends Multigraph<V, E>
{
	public String name;

	public ApplicationGraph(Class<? extends E> arg0, String name)
	{
		super(arg0);
		this.name = name;
	}

	private Collection<ApplianceDescriptor> appliances;

	public Collection<ApplianceDescriptor> getAppliances()
	{
		return this.appliances;
	}

	public void setAppliances(Collection<ApplianceDescriptor> appliances)
	{
		this.appliances = appliances;
	}

	public void addAppliance(ApplianceDescriptor appliance)
	{
		if (this.appliances == null)
		{
			this.appliances = new ArrayList<ApplianceDescriptor>();
		}
		this.appliances.add(appliance);
	}

	public String getName()
	{
		return this.name;
	}

}
