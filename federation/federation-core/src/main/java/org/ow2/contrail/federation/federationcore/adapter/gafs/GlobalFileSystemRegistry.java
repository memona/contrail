package org.ow2.contrail.federation.federationcore.adapter.gafs;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.ow2.contrail.federation.federationcore.adapter.vin.VinVm;

/**
 * @author Gaetano F. Anastasi (ISTI-CNR)
 * 
 * GafsRegistry allows for storing and accessing data needed for setting up a GAFS volume shared between 
 * some appliances of an application. 
 */
public class GlobalFileSystemRegistry {

	
	private static final GlobalFileSystemRegistry INSTANCE = new GlobalFileSystemRegistry();
		
		private static final String PERSISTENCE_UNIT_NAME = "gafsRegistry";
		private static EntityManagerFactory factory;
		  
		private GlobalFileSystemRegistry(){
			factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}
		
		public static final GlobalFileSystemRegistry getInstance() {
	        return INSTANCE;
	    }

		public void addVolume(String appId, String sharedDiskName) {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
		    GafsVolume volume = new GafsVolume();
		    volume.setAppId(appId);
		    volume.setVolumeName(sharedDiskName);
			em.persist(volume);
			em.getTransaction().commit();
			em.close();
		}

		public void add_DIR_Server(long volId, String ipAddr) {
			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			GafsDIR dir_server = new GafsDIR();
			dir_server.setAddressDIR(ipAddr);
			dir_server.setVolumeId(volId);
			em.persist(dir_server);
			em.getTransaction().commit();
			em.close();
		}

		public String get_DIR_Server(long volId){
			EntityManager em = factory.createEntityManager();
			Query q = em.createNamedQuery("GafsDIR.findDirById")
					.setParameter("volumeID", volId);
			String ip = (String) q.getSingleResult();
			em.close();
		    return ip;
		}
		
		public long getVolumeId(String appId, String diskName) {
			EntityManager em = factory.createEntityManager();
			Query q = em.createNamedQuery("GafsVolume.findIdByAppAndVolName")
			        .setParameter("appId", appId)
			        .setParameter("volumeName", diskName);
			long id = q.getFirstResult();
			em.close();
		    return id;
		}

}
