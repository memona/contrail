package org.ow2.contrail.federation.federationcore.adapter.providermanager;

import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERGROUPS;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERNAME;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERROLE;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERVID;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.XUSERNAME;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.APP_ID;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.VM_IDS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.federation.federationcore.adapter.vin.VirtualNetworkRegistry;
import org.ow2.contrail.federation.federationcore.authz.AuthzHub;
import org.ow2.contrail.federation.federationcore.exception.ProviderComunicationException;
import org.ow2.contrail.federation.federationcore.exception.UnknownProviderErrorException;
import org.ow2.contrail.federation.federationcore.monitoring.rest.ApplicationRestMonitoring;
import org.ow2.contrail.federation.federationcore.monitoring.rest.ContrailRestMonitoring;
import org.ow2.contrail.federation.federationcore.monitoring.watcher.ProviderWatcherManager;
import org.ow2.contrail.federation.federationcore.utils.FederationProperties;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.UGroup;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.multipart.MultiPart;

public class ContrailProviderLegacy extends BaseProviderManager
{
	private static Logger logger = Logger.getLogger(ContrailProviderLegacy.class);

	ContrailProviderLegacy(ProviderWatcherManager manager, CloudProviderId cpi, Provider provider, ProviderType type)
	{
		super(manager, cpi, provider, type);

		logger.info("Contrail Cloud Provider: creating provider \"" + provider.getName() + "\" with id \"" + providerID
				+ "\" with address \"" + provider.getProviderUri() + "\"");

		logger.info("Contrail Cloud Provider: provider created");
	}

	/**
	 * Deploys the application (a set of appliances) on the provider.
	 * 
	 * @throws UnknownProviderErrorException
	 */
	@Override
	public String submit(Collection<ApplianceDescriptor> appDesc, User user, String ovf, String ovfName, String slaName)
			throws ProviderComunicationException, UnknownProviderErrorException
	{
		String appId = "-1";
		

		// creates the client for the communication
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(providerAddress);
		logger.info("Set up the clients for the communication with provisioning manager");

		try {
			MultiPart mp = new MultiPart();
			// String slaID = "SLASOI.xml";

			// FIXME: CEROTTO GROSSO COME UNA CASA!!!!!!!
			// si passa direttamente la stringa dell'ovf perche non abbiamo ancora un renderer funzionante completo
			// senza RENDERER non si pùo fare niente

			// mp.bodyPart(ovfName, MediaType.TEXT_PLAIN_TYPE).bodyPart(slaName, MediaType.TEXT_PLAIN_TYPE).bodyPart(ovf, MediaType.TEXT_XML_TYPE)
			//		.bodyPart(user.getUsername(), MediaType.TEXT_PLAIN_TYPE).bodyPart(jsonVin2OVF, MediaType.APPLICATION_JSON_TYPE);
			
			//mp.bodyPart(ovfName, MediaType.TEXT_PLAIN_TYPE).bodyPart(ovf, MediaType.TEXT_XML_TYPE)
			//.bodyPart(user.getUsername(), MediaType.TEXT_PLAIN_TYPE).bodyPart(user.getUserId().toString(), MediaType.TEXT_PLAIN_TYPE);
			
			mp.bodyPart(ovfName, MediaType.TEXT_PLAIN_TYPE)
            .bodyPart(ovf, MediaType.TEXT_XML_TYPE)
            .bodyPart(user.getUsername(), MediaType.TEXT_PLAIN_TYPE)
            .bodyPart(Integer.toString(user.getUserId()), MediaType.TEXT_PLAIN_TYPE);

			ClientResponse answer = service.path("application-mgmt/submit").type("multipart/mixed").put(ClientResponse.class, mp);

			// boolean result = this.parseReturnStatus(answer);		move in the exception	
			
			JSONObject obj = answer.getEntity(JSONObject.class);
			appId = obj.getString(APP_ID);
			logger.info("Contrail Provider Manager: appliances successfully submitted with APP-ID \"" + appId + "\"");
			
		}
		catch (JSONException e)
		{
			logger.error("Contrail Provider Manager: json object is incorrect while submitting a new appliance");
			throw new ProviderComunicationException(e.toString());
		}
		catch (UniformInterfaceException e)
		{
			String error = "Maybe the VEP has noot been started for provider " + this.getProviderName() + "\n";
			logger.error("[ContrailProviderManager] " + error);
			throw new ProviderComunicationException(e.toString());
		}
		catch (ClientHandlerException e1)
		{
			String error = "Unable to contact the provisioning manager for provider " + this.getProviderName() + "\n";
			logger.error("[ContrailProviderManager] "+ error);
			throw new ProviderComunicationException(error + e1.toString());
		}
		catch (Exception e1)
		{
			logger.error("Contrail Provider Manager: user \"" + user.getUsername() + "\" error on submit APP-ID "+ appId);
			
		}
		
		return appId;
	}

	/**
	 * Starts an application (a set of appliances) on the provider.
	 * 
	 * @throws UnknownProviderErrorException
	 */
	@Override
	public Collection<String> internalStart(Collection<ApplianceDescriptor> appDesc, User user, String appId) throws ProviderComunicationException,
			UnknownProviderErrorException
	{
		System.out.println("+++++++++++++++++++++Internal start nel provider++++++++++++++++");
		Collection<String> vmId = null;

		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(providerAddress);

		JSONObject jobject = createJSONRequest(user, appDesc, "start");

		//Second hook for the integration with VIN 
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		String appName = registry.getAppNameByDeployId(appId, user.getUsername());
		JSONObject jsonVinIds = null;
		jsonVinIds = getVinIds(appName);
		try {
			jobject.put("vin_ids", jsonVinIds);
		} catch (Exception e2) {
			logger.info("[Core] ContrailProviderManager: impossible to send VIN Ids to Provisioning Manager -" + e2.getMessage());
		}

		try {
			ClientResponse answer = service.path("application-mgmt/start/" + appId).type("application/json").post(ClientResponse.class, jobject);
			
			// this method is buggy, please check it before uncommenting
			// boolean result = this.parseReturnStatus(answer);
			boolean result = true;
			
			if (result == true)
			{
				JSONObject obj = answer.getEntity(JSONObject.class);
				JSONArray array = obj.getJSONArray(VM_IDS);
				
				logger.info("Contrail Provider Manager: appliances successfully started with APP-ID " + appId + " on:\n");
				
				//TODO: to comment what this piece of code does
				vmId = new ArrayList<String>();
				for (int i = 0; i < array.length(); i++)
				{
					logger.info("\tvm - " + array.getString(i) + "\n");
					vmId.add(array.getString(i));
				}
			}
			else
			{
				logger.error("Contrail Provider Manager: user \"" + user.getUsername() + "\" error in perform application start");
			}
		}
		catch (JSONException e)
		{
			logger.error("Contrail Provider Manager: json object is incorrect while submitting a new appliance");
			throw new ProviderComunicationException(e.toString());
		}
		catch (ClientHandlerException e1)
		{
			logger.error("Contrail Provider Manager: Unable to contact the provisioning manager");
			throw new ProviderComunicationException(e1.toString());
		}
		
		return vmId;
	}

	@Override
	public boolean propagateUser(User user) throws ProviderComunicationException, UnknownProviderErrorException
	{
		boolean result = false;
		
		// creates the client for the communication
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(providerAddress);
		logger.debug("User propagation: Creating a service for REST invocation");
		
		// creating the JSON object
		JSONObject jobject = new JSONObject();

		// fill the JSON with the proper fields
		try
		{
			jobject.put(USERNAME, user.getUsername() + "");

			// user uuid
			String uuid = user.getUuid();
			if (uuid == null)
			{
				logger.warn("Invalid or null users uuid, using: -1");
				uuid = "-1";
			}
			jobject.put(USERVID, uuid);

			user.getURoleList();
			
			// user's role
			jobject.put(USERROLE, "user");

			// Xusernam
			jobject.put(XUSERNAME, FederationProperties.getInstance().getProperty("xuser.id"));

			JSONArray jsonArray = new JSONArray();
			if (user.getUGroupList() != null)
			{
				for (UGroup uGroup : user.getUGroupList()) 
				{
					jsonArray.put(uGroup.getName());
				}
			}
			
			jobject.put(USERGROUPS, jsonArray);

			logger.debug("User propagation: sending REST invocation.");
			
			ClientResponse answer = service.path("/user-mgmt/propagate").type("application/json").put(ClientResponse.class, jobject.toString());
			// JSONObject answer = service.path("/user-mgmt/propagate").type("application/json").put(JSONObject.class, jobject.toString());
			
			result = true;
			
			if (result == true)
			{
				logger.info("Contrail Provider Manager: user " + user.getUsername() + " successfully propagated");
			}
			else
			{
				logger.error("Contrail Provider Manager: user " + user.getUsername()+ " has not been propagated");
			}
			
		}
		catch (ClientHandlerException e1)
		{
			logger.error("Contrail Provider Manager: Unable to contact the provisioning manager: " + e1.getMessage());
			throw new ProviderComunicationException(e1);
		}
		catch (JSONException e)
		{
			logger.error("Contrail Provider Manager: json object is incorrect while propagating a new user");
			throw new ProviderComunicationException(e);
		}
		return result;
	}

	/**
	 * Stops an application (a set of appliances) on the provider.
	 * 
	 * @throws UnknownProviderErrorException
	 */
	@Override
	public boolean internalStop(Collection<ApplianceDescriptor> appDesc, User user, String appId) throws ProviderComunicationException,
			UnknownProviderErrorException
	{
		// recovering the list of attributes
		// String appId = "-1";
		boolean result = false;
		
		// creates the client for the communication
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(providerAddress);

		// creating the JSON object.
		JSONObject jobject = createJSONRequest(user, appDesc, "stop");
		
		ClientResponse answer = service.path("application-mgmt/stop/" + appId).type("application/json").post(ClientResponse.class, jobject);
		
//		result = this.parseReturnStatus(answer);
		
		result = true;

		
		if (result == true)
		{
			logger.info("Contrail Provider Manager: appliances successfully stopped with APP-ID " + appId);
		}
		else
		{
			logger.error("Contrail Provider Manager: user \"" + user.getUsername() + "\" not authorized to perform this action");
		}
			
			/*
			String jsonResult = answer.getString(RETURN_STATUS);
			switch (Integer.parseInt(jsonResult))
			{
			case 0:
			{
				
			}
			case 200:
			{
				
				result = true;
				break;
			}
			case 401:
			{
				logger.error("Contrail Provider Manager: user \"" + user.getUsername() + "\" not authorized to perform this action");
			}
			default:
			{
				throw new UnknownProviderErrorException("Unknown numerical provider error");
			}
			}
			*/

		// releasing authz
		// ad.stopAccess(appId);
		return result;
	}

	private static JSONObject getVinIds(String appName){
		VirtualNetworkRegistry registry = VirtualNetworkRegistry.getInstance();
		Map<String,String> mapVin2OVF = registry.getVmIdsByApplication(appName); //in case there is no ids, it seems the map is empty but it is created
		JSONObject jsonVin2OVF = new JSONObject(mapVin2OVF);
		return jsonVin2OVF;
	}
	
	/*
	private static String getAppNameById(String appId){
		return null;
	}
	*/
	
	private JSONObject createJSONRequest(User user, Collection<ApplianceDescriptor> appDesc, String operation) throws ProviderComunicationException
	{
		// creating the JSON object.
		JSONObject jobject = new JSONObject();

		// fill the JSON with the proper fields.
		try
		{
			jobject.put("name", user.getUsername() + "");

			// for each appliance add its the name in the request to provisioning manager.
			JSONArray jsonArray = new JSONArray();
			for (ApplianceDescriptor app : appDesc)
			{
				jsonArray.put(app.getID());
			}
			jobject.put("appliances", jsonArray);
		}
		catch (JSONException e)
		{
			logger.error("Contrail Provider Manager: incorrect json object.");
			throw new ProviderComunicationException(e.toString());
		}

		return jobject;
	}

	/**
	 * Starts application-level monitoring.
	 */
	@Override
	protected boolean startApplicationLevelMonitoring(Collection<String> result)
	{
		app_mon = new ApplicationRestMonitoring(this.providerWatcherManager, result, this);

		return true;
	}

	/**
	 * Stops application-level monitoring.
	 */
	protected boolean stopApplicationLevelMonitoring()
	{
		app_mon.stop();

		return true;
	}

	@Override
	public String notifyFederationConnection()
	{
		// TODO Auto-generated method stub
		return null;
	}


	/**
	 * This method check the return status from the provisioning manager.
	 * Current status supported are 
	 * 0: everything fine
	 * -1: some error
	 * @param answer
	 * @return
	 * @throws ProviderComunicationException 
	 */
	private boolean parseReturnStatus(ClientResponse answer) throws ProviderComunicationException
	{
		try
		{
			// TODO: add the management of the HTTP return codes. 
			
			String response = answer.getEntity(String.class);			
			JSONObject obj = new JSONObject(response);
			
			int pm_status = obj.getInt(ExternalizedReturnJSONstrings.RETURN_STATUS);
			
			System.out.println("--------------- " + pm_status);
			switch (pm_status)
			{
				case 0: // everything is fine
				{
					return true;
				}
				case -1:
				{
					return false;
				}
				default:
				{
					logger.error("Contrail Provider Manager: internal provider error, unknown provider response: "+answer);
					// throw new UnknownProviderErrorException("Unknown numerical provider error");
				}
			}
		}
		catch (JSONException e)
		{
			logger.error("Contrail Provider Manager: json object is incorrect while propagating a new user");
			throw new ProviderComunicationException(e);
		}
		
		return false;
		
	}

	@Override
	protected void createMonitoring(ProviderWatcherManager manager) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean internalKill(Collection<ApplianceDescriptor> appDesc,
			User user, String appId) throws ProviderComunicationException,
			UnknownProviderErrorException {
		// TODO Auto-generated method stub
		return false;
	}
}
