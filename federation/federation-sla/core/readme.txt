NOTES about this artifact
-------------------------

* this artifact contains classes that will be used 
by each component within federation-slam4osgi.

* Normally the 'core' module contains only interfaces
and data types.

* All contained classes will be exposed by the 
federation-slam4osgi bundle within OSGi. 

