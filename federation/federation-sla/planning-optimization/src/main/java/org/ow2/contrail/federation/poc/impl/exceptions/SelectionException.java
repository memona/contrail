package org.ow2.contrail.federation.poc.impl.exceptions;

public class SelectionException extends Exception
{
    private static final long serialVersionUID = 1L;

    public SelectionException(String message )
    {
        super( message );
        // TODO Auto-generated constructor stub
    }
}
