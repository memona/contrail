package org.ow2.contrail.federation.poc.impl.datastructure;

import org.ow2.contrail.federation.poc.impl.utils.Constant;

/**
 * <code>Harddisk</code> represents Harddisk in resource request.
 */
public class Harddisk extends Resource
{

    public Harddisk()
    {
        this.setResourceName( Constant.Harddisk );
    }
}
