package org.ow2.contrail.federation.poc.impl.manager;

import org.ow2.contrail.federation.poc.impl.exceptions.SubNegotiationException;
import org.ow2.contrail.federation.poc.impl.provider.manager.ProviderManager;
import org.ow2.contrail.federation.poc.impl.provider.selection.Criterion;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;


public class ProviderManagerMock implements ProviderManager {

	private static ProviderManagerMock restManager = null;


	private ProviderManagerMock(){
	}


	public static ProviderManagerMock getInstance(){
		if (restManager==null){
			restManager = new ProviderManagerMock();
		}
		return restManager;
	}


	@Override
	public String[] getProvidersList(SLATemplate slat)  {
		String[] endPoints = new String[3];
		endPoints[0] = "http://localhost:8080/services/contrailNegotiation?wsdl";
		endPoints[1] = "http://10.15.5.52:8080/services/contrailNegotiation?wsdl";
		return endPoints;
	}

	
	@Override
	public SLATemplate[] negotiate(String endpoint, SLATemplate slaTemplate) {
		/**  negotiation code here**/
		SLATemplate [] slat = null;
		return slat;
	}


	@Override
	public SLA createAgreement(SLATemplate slaTemplate)
			throws SubNegotiationException {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public Criterion[] getCriteria(SLATemplate slat) {
		// TODO Auto-generated method stub
		return null;
	}


		
}
