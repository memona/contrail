package org.ow2.contrail.federation.poc.impl.manager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import junit.framework.Assert;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.contrail.federation.poc.impl.provider.manager.ProviderManagerImpl;
import org.ow2.contrail.federation.poc.impl.provider.selection.Criterion;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.sla.SLATemplate;



public class ProviderManagerTest {

	private static SLATemplate proposalSlat;
	private static SLATemplate offerSlatIncomplete;
	private static ProviderManagerImpl pm;
	
	private final Comparator<Criterion> CRITERIA_ORDER = new Comparator<Criterion>() {
		public int compare(Criterion e1, Criterion e2) {
			return (e2.getName().compareTo(e1.getName()));
		}
	};
	
	
	@BeforeClass
	public static void setUp() throws Exception {
		
		pm = new ProviderManagerImpl();
		String proposalXml = FileUtils.readFileToString(
    			new File("src/test/resources/slats/SlatProposalFederation.xml"));
		String offerXmlInc = FileUtils.readFileToString(
    			new File("src/test/resources/slats/SlatOfferFederationIncomplete.xml"));
		
		SLASOITemplateParser tp = new SLASOITemplateParser();
		proposalSlat = tp.parseTemplate(proposalXml);
		offerSlatIncomplete = tp.parseTemplate(offerXmlInc);
	}

	
	
	//@Test
	public void getProvidersListTest() throws Exception {
		String[] providers = pm.getProvidersList(proposalSlat);
		List<String> providersList = Arrays.asList(providers);
		Collections.sort(providersList);
		
		List<String> expectedProvidersList = new ArrayList<String>();
		expectedProvidersList.add("http://10.15.8.2:8080/services/contrailNegotiation?wsdl");
		expectedProvidersList.add("http://10.15.8.23:8080/services/contrailNegotiation?wsdl");
		Collections.sort(expectedProvidersList);
		
		for (int i = 0; i < providersList.size(); i++) {
			Assert.assertEquals(expectedProvidersList.get(i), providersList.get(i));
		}
	}

	
	
	//@Test
	public void getCriteriaTest() throws Exception {
		Criterion[] criteria = pm.getCriteria(proposalSlat);
		List<Criterion> criteriaList = Arrays.asList(criteria);
		Collections.sort(criteriaList, CRITERIA_ORDER);
		
		List<Criterion> expectedCriteriaList = new ArrayList<Criterion>();
		expectedCriteriaList.add(new Criterion("vm_cores", 0.8));
		expectedCriteriaList.add(new Criterion("memory", 0.3));
		expectedCriteriaList.add(new Criterion("price", 0.9));
		Collections.sort(expectedCriteriaList, CRITERIA_ORDER);
		
		for (int i = 0; i < criteriaList.size(); i++) {
			Assert.assertEquals(expectedCriteriaList.get(i).getName(), 
					criteriaList.get(i).getName());
			Assert.assertEquals(expectedCriteriaList.get(i).getWeight(), 
					criteriaList.get(i).getWeight());
		}
	}


	
	//@Test
	public void getProviderUuid() throws Exception {
		String provUuid = pm.getProviderUuid(proposalSlat, "http://10.15.8.23:8080/services/contrailNegotiation?wsdl");
		Assert.assertEquals("37", provUuid);
	}

	
	
	//@Test
	public void setProvidersList() throws Exception {
		pm.setProvidersList(offerSlatIncomplete, "42", "http://10.15.8.2:8080/services/contrailNegotiation?wsdl");
		String providerUid = pm.getProviderUuid(offerSlatIncomplete, "http://10.15.8.2:8080/services/contrailNegotiation?wsdl");
		Assert.assertEquals("42", providerUid);
	}
	
	
	@AfterClass
	public static void tearDown() throws IOException {
	}

	
	

	
	
	
}
	
	
	
	

