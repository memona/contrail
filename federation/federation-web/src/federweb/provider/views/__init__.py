from django.http import Http404
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView

from federweb.base.views import RoleRequiredMixin, AppNodeMixin, RestMixin
from federweb.base.utils import cached_property

class AppMixin(RoleRequiredMixin):
    app_name = 'provider'
    user_role = 'CloudAdministrator'

class ProvidersMixin(AppMixin):
    @cached_property
    def providers_model(self):
        from federweb.base.models import federation
        return federation.providers
#        return self.request.user.providers
    
    def get_provider(self, id):
        if not id:
            raise AttributeError('Provider ID is not specified.')
        
        obj = self.providers_model.get(id)
        
        if not obj:
            raise Http404('Provider not found')
        
        return obj
    
    @cached_property
    def provider(self):
        return self.get_provider(self.kwargs['provider_id'])
    
    def get_context_data(self, **kwargs):
        ct = super(ProvidersMixin, self).get_context_data(**kwargs)
        ct['provider'] = self.provider
        return ct

class ProvidersList(AppMixin, AppNodeMixin, RestMixin, TemplateView):
    def get_template_names(self):
        return ['provider/providers.html']
    
    @cached_property
    def model(self):
        from federweb.base.models import federation
        return federation.providers
