from django.views.generic import TemplateView

from federweb.base.utils import cached_property
from federweb.base.views import AppNodeMixin, RestMixin, RestDetailView
from federweb.provider.views import ProvidersMixin

class ServersMixin(ProvidersMixin, AppNodeMixin):
    node_name = 'servers'
    
    @cached_property
    def model(self):
        return self.provider.servers

class ServersList(ServersMixin, RestMixin, TemplateView):
    pass

class ServersDetail(ServersMixin, RestDetailView):
    pass
