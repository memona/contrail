from django.views.generic import TemplateView

from federweb.base.utils import cached_property
from federweb.base.views import AppNodeMixin, RestMixin, RestDetailView
from federweb.provider.views import ProvidersMixin

class ClustersMixin(ProvidersMixin, AppNodeMixin):
    node_name = 'clusters'
    
    @cached_property
    def model(self):
        return self.provider.clusters

class ClustersList(ClustersMixin, RestMixin, TemplateView):
    pass

class ClustersDetail(ClustersMixin, RestDetailView):
    pass
