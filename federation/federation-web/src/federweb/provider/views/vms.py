from django.views.generic import TemplateView

from federweb.base.utils import cached_property
from federweb.base.views import AppNodeMixin, RestMixin, RestDetailView
from federweb.provider.views import ProvidersMixin

class VmsMixin(ProvidersMixin, AppNodeMixin):
    node_name = 'vms'
    
    @cached_property
    def model(self):
        return self.provider.vms

class VmsList(VmsMixin, RestMixin, TemplateView):
    pass

class VmsDetail(VmsMixin, RestDetailView):
    pass
