$(function () {
    var editor = CodeMirror.fromTextArea($('#sla-content')[0], {
        mode: {
            name: 'xml'
        },
        lineNumbers: true,
        lineWrapping: true
    });

});
