import logging
import json
from xml import etree
from xml.dom import minidom
import iso8601

from django.conf import settings
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required

from federweb.base import utils


logger = logging.getLogger(__name__)

@login_required
def show(request):
    logger.debug("show_sla_agreement() started.")

    slaId = request.POST.get('slaId')
    appId = request.POST.get('appId')
    user = request.user
    token = request.session['monitoring_access_token']
    logger.debug("slaId=%s" % (slaId))

    logger.debug("Retrieving SLA offer...")
    url = "%s/users/%s/slas/%s" % (settings.FEDERATION_API_URL_SSL, user.uuid, slaId)
    response = utils.send_http_request("GET", url, token)
    if response.status_code != 200:
        raise Exception("Failed to retrieve SLA agreement with ID %s" % slaId)
    sla = json.loads(response.content)

    # pretty print sla content
    xml = minidom.parseString(sla["content"])
    sla["content"] = xml.toprettyxml(indent="  ")

    effectiveFromElement = xml.getElementsByTagName("slam:EffectiveFrom")
    effectiveFrom = effectiveFromElement[0].firstChild.nodeValue
    effectiveFromDate = iso8601.parse_date(effectiveFrom)

    effectiveUntilElement = xml.getElementsByTagName("slam:EffectiveUntil")
    effectiveUntil = effectiveUntilElement[0].firstChild.nodeValue
    effectiveUntilDate = iso8601.parse_date(effectiveUntil)

    agreedAtElement = xml.getElementsByTagName("slam:AgreedAt")
    agreedAt = agreedAtElement[0].firstChild.nodeValue
    agreedAtDate = iso8601.parse_date(agreedAt)

    logger.debug("Retrieving application info...")
    url = "%s/users/%s/applications/%s" % (settings.FEDERATION_API_URL_SSL, user.uuid, appId)
    response = utils.send_http_request("GET", url, token)
    if response.status_code != 200:
        raise Exception("Failed to retrieve application with UUID %s" % appId)
    app = json.loads(response.content)

    params = dict(
        slaId=slaId,
        sla=sla,
        app=app,
        effectiveFrom=effectiveFromDate,
        effectiveUntil=effectiveUntilDate,
        agreedAt=agreedAtDate
    )

    return render_to_response('user/apps/sla_agreement.html', params, context_instance=RequestContext(request))
