import logging
import json

from django.conf import settings
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required

from federweb.base import utils


logger = logging.getLogger(__name__)

@login_required
def show_offers(request):
    logger.debug("show_offers() started.")

    appId = request.POST.get('appId')
    slatId = request.POST.get('slatId')
    negotiationId = request.POST.get("negotiationId")
    user = request.user
    token = request.session['monitoring_access_token']
    logger.debug("appId=%s, slatId=%s, negotiationId=%s" % (appId, slatId, negotiationId))

    logger.debug("Retrieving all SLA offers...")
    url = "%s/users/%s/slats/%s/negotiation/%s/proposals" % (settings.FEDERATION_API_URL_SSL, user.uuid, slatId, negotiationId)
    response = utils.send_http_request("GET", url, token)
    logger.debug(response)
    offersData = json.loads(response.content)

    offers = []
    for i, offerData in enumerate(offersData):

        offerId = offerData["proposalId"]
        logger.debug("Retrieving SLA offer %s..." % offerId)
        url = "%s/users/%s/slats/%s/negotiation/%s/proposals/%s" % (settings.FEDERATION_API_URL_SSL, user.uuid, slatId, negotiationId, offerId)
        response = utils.send_http_request("GET", url, token)
        offerData = json.loads(response.content)

        providerUuid = offerData["slatAbstract"]["providers"][0]
        logger.debug("Retrieving provider's %s data...")
        url = "%s/providers/%s" % (settings.FEDERATION_API_URL_SSL, providerUuid)
        providerResponse = utils.send_http_request("GET", url, token)
        providerData = json.loads(providerResponse.content)

        offer = dict(
            num=i+1,
            offerId=offerId,
            created=offerData["created"],
            providerName=providerData["name"]
        )
        offers.append(offer)

    params = dict(
        fedApiAddress=settings.FEDERATION_API_URL_SSL,
        offers=offers,
        slatId=slatId,
        offersJson=json.dumps(offers),
        negotiationId=negotiationId,
        appId=appId
    )

    return render_to_response('user/apps/offers.html', params, context_instance=RequestContext(request))
