package org.ow2.contrail.federation.federationapi.resources;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.ow2.contrail.federation.federationapi.interfaces.BaseSingle;

/**
 * @author ales
 *
 */
public interface IApplicationResource extends BaseSingle {

	@GET
	@Produces("application/json")
	@Path("/ovfs")
	public Response getOvfs() throws Exception;
	
	/**
     * Gets the OVF.
     * 
     * @return
     */
    @GET
    @Produces("application/json")
    @Path("/ovfs/{ovfId}")
    public Response getOvf(@PathParam("ovfId") int ovfId) throws Exception;
	
	/**
     * Gets the OVF.
     * 
     * @return
     */
    @DELETE
    @Produces("application/json")
    @Path("/ovfs/{ovfId}")
    public Response deleteOvf(@PathParam("ovfId") int ovfId) throws Exception;

    
}
