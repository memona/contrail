
/**
 * SLACreationExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package org.ow2.contrail.federation.federationapi.slas.negotiation;

public class SLACreationExceptionException extends java.lang.Exception{
    
    private org.ow2.contrail.federation.federationapi.slas.negotiation.ContrailNegotiationStub.SLACreationExceptionE faultMessage;

    
        public SLACreationExceptionException() {
            super("SLACreationExceptionException");
        }

        public SLACreationExceptionException(java.lang.String s) {
           super(s);
        }

        public SLACreationExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public SLACreationExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(org.ow2.contrail.federation.federationapi.slas.negotiation.ContrailNegotiationStub.SLACreationExceptionE msg){
       faultMessage = msg;
    }
    
    public org.ow2.contrail.federation.federationapi.slas.negotiation.ContrailNegotiationStub.SLACreationExceptionE getFaultMessage(){
       return faultMessage;
    }
}
    