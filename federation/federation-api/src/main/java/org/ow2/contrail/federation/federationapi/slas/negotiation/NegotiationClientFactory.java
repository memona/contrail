package org.ow2.contrail.federation.federationapi.slas.negotiation;

import org.apache.axis2.AxisFault;
import org.ow2.contrail.federation.federationapi.utils.Conf;

public class NegotiationClientFactory {
    public static INegotiationClient create() throws AxisFault {
        if (Conf.getInstance().getSlaNegotiationEngine().equals(Conf.NegotiationEngine.SLASOI)) {
            return new NegotiationClient();
        }
        else {
            return new NegotiationClientMock();
        }
    }
}
