/**
 *
 */
package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.ow2.contrail.federation.federationapi.identityprovider.FederationIdentityProvider;
import org.ow2.contrail.federation.federationapi.utils.FederationDBCommon;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.entities.UGroup;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

/**
 * @author ales
 */
@Path("/groups")
public class UGroupsResource {

    protected static Logger logger =
            Logger.getLogger(UGroupsResource.class);

    @GET
    @Produces("application/json")
    public Response get() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Query query = em.createNamedQuery("UGroup.findAll");
            List<UGroup> gList = query.getResultList();
            JSONArray uriList = new JSONArray();
            for (UGroup g : gList) {
                JSONObject o = new JSONObject();
                o.put("name", g.getName());
                o.put("uri", RestUriBuilder.getGroupUri(g));
                uriList.put(o);
            }
            return Response.ok(uriList.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /* (non-Javadoc)
      * @see org.ow2.contrail.federation.federationapi.interfaces.BaseCollection#post(java.lang.String)
      */
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response post(JSONObject groupData) throws Exception {
        logger.debug("Entering post");

        String name = (String) groupData.get("name");
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Query query = em.createQuery("SELECT COUNT(u) FROM UGroup u WHERE u.name=:name");
            query.setParameter("name", name);
            if ((Long) query.getSingleResult() > 0) {
                // resource is already registered
                return Response.status(Response.Status.CONFLICT).build();
            }

            UGroup group = new UGroup();
            if (groupData.has("name"))
                group.setName(name);
            else {
                logger.error("Group does not have a name attribute.");
                return Response.status(Response.Status.NOT_ACCEPTABLE).build();
            }

            if (groupData.has("name"))
                group.setName((String) groupData.get("name"));
            if (groupData.has("description"))
                group.setDescription((String) groupData.get("description"));

            em.getTransaction().begin();
            em.persist(group);
            em.getTransaction().commit();

            try {
                FederationIdentityProvider.post("groups", groupData.toString());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            URI resourceUri = new URI(String.format("/%d", group.getGroupId()));
            return Response.created(resourceUri).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
            logger.debug("Exiting post");
        }
    }
}
