package org.ow2.contrail.federation.federationapi.utils;

import org.ow2.contrail.federation.federationdb.jpa.entities.*;
import org.ow2.contrail.federation.herasafauthorizer.Rule;

public class RestUriBuilder {
    private static String providerUriFormat = "/providers/%s";
    private static String voUriFormat = "/providers/%s/vos/%d";
    private static String serverUriFormat = "/providers/%s/servers/%d";
    private static String vmUriFormat = "/providers/%s/vms/%d";
    private static String clusterUriFormat = "/providers/%s/clusters/%d";
    private static String slatUriFormat = "/providers/%s/slats/%d";
    private static String authRuleUriFormat = "/auth/rules/%s";
    private static String idpUriFormat = "/idps/%s";
    private static String applicationUriFormat = "/applications/%s";
    private static String userUriFormat = "/users/%s";
    private static String userOvfUriFormat = "/users/%s/ovfs/%d";
    private static String attributeUriFormat = "/attributes/%s";
    private static String userSlatUriFormat = "/users/%s/slats/%d";
    private static String userSlaUriFormat = "/users/%s/slas/%d";
    private static String userAttrUriFormat = "/users/%s/attributes/%d";
    private static String roleUriFormat = "/roles/%d";
    private static String groupUriFormat = "/groups/%d";
    private static String ovfUriFormat = "/providers/%s/ovfs/%d";
    private static String providerServiceUriFormat = "/providers/%s/services/%d";
    private static String federationSlatUriFormat = "/federation/slats/%d";

    public static String getProviderUri(Provider provider) {
        return String.format(providerUriFormat, provider.getUuid());
    }

    public static String getServerUri(Server server) {
        return String.format(serverUriFormat, server.getProviderId().getUuid(), server.getServerId());
    }

    public static String getVoUri(Vo vo) {
        return String.format(voUriFormat, vo.getProviderId().getUuid(), vo.getVoId());
    }

    public static String getVmUri(Vm vm) {
        return String.format(vmUriFormat, vm.getProviderId().getUuid(), vm.getVmId());
    }

    public static String getClusterUri(Cluster cluster) {
        return String.format(clusterUriFormat, cluster.getProviderId().getUuid(), cluster.getClusterId());
    }

    public static String getSlatUri(SLATemplate slat) {
        return String.format(slatUriFormat, slat.getProviderId().getUuid(), slat.getSlatId());
    }

    public static String getAuthRuleUri(Rule rule) {
        return String.format(authRuleUriFormat, rule.getRuleId());
    }

    public static String getIdpUri(IdentityProvider idp) {
        return String.format(idpUriFormat, idp.getIdpUuid());
    }

    public static String getApplicationUri(Application app) {
        return String.format(applicationUriFormat, app.getUuid());
    }

    public static String getUserUri(User user) {
        return String.format(userUriFormat, user.getUuid());
    }

    public static String getUserOvfUri(UserOvf ovf) {
        return String.format(userOvfUriFormat, ovf.getUserId().getUuid(), ovf.getOvfId());
    }

    public static String getAttributeUri(Attribute attribute) {
        return String.format(attributeUriFormat, attribute.getAttributeUuid());
    }

    public static String getUserSlatUri(UserSLATemplate slat) {
        return String.format(userSlatUriFormat, slat.getUserId().getUuid(), slat.getSLATemplateId());
    }

    public static String getUserSlaUri(UserSLA sla) {
        return String.format(userSlaUriFormat, sla.getUserId().getUuid(), sla.getSLAId());
    }

    public static String getRoleUri(URole role) {
        return String.format(roleUriFormat, role.getRoleId());
    }

    public static String getGroupUri(UGroup group) {
        return String.format(groupUriFormat, group.getGroupId());
    }

    public static String getUserAttrUri(UserhasAttribute attr) {
        return String.format(userAttrUriFormat, attr.getUser().getUuid(),
                attr.getUserhasAttributePK().getAttributeUuid());
    }

    public static String getOvfUri(Ovf ovf) {
        return String.format(ovfUriFormat, ovf.getProviderId().getUuid(), ovf.getOvfId());
    }

    public static String getProviderServiceUri(Service service) {
        return String.format(providerServiceUriFormat, service.getProvider().getUuid(), service.getServiceId());
    }

    public static String getFederationSlatUri(SLATemplate slat) {
        return String.format(federationSlatUriFormat, slat.getSlatId());
    }
}

