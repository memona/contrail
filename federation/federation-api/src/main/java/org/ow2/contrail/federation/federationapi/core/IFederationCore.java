/**
 * 
 */
package org.ow2.contrail.federation.federationapi.core;

import java.util.HashMap;
import java.util.List;

import org.ow2.contrail.federation.federationcore.sla.FederationProposal;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;

/**
 * Here are provided all the functions of the federation core.
 * 
 * @author ales
 *
 */
public interface IFederationCore {

	public void init(String configurationFile) throws Exception;
	
	public List<String> propagateUser(User user, List<Provider> providers) throws Exception;
	
	public void providerCreation(Provider provider) throws Exception;
	
	public boolean startApplication(User user, Application app) throws Exception;
	
	public void stopApplication(User user, Application app) throws Exception;
	
	public boolean submitApplication(User user, Application app, UserSLATemplate userSLATemplate) throws Exception;

	public boolean cancelSubmission(User user, Application app) throws Exception;

	public void revokeAccessApplication(User user, Application app) throws Exception;

	public boolean createAgreement(User user, Application app, int proposalId) throws Exception;

	public boolean killApplication(User user, Application app) throws Exception;
}
