package org.ow2.contrail.federation.federationapi.resources;

import javax.persistence.EntityManager;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationapi.MyServletContextListener;
import org.ow2.contrail.federation.federationapi.utils.FederationDBCommon;
import org.ow2.contrail.federation.federationdb.jpa.dao.ApplicationDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Application;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

@Path("/federation/manager")
public class FederationManagerResource {

	protected static Logger logger = Logger.getLogger(UserAppResource.class);

	@POST
	@Path("user/{userUuid}/applications/{appUuid}/revoke")
	public Response revokeUserApp(@PathParam("userUuid") String userUuid, @PathParam("appUuid") String appUuid) {
		EntityManager em = PersistenceUtils.getInstance().getEntityManager();

		try {
			User user = new UserDAO(em).findByUuid(userUuid);
			if (user == null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}

			Application application = new ApplicationDAO(em).findByUuid(appUuid);
			if (application == null || !user.getApplicationList().contains(application)) {
				throw new WebApplicationException(Response.Status.NOT_FOUND);
			}

			MyServletContextListener.getFederationCore().revokeAccessApplication(user, application);

			return Response.status(Response.Status.NO_CONTENT).build();
		}
		catch (Exception e1) {
			logger.error("Error trying to revoke acces for the application on the federation core.");
			logger.error(e1.getMessage());
			logger.error(FederationDBCommon.getStackTrace(e1));
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		finally {
			PersistenceUtils.getInstance().closeEntityManager(em);
		}
	}
}
