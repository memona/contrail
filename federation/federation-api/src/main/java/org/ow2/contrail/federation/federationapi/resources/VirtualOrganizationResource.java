package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.ClusterDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.VoDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Cluster;
import org.ow2.contrail.federation.federationdb.jpa.entities.Vo;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Path("/providers/{providerUuid}/vos/{voId}")
public class VirtualOrganizationResource {
    private static Logger log = Logger.getLogger(VirtualOrganizationResource.class);
    private String providerUuid;
    private int voId;

    public VirtualOrganizationResource(@PathParam("providerUuid") String providerUuid, @PathParam("voId") int voId) {
        this.providerUuid = providerUuid;
        this.voId = voId;
    }

    /**
     * Returns the JSON representation of the provider's virtual organization.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getVO() throws JSONException {
        if (log.isTraceEnabled()) {
            log.trace(String.format("getVO(ID=%d) started.", voId));
        }

        Vo vo = new VoDAO().findById(voId);
        if (vo == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        String resourceUri = RestUriBuilder.getVoUri(vo);
        org.json.JSONObject json = vo.toJSON();
        json.put("uri", resourceUri);
        json.put("clusters", resourceUri + "/clusters");

        log.trace("getVO() finished successfully.");
        return Response.ok(json.toString()).build();
    }

    /**
     * Updates the selected VO.
     *
     * @return
     */
    @PUT
    @Consumes("application/json")
    public Response updateVO(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("updateVO(ID=%d) started. Data: %s", voId, requestBody));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Vo vo = new VoDAO(em).findById(voId);
            if (vo == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            try {
                JSONObject json = new JSONObject(requestBody);

                em.getTransaction().begin();
                vo.update(json);
                em.getTransaction().commit();

                log.trace("updateVO() finished successfully.");
                return Response.status(Response.Status.NO_CONTENT).build();
            }
            catch (Exception e) {
                log.error("Update failed: ", e);
                throw new WebApplicationException(
                        Response.status(Response.Status.BAD_REQUEST).
                                entity(String.format("Update failed: %s.", e.getMessage())).
                                build());
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Deletes selected VO.
     *
     * @return
     */
    @DELETE
    public Response removeVO() throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("removeVO(ID=%d) started.", voId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Vo vo = new VoDAO(em).findById(voId);
            if (vo == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();
            em.remove(vo);
            em.getTransaction().commit();

            log.trace("removeVO() finished successfully.");
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns all clusters registered at the given VO.
     *
     * @return
     */
    @GET
    @Path("/clusters")
    @Produces("application/json")
    public Response getClusters() throws Exception {
        log.trace("getClusters() started.");

        Vo vo = new VoDAO().findById(voId);
        if (vo == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        JSONArray json = new JSONArray();
        for (Cluster cluster : vo.getClusterList()) {
            JSONObject o = new JSONObject();
            String uri = String.format("%s/clusters/%d", RestUriBuilder.getVoUri(vo), cluster.getClusterId());
            o.put("uri", uri);
            o.put("baseUri", RestUriBuilder.getClusterUri(cluster));
            json.put(o);
        }

        log.trace("getClusters() finished successfully.");
        return Response.ok(json.toString()).build();
    }

    /**
     * Registers cluster at the selected VO.
     *
     * @return
     */
    @POST
    @Path("/clusters")
    @Consumes("application/json")
    public Response registerCluster(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace("registerCluster() started. Data: " + requestBody);
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Vo vo = new VoDAO(em).findById(voId);
            if (vo == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            Cluster cluster;
            try {
                JSONObject json = new JSONObject(requestBody);
                String clusterURI = json.getString("clusterURI");
                Pattern uriPattern = Pattern.compile("^/providers/(\\d+)/clusters/(\\d+)$");
                Matcher m = uriPattern.matcher(clusterURI);
                if (!m.find()) {
                    throw new Exception("Invalid cluster URI: " + clusterURI);
                }
                int providerId = Integer.parseInt(m.group(1));
                int clusterId = Integer.parseInt(m.group(2));
                cluster = new ClusterDAO(em).findById(providerId, clusterId);
                if (cluster == null) {
                    throw new Exception(String.format("Cluster '%s' not found.", clusterURI));
                }
            }
            catch (Exception e) {
                throw new WebApplicationException(
                        Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
            }

            if (!vo.getClusterList().contains(cluster)) {
                em.getTransaction().begin();
                vo.getClusterList().add(cluster);
                cluster.getVoList().add(vo);
                em.getTransaction().commit();
            }
            // no problem if cluster is already registered

            URI resourceUri = new URI(String.format("/%d", cluster.getClusterId()));
            log.trace("registerCluster() finished successfully.");
            return Response.created(resourceUri).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns data about specific cluster registration
     *
     * @return
     */
    @GET
    @Path("/clusters/{id}")
    @Produces("application/json")
    public Response getClusterRegistration(@PathParam("id") int id) throws JSONException {
        if (log.isTraceEnabled()) {
            log.trace(String.format("getClusterRegistration(%d) started.", id));
        }

        Vo vo = new VoDAO().findById(voId);
        if (vo == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        for (Cluster cluster : vo.getClusterList()) {
            if (cluster.getClusterId() == id) {
                JSONObject o = new JSONObject();
                String uri = String.format("%s/clusters/%d", RestUriBuilder.getVoUri(vo), cluster.getClusterId());
                o.put("uri", uri);
                o.put("baseUri", RestUriBuilder.getClusterUri(cluster));

                log.trace("getClusterRegistration() finished successfully. Cluster registration found.");
                return Response.ok(o.toString()).build();
            }
        }

        log.trace("Cluster is not registered at given VO.");
        throw new WebApplicationException(Response.Status.NOT_FOUND);
    }

    /**
     * Unregisters given cluster from given VO.
     *
     * @return
     */
    @DELETE
    @Path("/clusters/{id}")
    @Produces("application/json")
    public Response unregisterCluster(@PathParam("id") int id) throws JSONException {
        if (log.isTraceEnabled()) {
            log.trace(String.format("unregisterCluster(%d) started.", id));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Vo vo = new VoDAO(em).findById(voId);
            if (vo == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            for (Cluster cluster : vo.getClusterList()) {
                if (cluster.getClusterId() == id) {
                    em.getTransaction().begin();
                    vo.getClusterList().remove(cluster);
                    cluster.getVoList().remove(vo);
                    em.getTransaction().commit();

                    return Response.status(Response.Status.NO_CONTENT).build();
                }
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }


        log.trace("Cluster is not registered at given VO.");
        throw new WebApplicationException(Response.Status.NOT_FOUND);
    }

    /**
     * Returns a list of all CEEs for the selected provider and virtual organization.
     *
     * @return
     */
    @GET
    @Path("/cees")
    @Produces("application/json")
    public Response getCEEs() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a new CEE for the selected provider and virtual organization.
     *
     * @return
     */
    @POST
    @Path("/cees")
    public Response createCEE() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns a list of elastic DCs for the provider's VO.
     *
     * @return
     */
    @GET
    @Path("/edcs")
    @Produces("application/json")
    public Response getEDCs() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a new EDC for the selected provider.
     *
     * @return
     */
    @POST
    @Path("/edcs")
    public Response createEDC() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns all attributes of the provider's VO.
     *
     * @return
     */
    @GET
    @Path("/attributes")
    @Produces("application/json")
    public Response getAttributes() {
        throw new UnsupportedOperationException();
    }

    /**
     * Updates attributes of the provider's VO.
     *
     * @return
     */
    @PUT
    @Path("/attributes")
    public Response updateAttributes() throws Exception {
        return Response.ok().build();
    }
}
