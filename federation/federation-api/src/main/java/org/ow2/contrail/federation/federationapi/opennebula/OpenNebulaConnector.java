package org.ow2.contrail.federation.federationapi.opennebula;


import org.opennebula.client.Client;
import org.opennebula.client.OneResponse;
import org.opennebula.client.user.User;
//import org.ow2.contrail.federation.utils.FederationProperties;
//import org.ow2.contrail.federation.utils.FederationPropertiesImpl;

public class OpenNebulaConnector {
	private static String ONE_ADMIN_USERNAME;
    private static String ONE_ADMIN_PASSWORD;
    public static String ONE_RPC_HOST;
    public static String ONE_RPC_PORT;
    public static String VIRTUAL_NETWORK;
    public static String VM_IMAGE;
    public static String MONITORING_REST_URI;
    public static String VM_DOMAIN;
    public static String CLUSTER_FQDN;
    
    private static Client oneClient;
	private static OpenNebulaConnector oneConnector;
    
    private OpenNebulaConnector() {
//    	FederationProperties properties = FederationPropertiesImpl.getInstance();
//		
//    	ONE_RPC_HOST = properties.getProperty("one_rpc_host");
//	    ONE_RPC_PORT = properties.getProperty("one_rpc_port");
//	    ONE_ADMIN_USERNAME = properties.getProperty("one_admin_username");
//	    ONE_ADMIN_PASSWORD = properties.getProperty("one_admin_password");
//	    VIRTUAL_NETWORK = properties.getProperty("virtual_network");
//	    MONITORING_REST_URI = properties.getProperty("monitoring_rest_uri");
//	    VM_DOMAIN = properties.getProperty("vm_domain");
//	    CLUSTER_FQDN = properties.getProperty("cluster_fqdn");
//	    
//	    try {
//			oneClient = new Client(ONE_ADMIN_USERNAME + ":" + ONE_ADMIN_PASSWORD, "http://" + ONE_RPC_HOST + ":" + ONE_RPC_PORT + "/RPC2");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
    
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
    
    public static synchronized OpenNebulaConnector getInstance() {
    	if(oneConnector == null)
			oneConnector = new OpenNebulaConnector();
			
    	return oneConnector;
    }
    
    public Client getAdminClient() {
    	return oneClient;
    }
    
    public OneResponse createUser(String username, String password) {
    	// XXX: Encode real password
    	return User.allocate(oneClient, username, "7bc8559a8fe509e680562b85c337f170956fcb06");
    }
}
