/**
 *
 */
package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.ow2.contrail.federation.federationapi.identityprovider.FederationIdentityProvider;
import org.ow2.contrail.federation.federationapi.utils.FederationDBCommon;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.entities.URole;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

/**
 * @author ales
 */
@Path("/roles")
public class URolesResource {

    protected static Logger logger = Logger.getLogger(URolesResource.class);

    @GET
    @Produces("application/json")
    public Response get() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Query query = em.createNamedQuery("URole.findAll");
            List<URole> roleList = query.getResultList();
            JSONArray uriList = new JSONArray();
            for (URole role : roleList) {
                JSONObject o = new JSONObject();
                o.put("name", role.getName());
                o.put("uri", RestUriBuilder.getRoleUri(role));
                uriList.put(o);
            }
            return Response.ok(uriList.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response post(JSONObject roleData) throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            URole role = new URole();

            if (roleData.has("name"))
                role.setName((String) roleData.get("name"));
            if (roleData.has("description"))
                role.setDescription((String) roleData.get("description"));
            if (roleData.has("acl"))
                role.setDescription((String) roleData.get("acl"));

            em.getTransaction().begin();
            em.persist(role);
            em.getTransaction().commit();

            try {
                FederationIdentityProvider.post("roles", roleData.toString());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            URI resourceUri = new URI(String.format("/%d", role.getRoleId()));
            return Response.created(resourceUri).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
            logger.debug("Exiting post");
        }
    }
}
