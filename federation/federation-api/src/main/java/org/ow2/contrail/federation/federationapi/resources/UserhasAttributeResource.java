package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.ow2.contrail.federation.federationapi.identityprovider.FederationIdentityProvider;
import org.ow2.contrail.federation.federationapi.utils.FederationDBCommon;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserhasAttributeDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserhasAttribute;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/users/{userUuid}/attributes/{attrUuid}")
public class UserhasAttributeResource {

    protected static Logger logger =
            Logger.getLogger(UserhasAttributeResource.class);

    private String userUuid;
    private String attrUuid;

    public UserhasAttributeResource(@PathParam("userUuid") String userUuid, @PathParam("attrUuid") String attrUuid) {
        this.userUuid = userUuid;
        this.attrUuid = attrUuid;
    }

    @GET
    @Produces("application/json")
    public Response get() throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            UserhasAttribute userAttr = new UserhasAttributeDAO(em).findByUuid(user.getUserId(), attrUuid);
            if (userAttr == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONObject json = new JSONObject();
            json.put("value", userAttr.getValue());
            json.put("referenceId", userAttr.getReferenceId());
            json.put("userId", String.format("/users/%d", user.getUserId()));
            if (userAttr.getAttribute() != null)
                json.put("attributeUuid", userAttr.getAttribute().getAttributeUuid());
            else
                logger.error("Attribute is null!");
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Remove user attribute, update lists in Attribute and User entities.
     */
    @DELETE
    public Response delete() throws Exception {
        logger.debug("Entering delete user attributes.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            UserhasAttribute userAttr = new UserhasAttributeDAO(em).findByUuid(user.getUserId(), attrUuid);
            if (userAttr == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();
            em.remove(userAttr);
            //user.getUserhasAttributeList().remove(userattr);
            //attribute.getUserhasAttributeList().remove(this.userattr);
            em.getTransaction().commit();
            try {
                FederationIdentityProvider.delete(String.format("users/%s/attributes/%s", user.getUuid(),
                        userAttr.getAttribute().getAttributeUuid()));
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            logger.debug("Exiting delete user attributes.");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public Response put(JSONObject attrData) throws Exception {
        logger.debug("Entering put user attributes.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            UserhasAttribute userAttr = new UserhasAttributeDAO(em).findByUuid(user.getUserId(), attrUuid);
            if (userAttr == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();

            if (attrData.has("value"))
                userAttr.setValue(attrData.getString("value"));
            if (attrData.has("referenceId"))
                userAttr.setReferenceId(attrData.getInt("referenceId"));

            em.getTransaction().commit();

            try {
                FederationIdentityProvider.put(String.format("users/%s/attributes/%s",
                        user.getUuid(), userAttr.getAttribute().getAttributeUuid()), attrData.toString());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            logger.debug("Entering put user attributes.");
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
