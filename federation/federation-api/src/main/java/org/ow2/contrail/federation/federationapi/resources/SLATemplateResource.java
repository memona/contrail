package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.SLATemplateDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.SLATemplate;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserSLATemplate;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/providers/{providerUuid}/slats/{slatId}")
public class SLATemplateResource {
    private static Logger log = Logger.getLogger(SLATemplateResource.class);
    private String providerUuid;
    private int slatId;

    public SLATemplateResource(@PathParam("providerUuid") String providerUuid, @PathParam("slatId") int slatId) {
        this.providerUuid = providerUuid;
        this.slatId = slatId;
    }

    /**
     * Returns the JSON representation of the given SLA template.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getSLATemplate() throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("getSLATemplate(ID=%d) started.", slatId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            SLATemplate slaTemplate = new SLATemplateDAO(em).findById(providerUuid, slatId);
            if (slaTemplate == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            org.json.JSONObject json = slaTemplate.toJSON();
            json.put("uri", RestUriBuilder.getSlatUri(slaTemplate));

            JSONArray userSLATArr = new JSONArray();
            for (UserSLATemplate userSlat : slaTemplate.getUserSLATemplateList()) {
                userSLATArr.put(RestUriBuilder.getUserSlatUri(userSlat));
            }
            json.put("userSLATs", userSLATArr);

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Updates the selected SLA template.
     *
     * @return
     */
    @PUT
    @Consumes("application/json")
    public Response updateSLATemplate(JSONObject json) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("updateSLATemplate(ID=%d) started.", slatId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            SLATemplate slaTemplate = new SLATemplateDAO(em).findById(providerUuid, slatId);
            if (slaTemplate == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            slaTemplate.update(json);
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Unregisters selected SLA template.
     *
     * @return
     */
    @DELETE
    public Response removeSLATemplate() throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("removeSLATemplate(ID=%d) started.", slatId));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            SLATemplate slaTemplate = new SLATemplateDAO(em).findById(providerUuid, slatId);
            if (slaTemplate == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            em.remove(slaTemplate);
            slaTemplate.getProviderId().getSLATemplateList().remove(slaTemplate);
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
