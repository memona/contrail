/**
 *
 */
package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.ow2.contrail.federation.federationapi.identityprovider.FederationIdentityProvider;
import org.ow2.contrail.federation.federationapi.utils.FederationDBCommon;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.entities.IdentityProvider;
import org.ow2.contrail.federation.federationdb.jpa.entities.UserhasidentityProvider;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * @author ales
 */
@Path("/idps/{idpUuid}")
public class IdentityProviderResource {

    protected static Logger logger = Logger.getLogger(IdentityProviderResource.class);
    private String idpUuid;

    public IdentityProviderResource(@PathParam("idpUuid") String idpUuid) {
        this.idpUuid = idpUuid;
    }

    @GET
    @Produces("application/json")
    public Response get() throws Exception {
        logger.debug("Entering get");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            IdentityProvider idp = em.find(IdentityProvider.class, idpUuid);
            if (idp == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            String baseUri = RestUriBuilder.getIdpUri(idp);
            JSONObject json = new JSONObject();
            json.put("uuid", idp.getIdpUuid());
            json.put("providerURI", idp.getProviderURI());
            json.put("description", idp.getDescription());
            json.put("providerName", idp.getProviderName());
            json.put("users", baseUri + "/users");
            json.put("attributes", baseUri + "/attributes");
            logger.debug("Exiting get");
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * @return the list of users who are in this group.
     * @throws Exception
     */
    @GET
    @Produces("application/json")
    @Path("/users")
    public Response getUsers() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            IdentityProvider idp = em.find(IdentityProvider.class, idpUuid);
            if (idp == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            JSONArray attr = new JSONArray();
            for (UserhasidentityProvider user : idp.getUserhasidentityProviderList()) {
                JSONObject o = new JSONObject();
                o.put("name", user.getUser().getUsername());
                o.put("uri", RestUriBuilder.getUserUri(user.getUser()));
                attr.put(o);
            }
            return Response.ok(attr.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @DELETE
    public Response delete() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            IdentityProvider idp = em.find(IdentityProvider.class, idpUuid);
            if (idp == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            em.remove(idp);
            em.getTransaction().commit();

            try {
                FederationIdentityProvider.delete("idps/" + idp.getIdpUuid());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public Response put(JSONObject idpData) throws Exception {
        logger.debug("Entering put");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            IdentityProvider idp = em.find(IdentityProvider.class, idpUuid);
            if (idp == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            logger.debug("Commiting new Idp");
            em.getTransaction().begin();

            if (idpData.has("description"))
                idp.setDescription(idpData.getString("description"));
            if (idpData.has("providerURI"))
                idp.setProviderURI(idpData.getString("providerURI"));
            if (idpData.has("providerName"))
                idp.setProviderName(idpData.getString("providerName"));

            em.getTransaction().commit();

            try {
                FederationIdentityProvider.put("idps/" + idp.getIdpUuid(), idpData.toString());
            }
            catch (Exception err) {
                logger.error(FederationDBCommon.getStackTrace(err));
            }

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
            logger.debug("Exiting put");
        }
    }
}
