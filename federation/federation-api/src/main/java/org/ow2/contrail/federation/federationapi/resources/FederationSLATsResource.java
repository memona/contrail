package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.ow2.contrail.federation.federationapi.utils.Conf;
import org.ow2.contrail.federation.federationapi.utils.DBUtils;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.ProviderDAO;
import org.ow2.contrail.federation.federationdb.jpa.dao.SLATemplateDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.Provider;
import org.ow2.contrail.federation.federationdb.jpa.entities.SLATemplate;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/federation/slats")
public class FederationSLATsResource {
    private static Logger log = Logger.getLogger(FederationSLATsResource.class);

    /**
     * Returns a list of all registered SLA templates at a federation level.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getAll() throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(Conf.FEDERATION_PROVIDER_UUID);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray json = new JSONArray();
            for (SLATemplate slaTemplate : provider.getSLATemplateList()) {
                JSONObject o = new JSONObject();
                o.put("name", slaTemplate.getName());
                o.put("uri", RestUriBuilder.getFederationSlatUri(slaTemplate));
                o.put("url", slaTemplate.getUrl());
                json.put(o);
            }

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Registers a new SLA template at the selected provider.
     *
     * @return
     */
    @POST
    @Consumes("application/json")
    public Response add(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace("addSLATemplate() started. Data: " + requestBody);
        }

        SLATemplate slaTemplate;
        try {
            JSONObject json = new JSONObject(requestBody);
            slaTemplate = new SLATemplate(json);
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(Conf.FEDERATION_PROVIDER_UUID);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            slaTemplate.setProviderId(provider);

            em.getTransaction().begin();
            em.persist(slaTemplate);
            provider.getSLATemplateList().add(slaTemplate);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", slaTemplate.getSlatId()));
            return Response.created(resourceUri).build();
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns the JSON representation of the specified SLA template.
     *
     * @return
     */
    @GET
    @Path("{slatId}")
    @Produces("application/json")
    public Response get(@PathParam("slatId") int slatId) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            SLATemplate slaTemplate = new SLATemplateDAO(em).findById(Conf.FEDERATION_PROVIDER_UUID, slatId);
            if (slaTemplate == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            org.json.JSONObject json = slaTemplate.toJSON();
            json.put("uri", RestUriBuilder.getFederationSlatUri(slaTemplate));

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Updates the selected SLA template.
     *
     * @return
     */
    @PUT
    @Path("{slatId}")
    @Consumes("application/json")
    public Response update(@PathParam("slatId") int slatId, JSONObject json) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            SLATemplate slaTemplate = new SLATemplateDAO(em).findById(Conf.FEDERATION_PROVIDER_UUID, slatId);
            if (slaTemplate == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            slaTemplate.update(json);
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Deletes specified SLA template.
     *
     * @return
     */
    @DELETE
    @Path("{slatId}")
    public Response delete(@PathParam("slatId") int slatId) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            SLATemplate slaTemplate = new SLATemplateDAO(em).findById(Conf.FEDERATION_PROVIDER_UUID, slatId);
            if (slaTemplate == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();
            em.remove(slaTemplate);
            slaTemplate.getProviderId().getSLATemplateList().remove(slaTemplate);
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
