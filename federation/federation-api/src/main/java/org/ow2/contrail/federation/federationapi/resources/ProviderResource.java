package org.ow2.contrail.federation.federationapi.resources;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.ow2.contrail.common.oauth.client.OAuthFilter;
import org.ow2.contrail.common.oauth.client.OAuthHttpClient;
import org.ow2.contrail.common.oauth.client.TokenInfo;
import org.ow2.contrail.federation.federationapi.exceptions.RestApiException;
import org.ow2.contrail.federation.federationapi.utils.Conf;
import org.ow2.contrail.federation.federationapi.utils.DBUtils;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.ProviderDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.*;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/providers/{providerUuid}")
public class ProviderResource {
    private static Logger log = Logger.getLogger(ProviderResource.class);
    private String providerUuid;

    public ProviderResource(@PathParam("providerUuid") String providerUuid) {
        this.providerUuid = providerUuid;
    }

    /**
     * Returns the JSON representation of the provider.
     *
     * @return
     */
    @GET
    @Produces("application/json")
    public Response getProvider() throws JSONException {
        if (log.isTraceEnabled()) {
            log.trace(String.format("getProvider(ID=%s) started.", providerUuid));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            org.json.JSONObject json = provider.toJSON();
            String providerUri = RestUriBuilder.getProviderUri(provider);
            json.put("uri", providerUri);
            json.put("servers", providerUri + "/servers");
            json.put("vms", providerUri + "/vms");
            json.put("clusters", providerUri + "/clusters");
            json.put("ovfs", providerUri + "/ovfs");
            json.put("slats", providerUri + "/slats");
            json.put("applications", providerUri + "/applications");
            json.put("services", providerUri + "/services");

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Updates the selected provider.
     *
     * @return
     */
    @PUT
    @Consumes("application/json")
    public Response updateProvider(String requestBody) {
        if (log.isTraceEnabled()) {
            log.trace(String.format("updateProvider(ID=%s) started. Data: %s", providerUuid, requestBody));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONObject json = new JSONObject(requestBody);

            em.getTransaction().begin();
            provider.update(json);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        catch (Exception e) {
            log.error("Update failed: ", e);
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity(String.format("Update failed: %s.", e.getMessage())).
                            build());
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Deletes selected provider.
     *
     * @return
     */
    @DELETE
    public Response removeProvider() throws Exception {
        if (log.isTraceEnabled()) {
            log.trace(String.format("removeProvider(ID=%s) started.", providerUuid));
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            em.getTransaction().begin();
            em.remove(provider);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Return a list of all virtual organizations for the provider.
     *
     * @return
     */
    @GET
    @Path("/vos")
    @Produces("application/json")
    public Response getVOs() throws Exception {
        log.trace("getVOs() started.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray json = new JSONArray();
            for (Vo vo : provider.getVoList()) {
                JSONObject o = new JSONObject();
                o.put("name", vo.getName());
                o.put("uri", RestUriBuilder.getVoUri(vo));
                json.put(o);
            }

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Creates a new virtual organization for the provider.
     *
     * @return
     */
    @POST
    @Path("/vos")
    @Consumes("application/json")
    public Response addVO(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace("addVO() started. Data: " + requestBody);
        }

        Vo vo = null;
        try {
            JSONObject json = new JSONObject(requestBody);
            vo = new Vo(json);
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            vo.setProviderId(provider);

            em.getTransaction().begin();
            em.persist(vo);
            provider.getVoList().add(vo);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", vo.getVoId()));
            return Response.created(resourceUri).build();
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Return a list of all users for the selected provider.
     *
     * @return
     */
    @GET
    @Path("/users")
    @Produces("application/json")
    public Response getUsers() {
        throw new UnsupportedOperationException();
    }

    /**
     * Links new user with the provider or updates existing one.
     *
     * @return
     */
    @POST
    @Path("/users")
    public Response addUser() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns a list of all data centers for the selected provider.
     *
     * @return
     */
    @GET
    @Path("/dcs")
    @Produces("application/json")
    public Response getDataCenters() {
        throw new UnsupportedOperationException();
    }

    /**
     * Links new data center with the provider or updates existing one.
     *
     * @return
     */
    @POST
    @Path("/dcs")
    public Response addDataCenter() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns all provider's virtual machines.
     *
     * @return
     */
    @GET
    @Path("/vms")
    @Produces("application/json")
    public Response getVMs() throws Exception {
        log.trace("getVMs() started.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray json = new JSONArray();
            for (Vm vm : provider.getVmList()) {
                JSONObject o = new JSONObject();
                o.put("name", vm.getName());
                o.put("uri", RestUriBuilder.getVmUri(vm));
                json.put(o);
            }

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Creates a new virtual machine for the selected provider.
     *
     * @return
     */
    @POST
    @Path("/vms")
    @Consumes("application/json")
    public Response addVM(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace("addVM() started. Data: " + requestBody);
        }

        Vm vm = null;
        try {
            JSONObject json = new JSONObject(requestBody);
            vm = new Vm(json);
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            vm.setProviderId(provider);
            em.getTransaction().begin();
            em.persist(vm);
            provider.getVmList().add(vm);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", vm.getVmId()));
            return Response.created(resourceUri).build();
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns all storages of the selected provider.
     *
     * @return
     */
    @GET
    @Path("/storages")
    @Produces("application/json")
    public Response getStorages() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a new storage for the selected provider.
     *
     * @return
     */
    @POST
    @Path("/storages")
    public Response createStorage() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns all networks of the selected provider.
     *
     * @return
     */
    @GET
    @Path("/networks")
    @Produces("application/json")
    public Response getNetworks() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates a new network for the selected provider.
     *
     * @return
     */
    @POST
    @Path("/networks")
    public Response createNetwork() throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns all provider's servers.
     *
     * @return
     */
    @GET
    @Path("/servers")
    @Produces("application/json")
    public Response getServers() throws Exception {
        log.trace("getServers() started.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray json = new JSONArray();
            for (Server server : provider.getServerList()) {
                JSONObject o = new JSONObject();
                o.put("name", server.getName());
                o.put("uri", RestUriBuilder.getServerUri(server));
                json.put(o);
            }

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Creates a new server for the given provider.
     *
     * @return
     */
    @POST
    @Path("/servers")
    @Consumes("application/json")
    public Response addServer(JSONObject serverData) throws Exception {
        log.trace("addServer() started.");

        Server server = null;
        try {
            server = new Server(serverData);
        }
        catch (JSONException e) {
            throw new RestApiException(Response.Status.BAD_REQUEST, "Invalid server data: " + e.getMessage());
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            server.setProviderId(provider);

            em.getTransaction().begin();
            em.persist(server);
            provider.getServerList().add(server);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", server.getServerId()));
            return Response.created(resourceUri).build();
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns all provider's clusters.
     *
     * @return
     */
    @GET
    @Path("/clusters")
    @Produces("application/json")
    public Response getClusters() throws Exception {
        log.trace("getClusters() started.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray json = new JSONArray();
            for (Cluster cluster : provider.getClusterList()) {
                JSONObject o = new JSONObject();
                o.put("name", cluster.getName());
                o.put("uri", RestUriBuilder.getClusterUri(cluster));
                json.put(o);
            }

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Creates a new cluster for the selected provider.
     *
     * @return
     */
    @POST
    @Path("/clusters")
    @Consumes("application/json")
    public Response addCluster(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace("addCluster() started. Data: " + requestBody);
        }

        Cluster cluster = null;
        try {
            JSONObject json = new JSONObject(requestBody);
            cluster = new Cluster(json);
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            cluster.setProviderId(provider);

            em.getTransaction().begin();
            em.persist(cluster);
            provider.getClusterList().add(cluster);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", cluster.getClusterId()));
            return Response.created(resourceUri).build();
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns all provider's OVFs.
     * Due to the move of OVFs under Application, this method is not working
     * properly.
     *
     * @return
     */
    @GET
    @Path("/ovfs")
    @Produces("application/json")
    public Response getOvfs() throws Exception {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray json = new JSONArray();
            for (Ovf ovf : provider.getOvfList()) {
                String uri = String.format("/providers/%d/ovfs/%d", provider.getProviderId(),
                        ovf.getOvfId());
                JSONObject o = new JSONObject();
                o.put("name", ovf.getName());
                o.put("uri", uri);
                json.put(o);
            }
            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }


    /**
     * Creates a new OVF for the given provider.
     * Due to the move of OVFs under Application, this method is not working
     * properly.
     *
     * @return
     */
    @POST
    @Path("/ovfs")
    @Consumes("application/json")
    public Response addOvf(String requestBody) throws Exception {
        Ovf ovf = null;
        try {
            JSONObject json = new JSONObject(requestBody);
            ovf = new Ovf(json);
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            ovf.setProviderId(provider);

            em.getTransaction().begin();
            em.persist(ovf);
            provider.getOvfList().add(ovf);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", ovf.getOvfId()));
            return Response.created(resourceUri).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns a list of all registered SLA templates for the selected provider.
     *
     * @return
     */
    @GET
    @Path("/slats")
    @Produces("application/json")
    public Response getSLATemplates() throws Exception {
        log.trace("getSLATemplates() started.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray json = new JSONArray();
            for (SLATemplate slaTemplate : provider.getSLATemplateList()) {
                JSONObject o = new JSONObject();
                o.put("name", slaTemplate.getName());
                o.put("uri", RestUriBuilder.getSlatUri(slaTemplate));
                o.put("url", slaTemplate.getUrl());
                json.put(o);
            }

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Registers a new SLA template at the selected provider.
     *
     * @return
     */
    @POST
    @Path("/slats")
    @Consumes("application/json")
    public Response addSLATemplate(String requestBody) throws Exception {
        if (log.isTraceEnabled()) {
            log.trace("addSLATemplate() started. Data: " + requestBody);
        }

        SLATemplate slaTemplate;
        try {
            JSONObject json = new JSONObject(requestBody);
            slaTemplate = new SLATemplate(json);
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            slaTemplate.setProviderId(provider);

            em.getTransaction().begin();
            em.persist(slaTemplate);
            provider.getSLATemplateList().add(slaTemplate);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", slaTemplate.getSlatId()));
            return Response.created(resourceUri).build();
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    /**
     * Returns all applications running at selected provider
     *
     * @return
     * @throws Exception
     */
    @GET
    @Path("/applications")
    @Produces("application/json")
    public Response getApplications() throws Exception {
        log.trace("getApplications() started.");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            Provider provider = new ProviderDAO(em).findByUuid(providerUuid);
            if (provider == null) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            JSONArray json = new JSONArray();
            for (Application application : provider.getApplicationList()) {
                String uri = String.format("/applications/%d", application.getApplicationId());
                JSONObject o = new JSONObject();
                o.put("uuid", application.getUuid());
                o.put("uri", uri);
                json.put(o);
            }

            return Response.ok(json.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Path("/accounting/historical_metrics_data")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHistoricalMetricsData(@Context HttpServletRequest httpServletRequest,
                                             String data) throws Exception {

        TokenInfo tokenInfo = OAuthFilter.getAccessTokenInfo(httpServletRequest);

        URI uri = new URI("https://contrail.xlab.si:8443/federation-accounting/reports/host_metrics_history");

        OAuthHttpClient oAuthHttpClient = new OAuthHttpClient(
                Conf.getInstance().getOAuthClientKeystoreFile(),
                Conf.getInstance().getOAuthClientKeystorePass(),
                Conf.getInstance().getOAuthClientTruststoreFile(),
                Conf.getInstance().getOAuthClientTruststorePass()
        );

        HttpEntity entity = new StringEntity(data, ContentType.APPLICATION_JSON);

        log.debug("Sending request to federation-accounting " + uri);
        HttpResponse response = oAuthHttpClient.post(uri, tokenInfo.getAccessToken(), entity);
        log.debug("Received response: " + response.getStatusLine());

        int status = response.getStatusLine().getStatusCode();
        String contentType = response.getFirstHeader("Content-Type").getValue();
        String content = oAuthHttpClient.getContent(response);
        Response.ResponseBuilder responseBuilder = Response
                .status(status)
                .type(contentType)
                .entity(content);
        if (response.containsHeader("Location")) {
            String location = response.getFirstHeader("Location").getValue();
            responseBuilder = responseBuilder.header("Location", location);
        }
        return responseBuilder.build();
    }
}
