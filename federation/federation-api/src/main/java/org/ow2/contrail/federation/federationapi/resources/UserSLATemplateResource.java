/**
 *
 */
package org.ow2.contrail.federation.federationapi.resources;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.ow2.contrail.federation.federationapi.MyServletContextListener;
import org.ow2.contrail.federation.federationapi.exceptions.NegotiationException;
import org.ow2.contrail.federation.federationapi.slas.negotiation.*;
import org.ow2.contrail.federation.federationapi.utils.FederationDBCommon;
import org.ow2.contrail.federation.federationapi.utils.JSONObject;
import org.ow2.contrail.federation.federationapi.utils.RestUriBuilder;
import org.ow2.contrail.federation.federationdb.jpa.dao.*;
import org.ow2.contrail.federation.federationdb.jpa.entities.*;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;
import org.slasoi.gslam.syntaxconverter.SLAParser;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.SLA;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ales.cernivec@xlab.si
 */
@Path("/users/{userUuid}/slats/{slatId}")
public class UserSLATemplateResource {

    protected static Logger logger =
            Logger.getLogger(UserSLATemplateResource.class);
    private String userUuid;
    private int slatId;

    public UserSLATemplateResource(@PathParam("userUuid") String userUuid, @PathParam("slatId") int slatId) {
        this.userUuid = userUuid;
        this.slatId = slatId;
    }

    @GET
    @Produces("application/json")
    public Response get() throws Exception {
        logger.debug("Entering get");
        UserSLATemplate userSlat = new UserSLATemplateDAO().findById(slatId);
        if (userSlat == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        JSONObject slat = new JSONObject();
        slat.put("SLATemplateId", RestUriBuilder.getUserSlatUri(userSlat));
        slat.put("url", userSlat.getUrl());
        slat.put("userid", RestUriBuilder.getUserUri(userSlat.getUserId()));
        slat.put("content", userSlat.getContent());
	slat.put("slatId", userSlat.getSlatId());
	slat.put("name", userSlat.getName());
        if (userSlat.getSlatId() != null)
            slat.put("slatId", String.format("/slats/%d", userSlat.getSlatId().getSlatId()));
        else
            slat.put("slatId", JSONObject.NULL);
        slat.put("userSLAs", String.format("/users/%d/slats/%d/slas",
                userSlat.getUserId().getUserId(), userSlat.getSLATemplateId()));
        slat.put("userOvfUri", RestUriBuilder.getUserOvfUri(userSlat.getUserOvf()));
        logger.debug("Exiting get");
        return Response.ok(slat.toString()).build();
    }

    /**
     * Return user slas corresponding to these SLATs.
     */
    @GET
    @Produces("application/json")
    @Path("/slas")
    public Response getSLATsUserSLA() throws Exception {
        logger.debug("Entering get");
        UserSLATemplate userSlat = new UserSLATemplateDAO().findById(slatId);
        if (userSlat == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        JSONArray json = new JSONArray();
        for (UserSLA userSla : userSlat.getUserSLAList()) {
            JSONObject o = new JSONObject();
            o.put("name", userSla.getSla());
            o.put("uri", RestUriBuilder.getUserSlaUri(userSla));
            json.put(o);
        }
        return Response.ok(json.toString()).build();
    }


    @DELETE
    public Response delete() throws Exception {
        logger.debug("Entering delete");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            UserSLATemplate userSlat = new UserSLATemplateDAO(em).findById(slatId);
            if (userSlat == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            UserOvf userOvf = userSlat.getUserOvf();

            logger.debug("Deleting userSLAT");
            em.getTransaction().begin();
            em.remove(userSlat);
            userOvf.getUserSLATemplateList().remove(userSlat);
            if (userOvf.getUserSLATemplateList().size() == 0) {
                em.remove(userOvf);
            }
            em.getTransaction().commit();

            logger.debug("Exiting delete");
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public Response put(JSONObject slatdata) throws Exception {
        logger.debug("Entering put");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            UserSLATemplate userSlat = new UserSLATemplateDAO(em).findById(slatId);
            if (userSlat == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            em.getTransaction().begin();

            if (slatdata.has("url")) {
                userSlat.setUrl(slatdata.getString("url"));
            }
            if (slatdata.has("slatId")) {
                SLATemplate slat = new SLATemplateDAO(em).findById(
                        FederationDBCommon.getIdFromString(slatdata.getString("slatId")));
                if (slat != null) {
                    logger.debug("Found SLATemplate with id " + slat.getSlatId());
                    SLATemplate slatBefore = userSlat.getSlatId();
                    if (slatBefore != null) {
                        logger.debug("Removing UserSLATemplate from SLATemplate" + slatBefore.getSlatId());
                        slatBefore.getUserSLATemplateList().remove(userSlat);
                    }
                    userSlat.setSlatId(slat);
                    slat.getUserSLATemplateList().add(userSlat);
                }
                else {
                    logger.error("Could not found SLATemplate with id: " + slatdata.getString("slatId"));
                    return Response.status(Response.Status.BAD_REQUEST).build();
                }
            }

            if (slatdata.has("content")) {
                userSlat.setContent(slatdata.getString("content"));
            }
            logger.debug("merging UserSLATemplate");

            em.getTransaction().commit();

            logger.debug("Exiting put");
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Produces("application/json")
    @Path("negotiation/initiate")
    public Response initiateNegotiation(@PathParam("slatId") int slatID) {
        logger.debug("Entering initiateNegotiation");
        UserSLATemplate slat = new UserSLATemplateDAO().findById(slatID);
        if (slat == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        try {
            INegotiationClient negotiationClient = NegotiationClientFactory.create();
            String negotiationId = negotiationClient.initiateNegotiation(slat.getContent());
            JSONObject response = new JSONObject();
            response.put("negotiationId", negotiationId);
            logger.debug("Exiting initiateNegotiation");
            return Response.ok(response.toString()).build();
        }
        catch (NegotiationException e) {
            logger.error(FederationDBCommon.getStackTrace(e));
            logger.error("initiateNegotiation() failed.", e);
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        catch (Exception e) {
            logger.error(FederationDBCommon.getStackTrace(e));
            logger.error("initiateNegotiation() failed.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
    }

    @POST
    @Produces("application/json")
    @Path("negotiation/{negotiationId}/cancel")
    public Response cancel(@PathParam("slatId") int slatId, @PathParam("negotiationId") String negotiationId,
                           JSONObject content) {
        logger.debug("Entering cancel");
        try {
            List<String> cancellationReason = null;
            if (content != null) {
                if (content.has("cancellationReason")) {
                    JSONArray cancellationReasonArr = content.getJSONArray("cancellationReason");
                    if (cancellationReasonArr.length() > 0) {
                        cancellationReason = new ArrayList<String>();
                        for (int i = 0; i < cancellationReasonArr.length(); i++) {
                            cancellationReason.add(cancellationReasonArr.getString(i));
                        }
                    }
                }
            }

            INegotiationClient negotiationClient = NegotiationClientFactory.create();
            boolean status = negotiationClient.cancel(negotiationId, cancellationReason);
            JSONObject response = new JSONObject();
            response.put("status", status);
            return Response.ok(response.toString()).build();
        }
        catch (JSONException e) {
            logger.error(FederationDBCommon.getStackTrace(e));
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        catch (NegotiationException e) {
            logger.error(FederationDBCommon.getStackTrace(e));
            logger.error("cancel() failed.", e);
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        catch (Exception e) {
            logger.error(FederationDBCommon.getStackTrace(e));
            logger.error("cancel() failed.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
    }

    @POST
    @Produces("application/json")
    @Path("negotiation/{negotiationId}/proposals/{proposalId}/negotiate")
    public Response negotiate(@PathParam("slatId") int slatId, @PathParam("negotiationId") String negotiationId,
                              @PathParam("proposalId") int proposalId) {
        logger.debug("Entering negotiate");

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            UserSLATemplate slat = new UserSLATemplateDAO(em).findById(slatId);
            if (slat == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            String customerSlat;
            if (proposalId == 0) {
                customerSlat = slat.getContent();
            }
            else {
                SLATemplateProposal proposal = new SLATemplateProposalDAO(em).find(slatId, negotiationId, proposalId);
                if (proposal == null) {
                    return Response.status(Response.Status.NOT_FOUND).build();
                }

                customerSlat = proposal.getContent();
            }
            INegotiationClient negotiationClient = NegotiationClientFactory.create();
            String[] slatXmlProposals = negotiationClient.negotiate(negotiationId, customerSlat);

            // persist SLAT proposals sent by the provider
            List<SLATemplateProposal> proposals = new ArrayList<SLATemplateProposal>();
            em.getTransaction().begin();
            for (String slatXmlProposal : slatXmlProposals) {
                SLATemplateProposal proposal = new SLATemplateProposal();
                proposal.setContent(slatXmlProposal);
                proposal.setCreated(new Date());
                proposal.setUserSLATemplate(slat);
                proposal.setNegotiationId(negotiationId);
                em.persist(proposal);
                proposals.add(proposal);
            }
            em.getTransaction().commit();

            // create response
            JSONObject response = new JSONObject();
            JSONArray array = new JSONArray();
            for (SLATemplateProposal proposal : proposals) {
                JSONObject proposalInfo = new JSONObject();
                proposalInfo.put("content", proposal.getContent());
                proposalInfo.put("created", proposal.getCreated());
                proposalInfo.put("userSLATemplateId", proposal.getUserSLATemplate().getSLATemplateId());
                String proposalUri = String.format("%s/negotiation/%s/proposals/%d",
                        RestUriBuilder.getUserSlatUri(slat), negotiationId, proposal.getProposalId());
                proposalInfo.put("proposalUri", proposalUri);
                array.put(proposalInfo);
            }
            logger.debug("Exiting negotiate");
            response.put("slatProposals", array);
            return Response.ok(response.toString()).build();
        }
        catch (NegotiationException e) {
            logger.error(FederationDBCommon.getStackTrace(e));
            logger.error("negotiate() failed.", e);
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        catch (Exception e) {
            logger.error(FederationDBCommon.getStackTrace(e));
            logger.error("negotiate() failed.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("negotiation/{negotiationId}/proposals/{proposalId}")
    public Response getSlatProposal(@PathParam("slatId") int slatId, @PathParam("negotiationId") String negotiationId,
                                    @PathParam("proposalId") int proposalId) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            UserSLATemplate slat = new UserSLATemplateDAO(em).findById(slatId);
            if (slat == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            if (proposalId == 0) {
                URI slatUri = new URI(RestUriBuilder.getUserSlatUri(slat));
                return Response.seeOther(slatUri).build();
            }

            SLATemplateProposal proposal = new SLATemplateProposalDAO(em).find(slatId, negotiationId, proposalId);
            if (proposal == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            SLATComparator comparator = new SLATComparator();
            SLATAbstract slatAbstract = comparator.parseSLAT(proposal.getContent());
            org.json.JSONObject json = proposal.toJSON();
            json.put("slatAbstract", slatAbstract.toJSON());

            return Response.ok(json.toString()).build();
        }
        catch (Exception e) {
            logger.error("getSlatProposal() failed.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Consumes("application/json")
    @Path("negotiation/{negotiationId}/proposals/{proposalId}/createAgreement")
    public Response createAgreement(@PathParam("slatId") int slatId, @PathParam("negotiationId") String negotiationId,
                                    @PathParam("proposalId") int proposalId, JSONObject content) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            String appUuid;
            try {
                appUuid = content.getString("appUuid");
            }
            catch (JSONException e) {
                return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
            }

            Application application = new ApplicationDAO(em).findByUuid(appUuid);
            if (application == null || !user.getApplicationList().contains(application)) {
                throw new WebApplicationException(Response.Status.NOT_FOUND);
            }

            boolean status = MyServletContextListener.getFederationCore()
                    .createAgreement(user, application, proposalId);

            if (!status) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity("Failed to create agreement.").build();
            }

            em.refresh(application);
            JSONObject appAttributes = new JSONObject(application.getAttributes());
            if (!appAttributes.has("slaId"))
                throw new Exception("Missing attribute 'slaId' in application attributes.");
            int slaId = appAttributes.getInt("slaId");
            UserSLA userSLA = new UserSLADAO(em).findById(slaId);
            if (userSLA == null) {
                throw new Exception(String.format("SLA with ID %d doesn't exist.", slaId));
            }

            // parse providerId from the SLA
            Provider provider;
            try {
                SLAParser slaParser = new SLASOIParser();
                SLA sla = slaParser.parseSLA(userSLA.getContent());
                // ProviderUUid property in SLA should be provider's UUID but is in fact provider's ID
                int providerId = Integer.parseInt(sla.getPropertyValue(new STND("ProviderUUid")));
                provider = new ProviderDAO(em).findById(providerId);
                if (provider == null) {
                    throw new Exception("No such provider: " + providerId);
                }
            }
            catch (Exception e) {
                throw new Exception("Failed to parse providerId from the SLA with ID " + userSLA.getSLAId());
            }

            // store providerId to the application's attributes
            appAttributes.put("providerId", provider.getUuid());
            em.getTransaction().begin();
            application.setAttributes(appAttributes.toString());
            em.getTransaction().commit();

            JSONObject respJson = new JSONObject();
            respJson.put("slaId", userSLA.getSLAId());

            URI slaUri = new URI(RestUriBuilder.getUserSlaUri(userSLA));
            return Response.created(slaUri).entity(respJson.toString()).build();
        }
        catch (Exception e) {
            logger.error("Failed to create SLA agreement: " + e.getMessage(), e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Failed to create SLA agreement: " + e.getMessage()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("negotiation/{negotiationId}/proposals")
    public Response getSlatProposals(@PathParam("slatId") int slatId, @PathParam("negotiationId") String negotiationId)
            throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            List<SLATemplateProposal> proposals = new SLATemplateProposalDAO(em).findByNegotiationId(negotiationId);
            JSONArray proposalsArray = new JSONArray();
            for (SLATemplateProposal proposal : proposals) {
                org.json.JSONObject o = new org.json.JSONObject();
                o.put("proposalId", proposal.getProposalId());
                String proposalUri = String.format("%s/negotiation/%s/proposals/%d",
                        RestUriBuilder.getUserSlatUri(proposal.getUserSLATemplate()),
                        negotiationId, proposal.getProposalId());
                o.put("uri", proposalUri);
                proposalsArray.put(o);
            }

            return Response.ok(proposalsArray.toString()).build();
        }
        catch (Exception e) {
            logger.error("getSlatProposals() failed.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("negotiation/{negotiationId}/proposals/{proposalId}/compare")
    public Response compareProposal(@PathParam("slatId") int slatId, @PathParam("negotiationId") String negotiationId,
                                    @PathParam("proposalId") int proposalId) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            UserSLATemplate slat = new UserSLATemplateDAO(em).findById(slatId);
            if (slat == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            SLATemplateProposal proposal = new SLATemplateProposalDAO(em).find(slatId, negotiationId, proposalId);
            if (proposal == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            SLATComparator slatComparator = new SLATComparator();
            SLATComparison comparison = slatComparator.compareSLATs(slat.getContent(), proposal.getContent());

            return Response.ok(comparison.toJSON().toString()).build();
        }
        catch (Exception e) {
            logger.error("compareProposal() failed.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Produces("application/json")
    @Path("negotiation/{negotiationId}/proposals/{proposalId}/getTreeGridComparison")
    public Response getTreeGridComparison(@PathParam("slatId") int slatId, @PathParam("negotiationId") String negotiationId,
                                    @PathParam("proposalId") int proposalId) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            UserSLATemplate slat = new UserSLATemplateDAO(em).findById(slatId);
            if (slat == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            SLATemplateProposal proposal = new SLATemplateProposalDAO(em).find(slatId, negotiationId, proposalId);
            if (proposal == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            SLATComparator slatComparator = new SLATComparator();
            SLATComparison comparison = slatComparator.compareSLATs(slat.getContent(), proposal.getContent());
            org.json.JSONObject treeGridData = slatComparator.getSLATComparisonTreeGrid(comparison);

            return Response.ok(treeGridData.toString()).build();
        }
        catch (Exception e) {
            logger.error("compareProposal() failed.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @PUT
    @Consumes("application/json")
    @Path("negotiation/{negotiationId}/proposals/{proposalId}")
    public Response updateProposal(@PathParam("slatId") int slatId, @PathParam("negotiationId") String negotiationId,
                                   @PathParam("proposalId") int proposalId, JSONObject content) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            SLATemplateProposal proposal = new SLATemplateProposalDAO(em).find(slatId, negotiationId, proposalId);
            if (proposal == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            SLATComparator slatComparator = new SLATComparator();
            String updatedSlatXml = slatComparator.updateSLAT(proposal.getContent(), content);

            em.getTransaction().begin();
            proposal.setContent(updatedSlatXml);
            em.getTransaction().commit();

            return Response.status(Response.Status.NO_CONTENT).build();
        }
        catch (JSONException e) {
            logger.error(FederationDBCommon.getStackTrace(e));
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        catch (Exception e) {
            logger.error(FederationDBCommon.getStackTrace(e));
            logger.error("updateProposal() failed.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Consumes("application/json")
    @Path("negotiation/{negotiationId}/proposals/{proposalId}/adapt")
    public Response adaptProposal(@PathParam("slatId") int slatId, @PathParam("negotiationId") String
            negotiationId, @PathParam("proposalId") int proposalId, JSONObject content) throws Exception {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            User user = new UserDAO(em).findByUuid(userUuid);
            if (user == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            UserSLATemplate baseSlat = new UserSLATemplateDAO(em).findById(slatId);
            if (baseSlat == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            SLATemplateProposal proposal = new SLATemplateProposalDAO(em).find(slatId, negotiationId, proposalId);
            if (proposal == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            SLATComparator slatComparator = new SLATComparator();
            String newSlatXml = slatComparator.updateSLATFromTreeGrid(proposal.getContent(), content);

            UserSLATemplate newSlat = new UserSLATemplate();
            newSlat.setContent(newSlatXml);
            newSlat.setName(baseSlat.getName());
            newSlat.setUserId(user);
            newSlat.setSlatId(baseSlat.getSlatId());
            newSlat.setUserOvf(baseSlat.getUserOvf());

            em.getTransaction().begin();
            em.persist(newSlat);
            em.getTransaction().commit();

            // set new SLAT name
            em.getTransaction().begin();
            newSlat.setName(newSlat.getName() + " v" + newSlat.getSLATemplateId());
            em.getTransaction().commit();

            JSONObject result = new JSONObject();
            result.put("slatId", newSlat.getSLATemplateId());
            String uri = RestUriBuilder.getUserSlatUri(newSlat);
            return Response.status(Response.Status.CREATED).header("Location", uri).entity(result.toString()).build();
        }
        catch (Exception e) {
            logger.error("adaptProposal failed: " + e.getMessage(), e);
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Produces("application/json")
    @Path("negotiation/{negotiationId}/proposals/{proposalId}/createAgreement")
    public Response createAgreement(@PathParam("slatId") int slatId, @PathParam("negotiationId") String negotiationId,
                                    @PathParam("proposalId") int proposalId) {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();

        try {
            UserSLATemplate slat = new UserSLATemplateDAO(em).findById(slatId);
            if (slat == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            SLATemplateProposal proposal = new SLATemplateProposalDAO(em).find(slatId, negotiationId, proposalId);
            if (proposal == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            INegotiationClient negotiationClient = NegotiationClientFactory.create();
            String slaXml = negotiationClient.createAgreement(negotiationId, proposal.getContent());

            // store SLA
            UserSLA sla = new UserSLA();
            sla.setContent(slaXml);
            sla.setName(slat.getName());
            sla.setSla(slat.getName());
            //sla.setTemplateURL(slat.getUrl());
            // This is ugly hack but at this point necessary!
            sla.setTemplateURL(slat.getSlatId().getProviderId().getProviderId().toString());
            sla.setSLATemplateId(slat);
            sla.setUserId(slat.getUserId());

            em.getTransaction().begin();
            em.persist(sla);
            em.getTransaction().commit();

            String slaUri = RestUriBuilder.getUserSlaUri(sla);

            JSONObject response = new JSONObject();
            response.put("slaContent", slaXml);
            response.put("slaId", sla.getSLAId());
            response.put("slaUri", slaUri);
            return Response.ok(response.toString()).build();
        }
        catch (NegotiationException e) {
            logger.error(FederationDBCommon.getStackTrace(e));
            logger.error("createAgreement() failed.", e);
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        catch (Exception e) {
            logger.error(FederationDBCommon.getStackTrace(e));
            logger.error("createAgreement() failed.", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
