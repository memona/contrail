package org.ow2.contrail.federation.federationapi.tests.providers;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.federation.federationapi.MyServletContextListener;
import org.ow2.contrail.federation.federationapi.core.impl.FederationCoreBasic;
import org.ow2.contrail.federation.federationapi.resources.IClusterResource;
import org.ow2.contrail.federation.federationapi.resources.IProviderResource;
import org.ow2.contrail.federation.federationapi.resources.ProvidersResource;
import org.ow2.contrail.federation.federationapi.tests.Utils;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import static junit.framework.Assert.assertEquals;

public class AssignResourceTest {
    private static Logger log = Logger.getLogger(AssignResourceTest.class);

    @Before
    public void setUp() {
        PersistenceUtils.createInstance("testPU");
        MyServletContextListener.setFederationCore(new FederationCoreBasic());
    }

    @Test
    public void testAssignServer() throws Exception {
        log.info("testAssignServer() started.");
        ProvidersResource providersResource = new ProvidersResource();
        Response response;

        // register provider A
        JSONObject providerAJson = new JSONObject();
        providerAJson.put("name", "ProviderA");
        providerAJson.put("location", "SI");
        providerAJson.put("email", "info@providerA.com");
        providerAJson.put("typeId", 3);
        providerAJson.put("providerUri", "contrail://providerA");
        response = providersResource.addProvider(providerAJson.toString());
        assertEquals(response.getStatus(), 201);
        assertEquals(response.getMetadata().get("Location").get(0).toString(), "/1");

        // register provider B
        JSONObject providerBJson = new JSONObject();
        providerBJson.put("name", "ProviderB");
        providerBJson.put("location", "SI");
        providerBJson.put("email", "info@providerB.com");
        providerBJson.put("typeId", 2);
        providerBJson.put("providerUri", "contrail://providerB");
        response = providersResource.addProvider(providerBJson.toString());
        assertEquals(response.getStatus(), 201);
        assertEquals(response.getMetadata().get("Location").get(0).toString(), "/2");

        // get registered providers list
        response = providersResource.getProviders();
        assertEquals(response.getStatus(), 200);
        JSONArray array = new JSONArray((String) response.getEntity());
        assertEquals(array.getJSONObject(0).getString("name"), "ProviderA");
        assertEquals(array.getJSONObject(1).getString("name"), "ProviderB");

        // get provider A
        IProviderResource providerAResource = providersResource.findProvider(1);
        response = providerAResource.getProvider();
        assertEquals(response.getStatus(), 200);
        JSONObject o = new JSONObject((String) response.getEntity());
        assertEquals(o.getString("name"), "ProviderA");
        assertEquals(o.getString("location"), "SI");
        assertEquals(o.getString("email"), "info@providerA.com");
        assertEquals(o.getInt("typeId"), 3);
        assertEquals(o.getString("providerUri"), "contrail://providerA");

        // get provider B
        IProviderResource providerBResource = providersResource.findProvider(2);

        // add some servers
        providerAResource.addServer("{'name':'server1'}");
        providerAResource.addServer("{'name':'server2'}");
        providerAResource.addServer("{'name':'server3'}");
        providerAResource.addServer("{'name':'server4'}");
        providerBResource.addServer("{'name':'server5'}");

        // add some clusters
        response = providerAResource.addCluster("{'name':'clusterA1'}");
        IClusterResource clusterA1Resource = providerAResource.findCluster(extractResourceId(response));
        response = providerAResource.addCluster("{'name':'clusterA2'}");
        IClusterResource clusterA2Resource = providerAResource.findCluster(extractResourceId(response));
        response = providerBResource.addCluster("{'name':'clusterB1'}");
        IClusterResource clusterB1Resource = providerBResource.findCluster(extractResourceId(response));

        // link servers to clusters
        response = clusterA1Resource.registerServer("{'serverURI':'/providers/1/servers/1'}");
        assertEquals(response.getStatus(), 201);
        response = clusterA1Resource.registerServer("{'serverURI':'/providers/1/servers/2'}");
        assertEquals(response.getStatus(), 201);
        response = clusterA2Resource.registerServer("{'serverURI':'/providers/1/servers/3'}");
        assertEquals(response.getStatus(), 201);
        response = clusterB1Resource.registerServer("{'serverURI':'/providers/2/servers/5'}");
        assertEquals(response.getStatus(), 201);

        // link again
        response = clusterA1Resource.registerServer("{'serverURI':'/providers/1/servers/1'}");
        assertEquals(response.getStatus(), 201);

        try {
            clusterA1Resource.registerServer("{'key':'value'}");
        }
        catch (Exception e) {
            assertEquals(((WebApplicationException) e).getResponse().getStatus(), 400);
        }
        try {
            clusterA1Resource.registerServer("{'serverURI':'invalid uri'}");
        }
        catch (Exception e) {
            assertEquals(((WebApplicationException) e).getResponse().getStatus(), 400);
        }
        try {
            clusterA1Resource.registerServer("{'serverURI':'/providers/1/servers/5'}");
        }
        catch (Exception e) {  // server 5 belongs to provider B
            assertEquals(((WebApplicationException) e).getResponse().getStatus(), 400);
        }

        // get registered servers
        response = clusterA1Resource.getServers();
        array = new JSONArray((String) response.getEntity());
        assertEquals(array.length(), 2);
        assertEquals(array.getJSONObject(0).getString("baseUri"), "/providers/1/servers/1");
        assertEquals(array.getJSONObject(0).getString("uri"), "/providers/1/clusters/1/servers/1");
        assertEquals(array.getJSONObject(1).getString("baseUri"), "/providers/1/servers/2");
        assertEquals(array.getJSONObject(1).getString("uri"), "/providers/1/clusters/1/servers/2");

        response = clusterA2Resource.getServers();
        array = new JSONArray((String) response.getEntity());
        assertEquals(array.length(), 1);
        assertEquals(array.getJSONObject(0).getString("baseUri"), "/providers/1/servers/3");
        assertEquals(array.getJSONObject(0).getString("uri"), "/providers/1/clusters/2/servers/3");

        // get server registration data
        response = clusterA1Resource.getServerRegistration(2);
        o = new JSONObject((String) response.getEntity());
        assertEquals(o.getString("baseUri"), "/providers/1/servers/2");
        assertEquals(o.getString("uri"), "/providers/1/clusters/1/servers/2");

        // unlink server
        response = clusterA1Resource.unregisterServer(1);
        assertEquals(response.getStatus(), 204);

        try {
            clusterA1Resource.getServerRegistration(1);
        }
        catch (Exception e) {
            assertEquals(((WebApplicationException) e).getResponse().getStatus(), 404);
        }

        try {
            clusterA1Resource.unregisterServer(1); // try to unlink again
        }
        catch (Exception e) {
            assertEquals(((WebApplicationException) e).getResponse().getStatus(), 404);
        }

        // get registered servers
        response = clusterA1Resource.getServers();
        array = new JSONArray((String) response.getEntity());
        assertEquals(array.length(), 1);

        log.info("testAssignServer() finished successfully.");
    }

    private int extractResourceId(Response response) {
        return Integer.parseInt(
                response.getMetadata().get("Location").get(0).toString().substring(1));
    }

    @After
    public void tearDown() throws Exception {
        PersistenceUtils.getInstance().close();
        Utils.dropTestDatabase();
    }
}
