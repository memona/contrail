package org.ow2.contrail.federation.federationapi.slas.negotiation;

import org.json.JSONObject;
import org.junit.Test;

import java.io.File;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class SLATComparatorTest {

    @Test
    public void testCompareSLATs() throws java.lang.Exception {
        SLATComparator slatComparator = new SLATComparator();
        String slat1 = new Scanner(
                new File("src/test/resources/ContrailSLAtemplateExampleAll-1.xml")).useDelimiter("\\Z").next();
        String slat2 = new Scanner(
                new File("src/test/resources/ContrailSLAtemplateExampleAll-2.xml")).useDelimiter("\\Z").next();

        SLATComparison comparison = slatComparator.compareSLATs(slat1, slat2);
        System.out.println(comparison.toJSON().toString(2));
        System.out.println();
        JSONObject treeGridData = slatComparator.getSLATComparisonTreeGrid(comparison);
        System.out.println(treeGridData.toString(2));
    }

    @Test
    public void testDeserializeSLATAbstract() throws Exception {
        String slatAbstractJson = new Scanner(
                new File("src/test/resources/ContrailSLAtemplate1-slat-abstract.json")).useDelimiter("\\Z").next();
        JSONObject o = new JSONObject(slatAbstractJson);
        SLATAbstract slatAbstract = new SLATAbstract(o);

        assertEquals(slatAbstract.getAgreementTerm("all-in-one").getGuarantee("MEMORY_STATE").getThreshold()
                .getValue(), "1024");
    }

    @Test
    public void testUpdateSLAT() throws Exception {
        String slat1Xml = new Scanner(
                new File("src/test/resources/ContrailSLAtemplateExampleAll-1.xml")).useDelimiter("\\Z").next();
        String slatAbstractJson = new Scanner(
                new File("src/test/resources/ContrailSLAtemplate1-slat-abstract.json")).useDelimiter("\\Z").next();

        SLATComparator slatComparator = new SLATComparator();
        String slat2Xml = slatComparator.updateSLAT(slat1Xml, new JSONObject(slatAbstractJson));

        SLATComparison comparison = slatComparator.compareSLATs(slat1Xml, slat2Xml);

        // MEMORY_STATE
        SLATComparison.GuaranteedState guarantee1 =
                comparison.getAgreementTerm("all-in-one").getGuarantee("MEMORY_STATE");
        assertEquals(guarantee1.getThreshold1().getValue(), "256");
        assertEquals(guarantee1.getThreshold2().getValue(), "1024");

        // CPU_CORES_STATE
        SLATComparison.GuaranteedState guarantee2 =
                comparison.getAgreementTerm("all-in-one").getGuarantee("CPU_CORES_STATE");
        assertEquals(guarantee2.getThreshold1().getValue(), "1");
        assertEquals(guarantee2.getThreshold2().getValue(), "4");
    }

    @Test
    public void testUpdateSLATFromTreeGrid() throws Exception {
        String slat1Xml = new Scanner(
                new File("src/test/resources/user-slat-1.xml")).useDelimiter("\\Z").next();
        String diff = "{\"modified\":[{\"id\":\"CPU_CORES_for_VirtualSystem2\",\"type\":2,\"modified\":{\"thresholdOld\":\"4\"}},{\"id\":\"MEMORY_for_VirtualSystem2\",\"type\":2,\"modified\":{\"thresholdOld\":\"8192 MB\"}}],\"removed\":[{\"id\":\"VirtualSystem1_Guarantees\",\"type\":1},{\"id\":\"CPU_SPEED_for_VirtualSystem2\",\"type\":2}]}";

        SLATComparator slatComparator = new SLATComparator();
        String slat2Xml = slatComparator.updateSLATFromTreeGrid(slat1Xml, new JSONObject(diff));

        SLATComparison comparison = slatComparator.compareSLATs(slat1Xml, slat2Xml);
        System.out.println(comparison.toJSON().toString(2));
    }
}
