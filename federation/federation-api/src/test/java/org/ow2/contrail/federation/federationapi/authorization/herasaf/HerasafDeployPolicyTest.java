package org.ow2.contrail.federation.federationapi.authorization.herasaf;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.ow2.contrail.federation.federationapi.authorization.Action;
import org.ow2.contrail.federation.federationapi.authorization.Authorizer;
import org.ow2.contrail.federation.federationapi.resources.AuthResource;
import org.ow2.contrail.federation.federationapi.resources.AuthRuleResource;
import org.ow2.contrail.federation.federationapi.tests.Utils;
import org.ow2.contrail.federation.federationdb.jpa.dao.UserDAO;
import org.ow2.contrail.federation.federationdb.jpa.entities.URole;
import org.ow2.contrail.federation.federationdb.jpa.entities.User;
import org.ow2.contrail.federation.federationdb.utils.PersistenceUtils;
import org.ow2.contrail.federation.herasafauthorizer.Rule;
import org.ow2.contrail.federation.herasafauthorizer.Subject;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.ContextLoaderListener;

import javax.persistence.EntityManager;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class HerasafDeployPolicyTest {
    private static Logger log = Logger.getLogger(HerasafAuthorizerTest.class);
    private Authorizer authorizer;

    @org.junit.Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Before
    public void setUp() throws Exception {
        PersistenceUtils.createInstance("testPU");

        MockServletContext sc = new MockServletContext("");
        sc.addInitParameter(ContextLoader.CONFIG_LOCATION_PARAM,
                "/herasaf-authorizer-test-context.xml");
        ServletContextListener listener = new ContextLoaderListener();
        ServletContextEvent event = new ServletContextEvent(sc);
        listener.contextInitialized(event);

        ApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        authorizer = (Authorizer) ctx.getBean("authorizer");

        User user = new User();
        user.setEmail("test@contrail.com");
        user.setUuid("68bcb06e-446f-43a6-9a3a-d9e915bc8480");

        URole role = new URole();
        role.setName("FederationUser");
        List<URole> roleList = new ArrayList<URole>();
        roleList.add(role);
        user.setURoleList(roleList);

        EntityManager em = null;
        try {
            em = PersistenceUtils.getInstance().getEntityManager();
            em.getTransaction().begin();
            em.persist(role);
            em.persist(user);
            em.getTransaction().commit();
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Test
    public void testDeploy() throws Exception {
        log.trace("testDeploy() started.");

        User user = UserDAO.findByUuid("68bcb06e-446f-43a6-9a3a-d9e915bc8480");

        // access should be denied
        assertFalse(
                authorizer.isAuthorized(user, "/providers/12/servers/154", Action.READ));

        // deploy new authorization rule
        AuthResource authResource = new AuthResource();
        Response response;

        response = authResource.getUserRules(user.getUuid());
        assertEquals(response.getStatus(), 200);
        assertEquals(response.getEntity(), "[]");

        String[] resourceURIs = new String[]{"/providers/12/servers/154", "/providers/12/servers/155",
                "/providers/12/servers/156"};
        String[] actions = new String[]{Action.READ.name()};
        Date now = new Date();

        Rule rule = new Rule();
        rule.setDescription("Rule 1");
        rule.setSubject(new Subject(Subject.Type.USER, "68bcb06e-446f-43a6-9a3a-d9e915bc8480"));
        rule.setResourceURIs(Arrays.asList(resourceURIs));
        rule.setActions(Arrays.asList(actions));
        rule.setStartTime(new Date(now.getTime() - 3600 * 1000));
        rule.setEndTime(new Date(now.getTime() + 3600 * 1000));

        response = authResource.addUserRule(user.getUuid(), rule.toJSON());
        assertEquals(response.getStatus(), 201);

        assertTrue(
                authorizer.isAuthorized(user, "/providers/12/servers/154", Action.READ));
        assertTrue(
                authorizer.isAuthorized(user, "/providers/12/servers/155", Action.READ));
        assertFalse(
                authorizer.isAuthorized(user, "/providers/12/servers/156", Action.WRITE));

        assertTrue(
                authorizer.isAuthorized(user, "/providers/12/servers/154/a", Action.READ));
        assertTrue(
                authorizer.isAuthorized(user, "/providers/12/servers/154/a/b", Action.READ));
        assertTrue(
                authorizer.isAuthorized(user, "/providers/12/servers/154/a/b/c", Action.READ));
        assertTrue(
                authorizer.isAuthorized(user, "/providers/12/servers/154/a/b/c/", Action.READ));

        // access should be denied
        assertFalse(
                authorizer.isAuthorized(user, "/providers/12/servers/154", Action.WRITE));

        // get rules
        response = authResource.getUserRules(user.getUuid());
        assertEquals(response.getStatus(), 200);
        /*Gson gson = JsonUtils.getInstance().getGson();
        Type collectionType = new TypeToken<List<Rule>>() {
        }.getType();
        List<Rule> ruleList = gson.fromJson((String) response.getEntity(), collectionType);*/
        JSONArray arr = new JSONArray(response.getEntity().toString());
        String ruleId = arr.getJSONObject(0).getString("id");
        assertNotNull(ruleId);

        // update rule
        resourceURIs = new String[]{"/providers/12/servers/154", "/providers/12/servers/160"};
        actions = new String[]{Action.READ.name(), Action.WRITE.name()};
        rule.setResourceURIs(Arrays.asList(resourceURIs));
        rule.setActions(Arrays.asList(actions));

        AuthRuleResource authRuleResource = authResource.findRule(ruleId);
        authRuleResource.updateRule(rule.toJSON());

        // check updated rule
        assertTrue(
                authorizer.isAuthorized(user, "/providers/12/servers/160", Action.READ));
        assertTrue(
                authorizer.isAuthorized(user, "/providers/12/servers/160", Action.WRITE));

        assertFalse(
                authorizer.isAuthorized(user, "/providers/12/servers/155", Action.READ));

        // remove rule
        authRuleResource = authResource.findRule(ruleId);
        authRuleResource.deleteRule();

        // check if rule was removed
        try {
            authRuleResource = authResource.findRule(ruleId);
            fail();
        }
        catch (WebApplicationException e) {
            assertEquals(e.getResponse().getStatus(), 404);
        }

        assertFalse(
                authorizer.isAuthorized(user, "/providers/12/servers/160", Action.WRITE));

        log.trace("testDeploy() finished successfully.");
    }

    @After
    public void tearDown() throws Exception {
        PersistenceUtils.getInstance().close();
        Utils.dropTestDatabase();
    }
}
