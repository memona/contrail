package org.ow2.contrail.federation.accounting.rest;

import com.mongodb.*;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.federation.accounting.jobs.AppMetricHistoryJob;
import org.ow2.contrail.federation.accounting.jobs.JobStatus;
import org.ow2.contrail.federation.accounting.scheduler.Scheduler;
import org.ow2.contrail.federation.accounting.scheduler.SchedulerFactory;
import org.ow2.contrail.federation.accounting.utils.Conf;
import org.ow2.contrail.federation.accounting.utils.MongoFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/reports/metrics_history/{jobId}")
public class MetricsHistoryReportResource {

    private String jobId;
    private AppMetricHistoryJob job;
    private DBObject report;

    public MetricsHistoryReportResource(@PathParam("jobId") String jobId) {
        this.jobId = jobId;

        Mongo mongo = MongoFactory.getMongo();
        DB mongoDB = mongo.getDB(Conf.getInstance().getMongoDBDatabase());
        DBCollection jobsColl = mongoDB.getCollection(Conf.MONGO_JOBS_COLLECTION);
        BasicDBObject searchQuery = new BasicDBObject().append("jobId", jobId);
        report = jobsColl.findOne(searchQuery);
        if (report == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        if (! report.get("type").equals(AppMetricHistoryJob.class.getSimpleName())) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("status")
    public JSONObject getStatus() throws JSONException {

        JSONObject json = new JSONObject();

        if (!report.get("status").equals(JobStatus.SUCCESS.name())) {
            Scheduler scheduler = SchedulerFactory.getScheduler();
            try {
                job = (AppMetricHistoryJob) scheduler.getJob(jobId);
            }
            catch (ClassCastException e) {
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }
            json.put("jobStatus", job.getJobStatus());
            json.put("details", job.getDetailedStatus());
        }
        else {
            json.put("jobStatus", report.get("status"));
        }

        return json;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("providers/{providerUuid}/metrics/{group}.{name}")
    public JSONObject getMetricHistory(@PathParam("providerUuid") String providerUuid,
                                       @PathParam("sid") String sid,
                                       @PathParam("group") String group,
                                       @PathParam("name") String name) throws JSONException {

        JSONObject o = new JSONObject();
        o.put("jobStatus", job.getJobStatus());
        o.put("details", job.getDetailedStatus());
        return o;
    }
}
