package org.ow2.contrail.federation.accounting.jobs;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.federation.accounting.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CountDownLatch;

public class AppMetricHistoryJob extends Job {
    private static Logger log = LoggerFactory.getLogger(AppMetricHistoryJob.class);

    private String appUuid;
    private List<String> metrics;
    private Date startTime;
    private Date endTime;
    private int maxNumberOfIntervals;
    private DetailedStatus detailedStatus;

    public AppMetricHistoryJob(String appUuid, List<String> metrics) {
        this.appUuid = appUuid;
        this.metrics = metrics;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getMaxNumberOfIntervals() {
        return maxNumberOfIntervals;
    }

    public void setMaxNumberOfIntervals(int maxNumberOfIntervals) {
        this.maxNumberOfIntervals = maxNumberOfIntervals;
    }

    @Override
    public DBObject export() throws Exception {
        DBObject jobData = super.export();
        jobData.put("appUuid", appUuid);
        BasicDBList metricsArray = new BasicDBList();
        for (String metric : metrics) {
            metricsArray.add(metric);
        }
        jobData.put("metrics", metricsArray);
        jobData.put("startTime", startTime);
        jobData.put("endTime", endTime);
        jobData.put("maxNumberOfIntervals", maxNumberOfIntervals);
        return jobData;
    }

    @Override
    public JSONObject getDetailedStatus() throws JSONException {
        return detailedStatus.toJson();
    }

    @Override
    public void run() {
        log.debug("Job {} started.", jobId);
        jobStatus = JobStatus.RUNNING;
        jobStartTime = new Date();

        try {
            // TODO: get providers where app was running
            String[] providerUuids = {"provider1", "provider2"};

            // init detailed status
            detailedStatus = new DetailedStatus(providerUuids);

            // start HistoryRetriever threads
            CountDownLatch latch = new CountDownLatch(providerUuids.length);
            for (String providerUuid : providerUuids) {
                // TODO: find out hosts where the application was running
                String[] sids = {"n0001-xc2-xlab-lan", "n0002-xc2-xlab-lan", "n0003-xc2-xlab-lan"};
                HistoryRetriever historyRetriever = new HistoryRetriever(providerUuid, sids, latch);
                Thread t = new Thread(historyRetriever);
                t.start();
            }

            log.debug("Waiting for all threads to finish...");
            latch.await();
            log.debug("All HistoryRetriever threads are finished.");

            // check all detailed statuses and determine job's total status
            // TODO

            // store status
            BasicDBObject update = new BasicDBObject();
            update.append("status", jobStatus.name())
                    .append("executionTime", getJobExecutionTime());
            updateJob(update);

            log.debug("Job {} finished with status {}.", jobId, jobStatus);
        }
        catch (Exception e) {
            jobStatus = JobStatus.FAILED;
            log.error(String.format("Job %s failed: %s", jobId, e.getMessage()), e);
        }
    }

    private void updateJob(BasicDBObject o) {
        BasicDBObject searchQuery = new BasicDBObject().append("_id", _id);
        BasicDBObject update = new BasicDBObject();
        update.append("$set", o);
        jobsColl.update(searchQuery, update);
    }

    class HistoryRetriever implements Runnable {

        private String providerUuid;
        private String[] sids;
        private CountDownLatch latch;
        private HttpClient httpClient;

        public HistoryRetriever(String providerUuid, String[] sids, CountDownLatch latch) {
            this.providerUuid = providerUuid;
            this.sids = sids;
            this.latch = latch;

            httpClient = new DefaultHttpClient();
        }

        @Override
        public void run() {
            try {
                log.debug("HistoryRetriever for the provider '{}' started.", providerUuid);
                DetailedStatus.ProviderStatus providerStatus = detailedStatus.getProviderStatus(providerUuid);

                for (String sid : sids) {
                    try {
                        log.debug("Retrieving metrics history for the VM {}.", sid);
                        HttpPost request = new HttpPost("http://10.31.1.1:8080/accounting/accounting/metric/cpu/load_one/history");

                        // headers
                        request.setHeader("Content-Type", "application/json");

                        JSONObject requestParams = new JSONObject();
                        requestParams.put("source", "host");
                        requestParams.put("sid", sid);
                        if (startTime != null) {
                            requestParams.put("startTime", DateUtils.format(startTime));
                        }
                        if (endTime != null) {
                            requestParams.put("endTime", DateUtils.format(endTime));
                        }
                        HttpEntity entity = new StringEntity(requestParams.toString());
                        request.setEntity(entity);

                        log.debug("Sending request to " + request.getURI());
                        providerStatus.setVmStatus(sid, RetrieverStatus.WAITING_FOR_RESPONSE);
                        HttpResponse response = httpClient.execute(request);
                        log.debug("Received response: " + response.getStatusLine());

                        String content = getContent(response);

                        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                            providerStatus.setVmStatus(sid, RetrieverStatus.ERROR);
                            log.debug("Invalid response received:\n{}\n{}", response.getStatusLine().toString(), content);
                            throw new Exception("Invalid response received from provider-accounting API: " +
                                    response.getStatusLine().toString());
                        }

                        // store data to MongoDB
                        BasicDBObject searchQuery = new BasicDBObject().append("_id", _id);
                        BasicDBObject update = new BasicDBObject();
                        String path = String.format("result.%s.%s.%s", providerUuid, sid, "cpu.load_one");
                        update.append("$set", new BasicDBObject().append(path, content));
                        jobsColl.update(searchQuery, update);

                        providerStatus.setVmStatus(sid, RetrieverStatus.OK);
                    }
                    catch (Exception e) {
                        providerStatus.setVmStatus(sid, RetrieverStatus.ERROR);
                        log.error(String.format(
                                "Failed to obtain metrics history data from the provider %s for the VM %s: %s",
                                providerUuid, sid, e.getMessage()), e);
                    }
                }
                log.debug("HistoryRetriever for the provider {} finished successfully.", providerUuid);
            }
            catch (Exception e) {
                log.error(String.format("Failed to obtain accounting data from the provider %s: %s",
                        providerUuid, e.getMessage()), e);
            }
            finally {
                latch.countDown();
            }
        }

        public String getContent(HttpResponse response) throws IOException {
            Scanner scanner = new Scanner(response.getEntity().getContent(), "UTF-8").useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : "";
        }
    }

    static enum RetrieverStatus {
        QUEUED,
        WAITING_FOR_RESPONSE,
        OK,
        ERROR,
        WAITING_FOR_RETRY
    }

    static class DetailedStatus {
        private Map<String, ProviderStatus> providerStatuses;

        public DetailedStatus(String[] providerUuids) {
            providerStatuses = new HashMap<String, ProviderStatus>();
            for (String providerUuid : providerUuids) {
                String[] sids = {"n0001-xc2-xlab-lan", "n0002-xc2-xlab-lan", "n0003-xc2-xlab-lan"};
                ProviderStatus providerStatus = new ProviderStatus(sids);
                providerStatuses.put(providerUuid, providerStatus);
            }
        }

        public ProviderStatus getProviderStatus(String providerUuid) {
            return providerStatuses.get(providerUuid);
        }

        public JSONObject toJson() throws JSONException {
            JSONObject o = new JSONObject();
            for (String providerUuid : providerStatuses.keySet()) {
                o.put(providerUuid, providerStatuses.get(providerUuid).toJson());
            }
            return o;
        }

        static class ProviderStatus {
            private Map<String, RetrieverStatus> vmStatuses = new HashMap<String, RetrieverStatus>();

            public ProviderStatus(String[] sids) {
                for (String sid : sids) {
                    vmStatuses.put(sid, RetrieverStatus.QUEUED);
                }
            }

            public RetrieverStatus getVmStatus(String sid) {
                return vmStatuses.get(sid);
            }

            public void setVmStatus(String sid, RetrieverStatus status) {
                vmStatuses.put(sid, status);
            }

            public JSONObject toJson() throws JSONException {
                JSONObject o = new JSONObject();
                for (String vm : vmStatuses.keySet()) {
                    o.put(vm, vmStatuses.get(vm));
                }
                return o;
            }
        }
    }
}
