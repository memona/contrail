package org.ow2.contrail.federation.accounting.utils;

import org.ow2.contrail.common.oauth.client.atmanager.MemoryOAuthATManager;
import org.ow2.contrail.common.oauth.client.atmanager.OAuthATManager;
import org.ow2.contrail.common.oauth.client.atmanager.OAuthATManagerFactory;
import org.ow2.contrail.federation.accounting.scheduler.SchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class WebAppInitializer implements ServletContextListener {
    private static Logger log = LoggerFactory.getLogger(WebAppInitializer.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            log.debug("Initializing federation-accounting...");
            ServletContext context = servletContextEvent.getServletContext();
            String configFilePath = context.getInitParameter("conf-file");
            if (configFilePath == null) {
                throw new RuntimeException("Missing parameter 'conf-file' in web.xml file.");
            }

            // load configuration file
            Conf.getInstance().load(configFilePath);

            // init scheduler
            SchedulerFactory.initScheduler();

            // init MongoDB connection
            MongoFactory.init();

            // init OAuthATManager
            OAuthATManager oauthATManager = new MemoryOAuthATManager(
                    Conf.getInstance().getAddressOAuthAS(),
                    Conf.getInstance().getKeystoreFile(), Conf.getInstance().getKeystorePass(),
                    Conf.getInstance().getTruststoreFile(), Conf.getInstance().getTruststorePass(),
                    Conf.getInstance().getOAuthClientId(), Conf.getInstance().getOAuthClientSecret());
            OAuthATManagerFactory.setOAuthATManager(oauthATManager);

            log.info("Federation-accounting was initialized successfully.");
        }
        catch (Exception e) {
            log.error("Failed to initialize federation-accounting: " + e.getMessage(), e);
            throw new RuntimeException("Failed to initialize federation-accounting.", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
