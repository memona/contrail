package org.ow2.contrail.federation.accounting.jobs;

public enum JobStatus {
    QUEUED,
    RUNNING,
    FAILED,
    ERROR,
    CANCELLED,
    SUCCESS
}
