package org.ow2.contrail.federation.accounting;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.contrail.federation.accounting.scheduler.SchedulerFactory;
import org.ow2.contrail.federation.accounting.utils.Conf;
import org.ow2.contrail.federation.accounting.utils.DateUtils;
import org.ow2.contrail.federation.accounting.utils.MongoFactory;

import java.util.Calendar;

import static org.junit.Assert.fail;

@Ignore
public class SchedulerTest extends JerseyTest {

    public SchedulerTest() throws Exception {
        super(new WebAppDescriptor.Builder("org.ow2.contrail.federation.accounting.rest")
                .contextPath("fed-acc").build());
    }

    @Before
    public void setUp() throws Exception {
        SchedulerFactory.initScheduler();
        Conf.getInstance().load("src/test/resources/federation-accounting-test.properties");
        MongoFactory.init();
    }

    @Test
    public void testScheduler() throws Exception {
        WebResource webResource = resource();
        String baseUri = webResource.getURI().toString();

        // submit job 1: cpu_usage metric history
        Calendar startTimeCal = Calendar.getInstance();
        startTimeCal.set(2013, 4, 29, 13, 58, 0);
        Calendar endTimeCal = Calendar.getInstance();
        endTimeCal.set(2013, 4, 29, 14, 58, 0);
        JSONObject postData = new JSONObject();
        JSONArray metrics = new JSONArray();
        metrics.put("cpu.load_one");
        postData.put("metrics", metrics);
        postData.put("startTime", DateUtils.format(startTimeCal.getTime()));
        postData.put("endTime", DateUtils.format(endTimeCal.getTime()));
        ClientResponse response = webResource.path("/applications/testApp/metrics_history")
                .post(ClientResponse.class, postData);
        if (response.getClientResponseStatus().getStatusCode() != 201) {
            fail("Invalid response code: " + response.getClientResponseStatus().toString());
        }
        String reportUriAbs = response.getHeaders().getFirst("Location");
        String reportUri = reportUriAbs.replaceFirst(baseUri, "");
        String jobId = reportUri.substring(reportUri.lastIndexOf("/") + 1);

        JSONObject status = webResource.path(reportUri + "/status").get(JSONObject.class);
        System.out.println(status.toString(3));

        Thread.sleep(15000);
        status = webResource.path(reportUri + "/status").get(JSONObject.class);
        System.out.println(status.toString(3));

        // get history
        String path = String.format("/reports/metrics_history/%s/providers/%s/vms/%s/metrics/%s",
                jobId, "provider1", "n0001-xc2-xlab-lan", "cpu.load_one");
        JSONObject history = webResource.path(path).get(JSONObject.class);
        System.out.println(history.toString(3));
    }
}
