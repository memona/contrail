package org.ow2.contrail.federation.herasafauthorizer.mongodb;

import org.apache.log4j.Logger;
import org.herasaf.xacml.core.context.RequestCtx;
import org.herasaf.xacml.core.context.RequestCtxFactory;
import org.herasaf.xacml.core.policy.Evaluatable;
import org.herasaf.xacml.core.policy.PolicyMarshaller;
import org.herasaf.xacml.core.policy.impl.PolicyType;
import org.herasaf.xacml.core.simplePDP.SimplePDPFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringReader;
import java.net.UnknownHostException;
import java.util.*;

import static org.junit.Assert.assertEquals;

@Ignore  // requires MongoDB running on local machine
public class MongoDBRepositoryPerformanceTest {
    private static Logger log = Logger.getLogger(MongoDBRepositoryPerformanceTest.class);
    private static String databaseName = "xacml-policy-repository-test";
    private MongoDBPolicyRepository repository;

    @Before
    public void setUp() throws UnknownHostException {
        SimplePDPFactory.getSimplePDP(); // This line is needed that the JAXB stuff is initialized.
        String[] indexes = {"subject:username", "subject:role", "resource:resource-id"};
        List<String> indexList = Arrays.asList(indexes);
        repository = new MongoDBPolicyRepository("localhost", 27017, databaseName,
                MongoDBPolicyRepository.MatchingMode.SUBJECT, indexList);
    }

    @Test
    public void testSubjectMatchingPerformance() throws Exception {
        log.trace("testSubjectMatchingPerformance() started.");
        int numOfPolices = 100000;
        // deploy policies
        log.trace("Deploying policies...");
        String template = readTextFile("src/test/resources/policy-template1.xml");

        List<String> uuids = new ArrayList<String>();
        for (int i = 0; i < numOfPolices; i++) {
            String uuid = UUID.randomUUID().toString();
            uuids.add(uuid);
            String xacml = template.replaceAll("\\$USER_UUID", uuid);
            StringReader reader = new StringReader(xacml);
            Evaluatable evaluatable = PolicyMarshaller.unmarshal(reader);
            repository.deploy(evaluatable);
        }
        log.trace("Policies deployed successfully.");


        // make requests
        int numOfRequest = 1000;
        template = readTextFile("src/test/resources/request-template1.xml");
        log.trace("Making access requests...");
        long responseTimeSum = 0;
        for (int i = 0; i < numOfRequest; i++) {
            int n = (int) (Math.random() * numOfPolices);
            String uuid = uuids.get(n);
            String xacmlRequest = template.replaceAll("\\$USER_UUID", uuid);
            StringReader reader = new StringReader(xacmlRequest);
            RequestCtx request = RequestCtxFactory.unmarshal(reader);
            Date startTime = new Date();
            List<Evaluatable> evaluatables = repository.getEvaluatables(request);
            Date endTime = new Date();
            long duration = endTime.getTime() - startTime.getTime();
            if (i > 0) {
                responseTimeSum += duration;
            }
            assertEquals(evaluatables.size(), 1);
            PolicyType policy = (PolicyType) evaluatables.get(0);
            String target = policy.getTarget().getSubjects().getSubjects().get(0).getSubjectMatches().get(0)
                    .getAttributeValue().getContent().get(0).toString();
            assertEquals(target.trim(), uuid);
        }
        double responseTimeAvg = (double) responseTimeSum / numOfRequest;
        log.info(String.format("%d policies, average response time %.2f ms.", numOfPolices, responseTimeAvg));

        log.trace("testSubjectMatchingPerformance() finished successfully.");
    }

    @After
    public void tearDown() {
        repository.clearRepository(databaseName);
    }

    private String readTextFile(String filePath) throws Exception {
        BufferedReader in = new BufferedReader(new FileReader(filePath));
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = in.readLine()) != null) {
            sb.append(line);
            sb.append("\n");
        }
        in.close();
        return sb.toString();
    }
}
