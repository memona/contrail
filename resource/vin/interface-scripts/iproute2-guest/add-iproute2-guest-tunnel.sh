#!/bin/sh

set -u -e
die() {
    echo>& 2 "$@"
    exit 1
}

[ "$#" -eq 3 ] || die "3 parameters required, but $# provided: [$@]"
TUNNEL="$1"
LOCAL_HOST="$2"
REMOTE_HOST="$3"

echo "${TUNNEL}" | grep -E -q '^[[:alnum:]_:-]+$' || die "Failed assertion"
echo "${LOCAL_HOST}" | grep -E -q '^[[:xdigit:]:.]+$' || die "Failed assertion"
echo "${REMOTE_HOST}" | grep -E -q '^[[:xdigit:]:.]+$' || die "Failed assertion"
echo "TUNNEL=${TUNNEL} LOCAL_HOST=${LOCAL_HOST} REMOTE_HOST=${REMOTE_HOST}"
/sbin/modprobe ip_gre
/bin/ip tunnel add name "${TUNNEL}" mode gre remote "${REMOTE_HOST}"
/bin/ip link set dev "${TUNNEL}" up
echo Created tunnel "'${TUNNEL}':"
/bin/ip tunnel show
