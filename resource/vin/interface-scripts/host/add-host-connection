#!/bin/sh
#
# Connect a host network device to the given IP address. Used in the case
# that VIN connects to physical machines.
#
# Usage:
#    add-host-connection <flags> <device-name> <host-address>
#

set -u -e

die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 3 ] || die "3 arguments required, $# provided: [$@]"

FLAGS=$1
DEVICENAME=$2
ADDRESS=$3

# Do sanity checks on the parameters.

# Require that an identifier only consists of alphanumeric characters, ':', or 
#  '-'. (Note that the '-' must be at the end, otherwise it introduces a range.)
echo "$DEVICENAME" | grep -E -q '^[[:alnum:]:-]+$' || die "Incorrect device name '$DEVICENAME'"

# Require that the addresses only consists of hex digits or ':' or '.' characters.
echo "$ADDRESS" | grep -E -q '^[[:xdigit:]:.]+$' || die "Incorrect address '$ADDRESS'"

echo "FLAGS=$FLAGS DEVICENAME=$DEVICENAME"
case "$FLAGS" in
  *[iI]*)
    id
esac
case "$FLAGS" in
  *[xX]*)
    set -x
esac

ip addr add $ADDRESS dev lo scope host

case "$FLAGS" in
*[dD]*)
    ip addr list
    ;;
esac
