package org.ow2.contrail.resource.vin.common;

import ibis.util.RunProcess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Properties;

import org.ow2.contrail.resource.vin.annotations.Nullable;
import org.slf4j.Logger;

public class Utils {
    private static final double NANOSECOND = 1e-9;

    private static final double MICROSECOND = 1e-6;

    private static final double MILLISECOND = 1e-3;

    private static final double SECOND = 1.0;

    /**
     * Returns a string with the platform version that is used.
     * 
     * @return The platform version.
     */
    public static String getPlatformVersion() {
        final java.util.Properties p = System.getProperties();

        return "Java " + p.getProperty("java.version") + " ("
                + p.getProperty("java.vendor") + ") on "
                + p.getProperty("os.name") + ' ' + p.getProperty("os.version")
                + " (" + p.getProperty("os.arch") + ')';
    }

    /**
     * Given a time in seconds, return a neat format string for it.
     * 
     * @param t
     *            The time to format.
     * @return The formatted string.
     */
    static String formatSeconds(final double t) {
        if (t == Double.POSITIVE_INFINITY) {
            return "infinite";
        }
        if (t == 0.0) {
            return "0 s";
        }
        if (t < MICROSECOND && t > -MICROSECOND) {
            return String.format("%4.1f ns", 1e9 * t).trim();
        }
        if (t < MILLISECOND && t > -MILLISECOND) {
            return String.format("%4.1f us", 1e6 * t).trim();
        }
        if (t < SECOND && t > -SECOND) {
            return String.format("%4.1f ms", 1e3 * t).trim();
        }
        return String.format("%4.1f s", t).trim();
    }

    /**
     * Given a byte count, return a human-readable representation of it.
     * 
     * @param n
     *            The byte count to represent.
     * @return The byte count as a human-readable string.
     */
    protected static String formatByteCount(final long n) {
        if (n < 1000) {
            // This deliberately covers negative numbers
            return n + "B";
        }
        if (n < 1000000) {
            return String.format("%.1fKB", n * 1e-3);
        }
        if (n < 1000000000L) {
            return String.format("%.1fMB", n * 1e-6);
        }
        if (n < 1000000000000L) {
            return String.format("%.1fGB", n * 1e-9);
        }
        return String.format("%.1fTB", n * 1e-12);
    }

    public static void printThreadStats(final Logger logger) {
        final ThreadMXBean threadBean = ManagementFactory.getThreadMXBean();

        logger.debug("Peak thread count: " + threadBean.getPeakThreadCount());
        final long lockedThreads[] = threadBean.findDeadlockedThreads();
        if (lockedThreads != null && lockedThreads.length > 0) {
            logger.debug("===== DEADLOCKED threads =====");
            for (final long tid : lockedThreads) {
                final ThreadInfo ti = threadBean.getThreadInfo(tid,
                        Integer.MAX_VALUE);
                if (ti != null) {
                    logger.debug(ti.toString());
                }
            }
        }
    }

    /**
     * @return Return the precise current time in seconds.
     */
    protected static double getPreciseTime() {
        return NANOSECOND * System.nanoTime();
    }

    @Nullable
    public static String getCanonicalHostname() {
        String res;

        try {
            final InetAddress localMachine = InetAddress.getLocalHost();
            res = localMachine.getCanonicalHostName();
        } catch (final UnknownHostException e) {
            res = null;
        }
        return res;
    }

    /**
     * Returns true iff the given class represents a primitive type, where we
     * consider String to be such a primitive type.
     * 
     * @param c
     *            The class under consideration.
     * @return <code>true</code> iff the class represents a primitive type.
     */
    private static boolean isPrimitiveType(final Class<?> c) {
        return c == java.lang.Boolean.TYPE || c == java.lang.Character.TYPE
                || c == java.lang.Byte.TYPE || c == java.lang.Short.TYPE
                || c == java.lang.Integer.TYPE || c == java.lang.Long.TYPE
                || c == java.lang.Float.TYPE || c == java.lang.Double.TYPE
                || c == String.class;
    }

    private static boolean writeFields(final StringBuffer buf,
            final Class<? extends Object> clazz, final Object o, boolean first) {
        final Class<? extends Object> superClazz = clazz.getSuperclass();
        if (superClazz != null) {
            first = writeFields(buf, superClazz, o, first);
        }
        final Field[] fields = clazz.getDeclaredFields();
        for (final Field f : fields) {
            final Class<?> t = f.getType();
            if (isPrimitiveType(t)) {
                if (first) {
                    first = false;
                } else {
                    buf.append(' ');
                }
                final String nm = f.getName();
                buf.append(nm);
                buf.append('=');
                try {
                    f.setAccessible(true);
                    if (t == String.class) {
                        buf.append('"');
                        buf.append(f.get(o));
                        buf.append('"');
                    } else {
                        buf.append(f.get(o));
                    }
                } catch (final IllegalAccessException x) {
                    buf.append("<hidden>");
                }
            }
        }
        return first;
    }

    static String toStringClassScalars(final Object o) {
        final StringBuffer buf = new StringBuffer();

        final Class<? extends Object> clazz = o.getClass();
        buf.append(clazz.getSimpleName());
        buf.append('[');
        final boolean first = true;
        writeFields(buf, clazz, o, first);
        buf.append(']');
        return buf.toString();
    }

    static String read(final BufferedReader br) throws IOException {
        final StringBuffer res = new StringBuffer();
        final char tmpBuf[] = new char[50];

        while (true) {
            final int numRead = br.read(tmpBuf);
            if (numRead < 0) {
                break;
            }
            res.append(tmpBuf, 0, numRead);
        }
        return res.toString();
    }

    /**
     * Given a filename, try to read that file.
     * 
     * @param f
     *            The name of the file to read.
     * @return The contents of the file, or null.
     * @throws IOException
     *             Thrown if for some reason the file cannot be read.
     */
    @Nullable
    public static String readFile(final File f) throws IOException {
        final StringBuffer res = new StringBuffer();
        FileReader fr = null;
        final char tmpBuf[] = new char[10];

        try {
            fr = new FileReader(f);
            while (true) {
                final int numRead = fr.read(tmpBuf);
                if (numRead < 0) {
                    break;
                }
                res.append(tmpBuf, 0, numRead);
            }
        } finally {
            if (fr != null) {
                try {
                    fr.close();
                } catch (final IOException e) {
                    // Nothing we can do about it.
                }
            }
        }
        return res.toString();
    }

    /**
     * Given a file <code>f</code> and a string <code>s</code>, create the given
     * file, and fill it with the text in <code>s</code>.
     * 
     * @param f
     *            The file to create.
     * @param s
     *            The contents of the file.
     * @return True iff we successfully created the file.
     * @throws IOException
     *             Thrown when somehow the file could not be written.
     */
    public static boolean writeFile(final File f, final String s)
            throws IOException {
        final boolean ok = f.delete(); // First make sure it doesn't exist.
        FileWriter output = null;
        try {
            output = new FileWriter(f);
            output.write(s);
        } finally {
            if (output != null) {
                output.close();
            }
        }
        return ok;
    }

    static RunProcess executeCommand(final String commandString,
            final String... parameters) {
        final File command = new File(commandString);
        return executeCommand(command, parameters);
    }

    private static RunProcess executeCommand(final File command,
            final String... parameters) {
        if (command.isAbsolute() && !command.exists()) {
            final File lrp = getLargestReadablePath(command);
            throw new VinInternalError("Executable '" + command
                    + "' does not exist. Largest readable path: [" + lrp + "]");
        }
        if (command.isAbsolute() && !command.canExecute()) {
            final File lrp = getLargestReadablePath(command);
            throw new VinInternalError("Executable '" + command
                    + "' is not executable. LRP=[" + lrp + "]");
        }
        final String pl[] = new String[parameters.length + 1];
        System.arraycopy(parameters, 0, pl, 1, parameters.length);
        pl[0] = command.getPath();
        final RunProcess p = new RunProcess(pl);
        Globals.externalExecutionLogger.info("executeCommand: pl="
                + Arrays.deepToString(pl));
        p.run();
        final byte[] o = p.getStdout();
        final byte[] e = p.getStderr();
        final int status = p.getExitStatus();
        Globals.externalExecutionLogger.debug("executeCommand: status="
                + status);
        if (o.length != 0) {
            Globals.externalExecutionLogger.info("stdout:\n" + new String(o));
        }
        if (e.length != 0) {
            Globals.externalExecutionLogger.info("stderr:\n" + new String(e));
        }
        return p;
    }

    private static File getLargestReadablePath(final File f) {
        File file = f;
        do {
            if (file == null || file.canRead()) {
                return file;
            }
            file = file.getParentFile();
        } while (true);
    }

    private static String getFileInfo(final File f) {
        final String absolutePath = f.getAbsolutePath();
        final RunProcess p = executeCommand("ls", "-l", absolutePath);
        if (p.getExitStatus() != 0) {
            return "[ls -l " + absolutePath + " returned exit code "
                    + p.getExitStatus() + "]";
        }
        return new String(p.getStdout()).trim();
    }

    static RunProcess executeScript(final File scriptsDir,
            final String scriptName, final String... parameters) {
        if (!scriptsDir.isDirectory()) {
            if (scriptsDir.exists()) {
                throw new VinInternalError("supposed scripts directory '"
                        + scriptsDir + "' exists, but is not a directory: "
                        + getFileInfo(scriptsDir));
            }
            throw new VinInternalError("scripts directory '" + scriptsDir
                    + "' (" + scriptsDir.getAbsolutePath()
                    + ") does not exists");
        }
        final File script = new File(scriptsDir, scriptName);
        if (!script.isFile()) {
            if (script.exists()) {
                throw new VinInternalError("supposed script  '" + script
                        + "' exists, but is not a file: " + getFileInfo(script));
            }
            throw new VinInternalError("script '" + script
                    + "' does not exists");
        }
        if (!script.canExecute()) {
            throw new VinInternalError("script '" + script
                    + "' is not executable: " + getFileInfo(script));
        }
        final String pl[] = new String[parameters.length + 1];
        System.arraycopy(parameters, 0, pl, 1, parameters.length);
        pl[0] = script.getAbsolutePath();
        final RunProcess p = new RunProcess(pl);
        Globals.externalExecutionLogger.info("executeScript: pl="
                + Arrays.deepToString(pl) + " [" + getFileInfo(script) + "]");
        p.run();
        final byte[] o = p.getStdout();
        final byte[] e = p.getStderr();
        final int status = p.getExitStatus();
        Globals.externalExecutionLogger
                .debug("executeScript: status=" + status);
        if (o.length != 0) {
            Globals.externalExecutionLogger.info("stdout:\n" + new String(o));
        }
        if (e.length != 0) {
            Globals.externalExecutionLogger.info("stderr:\n" + new String(e));
        }
        return p;
    }

    public static RunProcess cleanlyExecuteScript(final File scriptsDir,
            final String scriptName, final String... parameters) {
        final RunProcess p = executeScript(scriptsDir, scriptName, parameters);
        final int exitStatus = p.getExitStatus();
        if (exitStatus != 0) {
            final String msg = "Script " + scriptName + " parameters: "
                    + Arrays.deepToString(parameters)
                    + " failed with exit code " + exitStatus + "\nstdout:\n"
                    + new String(p.getStdout()) + "\nstderr:\n"
                    + new String(p.getStderr());
            Globals.externalExecutionLogger.error(msg);
            throw new VinInternalError(msg);
        }
        return p;
    }

    public static Process cleanlyStartConfig(final File safeconfigPath,
            final boolean debug, final File configDir, final String scriptName,
            final String... parameters) throws ConfigurationError, IOException {
        SanityChecks.ensureFileExists(safeconfigPath);
        SanityChecks
                .ensureDirectoryExists(configDir, "Configuration directory");
        final File config = new File(configDir, scriptName);
        SanityChecks.ensureFileExists(config);
        final int extra = debug ? 2 : 1;
        final String pl[] = new String[parameters.length + extra];
        System.arraycopy(parameters, 0, pl, extra, parameters.length);
        if (debug) {
            pl[0] = "-D";
            pl[1] = config.getAbsolutePath();
        } else {
            pl[0] = config.getAbsolutePath();
        }
        final ProcessBuilder builder = new ProcessBuilder(pl);
        final Process p = builder.start();
        return p;
    }

    public static RunProcess cleanlyRunConfig(final File safeconfigPath,
            final boolean debug, final File configDir, final String scriptName,
            final String... parameters) throws ConfigurationError {
        SanityChecks.ensureFileExists(safeconfigPath);
        SanityChecks
                .ensureDirectoryExists(configDir, "Configuration directory");
        final File config = new File(configDir, scriptName);
        SanityChecks.ensureFileExists(config);
        final int extra = debug ? 2 : 1;
        final String pl[] = new String[parameters.length + extra];
        System.arraycopy(parameters, 0, pl, extra, parameters.length);
        if (debug) {
            pl[0] = "-D";
            pl[1] = config.getAbsolutePath();
        } else {
            pl[0] = config.getAbsolutePath();
        }
        final RunProcess p = executeCommand(safeconfigPath, pl);
        final int exitStatus = p.getExitStatus();
        if (exitStatus != 0) {
            final String msg = "Configuration " + scriptName + " parameters: "
                    + Arrays.deepToString(parameters)
                    + " failed with exit code " + exitStatus + "\nstdout:\n"
                    + new String(p.getStdout()) + "\nstderr:\n"
                    + new String(p.getStderr());
            Globals.externalExecutionLogger.error(msg);
            throw new VinInternalError(msg);
        }
        return p;
    }

    public static RunProcess cleanlyExecuteCommand(final String command,
            final String... parameters) {
        final RunProcess p = executeCommand(command, parameters);
        final int exitStatus = p.getExitStatus();
        if (exitStatus != 0) {
            final String msg = "Command " + command + " parameters: "
                    + Arrays.deepToString(parameters)
                    + " failed with exit code " + exitStatus + "\nstdout:\n"
                    + new String(p.getStdout()) + "\nstderr:\n"
                    + new String(p.getStderr());
            Globals.externalExecutionLogger.error(msg);
            throw new VinInternalError(msg);
        }
        return p;
    }

    public static RunProcess cleanlyExecuteCommand(final File command,
            final String... parameters) {
        final RunProcess p = executeCommand(command, parameters);
        final int exitStatus = p.getExitStatus();
        if (exitStatus != 0) {
            final String msg = "Command " + command + " parameters: "
                    + Arrays.deepToString(parameters)
                    + " failed with exit code " + exitStatus + "\nstdout:\n"
                    + new String(p.getStdout()) + "\nstderr:\n"
                    + new String(p.getStderr());
            Globals.externalExecutionLogger.error(msg);
            throw new VinInternalError(msg);
        }
        return p;
    }

    public static int getIntProperty(final Properties properties,
            final String key, final String description) throws VinError {
        final String s = properties.getProperty(key);
        if (s == null) {
            throw new VinError("No integer property '" + key + "' ("
                    + description + ") specified in " + properties);
        }
        int val;

        try {
            val = Integer.parseInt(s);
        } catch (final NumberFormatException e) {
            throw new VinError("Bad value for integer property '" + key + "' ("
                    + description + ") in " + properties, e);
        }
        return val;
    }

    public static String getStringProperty(final Properties properties,
            final String key, final String description) throws VinError {
        final String val = properties.getProperty(key);
        if (val == null) {
            throw new VinError("No string property '" + key + "' ("
                    + description + ") specified in " + properties);
        }
        return val;
    }

    public static boolean getBooleanProperty(final Properties properties,
            final String key, final String description) throws VinError {
        final String val = properties.getProperty(key);
        if (val == null) {
            throw new VinError("No boolean property '" + key + "' ("
                    + description + ") specified in " + properties);
        }
        if (val.isEmpty() || val.equalsIgnoreCase("true")
                || val.equalsIgnoreCase("yes")) {
            return true;
        }
        if (val.equalsIgnoreCase("false") || val.equalsIgnoreCase("no")) {
            return false;
        }
        throw new VinError("Bad value for boolean property '" + key + "' ("
                + description + ") specified: '" + val + "'");
    }

    public static boolean getOptionalBooleanProperty(
            final Properties properties, final String key,
            final String description, final boolean deflt) throws VinError {
        final String val = properties.getProperty(key);
        if (val == null) {
            return deflt;
        }
        if (val.isEmpty() || val.equalsIgnoreCase("true")
                || val.equalsIgnoreCase("yes") || val.equalsIgnoreCase("1")) {
            return true;
        }
        if (val.equalsIgnoreCase("false") || val.equalsIgnoreCase("no")
                || val.equalsIgnoreCase("0")) {
            return false;
        }
        throw new VinError("Bad value for boolean property '" + key + "' ("
                + description + ") specified: '" + val + "'");
    }

}
