package org.ow2.contrail.resource.vin.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Indicates that the given method is part of the testing API (i.e. the method
 * is there for testing code, not for normal operation) (so don't complain that
 * it is only used in tests).
 * 
 * @author Kees van Reeuwijk
 * 
 */
@Retention(RetentionPolicy.SOURCE)
public @interface TestAPI {
    // Nothing
}
