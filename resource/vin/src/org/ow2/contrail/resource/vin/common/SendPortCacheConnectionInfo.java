package org.ow2.contrail.resource.vin.common;

import ibis.ipl.Ibis;
import ibis.ipl.IbisIdentifier;
import ibis.ipl.SendPort;

import java.io.IOException;

import org.ow2.contrail.resource.vin.annotations.Nullable;

final class SendPortCacheConnectionInfo {
    @Nullable
    private SendPort port;

    private int mostRecentUse = 0;

    @Nullable
    synchronized SendPort getPort(final Ibis localIbis,
            final IbisIdentifier remoteIbis, final int useCount) {
        if (port == null) {
            try {
                port = localIbis.createSendPort(PacketSendPort.portType);
                port.connect(remoteIbis, Globals.receivePortName,
                        Settings.COMMUNICATION_TIMEOUT, true);
            } catch (final IOException x) {
                Globals.transmitterLogger.error(
                        "Could not create a sendPort to " + remoteIbis + ": "
                                + x.getLocalizedMessage(), x);
                try {
                    if (port != null) {
                        port.close();
                        port = null;
                    }
                } catch (final Exception e) {
                    // Nothing we can do.
                }
                throw new VinInternalError("Could not create a sendPort to "
                        + remoteIbis + ": " + x.getLocalizedMessage(), x);
            }
        }
        mostRecentUse = useCount;
        return port;
    }

    synchronized void close() {
        if (port != null) {
            try {
                port.close();
            } catch (final IOException e) {
                // Nothing we can do.
            }
            port = null;
        }
    }

    synchronized int getMostRecentUse() {
        return mostRecentUse;
    }
}