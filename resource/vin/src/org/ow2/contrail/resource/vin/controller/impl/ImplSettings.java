package org.ow2.contrail.resource.vin.controller.impl;

class ImplSettings {
    /** Maximal number of messages in the receive queue before it blocks. */
    static final int MAXIMAL_RECEIVED_MESSAGE_QUEUE_LENGTH = 50;

    public static final int MAXIMAL_ENGINE_SLEEP_INTERVAL = 2000;

    /**
     * The maximal time in ms we wait for the transmitter to shut down.
     */
    static final long TRANSMITTER_SHUTDOWN_TIMEOUT = 30000;

    /**
     * The maximal time in ms we wait for the entire engine to shut down.
     */
    static final long ENGINE_SHUTDOWN_TIMEOUT = 10 * 60 * 1000;
}
