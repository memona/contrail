package org.ow2.contrail.resource.vin.controller;

import java.util.Properties;

import org.ow2.contrail.resource.vin.common.VinError;

/**
 * A factory for VIN administrations.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class VinAdministratorFactory {
    /**
     * Given a set of properties, create a VIN administrator.
     * 
     * @param properties
     *            The properties of the administrator to use.
     * @return The newly created VinAdministrator.
     * @throws VinError
     *             Thrown if there is an error in the configuration of the
     *             administrator.
     */
    public static VinAdministratorAPI createVinAdministrator(
            final Properties properties) throws VinError {
        return org.ow2.contrail.resource.vin.controller.impl.VinAdministratorFactory
                .createVinAdministrator(properties);
    }
}
