package org.ow2.contrail.resource.vin.agent;

class Settings {
    /** Maximal number of messages in the receive queue before it blocks. */
    static final int MAXIMAL_RECEIVED_MESSAGE_QUEUE_LENGTH = 50;

    static final int MAXIMAL_ENGINE_SLEEP_INTERVAL = 2000;

    /**
     * The maximal time in ms we wait for the transmitter to shut down.
     */
    static final long TRANSMITTER_SHUTDOWN_TIMEOUT = 30000;

    static final boolean traceDetailedProgress = true;
}
