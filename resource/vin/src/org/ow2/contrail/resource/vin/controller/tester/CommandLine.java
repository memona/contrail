package org.ow2.contrail.resource.vin.controller.tester;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import org.ow2.contrail.resource.vin.annotations.Nullable;

/**
 * The command-line interface of Vin. That is, this class parses the
 * command-line arguments, and executes a Vin deployment according to these
 * arguments.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class CommandLine {

    private static void usage(final PrintStream stream) {
        stream.println("Usage: " + CommandLine.class.getName()
                + " <option>] ... <option> <configFileName>");
        stream.println("Where <option> is:");
        stream.println(" -agentDir=<dir>\tThe directory that contains agent-related files");
        stream.println(" -k\tKeep the sandboxes and files of deployment agents");
        stream.println(" -p=<properties-file>\tSpecify a properties file");
    }

    private static class Parameters {
        @Nullable
        private final ArrayList<String> configFileNames = new ArrayList<String>();
        Properties properties = new Properties();

        @SuppressWarnings("synthetic-access")
        private Parameters(final String[] args) throws IOException {
            for (final String arg : args) {
                if (arg.equals("-h")) {
                    usage(System.out);
                    System.exit(0);
                } else if (arg.startsWith("-p=")) {
                    final File fnm = extractFileFromArg(arg);
                    final FileInputStream s = new FileInputStream(fnm);
                    properties.load(s);
                    s.close();
                } else if (arg.startsWith("-agentDir=")) {
                    extractDirFromArg(arg);
                } else if (arg.startsWith("-scriptsDir=")) {
                    extractDirFromArg(arg);
                } else if (arg.startsWith("-")) {
                    System.err.println("Error: Unknown option '" + arg + "'");
                    usage(System.err);
                    System.err.println("Actual arguments: "
                            + Arrays.deepToString(args));
                    throw new Error("Bad command-line arguments");
                } else {
                    configFileNames.add(arg);
                }
            }
        }

        private static File extractDirFromArg(final String arg) {
            final int ix = arg.indexOf('=');
            if (ix < 0) {
                throw new Error("No '=' in argument '" + arg + '\'');
            }
            final String nm = arg.substring(ix + 1);
            if (nm.isEmpty()) {
                throw new Error("Empty name in argument '" + arg + '\'');
            }
            final File dir = new File(nm);
            if (!dir.exists()) {
                throw new Error("Directory '" + dir
                        + "' does not exist, (specified in argument '" + arg
                        + '\'');
            }
            if (!dir.isDirectory()) {
                throw new Error("Not a directory: '" + dir
                        + "' (specified in argument '" + arg + '\'');
            }
            return dir;
        }

        private static File extractFileFromArg(final String arg) {
            final int ix = arg.indexOf('=');
            if (ix < 0) {
                throw new Error("No '=' in argument '" + arg + '\'');
            }
            final String nm = arg.substring(ix + 1);
            if (nm.isEmpty()) {
                throw new Error("Empty name in argument '" + arg + '\'');
            }
            final File fnm = new File(nm);
            if (!fnm.exists()) {
                throw new Error("File '" + fnm
                        + "' does not exist, (specified in argument '" + arg
                        + "\')");
            }
            if (!fnm.isFile()) {
                throw new Error("Not a file: '" + fnm
                        + "' (specified in argument '" + arg + "\')");
            }
            return fnm;
        }
    }

    /**
     * The method that is invoked from the command line.
     * 
     * @param args
     *            The command-line parameters
     */
    @SuppressWarnings("synthetic-access")
    public static void main(final String[] args) {
        Parameters parameters;
        try {
            parameters = new Parameters(args);
        } catch (final UnknownHostException e) {
            e.printStackTrace();
            System.exit(1);
            return;
        } catch (final IOException e) {
            e.printStackTrace();
            System.exit(1);
            return;
        }

        try {
            final boolean ok = Tester.runTest(parameters.configFileNames,
                    parameters.properties);
            if (!ok) {
                // Make sure the failure is visible.
                System.err.println("Test failed, goodbye");
                System.exit(1);
            }
        } catch (final Exception e) {
            /**
             * Exception handler of last resort. We should never get here, but
             * in case the impossible happens, we want to be absolutely sure we
             * stop the engine, and that we do an exit(2).
             */
            e.printStackTrace();
            try {
                // Sleep a while to allow all agents to shut down.
                Thread.sleep(2000);
            } catch (final InterruptedException e1) {
                // Ignore
            }
            System.err.println("Fatal exception, goodbye");
            System.exit(2);
        }
    }
}
