package org.ow2.contrail.resource.vin.controller.tester;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.VinError;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * The representation of an entire VIN: private networks, public connections.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class VinDescriptor {
    final ArrayList<MachineDescriptor> machines;
    final ArrayList<Network> networks;
    final Properties properties;
    final int lifetime;
    final String deployer;

    VinDescriptor(final ArrayList<MachineDescriptor> machines,
            final ArrayList<Network> networks, final Properties properties,
            final int lifetime, final String deployer) {
        this.machines = machines;
        this.networks = networks;
        this.properties = properties;
        this.lifetime = lifetime;
        this.deployer = deployer;
    }

    void addJson(final ObjectMapper mapper, final ObjectNode node) {
        if (machines != null) {
            final ArrayNode subtree = node.putArray("machines");
            for (final MachineDescriptor e : machines) {
                final ObjectNode nd = mapper.createObjectNode();
                e.addJSon(mapper, nd);
                subtree.add(nd);
            }
        }
        if (networks != null) {
            final ArrayNode subtree = node.putArray("networks");
            for (final Network e : networks) {
                final ObjectNode nd = mapper.createObjectNode();
                e.addJSon(mapper, nd);
                subtree.add(nd);
            }
        }
    }

    static VinDescriptor readJSon(final JsonNode node) throws VinError {
        final ArrayList<MachineDescriptor> machines = new ArrayList<MachineDescriptor>();
        final ArrayList<Network> networks = new ArrayList<Network>();
        Properties properties;
        int lifetime = 60 * 60; // One hour
        String deployer = "none";
        final JsonNode machinesNode = node.path("machines");
        if (!machinesNode.isMissingNode()) {
            for (final JsonNode e : machinesNode) {
                final MachineDescriptor m = MachineDescriptor.extractJson(e);
                if (m != null) {
                    machines.add(m);
                }
            }
        }
        final JsonNode privateNetworksNode = node.path("networks");
        if (!privateNetworksNode.isMissingNode()) {
            for (final JsonNode e : privateNetworksNode) {
                final Network nw = Network.extractJson(e);
                if (nw != null) {
                    networks.add(nw);
                }
            }
        }
        final JsonNode propertiesNode = node.path("properties");
        if (propertiesNode == null) {
            properties = new Properties();
        } else {
            properties = VinDescriptorParser.extractProperties(propertiesNode);
        }
        final JsonNode lifetimeNode = node.get("lifetime");
        if (lifetimeNode != null) {
            lifetime = lifetimeNode.asInt();
        }
        final JsonNode deployerNode = node.get("deployer");
        if (deployerNode != null) {
            deployer = deployerNode.asText();
        }
        return new VinDescriptor(machines, networks, properties, lifetime,
                deployer);
    }

    static VinDescriptor parseJsonString(final String json)
            throws JsonParseException, IOException, VinError {
        final ObjectMapper m = new ObjectMapper();
        final JsonNode rootNode = m.readTree(json);
        return readJSon(rootNode);
    }

    static VinDescriptor parseJsonFile(final File file)
            throws JsonProcessingException, IOException, VinError {
        final ObjectMapper m = new ObjectMapper();
        FileInputStream stream = null;
        try {
            stream = new FileInputStream(file);
            final JsonNode rootNode = m.readTree(stream);
            return readJSon(rootNode);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (final IOException e) {
                    // Ignore.
                }
            }
        }
    }

    String buildJsonString() throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final ObjectNode node = mapper.createObjectNode();
        addJson(mapper, node);
        return mapper.writeValueAsString(node);
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof VinDescriptor)) {
            return false;
        }
        final VinDescriptor that = (VinDescriptor) obj;
        return machines.equals(that.machines) && networks.equals(that.networks);
    }

    @Override
    public int hashCode() {
        return machines.hashCode() ^ networks.hashCode();
    }

}
