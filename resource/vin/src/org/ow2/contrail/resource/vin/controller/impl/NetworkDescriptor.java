package org.ow2.contrail.resource.vin.controller.impl;

import ibis.ipl.IbisIdentifier;

import java.net.InetAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.ArrayValueRange;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.EncapsulationType;
import org.ow2.contrail.resource.vin.common.IPAddressRangePool;
import org.ow2.contrail.resource.vin.common.Message;
import org.ow2.contrail.resource.vin.common.NetworkDescriptionMessage;
import org.ow2.contrail.resource.vin.common.OutOfEntriesError;
import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.Subnet;
import org.ow2.contrail.resource.vin.common.VinError;

/**
 * Relevant information for a network: type, name, and encryption key, and such.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class NetworkDescriptor {
    private int ordinalCounter = 0;
    private final String networkId;
    private final EncapsulationType networkType;
    private final ArrayList<ConnectionDescriptor> connections = new ArrayList<ConnectionDescriptor>();

    private final Properties networkProperties;
    private final IPAddressRangePool subnetPool;
    final ArrayValueRange macAddresses;
    final URI ca;
    final Subnet subnet;
    @SuppressWarnings("unused")
    private final InetAddress broadcast;
    @SuppressWarnings("unused")
    private final InetAddress dhcpAddress;
    private final boolean connectionsAreAlwaysReady;

    NetworkDescriptor(final EncapsulationType networkType, final String id,
            final Subnet subnet, final ArrayValueRange macAddresses,
            final URI ca, final boolean connectionsAreAlwaysReady,
            final Properties properties) throws OutOfEntriesError {
        super();
        assert id != null;
        this.networkType = networkType;
        this.networkProperties = properties;
        this.subnet = subnet;
        this.subnetPool = subnet.createPool("subnet pool");
        // Reserve first two addresses in this subnet.
        this.broadcast = subnetPool.getNextAddress();
        this.dhcpAddress = subnetPool.getNextAddress();
        this.macAddresses = macAddresses;
        this.ca = ca;
        this.connectionsAreAlwaysReady = connectionsAreAlwaysReady;
        networkId = id;
    }

    NetworkDescriptionMessage buildPrivateNetworkDescriptionMessage() {
        return new NetworkDescriptionMessage(networkType, networkId, subnet,
                macAddresses, ca, networkProperties);
    }

    void addConnection(final AgentInfo agent, final String connectionId,
            final InetAddress virtualAddress, final Properties properties)
            throws ConfigurationError {
        final String domain = networkProperties
                .getProperty(PropertyNames.DOMAIN);
        String name = properties.getProperty(PropertyNames.NAME);
        if (name == null) {
            if (domain != null) {
                /*
                 * The network has a name, so we want to give this machine a
                 * name as well.
                 */
                name = agent.getHostName();
                if (name != null) {
                    properties.setProperty(PropertyNames.NAME, name);
                }
            }
        } else {
            if (domain == null) {
                throw new ConfigurationError("Connection with name '" + name
                        + "' in a network without a domain name (id="
                        + networkId + ")");
            }
            for (final ConnectionDescriptor cn : connections) {
                final String otherName = cn.getName();
                if (otherName != null) {
                    if (name.equals(otherName)) {
                        throw new ConfigurationError("Duplicate hostname '"
                                + name + "' in network " + domain);
                    }
                }
            }
        }
        final int ordinal = ordinalCounter++;
        final ConnectionDescriptor e = new ConnectionDescriptor(ordinal, agent,
                networkId, connectionId, virtualAddress,
                connectionsAreAlwaysReady, properties);
        connections.add(e);
    }

    boolean registerMemberReady(final String connectionId) throws VinError {
        final int ix = searchMemberByConnectionId(connections, connectionId);
        if (ix < 0) {
            throw new VinError("Unknown connection id '" + connectionId
                    + "' in network " + networkId);
        }
        final ConnectionDescriptor c = connections.get(ix);
        return c.setReady();
    }

    private static final int searchMemberByConnectionId(
            final ArrayList<ConnectionDescriptor> l, final String connectionId) {
        for (int i = 0; i < l.size(); i++) {
            final ConnectionDescriptor m = l.get(i);
            if (m.connectionId.equals(connectionId)) {
                return i;
            }
        }
        return -1;
    }

    Message buildPrivateNetworkMembershipMessage(final String connectionId)
            throws VinError {
        final int ix = searchMemberByConnectionId(connections, connectionId);
        if (ix < 0) {
            throw new VinError("Unknown connection id '" + connectionId
                    + "' in network " + networkId);
        }
        final ConnectionDescriptor m = connections.get(ix);
        return m.buildPrivateNetworkMembershipMessage();
    }

    void removeConnection(final String connectionId,
            final ControllerEngine personality) throws VinError {
        final int ix = searchMemberByConnectionId(connections, connectionId);
        if (ix < 0) {
            throw new VinError("Unknown connection id '" + connectionId
                    + "' in network " + networkId);
        }
        final ConnectionDescriptor c = connections.remove(ix);
        c.returnVINAddress(personality);
    }

    void ensureConnectionIdExists(final String connectionId) throws VinError {
        final int ix = searchMemberByConnectionId(connections, connectionId);
        if (ix < 0) {
            throw new VinError("Unknown connection id '" + connectionId
                    + "' in network " + networkId);
        }
    }

    String getIPAddressString(final String connectionId) throws VinError {
        final int ix = searchMemberByConnectionId(connections, connectionId);
        if (ix < 0) {
            throw new VinError("Unknown connection id '" + connectionId
                    + "' in network " + networkId);
        }
        final ConnectionDescriptor m = connections.get(ix);
        return m.getVINAddressString();
    }

    /**
     * Register that the given node has left. Return <code>true</code> iff the
     * node was essential, and the entire network should be deleted.
     * 
     * @param node
     *            The Ibis node that has left.
     * @throws VinError
     */
    void registerNodeLeft(final IbisIdentifier node,
            final ControllerEngine personality) throws VinError {
        for (final ConnectionDescriptor c : connections) {
            if (c.agent.isThisNode(node)) {
                removeConnection(c.connectionId, personality);
                break;
            }
        }
    }

    InetAddress getNextAddress() throws OutOfEntriesError {
        return subnetPool.getNextAddress();
    }

    void recycleAddress(final InetAddress addr) {
        subnetPool.recycleAddress(addr);
    }

    String getVmId(final String connectionId) throws VinError {
        final int ix = searchMemberByConnectionId(connections, connectionId);
        if (ix < 0) {
            throw new VinError("Unknown connection id '" + connectionId
                    + "' in network " + networkId);
        }
        final ConnectionDescriptor m = connections.get(ix);
        return m.agent.agentId;
    }

}
