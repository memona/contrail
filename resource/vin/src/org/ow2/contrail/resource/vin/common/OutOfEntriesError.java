package org.ow2.contrail.resource.vin.common;

public class OutOfEntriesError extends Exception {
    private static final long serialVersionUID = 1L;

    public OutOfEntriesError(final String msg) {
        super(msg);
    }

}
