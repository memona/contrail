package org.ow2.contrail.resource.vin.common;

import java.io.Serializable;

/**
 * A range of value, represented by the lowest value in the range, and the last
 * value in the range.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class ArrayValueRange implements Serializable {
    private static final long serialVersionUID = 1L;

    /** The first value in the range. */
    public final ArrayValue startValue;

    /** The last value in the range. */
    public final ArrayValue endValue;

    /**
     * Constructs a new range with the given start and end value. The arrays
     * that are passed in are stored immediately, they are <em>not</em> copied.
     * 
     * @param startValue
     *            The first value in the range.
     * @param endValue
     *            The last value in the range.
     */
    public ArrayValueRange(final ArrayValue startValue,
            final ArrayValue endValue) {
        this.startValue = startValue;
        this.endValue = endValue;
    }

    /**
     * Constructs a new range consisting of a single value. The array that is
     * passed in is stored immediately, it is <em>not</em> copied.
     * 
     * @param startValue
     *            The value that constitutes the entire range.
     */
    ArrayValueRange(final ArrayValue startValue) {
        this.startValue = startValue;
        this.endValue = startValue;
    }

    /**
     * Given an other byte range, try to merge it with this byte range. Return
     * <code>null</code> if the ranges cannot be merged, or the new range if
     * they can.
     * 
     * @param r
     *            The range we would like to merge with.
     * @return <code>null</code>, or the new, merged, range.
     */
    public ArrayValueRange tryToMergeWith(final ArrayValueRange r) {
        ArrayValue start = null;
        final ArrayValue rUpToValue = r.endValue.increment();
        final ArrayValue upToValue = endValue.increment();

        if (ArrayValue.isLessThan(startValue, r.startValue)) {
            // Range 'r' starts before this range.
            if (ArrayValue.isGreaterOrEqual(startValue, rUpToValue)) {
                start = r.startValue;
            }
        } else if (ArrayValue.isLessOrEqual(upToValue, r.startValue)) {
            // Range 'r' starts somewhere within our range.
            start = startValue;
        }
        if (start != null) {
            ArrayValue upTo;
            // There certainly is overlap. Now just pick the highest upTo value.
            if (ArrayValue.isLessThan(upToValue, rUpToValue)) {
                // Our upTo value is the largest.
                upTo = upToValue;
            } else {
                upTo = rUpToValue;
            }
            return new ArrayValueRange(start, upTo.decrement());
        }
        return null;
    }

    /**
     * Returns true iff this range represents a single value.
     * 
     * @return <code>true</code> iff this range represents a single value.
     */
    boolean isSingleValue() {
        return endValue.equals(startValue);
    }

    ArrayValue extractFirstValue() {
        return startValue.increment();
    }

    @Override
    public String toString() {
        return "[" + startValue + ".." + endValue + ']';
    }

    /**
     * Returns true iff the given value is contained in this range.
     * 
     * @param val
     *            The value to test.
     * @return <code>true</code> iff the value is contained in this range.
     */
    boolean contains(final ArrayValue val) {
        return ArrayValue.isGreaterOrEqual(startValue, val)
                && ArrayValue.isLessOrEqual(endValue, val);
    }
}