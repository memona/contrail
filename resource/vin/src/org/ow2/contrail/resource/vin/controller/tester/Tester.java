package org.ow2.contrail.resource.vin.controller.tester;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.controller.VinAdministratorAPI;
import org.ow2.contrail.resource.vin.controller.VinAdministratorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A VIN tester.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class Tester {
    static final Logger logger = LoggerFactory.getLogger(Tester.class);

    /**
     * Runs a test of the VIN controller and agents.
     * 
     * @param configFileNames
     *            The names of the files that describe the network
     *            configurations to use.
     * @param properties
     *            The properties for this run.
     * @return <code>true</code> iff all the tests completed without errors.
     * @throws VinError
     *             Thrown if there is a configuration error in the VIN setup.
     */
    public static boolean runTest(final List<String> configFileNames,
            final Properties properties) throws VinError {
        final VinAdministratorAPI administrator = VinAdministratorFactory
                .createVinAdministrator(properties);
        final ArrayList<TestRunner> testThreads = new ArrayList<TestRunner>();
        // First start all threads
        for (final String configFileName : configFileNames) {
            final TestRunner t = new TestRunner(administrator, configFileName);
            t.start();
            testThreads.add(t);
        }
        boolean testsPassed = true;
        // Then wait for all threads to finish.
        for (final TestRunner r : testThreads) {
            try {
                r.join();
                testsPassed &= r.testPassed;
            } catch (final InterruptedException e) {
                // Ignore
            }
        }
        final boolean pristinePools = administrator
                .verifyNoOutstandingResources();
        final boolean res = pristinePools & testsPassed;
        logger.debug("Test completed: pristinePools=" + pristinePools
                + " testPassed=" + testsPassed + " returning " + res);
        return res;
    }
}
