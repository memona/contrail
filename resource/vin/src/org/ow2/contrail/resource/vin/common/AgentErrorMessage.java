package org.ow2.contrail.resource.vin.common;

/**
 * A message sent by an agent to the coordinator, informing it that there has
 * been a fatal error.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class AgentErrorMessage extends Message {
    private static final long serialVersionUID = 1L;
    /** The identifier of the agent that reported this error. */
    public final String agentId;
    /** The error message. */
    public final String message;
    public final Throwable throwable;

    /**
     * @param agentId
     *            The identifier of the agent that reported this error.
     * @param message
     *            The error message of this error.
     * @param throwable
     *            The throwable of this error.
     */
    public AgentErrorMessage(final String agentId, final String message,
            Throwable throwable) {
        this.agentId = agentId;
        this.message = message;
        this.throwable = throwable;
    }

    @Override
    public String toString() {
        return "AgentErrorMessage[id=" + agentId + " message=" + message
                + " throwable=" + throwable.getLocalizedMessage() + ']';
    }
}
