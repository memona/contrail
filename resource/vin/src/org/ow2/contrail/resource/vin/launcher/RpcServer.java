package org.ow2.contrail.resource.vin.launcher;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thetransactioncompany.jsonrpc2.server.Dispatcher;

/**
 * The Launcher RPC server listens on a port, and on each connection it spawns a
 * thread to handle a launcher RPC for a single client.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class RpcServer extends Thread {
    private final int serverPortNumber;
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final API engine;

    /**
     * Creates a new instance of the RPC server.
     * 
     * @param engine
     *            The engine that handles the calls.
     * @param serverPortNumber
     *            The port number the RPC server listens on.
     * @param verbose
     *            Should we be verbose?
     */
    RpcServer(final API engine, final int serverPortNumber,
            final boolean verbose) {
        super("RpcServer spawner thread");
        this.engine = engine;
        this.serverPortNumber = serverPortNumber;
    }

    private static Dispatcher createDispatcher(final API engine) {
        assert engine != null;
        final Dispatcher dispatcher = new Dispatcher();
        dispatcher.register(new VINRequestHandler(engine));
        return dispatcher;
    }

    @Override
    public void run() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket();
            serverSocket.bind(new java.net.InetSocketAddress(serverPortNumber));
        } catch (final IOException e) {
            logger.error("Could not listen on port " + serverPortNumber);
            System.exit(2);
            return;
        }
        logger.info("The RPC server is listening on port " + serverPortNumber);

        while (!isInterrupted()) {
            Socket socket;
            try {
                socket = serverSocket.accept();
            } catch (final IOException e) {
                e.printStackTrace();
                return;
            }
            final Dispatcher dispatcher = createDispatcher(engine);
            final RpcServerThread t = new RpcServerThread(dispatcher, socket);
            logger.info("Starting an RPC server thread");
            t.start();
        }

        try {
            serverSocket.close();
        } catch (final IOException e) {
            // Ignore.
        }
    }

}