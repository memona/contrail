package org.ow2.contrail.resource.vin.launcher;

import java.io.File;
import java.util.Arrays;

class Parameters {
    boolean runRPCServer = false;
    boolean runLauncher = false;
    boolean runHub = false;
    boolean verbose = false;
    int rpcPortNumber = Settings.RPC_PORT_NUMBER;
    int ibisPortNumber = Settings.IBIS_PORT_NUMBER;
    String hostId = null;
    File launchCommand;

    Parameters(final String[] args) {
        for (final String arg : args) {
            if (arg.equals("-h") || arg.equals("-help") || arg.equals("--help")) {
                Launcher.usage(System.out);
                System.exit(0);
            } else if (arg.startsWith("-rpcPortNumber=")) {
                rpcPortNumber = Launcher.extractIntFromArg(arg);
                runRPCServer = true;
            } else if (arg.equalsIgnoreCase("-rpcserver")) {
                runRPCServer = true;
            } else if (arg.equalsIgnoreCase("-launcher")) {
                runLauncher = true;
            } else if (arg.equalsIgnoreCase("-hub")) {
                runHub = true;
            } else if (arg.equalsIgnoreCase("-verbose")) {
                verbose = true;
            } else if (arg.startsWith("-launchCommand=")) {
                launchCommand = Launcher.extractFileFromArg(arg);
            } else if (arg.startsWith("-hostId=")) {
                hostId = Launcher.extractStringFromArg(arg);
                runLauncher = true;
            } else if (arg.startsWith("-")) {
                System.err.println("Error: Unknown option '" + arg + "'");
                Launcher.usage(System.err);
                System.err.println("Actual arguments: "
                        + Arrays.deepToString(args));
                throw new Error("Bad command-line arguments");
            } else {
                System.err.println("Error: superfluous argument '" + arg + "'");
                Launcher.usage(System.err);
                System.err.println("Actual arguments: "
                        + Arrays.deepToString(args));
                throw new Error("Bad command-line arguments");
            }
        }
    }
}