package org.ow2.contrail.resource.vin.agent;

import java.io.File;
import java.net.InetAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

import org.ow2.contrail.resource.vin.common.AgentContextType;
import org.ow2.contrail.resource.vin.common.ArrayValueRange;
import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.EncapsulationType;
import org.ow2.contrail.resource.vin.common.Subnet;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class AgentNetworkAdministration {
    private final HashMap<String, AgentNetworkDescriptor> networks = new HashMap<String, AgentNetworkDescriptor>();
    final File scriptDir;
    final File configDir;
    final File safeconfigPath;
    private final ArrayList<File> startScripts;
    private final ArrayList<File> stopScripts;
    final String libvirtId;
    final Properties properties = new Properties();
    private TunnelPool iproute2HostPool = null;
    AgentContextType context;
    private static final Logger logger = LoggerFactory
            .getLogger(AgentNetworkAdministration.class);

    AgentNetworkAdministration(final File scriptDir, final File configDir,
            final File safeconfigPath, final ArrayList<File> startScripts,
            final ArrayList<File> stopScripts, final String libvirtId) {
        this.scriptDir = scriptDir;
        this.configDir = configDir;
        this.safeconfigPath = safeconfigPath;
        this.startScripts = startScripts;
        this.stopScripts = stopScripts;
        this.libvirtId = libvirtId;
    }

    /**
     * Creates a new private network with the given name and encryption
     * algorithm, and returns an identifier for that network.
     * 
     * @param networkProperties
     *            The properties of the network to create.
     * @param networkType
     *            The type of network to create.
     * @param subnet
     * @param caUri
     *            The URI of the Certificate Authority of this VIN network.
     * @throws ConfigurationError
     *             Thrown if the configuration fails a sanity check.
     */
    void createNetwork(final EncapsulationType networkType,
            final String networkId, final Properties networkProperties,
            final Subnet subnet, final ArrayValueRange macAddresses,
            final URI caUri) throws ConfigurationError {
        final AgentNetworkDescriptor pn = new AgentNetworkDescriptor(this,
                networkProperties, networkType, networkId, subnet,
                macAddresses, caUri);
        networks.put(networkId, pn);
        pn.createNetwork();
        logger.debug("Created new network: networkType=" + networkType
                + " properties=" + networkProperties);
    }

    void removeNetwork(final String networkId) throws ConfigurationError {
        final AgentNetworkDescriptor nw = networks.remove(networkId);
        if (nw == null) {
            throw new VinInternalError("Unknown network, id='" + networkId
                    + '\'');
        }
        nw.removeNetwork();
    }

    void registerConnection(final AgentEngine engine, final int ordinal,
            final String networkId, final String connectionId,
            final String virtualMachine, final InetAddress addr,
            final InetAddress hostaddr, final String localHostId,
            final boolean ready, final Properties connectionProperties)
            throws ConfigurationError, VinError {
        final AgentNetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new VinInternalError("Unknown network, id='" + networkId
                    + '\'');
        }
        logger.trace("Adding configuration for networkId=" + networkId
                + " connectionId=" + connectionId);
        nw.registerConnection(engine, ordinal, virtualMachine, connectionId,
                addr, hostaddr, localHostId, ready, connectionProperties);
        logger.debug("Added connection: networkID=" + networkId
                + " virtualMachine=" + virtualMachine + " connectionId="
                + connectionId);
    }

    void removeConnection(final String networkId, final String connectionId,
            final String localHostId) throws ConfigurationError {
        final AgentNetworkDescriptor nw = networks.get(networkId);
        if (nw == null) {
            throw new VinInternalError("Unknown network: id='" + networkId
                    + '\'');
        }
        nw.ensureConnectionIdExists(connectionId);
        logger.trace("Removing configuration for networkId=" + networkId
                + " connectionId=" + connectionId);
        nw.removeConnection(connectionId, localHostId);
        logger.debug("Removed connection: networkID=" + networkId
                + " connectionId=" + connectionId);
    }

    /**
     * The agent is shutting down, try to clean up as much as we can.
     * 
     */
    public void onShutDown() {
        final Set<String> l = networks.keySet();
        for (final String key : l) {
            try {
                removeNetwork(key);
            } catch (final ConfigurationError e) {
                logger.debug("Cannot remove network '" + key + "'"
                        + e.getLocalizedMessage());
                // Nothing we can do about it.
            }
        }
    }

    TunnelPool getIproute2Pool() {
        if (iproute2HostPool == null) {
            iproute2HostPool = new TunnelPool("tunVingre");
        }
        return iproute2HostPool;
    }

    void setContextType(final AgentContextType ctx) {
        context = ctx;
    }

    void setVMProperties(final Properties prop) {
        properties.putAll(prop);
    }

    ArrayList<File> getNetworkStartScripts() {
        return startScripts;
    }

    ArrayList<File> getNetworkStopScripts() {
        return stopScripts;
    }

}
