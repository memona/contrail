package org.ow2.contrail.resource.vin.controller.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.ow2.contrail.resource.vin.common.ArrayValue;
import org.ow2.contrail.resource.vin.common.IPv4Subnet;
import org.ow2.contrail.resource.vin.common.IPv6Subnet;
import org.ow2.contrail.resource.vin.common.VinError;

class AddressRange {
    private final InetAddress address;
    private final ArrayValue mask;

    private AddressRange(final InetAddress address, final int addressBits) {
        super();
        this.address = address;
        final boolean isIPv4 = address.getAddress().length == 4;
        if (isIPv4) {
            mask = IPv4Subnet.buildNetmask(addressBits);
        } else {
            mask = IPv6Subnet.buildNetmask(addressBits);
        }
    }

    boolean contains(final InetAddress a1) {
        final byte[] a1bytes = a1.getAddress();
        final byte[] a2bytes = address.getAddress();
        if (a1bytes.length != a2bytes.length) {
            return false;
        }
        if (a1bytes.length == 4) {
            return IPv4Subnet.isInSubnet(a1bytes, a2bytes, mask.val);
        }
        return IPv6Subnet.isInSubnet(a1bytes, a2bytes, mask.val);
    }

    static AddressRange parseRange(final String ar) throws VinError {
        final String[] e = ar.split("/");
        if (e.length != 2) {
            throw new VinError("Malformed address range '" + ar
                    + "': format: <address>/<significant-bits>");
        }
        try {
            final InetAddress a = InetAddress.getByName(e[0]);
            final int bits = Integer.parseInt(e[1]);
            return new AddressRange(a, bits);
        } catch (final UnknownHostException e1) {
            throw new VinError("Malformed address '" + e[0] + "'", e1);
        } catch (final NumberFormatException e1) {
            throw new VinError("Malformed integer '" + e[1] + "' in '" + ar
                    + "'", e1);
        }
    }
}
