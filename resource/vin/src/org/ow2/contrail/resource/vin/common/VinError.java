package org.ow2.contrail.resource.vin.common;

/**
 * Exception thrown by the Vin framework.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class VinError extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new VinError with the given error message.
     * 
     * @param msg
     *            The error message.
     */
    public VinError(final String msg) {
        super(msg);
    }

    /**
     * Constructs a new VinError with the given error message and exception.
     * 
     * @param msg
     *            The error message.
     * @param e
     *            The exception that caused the error.
     */
    public VinError(final String msg, final Exception e) {
        super(msg, e);
    }
}
