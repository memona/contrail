package org.ow2.contrail.resource.vin.controller.impl;

import ibis.ipl.IbisCreationFailedException;

import java.io.IOException;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.ArrayValueRange;
import org.ow2.contrail.resource.vin.common.IPAddressRangePool;
import org.ow2.contrail.resource.vin.common.IPv4AddressRangePool;
import org.ow2.contrail.resource.vin.common.IPv6AddressRangePool;
import org.ow2.contrail.resource.vin.common.OutOfEntriesError;
import org.ow2.contrail.resource.vin.common.Subnet;
import org.ow2.contrail.resource.vin.common.Utils;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.ow2.contrail.resource.vin.controller.VinAPI;
import org.ow2.contrail.resource.vin.controller.VinAdministratorAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class VinAdministrator implements VinAdministratorAPI, ControllerServiceAPI {
    private static final Logger logger = LoggerFactory
            .getLogger(VinAdministrator.class);
    private final IntegerPool portNumberPool;
    private final IPAddressRangePool localAddressPool;
    private final MACPool macAddressPool;
    private final URI caFactoryUri;
    private final boolean verbose;
    private final AddressRanges hostNetworks;

    public VinAdministrator(final Properties properties) throws VinError {
        assert properties != null;
        final int firstPortNumber = Utils
                .getIntProperty(properties,
                        PropertyFileKeys.FIRST_CONTROLLER_PORT,
                        "The start of the range of port numbers to be used by VIN central controllers");
        final int lastPortNumber = Utils
                .getIntProperty(properties,
                        PropertyFileKeys.LAST_CONTROLLER_PORT,
                        "The end of the range of port numbers to be used by VIN central controllers");
        portNumberPool = new IntegerPool("Port number pool", firstPortNumber,
                lastPortNumber);
        final String firstMacAddress = Utils.getStringProperty(properties,
                PropertyFileKeys.MAC_ADDRESS_START,
                "The start of the range of MAC addresses for virtual NICs");
        final String lastMacAddress = Utils.getStringProperty(properties,
                PropertyFileKeys.MAC_ADDRESS_END,
                "The end of the range of MAC addresses for virtual NICs");
        macAddressPool = new MACPool(firstMacAddress, lastMacAddress);
        this.verbose = Utils.getBooleanProperty(properties,
                PropertyFileKeys.VERBOSE_CONTROLLER, "Verbose controller");
        final String startAddress = Utils.getStringProperty(properties,
                PropertyFileKeys.LOCAL_ADDRESS_START,
                "Start of the local addresses");
        final String endAddress = Utils.getStringProperty(properties,
                PropertyFileKeys.LOCAL_ADDRESS_END,
                "End of the local addresses");
        final String hostAddressesString = Utils.getStringProperty(properties,
                PropertyFileKeys.HOST_NETWORKS, "Host networks");
        final String caFactoryUriString = properties
                .getProperty(PropertyFileKeys.CA_FACTORY_URI);
        caFactoryUri = caFactoryUriString == null ? null : URI
                .create(caFactoryUriString);
        hostNetworks = AddressRanges.parseRangesString(hostAddressesString);
        try {
            if (startAddress.contains(":")) {
                // Presumably an IPv6 address
                localAddressPool = new IPv6AddressRangePool(startAddress,
                        endAddress, "local address");
            } else {
                localAddressPool = new IPv4AddressRangePool(startAddress,
                        endAddress, "local address");
            }
        } catch (final UnknownHostException e) {
            throw new VinError("Malformed local address range", e);
        }
    }

    private void onVinStopped(final int ibisPortNumber) {
        portNumberPool.recycleValue(ibisPortNumber);
    }

    private VinAPI createEngine(final int ibisPortNumber,
            final ControllerServiceAPI vinAdministrator) {
        ControllerEngine egn;
        try {
            final String poolName = "vinpool" + ibisPortNumber;
            System.setProperty("ibis.pool.name", poolName);
            egn = new ControllerEngine(ibisPortNumber, verbose, poolName,
                    vinAdministrator, hostNetworks, caFactoryUri);
            egn.start();
        } catch (final IbisCreationFailedException e) {
            logger.error("Could not create ibis: ", e);
            throw new VinInternalError("Could not create engine", e);
        } catch (final InterruptedException e) {
            logger.error("Main thread got interrupt", e);
            throw new VinInternalError("Could not create engine", e);
        } catch (final IOException e) {
            logger.error("I/O error", e);
            throw new VinInternalError("Could not create engine", e);
        } catch (final Exception e) {
            logger.error("Execution failed", e);
            throw new VinInternalError("Could not create engine", e);
        }
        return egn;
    }

    @Override
    public VinAPI createController() throws OutOfEntriesError {
        final int ibisPortNumber = portNumberPool.getNextValue();
        final VinAPI engine = createEngine(ibisPortNumber, this);
        logger.info("Starting a VIN controller, using port " + ibisPortNumber
                + " for Ibis");
        return engine;
    }

    @Override
    public void onControllerTerminated(final int ibisSocketNumber) {
        onVinStopped(ibisSocketNumber);
    }

    @Override
    public Subnet getNextSubnet(final long sz) throws OutOfEntriesError {
        return localAddressPool.getNextSubnet(sz);
    }

    @Override
    public ArrayValueRange getNextMacSubrange(final long members)
            throws OutOfEntriesError {
        return macAddressPool.getNextSubrange(members, 0);
    }

    @Override
    public void returnInetSubnet(final Subnet subnet) {
        localAddressPool.recycleSubnet(subnet);
    }

    @Override
    public void returnMACAddresses(final ArrayValueRange macAddresses) {
        macAddressPool.addToRecyclebin(macAddresses);
    }

    @Override
    public boolean verifyNoOutstandingResources() {
        boolean res = portNumberPool.isInPristineState();
        res &= localAddressPool.isInPristineState();
        res &= macAddressPool.isInPristineState();
        return res;
    }
}