package org.ow2.contrail.resource.vin.common;

/**
 * String constants for the names of properties.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class PropertyNames {

    /** The name of the network or VM. */
    public static final String NAME = "name";

    /** The domain of the network. */
    public static final String DOMAIN = "domain";

    public static final String MAXIMUM_NETWORK_SIZE_KEY = "maximumSize";
    public static final String AGENT_CONTEXT = "agent.context";
    public static final String TEMPLATE = "template";
    public static final String VM_TEMPLATE = "machine.template";
    public static final String APPLICATION_TEMPLATE = "application.template";
    public static final String APPLICATION_USER = "application.user";

    /** The config to run for testing purposes. */
    public static final String TESTCONFIG = "test.config";
    public static final String TESTSCRIPT = "test.script";

    /** The delay in seconds before we execute the test script. */
    public static final String TESTDELAY = "test.delay";

    public static final String DEBUG_FLAG = "debug";

}
