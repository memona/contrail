package org.ow2.contrail.resource.vin.common;

/**
 * A message from a coordinator to an agent, telling that a particular
 * connection to a private network should be removed.
 * 
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class NetworkConnectionRemovalMessage extends Message {
    private static final long serialVersionUID = 1L;
    /**
     * The identifier of the network the connection belongs to.
     */
    public final String networkId;
    /**
     * The identifier of the connection to delete.
     */
    public final String connectionId;

    /**
     * Constructs a new message.
     * 
     * @param networkId
     *            The identifier of the network the connection belongs to.
     * @param connectionId
     *            The identifier of the connection to delete.
     */
    public NetworkConnectionRemovalMessage(final String networkId,
            final String connectionId) {
        this.networkId = networkId;
        this.connectionId = connectionId;
    }

    @Override
    public String toString() {
        return NetworkConnectionRemovalMessage.class.getName() + "[networkId="
                + networkId + " connectionId=" + connectionId + "]";
    }

}
