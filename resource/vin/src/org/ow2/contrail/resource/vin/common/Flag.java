package org.ow2.contrail.resource.vin.common;

/**
 * A boolean with synchronous access.
 * 
 * @author Kees van Reeuwijk.
 */
public class Flag {
    private boolean flag;

    public Flag(final boolean flag) {
        this.flag = flag;
    }

    public void set() {
        set(true);
    }

    private synchronized void set(final boolean val) {
        final boolean changed = flag != val;
        flag = val;
        if (changed) {
            this.notifyAll();
        }
    }

    public synchronized boolean isSet() {
        return flag;
    }

}
