package org.ow2.contrail.resource.vin.controller.impl;

import java.net.InetAddress;
import java.util.Arrays;

import org.ow2.contrail.resource.vin.common.VinError;

class AddressRanges {
    private final AddressRange ranges[];

    private AddressRanges(final AddressRange l[]) {
        this.ranges = l;
    }

    static AddressRanges parseRangesString(final String s) throws VinError {
        final String al[] = s.split(" ");
        final AddressRange ranges[] = new AddressRange[al.length];
        for (int i = 0; i < al.length; i++) {
            ranges[i] = AddressRange.parseRange(al[i]);
        }
        return new AddressRanges(ranges);
    }

    boolean contains(final InetAddress a) {
        for (final AddressRange r : ranges) {
            if (r.contains(a)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return Arrays.deepToString(ranges);
    }
}
