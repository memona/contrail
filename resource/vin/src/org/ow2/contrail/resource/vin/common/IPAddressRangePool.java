package org.ow2.contrail.resource.vin.common;

import java.net.InetAddress;


public interface IPAddressRangePool {

    Subnet getNextSubnet(long sz) throws OutOfEntriesError;

    boolean isInPristineState();

    void recycleSubnet(Subnet subnet);

    InetAddress getNextAddress() throws OutOfEntriesError;

    void recycleAddress(InetAddress addr);

}
