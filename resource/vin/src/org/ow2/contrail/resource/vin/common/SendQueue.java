/**
 * 
 */
package org.ow2.contrail.resource.vin.common;

import ibis.ipl.IbisIdentifier;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;

/**
 * @author Kees van Reeuwijk
 * 
 */
class SendQueue {

    private final ConcurrentLinkedQueue<QueuedMessage> q = new ConcurrentLinkedQueue<QueuedMessage>();
    private final TimeStatistics queingStatistics = new TimeStatistics();
    private int maximalQueueLength = 0;

    void add(final IbisIdentifier destination, final Message msg) {
        q.add(new QueuedMessage(destination, msg));
        synchronized (this) {
            final int sz = q.size();
            if (maximalQueueLength < sz) {
                maximalQueueLength = sz;
            }
        }
    }

    void add(final QueuedMessage msg) {
        q.add(msg);
        synchronized (this) {
            final int sz = q.size();
            if (maximalQueueLength < sz) {
                maximalQueueLength = sz;
            }
        }
    }

    void addFirst(final IbisIdentifier destination, final Message msg) {
        // FIXME: actually add the emergency message to the front.
        q.add(new QueuedMessage(destination, msg));
        synchronized (this) {
            final int sz = q.size();
            if (maximalQueueLength < sz) {
                maximalQueueLength = sz;
            }
        }
    }

    QueuedMessage getNext() {
        final QueuedMessage msg = q.poll();
        if (msg != null) {
            final long waittime = System.currentTimeMillis() - msg.enqueueTime;
            queingStatistics.registerSample(waittime * 1e-3);
        }
        return msg;
    }

    boolean isEmpty() {
        return q.isEmpty();
    }

    int size() {
        return q.size();
    }

    synchronized void printStatistics(final Logger logger, final String name) {
        queingStatistics.printStatistics(logger, name + " linger time");
        logger.trace("  current length of " + name + ": " + q.size()
                + " maximal: " + maximalQueueLength);
    }

    void clear() {
        q.clear();
    }
}
