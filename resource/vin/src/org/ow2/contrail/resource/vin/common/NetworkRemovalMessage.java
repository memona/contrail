package org.ow2.contrail.resource.vin.common;

/**
 * A message from a coordinator to an agent, telling that a particular
 * connection to a private network should be removed.
 * 
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class NetworkRemovalMessage extends Message {
    private static final long serialVersionUID = 1L;
    /** The identifier of the network to remove. */
    public final String networkId;

    /**
     * Constructs a new removal message.
     * 
     * @param networkId
     *            The identifier of the network to remove.
     */
    public NetworkRemovalMessage(final String networkId) {
        this.networkId = networkId;
    }

    @Override
    public String toString() {
        return NetworkRemovalMessage.class.getName() + "[networkId="
                + networkId + "]";
    }
}
