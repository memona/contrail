package org.ow2.contrail.resource.vin.controller.tester;

import java.util.ArrayList;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.ExecuteCommandThread;
import org.ow2.contrail.resource.vin.common.VinError;

public final class TestExecuteCommandThread {
    /**
     * @param args
     *            The command-line parameters
     */
    public static void main(final String[] args) {
        System.out.println("Starting");
        final String id = "id";
        try {
            final ArrayList<VINNetworkInfo> runningMachines = new ArrayList<VINNetworkInfo>();
            final Properties properties = new Properties();
            properties.put("machine", "localhost");
            final ExecuteCommandThread t = TestRunner.deployPhysicalMachine(id,
                    runningMachines, properties, "fakepool", "fakeaddress");
            t.join();
        } catch (final InterruptedException e) {
            System.err.println("Interrupted: " + e.getLocalizedMessage());
        } catch (final VinError e) {
            System.err.println("VIN error: " + e.getLocalizedMessage());
        }
    }
}
