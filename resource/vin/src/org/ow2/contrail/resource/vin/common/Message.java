package org.ow2.contrail.resource.vin.common;

import ibis.ipl.IbisIdentifier;

import java.io.Serializable;

/**
 * The abstract superclass of all messages that are sent in the VIN.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public abstract class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * The source of this message.
     */
    public transient IbisIdentifier source;

    @Override
    public String toString() {
        return Utils.toStringClassScalars(this);
    }
}
