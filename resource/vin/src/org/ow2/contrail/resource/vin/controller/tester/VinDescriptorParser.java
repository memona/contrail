package org.ow2.contrail.resource.vin.controller.tester;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;

import org.ow2.contrail.resource.vin.annotations.Nullable;
import org.ow2.contrail.resource.vin.common.VinError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

class VinDescriptorParser {
    public final static Logger parserLogger = LoggerFactory
            .getLogger("org.ow2.contrail.resource.vin.controller.parser");

    static void write(final VinDescriptor tree, final File file)
            throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final ObjectNode node = mapper.createObjectNode();
        tree.addJson(mapper, node);
        mapper.writeValue(file, node);
    }

    private static VinDescriptor read(final File file)
            throws JsonParseException, IOException, VinError {
        final ObjectMapper m = new ObjectMapper();
        final JsonNode rootNode = m.readValue(file, JsonNode.class);
        return VinDescriptor.readJSon(rootNode);
    }

    static @Nullable
    VinDescriptor load(final File f) {
        try {
            return read(f);
        } catch (final JsonParseException e) {
            parserLogger.error("Bad json file '" + f + "':"
                    + e.getLocalizedMessage());
        } catch (final IOException e) {
            parserLogger.error("Cannot read json file '" + f + "':"
                    + e.getLocalizedMessage());
        } catch (final VinError e) {
            parserLogger.error("Cannot read json file '" + f + "':"
                    + e.getLocalizedMessage());
        }
        return null;
    }

    static Properties extractProperties(final JsonNode node) {
        final Properties res = new Properties();
        final Iterator<Entry<String, JsonNode>> fields = node.fields();
        while (fields.hasNext()) {
            final Entry<String, JsonNode> field = fields.next();
            final JsonNode n1 = field.getValue();
            res.put(field.getKey(), n1.asText());
        }
        return res;
    }
}
