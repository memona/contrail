package org.ow2.contrail.resource.vin.common;

/**
 * The possible types of network we can construct.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public enum EncapsulationType {
    /** strongSwan-managed IPSec network. */
    StrongSwan,

    /** Encapsulated network packages, as supported by the iproute2 command. */
    GRE,

    /** OpenVPN tunnels */
    OpenVPN
}