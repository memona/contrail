package org.ow2.contrail.resource.vin.launcher;

import org.ow2.contrail.resource.vin.common.Message;

/**
 * A message from the launcher to the coordinator, telling it the agent has been
 * launched.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class AgentLaunchedMessage extends Message {
    private static final long serialVersionUID = 1L;

    final String launchId;

    final String msg;

    final String stdoutString;

    final String stderrString;

    final int exitStatus;

    AgentLaunchedMessage(final String launchId, final int exitStatus) {
        this.launchId = launchId;
        this.exitStatus = exitStatus;
        msg = null;
        stdoutString = null;
        stderrString = null;
    }

    AgentLaunchedMessage(final String launchId, final int exitStatus,
            final String msg, final String stdoutString,
            final String stderrString) {
        this.launchId = launchId;
        this.exitStatus = exitStatus;
        this.msg = msg;
        this.stdoutString = stdoutString;
        this.stderrString = stderrString;
    }

    @Override
    public String toString() {
        return "AgentLaunchedMessage[launchId=" + launchId + ']';
    }

}
