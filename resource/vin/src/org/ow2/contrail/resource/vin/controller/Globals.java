package org.ow2.contrail.resource.vin.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Some global variables.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class Globals {

    /** A logger for all controller administration. */
    public final static Logger administrationLogger = LoggerFactory
            .getLogger("org.ow2.contrail.resource.vin.controller.administration");

}
