package org.ow2.contrail.resource.vin.launcher;

import java.util.Properties;

/**
 * The API of the central controller of the launcher infrastructure.
 * 
 * @author Kees van Reeuwijk
 * 
 */
interface API {
    /**
     * Request a new agent to be launched on the given host with the given
     * properties. The method only registers a request to launch the agent, so
     * the agent may not have been launched when this method returns. To ensure
     * that the agent has been launched, use the identifier of the launch event
     * that is returned by this method.
     * 
     * @param getIdentifier
     *            If set to {@code true}, return the identifier of this launch
     *            event. In this case, you <b>must</b> invoke
     *            {@code waitForLaunch()} exactly once at some time in the
     *            future to release the resources that are allocated to keep
     *            track of this launch request.
     * @param host
     *            The name of the host to launch on.
     * @param properties
     *            The properties of the new agent.
     * @return The id of this launch event
     */
    String requestAgentLaunch(boolean getIdentifier, String host,
            Properties properties);

    /**
     * Wait until the launch request with the given identifier has been
     * completed. If the agent has already been launched when this method is
     * invoked, it will return immediately. Otherwise, it will only return after
     * the agent has been launched.
     * <p>
     * Launch request identifiers are only returned when the parameter
     * {@code getIdentifier} has been set to {@code true}. When you request an
     * identifier, you must invoke {@code waitForLaunch()} exactly once for that
     * identifier. Not invoking this method will cause resource leaks, invoking
     * it more than once causes unspecified behaviour.
     * 
     * @param id
     *            The identifier of the launch request.
     * @return 0 on success (which currently is always)
     */
    int waitForLaunch(String id);

    /**
     * Stop this server.
     * 
     * @return 0 on success (which currently always)
     */
    int setStopped();
}
