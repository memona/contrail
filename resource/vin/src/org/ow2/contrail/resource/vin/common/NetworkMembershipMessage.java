package org.ow2.contrail.resource.vin.common;

import java.net.InetAddress;
import java.util.Properties;

/**
 * A message from a coordinator or an agent to an agent, telling it that the
 * given node is member of the given network.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class NetworkMembershipMessage extends Message {
    private static final long serialVersionUID = 1L;

    /** The ordinal number of the connection. */
    public final int ordinal;

    /** The identifier of the network to which this node is connected. */
    public final String networkId;

    /** The identifier of this connection. */
    public final String connectionId;

    /** The identifier of the connected machine. */
    public final String machine;

    /** The address assigned to this connection. */
    public final InetAddress address;

    /** The host address associated with this connection. */
    public final InetAddress hostAddress;

    /** Is the node ready for incoming connections? */
    public final boolean ready;

    public final Properties properties;

    /**
     * Constructs a new message with the given information.
     * 
     * @param ordinal
     *            The ordinal number of the connection.
     * @param networkId
     *            The identifier of the network to which this node is connected.
     * @param connectionId
     *            The identifier of this connection.
     * @param machine
     *            The identifier of the connected machine.
     * @param address
     *            The address assigned to this connection.
     * @param hostAddress
     *            The host address associated with this connection.
     * @param ready
     *            Is the node ready for incoming connections?
     * @param properties
     *            The properties of this connection
     */
    public NetworkMembershipMessage(final int ordinal, final String networkId,
            final String connectionId, final String machine,
            final InetAddress address, final InetAddress hostAddress,
            final boolean ready, final Properties properties) {
        this.ordinal = ordinal;
        this.networkId = networkId;
        this.connectionId = connectionId;
        this.machine = machine;
        this.address = address;
        this.hostAddress = hostAddress;
        this.ready = ready;
        this.properties = properties;
    }

    @Override
    public String toString() {
        return NetworkMembershipMessage.class.getName() + "[machine=" + machine
                + " network=" + networkId + " connection=" + connectionId
                + " address=" + address + " ready=" + ready + "]";
    }

}
