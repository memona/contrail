package org.ow2.contrail.resource.vin.launcher;

import java.util.Properties;

import org.ow2.contrail.resource.vin.common.Message;

/**
 * A message from the coordinator to a launcher, telling it to launch an agent.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class LaunchAgentMessage extends Message {
    private static final long serialVersionUID = 1L;

    /** The id of the host to start an agent on. */
    final String hostId;

    /** The id of the launch action. */
    final String launchId;

    /**
     * The properties of the agent.
     */
    final Properties properties;

    LaunchAgentMessage(final String launchId, final String hostId,
            final Properties properties) {
        this.launchId = launchId;
        this.hostId = hostId;
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "LaunchAgentMessage[lauchId=" + launchId + ",hostId=" + hostId
                + ",properties=" + properties + "]";
    }

}
