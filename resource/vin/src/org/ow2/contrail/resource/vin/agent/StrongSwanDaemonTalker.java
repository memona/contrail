package org.ow2.contrail.resource.vin.agent;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;

import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.code.juds.UnixDomainSocket;
import com.google.code.juds.UnixDomainSocketClient;
import com.googlecode.jsonrpc4j.JsonRpcClient;

/**
 * This class opens a socket to the StrongSwan daemon, and handles all
 * connection requests related to it.
 *
 * @author Kees van Reeuwijk
 * @author Teodor Crivat
 *
 */
class StrongSwanDaemonTalker {
    private static final File STRONGSWAN_UNIX_DOMAIN_SOCKET = new File(
            "/var/run/styx.sock");
    private static final Logger logger = LoggerFactory
            .getLogger(StrongSwanDaemonTalker.class);

    /**
     * A map from VIN connection identifiers to strongSwan tunnel identifiers.
     */
    private final HashMap<String, Integer> identifierMap = new HashMap<String, Integer>();
    private static int requestIdCounter = 0;

    private static Object doRpc(final Type returnType, final String command,
            final Object... args) throws IOException {
        final UnixDomainSocketClient sock;

        logger.debug("Sending RPC to " + STRONGSWAN_UNIX_DOMAIN_SOCKET
                + ": command=" + command + " args=" + Arrays.deepToString(args));
        try {
            final File f = STRONGSWAN_UNIX_DOMAIN_SOCKET;
            if (!f.exists()) {
                throw new VinInternalError("Socket file '" + f
                        + "' does not exist");
            }
            sock = new UnixDomainSocketClient(f.getAbsolutePath(),
                    UnixDomainSocket.SOCK_STREAM);
        } catch (final IOException e) {
            logger.error("Cannot connect to server at "
                    + STRONGSWAN_UNIX_DOMAIN_SOCKET);
            throw e;
        }
        logger.debug("Connected");
        final JsonRpcClient client = new JsonRpcClient();
        final InputStream is = sock.getInputStream();
        final OutputStream os = sock.getOutputStream();
        client.writeRequest(command, args, os,
                Integer.toString(requestIdCounter++));
        logger.debug("Request written");
        os.flush();
        os.close();
        logger.debug("Output stream closed");
        final Object rc;
        try {
            logger.debug("Waiting for response from server");
            rc = client.readResponse(returnType, is);
            logger.debug("Got back: " + rc);
        } catch (final Throwable e) {
            logger.error("Cannot read back response", e);
            throw new VinInternalError(
                    "Cannot read back response of RPC to strongSwan", e);
        } finally {
            is.close();
            sock.close();
        }
        return rc;
    }

    int createReceivingConnection(final String connectionId,
            final AgentConnectionDescriptor local) throws VinError {
        int res = 0;
        {
            final String cert = "dummy-cert.der";
            final Object args[] = { connectionId,
                    local.getHostAddressString(), "%any",
                    "leftcert=" + cert, "rightcert=" + cert,
                    "leftsubnet=" + local.getVINAddressString() + "/32",
                    "rightsubnet=" + local.network.getSubnetAddressString(),
                    "leftfirewall=yes",
                    "keyexchange=ikev2" };
            try {
                res = (Integer) doRpc(Integer.class, "addConfig", args);
            } catch (final IOException e) {
                throw new VinError(
                        "Failed to create a StrongSwan receiving connection: id="
                                + connectionId + " args=" + args, e);
            }
        }
        return res;
    }

    int createConnection(final String connectionId,
            final AgentConnectionDescriptor local,
            final AgentConnectionDescriptor remote) throws VinError {
        int res = 0;
        {
            final String cert = "dummy-cert.der";
            final Object args[] = { connectionId,
                    local.getHostAddressString(),
                    remote.getHostAddressString(),
                    "leftcert=" + cert, "rightcert=" + cert,
                    "leftsubnet=" + local.getVINAddressString() + "/32",
                    "rightsubnet=" + remote.getVINAddressString() + "/32",
                    "leftfirewall=yes",
                    "keyexchange=ikev2" };
            try {
                res = (Integer) doRpc(Integer.class, "addConfig", args);
            } catch (final IOException e) {
                throw new VinError(
                        "Failed to create a StrongSwan connection: id="
                                + connectionId + " args=" + args, e);
            }
        }
        {
            try {
                final Integer strongSwanId = (Integer) doRpc(Integer.class,
                        "connect", false, connectionId);
                identifierMap.put(connectionId, strongSwanId);
            } catch (final IOException e) {
                throw new VinError("Failed to create a StrongSwan tunnel: id="
                        + connectionId, e);
            }
        }
        return res;
    }

    void removeConnection(final String connectionId) throws VinError {
        try {
            final Integer strongSwanId = identifierMap.remove(connectionId);
            if (strongSwanId != null) {
                doRpc(Integer.class, "terminate", strongSwanId);
            }
            doRpc(Integer.class, "removeConfig", connectionId);
        } catch (final IOException e) {
            throw new VinError("Failed to remove a StrongSwan connection: id="
                    + connectionId, e);
        }
    }

    static String testConnection() throws IOException {
        final String s = (String) doRpc(String.class, "version", "xx");
        logger.debug("version test returns " + s);
        return s;
    }

}
