package org.ow2.contrail.resource.vin.launcher;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.OutOfEntriesError;
import org.ow2.contrail.resource.vin.common.VinError;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

class VINRequestHandler implements RequestHandler {
    private final API rpcHandler;

    VINRequestHandler(final API rpcHandler) {
        assert rpcHandler != null;
        this.rpcHandler = rpcHandler;
    }

    private static interface SingleRequestHandler {
        String getName();

        JSONRPC2Response process(Object reqId, List<?> params,
                MessageContext ctx) throws VinError, ConfigurationError,
                OutOfEntriesError;
    }

    private static JSONRPC2Error buildError(final String message) {
        final int code = JSONRPC2Error.INVALID_PARAMS.getCode();
        return new JSONRPC2Error(code, message);
    }

    private final class RequestAgentLaunchHandler implements
            SingleRequestHandler {
        @Override
        public String getName() {
            return "requestAgentLaunch";
        }

        @SuppressWarnings("synthetic-access")
        @Override
        public JSONRPC2Response process(final Object reqId,
                final List<?> params, final MessageContext ctx) throws VinError {
            if (params.size() != 3) {
                final JSONRPC2Error error = buildError("There are "
                        + params.size()
                        + " parameters, where exactly 3 are required");
                return new JSONRPC2Response(error, reqId);
            }
            final Boolean getIdentifier = (Boolean) params.get(0);
            final String host = (String) params.get(1);
            final Map<String, String> properties = (Map<String, String>) params
                    .get(2);
            final String res = rpcHandler.requestAgentLaunch(getIdentifier,
                    host, buildProperties(properties));
            return new JSONRPC2Response(res, reqId);
        }

        private Properties buildProperties(final Map<String, String> properties) {
            final Properties res = new Properties();
            for (final String key : properties.keySet()) {
                res.put(key, properties.get(key));
            }
            return res;

        }
    }

    private final class WaitForLaunchHandler implements SingleRequestHandler {
        @Override
        public String getName() {
            return "waitForLaunch";
        }

        @SuppressWarnings("synthetic-access")
        @Override
        public JSONRPC2Response process(final Object reqId,
                final List<?> params, final MessageContext ctx) throws VinError {
            if (params.size() != 1) {
                final JSONRPC2Error error = buildError("There are "
                        + params.size()
                        + " parameters, where exactly one is required");
                return new JSONRPC2Response(error, reqId);
            }
            final String identifier = (String) params.get(0);
            final int res = rpcHandler.waitForLaunch(identifier);
            return new JSONRPC2Response(res, reqId);
        }

    }

    private final class StopHandler implements SingleRequestHandler {

        @Override
        public String getName() {
            return "stop";
        }

        @SuppressWarnings("synthetic-access")
        @Override
        public JSONRPC2Response process(final Object reqId,
                final List<?> params, final MessageContext ctx) {
            if (params.size() != 0) {
                final JSONRPC2Error error = buildError("There are "
                        + params.size()
                        + "parameters, where exactly 0 are required");
                return new JSONRPC2Response(error, reqId);
            }
            final int res = rpcHandler.setStopped();
            return new JSONRPC2Response(res, reqId);
        }
    }

    @SuppressWarnings("synthetic-access")
    private final SingleRequestHandler[] requests = {
            new RequestAgentLaunchHandler(), new WaitForLaunchHandler(),
            new StopHandler() };

    /** Reports the method names of the handled requests. */
    @Override
    public String[] handledRequests() {
        final String res[] = new String[requests.length];
        for (int i = 0; i < res.length; i++) {
            res[i] = requests[i].getName();
        }
        return res;
    }

    /** Processes the requests. */
    @Override
    public JSONRPC2Response process(final JSONRPC2Request req,
            final MessageContext ctx) {

        final String methodName = req.getMethod();
        for (final SingleRequestHandler r : requests) {
            final String handlerName = r.getName();

            if (methodName.equals(handlerName)) {
                final List<Object> params = req.getPositionalParams();
                final Object reqId = req.getID();
                try {
                    return r.process(reqId, params, ctx);
                } catch (final VinError e) {
                    final String cmd = methodName
                            + Arrays.deepToString(params.toArray());
                    final JSONRPC2Error err = buildError("Command " + cmd
                            + " failed: " + e.getLocalizedMessage());
                    return new JSONRPC2Response(err, reqId);
                } catch (final ConfigurationError e) {
                    final String cmd = methodName
                            + Arrays.deepToString(params.toArray());
                    final JSONRPC2Error err = buildError("Command " + cmd
                            + " failed: " + e.getLocalizedMessage());
                    return new JSONRPC2Response(err, reqId);
                } catch (final OutOfEntriesError e) {
                    final String cmd = methodName
                            + Arrays.deepToString(params.toArray());
                    final JSONRPC2Error err = buildError("Command " + cmd
                            + " failed: " + e.getLocalizedMessage());
                    return new JSONRPC2Response(err, reqId);
                }
            }
        }
        return new JSONRPC2Response(JSONRPC2Error.METHOD_NOT_FOUND, req.getID());
    }
}