package org.ow2.contrail.resource.vin.controller.impl;

import java.net.URI;
import java.net.URISyntaxException;

import org.ow2.contrail.resource.vin.common.RESTTalker;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;

/**
 * Methods to talk with the CA factory.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class CAFactoryTalker {
    private final URI server;
    private final URI vinsURI;
    private final URI vinURI;
    private final String sessionId;
    private final RESTTalker restTalker;

    CAFactoryTalker(final URI server, final String sessionId) {
        this.server = server;
        this.vinsURI = server.resolve("/vins");
        this.vinURI = vinsURI.resolve("/" + sessionId);
        this.sessionId = sessionId;
        this.restTalker = new RESTTalker(sessionId);
    }

    URI registerOurSession() throws VinError {
        final String res = restTalker.sendPostRequest(vinsURI, "{'uid':'"
                + sessionId + "'}", RESTTalker.JSON_MIME);
        final String locStr = "Location: ";
        if (res.regionMatches(0, locStr, 0, locStr.length())) {
            final String locationUrl = res.substring(locStr.length());
            try {
                return new URI(locationUrl);
            } catch (final URISyntaxException e) {
                throw new VinInternalError(
                        "CA returned malformed location URL '" + locationUrl, e);
            }
        }
        throw new VinInternalError("CA returned malformed location string '"
                + res + "'");
    }

    void destroyOurSession() throws VinError {
        final String res = restTalker.sendDeleteRequest(vinURI);
    }

    URI createNetworkCA(final String networkId) throws VinError {
        final String res = restTalker.sendPostRequest(vinURI.resolve("/cas"),
                "{'uid':'" + networkId + "'}", RESTTalker.JSON_MIME);
        final String locStr = "Location: ";
        if (res.startsWith(locStr)) {
            final String locationUrl = res.substring(locStr.length());
            try {
                return new URI(locationUrl);
            } catch (final URISyntaxException e) {
                throw new VinInternalError(
                        "CA returned malformed location URL '" + locationUrl, e);
            }
        }
        throw new VinInternalError("CA returned malformed location string '"
                + res + "'");
    }

    void destroyNetworkCA(final String networkId) throws VinError {
        final String res = restTalker.sendDeleteRequest(vinURI.resolve("/cas/"
                + networkId));
    }

    URI requestCertificateSignature(final URI authority,
            final String certificate) throws VinError {
        final String res = restTalker.sendPostRequest(authority, certificate,
                RESTTalker.PLAIN_TEXT_MIME);
        final String locStr = "Location: ";
        if (res.startsWith(locStr)) {
            final String locationUrl = res.substring(locStr.length());
            try {
                return new URI(locationUrl);
            } catch (final URISyntaxException e) {
                throw new VinInternalError(
                        "CA returned malformed location URL '" + locationUrl, e);
            }
        }
        throw new VinInternalError("CA returned malformed location string '"
                + res + "'");
    }

}
