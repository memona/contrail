package org.ow2.contrail.resource.vin.controller.impl;

class PropertyFileKeys {

    static final String FIRST_CONTROLLER_PORT = "controller.ports.start";
    static final String LAST_CONTROLLER_PORT = "controller.ports.end";
    static final String VERBOSE_CONTROLLER = "controller.verbose";
    static final String LOCAL_ADDRESS_START = "controller.localaddresses.start";
    static final String LOCAL_ADDRESS_END = "controller.localaddresses.end";
    static final String MAC_ADDRESS_START = "controller.macaddresses.start";
    static final String MAC_ADDRESS_END = "controller.macaddresses.end";
    static final String HOST_NETWORKS = "hostnetworks";
    static final String CA_FACTORY_URI = "ca.uri.cafactory";
}
