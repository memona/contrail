package org.ow2.contrail.resource.vin.common;

/**
 * The possible contexts in which a VIN agent can be executed.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public enum AgentContextType {
    /** The agent runs in the client VM. */
    CLIENT,

    /** The agent runs on the host of the VM. */
    HOST,

    /** The agent runs on the physical machine. */
    PHYSICAL_MACHINE;
}
