package org.ow2.contrail.resource.vin.controller.tester;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.ow2.contrail.resource.vin.common.ConfigurationError;
import org.ow2.contrail.resource.vin.common.ExecuteCommandListener;
import org.ow2.contrail.resource.vin.common.ExecuteCommandThread;
import org.ow2.contrail.resource.vin.common.GlobalGlobals;
import org.ow2.contrail.resource.vin.common.OutOfEntriesError;
import org.ow2.contrail.resource.vin.common.PropertyNames;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;
import org.ow2.contrail.resource.vin.controller.VinAPI;
import org.ow2.contrail.resource.vin.controller.VinAdministratorAPI;
import org.ow2.contrail.resource.vin.onedeployer.OneDeployer;
import org.ow2.contrail.resource.vin.vepdeployer.VEPDeployer;

import com.fasterxml.jackson.core.JsonProcessingException;

final class TestRunner extends Thread {
    private static final String PHYSICAL_START_AGENT_SCRIPT = "/usr/share/contrail/support/vin/interface-scripts/physical-start-agent";
    private final VinAdministratorAPI administrator;
    private final String configFileName;
    boolean testPassed;
    private VinAPI engine;

    public TestRunner(final VinAdministratorAPI administrator,
            final String configFileName) {
        this.administrator = administrator;
        this.configFileName = configFileName;
    }

    private static MachineDescriptor findMachine(
            final ArrayList<MachineDescriptor> l, final String name) {
        for (final MachineDescriptor m : l) {
            if (m.name.equals(name)) {
                return m;
            }
        }
        return null;
    }

    private static String createPrivateNetwork(final VinAPI engine,
            final ArrayList<MachineDescriptor> machines, final Network nw)
            throws VinError, ConfigurationError, OutOfEntriesError {
        Tester.logger
                .info("Creating network '"
                        + nw.properties.get(PropertyNames.DOMAIN)
                        + "' from descriptor");
        GlobalGlobals.demoLogger.info("TESTER: creating network '"
                + nw.properties.get(PropertyNames.DOMAIN) + "'");
        final ArrayList<String> members = nw.machines;
        final String networkId = engine.addNetwork(nw.encapsulation,
                nw.properties);
        for (final String member : members) {
            final MachineDescriptor machine = findMachine(machines, member);
            if (machine == null) {
                Tester.logger.error("Unknown machine '" + member
                        + "' in name map " + machines);
                throw new VinInternalError("Unknown machine '" + member + "'");
            }
            final String connectionId = engine.addConnection(networkId,
                    machine.machineId, new Properties());
            Tester.logger.info("Added machine to network: vmId="
                    + machine.machineId + " (" + member + ") connectionId="
                    + connectionId);
            GlobalGlobals.demoLogger.info("TESTER: added machine "
                    + machine.machineId + " to network " + networkId);
        }
        return networkId;
    }

    static ExecuteCommandThread deployPhysicalMachine(final String machineId,
            final ArrayList<VINNetworkInfo> runningMachines,
            final Properties properties, final String poolName,
            final String ibisServerAddress) throws VinError {
        Tester.logger
                .debug("Starting agent on physical machine with properties "
                        + properties);
        final String user = properties.getProperty("user");
        final String machine = properties.getProperty("machine");
        if (machine == null) {
            throw new VinError("No machine specified; machine properties: "
                    + properties);
        }
        final String address;
        if (user == null) {
            address = machine;
        } else {
            address = user + "@" + machine;
        }
        final ExecuteCommandListener listener = new TesterExecuteCommandListener(
                Tester.logger);
        final ExecuteCommandThread xt;
        if (machine.equals("localhost")) {
            xt = new ExecuteCommandThread(listener,
                    PHYSICAL_START_AGENT_SCRIPT, machineId, ibisServerAddress,
                    poolName);
        } else {
            xt = new ExecuteCommandThread(listener, "/usr/bin/ssh", "-o",
                    "PasswordAuthentication=no", address,
                    PHYSICAL_START_AGENT_SCRIPT, machineId, ibisServerAddress,
                    poolName);
        }
        Tester.logger.debug("added physical machine: id=" + machineId
                + " properties=" + properties);
        xt.run();
        final VINNetworkInfo info = new PhysicalMachineInfo(machineId);
        runningMachines.add(info);
        return xt;
    }

    private void deployWithOpenNebula(
            final ArrayList<VINNetworkInfo> runningMachines,
            final ArrayList<MachineDescriptor> machines, final String poolName,
            final String ibisServerAddress) throws VinError {
        final OpenNebulaNetworkInfo info = new OpenNebulaNetworkInfo();
        for (final MachineDescriptor m : machines) {
            if (m.isPhysical) {
                final String machineId = engine
                        .registerPhysicalMachine(m.properties);
                m.machineId = machineId;
                deployPhysicalMachine(machineId, runningMachines, m.properties,
                        poolName, ibisServerAddress);
            } else {
                Tester.logger.debug("Requesting OpenNebula VM with properties "
                        + m.properties);
                GlobalGlobals.demoLogger
                        .info("TESTER: requesting OpenNebula VM with properties "
                                + m.properties);
                final String vmId = engine.registerVirtualMachine(m.properties);
                m.machineId = vmId;
                Tester.logger.debug("added OpenNebula VM id=" + vmId
                        + " properties=" + m.properties);
                final String templateName = m.properties
                        .getProperty(PropertyNames.TEMPLATE);
                if (templateName == null) {
                    throw new VinError("The OpenNebula deployer requires the '"
                            + PropertyNames.TEMPLATE
                            + "' property to be set: properties="
                            + m.properties);
                }
                final File templateFile = new File(templateName);
                final String oneId = OneDeployer.deployAgent(templateFile,
                        vmId, ibisServerAddress, poolName);
                info.registerVM(oneId);
            }
        }
        runningMachines.add(info);
    }

    private void deployWithVEP(final ArrayList<VINNetworkInfo> runningMachines,
            final VinDescriptor theVin, final String poolName,
            final String ibisServerAddress) throws VinError {
        final ArrayList<String> vmIds = new ArrayList<String>();
        final ArrayList<MachineDescriptor> machines = theVin.machines;
        String payload = "";
        for (final MachineDescriptor m : machines) {
            if (m.isPhysical) {
                final String machineId = engine
                        .registerPhysicalMachine(m.properties);
                m.machineId = machineId;
                deployPhysicalMachine(machineId, runningMachines, m.properties,
                        poolName, ibisServerAddress);
            } else {
                Tester.logger.debug("Requesting VEP VM with properties "
                        + m.properties);
                GlobalGlobals.demoLogger
                        .info("TESTER: requesting VEP VM with properties "
                                + m.properties);
                final String vmId = engine.registerVirtualMachine(m.properties);
                m.machineId = vmId;
                vmIds.add(vmId);
                Tester.logger.debug("adding VEP VM id=" + vmId + " properties="
                        + m.properties);
                final String vmTemplateName = m.properties
                        .getProperty(PropertyNames.VM_TEMPLATE);
                if (vmTemplateName == null) {
                    throw new VinError("The VEP deployer requires the '"
                            + PropertyNames.VM_TEMPLATE
                            + "' property to be set: properties="
                            + m.properties);
                }
                payload += VEPDeployer.createVMOvfFragment(vmTemplateName,
                        vmId, ibisServerAddress, poolName);
            }
        }
        final File applicationTemplateFile;
        {
            final String applicationTemplateName = theVin.properties
                    .getProperty(PropertyNames.APPLICATION_TEMPLATE);
            if (applicationTemplateName == null) {
                throw new VinError("The VEP deployer requires the '"
                        + PropertyNames.APPLICATION_TEMPLATE
                        + "' property to be set: properties="
                        + theVin.properties);
            }
            applicationTemplateFile = new File(applicationTemplateName);
        }

        final String user = theVin.properties
                .getProperty(PropertyNames.APPLICATION_USER);
        if (user == null) {
            throw new VinError("The VEP deployer requires the '"
                    + PropertyNames.APPLICATION_USER
                    + "' property to be set: properties=" + theVin.properties);
        }
        Tester.logger
                .trace("Deploying new cluster using VEP. applicationTemplate=["
                        + applicationTemplateFile + "] payload=[" + payload
                        + "]");
        final String appName = "VinTester" + poolName;
        final String applicationId = VEPDeployer.deployNetwork(user, appName,
                applicationTemplateFile, payload);
        final VEPNetworkInfo info = new VEPNetworkInfo(user, applicationId);
        runningMachines.add(info);
    }

    private void registerMachinesNoDeployment(
            final ArrayList<MachineDescriptor> machines) throws VinError {
        for (final MachineDescriptor m : machines) {
            if (m.isPhysical) {
                final String machineId = engine
                        .registerPhysicalMachine(m.properties);
                m.machineId = machineId;
                Tester.logger.debug("added physical machine: id=" + machineId
                        + " properties=" + m.properties);
            } else {
                final String vmId = engine.registerVirtualMachine(m.properties);
                m.machineId = vmId;
                Tester.logger.debug("added virtual machine: id=" + vmId
                        + " properties=" + m.properties);
            }
        }
    }

    /**
     * Given a VIN descriptor, create that VIN.
     *
     * @param engine
     *            The VIN engine to talk to to create the VIN networks.
     * @param theVin
     *            The VIN to create
     * @param agentDir
     *            The directory with agent information.
     * @return A list of network identifiers.
     * @throws VinError
     *             Thrown if the VIN cannot be created.
     * @throws OutOfEntriesError
     *             Thrown if there not enough entries in the one of the pools to
     *             run this test.
     * @throws ConfigurationError
     *             Thrown if the configuration of the test setup fails.
     */
    private ArrayList<String> createVIN(
            final ArrayList<VINNetworkInfo> runningMachines,
            final VinDescriptor theVin) throws VinError, ConfigurationError,
            OutOfEntriesError {
        GlobalGlobals.demoLogger
                .info("Creating VIN from descriptor; properties="
                        + theVin.properties);
        final ArrayList<MachineDescriptor> machines = theVin.machines;

        final String poolName = engine.getIbisPoolName();
        if (poolName == null) {
            throw new VinInternalError("Server returns a null pool name");
        }
        final String ibisServerAddress = engine.getIbisServerAddress();
        if (ibisServerAddress == null) {
            throw new VinInternalError(
                    "Server returns a null ibis server address");
        }
        final String deployer = theVin.deployer;
        if (deployer == null || deployer.equalsIgnoreCase("OpenNebula")) {
            deployWithOpenNebula(runningMachines, machines, poolName,
                    ibisServerAddress);
        } else if (deployer.equalsIgnoreCase("vep")) {
            deployWithVEP(runningMachines, theVin, poolName, ibisServerAddress);
        } else if (deployer.equals("none")) {
            /* If no deployer is set, al least register the machines
             * assuming they are deployed by other means */
            registerMachinesNoDeployment(machines);
        } else {
            throw new VinError("unknown deployer '" + deployer + '\'');
        }
        final ArrayList<String> networks = new ArrayList<String>();
        for (final Network nw : theVin.networks) {
            final String network = createPrivateNetwork(engine, machines, nw);
            networks.add(network);
        }
        return networks;
    }

    private static void destroyNetworks(final VinAPI engine,
            final ArrayList<String> networks) throws VinError {
        for (final String network : networks) {
            engine.removeNetwork(network);
        }
        engine.setStopped();
    }

    boolean runTest() throws IOException, VinError, ConfigurationError,
            OutOfEntriesError {
        final File configFile = new File(configFileName);
        VinDescriptor theVin;
        try {
            theVin = VinDescriptor.parseJsonFile(configFile);
            GlobalGlobals.demoLogger.info("TESTER: parsed VIN descriptor file "
                    + configFileName);
        } catch (final JsonProcessingException e) {
            Tester.logger.error("Cannot parse file '" + configFile + "': "
                    + e.getLocalizedMessage());
            throw e;
        } catch (final IOException e) {
            Tester.logger.error("Cannot read file '" + configFile + "': "
                    + e.getLocalizedMessage());
            throw e;
        } catch (final VinError e) {
            Tester.logger.error("Cannot read file '" + configFile + "': "
                    + e.getLocalizedMessage());
            throw e;
        }
        final ArrayList<VINNetworkInfo> runningNetworks = new ArrayList<VINNetworkInfo>();
        Tester.logger.debug("Creating VIN from description in file "
                + configFile);
        final ArrayList<String> networks = createVIN(runningNetworks, theVin);
        GlobalGlobals.demoLogger.info("Networks constructed, sleeping for "
                + theVin.lifetime + " seconds");
        try {
            Thread.sleep(theVin.lifetime * 1000L);
        } catch (final InterruptedException e) {
            // Ignore
        }
        destroyNetworks(engine, networks);
        try {
            Thread.sleep(2 * 1000L);
        } catch (final InterruptedException e) {
            // Ignore
        }
        for (final VINNetworkInfo e : runningNetworks) {
            e.destroy();
        }
        engine.setStopped();
        return engine.getTerminatedNormally();
    }

    @Override
    public void run() {
        try {
            engine = administrator.createController();
            testPassed = runTest();
        } catch (final IOException e) {
            engine.setStopped("Run failed: " + e.getLocalizedMessage(), false);
            testPassed = false;
        } catch (final ConfigurationError e) {
            engine.setStopped("Run failed: " + e.getLocalizedMessage(), false);
            testPassed = false;
        } catch (final OutOfEntriesError e) {
            engine.setStopped("Run failed: " + e.getLocalizedMessage(), false);
            testPassed = false;
        } catch (final VinInternalError e) {
            engine.setStopped("Run failed: " + e.getLocalizedMessage(), false);
            testPassed = false;
        } catch (final VinError e) {
            engine.setStopped("Run failed: " + e.getLocalizedMessage(), false);
            testPassed = false;
        }
    }

}
