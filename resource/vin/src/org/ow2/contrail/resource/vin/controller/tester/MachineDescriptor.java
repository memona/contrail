package org.ow2.contrail.resource.vin.controller.tester;

import java.util.Properties;
import java.util.Set;

import org.ow2.contrail.resource.vin.common.VinError;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * All the necessary information about one virtual or physical machine.
 * 
 * @author Kees van Reeuwijk
 * 
 */
class MachineDescriptor {
    /** The name of the machine. Only used for debugging. */
    final String name;

    /** Is this a physical machine (rather than a virtual one)? */
    final boolean isPhysical;

    /** The properties of the machine. */
    final Properties properties;

    /** The internal identifier of this machine. */
    String machineId = null;

    private MachineDescriptor(final String name, final boolean isPhysicial,
            final Properties properties) {
        assert name != null;
        this.name = name;
        this.isPhysical = isPhysicial;
        this.properties = properties;
    }

    @Override
    public int hashCode() {
        final int vName = name == null ? 0 : name.hashCode();
        final int vProperties = properties.hashCode();
        return vName ^ vProperties;
    }

    void addJSon(final ObjectMapper mapper, final ObjectNode node) {
        node.put("name", name);
        node.put("isPhysical", isPhysical);
        if (!properties.isEmpty()) {
            final ObjectNode pn = mapper.createObjectNode();
            final Set<String> keys = properties.stringPropertyNames();
            for (final String key : keys) {
                pn.put(key, properties.getProperty(key));
            }
            node.put("properties", pn);
        }
    }

    static MachineDescriptor extractJson(final JsonNode node) throws VinError {
        String nm;
        boolean isPhysical;
        final Properties properties;
        {
            final JsonNode nameNode = node.path("name");
            if (nameNode == null) {
                throw new VinError("No name specified");
            }
            nm = nameNode.asText();
        }
        {
            final JsonNode phNode = node.path("isPhysical");
            isPhysical = phNode.asBoolean(false);
        }
        final com.fasterxml.jackson.databind.JsonNode propertiesNode = node
                .path("properties");
        if (propertiesNode == null) {
            properties = new Properties();
        } else {
            properties = VinDescriptorParser.extractProperties(propertiesNode);
        }
        return new MachineDescriptor(nm, isPhysical, properties);
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof MachineDescriptor)) {
            return false;
        }
        final MachineDescriptor that = (MachineDescriptor) obj;
        return properties.equals(that.properties);
    }

}
