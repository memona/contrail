package org.ow2.contrail.resource.vin.agent;

import java.net.URI;
import java.net.URISyntaxException;

import org.ow2.contrail.resource.vin.common.RESTTalker;
import org.ow2.contrail.resource.vin.common.VinError;
import org.ow2.contrail.resource.vin.common.VinInternalError;

/**
 * Methods to talk with the CA for a particular VIN network.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class CATalker {
    private final URI authority;
    private final URI vinsURI;
    private final RESTTalker restTalker;

    CATalker(final URI authority, final String user) {
        this.authority = authority;
        this.vinsURI = authority.resolve("/vins");
        this.restTalker = new RESTTalker(user);
    }

    URI registerOurSession(final String sessionId) throws VinError {
        final String res = restTalker.sendPostRequest(vinsURI, "{'uid':'"
                + sessionId + "'}", RESTTalker.JSON_MIME);
        final String locStr = "Location: ";
        if (res.regionMatches(0, locStr, 0, locStr.length())) {
            final String locationUrl = res.substring(locStr.length());
            try {
                return new URI(locationUrl);
            } catch (final URISyntaxException e) {
                throw new VinInternalError(
                        "CA returned malformed location URL '" + locationUrl
                                + '\'', e);
            }
        }
        throw new VinInternalError("CA returned malformed location string '"
                + res + "'");
    }

    public String requestCertificateSignature(final String csr) throws VinError {
        final String res = restTalker.sendPostRequest(authority, csr,
                RESTTalker.PLAIN_TEXT_MIME);
        return res;
    }

}
