package org.ow2.contrail.resource.vin.vepdeployer;

import java.net.URI;

import org.ow2.contrail.resource.vin.common.RESTTalker;
import org.ow2.contrail.resource.vin.common.VinError;

public class WikipediaTest {

    /**
     * @param args
     *            The command-line arguments.
     */
    public static void main(final String[] args) {
        // http://services.gisgraphy.com//geocoding/geocode?address='1%20avenue%20des%20champs%20elysees%2075003%20paris'&country=FR
        final URI gisURI = URI
                .create("http://services.gisgraphy.com//geocoding");
        final RESTTalker wikiTalker = new RESTTalker("reeuwijk@few.vu.nl");
        // final String query =
        // "?format=json&action=query&list=search&srsearch=gps&srprop=timestamp";
        final String query = "geocode?address='1%20avenue%20des%20champs%20elysees%2075003%20paris'&country=FR";
        String res;
        try {
            final URI queryURI = gisURI.resolve(query);
            System.err.println("Query URI=[" + queryURI + "]");
            res = wikiTalker.requestGet(queryURI);
        } catch (final VinError e) {
            System.err.println("Failed: " + e.getLocalizedMessage());
            e.printStackTrace(System.err);
            System.exit(1);
            return;
        }
        System.out.println("Result: " + res);
    }
}
