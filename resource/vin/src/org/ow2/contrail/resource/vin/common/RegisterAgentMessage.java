package org.ow2.contrail.resource.vin.common;

import java.net.InetAddress;
import java.util.Arrays;

/**
 * A message sent by an agent to the coordinator, informing it that it is
 * available.
 * 
 * @author Kees van Reeuwijk
 * 
 */
public class RegisterAgentMessage extends Message {
    private static final long serialVersionUID = 1L;

    /** The identifier of this agent. */
    public final String agentId;

    /** The addresses of this agent. */
    public final InetAddress[] addresses;

    /**
     * Constructs a new RegisterAgentMessage.
     * 
     * @param agentId
     *            The identifier of this agent.
     * @param addresses
     *            The addresses of this agent.
     */
    public RegisterAgentMessage(final String agentId,
            final InetAddress[] addresses) {
        this.agentId = agentId;
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return "RegisterAgentMessage[id=" + agentId + " addr="
                + Arrays.deepToString(addresses) + ']';
    }
}
