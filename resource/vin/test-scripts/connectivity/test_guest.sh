#!/bin/bash
#
# Usage:
#   test_guest <ip-address>
#
# Where <ip-address> is an address of a machine to be tested for connectivity.

die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 1 ] || die "1 arguments required, $# provided: [$@]"

IP_ADDR=$1

#parameters sanity check
echo "$IP_ADDR" | grep -E -q '^[[:xdigit:]:.]+$' || die "Incorrect IP address '$IP_ADDR'"

#check whether there is a connection to given IP address
ping -c 1 "$IP_ADDR">/dev/null; [ $? -eq 0 ]
rv=$?
if [ $rv -eq 0 ]
then
    echo "$IP_ADDR UP"
    #traceroute to destination
    traceroute "$IP_ADDR"
else
    echo "$IP_ADDR DOWN"
fi