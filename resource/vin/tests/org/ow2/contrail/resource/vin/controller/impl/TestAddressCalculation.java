/**
 * Test MAC address conversion function.
 */
package org.ow2.contrail.resource.vin.controller.impl;

import junit.framework.Assert;

import org.junit.Test;
import org.ow2.contrail.resource.vin.common.IPv4AddressRangePool;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestAddressCalculation {
    private static void testCalculateAlignmentBits(int members,
            int correctAlignment) {
        final int bits = IPv4AddressRangePool.calculateAlignmentBits(members);
        Assert.assertEquals(correctAlignment, bits);
    }

    /**
     * Test alignment computation
     * 
     */
    @Test()
    public void byteArrayOffsetTest() {
        testCalculateAlignmentBits(1, 0);
        testCalculateAlignmentBits(3, 2);
        testCalculateAlignmentBits(4, 2);
        testCalculateAlignmentBits(123, 7);
        testCalculateAlignmentBits(65533, 16);
    }
}
