/**
 * Test host context functions.
 */
package org.ow2.contrail.resource.vin.agent;

import java.io.PrintWriter;
import java.io.StringWriter;

import junit.framework.Assert;

import org.junit.Test;
import org.ow2.contrail.resource.vin.common.ArrayValue;
import org.ow2.contrail.resource.vin.common.ArrayValueRange;
import org.ow2.contrail.resource.vin.common.IPv4Subnet;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestHostContext {
    private static final String TEST_DOMAIN = "test.local";

    private static void testMapping(final int alignBits,
            final ArrayValue startValue, final ArrayValue endValue,
            final ArrayValue macStartValue, final ArrayValue macEndValue,
            final String ref) {
        final ArrayValueRange range = new ArrayValueRange(startValue, endValue);
        final ArrayValueRange macAddresses = new ArrayValueRange(macStartValue,
                macEndValue);
        final IPv4Subnet subnet = new IPv4Subnet(range, alignBits);
        final StringWriter wr = new StringWriter();
        final PrintWriter out = new PrintWriter(wr);
        final String bridgeName = "testbridge";
        Iproute2HostNetworkHandler.writeNetworkXMLFile(subnet, TEST_DOMAIN,
                out, bridgeName, macAddresses);
        final String s = wr.toString();
        Assert.assertEquals(ref, s);
    }

    private static final String reference = "<network>\n"
            + " <name>testbridge</name>\n"
            + " <bridge name='testbridge'/>\n"
            + " <forward mode='route'/>\n"
            + " <ip address='192.168.1.1' netmask='255.255.255.248'>\n"
            + "  <dhcp>\n"
            + "   <range start='192.168.1.2' end='192.168.1.6'/>\n"
            + "   <host mac=\"00:01:02:03:04:02\" ip=\"192.168.1.2\" name=\"vm2.test.local\"/>\n"
            + "   <host mac=\"00:01:02:03:04:03\" ip=\"192.168.1.3\" name=\"vm3.test.local\"/>\n"
            + "   <host mac=\"00:01:02:03:04:04\" ip=\"192.168.1.4\" name=\"vm4.test.local\"/>\n"
            + "   <host mac=\"00:01:02:03:04:05\" ip=\"192.168.1.5\" name=\"vm5.test.local\"/>\n"
            + "   <host mac=\"00:01:02:03:04:06\" ip=\"192.168.1.6\" name=\"vm6.test.local\"/>\n"
            + "  </dhcp>\n" + " </ip>\n" + "</network>\n";

    /**
     * Test IPv4 byte array to string conversion.
     * 
     */
    @Test()
    public void mappingTest() {
        final int alignBits = 3;
        final ArrayValue startValue = new ArrayValue((byte) 192, (byte) 168, 1,
                0);
        final ArrayValue endValue = new ArrayValue((byte) 192, (byte) 168, 1,
                (1 << alignBits) - 1);
        final ArrayValue macStartValue = new ArrayValue(0, 1, 2, 3, 4, 0);
        final ArrayValue macEndValue = new ArrayValue(0, 1, 2, 3, 4,
                (1 << alignBits) - 1);
        testMapping(alignBits, startValue, endValue, macStartValue,
                macEndValue, reference);
    }
}
