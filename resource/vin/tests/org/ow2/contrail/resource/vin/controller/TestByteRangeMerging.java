/**
 * Test the merging of byte array ranges.
 */
package org.ow2.contrail.resource.vin.controller;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.contrail.resource.vin.common.ArrayValue;
import org.ow2.contrail.resource.vin.common.ArrayValueRange;

/**
 * @author Kees van Reeuwijk
 * 
 */
@SuppressWarnings("static-method")
public class TestByteRangeMerging {

    private static ArrayValue buildArray(final int len, final int value) {
        final byte res[] = new byte[len];

        int val = value;
        int ix = len;
        while (val != 0 && ix > 0) {
            ix--;
            final byte v = (byte) (val & 0xFF);
            res[ix] = v;
            val >>= 8;
        }
        return new ArrayValue(res);
    }

    private static void assertArrayValuesAreEqual(ArrayValue a, ArrayValue b) {
        Assert.assertArrayEquals(a.val, b.val);
    }

    /**
     * Given two overlapping value ranges, build byte ranges for them, and make
     * sure they are merged properly.
     * 
     * @param len
     *            The length of the byte arrays to use.
     * @param starta
     *            Start of first range.
     * @param enda
     *            End of first range.
     * @param startb
     *            Start of second range.
     * @param endb
     *            End of second range.
     * @param startres
     *            Expected start of merged range.
     * @param endres
     *            Expected end of merged range.
     */
    private static void assertIsMerged(final int len, final int starta,
            final int enda, final int startb, final int endb,
            final int startres, final int endres) {
        final ArrayValueRange a = new ArrayValueRange(buildArray(len, starta),
                buildArray(len, enda));
        final ArrayValueRange b = new ArrayValueRange(buildArray(len, startb),
                buildArray(len, endb));
        {
            final ArrayValueRange res = a.tryToMergeWith(b);
            Assert.assertNotNull(res);
            assertArrayValuesAreEqual(buildArray(len, startres), res.startValue);
            assertArrayValuesAreEqual(buildArray(len, endres), res.endValue);
        }
        {
            final ArrayValueRange res = b.tryToMergeWith(a);
            Assert.assertNotNull(res);
            assertArrayValuesAreEqual(buildArray(len, startres), res.startValue);
            assertArrayValuesAreEqual(buildArray(len, endres), res.endValue);
        }
    }

    private static void assertIsNotMerged(final int len, final int starta,
            final int enda, final int startb, final int endb) {
        final ArrayValueRange a = new ArrayValueRange(buildArray(len, starta),
                buildArray(len, enda));
        final ArrayValueRange b = new ArrayValueRange(buildArray(len, startb),
                buildArray(len, endb));
        final ArrayValueRange res = a.tryToMergeWith(b);
        Assert.assertNull(res);
        final ArrayValueRange rres = b.tryToMergeWith(a);
        Assert.assertNull(rres);
    }

    /**
     * Test handling of range merger.
     */
    @Test
    public void mergeTest() {
        assertIsMerged(2, 0, 256 + 4, 4, 8, 0, 256 + 4);
        assertIsMerged(1, 0, 3, 4, 8, 0, 8);
        assertIsMerged(1, 0, 4, 4, 8, 0, 8);
        assertIsMerged(1, 0, 5, 4, 8, 0, 8);
        assertIsNotMerged(1, 0, 2, 4, 8);
        assertIsMerged(2, 0, 3, 4, 8, 0, 8);
        assertIsMerged(2, 0, 4, 4, 8, 0, 8);
        assertIsMerged(2, 0, 5, 4, 8, 0, 8);
        assertIsNotMerged(2, 0, 2, 4, 8);
        assertIsMerged(2, 256 + 0, 256 + 3, 256 + 4, 256 + 8, 256 + 0, 256 + 8);
        assertIsMerged(2, 256 + 0, 256 + 4, 256 + 4, 256 + 8, 256 + 0, 256 + 8);
        assertIsMerged(2, 256 + 0, 256 + 5, 256 + 4, 256 + 8, 256 + 0, 256 + 8);
        assertIsNotMerged(2, 256 + 0, 256 + 2, 256 + 4, 256 + 8);
        assertIsNotMerged(2, 256 + 0, 256 + 4, 4, 8);
        assertIsMerged(4, 0, 3, 4, 8, 0, 8);
        assertIsMerged(4, 0, 4, 4, 8, 0, 8);
        assertIsMerged(4, 0, 5, 4, 8, 0, 8);
        assertIsNotMerged(4, 0, 2, 4, 8);
        assertIsMerged(6, 0, 3, 4, 8, 0, 8);
        assertIsMerged(6, 0, 4, 4, 8, 0, 8);
        assertIsMerged(6, 0, 5, 4, 8, 0, 8);
        assertIsNotMerged(6, 0, 2, 4, 8);
    }

}
