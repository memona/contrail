# Usage:
#   safeconfig remove-openvpn-host-route.conf <bridge-name> <local-ip> <local-netmask> <remote-ip> <tunnel>
#
# Where <bridge-name> is a libvirt network name, <local-ip> is a local IP
# address of a libvirt network, <local-netmask> is a netmask of a libvirt
# network, <remote-ip> is a remote VM IP address and <tunnel> is a name
# of an interface through which traffic has been routed.

parameters TUNNEL LOCAL_VIN REMOTE_VIN NETMASKBITS BR_NAME

#parameter sanity checks
assertmatch [[:alnum:]_-]+ $(BR_NAME)
assertmatch [[:xdigit:]:.]+ $(LOCAL_VIN)
assertmatch [[:xdigit:]:.]+ $(REMOTE_VIN)
assertmatch [[:alnum:]_-]+ $(TUNNEL)

assertmatch [[:digit:]][[:digit:]]? $(NETMASKBITS)

#remove routing entry
run /bin/ip route del $(REMOTE_VIN) dev $(TUNNEL)

#remove iptable rules
run /sbin/iptables -D FORWARD -s $(LOCAL_VIN)/$(NETMASKBITS) -d $(REMOTE_VIN)/32 -i $(BR_NAME) -o $(TUNNEL) -j ACCEPT
run /sbin/iptables -D FORWARD -s $(REMOTE_VIN)/32 -d $(LOCAL_VIN)/$(NETMASKBITS) -i $(TUNNEL) -o $(BR_NAME) -j ACCEPT

debugprint Removed tunnel '$(TUNNEL)':
debugrun /bin/ip tunnel show
debugprint Removed route:
debugrun /bin/ip route show
debugprint Iptables:
debugrun /sbin/iptables --list
