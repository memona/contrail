# Remove the local VIN address from the host
#
# Usage:
#     safeconfig remove-iproute2-physical-address.conf <local-interface-name>
#         <local-vin-address>

# IF_NAME:     The name of the local interface corresponding to this network
# LOCAL_VIN:   The IP address of the local VIN
parameters IF_NAME LOCAL_VIN

# Do sanity checks on the parameters.

# Require that an identifier only consists of alphanumeric characters, or 
#  '-'. (Note that the '-' must be at the end, otherwise it introduces a range.)
assertmatch [[:alnum:]-]+ $(IF_NAME)

# Require that the addresses only consist of hex digits or ':' or '.' characters.
# This matches both ipv4 and ipv6 addresses.
assertmatch [[:xdigit:]:.]+ $(LOCAL_VIN)

debugprint "IF_NAME=$(IF_NAME) LOCAL_VIN=$(LOCAL_VIN)"

# Take the interface down
run /bin/ip link set $(IF_NAME) down
debugprint Interface $(IF_NAME) is now down

# Remove the interface
run /bin/ip link del $(IF_NAME)
debugprint Removed interface $(IF_NAME)

debugrun /bin/ip addr show
