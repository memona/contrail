# Remove the route for the GRE tunnel
#
# Usage:
#     safeconfig remove-iproute2-physical-route.conf <tunnel-name>
#             <local-vin-address> <remote-vin-address>

# TUNNEL:      Name of the tunnel device to create
# LOCAL_VIN:   The address of the local VIN
# REMOTE_VIN:  The address of the remote VIN
parameters TUNNEL LOCAL_VIN REMOTE_VIN

# Require that a tunnel name only consists of alphanumeric characters, or the
# characters '_', ':', or  '-'. (Note that the '-' must be at the
# end, otherwise it introduces a range.)
assertmatch [[:alnum:]_:-]+ $(TUNNEL)

# Require that the addresses only consist of hex digits or ':' or '.' characters.
# This matches both ipv4 and ipv6 addresses.
assertmatch [[:xdigit:]:.]+ $(LOCAL_VIN)
assertmatch [[:xdigit:]:.]+ $(REMOTE_VIN)

debugprint "TUNNEL=$(TUNNEL) LOCAL_VIN=$(LOCAL_VIN) REMOTE_VIN=$(REMOTE_VIN)"

# Remove routing entry
run /bin/ip route del $(REMOTE_VIN)/32 dev $(TUNNEL) src $(LOCAL_VIN)

debugprint Removed route:
debugrun /bin/ip route show
