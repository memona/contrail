package org.ow2.contrail.resource.monitoringnotifications;

import com.mongodb.*;
import org.ow2.contrail.resource.monitoringnotifications.utils.Conf;

import java.net.UnknownHostException;
import java.util.Date;
import java.util.Properties;

public class MongoDBNotificationService implements INotificationService {
    private Mongo mongo;
    private DBCollection collection;

    MongoDBNotificationService(String mongoHost, int mongoPort, String databaseName,
                                 String collectionName) throws UnknownHostException {
        mongo = new Mongo(mongoHost, mongoPort);
        DB db = mongo.getDB(databaseName);
        collection = db.getCollection(collectionName);
    }

    MongoDBNotificationService(Properties props) throws UnknownHostException {
        String mongoHost = props.getProperty("monitoring-notifications.mongodb.host");
        int mongoPort = Integer.parseInt(props.getProperty("monitoring-notifications.mongodb.port"));
        String databaseName = props.getProperty("monitoring-notifications.mongodb.database");

        mongo = new Mongo(mongoHost, mongoPort);
        DB db = mongo.getDB(databaseName);
        collection = db.getCollection(Conf.MONGO_EVENTS_COLL_NAME);
    }

    @Override
    public void notify(EventSource eventSource, ActionType action) {
        DBObject record = new BasicDBObject();
        record.put("host", eventSource.getHost());
        record.put("oneId", eventSource.getOneId());
        record.put("vepId", eventSource.getVepId());
        record.put("slaId", eventSource.getSlaId());
        record.put("userUuid", eventSource.getUserUuid());
        record.put("appUuid", eventSource.getApplicationUuid());
        record.put("action", action.name());
        record.put("timestamp", new Date());

        collection.insert(record);
    }

    public void close() {
        try {
            mongo.close();
        }
        catch (Exception ignored) {
        }
    }
}
