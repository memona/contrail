/* File: error.c
 *
 * Error handling.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

#include "origin.h"
#include "tmdefs.h"
#include "error.h"
#include "global.h"
#include "sgstring.h"

static bool goterr = false;

/* Given an origin filename and line number, fill 'errpos' with that
 * origin info.
 */
static char *fileline_errpos( const char *fnm, unsigned int lineno )
{
    char *errpos;
    if( noerrorline ){
        (void) asprintf( &errpos, "%s", fnm );
    }
    else {
        (void) asprintf( &errpos, "%s:%u", fnm, lineno );
    }
    return errpos;
}

/* Given an origin 'org', fill 'errpos' with the origin info from 'org'. */
static char *orig_errpos( const origin *org )
{
    char *errpos;
    if( org != originNIL ){
        errpos = fileline_errpos( org->file, org->line );
    }
    else {
        errpos = NULL;
    }
    return errpos;
}

/* Central handler for all error and warning printing routines. */
static void vmessage( const char *errpos, const char *prefix, const char *msg, va_list args )
{
    if( errpos != stringNIL ){
        (void) fputs( errpos, stderr );
        (void) fputs( ": ", stderr );
    }
    if( prefix != NULL ){
        (void) fputs( prefix, stderr );
        (void) fputs( ": ", stderr );
    }
    (void) vfprintf( stderr, msg, args );
    (void) fputs( "\n", stderr );
}

/* Central handler of all error printing routines. */
static void verror( const char *errpos, const char *msg, va_list args )
{
    vmessage( errpos, NULL, msg, args );
    goterr = true;
}

/* Central handler of all internal error printing routines. */
static void vinternal( const char *errpos, const char *msg, va_list args )
{
    vmessage( errpos, "Internal error", msg, args );
    exit( EXIT_FAILURE );
}

/* Central handler of all fatal error printing routines. */
static void vfatal( const char *errpos, const char *msg, va_list args )
{
    vmessage( errpos, "Fatal error", msg, args );
    exit( EXIT_FAILURE );
}

/* Error printing routine: print error message 'msg'.
 *
 * Set a flag to indicate an error has occurred. If the function errcheck()
 * is called, it will cause an exit( EXIT_FAILURE ) if 'goterr' is now
 * true.
 */
void error( const char *msg, ... )
{
    va_list args;

    va_start( args, msg );
    verror( stringNIL, msg, args );
    va_end( args );
}

/** Fatal error printing routine: print error message 'msg' possibly preceded
 * by string in 'errpos'.
 */
void fatal_error( const char *msg, ... )
{
    va_list args;

    va_start( args, msg );
    vfatal( stringNIL, msg, args );
    va_end( args );
}

/* Handle an internal error. */
void internal_error( const char *msg, ... )
{
    va_list args;

    va_start( args, msg );
    vinternal( stringNIL, msg, args );
    va_end( args );
}

/* Given an origin 'org' and a message 'msg', fill errpos 
 * with the position of the symbol, and generate an
 * internal error message 'msg'.
 */
void origin_internal_error( const origin *org, const char *msg, ... )
{
    va_list args;

    char *errpos = orig_errpos(org);
    va_start( args, msg );
    vinternal( errpos, msg, args );
    va_end( args );
    fre_string(errpos);
}

/* Check if 'goterr' flag is set, and do exit(EXIT_FAILURE) if it is. */
void errcheck( void )
{
    if( goterr ){
        fprintf( stderr, "errors detected, program aborted\n" );
        exit( EXIT_FAILURE );
    }
}

/* Returns true iff there has been an error. */
bool error_seen()
{
    return goterr;
}

/* Given a filename, a line number, and a message 'msg', 
 * generate an error message.
 */
void fileline_error( const char *nm, unsigned int lineno, const char *msg, ... )
{
    va_list args;

    char *errpos = fileline_errpos(nm, lineno);
    fileline_errpos( nm, lineno );
    va_start( args, msg );
    verror( errpos, msg, args );
    va_end( args );
    fre_string(errpos);
}

/* Given an origin 's' and a message 'msg', fill errpos
 * with the position of the symbol, and generate error
 * message 'msg'.
 */
void origin_error( const origin *org, const char *msg, ... )
{
    va_list args;

    char *errpos = orig_errpos(org);
    va_start( args, msg );
    verror( errpos, msg, args );
    va_end( args );
    fre_string(errpos);
}

/* Given an origin 's' and a message 'msg', fill errpos
 * with the position of the symbol, and generate error
 * message 'msg'.
 */
void origin_fatal_error( const origin *org, const char *msg, ... )
{
    va_list args;

    char *errpos = orig_errpos(org);
    va_start( args, msg );
    vfatal( errpos, msg, args );
    va_end( args );
    fre_string(errpos);
}


/* System error handler.  */
void sys_error( int no, const char *msg, ... )
{
    va_list args;

    va_start( args, msg );
    (void) fputs( strerror( no ), stderr );
    (void) fputs( ": ", stderr );
    verror( stringNIL, msg, args );
    va_end( args );
}


/* System error handler.  */
void origin_sys_error( const origin *org, int no, const char *msg, ... )
{
    va_list args;

    char *errpos = orig_errpos(org);
    char *msg1;

    va_start( args, msg );
    asprintf(&msg1, "%s: %s", msg, strerror(no));
    verror( errpos, msg1, args );
    va_end( args );
    fre_string(errpos);
    fre_string(msg1);
}
