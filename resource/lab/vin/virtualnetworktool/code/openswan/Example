An example scenario
--------------------

Launching EC2 instances
• Create VPC
	– Goto VPC tab in AWS console
	– Create VPC with single subnet (use default settings)
• Security groups
	– While in VPC tab, click on security groups. From the security groups select default (if you are using default settings)
	– In the section below select ‘Inbound ’. Edit rules to accept all traffic (atleast protocol 50 ie ESP). Similarly edit ‘Outbound’ to
allow all the traffic.
• Creating an Elastic IP
	– Goto ‘EC2 tab’
	– In the network and security, click on Elastic IP
	– Allocate new address. EIP used in VPC
• Launch EC2 instance to VPC
	– While in EC2 tab. Click on ‘Instances’ and launch instance
	– Select a basic Amazon Linux AMI
	– Select a small instance and launch it into VPC (Micro instance doesn’t support VPC).
	– In the ‘Instance details’ choose default settings
	– Create/Download or use the keypair
	– In the firewall configuration accept all traffic
• Attach Elastic IP to the instance
	– Goto Elastic IP section
	– Select the newly created EIP. Click on Associate address and associate it with the newly created instance
• Configure instance
	– Login to the newly created instance
		∗ ssh -i keyfile ec2-user@ipaddress
	– Install OpenSwan
		∗ sudo apt-get install openswan
	– Configure ssh-keys
		∗ Copy public key of the Master node to \home\ec2-user\.ssh\authorized keys

Launching Rackspace instances
• Launch the server
	– In the rackspace control panel, select Hosting->Cloud server->Add server.
	– Launch an Ubuntu instance
• Configure instance
	– Login to the newly created instance
		∗ ssh root@ipaddress
	– Install OpenSwan
		∗ sudo apt-get install openswan
	– Create ssh-keys
		∗ ssh-keygen -t rsa
	– Configure ssh-keys
		∗ Copy public key of the Master node to \root\.ssh\authorized keys
Launching DAS4 instances
• Launch the instances using opennebula (specify key location in *.one file) (More information on DAS4 wiki)
• Configure instance
	– Login to the newly created instance
		∗ ssh -i keyfile root@ipaddress
	– Install OpenSwan
		∗ sudo apt-get install openswan
	– Configure ssh-keys
		∗ Copy public key of the Master node to \home\ec2-user\.ssh\authorized keys
• Set up one of the nodes as Master node
	– Install python lxml and fabric tools(> 1.0)
		∗ easy install lxml
		∗ easy install fabric

---------------------------------------------
Python tool usage is given in the README file
---------------------------------------------
