import argparse
import random
import os
import sys

from fabfile import Deploy
from fabric.api import *
from fabric.state import connections
from fabric.network import *
from lxml import etree

class Master:
    #Shows the list of all configured network
    def __showNetworkList(self):
        xml=etree.parse('network.xml')
        all=xml.findall("network")
        print("NetworkID\tEncryption\tNodes")
        for net in all:
            encrypt=net.find('encryption')
            nodes=net.findall('nodelist/IP')
            sys.stdout.write("%s\t\t%s\t\t"%(net.get('id'),encrypt.text))
            i=0
            for node in nodes:
                sys.stdout.write("%s "%(node.text))
                i=i+1
                if i%4 == 0:
                    print("")
                    sys.stdout.write("\t\t\t\t")
            print("")

    #Creates a random ID for the network
    def __getID(self): #TODO Make sure the id is not in use currently
        return str(random.randint(1,10000000))

    def __getKID(self):
        return str(random.randint(1,10000))

    #Shows the information of a network
    def __showNetwork(self,id):
        xml=etree.parse('network.xml')
        all=xml.xpath("network[@id=%s]"%(id))
        print("NetworkID\tEncryption\tNodes")
        for net in all:
            encrypt=net.find('encryption')
            nodes=net.findall('nodelist/IP')
            sys.stdout.write("%s\t\t%s\t\t"%(net.get('id'),encrypt.text))
            i=0
            for node in nodes:
                sys.stdout.write("%s "%(node.text))
                i=i+1
                if i%4 == 0:
                    print("")
                    sys.stdout.write("\t\t\t\t")
            print("")

    #get keys from keys.xml
    def __getKey(self,id):
        xml=etree.parse('keys.xml')
        all=xml.xpath("id[@id=%s]"%(id))
        for a in all:
            #TODO HANDLE KEY NOT DEFINED
            user=a.find('username')
            key=a.find('key')
        return user.text,key.text

    
    #Gets the iplist for a networkid
    def __getNodeListofNetwork(self,id):
        xml=etree.parse('network.xml')
        allnodes=xml.xpath("network[@id=%s]/nodelist/IP/text()"%(id))
        ec2nodes=xml.xpath("network[@id=%s]/ec2nodes/IP/text()"%(id))
        nodelist=[]
        keylist={}
        ec2nodelist=[]
        for node in allnodes:
            if '::' in node:
                pos=node.find("::")
                temp=node[pos+2:]
                node=node[:pos]
            else:
                temp=""
            nodelist.append(node)
            keylist[node]=temp
        for node in ec2nodes:
            if '::' in node:
                pos=node.find("::")
                temp=node[pos+2:]
                node=node[:pos]
            else:
                temp=""
            ec2nodelist.append(node)
            keylist[node]=temp
        return nodelist,keylist,ec2nodelist
    #Gets the encryption algorithm associated with a network id
    def __getEncryptionofNetwork(self,id):
        xml=etree.parse('network.xml')
        algo=xml.xpath("network[@id=%s]/encryption/text()"%(id))
        return algo[0]

    #Recent Mod
    def __addNewNodes(self,args,id,verbose):
        deploy=Deploy()
        try:
            if(os.path.exists('network.xml')):
                parser = etree.XMLParser(remove_blank_text=True)
                xml=etree.parse('network.xml',parser)
                (nodelist,keylist,ec2nodelist)=self.__getNodeListofNetwork(id)
                encryalgo=self.__getEncryptionofNetwork(id)

                #for node in args.nodes:
                #    etree.SubElement(parentnode,"IP").text=node
                #for node in args.ec2nodes:
                #    etree.SubElement(parentec2,"IP").text=node
                if args.nodes != []:
                    node=args.nodes[0]
                    nodes=xml.xpath("network[@id=%s]/nodelist/IP"%(id))
                    parentnode=nodes[0].getparent();
                    etree.SubElement(parentnode,"IP").text=node
                    if '::' in node:
                        pos=node.find("::")
                        keyid=node[pos+2:]
                        node=node[:pos]
                    else:
                        keyid=""
                    if keyid == "":
                        with settings (hosts=nodelist,ec2hosts=ec2nodelist,ec2=False,host_string=node,warn_only=True):
                            deploy.configIPSec(encryalgo,verbose,id)
                    else:
                        (username,key)=self.__getKey(keyid)
                        with settings (hosts=nodelist,ec2hosts=ec2nodelist,ec2=False,host_string=node,warn_only=True,user=username,key_filename=key):
                            deploy.configIPSec(encryalgo,verbose,id)
                if args.ec2nodes != []:
                    node=args.ec2nodes[0]
                    ec2nodes=xml.xpath("network[@id=%s]/ec2nodes/IP"%(id))
                    parentec2=ec2nodes[0].getparent();
                    etree.SubElement(parentec2,"IP").text=node
                    if '::' in node:
                        pos=node.find("::")
                        keyid=node[pos+2:]
                        node=node[:pos]
                    else:
                        keyid=""
                    if keyid == "":
                        with settings (hosts=nodelist,ec2hosts=ec2nodelist,ec2=True,host_string=node,warn_only=True):
                            deploy.configIPSec(encryalgo,verbose,id)
                    else:
                        (username,key)=self.__getKey(keyid)
                        with settings (hosts=nodelist,ec2hosts=ec2nodelist,ec2=True,host_string=node,warn_only=True,user=username,key_filename=key):
                            deploy.configIPSec(encryalgo,verbose,id)
                if args.nodes != []:
                    node=args.nodes[0]
                    if '::' in node:
                        pos=node.find("::")
                        node=node[:pos]
                    newnode=[node]
                    newec2node=[]
                else:
                    node=args.ec2nodes[0]
                    if '::' in node:
                        pos=node.find("::")
                        node=node[:pos]
                    newnode=[]
                    newec2node=[node]

                (nodelist,keylist,ec2nodelist)=self.__getNodeListofNetwork(id)            

                for host in nodelist:
                       if keylist[host]=="":
                           with settings (hosts=newnode,ec2hosts=newec2node,ec2=False,host_string=host,warn_only=True):
                               deploy.configIPSec(encryalgo,verbose,id)
                       else:
                           (username,key)=self.__getKey(keylist[host])
                           with settings (hosts=newnode,ec2hosts=newec2node,ec2=False,host_string=host,warn_only=True,user=username,key_filename=key):
                               deploy.configIPSec(encryalgo,verbose,id)
                
                for host in ec2nodelist:
                    if keylist[host]=="":
                        print("Key for node %s not defined"%(host))
                    else:
                        (username,key)=self.__getKey(keylist[host])
                        with settings (hosts=newnode,ec2hosts=newec2node,ec2=True,host_string=host,warn_only=True,user=username,key_filename=key):
                            deploy.configIPSec(encryalgo,verbose,id)
                
                
                f = open('network.xml', 'w')
                f.write(etree.tostring(xml,pretty_print=True,xml_declaration=True,encoding='iso-8859-1'))
                f.close()
        finally:
            disconnect_all()
            
    #end recent mod
    #Creating the xml
    def __defineNetwork(self,args):
        
        #Write to a file
        if(os.path.exists('network.xml')):
            parser = etree.XMLParser(remove_blank_text=True)
            xml=etree.parse('network.xml',parser)
            root=xml.getroot()
            network=etree.SubElement(root,"network")
            id=self.__getID()
            network.set('id',id)
            nodelist=etree.SubElement(network,"nodelist")
            ec2nodes=etree.SubElement(network,"ec2nodes")
            encrypt=etree.SubElement(network,"encryption")
            encrypt.text=args.encryption
            for node in args.nodes:
                etree.SubElement(nodelist,"IP").text=node
            for node in args.ec2nodes:
                etree.SubElement(ec2nodes,"IP").text=node
            xml.write('network.xml',pretty_print=True)
            print("Network Defined ")
            self.__showNetwork(id)
            
        else:
            xml=etree.Element("xml")
            network=etree.SubElement(xml,"network")
            id=self.__getID()
            network.set('id',id)
            nodelist=etree.SubElement(network,"nodelist")
            ec2nodes=etree.SubElement(network,"ec2nodes")
            encrypt=etree.SubElement(network,"encryption")
            encrypt.text=args.encryption
            for node in args.nodes:
                etree.SubElement(nodelist,"IP").text=node
            for node in args.ec2nodes:
                etree.SubElement(ec2nodes,"IP").text=node
            f = open('network.xml', 'w')
            f.write(etree.tostring(xml, pretty_print=True,xml_declaration=True,encoding='iso-8859-1'))
            f.close()
            print("Network Defined ")
            self.__showNetwork(id)
        

    def __addKey(self,args):
                
        #Write to a file
        if(os.path.exists('keys.xml')):
            parser = etree.XMLParser(remove_blank_text=True)
            xml=etree.parse('keys.xml',parser)
            root=xml.getroot()
            node=etree.SubElement(root,"id")
            kid=self.__getKID()
            node.set('id',kid)
            username=etree.SubElement(node,"username")
            key=etree.SubElement(node,"key")
            username.text=args.username
            key.text=args.key
            xml.write('keys.xml',pretty_print=True)
            print("Key registered id=%s "%kid)
            
        else:
            xml=etree.Element("xml")
            node=etree.SubElement(xml,"id")
            kid=self.__getKID()
            node.set('id',kid)
            username=etree.SubElement(node,"username")
            key=etree.SubElement(node,"key")
            username.text=args.username
            key.text=args.key
            f = open('keys.xml', 'w')
            f.write(etree.tostring(xml, pretty_print=True,xml_declaration=True,encoding='iso-8859-1'))
            f.close()
            print("Key registered %s"%kid)
    
    
    #Removes a network from the list
    def __removeNetwork(self,id):
        parser = etree.XMLParser(remove_blank_text=True)
        xml=etree.parse('network.xml',parser)
        net=xml.xpath("network[@id=%s]"%(id))
        try:
            net[0].getparent().remove(net[0])      
            xml.write('network.xml',pretty_print=True)  
        except:
            print("Cannot remove network. Please make sure that the id exists.")

    def __startNetwork(self,id,verbose):
        deploy=Deploy()
        try:
            (nodelist,keylist,ec2nodelist)=self.__getNodeListofNetwork(id)
            encryalgo=self.__getEncryptionofNetwork(id)
            if not nodelist:
                print("Network with that id is not defined")
                sys.exit(0)
            for host in nodelist:
                if keylist[host]=="":
                    with settings (hosts=nodelist,ec2hosts=ec2nodelist,ec2=False,host_string=host,warn_only=True):
                        deploy.configIPSec(encryalgo,verbose,id)
                else:
                    (username,key)=self.__getKey(keylist[host])
                    with settings (hosts=nodelist,ec2hosts=ec2nodelist,ec2=False,host_string=host,warn_only=True,user=username,key_filename=key):
                        deploy.configIPSec(encryalgo,verbose,id)
            
            for host in ec2nodelist:
                if keylist[host]=="":
                    print("Key for node %s not defined"%(host))
                else:
                    (username,key)=self.__getKey(keylist[host])
                    with settings (hosts=nodelist,ec2hosts=ec2nodelist,ec2=True,host_string=host,warn_only=True,user=username,key_filename=key):
                        deploy.configIPSec(encryalgo,verbose,id)
        finally:
            disconnect_all()

    def __showStatus(self,id,verbose):
        deploy=Deploy()
        print("Please wait while we check the status")
        try:
            (nodelist,keylist,ec2nodelist)=self.__getNodeListofNetwork(id)
            down=[]
            if not nodelist:
                print("Network with that id is not defined")
                sys.exit(0)
            for host in nodelist:
                if keylist[host]=="":
                    with settings (hosts=nodelist,host_string=host,warn_only=True):
                        downnode,host=deploy.showStatus(id,verbose)
                        down.append(downnode)
                else:
                    (username,key)=self.__getKey(keylist[host])
                    with settings (hosts=nodelist,host_string=host,warn_only=True ,user=username,key_filename=key):
                        downnode,host=deploy.showStatus(id,verbose)
                        down.append(downnode)
            for host in ec2nodelist:
                if keylist[host]=="":
                    with settings (hosts=nodelist,host_string=host,warn_only=True):
                        downnode,host=deploy.showStatus(id,verbose)
                        down.append(downnode)
                else:
                    (username,key)=self.__getKey(keylist[host])
                    with settings (hosts=nodelist,host_string=host,warn_only=True ,user=username,key_filename=key):
                        downnode,host=deploy.showStatus(id,verbose)
                        down.append(downnode)
            
            print("The following nodes are not using IPSec %s"%(down))
        finally:
            disconnect_all()
    
    def __testStatus(self,id,hide,verbose):
        deploy=Deploy()
        
        try:
            (nodelist,keylist,ec2nodelist)=self.__getNodeListofNetwork(id)
            down=[]
            down2=[]
            if not nodelist:
                print("Network with that id is not defined")
                sys.exit(0)
            for host in nodelist:
                if keylist[host]=="":
                    with settings (hosts=nodelist,host_string=host,warn_only=True):
                        status=deploy.testStatus(ec2nodelist,verbose)
                        down.append(status)
                else:
                    (username,key)=self.__getKey(keylist[host])
                    with settings (hosts=nodelist,host_string=host,warn_only=True,user=username,key_filename=key):
                        status=deploy.testStatus(ec2nodelist,verbose)
                        down.append(status)
                        
            for host in ec2nodelist:
                if keylist[host]=="":
                    with settings (hosts=nodelist,host_string=host,warn_only=True):
                        status=deploy.testStatus(nodelist,verbose)
                        down2.append(status)
                else:
                    (username,key)=self.__getKey(keylist[host])
                    with settings (hosts=nodelist,host_string=host,warn_only=True,user=username,key_filename=key):
                        status=deploy.testStatus(nodelist,verbose)
                        down2.append(status)
               
            #print("The following nodes are not using IPSec %s"%(down))
            print("Source\t\tDestination\t\t%Loss")
            print("-----------------------------------------------")
            for outer in down:
                for inner in outer:
                    if not hide:
                        print("%s\t%s\t\t%s"%(inner[0],inner[1],inner[2]))
            for outer in down2:
                for inner in outer:
                    if not hide:
                        print("%s\t%s\t\t%s"%(inner[0],inner[1],inner[2]))
                    
        finally:
            disconnect_all()

    def __removeNodeFromDefinition(self,id,node,ec2):
        parser = etree.XMLParser(remove_blank_text=True)
        xml=etree.parse('network.xml',parser)
        #node="%s"%(node)
        #print(node)
        if ec2 == True:
            netid=xml.xpath("network[@id=%s]/ec2nodes/IP[text()[contains(.,\'%s\')]]"%(id,node))
            #node=netid[0].xpath(".nodelist/IP[contains(text(),%s)]"%(node))
        else:
            #print("network[@id=%s] and network/nodelist/[contains(IP/text(),%s)]"%(id,node))
            netid=xml.xpath("network[@id=%s]/nodelist/IP[text()[contains(.,\'%s\')]]"%(id,node))
            #node=netid[0].xpath(".ec2nodes/IP[contains(text(),%s)]"%(node))
        for ip in netid:
            ip.getparent().remove(ip)
            f = open('network.xml', 'w')
            f.write(etree.tostring(xml, pretty_print=True,xml_declaration=True,encoding='iso-8859-1'))
            f.close()
    
    def __checkNodeDefinition(self,id,node,ec2):
        parser = etree.XMLParser(remove_blank_text=True)
        xml=etree.parse('network.xml',parser)
        #node="%s"%(node)
        #print(node)
        if ec2 == True:
            netid=xml.xpath("network[@id=%s]/ec2nodes/IP[text()[contains(.,\'%s\')]]"%(id,node))
            #node=netid[0].xpath(".nodelist/IP[contains(text(),%s)]"%(node))
        else:
            #print("network[@id=%s] and network/nodelist/[contains(IP/text(),%s)]"%(id,node))
            netid=xml.xpath("network[@id=%s]/nodelist/IP[text()[contains(.,\'%s\')]]"%(id,node))
            #node=netid[0].xpath(".ec2nodes/IP[contains(text(),%s)]"%(node))
        if netid == []:
            return False
        else:
            return True
    
    def __removeNode(self,args):
        deploy=Deploy()
        id=args.id
        verbose=args.verbose
        if args.nodes == []:
            ec2=True
            node=args.ec2nodes
        else:
            ec2=False
            node=args.nodes
        if '::' in node:
            pos=node.find("::")
            node=node[:pos]
        try:
            (nodelist,keylist,ec2nodelist)=self.__getNodeListofNetwork(id)
            if self.__checkNodeDefinition(id,node,ec2) == False:
                print("Node not defined")
                return
            if not nodelist:
                print("Network with that id is not defined")
                sys.exit(0)
            for host in nodelist:
                if keylist[host]=="":
                    with settings (hosts=nodelist,host_string=host,warn_only=True):
                        deploy.removeNode(node,id,verbose)
                else:                    
                    (username,key)=self.__getKey(keylist[host])
                    with settings (hosts=nodelist,host_string=host,warn_only=True,user=username,key_filename=key):
                        deploy.removeNode(node,id,verbose)

            for host in ec2nodelist:
                if keylist[host]=="":
                    with settings (hosts=nodelist,host_string=host,warn_only=True):
                        deploy.removeNode(node,id,verbose)
                else:                    
                    (username,key)=self.__getKey(keylist[host])
                    with settings (hosts=nodelist,host_string=host,warn_only=True,user=username,key_filename=key):
                        deploy.removeNode(node,id,verbose)
            self.__removeNodeFromDefinition(id,node,ec2)
        finally:
            disconnect_all()
    
    def __stopNetwork(self,id,verbose):
        deploy=Deploy()
        try:
            (nodelist,keylist,ec2nodelist)=self.__getNodeListofNetwork(id)
            if not nodelist:
                print("Network with that id is not defined")
                sys.exit(0)
            for host in nodelist:
                if keylist[host]=="":
                    with settings (hosts=nodelist,host_string=host,warn_only=True):
                        deploy.stopIPSec(id,verbose)
                else:                    
                    (username,key)=self.__getKey(keylist[host])
                    with settings (hosts=nodelist,host_string=host,warn_only=True,user=username,key_filename=key):
                        deploy.stopIPSec(id,verbose)

            for host in ec2nodelist:
                if keylist[host]=="":
                    with settings (hosts=nodelist,host_string=host,warn_only=True):
                        deploy.stopIPSec(id,verbose)
                else:                    
                    (username,key)=self.__getKey(keylist[host])
                    with settings (hosts=nodelist,host_string=host,warn_only=True,user=username,key_filename=key):
                        deploy.stopIPSec(id,verbose)

        finally:
            disconnect_all()

    #MENU HANDLERS#  
          
    def handleShow(self,args):
        if args.all:
            self.__showNetworkList()
        else:
            self.__showNetwork(args.id)

    def handleDefine(self,args):
        self.__defineNetwork(args)
    
    def handleAddKey(self,args):
        self.__addKey(args)

    def handleStart(self,args):
        self.__startNetwork(args.id,args.verbose)

    def handleStop(self,args):
        self.__stopNetwork(args.id,args.verbose)

    def handleRemoveNode(self,args):
        self.__removeNode(args)
        
    def handleRemove(self,args):
        self.__removeNetwork(args.id)
        
    def handleStatus(self,args):
        self.__showStatus(args.id,args.verbose)
    
    def handleTest(self,args):
        self.__testStatus(args.id,args.hide,args.verbose)
    def handleNewNodes(self,args):
        self.__addNewNodes(args,args.id,args.verbose)


if __name__ == "__main__":
    
    #Handle Command line arguments
    master=Master()
    parser = argparse.ArgumentParser(description='Configure the Network.')
    subparsers = parser.add_subparsers(title='Subcommands',help='sub-command help')

    parser_define=subparsers.add_parser('define',help='Define a network')
    parser_define.add_argument('-n', '--nodes', metavar='nodelist', nargs='+', required=True,
                       help='Specify the node list seperated by space')
    
    parser_define.add_argument('-x', '--ec2nodes', metavar='ec2list', nargs='+', default=[],
                       help='Specify the ec2 node list seperated by space')

    #TODO:Add other encryption algos
    parser_define.add_argument('-e','--encryption', default='rijndael',
                       help='Specify the encryption algorithm)', choices=['3des','blowfish', 'des', 'rijndael'])
    parser_define.set_defaults(function=master.handleDefine)
    
    #RECENT MOD
    parser_addnode=subparsers.add_parser('addnode',help='Define a network')
    parser_addnode.add_argument('-n', '--nodes', metavar='nodelist', nargs='+', default=[],
                       help='Specify the node list seperated by space')
    
    parser_addnode.add_argument('-x', '--ec2nodes', metavar='ec2list', nargs='+', default=[],
                       help='Specify the ec2 node list seperated by space')
    parser_addnode.add_argument('-v','--verbose', help='Verbose mode',action='store_true')                
    parser_addnode.add_argument('-i','--id', required=True,
                       help='Specify the existing network id)')
    parser_addnode.set_defaults(function=master.handleNewNodes)
    
    #END RECENT MOD
    
    parser_addkey=subparsers.add_parser('addkey',help='Add keys for a node')
    parser_addkey.add_argument('-u','--username',help='Specify the username')
    parser_addkey.add_argument('-k','--key',help='Specify the key')
    parser_addkey.add_argument('-v','--verbose', help='Verbose mode',action='store_true')
    parser_addkey.set_defaults(function=master.handleAddKey)

                    
    parser_show=subparsers.add_parser('show',help='Shows the defined network.')
    parser_show.add_argument('-a','--all',help='Show all the networks',action='store_true')
    parser_show.add_argument('-i','--id',help='Show the network with the specified id')                
    parser_show.set_defaults(function=master.handleShow,all=False)

    parser_start=subparsers.add_parser('start',help='Start a predefined network. The network id must be given.')
    parser_start.add_argument('-i','--id',help='Create the network specified by the id',required=True)
    parser_start.add_argument('-v','--verbose', help='Verbose mode',action='store_true')
    parser_start.set_defaults(function=master.handleStart)

    parser_stop=subparsers.add_parser('stop',help='Stop a predefined network. The network id must be given.')
    parser_stop.add_argument('-i','--id',help='Stop the network specified by the id',required=True)
    parser_stop.add_argument('-v','--verbose', help='Verbose mode',action='store_true')
    parser_stop.set_defaults(function=master.handleStop)
    
    parser_remn=subparsers.add_parser('remnode',help='Removes a node from the network')
    parser_remn.add_argument('-i','--id',help='Specified network id',required=True)
    parser_remn.add_argument('-v','--verbose', help='Verbose mode',action='store_true')
    parser_remn.add_argument('-n', '--nodes', metavar='nodelist', default=[],
                       help='Specify the node')
    
    parser_remn.add_argument('-x', '--ec2nodes', metavar='ec2list', default=[],
                        help='Specify the external node')

    parser_remn.set_defaults(function=master.handleRemoveNode)


    parser_remove=subparsers.add_parser('remove',help='Remove a network from the list.')
    parser_remove.add_argument('-i','--id',help='Remove the network specified by the id',required=True)
    parser_remove.set_defaults(function=master.handleRemove)
    
    parser_remove=subparsers.add_parser('status',help='Show status of a network')
    parser_remove.add_argument('-i','--id',help='Show status of the network specified by the id',required=True)
    parser_remove.add_argument('-v','--verbose', help='Verbose mode',action='store_true')
    parser_remove.set_defaults(function=master.handleStatus)
    
    parser_remove=subparsers.add_parser('test',help='Tests network with ping command')
    parser_remove.add_argument('-i','--id',help='Tests the network specified by the id',required=True)
    parser_remove.add_argument('--hide',help='Hide reachable connections.',action='store_true')
    parser_remove.add_argument('-v','--verbose', help='Verbose mode',action='store_true')
    parser_remove.set_defaults(function=master.handleTest,hide=False)

    parser.add_argument('--version', action='version', version='%(prog)s 0.1')
    args = parser.parse_args()

    args.function(args)
