import argparse
import random
import os
import sys

from fabfile import Deploy
from fabric.api import *
from fabric.state import connections
from fabric.network import *
from lxml import etree

class Master:
    #Shows the list of all configured network
    def __showNetworkList(self):
        xml=etree.parse('network.xml')
        all=xml.findall("network")
        print("NetworkID\tEncryption\tNodes")
        for net in all:
            encrypt=net.find('encryption')
            nodes=net.findall('nodelist/IP')
            sys.stdout.write("%s\t\t%s\t\t"%(net.get('id'),encrypt.text))
            i=0
            for node in nodes:
                sys.stdout.write("%s "%(node.text))
                i=i+1
                if i%4 == 0:
                    print("")
                    sys.stdout.write("\t\t\t\t")
            print("")

    #Creates a random ID for the network
    def __getID(self): #TODO Make sure the id is not in use currently
        return str(random.randint(1,10000000))


    #Shows the information of a network
    def __showNetwork(self,id):
        xml=etree.parse('network.xml')
        all=xml.xpath("network[@id=%s]"%(id))
        print("NetworkID\tEncryption\tNodes")
        for net in all:
            encrypt=net.find('encryption')
            nodes=net.findall('nodelist/IP')
            sys.stdout.write("%s\t\t%s\t\t"%(net.get('id'),encrypt.text))
            i=0
            for node in nodes:
                sys.stdout.write("%s "%(node.text))
                i=i+1
                if i%4 == 0:
                    print("")
                    sys.stdout.write("\t\t\t\t")
            print("")

    #Gets the iplist for a networkid
    def __getNodeListofNetwork(self,id):
        xml=etree.parse('network.xml')
        allnodes=xml.xpath("network[@id=%s]/nodelist/IP/text()"%(id))
        return allnodes

    #Gets the encryption algorithm associated with a network id
    def __getEncryptionofNetwork(self,id):
        xml=etree.parse('network.xml')
        algo=xml.xpath("network[@id=%s]/encryption/text()"%(id))
        return algo[0]

    #Creating the xml
    def __defineNetwork(self,args):
        
        #Write to a file
        if(os.path.exists('network.xml')):
            parser = etree.XMLParser(remove_blank_text=True)
            xml=etree.parse('network.xml',parser)
            root=xml.getroot()
            network=etree.SubElement(root,"network")
            id=self.__getID()
            network.set('id',id)
            nodelist=etree.SubElement(network,"nodelist")
            encrypt=etree.SubElement(network,"encryption")
            encrypt.text=args.encryption
            for node in args.nodes:
                etree.SubElement(nodelist,"IP").text=node
            xml.write('network.xml',pretty_print=True)
            print("Network Defined ")
            self.__showNetwork(id)
            
        else:
            xml=etree.Element("xml")
            network=etree.SubElement(xml,"network")
            id=self.__getID()
            network.set('id',id)
            nodelist=etree.SubElement(network,"nodelist")
            encrypt=etree.SubElement(network,"encryption")
            encrypt.text=args.encryption
            for node in args.nodes:
                etree.SubElement(nodelist,"IP").text=node
            f = open('network.xml', 'w')
            f.write(etree.tostring(xml, pretty_print=True,xml_declaration=True,encoding='iso-8859-1'))
            f.close()
            print("Network Defined ")
            self.__showNetwork(id)
        
    #Removes a network from the list
    def __removeNetwork(self,id):
        parser = etree.XMLParser(remove_blank_text=True)
        xml=etree.parse('network.xml',parser)
        net=xml.xpath("network[@id=%s]"%(id))
        try:
            net[0].getparent().remove(net[0])      
            xml.write('network.xml',pretty_print=True)  
        except:
            print("Cannot remove network. Please make sure that the id exists.")

    def __startNetwork(self,id,verbose):
        deploy=Deploy()
        try:
            nodelist=self.__getNodeListofNetwork(id)
            encryalgo=self.__getEncryptionofNetwork(id)
            if not nodelist:
                print("Network with that id is not defined")
                sys.exit(0)
            for host in nodelist:
                with settings (host_string=host,warn_only=True):
                    deploy.configIPSec(encryalgo,verbose)
        finally:
            disconnect_all()

    def __showStatus(self,id,verbose):
        deploy=Deploy()
        try:
            nodelist=self.__getNodeListofNetwork(id)
            down=[]
            if not nodelist:
                print("Network with that id is not defined")
                sys.exit(0)
            for host in nodelist:
                with settings (host_string=host,warn_only=True):
                    status,downnode=deploy.showStatus(verbose)
                    if not status:
                        down.append(downnode)
            
            print("The following nodes are not using IPSec %s"%(down))
        finally:
            disconnect_all()
    
    def __testStatus(self,id,hide,verbose):
        deploy=Deploy()
        
        try:
            nodelist=self.__getNodeListofNetwork(id)
            down=[]
            if not nodelist:
                print("Network with that id is not defined")
                sys.exit(0)
            for host in nodelist:
                with settings (host_string=host,warn_only=True):
                    status=deploy.testStatus(verbose)
                    down.append(status)
                    
            #print("The following nodes are not using IPSec %s"%(down))
            print("Source\t\tDestination\t\t%Loss")
            print("-----------------------------------------------")
            for outer in down:
                for inner in outer:
                    if not hide:
                        print("%s\t%s\t\t%s"%(inner[0],inner[1],inner[2]))
                    
        finally:
            disconnect_all()


    
    def __stopNetwork(self,id,verbose):
        deploy=Deploy()
        try:
            nodelist=self.__getNodeListofNetwork(id)
            if not nodelist:
                print("Network with that id is not defined")
                sys.exit(0)
            for host in nodelist:
                with settings (host_string=host,warn_only=True):
                    deploy.stopIPSec(verbose)
        finally:
            disconnect_all()

    #MENU HANDLERS#  
          
    def handleShow(self,args):
        if args.all:
            self.__showNetworkList()
        else:
            self.__showNetwork(args.id)

    def handleDefine(self,args):
        self.__defineNetwork(args)

    def handleStart(self,args):
        self.__startNetwork(args.id,args.verbose)

    def handleStop(self,args):
        self.__stopNetwork(args.id,args.verbose)
        
    def handleRemove(self,args):
        self.__removeNetwork(args.id)
        
    def handleStatus(self,args):
        self.__showStatus(args.id,args.verbose)
    
    def handleTest(self,args):
        self.__testStatus(args.id,args.hide,args.verbose)


if __name__ == "__main__":
    
    #Handle Command line arguments
    master=Master()
    parser = argparse.ArgumentParser(description='Configure the Network.')
    subparsers = parser.add_subparsers(title='Subcommands',help='sub-command help')

    parser_define=subparsers.add_parser('define',help='Define a network')
    parser_define.add_argument('-n', '--nodes', metavar='nodelist', nargs='+', required=True,
                       help='Specify the node list seperated by space')

    #TODO:Add other encryption algos
    parser_define.add_argument('-e','--encryption', default='rijndael',
                       help='Specify the encryption algorithm)', choices=['3des','blowfish', 'des', 'rijndael'])
    parser_define.set_defaults(function=master.handleDefine)
                    
    parser_show=subparsers.add_parser('show',help='Shows the defined network.')
    parser_show.add_argument('-a','--all',help='Show all the networks',action='store_true')
    parser_show.add_argument('-i','--id',help='Show the network with the specified id')                
    parser_show.set_defaults(function=master.handleShow,all=False)

    parser_start=subparsers.add_parser('start',help='Start a predefined network. The network id must be given.')
    parser_start.add_argument('-i','--id',help='Create the network specified by the id',required=True)
    parser_start.add_argument('-v','--verbose', help='Verbose mode',action='store_true')
    parser_start.set_defaults(function=master.handleStart)

    parser_stop=subparsers.add_parser('stop',help='Stop a predefined network. The network id must be given.')
    parser_stop.add_argument('-i','--id',help='Stop the network specified by the id',required=True)
    parser_stop.add_argument('-v','--verbose', help='Verbose mode',action='store_true')
    parser_stop.set_defaults(function=master.handleStop)


    parser_remove=subparsers.add_parser('remove',help='Remove a network from the list.')
    parser_remove.add_argument('-i','--id',help='Remove the network specified by the id',required=True)
    parser_remove.set_defaults(function=master.handleRemove)
    
    parser_remove=subparsers.add_parser('status',help='Show status of a network')
    parser_remove.add_argument('-i','--id',help='Show status of the network specified by the id',required=True)
    parser_remove.add_argument('-v','--verbose', help='Verbose mode',action='store_true')
    parser_remove.set_defaults(function=master.handleStatus)
    
    parser_remove=subparsers.add_parser('test',help='Tests network with ping command')
    parser_remove.add_argument('-i','--id',help='Tests the network specified by the id',required=True)
    parser_remove.add_argument('--hide',help='Hide reachable connections.',action='store_true')
    parser_remove.add_argument('-v','--verbose', help='Verbose mode',action='store_true')
    parser_remove.set_defaults(function=master.handleTest,hide=False)

    parser.add_argument('--version', action='version', version='%(prog)s 0.1')
    args = parser.parse_args()

    args.function(args)