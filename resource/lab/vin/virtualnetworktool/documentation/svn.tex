\chapter{Creating a secure virtual network}

\label{chap:svn}

As described in Chapter~\ref{chap:Introduction} one of the main goal of the VIN is to create a virtual private network with the VM's participating in the virtual cluster. This virtual private network should be secure and should be less intrusive. The secure communication between the VM's can be established by creating the virtual network within the VM (Figure ~\ref{fig:vinarch1}) or outside the VM (Figure ~\ref{fig:vinarch2}) at the bare hardware level used by the VM. If we are creating the virtual network within the VM then the tunnel endpoints will be inside the virtual machine. In the other case the tunnel endpoints will be in the host machine and the guest VM will not be aware of the existence of the virtual network.

\begin{figure}[htb]
\begin{center}
\includegraphics[scale=.50,angle=270]{figures/VIN-Arch1.pdf}
\end{center}
\caption{Virtual network created inside VM}
\label{fig:vinarch1}
\end{figure}

\begin{figure}[htb]
\begin{center}
\includegraphics[scale=.50,angle=270]{figures/VIN-Arch2.pdf}
\end{center}
\caption{Virtual network created outside VM}
\label{fig:vinarch2}
\end{figure}

\section{Architecture}
In this report we are evaluating the scenario in which the virtual which is created inside the VM's. This scenario has the advantage of having a tunnel starting from inside the guest machine. This means that even the packet flow from guest machine to the host machine is secured. Moreover the host machine need not be modified to provide secure connectivity for the guest virtual machines. This is important when using the service of an external cloud service provider where we may not have access to the host machine. Also, if needed the end users can reconfigure the virtual network the way they want since the end user has complete access to the guest machine.

Once configured, secure tunnels are created between any two pair of virtual machines of the virtual cluster. The tunnel ensures that all the communication between those machines will be encrypted. In some external cloud service all the communication between the virtual machines instances belonging to the same provider is secure and therefore we need not create any tunnels between those VM's to ensure secure communication. Thus we can avoid the overhead of using tunnels.


\section{Available solutions}
It is preferable to create the virtual network with a standardised solution which could be deployed in the guest virtual machine with least effort. We studied some of the existing solution which could be used to deploy the virtual network. Three most elegant solutions to implement the secure network is IPSec, OpenVPN and OpenSSH.

\subsection{IPSec}{
IPSec is a set of protocol standard solution which is implemented at the network layer of the OSI model and provide end-to-end security. IPSec is implemented in kernel space and has tight integration  with the TCP/IP stack. Being in kernel space, IPSec has the advantage of being faster than user space implementations. However, setting up IPSec is more difficult compared to other VPN solutions. IPSec uses Internet Security Association and Key Management Protocol (ISAKMP)~\cite{maughanrfc} which does authentication and key exchange between the participating peers. IPSec uses Internet Key Exchange (IKE) protocol~\cite{kaufman2005rfc} to set up a security association (SA). A Security Association (SA) is the establishment of shared security attributes between two network entities to support secure communication. An SA may include attributes such as: cryptographic algorithm and mode; traffic encryption key; and parameters for the network data to be passed over the connection. Most IPSec implementations consist of an IKE daemon that runs in user space and an IPSec stack in the kernel that processes the actual IP packets.

}

\subsection{OpenVPN and OpenSSH}{

OpenVPN~\cite{openvpn} and OpenSSH~\cite{openssh} are a SSL based VPN solution used for creating secure point-to-point or site-to-site connections in routed or bridged configurations and remote access facilities. It uses SSL/TLS~\cite{dierks2008transport} security for encryption and is capable of traversing network address translators (NATs)~\cite{egevangrfc} and firewalls. OpenVPN is elegant due to its ease of use and portability. But unlike IPSec, they operate at the application layer. OpenVPN's security model is heavily based on the IPSec ESP protocol for secure tunnel transport over UDP. OpenVPN uses the OpenSSL~\cite{openssl} library to provide encryption of both the data and control channels. It lets OpenSSL do all the encryption and authentication work, allowing OpenVPN to use all the ciphers available in the OpenSSL package~\cite{osslciphers}. OpenVPN and OpenSSH have slightly higher overhead due to the encapsulation of the payload within higher layers of the OSI model. 

}

\section{Choice of technology}{
\label{sec:tech}
OpenVPN and IPSec are standardized solutions and can be used with most of the popular operating systems.  Both OpenVPN and IPSec support most of the encryption standards. We did some performance evaluation of OpenVPN and IPSec. We compared the performance of OpenVPN and IPSec using different algorithms. The results are shown in Table~\ref{tab:transaction} and Table~\ref{tab:throughput} of the evaluation section. We found out that IPSec is significantly faster in high speed communication channel. Moreover IPSec provides a perfectly symmetric connection between two endpoints of the tunnel unlike OpenVPN which has a client/server architecture which is difficult for the automation when we use with VIN and hence we decided to use IPSec to create the secure virtual network. In this report, we only focus on creating the virtual network using IPSec. 


To setup a secure virtual network in a cloud federation, the nodes in the external cloud should have a public IP address and also it should accept Encapsulating Security payload (ESP)~\cite{kent2005ip}. In IPSec, ESP provides origin authenticity, integrity, and confidentiality protection of packets. Some external cloud service provider may have firewall configured to drop ESP packets.
}

\section{IPSec using KAME and OpenSwan}{
We used two major implementations to establish IPSec namely ipsec-tools~\cite{ipsectools} (with racoon) and Openswan~\cite{openswan}. This section explains in detail both these implementation.

For two nodes to communicate with each other using IPSec it should first establish Security Association(SA) with each other. This can be done by either manual configuration or automatically by using some daemon such as racoon or pluto which runs in the user space. The IKE daemon uses Internet Key Exchange(IKE) protocol to set-up the IPSec-SA. This requires several key exchanges and is done in 2 phases. IKE phase 1 establish a secure communication channel by using Diffie-Helman key exchange algorithm and generates a shared secret key to encrypt further IKE communications. Authentication is done using shared secrets or certificates. This creates a SA for IKE and is called ISAKMP-SA. In the second phase the IKE uses ISAKMP-SA to negotiate the IPSec-SA. ISAKMP-SA is bidirectional and IPSec-SA is unidirectional, which means IPSec-SA should have SA for both inbound and outbound connections.

\subsection{IPSec Architecture}{
IPSec implementation is in the kernel space and the key exchange daemon is in the userspace. Figure~\ref{fig:IPSec-highlevel} shows the highlevel architecture of IPSec implementation. IPSec maintains two databases.  The first one is Security Policy Database(SPD). When a packet reaches the kernel it will refer the SPD to check the policy defined for the packet. Depending on that IPSec is applied to the packet. SPD also maintains the record of which IPSec-SA to use to a packet. The second database is the Security Association Database(SAD) which contains the IPSec-SA's. Ipsec-tools uses setkey to define the security policy of a particular connection. In case of openswan this is done by pluto itself. This creates a security policy entry for that connection in the SPD. When the kernel receives an outbound or inbound packet kernel checks the policy for that connection.  If a policy is defined in the SPD it will the IPSec-SA associated with that connection in the SAD. If a corresponding IPSec-SA is found it is applied. Otherwise the kernel will send a request for the key to the IKE daemon. IKE daemon will negotiate the IPSec-SA and is inserted into the SAD.

\begin{figure}[htb]
\includegraphics[scale=.50,angle=270]{figures/IPSec.pdf}
\caption{Highlevel architecture of IPSec implementation (numbers shows the flow)}
\label{fig:IPSec-highlevel}
\end{figure}
 
\subsection{Steps for establishing IPsec connection}{
\begin{itemize}
\item {\bf Determining the traffic to encrypt: }
The traffic to encrypt is defined in the Security policy database. A policy will be defined for the end points of the network. When a policy is found for a particular traffic, IPSec initiates the next step in the process, negotiating an IKE phase 1 exchange.

\item {\bf IKE phase 1: }
In IKE phase 1 the IPSec peers are authenticated and secure channel between the peers are set up to enable IKE exchanges. Functions performed in IKE phase 1 is explained in Appendix~\ref{sec:IKE phase 1 functions}. IKE phase 1 occurs in two modes: main mode and aggressive mode. Detailed explanation of both these are given in Appendix~\ref{sec:IKE Phase 1 modes}

\item {\bf IKE Phase 2: }
The purpose of IKE phase 2 is to negotiate IPSec SAs to set up the IPSec tunnel. Functions performed in IKE phase 2 is explained in Appendix~\ref{sec:IKE phase 2 functions}. IKE phase 2 occurs in one mode called quick mode. Detailed explanation of quick mode is given in Appendix~\ref{sec:IKE Phase 2 mode}. An improved resistance to the cryptographic attacks can be ensured by using Perfect Forward Secrecy (PFS). If it is specified a new Diffie-Hellman exchange is performed with each quick mode, providing keying material that has greater entropy (key material life). However Diffie-Hellman exchange is computationally expensive and will affect the performance. 

\item {\bf IPSec Encrypted Tunnel: } Once IKE Phase 2 is over information, information is exchanged using an encrypted IPSec tunnel. Packets are encrypted and decrypted using the encryption specified in the IPSec SA. IPSec encrypted tunnel is shown in figure~\ref{fig:tunnel}

\begin{figure}[htb]
\begin{center}
\includegraphics[scale=.50]{figures/Tunnel.pdf}
\end{center}
\caption{IPSec Tunnel}
\label{fig:tunnel}
\end{figure}

\item {\bf IPSec tunnel termination: } IPSec can be terminated either by deletion or my time out. When an IPSec SA is terminated the keys are also dropped. Further communication required new keys, IKE performs a new phase 2/phase 1 negotiation.  
\end{itemize}
}

\subsection{Setting up IPSec using ipsec-tools}{

Ipsec-tools is a port of KAME's IPsec utilities to the Linux-2.6 IPsec implementation. It consist of the following 
\begin{itemize}

\item {\bf libipsec} Library with PF{\_}KEY\footnote{PF{\_}KEY is a socket protocol family used by trusted privileged key management applications to communicate with Security Association Database ~\cite{PFKEY}} implementation.
\item {\bf setkey} This tool is used to manipulate and dump the kernel Security Policy Database (SPD) and Security Association Database (SAD).
\item {\bf racoon} Racoon is an
Internet Key Exchange (IKE) daemon for automatically keying IPsec connections.
\end{itemize}

This section explains a simple configuration which can be used to establish IPSec connection. 

\subsubsection{Racoon}

Racoon uses port 500 of the UDP. We have to specify a configuration file to start a racoon instance. Given below is a sample racoon configuration file.

\lstset{language=c}
\begin{lstlisting}[numbers=left,numberstyle=\tiny,firstnumber=1,numberblanklines=false]
path pre_shared_key "/root/config/psk.txt";

remote anonymous {
        exchange_mode main,aggressive;
        nat_traversal on;
        proposal {
                encryption_algorithm aes;
                hash_algorithm sha1;
                authentication_method pre_shared_key;
                dh_group 2;
        }
}

sainfo anonymous {
        pfs_group 2;
        lifetime time 1 hour ;
        encryption_algorithm aes;
        authentication_algorithm hmac_md5 ;
        compression_algorithm deflate ;
}
\end{lstlisting}


Without going into too much details we will explain the important statements of the above configuration file. Line 1 defines the path of the shared key. This is used when the authentication method is pre-shared key. This key used in the phase 1 for authentication.

Everything listed inside the remote directive is used during the phase 1. 'anonymous' tells racoon that this can be used for all connections.
\begin{itemize}
\item \textbf{exchange\_mode} - Phase 1 operates in either Main Mode or Aggressive Mode. Main Mode protects the identity of the peers. Aggressive Mode does not protect the identity of the peers.
\item \textbf{nat\_traversal} - This is used when nat traversal is required.
\item \textbf{encryption\_algorithm} - This specifies the encryption algorithm to be used in the phase 1. The following algorithms can be used for encryption: des, 3des, rc5, idea, cast, blowfish.
\item \textbf{hash\_algorithm} - Racoon can use  md5 or sha1 as the hash algorithm in the phase 1.
\item \textbf{dh\_group} - This specifies the Diffie-helman group to be used during the IKE. The values can be 1,2 or 5.
\end{itemize} 

Everything listed in the sainfo is used for the phase 2. 
\begin{itemize}
\item \textbf{pfs\_group} - Specifies the Diffie-helman group to be used during the phase 2.
\item \textbf{lifetime} - Specifies the frequency of SA key refresh. It can be either in terms of time or in terms of bytes. Frequent refreshing of key ensures better security but it will slow down the connection.
\item \textbf{encryption\_algorithm} - Specifies the encryption algorithm to be used for the Encapsulated Security Payload (ESP). Racoon can use the following algorithms: des, 3des, cast, blowfish, twofish, rijndael and null\_enc.
\item \textbf{authentication\_algorithm} - Specifies the  authentication algorithm for ESP and Authentication Header (AH). Authentication algorithm can be hmac\_sha1, hmac\_md5 and kpdk.
\item \textbf{compression\_algorithm} - Specifies the compression algorithm. Racoon can use deflate.
\end{itemize}

}
\subsubsection{Setkey}
Setkey is used to set the security policy in the SPD. We can specify the policy in a configuration file. We can use IPSec in either Transport or Tunnel mode. An example of two nodes in tunnel mode is shown in figure~\ref{fig:tunnel}. Tunnel mode is used to form a traditional VPN, where the tunnel generally creates a secure tunnel across an untrusted Internet. When we use IPSec in tunnel mode the IP packet is encrypted and/or authenticated and is encapsulated into a new IP packet with a new IP header.

The following are the most important lines of the configuration file.

\begin{lstlisting}[numbers=left,numberstyle=\tiny,firstnumber=1,numberblanklines=false]
spdadd 10.141.0.135 10.141.0.129 any -P out ipsec
esp/tunnel/10.141.0.135-10.141.0.129/require;

spdadd 10.141.0.129 10.141.0.135 any -P in ipsec
esp/tunnel/10.141.0.129-10.141.0.135/require;

\end{lstlisting}

}
\subsection{Setting up IPSec using Openswan}
{
\label{sec:openswan}
Openswan uses pluto daemon to setup both SPD and SAD. Openswan uses a single configuration file which describes both the policy and the pluto configuration. This section describes a simple configuration of openswan.
\begin{lstlisting}[numbers=left,numberstyle=\tiny,firstnumber=1,numberblanklines=false]
config setup
interfaces=%defaultroute
nat_traversal=yes

conn west-east
left=193.110.157.131
right=205.150.200.209
type=tunnel
authby=rsasig
leftrsasigkey=0sAQOkF1Ggd4iFfI2nQxJYbN9HGD...
rightrsasigkey=0sAQPEAl+N52EIRrIAA5cxl8U...
auto=start
ike=aes128
esp=aes128
   

\end{lstlisting}

ipsec.conf configuration file consists of two different section types: 
\begin{itemize}
\item config setup defines general configuration parameters
\item conn $<$name$>$ defines a connection
\end{itemize}

There can be only one config setup section but an unlimited number of conn sections.

`config setup' defines the general configuration used by all the connection in the local machine. This includes selecting the interface to use and nat traversal. Openswan does not support per connection nat traversal.

`conn $<$name$>$' section contains the configuration of each tunnel. For each tunnel the ends are referred to as left and right. This is similar to the client server concept. However, since the IPSec is a peer to peer protocol the term left and right is used. Configuration at both ends should define same node as left. When Openswan loads a new connection, it will look at both left and right entries, and it will try to figure out which end it is. 
This section also contains the information required for the authentication of IKE daemon. `ike=' and `esp=' in this section describes the algorithm~\cite{hoffman2005internet}~\cite{schiller2005cryptographic}~\cite{hoffman2005rfc} to use during key exchange and for the actual IPSec communication. Detailed explanation of ipsec.conf file is available in the man page~\cite{manipsec}.




}
}
