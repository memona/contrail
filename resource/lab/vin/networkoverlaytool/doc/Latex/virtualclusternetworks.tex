\chapter{Virtual Cluster Networks}
\label{chap:virtualclusternetworks}

One of the most challenging issues when facing \marginpar{\emph{\textbf{Andrei}: introducing some of the problems virtual clusters solve as an introduction to the chapter}} with Grid and Cloud communities is that even if they have the capabilities to provide access to a large number of heterogeneous resources, these resources often don't fully satisfy the needs of specific applications, especially in terms of the operating systems provided. Considering that multiple users make requests in a short timespan, resources of a certain type often get reserved faster than others which causes problems with subsequent requests, further causing user dissatisfaction and resource underutilization. One of the best and most researched types of solutions for this issue is represented by virtual clusters.

%Typically, virtual machines can be used as the backbone for virtual clusters. Besides virtual machines, chroots and global software stacks could also be used, each with their own advantages. While chroots and global software stacks provide a performance advantage, this is outweighed by the major administrative and abstraction advantage brought by utilizing virtual machines. Moreover, virtualization doesn't require homogeneity among all used physical sites, but rather provides homogeneity, if required, through the virtual machines that are run.

In this report we will make use of virtual machine technologies in order to configure and deploy virtual clusters. The main goal will be to automatically launch virtual machines  and establish connections between them, thus creating the backbone of the virtual cluster, later on focusing on adding a security layer on top of this backbone.

This chapter is structured as follows: Section 1 presents how communication is established between the virtual cluster components focusing on pairs of virtual machine components, Section 2 explains the need for a security layer in the context of our project, summarizes the major technology options for the security layer and provides a view of how the security layer needs to be applied on top of the communication backbone, Section 2.1 presents a theoretical background and brief introduction in the OpenVPN technology, Section 2.2 provides a theoretical background and brief introduction in the IPsec technology and Section 2.2.1 describes the Internet Key Exchange protocol (IKE) and the need for it's implementation within the scope of our project.



\section{Communication}

The first important step in creating a virtual cluster is managing to link together all the allocated virtual machines. Our target is to accomplish a setup in which the virtual machines are entirely unaware neither of any network topology nor of the fact that there are any physical machines facilitating the communication between virtual machines. 

\begin{figure}[htb]
\centering
\includegraphics[width=100mm]{res/Communication.png}
\caption{Communication between two virtual machine endpoints.}\label{fig:Communication}
\end{figure}

There are two major options to enable communication between virtual machines on different physical cluster nodes: a bridged setup or a routed setup.

Bridging is a forwarding technique used in packet switched computer networks. It relies on flooding and examination of source addresses in received packets to locate unknown devices and records their locations in tables so as to minimize the need for flooding. Bridging takes place at the OSI Model Layer 2 \cite{Tanenbaum1988} (data link layer), which means that frames are directed according to MAC addresses. This implies that bridging is unable to distinguish networks. Moreover, due to the flooding approach bridging does not scale to large networks.

Routing is the process of selecting paths along which to send traffic in a network. It takes place at the OSI Model Layer 3 \cite{Tanenbaum1988} (network layer), which means that decisions are made based on assigned IP addresses. As a result, routing can distinguish between networks, unlike bridging. In addition, as explained in the following section, this is the needed setup for the security layer.

For examples of how to create a routing setup between two physical machines, each hosting virtual machines, please check Appendix A.

%To facilitate the creation of a transparent configuration in which the virtual machines are unaware of topologies and that also allows an easy tunneling configuration, a routing setup needs to be used. Hence, all the communication between two virtual machines needs to be governed by the two parent physical cluster nodes through separate private networks and forwarding packets. For example, assuming that one virtual machine has IP address 192.168.0.2, the other one having 192.168.0.3 (note that they are not visible to each other as they are in different private networks), in order to route the packets between them, one needs to execute the following ip route commands:

%\textit{ip route add 192.168.0.3 via cluster\_net\_ip\_second\_physical\_machine}

%on the first physical machine and

%\textit{ip route add 192.168.0.2 via cluster\_net\_ip\_first\_physical\_machine }

%on the second physical machine to describe the path for reply packets.

%Furthermore, packet forwarding needs to be enabled in the kernel of the cluster's physical machines:

%\textit{echo 1 > /proc/sys/net/ipv4/ip\_forward or set net.ipv4.ip\_forward=1 in /etc/sysctl.conf.}

%Finally, considering that some clusters might have strict firewall policies (e.g. DROP policy by default), some firewall rules need to be in-place on the cluster nodes.

%\textit{iptables -I FORWARD 1 -s 192.168.0.2 -j ACCEPT} and \textit{iptables -I FORWARD 1 -d 192.168.0.2 -j ACCEPT}

%on the first cluster node and similar rules for the other one.

%At this point, the virtual machines should be able to see each other as if they were in the same network.



\section{Security layer}

One of the important aspects in creating virtual clusters for our purpose is security. Considering that multiple users will be making use of the same physical resources at the same time, multiple virtual clusters will coexist concurrently, most of them having different owners. Assuming that the environment is not entirely secure and that some of the users of virtual clusters or of the underlying physical cluster might be with bad intentions, a security layer needs to be configured on top of the communication backbone such that malicious users cannot interfere with the normal execution of another user's virtual cluster.

Figure 3.2 depicts our proposed security overlay on top of the communication layer.

\begin{figure}[htb]
\centering
\includegraphics[width=100mm]{res/SecurityLayer.png}
\caption{Security layer with regards to two communicating endpoints.}\label{fig:SecurityLayer}
\end{figure}

As it can be seen in Figure 3.2, we propose a scheme in which the virtual machines communicate in an unencrypted manner with their parent physical machines, but with communication being encrypted on the segment between the parent physical machines. At a closer look, we can identify this scheme as a tunneled mode where the tunnel endpoints are the physical machines and the virtual machines being part of private networks. In this regards, we can look at the physical machines as being gateways for communication between virtual machines.

There exists a number of technologies that provide this functionality in the industry with the most prevalent being OpenVPN and IPsec, which is why both need to be considered in order to choose the most appropriate performance wise and from an administrative and configuration point of view. We will give a brief introduction for both of these technologies from a theoretical point of view in the following sections.



\subsection{OpenVPN}

OpenVPN is a free and open source software application that implements virtual private network (VPN) techniques for creating secure point-to-point or site-to-site connections in routed or bridged configurations and remote access facilities~\cite{OpenVPN}. OpenVPN is based on SSL/TLS for encryption functionality relying heavily on the OpenSSL~\cite{OpenSSL} encryption library and also has the ability to traverse firewalls and NATs (network address translators).

OpenVPN offers various capabilities for peers to authenticate each other like pre-shared secret keys, username/password pairs and certificates and provides support for using hardware acceleration in order to enhance the encryption performance.

It supports running over User Datagram Protocol (UDP) or Transmission Control Protocol (TCP), it provides LZO~\cite{LZO} compression support for encrypting the data stream, it runs in userspace and it is supported on a multitude of platforms like Solaris, Linux, Mac OS X and Windows 2000/XP/Vista /7.

Examples and technical details on how to install and configure OpenVPN to construct point-to-point and site-to-site networks can be found in Appendix A.

\subsection{IPsec}

IPsec is a protocol suite for securing Internet Protocol (IP) communications by authenticating and encrypting each IP packet of a communication session~\cite{IPsec}. Moreover, IPsec is an end-to-end security scheme operating in the Internet Layer of the Internet Protocol Suite that can be used to protect data flows between a pair of hosts (host-to-host), between a gateway and a host (network to host) or between a pair of gateways (network-to-network)~\cite{IPsec} which is exactly the tunnel behavior required by our proposed virtual cluster implementation.

IPsec operates in lower layers of the TCP/IP model in comparison to other widely used systems like SSL, TLS and SSH, thus applications need not be designed to use IPsec. This enables IPsec to protect any application traffic across an entire IP network.

IPsec makes use of the Authentication Headers (AH) protocol~\cite{AH} to provide connectionless integrity and protection against replay attacks. Also, it uses the Encapsulating Security Payloads (ESP) protocol~\cite{ESP1} to provide confidentiality, connectionless integrity and data origin authentication.

Finally, the Internet Security Association and Key Management Protocol (ISAKMP) provides a support for authentication and key exchange. We will briefly introduce this concept in the next subsection.

Examples and technical details on how to install and configure IPsec to construct point-to-point and site-to-site networks can be found in Appendix A.

\subsubsection{Internet Key Exchange protocol}

The Internet Key Exchange (IKE) is the protocol used to set up security associations (SAs) within the IPsec protocol suite. While IKE sets up security associations, the security policies for every peer which will connect must be manually maintained.

A great majority of IPsec implementations make use of an IKE daemon running in user space, together with an kernel IPsec stack managing IP packets. One of the main reasons for this is performance as the kernel can process packets with little overhead, thus enhancing overall performance. 

Internet Key Exchange consists of two phases with the first phase designed to establish a secure authenticated communication channel with the help of the Diffie-Hellman key exchange algorithm for shared secret key generation, and with the second phase designed to negociate Security Associations between the communicating endpoints using the secure channel created in the first phase.

Examples and technical details on how to install and configure racoon, an IKE implementation, to automatically establish security associations can be found in Appendix A.
