\chapter{Evaluation}
\label{chap:evaluation}

This section provides the evaluation infrastructure for the virtual cluster management \emph{Python} tool we implemented and a set of benchmarks that are designed to justify the choice in infrastructure and technologies for the implementation phase. For the evaluation part, we decided to use the Netperf benchmarking tool that is specifically designed to measure the performance of networking configurations. Netperf also provides support for unidirectional throughput and end-to-end latency testing.

The virtual cluster management \emph{Python} tool we implemented was deployed on the DAS3 cluster site at the Vrije Universiteit Amsterdam with the following specifications:

\begin{itemize}
  \item Scientific Linux – 2.6.26-2-xen-686 cluster nodes distribution;
  \item Xen used as a virtual machine monitor;
  \item Dual Core AMD Opteron(tm) Processor 280, 2.4 GHz, 4 GB of memory for each cluster node;
  \item GbE network.
\end{itemize}

We will present bandwidth measurements and latency measurements, both provided by the Netperf utility. These tests are the TCP\_STREAM test which is designed to measure the bandwidth of the communication channels, and the TCP\_RR (request response) test which is designed to measure the number of transactions per second that can be handled by the network. We define a transaction as an exchange of a single request and a single response and note that it can be used as a measure for one way and round-trip average latency. Round-trip latency is computed, as suggested by the Netperf instructions, with the following formula:

\begin{equation}
  \frac{1}{Number\ of\ transactions\ per\ second}
\end{equation}

This chapter is structured as follows: Section 1 provides some measurements for the time it takes to setup a virtual cluster and suggests future work in this area, Section 2 presents the measurements for the IPsec and OpenVPN transport mode, Section 3 presents the measurements for the IPsec and OpenVPN tunnel mode and Section 4 estimates the scalability of the implementation with regard to an increasing virtual cluster size by testing performance either with many security associations in place or with stress tests on one of the virtual cluster nodes.



\section{Implementation performance evaluation}

The performance evaluations can be split in two major categories: implementation performance evaluation and deployed infrastructure performance evaluation. In this section we will show some of the most important elements that influence the implementation's performance.

The implementation's performance is affected by three major factors: the time cost to push the virtual cluster configurations on each machine, time cost for adding a large number of IPsec security policies on a machine and racoon's cost upon establishing connections. After several measurements averaging them, we discovered that pushing the configurations to a single machine takes between four and eight seconds. For the first implementation we used a sequential approach for pushing configurations. There are various ways in which this can be addressed in a more efficient way: either a distributed scheme (e.g. establish a binary tree structure in which each node is a machine that pushes configurations to two other machines) or a parallel distribution scheme, which would be a very good approach considering that we are working in a cluster environment. 

The implementation's performance is influenced by the time it takes to create many security policies on a physical machine, considering that for large virtual clusters a considerable amount of policies would be required for each machine. After several measurements and averaging on them we discovered that one hundred security associations need approximately ten seconds which is a manageable time cost, considering the prospectives of a parallel configuration distribution. Moreover, this time cost is due to indexing costs and creation of internal data structures for management of caching policies that help yield better retrieval times on packet send and receive operations.

The implementation's performance is also influenced by racoon's cost for setting up security associations between IPsec communicating endpoints. Considering that this is a one time only cost taken on the first communication attempt between two endpoints and that this cost is a couple seconds, this is manageable for a large sized cluster.

Finally, we will note that the memory cost for one hundred security associations is approximately 750 KB, which yields 7.5 KB per entry, while the cost for one hundred security policies is approximately 250 KB, which yields 2.5 KB per entry. This suggests that a large number of security associations and policies would not saturate the memory of even a low-end machine.



\section{Transport mode evaluation}

One important step when building the virtual cluster is to create tighly coupled virtual machine components. The first step towards this goal and towards evaluating the performance of the virtual cluster is to establish the cost of communication between the virtual machine components, considering that communication is encrypted. 

In this section we will evaluate encrypted and unencrypted communication costs between two nodes from the same physical cluster, also called \textbf{transport mode} in \emph{IPsec} or \textbf{point-to-point} in \emph{OpenVPN}. The purpose of the transport mode is to later establish the overhead caused by using the cluster nodes as middle men for the communication between virtual machines. The measurements in this section will later be used as a reference point.

It is important to state that the \emph{IPsec} evaluations use \emph{ESP} (Encapsulating Security Payload) which is a member of the IPsec protocol suite that provides origin authenticity, integrity and confidentiality protection of packets. \emph{ESP} operates directly on top of \emph{IP} using \emph{IP} protocol number 50. Besides \emph{ESP} there exists the \emph{AH} member of the \emph{IPsec} protocol suite which is focused on authentication, while \emph{ESP} is focused both on authentication and encryption. Typically, \emph{ESP} and \emph{AH} are used independently, though it is possible (but uncommon) to use them both together.

Please note that some combinations are unsupported (e.g. racoon cannot be used with the IPsec stack disabled and it doesn't make sense to use racoon with no security policies/associations in place). In addition, racoon and OpenVPN don't support some encryption algorithms that the \emph{ipsec-tools} support. All the unsupported combinations are marked in the tables with \emph{NA} entries.

\begin{table}
\resizebox{12.5cm}{!} {
\begin{tabular}{|p{4cm}|p{1.5cm}|p{3cm}|p{1.5cm}|p{3cm}|}
  \hline
   & \multicolumn{2}{|p{5cm}|}{\bf{Manual security associations}} & \multicolumn{2}{|p{5cm}|}{\bf{IKE enabled (racoon)}} \\
  \hline
                                                         & \bf{MB/sec} & \bf{Round-trip latency ($\mu$s)} & \bf{MB/sec} & \bf{Round-trip latency ($\mu$s)} \\
  \hline
  \bf{IP (IPsec stack disabled)}                         & 941.33      & 95                       & \emph{NA}           & \emph{NA}                    \\
  \hline
  \bf{IP (IPsec stack enabled no security associations)} & 941.33      & 95                       & \emph{NA}           & \emph{NA}                     \\
  \hline
  \bf{ESP with blowfish-cbc-192}                         & 317.12      & 123                      & 322.65      & 124               \\
  \hline
  \bf{ESP with 3des-cbc-192}                             & 132.18      & 129                      & 136.61      & 129               \\
  \hline
  \bf{ESP with camellia-cbc-192}                         & 256.25      & 125                      & \emph{NA}           & \emph{NA}                     \\
  \hline
  \bf{ESP with rijndael-cbc-192 (aes-cbc)}               & 465.78      & 123                      & 471.36      & 123               \\
  \hline
\end{tabular}
}
\caption{IPsec transport mode performance measurements with various encryption algorithms with and without IKE clients.}\label{fig:IPsecTransport}
\end{table}

Tables~\ref{fig:IPsecTransport} and~\ref{fig:OpenVPNTransport}  present performance measurements for an unencrypted channel which is the infrastructure provided for a dedicated cluster, for an IPsec encrypted channel without the use of an IKE client, for an IPsec encrypted channel with an IKE client (racoon) and for an encrypted channel using OpenVPN. 

There are several important things to note in Table~\ref{fig:IPsecTransport}. First, we can see that the triple DES algorithm is by far the slowest one. Second, it can be seen that the latency the encrypted network connection can achieve varies to a small extent in comparison to the bandwidth. Third, it is obvious that the use of an IKE client doesn't affect performance at all. Fourth, we note that disabling the IPsec stack has no influence on the performance of the network. And finally, the best encryption algorithm is Rijndael-cbc or the Advanced Encryption Standard (AES) which is a symmetric-key encryption standard adopted by the U.S. government, achieving half the bandwidth of an unencrypted channel.

Table~\ref{fig:OpenVPNTransport} shows that, as the case with IPsec, the latency for OpenVPN does not vary as much from one encryption algorithm to another as does the bandwidth. In addition, we can see that the fastest algorithm is the AES, just like with IPsec. 

If we compare IPsec with OpenVPN, we observe that IPsec is much faster and provides a much better latency. While IPsec's Rijndael implementation achieves approximately 470 MB/s, the OpenVPN similar implementation achieves only 167 MB/s which is almost three times slower. In addition, IPsec achieves better latency performance gaining between 50 $\mu$s and 80 $\mu$s on OpenVPN, depending on the encryption algorithm used.

\begin{table}
\centering
%\resizebox{12.5cm}{!} {
\begin{tabular}{|p{4cm}|p{1.5cm}|p{3.3cm}|}
  \hline
                                                & \bf{MB/sec} & \bf{Round-trip latency ($\mu$s)} \\
  \hline
  \bf{Blowfish-cbc-192}                         & 164.87      & 185               \\
  \hline
  \bf{3des-cbc-192}                             & 88.56       & 193               \\
  \hline
  \bf{Rijndael-cbc-192 (aes-cbc)}               & 167.77      & 178               \\
  \hline
\end{tabular}
%}
\caption{OpenVPN transport mode performance measurements with various encryption algorithms.}\label{fig:OpenVPNTransport}
\end{table}



\section{Tunnel mode evaluation}

The second step towards evaluating the performance of the secure virtual cluster networks created by our Python implementation is to measure the bandwidth and latency achieved between two virtual machines, in tunnel mode. By \textbf{tunnel mode} (or \textbf{site-to-site} in \emph{OpenVPN}) we understand that each virtual machine is connected with its host cluster node by a virtual network that is private and that communication between virtual machines goes through and is forwarded by their parent cluster nodes and is encrypted between those cluster nodes. With the help of this setup the virtual machines can be kept unaware of encryption and forwarding rules. These results need to be compared to those achieved in the previous section to see to which extent the performance of the virtual cluster network comes close to that of the underlying physical cluster network.

%\marginpar{\emph{\textbf{Andrei}:Cannot use null\_enc, most likely unsupported. We get: "ERROR: no suitable proposal was created.", "ERROR: failed to start post getspi." in phase 2 of racoon's negociations.}}
In this section we will present results for an unencrypted tunnel, for an IPsec encrypted tunnel and for an OpenVPN encrypted tunnel. We will use multiple encryption algorithms to discover which one is better suited for a secure virtual cluster network. We have chosen the best known encryption algorithms and one of the most popular algorithms (Triple DES), all of which are supported by both \emph{IPsec} and \emph{OpenVPN}. 

All the measurements in this section will be between two virtual machines, hence the communication will always have two hops, the parent physical cluster nodes. 

\begin{table}
\resizebox{12.5cm}{!} {
\begin{tabular}{|p{4cm}|p{1.5cm}|p{3cm}|p{1.5cm}|p{3cm}|}
  \hline
   & \multicolumn{2}{|p{5cm}|}{\bf{Manual security associations}} & \multicolumn{2}{|p{5cm}|}{\bf{IKE enabled (racoon)}} \\
  \hline
                                                         & \bf{MB/sec} & \bf{Round-trip latency ($\mu$s)} & \bf{MB/sec} & \bf{Round-trip latency ($\mu$s)} \\
  \hline 
  \bf{IP (IPsec stack disabled)}                         & 941.28      & 142               & \emph{NA}           & \emph{NA}                     \\ 
  \hline 
  \bf{IP (IPsec stack enabled no security associations)} & 939.51      & 142               & \emph{NA}           & \emph{NA}                     \\
  \hline
  \bf{ESP with blowfish-cbc-192}                         & 271.80      & 164               & 301.11      & 166               \\
  \hline
  \bf{ESP with 3des-cbc-192}                             & 135.05      & 172               & 136.97      & 173               \\
  \hline
  \bf{ESP with camellia-cbc-192}                         & 234.17      & 166               & \emph{NA}           & \emph{NA}                     \\
  \hline 
  \bf{ESP with rijndael-cbc-192 (aes-cbc)}               & 361.34      & 166               & 413.21      & 166               \\ 
  \hline
\end{tabular}
}
\caption{IPsec tunnel mode performance measurements with various encryption algorithms with and without IKE clients.}\label{fig:IPsecTunnel}
\end{table}

%\marginpar{\emph{\textbf{Andrei}:tunnel mode means encryption and decryption occurs on the physical machines, as explained in the previous chapter. Virtual machines are totally unaware of the encryption that occurs further on the route.}}
Table~\ref{fig:IPsecTunnel} presents performance measurements for an encrypted tunnel using IPsec. We can notice that the bandwidth between two virtual machines is almost equal to the bandwidth between two physical cluster nodes. On the other hand, the latency declines from 95 $\mu$s between two physical nodes to 142 $\mu$s between two virtual machines on an unencrypted channel, as expected, due to the extra two hops.

The slowest encryption algorithm is the triple DES again, just like for the encrypted channel between two physical machines. Also, we can notice that there is very little variation in the latency from one encryption algorithm to the other.

The fastest algorithm is Rijndael or the AES, as was the case between two physical machines, hence this serves as the default encryption algorithm for our Python tool. Moreover, considering that we use tunneling with encryption, the achieved 413 MB/s is remarkable, considering that an unencrypted channel achieves approximately 940 MB/s and that the triple DES achieves only 136 MB/s. Moreover, the achieved latency of roughly 166 $\mu$s is promising compared to the approximately 142 $\mu$s achieved for an unencrypted tunnel. 

In addition, we can state that the latency drops from 123 $\mu$s between two physical machines with AES to roughly 166 $\mu$s between two virtual machines with AES, which can be explained again by the extra two hops.

\begin{table}
\centering
%\resizebox{12.5cm}{!} {
\begin{tabular}{|p{4cm}|p{1.5cm}|p{3.3cm}|}
  \hline
                                                & \bf{MB/sec} & \bf{Round-trip latency ($\mu$s)} \\
  \hline
  \bf{Blowfish-cbc-192}                         & 152.77      & 222               \\
  \hline
  \bf{3des-cbc-192}                             & 86.24       & 234               \\
  \hline
  \bf{Rijndael-cbc-192 (aes-cbc)}               & 155.08      & 222               \\
  \hline
\end{tabular}
%}
\caption{OpenVPN tunnel mode performance measurements with various encryption algorithms.}\label{fig:OpenVPNTunnel}
\end{table}

Table~\ref{fig:OpenVPNTunnel} presents performance measurements for an encrypted tunnel using OpenVPN. Again, we can notice that OpenVPN performs really badly on the same encryption algorithms being substantially outperformed by IPsec (e.g. for AES we have 155 MB/s for OpenVPN and 413 MB/s for IPsec). Moreover, this and the worse latency (e.g. 222 $\mu$s for OpenVPN and 166 $\mu$s for IPsec) can be explained by the fact that OpenVPN is an user-space tool and a lot of additional work is required for packet processing.

Table~\ref{fig:Comparisson} concludes that, from a performance point of view, IPsec is more suited for our needs clearly outperforming OpenVPN in all measurement categories. Moreover, Table~\ref{fig:Comparisson} directly compares transport and tunneled mode performance results with the best encryption (i.e. AES) and without encryption.

\begin{table}
\centering
\resizebox{12.5cm}{!} {
\begin{tabular}{|p{1.7cm}|p{2cm}|p{1cm}|p{1.5cm}|p{2cm}|p{1cm}|p{1.5cm}|}
  \hline
   & \multicolumn{3}{|p{5cm}|}{\bf{Transport mode (MB/s)}} & \multicolumn{3}{|p{5cm}|}{\bf{Tunnel mode (MB/s)}} \\
  \hline
   & Unencrypted               & IPsec AES      & OpenVPN AES & Unencrypted               & IPsec AES      & OpenVPN AES            \\
  \hline
  Bandwidth (MB/s) & 941.33                    & 471.36         & 167.77      & 941.28                    & 413.21         & 155.08            \\
  \hline
  Round-trip latency ($\mu$s) & 95                        & 123            & 178         & 142                       & 166            & 222            \\
  \hline
\end{tabular}
}
\caption{Direct comparison of bandwidth and round-trip latency for transport and tunnel mode with IPsec and OpenVPN with the AES encryption algorithm.}\label{fig:Comparisson}
\end{table}


\section{Scalability evaluation}

Now that we have established the superiority of IPsec in the context of our secure virtual clusters from a performance point of view, estimations are needed about the scalability of our approach. 

The first scalability estimation test introduces multiple security associations and tests whether these induce a performance problem, hence a scalability issue. We will accomplish this by making IPsec tunnel mode performance measurements between two virtual machines, having inserted one thousand extra security associations and policies on both endpoints besides the required settings for the tunnel mode setup. By extra security associations we understand security associations that connect an endpoint not only with the destination endpoint, but also with other random IP addresses and/or random ports on the destination endpoint.

The second scalability estimation test creates virtual clusters of size three and four and stresses one of the endpoints with all the others simultaneously using the Netperf TCP\_STREAM test on it with a duration of twenty seconds. This second test runs with racoon enabled and checks whether performance drops as the number of nodes increases. Please note that this simulates a highly data intensive computation which leaves very little time for computation, which is rarely the case in real life scenarios.

Table~\ref{fig:ExtraAssoc} presents performance measurements for an encrypted tunnel using IPsec together with the additional security associations and policies. If we compare the IPsec tunnel mode results without extra associations, found in the previous section with the measurements in this section, we can safely state that the extra associations barely affect performance, if at all. For example, for the AES encryption algorithm the bandwidth drops from 413 MB/s to 409 MB/s and the latency remains 166 $\mu$s. This concludes that a high number of security associations and policies has no influence on the IPsec achieved performance, hence partially supporting the scalability of IPsec for the Python tool.

\begin{table}
\centering
%\resizebox{12.5cm}{!} {
\begin{tabular}{|p{4cm}|p{1.5cm}|p{3.3cm}|}
  \hline
                                                & \bf{MB/sec} & \bf{Round-trip latency ($\mu$s)} \\
  \hline
  \bf{Blowfish-cbc-192}                         & 301.45      & 166               \\
  \hline
  \bf{3des-cbc-192}                             & 136.64      & 173               \\
  \hline
  \bf{Rijndael-cbc-192 (aes-cbc)}               & 409.55      & 166               \\
  \hline
\end{tabular}
%}
\caption{IPsec tunnel mode performance measurements with various encryption algorithms and many security associations and policies.}\label{fig:ExtraAssoc}
\end{table}

Table~\ref{fig:StressTest} presents the stress test results where all virtual cluster nodes send packets to the same node. It shows the bandwidth accomplished at each of the virtual machines, and their sum. This sum must be as close as possible to the values obtained in the previous section, in the tunneled configuration measurements, for no scalability issues. In the case of blowfish we see similar performances (from 301.11 MB/s to 299.47 MB/s to 298.83 for a virtual cluster of size three and four respectively). In the case of 3des, we see similar performances as well (136.97 MB/s to 133.36 MB/s to 134.25 MB/s for a virtual cluster of size three and four respectively). And finally, differences in performance are rather small for aes (413.21 MB/s to 398.17 MB/s to 398.22 MB/s for a virtual cluster of size three and four respectively). These differences are a little larger than for the other encryption algorithms but it is important to note that there is practically no difference between size three and size four.

\begin{table}
\resizebox{12.5cm}{!} {
\begin{tabular}{|p{4cm}|p{3cm}|p{3cm}|}
  \hline
                                                         & \bf{Virtual cluster of size three (MB/sec)} & \bf{Virtual cluster of size four (MB/sec)} \\
  \hline
  \bf{ESP with blowfish-cbc-192}                         & 151.26 + 148.21 = 299.47                    & 101.15 + 96.39 + 101.29 = 298.83           \\
  \hline
  \bf{ESP with 3des-cbc-192}                             & 66.33 + 67.03 = 133.36                      & 44.29 + 45.76 + 44.20 = 134.25             \\
  \hline 
  \bf{ESP with rijndael-cbc-192 (aes-cbc)}               & 208.02 + 190.15 = 398.17                    & 132.80 + 124.47 + 140.95 = 398.22          \\ 
  \hline
\end{tabular}
}
\caption{IPsec tunnel mode stress test with all virtual cluster nodes sending packets to the same node, with various encryption algorithms. }\label{fig:StressTest}
\end{table}
