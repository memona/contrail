##
# The exception that gets raised whenever there are virtual cluster problems.
#
class VirtualClusterException(Exception):
  def __init__(self, value):
    self.parameter = value

  def __str__(self):
    return repr(self.parameter)


