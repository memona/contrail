##
# The exception that gets raised whenever there are not sufficient resources
# available to satisfy a virtual cluster creation request.
#
class SchedulerNotEnoughResourcesException(Exception):
  def __init__(self, value):
    self.parameter = value

  def __str__(self):
    return repr(self.parameter)


