from vcn import vcnfromhostlist
from vcn import delvcnbyid

def example_fromhostlist():
  hostlist = []

  hostlist.append("node063.das3.cs.vu.nl")
  hostlist.append("node062.das3.cs.vu.nl")

  # Create a virtual cluster network from the provided hostlist.
  vc=vcnfromhostlist(hostlist, "aes")

  # Now we can get the virtual machine IPs for which the vcn has been
  # configured.
  hostnameiplist = vc.getvmipandgwlist()
  for t in hostnameiplist:
    print "hostname: " + t[0] + "; VM IP: " + t[1] + "; default route: " + t[2] + "; bridge to: " + t[3]

  return vc.getuniqueid()

if __name__ == "__main__":

  id1 = example_fromhostlist()
#  id2 = example_fromhostlist()

  delvcnbyid(id1, False)
#  delvcnbyid(id2, False)

#  delvcnbyid("0e0cbdfa75334aada6a98e213bbf1245", False) 
