//automatically generated from Ping.proto at Wed Oct 24 12:53:14 CEST 2012
//(c) 2012. See LICENSE file for details.

#ifndef PINGSERVICECONSTANTS_H_
#define PINGSERVICECONSTANTS_H_
#include <stdint.h>

namespace xtreemfs {
namespace pbrpc {

const uint32_t INTERFACE_ID_PING = 1;
const uint32_t PROC_ID_DOPING = 1;
const uint32_t PROC_ID_EMPTYPING = 2;

}  // namespace pbrpc
}  // namespace xtreemfs

#endif // PINGSERVICECLIENT_H_
