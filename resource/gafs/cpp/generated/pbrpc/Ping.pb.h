// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: pbrpc/Ping.proto

#ifndef PROTOBUF_pbrpc_2fPing_2eproto__INCLUDED
#define PROTOBUF_pbrpc_2fPing_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 2003000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 2003000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/service.h>
#include "include/PBRPC.pb.h"
// @@protoc_insertion_point(includes)

namespace xtreemfs {
namespace pbrpc {

// Internal implementation detail -- do not call these.
void  protobuf_AddDesc_pbrpc_2fPing_2eproto();
void protobuf_AssignDesc_pbrpc_2fPing_2eproto();
void protobuf_ShutdownFile_pbrpc_2fPing_2eproto();

class PingRequest;
class PingResponse;
class PingResponse_PingResult;
class PingResponse_PingError;
class Ping_emptyRequest;
class Ping_emptyResponse;

// ===================================================================

class PingRequest : public ::google::protobuf::Message {
 public:
  PingRequest();
  virtual ~PingRequest();
  
  PingRequest(const PingRequest& from);
  
  inline PingRequest& operator=(const PingRequest& from) {
    CopyFrom(from);
    return *this;
  }
  
  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }
  
  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }
  
  static const ::google::protobuf::Descriptor* descriptor();
  static const PingRequest& default_instance();
  
  void Swap(PingRequest* other);
  
  // implements Message ----------------------------------------------
  
  PingRequest* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const PingRequest& from);
  void MergeFrom(const PingRequest& from);
  void Clear();
  bool IsInitialized() const;
  
  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  
  ::google::protobuf::Metadata GetMetadata() const;
  
  // nested types ----------------------------------------------------
  
  // accessors -------------------------------------------------------
  
  // required string text = 1;
  inline bool has_text() const;
  inline void clear_text();
  static const int kTextFieldNumber = 1;
  inline const ::std::string& text() const;
  inline void set_text(const ::std::string& value);
  inline void set_text(const char* value);
  inline void set_text(const char* value, size_t size);
  inline ::std::string* mutable_text();
  
  // required bool sendError = 2;
  inline bool has_senderror() const;
  inline void clear_senderror();
  static const int kSendErrorFieldNumber = 2;
  inline bool senderror() const;
  inline void set_senderror(bool value);
  
  // @@protoc_insertion_point(class_scope:xtreemfs.pbrpc.PingRequest)
 private:
  ::google::protobuf::UnknownFieldSet _unknown_fields_;
  mutable int _cached_size_;
  
  ::std::string* text_;
  static const ::std::string _default_text_;
  bool senderror_;
  friend void  protobuf_AddDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_AssignDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_ShutdownFile_pbrpc_2fPing_2eproto();
  
  ::google::protobuf::uint32 _has_bits_[(2 + 31) / 32];
  
  // WHY DOES & HAVE LOWER PRECEDENCE THAN != !?
  inline bool _has_bit(int index) const {
    return (_has_bits_[index / 32] & (1u << (index % 32))) != 0;
  }
  inline void _set_bit(int index) {
    _has_bits_[index / 32] |= (1u << (index % 32));
  }
  inline void _clear_bit(int index) {
    _has_bits_[index / 32] &= ~(1u << (index % 32));
  }
  
  void InitAsDefaultInstance();
  static PingRequest* default_instance_;
};
// -------------------------------------------------------------------

class PingResponse_PingResult : public ::google::protobuf::Message {
 public:
  PingResponse_PingResult();
  virtual ~PingResponse_PingResult();
  
  PingResponse_PingResult(const PingResponse_PingResult& from);
  
  inline PingResponse_PingResult& operator=(const PingResponse_PingResult& from) {
    CopyFrom(from);
    return *this;
  }
  
  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }
  
  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }
  
  static const ::google::protobuf::Descriptor* descriptor();
  static const PingResponse_PingResult& default_instance();
  
  void Swap(PingResponse_PingResult* other);
  
  // implements Message ----------------------------------------------
  
  PingResponse_PingResult* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const PingResponse_PingResult& from);
  void MergeFrom(const PingResponse_PingResult& from);
  void Clear();
  bool IsInitialized() const;
  
  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  
  ::google::protobuf::Metadata GetMetadata() const;
  
  // nested types ----------------------------------------------------
  
  // accessors -------------------------------------------------------
  
  // required string text = 1;
  inline bool has_text() const;
  inline void clear_text();
  static const int kTextFieldNumber = 1;
  inline const ::std::string& text() const;
  inline void set_text(const ::std::string& value);
  inline void set_text(const char* value);
  inline void set_text(const char* value, size_t size);
  inline ::std::string* mutable_text();
  
  // @@protoc_insertion_point(class_scope:xtreemfs.pbrpc.PingResponse.PingResult)
 private:
  ::google::protobuf::UnknownFieldSet _unknown_fields_;
  mutable int _cached_size_;
  
  ::std::string* text_;
  static const ::std::string _default_text_;
  friend void  protobuf_AddDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_AssignDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_ShutdownFile_pbrpc_2fPing_2eproto();
  
  ::google::protobuf::uint32 _has_bits_[(1 + 31) / 32];
  
  // WHY DOES & HAVE LOWER PRECEDENCE THAN != !?
  inline bool _has_bit(int index) const {
    return (_has_bits_[index / 32] & (1u << (index % 32))) != 0;
  }
  inline void _set_bit(int index) {
    _has_bits_[index / 32] |= (1u << (index % 32));
  }
  inline void _clear_bit(int index) {
    _has_bits_[index / 32] &= ~(1u << (index % 32));
  }
  
  void InitAsDefaultInstance();
  static PingResponse_PingResult* default_instance_;
};
// -------------------------------------------------------------------

class PingResponse_PingError : public ::google::protobuf::Message {
 public:
  PingResponse_PingError();
  virtual ~PingResponse_PingError();
  
  PingResponse_PingError(const PingResponse_PingError& from);
  
  inline PingResponse_PingError& operator=(const PingResponse_PingError& from) {
    CopyFrom(from);
    return *this;
  }
  
  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }
  
  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }
  
  static const ::google::protobuf::Descriptor* descriptor();
  static const PingResponse_PingError& default_instance();
  
  void Swap(PingResponse_PingError* other);
  
  // implements Message ----------------------------------------------
  
  PingResponse_PingError* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const PingResponse_PingError& from);
  void MergeFrom(const PingResponse_PingError& from);
  void Clear();
  bool IsInitialized() const;
  
  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  
  ::google::protobuf::Metadata GetMetadata() const;
  
  // nested types ----------------------------------------------------
  
  // accessors -------------------------------------------------------
  
  // required string errorMessage = 1;
  inline bool has_errormessage() const;
  inline void clear_errormessage();
  static const int kErrorMessageFieldNumber = 1;
  inline const ::std::string& errormessage() const;
  inline void set_errormessage(const ::std::string& value);
  inline void set_errormessage(const char* value);
  inline void set_errormessage(const char* value, size_t size);
  inline ::std::string* mutable_errormessage();
  
  // @@protoc_insertion_point(class_scope:xtreemfs.pbrpc.PingResponse.PingError)
 private:
  ::google::protobuf::UnknownFieldSet _unknown_fields_;
  mutable int _cached_size_;
  
  ::std::string* errormessage_;
  static const ::std::string _default_errormessage_;
  friend void  protobuf_AddDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_AssignDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_ShutdownFile_pbrpc_2fPing_2eproto();
  
  ::google::protobuf::uint32 _has_bits_[(1 + 31) / 32];
  
  // WHY DOES & HAVE LOWER PRECEDENCE THAN != !?
  inline bool _has_bit(int index) const {
    return (_has_bits_[index / 32] & (1u << (index % 32))) != 0;
  }
  inline void _set_bit(int index) {
    _has_bits_[index / 32] |= (1u << (index % 32));
  }
  inline void _clear_bit(int index) {
    _has_bits_[index / 32] &= ~(1u << (index % 32));
  }
  
  void InitAsDefaultInstance();
  static PingResponse_PingError* default_instance_;
};
// -------------------------------------------------------------------

class PingResponse : public ::google::protobuf::Message {
 public:
  PingResponse();
  virtual ~PingResponse();
  
  PingResponse(const PingResponse& from);
  
  inline PingResponse& operator=(const PingResponse& from) {
    CopyFrom(from);
    return *this;
  }
  
  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }
  
  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }
  
  static const ::google::protobuf::Descriptor* descriptor();
  static const PingResponse& default_instance();
  
  void Swap(PingResponse* other);
  
  // implements Message ----------------------------------------------
  
  PingResponse* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const PingResponse& from);
  void MergeFrom(const PingResponse& from);
  void Clear();
  bool IsInitialized() const;
  
  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  
  ::google::protobuf::Metadata GetMetadata() const;
  
  // nested types ----------------------------------------------------
  
  typedef PingResponse_PingResult PingResult;
  typedef PingResponse_PingError PingError;
  
  // accessors -------------------------------------------------------
  
  // optional .xtreemfs.pbrpc.PingResponse.PingResult result = 1;
  inline bool has_result() const;
  inline void clear_result();
  static const int kResultFieldNumber = 1;
  inline const ::xtreemfs::pbrpc::PingResponse_PingResult& result() const;
  inline ::xtreemfs::pbrpc::PingResponse_PingResult* mutable_result();
  
  // optional .xtreemfs.pbrpc.PingResponse.PingError error = 2;
  inline bool has_error() const;
  inline void clear_error();
  static const int kErrorFieldNumber = 2;
  inline const ::xtreemfs::pbrpc::PingResponse_PingError& error() const;
  inline ::xtreemfs::pbrpc::PingResponse_PingError* mutable_error();
  
  // @@protoc_insertion_point(class_scope:xtreemfs.pbrpc.PingResponse)
 private:
  ::google::protobuf::UnknownFieldSet _unknown_fields_;
  mutable int _cached_size_;
  
  ::xtreemfs::pbrpc::PingResponse_PingResult* result_;
  ::xtreemfs::pbrpc::PingResponse_PingError* error_;
  friend void  protobuf_AddDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_AssignDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_ShutdownFile_pbrpc_2fPing_2eproto();
  
  ::google::protobuf::uint32 _has_bits_[(2 + 31) / 32];
  
  // WHY DOES & HAVE LOWER PRECEDENCE THAN != !?
  inline bool _has_bit(int index) const {
    return (_has_bits_[index / 32] & (1u << (index % 32))) != 0;
  }
  inline void _set_bit(int index) {
    _has_bits_[index / 32] |= (1u << (index % 32));
  }
  inline void _clear_bit(int index) {
    _has_bits_[index / 32] &= ~(1u << (index % 32));
  }
  
  void InitAsDefaultInstance();
  static PingResponse* default_instance_;
};
// -------------------------------------------------------------------

class Ping_emptyRequest : public ::google::protobuf::Message {
 public:
  Ping_emptyRequest();
  virtual ~Ping_emptyRequest();
  
  Ping_emptyRequest(const Ping_emptyRequest& from);
  
  inline Ping_emptyRequest& operator=(const Ping_emptyRequest& from) {
    CopyFrom(from);
    return *this;
  }
  
  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }
  
  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }
  
  static const ::google::protobuf::Descriptor* descriptor();
  static const Ping_emptyRequest& default_instance();
  
  void Swap(Ping_emptyRequest* other);
  
  // implements Message ----------------------------------------------
  
  Ping_emptyRequest* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const Ping_emptyRequest& from);
  void MergeFrom(const Ping_emptyRequest& from);
  void Clear();
  bool IsInitialized() const;
  
  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  
  ::google::protobuf::Metadata GetMetadata() const;
  
  // nested types ----------------------------------------------------
  
  // accessors -------------------------------------------------------
  
  // @@protoc_insertion_point(class_scope:xtreemfs.pbrpc.Ping_emptyRequest)
 private:
  ::google::protobuf::UnknownFieldSet _unknown_fields_;
  mutable int _cached_size_;
  
  friend void  protobuf_AddDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_AssignDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_ShutdownFile_pbrpc_2fPing_2eproto();
  
  ::google::protobuf::uint32 _has_bits_[1];
  
  // WHY DOES & HAVE LOWER PRECEDENCE THAN != !?
  inline bool _has_bit(int index) const {
    return (_has_bits_[index / 32] & (1u << (index % 32))) != 0;
  }
  inline void _set_bit(int index) {
    _has_bits_[index / 32] |= (1u << (index % 32));
  }
  inline void _clear_bit(int index) {
    _has_bits_[index / 32] &= ~(1u << (index % 32));
  }
  
  void InitAsDefaultInstance();
  static Ping_emptyRequest* default_instance_;
};
// -------------------------------------------------------------------

class Ping_emptyResponse : public ::google::protobuf::Message {
 public:
  Ping_emptyResponse();
  virtual ~Ping_emptyResponse();
  
  Ping_emptyResponse(const Ping_emptyResponse& from);
  
  inline Ping_emptyResponse& operator=(const Ping_emptyResponse& from) {
    CopyFrom(from);
    return *this;
  }
  
  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }
  
  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }
  
  static const ::google::protobuf::Descriptor* descriptor();
  static const Ping_emptyResponse& default_instance();
  
  void Swap(Ping_emptyResponse* other);
  
  // implements Message ----------------------------------------------
  
  Ping_emptyResponse* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const Ping_emptyResponse& from);
  void MergeFrom(const Ping_emptyResponse& from);
  void Clear();
  bool IsInitialized() const;
  
  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  
  ::google::protobuf::Metadata GetMetadata() const;
  
  // nested types ----------------------------------------------------
  
  // accessors -------------------------------------------------------
  
  // @@protoc_insertion_point(class_scope:xtreemfs.pbrpc.Ping_emptyResponse)
 private:
  ::google::protobuf::UnknownFieldSet _unknown_fields_;
  mutable int _cached_size_;
  
  friend void  protobuf_AddDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_AssignDesc_pbrpc_2fPing_2eproto();
  friend void protobuf_ShutdownFile_pbrpc_2fPing_2eproto();
  
  ::google::protobuf::uint32 _has_bits_[1];
  
  // WHY DOES & HAVE LOWER PRECEDENCE THAN != !?
  inline bool _has_bit(int index) const {
    return (_has_bits_[index / 32] & (1u << (index % 32))) != 0;
  }
  inline void _set_bit(int index) {
    _has_bits_[index / 32] |= (1u << (index % 32));
  }
  inline void _clear_bit(int index) {
    _has_bits_[index / 32] &= ~(1u << (index % 32));
  }
  
  void InitAsDefaultInstance();
  static Ping_emptyResponse* default_instance_;
};
// ===================================================================

class PingService_Stub;

class PingService : public ::google::protobuf::Service {
 protected:
  // This class should be treated as an abstract interface.
  inline PingService() {};
 public:
  virtual ~PingService();
  
  typedef PingService_Stub Stub;
  
  static const ::google::protobuf::ServiceDescriptor* descriptor();
  
  virtual void doPing(::google::protobuf::RpcController* controller,
                       const ::xtreemfs::pbrpc::PingRequest* request,
                       ::xtreemfs::pbrpc::PingResponse* response,
                       ::google::protobuf::Closure* done);
  virtual void emptyPing(::google::protobuf::RpcController* controller,
                       const ::xtreemfs::pbrpc::Ping_emptyRequest* request,
                       ::xtreemfs::pbrpc::Ping_emptyResponse* response,
                       ::google::protobuf::Closure* done);
  
  // implements Service ----------------------------------------------
  
  const ::google::protobuf::ServiceDescriptor* GetDescriptor();
  void CallMethod(const ::google::protobuf::MethodDescriptor* method,
                  ::google::protobuf::RpcController* controller,
                  const ::google::protobuf::Message* request,
                  ::google::protobuf::Message* response,
                  ::google::protobuf::Closure* done);
  const ::google::protobuf::Message& GetRequestPrototype(
    const ::google::protobuf::MethodDescriptor* method) const;
  const ::google::protobuf::Message& GetResponsePrototype(
    const ::google::protobuf::MethodDescriptor* method) const;

 private:
  GOOGLE_DISALLOW_EVIL_CONSTRUCTORS(PingService);
};

class PingService_Stub : public PingService {
 public:
  PingService_Stub(::google::protobuf::RpcChannel* channel);
  PingService_Stub(::google::protobuf::RpcChannel* channel,
                   ::google::protobuf::Service::ChannelOwnership ownership);
  ~PingService_Stub();
  
  inline ::google::protobuf::RpcChannel* channel() { return channel_; }
  
  // implements PingService ------------------------------------------
  
  void doPing(::google::protobuf::RpcController* controller,
                       const ::xtreemfs::pbrpc::PingRequest* request,
                       ::xtreemfs::pbrpc::PingResponse* response,
                       ::google::protobuf::Closure* done);
  void emptyPing(::google::protobuf::RpcController* controller,
                       const ::xtreemfs::pbrpc::Ping_emptyRequest* request,
                       ::xtreemfs::pbrpc::Ping_emptyResponse* response,
                       ::google::protobuf::Closure* done);
 private:
  ::google::protobuf::RpcChannel* channel_;
  bool owns_channel_;
  GOOGLE_DISALLOW_EVIL_CONSTRUCTORS(PingService_Stub);
};


// ===================================================================


// ===================================================================

// PingRequest

// required string text = 1;
inline bool PingRequest::has_text() const {
  return _has_bit(0);
}
inline void PingRequest::clear_text() {
  if (text_ != &_default_text_) {
    text_->clear();
  }
  _clear_bit(0);
}
inline const ::std::string& PingRequest::text() const {
  return *text_;
}
inline void PingRequest::set_text(const ::std::string& value) {
  _set_bit(0);
  if (text_ == &_default_text_) {
    text_ = new ::std::string;
  }
  text_->assign(value);
}
inline void PingRequest::set_text(const char* value) {
  _set_bit(0);
  if (text_ == &_default_text_) {
    text_ = new ::std::string;
  }
  text_->assign(value);
}
inline void PingRequest::set_text(const char* value, size_t size) {
  _set_bit(0);
  if (text_ == &_default_text_) {
    text_ = new ::std::string;
  }
  text_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* PingRequest::mutable_text() {
  _set_bit(0);
  if (text_ == &_default_text_) {
    text_ = new ::std::string;
  }
  return text_;
}

// required bool sendError = 2;
inline bool PingRequest::has_senderror() const {
  return _has_bit(1);
}
inline void PingRequest::clear_senderror() {
  senderror_ = false;
  _clear_bit(1);
}
inline bool PingRequest::senderror() const {
  return senderror_;
}
inline void PingRequest::set_senderror(bool value) {
  _set_bit(1);
  senderror_ = value;
}

// -------------------------------------------------------------------

// PingResponse_PingResult

// required string text = 1;
inline bool PingResponse_PingResult::has_text() const {
  return _has_bit(0);
}
inline void PingResponse_PingResult::clear_text() {
  if (text_ != &_default_text_) {
    text_->clear();
  }
  _clear_bit(0);
}
inline const ::std::string& PingResponse_PingResult::text() const {
  return *text_;
}
inline void PingResponse_PingResult::set_text(const ::std::string& value) {
  _set_bit(0);
  if (text_ == &_default_text_) {
    text_ = new ::std::string;
  }
  text_->assign(value);
}
inline void PingResponse_PingResult::set_text(const char* value) {
  _set_bit(0);
  if (text_ == &_default_text_) {
    text_ = new ::std::string;
  }
  text_->assign(value);
}
inline void PingResponse_PingResult::set_text(const char* value, size_t size) {
  _set_bit(0);
  if (text_ == &_default_text_) {
    text_ = new ::std::string;
  }
  text_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* PingResponse_PingResult::mutable_text() {
  _set_bit(0);
  if (text_ == &_default_text_) {
    text_ = new ::std::string;
  }
  return text_;
}

// -------------------------------------------------------------------

// PingResponse_PingError

// required string errorMessage = 1;
inline bool PingResponse_PingError::has_errormessage() const {
  return _has_bit(0);
}
inline void PingResponse_PingError::clear_errormessage() {
  if (errormessage_ != &_default_errormessage_) {
    errormessage_->clear();
  }
  _clear_bit(0);
}
inline const ::std::string& PingResponse_PingError::errormessage() const {
  return *errormessage_;
}
inline void PingResponse_PingError::set_errormessage(const ::std::string& value) {
  _set_bit(0);
  if (errormessage_ == &_default_errormessage_) {
    errormessage_ = new ::std::string;
  }
  errormessage_->assign(value);
}
inline void PingResponse_PingError::set_errormessage(const char* value) {
  _set_bit(0);
  if (errormessage_ == &_default_errormessage_) {
    errormessage_ = new ::std::string;
  }
  errormessage_->assign(value);
}
inline void PingResponse_PingError::set_errormessage(const char* value, size_t size) {
  _set_bit(0);
  if (errormessage_ == &_default_errormessage_) {
    errormessage_ = new ::std::string;
  }
  errormessage_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* PingResponse_PingError::mutable_errormessage() {
  _set_bit(0);
  if (errormessage_ == &_default_errormessage_) {
    errormessage_ = new ::std::string;
  }
  return errormessage_;
}

// -------------------------------------------------------------------

// PingResponse

// optional .xtreemfs.pbrpc.PingResponse.PingResult result = 1;
inline bool PingResponse::has_result() const {
  return _has_bit(0);
}
inline void PingResponse::clear_result() {
  if (result_ != NULL) result_->::xtreemfs::pbrpc::PingResponse_PingResult::Clear();
  _clear_bit(0);
}
inline const ::xtreemfs::pbrpc::PingResponse_PingResult& PingResponse::result() const {
  return result_ != NULL ? *result_ : *default_instance_->result_;
}
inline ::xtreemfs::pbrpc::PingResponse_PingResult* PingResponse::mutable_result() {
  _set_bit(0);
  if (result_ == NULL) result_ = new ::xtreemfs::pbrpc::PingResponse_PingResult;
  return result_;
}

// optional .xtreemfs.pbrpc.PingResponse.PingError error = 2;
inline bool PingResponse::has_error() const {
  return _has_bit(1);
}
inline void PingResponse::clear_error() {
  if (error_ != NULL) error_->::xtreemfs::pbrpc::PingResponse_PingError::Clear();
  _clear_bit(1);
}
inline const ::xtreemfs::pbrpc::PingResponse_PingError& PingResponse::error() const {
  return error_ != NULL ? *error_ : *default_instance_->error_;
}
inline ::xtreemfs::pbrpc::PingResponse_PingError* PingResponse::mutable_error() {
  _set_bit(1);
  if (error_ == NULL) error_ = new ::xtreemfs::pbrpc::PingResponse_PingError;
  return error_;
}

// -------------------------------------------------------------------

// Ping_emptyRequest

// -------------------------------------------------------------------

// Ping_emptyResponse


// @@protoc_insertion_point(namespace_scope)

}  // namespace pbrpc
}  // namespace xtreemfs

#ifndef SWIG
namespace google {
namespace protobuf {


}  // namespace google
}  // namespace protobuf
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_pbrpc_2fPing_2eproto__INCLUDED
