//automatically generated at Wed Oct 24 12:53:13 CEST 2012
//(c) 2012. See LICENSE file for details.

#ifndef CPP_GENERATED_XTREEMFS_GET_REQUEST_MESSAGE_H_
#define CPP_GENERATED_XTREEMFS_GET_REQUEST_MESSAGE_H_

#include <stdint.h>

namespace google {
namespace protobuf {
class Message;
}  // namespace protobuf
}  // namespace google

namespace xtreemfs {
namespace pbrpc {

google::protobuf::Message* GetMessageForProcID(uint32_t interface_id,
                                               uint32_t proc_id);

}  // namespace pbrpc
}  // namespace xtreemfs

#endif  // CPP_GENERATED_XTREEMFS_GET_REQUEST_MESSAGE_H_
