Format: 1.0
Source: xtreemfs
Version: _VERSION_
Binary: xtreemfs-check-packages
Maintainer: xtreemfs <xtreemfs-users@googlegroups.com>
Architecture: any
Build-Depends:
  debhelper (>= 5),
  build-essential (>=11),
  xtreemfs-client (=_VERSION_), xtreemfs-server (=_VERSION_), xtreemfs-tools (=_VERSION_)
Files: 
 d57283ebb8157ae919762c58419353c8 133282 XtreemFS-_VERSION_.tar.gz
