#!/usr/bin/env ruby
require 'socket'

begin
	if(!ARGV.at(3))
		puts("4 arguments required")
	else
		sck = TCPSocket.new("127.0.0.1", 4321)
		if(sck)
			sck.write(ARGV[0] + "_" + ARGV[1] + "_" + ARGV[2] + "_" + ARGV[3])
			sck.close
		end
	end	
rescue Errno::ECONNREFUSED
	p 'TCP socket connection refused on port 4321 - is sensor running?'	
end


