/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */


package eu.contrail.infrastructure_monitoring.monitors;

import java.util.Date;
import java.util.List;

import eu.contrail.infrastructure_monitoring.exceptions.MetricNotSupportedException;
import eu.contrail.infrastructure_monitoring.monitors.data.MetricData;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;

public interface IMonitoringEngine {

    public enum Status {
        OK,
        ERROR,
        BUSY;
    }

    public void query() throws Exception;

    public Status getStatus();

    public Date getMetricsCollectedDate();

    public MetricData getVmMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException;

    public MetricData getHostMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException;

    public boolean checkVmExists(String fqdn);

    public boolean checkHostExists(String fqdn);

    public String getVmHostMachine(String fqdn);

    public List<String> getGuestMachines(String hostFqdn);

    public List<String> getAllHostMachines();

    public List<String> getAllGuestMachines();

    public String getInfrastructureLayoutXml();

    //import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
    //public boolean isMonitoringDataAvailable(Service service);

	public boolean isVmMonitoringDataAvailable(String[] fqdns);
}
