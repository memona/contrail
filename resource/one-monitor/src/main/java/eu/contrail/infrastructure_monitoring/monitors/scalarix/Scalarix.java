package eu.contrail.infrastructure_monitoring.monitors.scalarix;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eu.contrail.infrastructure_monitoring.enums.DataType;
import eu.contrail.infrastructure_monitoring.monitors.data.MetricData;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;
import eu.contrail.infrastructure_monitoring.monitors.data.ScalarixData;

public class Scalarix {
	private ScalarixGangliaMonitor gangliaMonitor;
	private DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	private DocumentBuilder db;
	private Node docEle = null;
	private XPathFactory factory = XPathFactory.newInstance();
    private XPath xpath = factory.newXPath();
    private List<ScalarixData> nodesMetrics = new ArrayList<ScalarixData>();
    private boolean hasData;
    
	private static Logger log = Logger.getLogger(Scalarix.class);
	
	public Scalarix() {
		hasData = false;
		gangliaMonitor = new ScalarixGangliaMonitor(ScalarixMonitoringEngine.GANGLIA_HOST, ScalarixMonitoringEngine.GANGLIA_PORT);
	}
	
	public boolean hasData() {
		return hasData;
	}
	
	public List<ScalarixData> getNodesMetrics() {
		if(gangliaMonitor.getXml() != null && !gangliaMonitor.getXml().isEmpty()) {
			parseGangliaNodesMetrics();
		}
		else
			log.warn("Scalarix Ganglia metrics empty?");
		
		return nodesMetrics;
	}
	
	private void parseGangliaNodesMetrics() {
		try {
			db = dbf.newDocumentBuilder();
			Document dom = db.parse(new ByteArrayInputStream(gangliaMonitor.getXml().getBytes()));
			this.docEle = dom.getDocumentElement();
			NodeList children = this.docEle.getChildNodes();
			// Cluster seems to be the second element for the parser
			NodeList cluster = (NodeList) children.item(1);
			NodeList hosts = (NodeList) xpath.evaluate("HOST", cluster, XPathConstants.NODESET);
			if(hosts != null && hosts.getLength() > 0) {
				for(int i=0; i<hosts.getLength(); i++) {
					Node host = hosts.item(i);
					
					String fqdn = xpath.evaluate("@NAME", host);
					log.trace("FQDN: " + fqdn);
					if(fqdn != null && !fqdn.isEmpty()) {
						NodeList metrics = (NodeList) xpath.evaluate("METRIC", host, XPathConstants.NODESET);
						if(metrics != null && metrics.getLength() > 0) {
							ScalarixData sn = new ScalarixData();
							sn.setFqdn(fqdn);
							Node metric = null;
							
							metric = (Node) xpath.evaluate("METRIC[@NAME='" + RawMetric.SCALARIX_GANGLIA_ERLANG_PROCESSES.getMetricTitle() + "']", host, XPathConstants.NODE);
							if(metric != null) {
								String metricVal = (String) xpath.evaluate("@VAL", metric, XPathConstants.STRING);
								if(metricVal != null) {
									sn.putMetricData(new MetricData(RawMetric.SCALARIX_GANGLIA_ERLANG_PROCESSES, Integer.parseInt(metricVal), DataType.INTEGER, ""));
									log.trace("Setting Erlang processes to " + metricVal);	
								}
							}
							
							metric = (Node) xpath.evaluate("METRIC[@NAME='" + RawMetric.SCALARIX_GANGLIA_MEMORY_ATOMS.getMetricTitle() + "']", host, XPathConstants.NODE);
							if(metric != null) {
								String metricVal = (String) xpath.evaluate("@VAL", metric, XPathConstants.STRING);
								if(metricVal != null) {
									sn.putMetricData(new MetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_ATOMS, Integer.parseInt(metricVal), DataType.INTEGER, "B"));
									log.trace("Setting Erlang memory atoms to " + metricVal);	
								}
							}
							
							metric = (Node) xpath.evaluate("METRIC[@NAME='" + RawMetric.SCALARIX_GANGLIA_MEMORY_BINARIES.getMetricTitle() + "']", host, XPathConstants.NODE);
							if(metric != null) {
								String metricVal = (String) xpath.evaluate("@VAL", metric, XPathConstants.STRING);
								if(metricVal != null) {
									sn.putMetricData(new MetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_BINARIES, Integer.parseInt(metricVal), DataType.INTEGER, "B"));
									log.trace("Setting Erlang memory binaries to " + metricVal);	
								}
							}
							
							metric = (Node) xpath.evaluate("METRIC[@NAME='" + RawMetric.SCALARIX_GANGLIA_MEMORY_DHT.getMetricTitle() + "']", host, XPathConstants.NODE);
							if(metric != null) {
								String metricVal = (String) xpath.evaluate("@VAL", metric, XPathConstants.STRING);
								if(metricVal != null) {
									sn.putMetricData(new MetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_DHT, Integer.parseInt(metricVal), DataType.INTEGER, "B"));
									log.trace("Setting Erlang memory DHT to " + metricVal);	
								}
							}
							
							metric = (Node) xpath.evaluate("METRIC[@NAME='" + RawMetric.SCALARIX_GANGLIA_MEMORY_ERLANG_PROCESSES.getMetricTitle() + "']", host, XPathConstants.NODE);
							if(metric != null) {
								String metricVal = (String) xpath.evaluate("@VAL", metric, XPathConstants.STRING);
								if(metricVal != null) {
									sn.putMetricData(new MetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_ERLANG_PROCESSES, Integer.parseInt(metricVal), DataType.INTEGER, "B"));
									log.trace("Setting memory Erlang processes to " + metricVal);	
								}
							}
							
							metric = (Node) xpath.evaluate("METRIC[@NAME='" + RawMetric.SCALARIX_GANGLIA_MEMORY_ETS.getMetricTitle() + "']", host, XPathConstants.NODE);
							if(metric != null) {
								String metricVal = (String) xpath.evaluate("@VAL", metric, XPathConstants.STRING);
								if(metricVal != null) {
									sn.putMetricData(new MetricData(RawMetric.SCALARIX_GANGLIA_MEMORY_ETS, Integer.parseInt(metricVal), DataType.INTEGER, "B"));
									log.trace("Setting memory for ETS tables to " + metricVal);	
								}
							}
							
							metric = (Node) xpath.evaluate("METRIC[@NAME='" + RawMetric.SCALARIX_GANGLIA_TRANSACTIONS_PER_SECOND.getMetricTitle() + "']", host, XPathConstants.NODE);
							if(metric != null) {
								String metricVal = (String) xpath.evaluate("@VAL", metric, XPathConstants.STRING);
								if(metricVal != null) {
									sn.putMetricData(new MetricData(RawMetric.SCALARIX_GANGLIA_TRANSACTIONS_PER_SECOND, Float.parseFloat(metricVal), DataType.DOUBLE, ""));
									log.trace("Setting number of transactions per second to " + metricVal);	
								}
							}
							
							metric = (Node) xpath.evaluate("METRIC[@NAME='" + RawMetric.SCALARIX_GANGLIA_AVG_TRANSACTION_LATENCY.getMetricTitle() + "']", host, XPathConstants.NODE);
							if(metric != null) {
								String metricVal = (String) xpath.evaluate("@VAL", metric, XPathConstants.STRING);
								if(metricVal != null) {
									sn.putMetricData(new MetricData(RawMetric.SCALARIX_GANGLIA_AVG_TRANSACTION_LATENCY, Float.parseFloat(metricVal), DataType.DOUBLE, "ms"));
									log.trace("Setting average transaction latency to " + metricVal);	
								}
							}
							
							metric = (Node) xpath.evaluate("METRIC[@NAME='" + RawMetric.SCALARIX_GANGLIA_KV_PAIRS.getMetricTitle() + "']", host, XPathConstants.NODE);
							if(metric != null) {
								String metricVal = (String) xpath.evaluate("@VAL", metric, XPathConstants.STRING);
								if(metricVal != null) {
									sn.putMetricData(new MetricData(RawMetric.SCALARIX_GANGLIA_KV_PAIRS, Integer.parseInt(metricVal), DataType.INTEGER, ""));
									log.trace("Setting key-value pairs to " + metricVal);	
								}
							}
							
							// Only add to the list if it contains Scalarix metrics
							if(sn.hasMetricData()) {
								nodesMetrics.add(sn);
							}
						}
					}
				}
				hasData = true;
			}
			else
				log.info("Hosts empty");
			
	    } catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
