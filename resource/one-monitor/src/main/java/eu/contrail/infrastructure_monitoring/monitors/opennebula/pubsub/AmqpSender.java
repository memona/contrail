/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */


package eu.contrail.infrastructure_monitoring.monitors.opennebula.pubsub;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;

public class AmqpSender {
	public static final String EXCHANGE_NAME = "input";
	public static final String ROUTING_KEY_PREFIX = "input";
	public static final boolean DURABLE_FLAG = false;
	public static final boolean AUTO_DELETE_FLAG = true;
	public static final boolean INTERNAL_FLAG = false;
	
	private static Logger log = Logger.getLogger(AmqpSender.class);
    
	private static Channel channel = null;
	private static AmqpSender amqpSender;
	
	public static final String MESSAGE_TYPE_HOST = "host";
	public static final String MESSAGE_TYPE_VM = "vm";
	public static final String MESSAGE_TYPE_EVENTS = "events";
	
	private AmqpSender() throws IOException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(OpenNebula.RABBIT_MQ_HOST);
        Connection connection = factory.newConnection();
        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "topic", DURABLE_FLAG, AUTO_DELETE_FLAG, INTERNAL_FLAG, null);
	}	
	
    public static AmqpSender getInstance() {
    	if(amqpSender == null)
			try {
				amqpSender = new AmqpSender();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	return amqpSender;
    }

    public void sendMessages(List<AmqpData> messages) {
       	for(AmqpData d : messages) {
           	sendMessage(d.getType(), d.getRoutingKey(), d.getXml());
        }
    }
    
    public void sendEventMessage(String routingKey, String message) {
    	sendMessage(MESSAGE_TYPE_EVENTS, routingKey, message);
    }
    
    private void sendMessage(String type, String routingKey, String message) {
        String fullRoutingKey = ROUTING_KEY_PREFIX + "." + type + ".";
        if(routingKey != null && !routingKey.isEmpty()) {
        	fullRoutingKey += routingKey;
        }
    	log.trace("Publishing " + message + " with routing key " + fullRoutingKey + " on exchange "+ EXCHANGE_NAME);
		try {
			channel.basicPublish(EXCHANGE_NAME, fullRoutingKey, null, message.getBytes());
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
