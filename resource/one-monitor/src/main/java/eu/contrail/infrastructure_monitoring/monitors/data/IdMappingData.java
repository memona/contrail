package eu.contrail.infrastructure_monitoring.monitors.data;

import eu.contrail.infrastructure_monitoring.monitors.opennebula.pubsub.AmqpSender;

public class IdMappingData {
	private String sid;
	private String oneId;
	private String vepId;
	private String ovfId;
	
	public IdMappingData(String sid) {
		this.setSid(sid);
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getVepId() {
		return vepId;
	}

	public void setVepId(String vepId) {
		this.vepId = vepId;
	}

	public String getOvfId() {
		return ovfId;
	}

	public void setOvfId(String ovfId) {
		this.ovfId = ovfId;
	}

	public String getOneId() {
		return oneId;
	}

	public void setOneId(String oneId) {
		this.oneId = oneId;
	}
	
	public String getRoutingKey() {
		return "ovfid." + this.ovfId + ".vepid." + getVepId();
	}
	
	private static String getRoutingKeyPrefix() {
		return AmqpSender.ROUTING_KEY_PREFIX + ".";
	}
	
	public String getFullRoutingKey() {
		return getRoutingKeyPrefix() + getRoutingKey() + ".#";
	}

	public static String getFullRoutingKeyByOvfId(String ovfId) {
		return getRoutingKeyPrefix() + ovfId + ".#";
	}
}