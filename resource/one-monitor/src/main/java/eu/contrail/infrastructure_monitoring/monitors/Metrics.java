/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.monitors;

import eu.contrail.infrastructure_monitoring.exceptions.MetricNotSupportedException;
import eu.contrail.infrastructure_monitoring.monitors.data.MetricData;
import eu.contrail.infrastructure_monitoring.monitors.data.MonitoringData;
import eu.contrail.infrastructure_monitoring.monitors.data.RawMetric;
import eu.contrail.infrastructure_monitoring.monitors.data.VmData;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OneExporter;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class Metrics {

    private MonitoringData vmLayoutData;
    private MonitoringData clusterConfData;

    private static RawMetric[] clusterConfHostMetrics = {
            RawMetric.AUDITABILITY,
            RawMetric.LOCATION,
            RawMetric.SAS70_COMPLIANCE,
            RawMetric.CCR,
            RawMetric.DATA_CLASSIFICATION,
            RawMetric.HW_REDUNDANCY_LEVEL,
            RawMetric.DISK_THROUGHPUT,
            RawMetric.NET_THROUGHPUT,
            RawMetric.DATA_ENCRYPTION
    };
    private static final Set<RawMetric> clusterConfHostMetricsSet = new HashSet<RawMetric>(Arrays.asList(clusterConfHostMetrics));

    private static RawMetric[] vmLayoutHostMetrics = {
            RawMetric.HOSTNAME,
            RawMetric.AVAILABILITY_STATUS,
            RawMetric.CPU_CORES_COUNT,
            RawMetric.CPU_SPEED,
            RawMetric.MEM_TOTAL,
            // New in Contrail
            RawMetric.MEM_FREE,
            RawMetric.MEM_USAGE,
            RawMetric.CPU_USER,
            RawMetric.CPU_SYSTEM,
            RawMetric.CPU_IDLE,
            RawMetric.LOAD_ONE,
            RawMetric.LOAD_FIVE,
            RawMetric.LOAD_ONE_NORM,
            RawMetric.LOAD_FIVE_NORM,
            RawMetric.SHARED_IMAGES_DISK_FREE,
            RawMetric.SHARED_IMAGES_DISK_USED,
            RawMetric.NET_BYTES_RX,
            RawMetric.NET_BYTES_TX
    };
    private static final Set<RawMetric> vmLayoutHostMetricsSet = new HashSet<RawMetric>(Arrays.asList(vmLayoutHostMetrics));

    private static RawMetric[] vmLayoutVmMetrics = {
            RawMetric.HOSTNAME,
            RawMetric.VM_STATE,
            RawMetric.AVAILABILITY_STATUS,
            RawMetric.CPU_CORES_COUNT,
            RawMetric.CPU_SPEED,
            RawMetric.MEM_TOTAL,
            RawMetric.MEM_HOST,
            RawMetric.VM_IMAGE_TEMPLATE,
            RawMetric.VM_PERSISTENCE,
            RawMetric.ID_OWNER,
            // New in Contrail
            RawMetric.MEM_FREE,
            RawMetric.MEM_USAGE,
            RawMetric.CPU_LOAD,
            RawMetric.CPU_SHARE,
            RawMetric.CPU_SECONDS,
            RawMetric.IP_ADDRESS,
            RawMetric.DISK_FREE,
            RawMetric.DISK_TOTAL,
            RawMetric.NET_BYTES_RX,
            RawMetric.NET_BYTES_TX
    };
    private static final Set<RawMetric> vmLayoutVmMetricsSet = new HashSet<RawMetric>(Arrays.asList(vmLayoutVmMetrics));

    public MetricData getVmMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException {
        if (vmLayoutVmMetricsSet.contains(rawMetric)) {
            return vmLayoutData.getVmData(fqdn).getMetricData(rawMetric);
        }
        else {
            throw new MetricNotSupportedException("Metric " + rawMetric + " is not supported by monitoring.");
        }
    }

    public MetricData getHostMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException {
        if (clusterConfHostMetricsSet.contains(rawMetric)) {
            return clusterConfData.getHostData(fqdn).getMetricData(rawMetric);
        }
        else if (vmLayoutHostMetricsSet.contains(rawMetric)) {
            return vmLayoutData.getHostData(fqdn).getMetricData(rawMetric);
        }
        else {
            throw new MetricNotSupportedException("Metric " + rawMetric + " is not supported by monitoring.");
        }
    }

    public MonitoringData getVmLayoutData() {
        return vmLayoutData;
    }

    public void setVmLayoutData(MonitoringData vmLayoutData) {
        this.vmLayoutData = vmLayoutData;
    }

    public MonitoringData getClusterConfData() {
        return clusterConfData;
    }

    public void setClusterConfData(MonitoringData clusterConfData) {
        this.clusterConfData = clusterConfData;
    }

    public boolean checkVmExists(String fqdn) {
        return vmLayoutData.containsVmData(fqdn);
    }

    public boolean checkHostExists(String fqdn) {
        return vmLayoutData.getHostData(fqdn) != null;
    }

    public List<String> getGuestMachines(String hostFqdn) {
        return vmLayoutData.getHostData(hostFqdn).getVmList();
    }

    public List<String> getAllHostMachines() {
        return vmLayoutData.getHostList();
    }

    public List<String> getAllGuestMachines() {
        return vmLayoutData.getVmList();
    }

    public boolean isVmMonitoringDataAvailable(String fqdn) {
        return vmLayoutData.containsVmData(fqdn);
    }

    public String getVmHostMachine(String fqdn) {
        VmData vmData = vmLayoutData.getVmData(fqdn);
        if (vmData != null) {
            return vmLayoutData.getVmData(fqdn).getHostFqdn();
        }
        else {
            return null;
        }
    }

    public String getInfrastructureLayoutXml() {
        return OneExporter.getInstance().getInfrastructureLayoutXml();
    }

    public String getFqdnFromOneId(String oneId) {
        for (String fqdn : vmLayoutData.getVmList()) {
            if (
                    vmLayoutData.getVmData(fqdn).getOneId() != null
                            && vmLayoutData.getVmData(fqdn).getOneId().equalsIgnoreCase(oneId)
                    )
                return fqdn;
        }
        return null;
    }

    // Returns true if startMonitoring call was issues that has set the mapping OVF id -> ONE id for this fqdn
    public boolean checkVmMonitorable(String fqdn) {
        if (isVmMonitoringDataAvailable(fqdn)) {
            if (
                    OpenNebula.getInstance().getIdMap(fqdn) != null
                            && OpenNebula.getInstance().getIdMap(fqdn).getOvfId() != null
                            && !OpenNebula.getInstance().getIdMap(fqdn).getOvfId().isEmpty()
                    )
                return true;
        }
        return false;
    }
}
