package eu.contrail.infrastructure_monitoring.monitors.scalarix;

import java.util.Date;

import org.apache.log4j.Logger;

import eu.contrail.infrastructure_monitoring.monitors.IMonitoringEngine;
import eu.contrail.infrastructure_monitoring.monitors.IMonitoringEngine.Status;

public class ScalarixMonitoringEngine {
	private static Logger log = Logger.getLogger(ScalarixMonitoringEngine.class);
	Status status;
    Date metricsCollectedDate;
	public static String GANGLIA_HOST;
	public static String GANGLIA_PORT;
    
	public void report() throws Exception {
		log.trace("Collecting Scalarix metrics data...");
        this.status = IMonitoringEngine.Status.BUSY;
        try {
            Scalarix scalarix = new Scalarix();
            ScalarixAmqpSender.getInstance().sendMessages(scalarix.getNodesMetrics());
		}
        catch (Exception e) {
            this.status = IMonitoringEngine.Status.ERROR;
            throw new Exception("Failed to collect Scalarix monitoring data: " + e.getMessage(), e);
        }
        metricsCollectedDate = new Date();
        this.status = IMonitoringEngine.Status.OK;
        log.trace("Scalarix metrics data collected successfully.");
    }
}
