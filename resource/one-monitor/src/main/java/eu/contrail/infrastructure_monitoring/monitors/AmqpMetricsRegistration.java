package eu.contrail.infrastructure_monitoring.monitors;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import eu.contrail.infrastructure_monitoring.monitors.federation.FederationRouting;
import eu.contrail.infrastructure_monitoring.monitors.opennebula.OpenNebula;
import eu.contrail.infrastructure_monitoring.utils.Util;

public class AmqpMetricsRegistration {
	public static final String EXCHANGE_NAME = "metrics.registration";
	public static final String ROUTING_KEY_PREFIX = "metrics.registration";
	
	public static final boolean DURABLE_FLAG = false;
	public static final boolean AUTO_DELETE_FLAG = true;
	public static final boolean INTERNAL_FLAG = false;
	
	private static AmqpMetricsRegistration amqpMetricRegistration = null;
	private static Logger log = Logger.getLogger(AmqpMetricsRegistration.class);
    
	private static Channel channel = null;
	private static boolean sendToHub = false;
		
	private AmqpMetricsRegistration() throws IOException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(OpenNebula.RABBIT_MQ_HOST);
        Connection connection = factory.newConnection();
        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "topic", DURABLE_FLAG, AUTO_DELETE_FLAG, INTERNAL_FLAG, null);
        if(Util.isPropertyTrue(OpenNebula.SEND_TO_MONITORING_HUB))
        	sendToHub = true;
	}	
	
    public static AmqpMetricsRegistration getInstance() {
    	if(amqpMetricRegistration == null)
			try {
				amqpMetricRegistration = new AmqpMetricsRegistration();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	return amqpMetricRegistration;
    }
    
    public void sendMessage(String type, String routingKey, String message) {
        String fullRoutingKey = ROUTING_KEY_PREFIX + "." + type + ".";
        if(routingKey != null && !routingKey.isEmpty()) {
        	fullRoutingKey += routingKey;
        }
    	try {
    		log.trace("Publishing " + message + " to " + fullRoutingKey);
			channel.basicPublish(EXCHANGE_NAME, fullRoutingKey, null, message.getBytes());
			if(sendToHub) {
				log.trace("Sending Finagle request to Federation");
				FederationRouting.getInstance().registerRouteOnFederation(type + "." + routingKey);
	    	}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
