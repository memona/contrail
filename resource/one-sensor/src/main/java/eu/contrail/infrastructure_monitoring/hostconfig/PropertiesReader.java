package eu.contrail.infrastructure_monitoring.hostconfig;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import eu.contrail.infrastructure_monitoring.OneSensor;

public class PropertiesReader implements IConfigReader {
	private static Properties props = new Properties();
	
	public PropertiesReader(String pathToFile) {
		try {
			props.load(new FileInputStream(pathToFile));
		 } catch (IOException e) {
			 e.printStackTrace();
		 }
	}
	
	public PropertiesReader() {
		try {
			props.load(new FileInputStream(OneSensor.getHostPropertiesFile()));
		 } catch (IOException e) {
			 e.printStackTrace();
		 }
	}
	
	@Override
	public String readKey(String key) {
		return props.getProperty(key).trim();
	}

}
