package eu.contrail.infrastructure_monitoring.hostconfig;

public interface IConfigReader {
	public String readKey(String key);
}
