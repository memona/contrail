/**
 * Copyright (c) 2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Reads input from socket, creates XML and publishes it as new auditRecord on RabbitMq fanout exchange (e.g. Pub/Sub)
 *
 * @author Gregor Beslic - gregor.beslic@cloud.si
 */

package eu.contrail.infrastructure_monitoring.opennebula.events;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.Socket;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import eu.contrail.infrastructure_monitoring.OneSensor;

/**
 * Listens on socket for OpenNebula hooks, does a little bit of error handling and publishes the event to RabbitMq queue for furher processing by the monitor
 * @author gregor.beslic@cloud.si
 *
 */
public class EventPublisherThread extends Thread {
	private Socket socket = null;
	private static final String EXCHANGE_NAME_EVENTS = "openNebulaHooks";
	private static Logger log = Logger.getLogger(EventPublisherThread.class);
	
    public EventPublisherThread(Socket socket) {
    	super("EventListenerThread");
    	this.socket = socket;
    }

    public void run() {
		try {
	        InputStream in = socket.getInputStream();
	
	        StringWriter writer = new StringWriter();
	        IOUtils.copy(in, writer, "utf-8");
	        String input = writer.toString();
	        log.trace("Received " + input);
	        String[] msgParts = input.split("_");
	        
	        boolean msgOK = true;

	        if(!msgParts[0].equalsIgnoreCase("audit") && !msgParts[0].equalsIgnoreCase("scheduler")) {
	    		log.warn("Message is not of type AuditRecord or SchedulerEvent");
	    		msgOK = false;
		    }
	    	
	        if(msgOK) {
	        	ConnectionFactory factory = new ConnectionFactory();
		        factory.setHost(OneSensor.getRabbitMqHost());
		        Connection connection = factory.newConnection();
		        Channel channel = connection.createChannel();
	        	channel.exchangeDeclare(EXCHANGE_NAME_EVENTS, "fanout");
	        	channel.basicPublish(EXCHANGE_NAME_EVENTS, "", null, input.getBytes());
	        	channel.close();
		        connection.close();
	        }
	        
	        in.close();
	        socket.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
    }
}
