/* Automatic parsing and setting config options
 * Copyright (C) 2012 Razvan Ghitulete
 * Vrije Universiteit Amsterdam / Universitatea Politehnica Bucuresti
 *
 * Copyright (C) 2006 Andreas Steffen
 * Hochschule fuer Technik Rapperswil, Switzerland
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

#include "styx_parser.h"

#define TOTAL_KEYWORDS 131
#define IPV6_LEN       16
#define IPV4_LEN        4
#define CERT_YES_SEND   3
#define CERT_NO_SEND    4

/* argument types */
static char *err_buf;

typedef enum {
	ARG_NONE,
	ARG_ENUM,
	ARG_UINT,
	ARG_TIME,
	ARG_ULNG,
	ARG_ULLI,
	ARG_PCNT,
	ARG_STR,
	ARG_MISC,
    ARG_PREP
} arg_t;

/* various keyword lists */

static const char *LST_bool[] = {
	"no",
	"yes",
	 NULL
};

static const char *LST_sendcert[] = {
	"always",
	"ifasked",
	"never",
	"yes",
	"no",
	 NULL
};

static const char *LST_dpd_action[] = {
	"none",
	"clear",
	"hold",
	"restart",
	 NULL
};

static const char *LST_keyexchange[] = {
	"ike",
	"ikev1",
	"ikev2",
	 NULL
};

typedef struct {
	arg_t       type;
	const char  **list;
    size_t offset;
} token_info_t;

static const token_info_t token_info[] =
{
	/* config setup keywords */
	{ ARG_NONE  ,   NULL, 0                 },  /* KW_CHARONDEBUG       */
	{ ARG_NONE  ,   NULL, 0                 },  /* KW_UNIQUEIDS    */
	{ ARG_NONE  ,   NULL, 0                 },  /* KW_CACHECRLS   */
	{ ARG_NONE  ,   NULL, 0                 },  /* KW_STRICTCRLPOLICY    */
	{ ARG_NONE  ,   NULL, 0                 },  /* KW_PKCS11_DEPRECATED    */
	{ ARG_NONE  ,   NULL, 0                 },  /* KW_SETUP_DEPRECATED    */

	/* conn section keywords */
	{ ARG_NONE,  NULL,            0 },  /* KW_CONN_NAME     */
	{ ARG_NONE,  NULL,            0 },  /* KW_CONN_SETUP    */
	{ ARG_ENUM,  LST_keyexchange, offsetof(styx_conn_t, version) },  /* KW_KEYEXCHANGE   */
	{ ARG_MISC,  NULL,            0 },  /* KW_TYPE          */
	{ ARG_MISC,  NULL,            0 },  /* KW_COMPRESS      */
	{ ARG_ENUM,  LST_bool, offsetof(styx_conn_t, install_policy) },  /* KW_INSTALLPOLICY */
	{ ARG_ENUM,  LST_bool, offsetof(styx_conn_t, aggresive) },  /* KW_AGGRESSIVE    */
	{ ARG_MISC,  NULL,            0 },  /* KW_AUTH          */
	{ ARG_MISC,  NULL,            0 },  /* KW_AUTHBY        */
	{ ARG_STR,   NULL, offsetof(styx_conn_t, eap_identity) },  /* KW_EAP_IDENTITY  */
	{ ARG_STR,   NULL, offsetof(styx_conn_t, aaa_identity) },  /* KW_AAA_IDENTITY  */
	{ ARG_MISC,  NULL,            0 },  /* KW_MOBIKE        */
	{ ARG_MISC,  NULL,            0 },  /* KW_FORCEENCAPS   */
	{ ARG_TIME,  NULL, offsetof(styx_conn_t, rekey.ike_lifetime) },  /* KW_IKELIFETIME   */
	{ ARG_TIME,  NULL, offsetof(styx_conn_t, rekey.ipsec_lifetime) },  /* KW_KEYLIFE       */
	{ ARG_TIME,  NULL, offsetof(styx_conn_t, rekey.margin) },  /* KW_REKEYMARGIN   */
	{ ARG_ULLI,  NULL, offsetof(styx_conn_t, rekey.life_bytes) },  /* KW_LIFEBYTES     */
	{ ARG_ULLI,  NULL, offsetof(styx_conn_t, rekey.margin_bytes) },  /* KW_MARGINBYTES   */
	{ ARG_ULLI,  NULL, offsetof(styx_conn_t, rekey.life_packets) },  /* KW_LIFEPACKETS   */
	{ ARG_ULLI,  NULL, offsetof(styx_conn_t, rekey.margin_packets) },  /* KW_MARGINPACKETS */
	{ ARG_MISC,  NULL,            0 },  /* KW_KEYINGTRIES   */
	{ ARG_PCNT,  NULL, offsetof(styx_conn_t, rekey.fuzz) },  /* KW_REKEYFUZZ     */
	{ ARG_MISC,  NULL,            0 },  /* KW_REKEY         */
	{ ARG_MISC,  NULL,            0 },  /* KW_REAUTH        */
	{ ARG_STR,   NULL, offsetof(styx_conn_t, algorithms.ike) },  /* KW_IKE           */
	{ ARG_STR,   NULL, offsetof(styx_conn_t, algorithms.esp) },  /* KW_ESP           */
	{ ARG_TIME,  NULL, offsetof(styx_conn_t, dpd.delay) },  /* KW_DPDDELAY      */
	{ ARG_TIME,  NULL, offsetof(styx_conn_t, dpd.timeout) },  /* KW_DPDTIMEOUT    */
	{ ARG_ENUM,  LST_dpd_action, offsetof(styx_conn_t, dpd.action) },  /* KW_DPDACTION     */
	{ ARG_ENUM,  LST_dpd_action, offsetof(styx_conn_t, close_action) },  /* KW_CLOSEACTION   */
	{ ARG_TIME,  NULL, offsetof(styx_conn_t, inactivity) },  /* KW_INACTIVITY    */
	{ ARG_MISC,  NULL,            0 },  /* KW_MODECONFIG    */
	{ ARG_MISC,  NULL,            0 },  /* KW_XAUTH         */
	{ ARG_STR,   NULL, offsetof(styx_conn_t, xauth_identity) },  /* KW_XAUTH_IDENTITY*/
	{ ARG_ENUM,  LST_bool, offsetof(styx_conn_t, ikeme.mediation) },  /* KW_MEDIATION     */
	{ ARG_STR,   NULL, offsetof(styx_conn_t, ikeme.mediated_by) },  /* KW_MEDIATED_BY   */
	{ ARG_STR,   NULL, offsetof(styx_conn_t, ikeme.peerid) },  /* KW_ME_PEERID     */
	{ ARG_UINT,  NULL, offsetof(styx_conn_t, reqid) },  /* KW_REQID         */
	{ ARG_MISC,  NULL,            0 },  /* KW_MARK          */
	{ ARG_MISC,  NULL,            0 },  /* KW_MARK_IN       */
	{ ARG_MISC,  NULL,            0 },  /* KW_MARK_OUT      */
	{ ARG_MISC,  NULL,            0 },  /* KW_TFC           */
	{ ARG_MISC,  NULL,            0 },  /* KW_PFS_DEPRECATED*/
	{ ARG_MISC,  NULL,            0 },  /* KW_CONN_DEPRECATED */

	/* ca section keywords */
	{ ARG_NONE,  NULL, 0          },  /* KW_CA_NAME       */
	{ ARG_NONE,  NULL, 0          },  /* KW_CA_SETUP      */
	{ ARG_NONE,  NULL, 0          },  /* KW_CACERT        */
	{ ARG_NONE,  NULL, 0          },  /* KW_LDAPHOST      */
	{ ARG_NONE,  NULL, 0          },  /* KW_LDAPBASE      */
	{ ARG_NONE,  NULL, 0          },  /* KW_CRLURI        */
	{ ARG_NONE,  NULL, 0          },  /* KW_CRLURI2       */
	{ ARG_NONE,  NULL, 0          },  /* KW_OCSPURI       */
	{ ARG_NONE,  NULL, 0          },  /* KW_OCSPURI2      */
	{ ARG_NONE,  NULL, 0          },  /* KW_CERTURIBASE   */
	{ ARG_MISC,  NULL, 0          },  /* KW_CA_DEPRECATED */

	/* end keywords */
	{ ARG_STR,   NULL, offsetof(stroke_end_t, address) },  /* KW_HOST          */
	{ ARG_UINT,  NULL, offsetof(stroke_end_t, ikeport) },  /* KW_IKEPORT       */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, subnets) },  /* KW_SUBNET        */
	{ ARG_MISC,  NULL, 0                        },  /* KW_PROTOPORT     */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, sourceip) },  /* KW_SOURCEIP      */
	{ ARG_MISC,  NULL, 0                        },  /* KW_NATIP         */
	{ ARG_MISC,  NULL, 0                        },  /* KW_FIREWALL      */
	{ ARG_ENUM,  LST_bool, offsetof(stroke_end_t, hostaccess) },  /* KW_HOSTACCESS    */
	{ ARG_ENUM,  LST_bool, offsetof(stroke_end_t, allow_any) },  /* KW_ALLOWANY      */
	{ ARG_MISC,  NULL, 0                        },  /* KW_UPDOWN        */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, auth) },  /* KW_AUTH1 (left/right auth) */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, auth2) },  /* KW_AUTH2         */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, id) },  /* KW_ID            */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, id2) },  /* KW_ID2           */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, rsakey) },  /* KW_RSASIGKEY     */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, cert) },  /* KW_CERT          */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, cert2) },  /* KW_CERT2         */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, cert_policy) },  /* KW_CERTPOLICY    */
	{ ARG_ENUM,  LST_sendcert, offsetof(stroke_end_t, sendcert) },  /* KW_SENDCERT      */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, ca) },  /* KW_CA            */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, ca2) },  /* KW_CA2           */
	{ ARG_STR,  NULL, offsetof(stroke_end_t, groups) },  /* KW_GROUPS        */
	{ ARG_MISC,  NULL, 0                        }   /* KW_END_DEPRECATED     */
};

static const struct kw_entry wordlist[] =
  {
    {"charondebug",       KW_CHARONDEBUG},
    {"uniqueids",         KW_UNIQUEIDS},
    {"cachecrls",         KW_CACHECRLS},
    {"strictcrlpolicy",   KW_STRICTCRLPOLICY},
    {"keyexchange",       KW_KEYEXCHANGE},
    {"type",              KW_TYPE},
    {"compress",          KW_COMPRESS},
    {"installpolicy",     KW_INSTALLPOLICY},
    {"aggressive",        KW_AGGRESSIVE},
    {"auth",              KW_AUTH},
    {"authby",            KW_AUTHBY},
    {"eap_identity",      KW_EAP_IDENTITY},
    {"aaa_identity",      KW_AAA_IDENTITY},
    {"mobike",	           KW_MOBIKE},
    {"forceencaps",       KW_FORCEENCAPS},
    {"ikelifetime",       KW_IKELIFETIME},
    {"lifetime",          KW_KEYLIFE},
    {"keylife",           KW_KEYLIFE},
    {"rekeymargin",       KW_REKEYMARGIN},
    {"margintime",        KW_REKEYMARGIN},
    {"lifebytes",         KW_LIFEBYTES},
    {"marginbytes",       KW_MARGINBYTES},
    {"lifepackets",       KW_LIFEPACKETS},
    {"marginpackets",     KW_MARGINPACKETS},
    {"keyingtries",       KW_KEYINGTRIES},
    {"rekeyfuzz",         KW_REKEYFUZZ},
    {"rekey",             KW_REKEY},
    {"reauth",            KW_REAUTH},
    {"ike",               KW_IKE},
    {"esp",               KW_ESP},
    {"dpddelay",          KW_DPDDELAY},
    {"dpdtimeout",        KW_DPDTIMEOUT},
    {"dpdaction",         KW_DPDACTION},
    {"closeaction",       KW_CLOSEACTION},
    {"inactivity",        KW_INACTIVITY},
    {"modeconfig",        KW_MODECONFIG},
    {"xauth",             KW_XAUTH},
    {"xauth_identity",    KW_XAUTH_IDENTITY},
    {"mediation",         KW_MEDIATION},
    {"mediated_by",       KW_MEDIATED_BY},
    {"me_peerid",         KW_ME_PEERID},
    {"reqid",             KW_REQID},
    {"mark",              KW_MARK},
    {"mark_in",           KW_MARK_IN},
    {"mark_out",          KW_MARK_OUT},
    {"tfc",               KW_TFC},
    {"cacert",            KW_CACERT},
    {"ldaphost",          KW_LDAPHOST},
    {"ldapbase",          KW_LDAPBASE},
    {"crluri",            KW_CRLURI},
    {"crluri1",           KW_CRLURI},
    {"crluri2",           KW_CRLURI2},
    {"ocspuri",           KW_OCSPURI},
    {"ocspuri1",          KW_OCSPURI},
    {"ocspuri2",          KW_OCSPURI2},
    {"certuribase",       KW_CERTURIBASE},
    {"left",              KW_LEFT},
    {"leftikeport",       KW_LEFTIKEPORT},
    {"leftsubnet",        KW_LEFTSUBNET},
    {"leftsubnetwithin",  KW_LEFTSUBNET},
    {"leftprotoport",     KW_LEFTPROTOPORT},
    {"leftsourceip",      KW_LEFTSOURCEIP},
    {"leftnatip",         KW_LEFTNATIP},
    {"leftfirewall",      KW_LEFTFIREWALL},
    {"lefthostaccess",    KW_LEFTHOSTACCESS},
    {"leftallowany",      KW_LEFTALLOWANY},
    {"leftupdown",        KW_LEFTUPDOWN},
    {"leftauth",          KW_LEFTAUTH},
    {"leftauth2",         KW_LEFTAUTH2},
    {"leftid",            KW_LEFTID},
    {"leftid2",           KW_LEFTID2},
    {"leftrsasigkey",     KW_LEFTRSASIGKEY},
    {"leftcert",          KW_LEFTCERT},
    {"leftcert2",         KW_LEFTCERT2},
    {"leftcertpolicy",    KW_LEFTCERTPOLICY},
    {"leftsendcert",      KW_LEFTSENDCERT},
    {"leftca",            KW_LEFTCA},
    {"leftca2",           KW_LEFTCA2},
    {"leftgroups",        KW_LEFTGROUPS},
    {"right",             KW_RIGHT},
    {"rightikeport",      KW_RIGHTIKEPORT},
    {"rightsubnet",       KW_RIGHTSUBNET},
    {"rightsubnetwithin", KW_RIGHTSUBNET},
    {"rightprotoport",    KW_RIGHTPROTOPORT},
    {"rightsourceip",     KW_RIGHTSOURCEIP},
    {"rightnatip",        KW_RIGHTNATIP},
    {"rightfirewall",     KW_RIGHTFIREWALL},
    {"righthostaccess",   KW_RIGHTHOSTACCESS},
    {"rightallowany",     KW_RIGHTALLOWANY},
    {"rightupdown",       KW_RIGHTUPDOWN},
    {"rightauth",         KW_RIGHTAUTH},
    {"rightauth2",        KW_RIGHTAUTH2},
    {"rightid",           KW_RIGHTID},
    {"rightid2",          KW_RIGHTID2},
    {"rightrsasigkey",    KW_RIGHTRSASIGKEY},
    {"rightcert",         KW_RIGHTCERT},
    {"rightcert2",        KW_RIGHTCERT2},
    {"rightcertpolicy",   KW_RIGHTCERTPOLICY},
    {"rightsendcert",     KW_RIGHTSENDCERT},
    {"rightca",           KW_RIGHTCA},
    {"rightca2",          KW_RIGHTCA2},
    {"rightgroups",       KW_RIGHTGROUPS},
    {"also",              KW_ALSO},
    {"auto",              KW_AUTO},
/* deprecated/removed keywords */
    {"interfaces",        KW_SETUP_DEPRECATED},
    {"dumpdir",           KW_SETUP_DEPRECATED},
    {"charonstart",       KW_SETUP_DEPRECATED},
    {"plutostart",        KW_SETUP_DEPRECATED},
    {"klipsdebug",        KW_SETUP_DEPRECATED},
    {"plutodebug",        KW_SETUP_DEPRECATED},
    {"prepluto",          KW_SETUP_DEPRECATED},
    {"postpluto",         KW_SETUP_DEPRECATED},
    {"plutostderrlog",    KW_SETUP_DEPRECATED},
    {"fragicmp",          KW_SETUP_DEPRECATED},
    {"packetdefault",     KW_SETUP_DEPRECATED},
    {"hidetos",           KW_SETUP_DEPRECATED},
    {"overridemtu",       KW_SETUP_DEPRECATED},
    {"crlcheckinterval",  KW_SETUP_DEPRECATED},
    {"nocrsend",          KW_SETUP_DEPRECATED},
    {"nat_traversal",     KW_SETUP_DEPRECATED},
    {"keep_alive",        KW_SETUP_DEPRECATED},
    {"force_keepalive",   KW_SETUP_DEPRECATED},
    {"virtual_private",   KW_SETUP_DEPRECATED},
    {"pkcs11module",      KW_PKCS11_DEPRECATED},
    {"pkcs11initargs",    KW_PKCS11_DEPRECATED},
    {"pkcs11keepstate",   KW_PKCS11_DEPRECATED},
    {"pkcs11proxy",       KW_PKCS11_DEPRECATED},
    {"pfs",               KW_PFS_DEPRECATED},
    {"pfsgroup",          KW_PFS_DEPRECATED},
    {"eap",               KW_CONN_DEPRECATED},
    {"leftnexthop",       KW_LEFT_DEPRECATED},
    {"rightnexthop",      KW_RIGHT_DEPRECATED},
  };



static void ip_address2string(host_t *host, char *buffer, size_t len)
{
	switch (host->get_family(host))
	{
		case AF_INET6:
		{
			struct sockaddr_in6* sin6 = (struct sockaddr_in6*)host->get_sockaddr(host);
			u_int8_t zeroes[IPV6_LEN];

			memset(zeroes, 0, IPV6_LEN);
			if (memcmp(zeroes, &(sin6->sin6_addr.s6_addr), IPV6_LEN) &&
				inet_ntop(AF_INET6, &sin6->sin6_addr, buffer, len))
			{
				return;
			}
			snprintf(buffer, len, "%%any6");
			break;
		}
		case AF_INET:
		{
			struct sockaddr_in* sin = (struct sockaddr_in*)host->get_sockaddr(host);
			u_int8_t zeroes[IPV4_LEN];

			memset(zeroes, 0, IPV4_LEN);
			if (memcmp(zeroes, &(sin->sin_addr.s_addr), IPV4_LEN) &&
				inet_ntop(AF_INET, &sin->sin_addr, buffer, len))
			{
				return;
			}
			/* fall through to default */
		}
		default:
			snprintf(buffer, len, "%%any");
			break;
	}
}

static int handle_mark(char *value, mark_t *mark)
{
	char *pos, *endptr;

	pos = strchr(value, '/');
	if (pos)
	{
		*pos = '\0';
		mark->mask = strtoul(pos+1, &endptr, 0);
		if (*endptr != '\0')
		{
            return -1;
		}
	}
	else
	{
		mark->mask = 0xffffffff;
	}
	if (*value == '\0')
	{
		mark->value = 0;
	}
	else
	{
		mark->value = strtoul(value, &endptr, 0);
		if (*endptr != '\0')
		{
            return -1;
		}
	}
	return 0;
}


styx_kw_entry_t *parse_entry(const char *token)
{
    styx_kw_entry_t *entry;
    char *key, *value;
    int i;
    key = strdup(token);
    value = strchr(key,'=');
    /* check to see if the string matches the key=value format */
    if (value == NULL)
    {
        free(key);
        return NULL;
    }
    else
    {
        *value = 0;
        value = value + 1;
    }
    entry = malloc_thing(styx_kw_entry_t);
    for (i = 0; i < TOTAL_KEYWORDS; ++i)
    {
        if(streq(key, wordlist[i].name))
        {
            entry->token = wordlist[i].token;
            entry->name = wordlist[i].name;
            entry->value = strdup(value);
            free(key);
            return entry;
        }
    }
    free(key);
    free(entry);
    return NULL;
}

int parse_option(linked_list_t *report, styx_conn_t *conn, const char * token)
{
    FILE *f = fopen("/var/log/porrrrn","a");
    styx_kw_entry_t *option;
    char *buffer;
    int rc = 0;
    err_buf = NULL;
    option = parse_entry(token);
    fprintf(f,"%s %s %d %d\n",option->name, option->value, option->token, KW_KEYINGTRIES);
    fflush(f);
    if (option == NULL)
    {
        buffer = malloc(256 * sizeof(char));
        snprintf(buffer, 255, 
                "Error: Input \'%s\' is not a valid conn option",
                token);
        report->insert_last(report, buffer); 
        return -1;
    }
    else if ((option->token >= KW_LEFT_FIRST)
          && (option->token <= KW_LEFT_LAST))
    {
        DBG2(DBG_CFG, "styx: received %s for left", token);
        option->token -= KW_LEFT_FIRST - KW_END_FIRST;
        rc = load_end(conn, 
                      &conn->me,
                      &conn->me_state,
                      option->token,
                      option);
    }
    else if ((option->token >= KW_RIGHT_FIRST)
          && (option->token <= KW_RIGHT_LAST))
    {
        DBG2(DBG_CFG, "styx: received %s for right", token);
        option->token -= KW_RIGHT_FIRST - KW_END_FIRST,
        rc = load_end(conn, 
                      &conn->other,
                      &conn->other_state,
                      option->token,
                      option);
    }
    else if ((option->token < KW_CONN_FIRST)
          || (option->token > KW_CONN_LAST))
    {
        buffer = malloc(256 * sizeof(char));
        snprintf(buffer, 255, 
                "Error: Input \'%s\' is not a valid conn option",
                token);
        report->insert_last(report, buffer); 
        del_entry(option);
        return -1;
    }
    else if (token_info[option->token].type == ARG_NONE)
    {
        buffer = malloc(256 * sizeof(char));
        snprintf(buffer, 255, 
                "Error: Input \'%s\' is not supported as an option for styx",
                token);
        report->insert_last(report, buffer); 
        del_entry(option);
        return -1;
    }
    else 
    {
        switch (option->token)
        {
            case KW_TYPE:
                if (conn->type_set)
                {
                    rc = -1;
                    err_buf = strdup ("Error: type is already set");
                    break;
                }
                conn->type_set = 1;
                conn->mode = MODE_NONE;
                if (streq(option->value, "tunnel"))
                {
                    conn->mode = MODE_TUNNEL;
                }
                else if (streq(option->value, "beet"))
                {
                    conn->mode = MODE_BEET;
                }
                else if (streq(option->value, "transport_proxy"))
                {
                    conn->mode = MODE_TRANSPORT;
                    conn->proxy_mode = TRUE;
                }
			    else if (streq(option->value, "passthrough") || streq(option->value, "pass"))
			    {
				    conn->mode = MODE_PASS;
			    }
			    else if (streq(option->value, "drop"))
			    {
				    conn->mode = MODE_DROP;
			    }
			    else if (streq(option->value, "reject"))
			    {
				    conn->mode = MODE_DROP;
			    }   
			    else if (strcmp(option->value, "transport") != 0)
			    {
                    rc = -1;
                    break;
			    }
                conn->mode = MODE_TRANSPORT;
			    break;

            case KW_COMPRESS:
                if (streq(option->value, "yes"))
                {
                    conn->ipcomp = TRUE;
                }
                else if (streq(option->value, "no"))
                {
                    conn->ipcomp = FALSE;
                }
                else
                    rc = -1;
                break;

            case KW_AUTH:
                err_buf = strdup ("Error: charon only accepts esp as auth method");
                rc = -1;
                break;

            case KW_AUTHBY:
                if (streq(option->value, "rsa") || streq(option->value, "rsasig") ||
                    streq(option->value, "ecdsa") || streq(option->value, "ecdsasig") ||
                    streq(option->value, "pubkey"))
                {
                    if (conn->me.auth != NULL)
                        free(conn->me.auth);
                    conn->me.auth = strdup("pubkey");
                    if (conn->other.auth != NULL)
                        free(conn->other.auth);
                    conn->other.auth = strdup("pubkey");
                }
                else if(streq(option->value, "secret") || streq(option->value, "psk"))
                {
                    if (conn->me.auth != NULL)
                        free(conn->me.auth);
                    conn->me.auth = strdup("psk");
                    if (conn->other.auth != NULL)
                        free(conn->other.auth);
                    conn->other.auth = strdup("psk");
                }
                else
                {
                    err_buf = strdup ("Error: authby value provided is not supported yet.");
                    rc = -1;
                }
                break;
            
            case KW_MARK:
                rc = handle_mark(option->value, &(conn->mark_in));
                if (rc != 0)
                    break;
                conn->mark_out = conn->mark_in;
                break;

            case KW_MARK_IN:
                rc = handle_mark(option->value, &(conn->mark_in));
                break;

            case KW_MARK_OUT:
                rc = handle_mark(option->value, &(conn->mark_out));
                break;

            case KW_TFC:
                if (streq(option->value, "%mtu"))
                {
                    conn->tfc = -1;
                }
                else
                {
                    char *endptr;
                    conn->tfc = strtoul(option->value, &endptr, 10);
                    if (*endptr != '\0')
                        rc = -1;
                }
                break;

            case KW_KEYINGTRIES:
                if (streq(option->value, "%forever"))
                {
                    conn->rekey.tries = 0;
                }
                else
                {
                   char *endptr;

                   conn->rekey.tries = strtoul(option->value, &endptr, 10);
                   if (*endptr != '\0')
                       rc = -1;
                }
                break;

            case KW_REKEY:
                if (streq(option->value, "yes"))
                {
                    conn->dont_rekey = 0;
                    if (conn->dont_reauth == 0)
                    {
                        conn->rekey.reauth = 1;
                    }
                }
                else if (streq(option->value, "no"))
                {
                    conn->dont_rekey = 1;
                    memset(&(conn->rekey), 0, sizeof(conn->rekey)); 
                }
                else rc = -1;
                break;

            case KW_REAUTH:
                if (streq(option->value, "yes"))
                {
                    conn->dont_reauth = 0;
                    if (conn->dont_rekey == 0)
                        conn->rekey.reauth = 1;
                }
                else if (streq(option->value, "no"))
                {
                    conn->dont_reauth = 1;
                    conn->rekey.reauth = 0;
                }
                else rc = -1;
                break;

            case KW_MOBIKE:
                if (streq(option->value, "yes"))
                {
                    conn->mobike = 1;
                }
                else if (streq(option->value, "no"))
                {
                    conn->mobike = 0;
                }
                else rc = -1;
                break;

            case KW_FORCEENCAPS:
                if (streq(option->value, "yes"))
                {
                    conn->force_encap = 1;
                }
                else if (streq(option->value, "no"))
                {
                    conn->force_encap = 0;
                }
                else rc = -1;
                break;

            case KW_MODECONFIG:
                err_buf = strdup("Error: modeconfig not supported for ikev2");
                rc = -1;
                break;

            case KW_XAUTH:
                err_buf = strdup ("Error: xauth is deprecated as a general"
                                  " parameter for both connections in ikev2"
                                  " use (left/right)auth");
                rc = -1;
                break;

            default:
                rc = parse_direct_value((char*)conn, option);
                break;
        }
    }

    
    if (rc < 0)
    {
        if (err_buf == NULL)
        {
            buffer = malloc(256 * sizeof(char));
            snprintf(buffer, 255, 
                    "Error: Invalid value \'%s\' for option \'%s\'",
                    option->value, option->name);
        }
        else
            buffer = err_buf;
        report->insert_last(report, buffer);
        del_entry(option);
        return -1;
    }
    else if (rc > 0)
    {
        buffer = malloc(256 * sizeof(char));
        snprintf(buffer, 255, 
                "Error: Internal parser error. Check strongswan logs",
                option->value, option->name);
        report->insert_last(report, buffer); 
        del_entry(option);
        return -1;
    }
    del_entry(option);
    return 0;
}

int parse_direct_value(char *base, styx_kw_entry_t *option)
{
    kw_token_t  token = option->token;
    const char **list = token_info[token].list;
    char *endptr, *p = base + token_info[token].offset;
    int index = -1;
    bool match;

    if (list != NULL)
    {
        match = false;

        while (*list != NULL && match == false)
        {
            index++;
            match = streq(option->value, *list++);
        }
        if (match == false)
        {
            return -1;
        }
    }

    switch (token_info[token].type)
    {
        case ARG_NONE:
            DBG1(DBG_CFG, "styx: internal error while parsing \'%s=%s\'",
                 option->name, option->value);
            return 1;

        case ARG_ENUM:
        {
            if (index < 0)
            {
                DBG2(DBG_CFG, "styx: internal parser error, enum not initialized"
                        " for key:\'%s\'", option->name);
                return 1;
            }
            if (token_info[token].list == LST_bool)
            {
                bool *b = (bool *)p;
                *b = (index > 0);
            }
            else
            {
                int *i = (int *)p;
                *i = index;
            }
        }
            break;
        
        case ARG_UINT:
        {
            u_int32_t *u = (u_int32_t *)p;

            *u = strtoul(option->value, &endptr, 10);
            if (*endptr != '\0')
            {
                return -1;
            }
        }
            break;

        case ARG_ULNG:
        case ARG_PCNT:
        {
            unsigned long *u = (unsigned long *)p;

            *u = strtoul(option->value, &endptr, 10);
            if (token_info[token].type == ARG_ULNG)
            {
                if (*endptr != '\0')
                {
                    return -1;
                }
            }
            else
            {
                if ((*endptr != '%') 
                 || (endptr[1] != '\0')
                 || (endptr == option->value))
                {
                    return -1;
                }
            }
        }
            break;
            
        case ARG_ULLI:
        {
            u_int64_t *ull = (u_int64_t *)p;

            *ull = strtoull(option->value, &endptr, 10);
            if (*endptr != '\0')
            {
                return -1;
            }
        }
            break;

        case ARG_TIME:
        {
            time_t *t = (time_t *)p;

            *t = strtoul(option->value, &endptr, 10);
            if (*endptr == '\0')
            {
                break;
            }
            if (endptr[1] == '\0')
            {
                switch(*endptr)
                {
                    case 's':
                        break;
                    case 'm':
                        *t = *t * 60;
                        break;
                    case 'h':
                        *t = *t * 3600;
                        break;
                    case 'd':
                        *t = *t * (3600 * 24);
                        break;
                    default:
                        return -1;
                }
            }
            else
                return -1;
        }
            break;

        case ARG_STR:
        {
            char **cp = (char **)p;
            /* free existing value */
            free(*cp);

            *cp = strdup(option->value);
        }
            break;
        default:
            break;
    }
    return 0;
}

int load_end(styx_conn_t *conn, 
             stroke_end_t *end,
             styx_end_state_t *state,
             kw_token_t index, 
             styx_kw_entry_t *option)
{
    int rc; 
    char *value = option->value, *name = option->name;

    rc = parse_direct_value((char *)end, option);
    if (rc != 0)
        return rc;
    /* additional work on the values added by parse */
    switch (index)
    {
        case KW_HOST:
        {
            host_t *host;
            char buffer[256];
            free(end->address);
            end->address = NULL;
            if (streq(value, "%any") || streq(value, "%any4"))
            {
                host = host_create_any(AF_INET);
            }
            else if (streq(value, "%any6"))
            {
                host = host_create_any(AF_INET6);
            }
            else
            {
                if (value[0] == '%')
                {
                    end->allow_any = true;
                    value++;
                }
                host = host_create_from_dns(value, 0, 0);
                if (host == NULL)
                {
                    DBG2(DBG_CFG, "styx: dns lookup failed, getting "
                                  "ip from value");
                    host = host_create_from_string(value, 0);
                }
                if (host == NULL)
                {
                    if (end->allow_any == true)
                    {
                        host = host_create_any(AF_INET);
                    }
                    else
                    {
                        DBG2(DBG_CFG, "styx: getting the ip from value failed");
                        return -1;
                    }
                }

            }
            ip_address2string(host, buffer, sizeof(buffer));
            end->address = strdup(buffer);
            host->destroy(host);

            return 0;
        }
        case KW_SUBNET:
        {
            char *pos, *copy, *tok_state;
            host_t *host;
            int mask;
            
            copy = strdup(value);
            pos = strtok_r(copy, ",", &tok_state);
            while (pos)
            {
                host = host_create_from_subnet(pos, &mask);
                if (!host)
                {
                    return -1;
                }
                host->destroy(host);
                pos = strtok_r(NULL, ",", &tok_state);
            }

            end->tohost = FALSE;
            return 0;
        }
 
        case KW_SOURCEIP:
        {
            if (state->has_natip) 
            {
                err_buf = malloc (256 * sizeof(char));
                snprintf(err_buf, 255, "Error: natip and sourceip cannot be "
                                      "defined at same time");
                return -1;
            }
            if (value[0] == '%')
            {
                if (streq(value, "%modeconfig") || streq(value, "%modecfg") ||
                    streq(value, "%config") || streq(value, "%cfg"))
                {
                    free(end->sourceip);
                    end->sourceip = NULL;
                    end->sourceip_mask = 1;
                }
                else
                {
                    /* %poolname, strip %, serve ip req */
                    free(end->sourceip);
                    end->sourceip = strdup(value + 1);
                    end->sourceip_mask = 0;
                }
            }
            else
            {
                char *pos;
                host_t *host;

                pos = strchr(value, '/');
                if (pos)
                {/* CIDR Notation, address pool */
                    *pos = 0;
                    host = host_create_from_string(value, 0);
                    if (!host)
                    {
                        char buf[512];
                        snprintf(buf,511,"Error: bad address '%s' in address pool '%s/%s'", value, value, pos+1);
                        err_buf = strdup(buf);
                        return -1;
                    }
                    host->destroy(host);
                    free(end->sourceip);
                    end->sourceip = strdup(value);
                    end->sourceip_mask = atoi(pos + 1);
                }
                else
                {
                    /* fixed srcip */
                    host = host_create_from_string(value,0);
                    if (!host)
                    {
                        err_buf = strdup("Error: bad srcip address");
                        return -1;
                    }
                    end->sourceip_mask = (host->get_family(host) == AF_INET) ?
                                         32 : 128;
                    host->destroy(host);
                }
            }
            conn->mode = MODE_TUNNEL;
            conn->proxy_mode = FALSE;
            return 0;
        }
        case KW_SENDCERT:
            if (end->sendcert == CERT_YES_SEND)
            {
                end->sendcert = CERT_ALWAYS_SEND;
            }
            else if (end->sendcert == CERT_NO_SEND)
            {
                end->sendcert = CERT_NEVER_SEND;
            }
            return 0;

        default:
            break;
   }

    switch (index)
    {
        case KW_PROTOPORT:
        {
            struct protoent *proto;
            struct servent *svc;
            char *pos, *port = "";
            long int p;

            pos = strchr(value,'/');
            if (pos)
            { /* protocol/port */
                *pos = 0;
                port = pos + 1;
            }

            proto = getprotobyname(value);
            if (proto)
            {
                end->protocol = proto->p_proto;
            }
            else
            {
                p = strtol(value, &pos, 0);
                if ((*value && *pos) || p < 0 || p > 0xff)
                {
                    err_buf = calloc(32 + strlen(value) + strlen(name), sizeof(char));
                    snprintf(err_buf, 31 + strlen(value) + strlen(name),
                            "Error: bad protocol %s=%s", name, value);
                    return -1;
                }
                end->protocol = (u_int8_t)p;
            }
		    if (streq(port, "%any"))
		    {
			    end->port = 0;
		    }
		    else
		    {
		        svc = getservbyname(port, NULL);
			    if (svc)
			    {
				    end->port = ntohs(svc->s_port);
			    }
			    else
			    {
				    p = strtol(port, &pos, 0);
				    if ((*port && *pos) || p < 0 || p > 0xffff)
				    {
                        err_buf = calloc(32 + strlen(value) + strlen(name), sizeof(char));
                        snprintf(err_buf, 31 + strlen(value) + strlen(name),
                                "Error: bad port %s=%s", name, value);
                        return -1;
				    }
				    end->port = (u_int16_t)p;
			    }
		    }
            return 0;
        }

        case KW_NATIP:
        {
            host_t *host;
            if (end->sourceip)
            {
                err_buf = strdup ("Error: natip and sourceip cannot be "
                                  "defined at the same time");
                return -1;
            }
            
            host = host_create_from_string(value,0);
            if (!host)
            {
                err_buf = strdup ("Error: bad address for natip");
                return -1;
            }
            host->destroy(host);
            end->sourceip = strdup(value);
            state->has_natip = TRUE;
            conn->mode = MODE_TUNNEL;
            conn->proxy_mode = FALSE;
            return 0;
        }

        case KW_FIREWALL:
           if (streq(value, "yes"))
           {
                if (end->updown != NULL)
                {
                   err_buf = strdup ("Error: cannot have both updown and firewall on "
                                    " the same end, or declare firewall multiple times");
                   return -1;
                }
                else
                {
                    end->updown = strdup("ipsec _updown iptables");
                    return 0;
                }
           }
           if (!streq(value, "no"))
               return -1;
           return 0;

       case KW_UPDOWN:
           if (end->updown != NULL)
           {
              err_buf = strdup ("Error: cannot have both updown and firewall on "
                                " the same end, or declare updown multiple times");
              return -1;         
           }
           end->updown = strdup(value);
           return 0;
       default:
           break;
   }
   return 0;
}

int pre_parse(linked_list_t *report, styx_conn_t *conn, const char *token)
{
    styx_kw_entry_t *option;
    char *buffer;
    int rc = 0;
 
    err_buf = NULL;
    option = parse_entry(token);
    if (option == NULL)
    {
        buffer = malloc(256 * sizeof(char));
        snprintf(buffer, 255, 
                "Error: Input \'%s\' is not a valid conn option",
                token);
        report->insert_last(report, buffer); 
        return -1;
    }
    
    switch (option->token)
    {
        case KW_LEFTALLOWANY:
            buffer = malloc(256 * sizeof(char));
            snprintf(buffer, 255, 
                "Error: %%any is disabled for left end");
            report->insert_last(report, buffer); 
            rc = -1;
            break;
        case KW_RIGHTALLOWANY:
            if (streq(option->value, "yes"))
                conn->other.allow_any = TRUE;
            else if (streq(option->value, "no"))
                conn->other.allow_any = FALSE;
            else
            {
                buffer = malloc(256 * sizeof(char));
                snprintf(buffer, 255, 
                    "Error: invalid value `%s` for key `rightallowany`", option->value);
                report->insert_last(report, buffer); 
                rc = -1;
                break;
            }
         default:
            break;
    }
    del_entry(option);
    return rc;
}

void del_entry(styx_kw_entry_t *entry)
{
    free(entry->value);
    entry->name = NULL;
    entry->value = NULL;
    entry->token = -1;
    free(entry);
    return;
}
