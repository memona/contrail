/*
 * Copyright (C) 2012 Razvan Ghitulete
 * Vrije Universiteit Amsterdam / Universitatea Politehnica Bucuresti
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */


/**
 * @defgroup styx styx
 * @ingroup cplugins
 *
 * @defgroup styx_plugin styx_plugin
 * @{ @ingroup styx
 */

#ifndef STYX_H_
#define STYX_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>

#include <jansson.h>

#include <library.h>
#include <daemon.h>
#include <processing/jobs/callback_job.h>
#include <plugins/plugin.h>
#include <threading/mutex.h>
#include <threading/thread.h>
#include <utils/linked_list.h>
#include <utils/leak_detective.h>
#include <stroke_msg.h>

#include "json_rpc.h"

#define STYX_VERSION "Styx plugin version 1.03"

#define DBGX(group, string) DBG1(group, "(%s : %s -> %d)" string, \
                            __FILE__, __FUNCTION__, __LINE__);
#define STYX_CONN_NAME      0
#define STYX_CONN_LOCAL     1
#define STYX_CONN_REMOTE    2
#define STYX_OPTIONS        3

typedef struct styx_plugin_t styx_plugin_t;

/**
 * STYX configuration and control interface.
 *
 * The STYX interface uses a unix socket to receive rpc calls
 * with the use of the jansson library.
 */
struct styx_plugin_t {

    /**
     * implements the plugin interface.
     */
    plugin_t plugin;
};

/**
 * STYX internal structure to store the flags for parsing
 * during phase 1, for phase 2
 */
typedef struct styx_end_state_t styx_end_state_t;

struct styx_end_state_t {
    bool allow_any; 
    int has_natip;
    int has_sourceip;
};

typedef struct styx_conn_t styx_conn_t;

/**
 *
 * STYX structure for transfering the data about
 * the connectiont to the styx_backend after parsing
 * it from the rpc-call
 */
struct styx_conn_t {
    char *name;

    int version;
    char *eap_identity;
    char *aaa_identity;
    char *xauth_identity;
    int mode;
    int mobike;
    int aggresive;
    int force_encap;
    int ipcomp;
    time_t inactivity;
    int proxy_mode;
    int install_policy;
    int close_action;
    u_int32_t reqid;
    u_int32_t tfc;
    
    crl_policy_t crl_policy;
    int unique;
    struct {
        char *ike;
        char *esp;
    } algorithms;

    struct {
        int reauth;
        time_t ipsec_lifetime;
        time_t ike_lifetime;
        time_t margin;
        u_int64_t life_bytes;
        u_int64_t margin_bytes;
        u_int64_t life_packets;
        u_int64_t margin_packets;
        unsigned long tries;
        unsigned long fuzz;
    } rekey;

    struct {
        time_t delay;
        time_t timeout;
        int action;
    } dpd;

    struct {
        int mediation;
        char *mediated_by;
        char *peerid;
    } ikeme;
    
    struct {
        u_int32_t value;
        u_int32_t mask;
    } mark_in, mark_out;
    stroke_end_t me, other;
    
    styx_end_state_t me_state, other_state;
    int type_set;
    int dont_rekey;
    int dont_reauth;
};

stroke_end_t default_stroke_end(stroke_end_t *end);
styx_conn_t *default_conn();
void free_conn(styx_conn_t *);
int check_addConfig(json_rpc_t *context, json_t *params);
int check_connect(json_rpc_t *context, json_t *params);
int check_terminate(json_rpc_t *context, json_t *params);
int check_remove(json_rpc_t *context, json_t *params);
#endif /** STYX_H_ @}*/
