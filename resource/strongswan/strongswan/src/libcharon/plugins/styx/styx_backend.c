/*
 * Copyright (C) 2012 Razvan Ghitulete
 * Vrije Universiteit Amsterdam / Universitatea Politehnica Bucuresti
 *
 * Copyright (C) 2012 Tobias Brunner
 * Copyright (C) 2008 Martin Willi
 * Hochschule fuer Technik Rapperswil
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */


#include "styx_backend.h"
#include "styx.h"
#include "styx_cred.h"
#include "styx_ca.h"

typedef struct private_styx_config_t private_styx_config_t;

struct private_styx_config_t{
    /**
     * public functions
     */
    styx_config_t public;

    /**
     * list of available runtime configs
     */
    linked_list_t *list;

    /**
     * mutex to protect the acces to the runtime configs
     */
    mutex_t *mutex;

    /**
     * credentials : copied from stroke
     */
    styx_cred_t *cred;

    /**
     * ca : again copied from stroke
     */
    styx_ca_t *ca;

};

static action_t map_action(int starter_action)
{
	switch (starter_action)
	{
		case 2: /* =hold */
			return ACTION_ROUTE;
		case 3: /* =restart */
			return ACTION_RESTART;
		default:
			return ACTION_NONE;
	}
}

METHOD(backend_t, create_peer_cfg_enumerator, enumerator_t *,
        private_styx_config_t *this, identification_t *me, identification_t *other)
{
    this->mutex->lock(this->mutex);
    return enumerator_create_cleaner(this->list->create_enumerator(this->list),
                                        (void*) this->mutex->unlock, this->mutex);
}

/**
 * filter function for ike_configs
 */
static bool ike_cfg_filter(void *data, peer_cfg_t **in, ike_cfg_t **out)
{
    *out = (*in)->get_ike_cfg(*in);
    if (*out != NULL)
        /* Add logger info if this fails...though it shouldn't ever */
        return true;
    return false;
}

METHOD(backend_t, create_ike_cfg_enumerator, enumerator_t *,
        private_styx_config_t *this, host_t *me, host_t *other)
{
    this->mutex->lock(this->mutex);
    return enumerator_create_filter(this->list->create_enumerator(this->list),
                                    (void*)ike_cfg_filter, this->mutex,
                                    (void*)this->mutex->unlock);
}

METHOD(backend_t, get_peer_cfg_by_name, peer_cfg_t *,
        private_styx_config_t *this, char *name)
{   
    peer_cfg_t *toRet = NULL, *peer;
    enumerator_t *e1, *e2;
    child_cfg_t *child;
    this->mutex->lock(this->mutex);
    e1 = this->list->create_enumerator(this->list);
    while(e1->enumerate(e1, &peer))
    {
        /* compare peer_cfg names at first */
        if(streq(peer->get_name(peer), name))
        {
            toRet = peer;
            toRet->get_ref(toRet);
            break;
        }
        /* if not found yet, lookg for child_cfg */
        e2 = peer->create_child_cfg_enumerator(peer);

        while(e2->enumerate(e2, &child))
        {
            if(streq(child->get_name(child), name))
            {
                toRet = peer;
                toRet->get_ref(toRet);
                break;
            }
        }
        e2->destroy(e2);
        if (toRet != NULL)
        {
            break;
        }
    }
    e1->destroy(e1);
    this->mutex->unlock(this->mutex);
    return toRet;
}

static ike_cfg_t *build_ike_cfg(private_styx_config_t *this, 
                                    styx_conn_t *conn)
{
    ike_cfg_t *ike_cfg;
    char *interface, *single, *strict, *string;
    host_t *host;
    proposal_t *proposal;
    styx_config_t *pub = (styx_config_t *)this;
    /* getting a host_t with the first match for the specified domain name */
    host = host_create_from_dns(conn->me.address, 0, 0);
    if (!host)
    {
        pub->error_list->insert_last(this->public.error_list, 
                                     strdup("Error: localhost dns lookup failed"));
        DBG1(DBG_CFG, "localhost dns lookup failed: '%s' not found",conn->me.address);
        return NULL;
    }
    interface = hydra->kernel_interface->get_interface(
                                                hydra->kernel_interface, host);
    host->destroy(host);
    if (!interface)
    {
        char buf[512];
        snprintf(buf, 511, "Error: no interface with address '%s' on localhost", conn->me.address);
        const char *msg = strdup(buf);
        pub->error_list->insert_last(this->public.error_list, msg);
        DBG1(DBG_CFG, "Unknown interface '%s' on localhost", conn->me.address);
        return NULL;
    }
    free (interface);

    ike_cfg = ike_cfg_create(conn->other.sendcert != CERT_NEVER_SEND,
                    conn->force_encap,
                    conn->me.address,
                    conn->me.allow_any,
                    conn->me.ikeport,
                    conn->other.address,
                    conn->other.allow_any,
                    conn->other.ikeport);
    /* adding default proposal if not specified */
    if (!conn->algorithms.ike)
    {
        DBGX(DBG_CFG, "No algorithm was specified, adding default proposal");
        ike_cfg->add_proposal(ike_cfg, proposal_create_default(PROTO_IKE));
        return ike_cfg;
    }

    /* if not check to find the appropriate ike proposal */
    strict = conn->algorithms.ike + strlen(conn->algorithms.ike) - 1;
    if (*strict == '!')
        *strict = '\0';
    else
        strict = NULL;
    string = conn->algorithms.ike;
    while ((single = strsep(&(string), ",")))
    {
        proposal = proposal_create_from_string(PROTO_IKE, single);
        if(proposal)
        {
            ike_cfg->add_proposal(ike_cfg, proposal);
            continue;
        }
        DBG1(DBG_CFG, "skipped invalid proposal string: '%s'", single);
    }
    if (!strict)
    {
        DBGX(DBG_CFG, "added default proposal, due to non-strict requirements");
        ike_cfg->add_proposal(ike_cfg, proposal_create_default(PROTO_IKE));
    }
    return ike_cfg;
}

/**
 * Add CRL constraint to config
 */
static void build_crl_policy(auth_cfg_t *cfg, bool local, int policy)
{
	/* CRL/OCSP policy, for remote config only */
	if (!local)
	{
		switch (policy)
		{
			case CRL_STRICT_YES:
				/* if yes, we require a GOOD validation */
				cfg->add(cfg, AUTH_RULE_CRL_VALIDATION, VALIDATION_GOOD);
				break;
			case CRL_STRICT_IFURI:
				/* for ifuri, a SKIPPED validation is sufficient */
				cfg->add(cfg, AUTH_RULE_CRL_VALIDATION, VALIDATION_SKIPPED);
				break;
			default:
				break;
		}
	}
}



/**
 * build authentication config
 */
static auth_cfg_t *build_auth_cfg(private_styx_config_t *this,
                                  styx_conn_t *conn, bool local, bool primary)
{
    identification_t *identity;
    certificate_t *certificate;
    char *auth, *id, *pubkey, *cert, *ca;
    stroke_end_t *end, *other_end;
    auth_cfg_t *cfg;

    /* select strings */
    if (local)
    {
        end = &conn->me;
        other_end = &conn->other;
    }
    else
    {
        end = &conn->other;
        other_end = &conn->me;
    }
    if (primary)
    {
        auth = end->auth;
        id = end->id;
        if (!id)
        {   /* leftid/rightid fallback to address */
            id = end->address;
        }
        cert = end->cert;
        ca = end->ca;
        if (ca && streq(ca, "%same"))
        {
            ca = other_end->ca;
        }
    }
    else
    {
        auth = end->auth2;
        id = end->id2;
        if (local && !id)
        {   /* leftid2 falls back to leftid */
            id = end->id;
        }
        cert = end->cert2;
        ca = end->ca2;
        if (ca && streq(ca, "%same"))
        {
            ca = other_end->ca2;
        }
    }

    if (!auth)
    {
        if (primary)
        {
            auth = "pubkey";
        }
        else
        {   /* no second authentication round, fine. But load certificates
             * for other purposes (EAP-TLS) */
            if (cert)
            {
                certificate = this->cred->load_peer(this->cred, cert);
                if (certificate)
                {
                    certificate->destroy(certificate);
                }
            }
            return NULL;
        }
    }

    cfg = auth_cfg_create();

    /* add identity and peer certificate */
    identity = identification_create_from_string(id);
    if (cert)
    {
        certificate = this->cred->load_peer(this->cred, cert);
        if (certificate)
        {
            if (local)
            {
                this->ca->check_for_hash_and_url(this->ca, certificate);
            }
            cfg->add(cfg, AUTH_RULE_SUBJECT_CERT, certificate);
            if (identity->get_type(identity) == ID_ANY ||
                !certificate->has_subject(certificate, identity))
            {
                DBG1(DBG_CFG, "  id '%Y' not confirmed by certificate, "
                     "defaulting to '%Y'", identity,
                     certificate->get_subject(certificate));
                identity->destroy(identity);
                identity = certificate->get_subject(certificate);
                identity = identity->clone(identity);
            }
        }
    }
    cfg->add(cfg, AUTH_RULE_IDENTITY, identity);

    /* add raw RSA public key */
    pubkey = end->rsakey;
    if (pubkey && !streq(pubkey, "") && !streq(pubkey, "%cert"))
    {
        certificate = this->cred->load_pubkey(this->cred, KEY_RSA, pubkey,
                                              identity);
        if (certificate)
        {
            cfg->add(cfg, AUTH_RULE_SUBJECT_CERT, certificate);
        }
    }

    /* CA constraint */
    if (ca)
    {
        identity = identification_create_from_string(ca);
        certificate = lib->credmgr->get_cert(lib->credmgr, CERT_X509,
                                             KEY_ANY, identity, TRUE);
        identity->destroy(identity);
        if (certificate)
        {
            cfg->add(cfg, AUTH_RULE_CA_CERT, certificate);
        }
        else
        {
            DBG1(DBG_CFG, "CA certificate \"%s\" not found, discarding CA "
                 "constraint", ca);
        }
    }

    /* groups */
    if (end->groups)
    {
        enumerator_t *enumerator;
        char *group;

        enumerator = enumerator_create_token(end->groups, ",", " ");
        while (enumerator->enumerate(enumerator, &group))
        {
            cfg->add(cfg, AUTH_RULE_GROUP,
                     identification_create_from_string(group));
        }
        enumerator->destroy(enumerator);
    }

    /* certificatePolicies */
    if (end->cert_policy)
    {
        enumerator_t *enumerator;
        char *policy;

        enumerator = enumerator_create_token(end->cert_policy, ",", " ");
        while (enumerator->enumerate(enumerator, &policy))
        {
            cfg->add(cfg, AUTH_RULE_CERT_POLICY, strdup(policy));
        }
        enumerator->destroy(enumerator);
    }

    /* authentication metod (class, actually) */
    if (streq(auth, "pubkey") ||
        strneq(auth, "rsa", strlen("rsa")) ||
        strneq(auth, "ecdsa", strlen("ecdsa")))
    {
        u_int strength;

        cfg->add(cfg, AUTH_RULE_AUTH_CLASS, AUTH_CLASS_PUBKEY);
        build_crl_policy(cfg, local, conn->crl_policy);

        if (sscanf(auth, "rsa-%d", &strength) == 1)
        {
            cfg->add(cfg, AUTH_RULE_RSA_STRENGTH, (uintptr_t)strength);
        }
        if (sscanf(auth, "ecdsa-%d", &strength) == 1)
        {
            cfg->add(cfg, AUTH_RULE_ECDSA_STRENGTH, (uintptr_t)strength);
        }
    }
    else if (streq(auth, "psk") || streq(auth, "secret"))
    {
        cfg->add(cfg, AUTH_RULE_AUTH_CLASS, AUTH_CLASS_PSK);
    }
    else if (strneq(auth, "xauth", 5))
    {
        char *pos;

        pos = strchr(auth, '-');
        if (pos)
        {
            cfg->add(cfg, AUTH_RULE_XAUTH_BACKEND, strdup(++pos));
        }
        cfg->add(cfg, AUTH_RULE_AUTH_CLASS, AUTH_CLASS_XAUTH);
        if (conn->xauth_identity)
        {
            cfg->add(cfg, AUTH_RULE_XAUTH_IDENTITY,
                identification_create_from_string(conn->xauth_identity));
        }
    }
    else if (strneq(auth, "eap", 3))
    {
        enumerator_t *enumerator;
        char *str;
        int i = 0, type = 0, vendor;

        cfg->add(cfg, AUTH_RULE_AUTH_CLASS, AUTH_CLASS_EAP);

        /* parse EAP string, format: eap[-type[-vendor]] */
        enumerator = enumerator_create_token(auth, "-", " ");
        while (enumerator->enumerate(enumerator, &str) && i < 3)
        {
            switch (i)
            {
                case 0:
                    break;
                case 1:
                    type = eap_type_from_string(str);
                    if (!type)
                    {
                        type = atoi(str);
                        if (!type)
                        {
                            DBG1(DBG_CFG, "styx: unknown EAP method: %s", str);
                            return NULL;
                        }
                    }
                    cfg->add(cfg, AUTH_RULE_EAP_TYPE, type);
                    break;
                case 2:
                    vendor = atoi(str);
                    if (vendor)
                    {
                        cfg->add(cfg, AUTH_RULE_EAP_VENDOR, vendor);
                    }
                    else
                    {
                        DBG1(DBG_CFG, "styx: unknown EAP vendor: %s", str);
                        return NULL;
                    }
                    break;
                default:
                    DBG1(DBG_CFG, "styx: EAP format unknown");
                    return NULL;
            }
            i++;
        }
        enumerator->destroy(enumerator);

        if (conn->eap_identity)
        {
            if (streq(conn->eap_identity, "%identity"))
            {
                identity = identification_create_from_encoding(ID_ANY,
                                                    chunk_empty);
            }
            else
            {
                identity = identification_create_from_string(
                                                    conn->eap_identity);
            }
            cfg->add(cfg, AUTH_RULE_EAP_IDENTITY, identity);
        }
        if (conn->aaa_identity)
        {
            cfg->add(cfg, AUTH_RULE_AAA_IDENTITY,
                identification_create_from_string(conn->aaa_identity));
        }
    }
    else
    {
        if (!streq(auth, "any"))
        {
            DBG1(DBG_CFG, "authentication method %s unknown",
                 auth);
            return NULL;
        }
        build_crl_policy(cfg, local, conn->crl_policy);
    }
    return cfg;
}



/**
 * This function should be used to create a new peer cfg
 * using the info got from the json-rpc.
 *
 * @param conn          the styx connection structure, populated with the
 *                      info received from the rpc
 * @param ike_cfg       the previously built ike_cfg tunnel
 * @ret                 the built peer_cfg in case of success, or NULL
 *                      otherwise
 */
static peer_cfg_t *build_peer_cfg(private_styx_config_t *this,
                                    styx_conn_t *conn, ike_cfg_t *ike_cfg)
{
    identification_t *peer_id = NULL;
    peer_cfg_t *mediated_by = NULL;
    host_t *vip = NULL;
    unique_policy_t unique;
    u_int32_t rekey = 0, reauth = 0, over, jitter;
    peer_cfg_t *peer_cfg;
    auth_cfg_t *auth_cfg;

#ifdef ME
    if (conn->ikeme.mediation && conn->ikeme.mediated_by)
    {
        this->public.error_list->insert_last(this->public.error_list, 
                                             strdup("Error: a mediation connection"
                                                    " cannot be a mediated connection"));
        DBG1(DBG_CFG, "A mediation connection cannot be a mediated connection "
                "at the same time, aborting");
        return NULL;
    }

    if (conn->ikeme.mediation)
    {
        /* unique connections should be forced for mediation */
        conn->unique = 1; 
    }

    if (conn->ikeme.mediated_by)
    {
        mediated_by = charon->backends->get_peer_cfg_by_name(charon->backends,
                                            conn->ikeme.mediated_by);
        if (!mediated_by)
        {
            this->public.error_list->insert_last(this->public.error_list, strdup("Error: mediated_by connection"
                                              " not found"));
            DBG1(DBG_CFG, "mediation connection '%s' not found, aborting",
                 conn->ikeme.mediated_by);
            return NULL;
        }
        if (!mediated_by->is_mediation(mediated_by))
        {
            this->public.error_list->insert_last(this->public.error_list, 
                                                 strdup("Error: mediated_by field "
                                                        "does not refer a mediation connection"));
            DBG1(DBG_CFG, "connection '%s' as referred to by '%s' is "
                 "no mediation connection, aborting",
                 conn->ikeme.mediated_by, conn->name);
            mediated_by->destroy(mediated_by);
            return NULL;
        }
        if (conn->ikeme.peerid)
        {
            peer_id = identification_create_from_string(conn->ikeme.peerid);
        }
        else if (conn->other.id)
        {
            peer_id = identification_create_from_string(conn->other.id);
        }
    }
#endif /* ME */
    
    jitter = conn->rekey.margin * conn->rekey.fuzz / 100;
    over = conn->rekey.margin;
    if (conn->rekey.reauth)
    {
        reauth = conn->rekey.ike_lifetime - over;
    }
    else
    {
        rekey = conn->rekey.ike_lifetime - over;
    }
    if (conn->me.sourceip_mask)
    {
        if (conn->me.sourceip)
        {
            vip = host_create_from_string(conn->me.sourceip, 0);
        }
        if (!vip)
        {   /* if it is set to something like %poolname, request an address */
            if (conn->me.subnets)
            {   /* use the same address as in subnet, if any */
                if (strchr(conn->me.subnets, '.'))
                {
                    vip = host_create_any(AF_INET);
                }
                else
                {
                    vip = host_create_any(AF_INET6);
                }
            }
            else
            {
                if (strchr(ike_cfg->get_my_addr(ike_cfg, NULL), ':'))
                {
                    vip = host_create_any(AF_INET6);
                }
                else
                {
                    vip = host_create_any(AF_INET);
                }
            }
        }
    }
    switch (conn->unique)
    {
        case 1: /* yes */
        case 2: /* replace */
            unique = UNIQUE_REPLACE;
            break;
        case 3: /* keep */
            unique = UNIQUE_KEEP;
            break;
        default: /* no */
            unique = UNIQUE_NO;
            break;
    }
    if (conn->dpd.action == 0)
    {   /* dpdaction=none disables DPD */
        conn->dpd.delay = 0;
    }

    /* other.sourceip is managed in stroke_attributes. If it is set, we define
     * the pool name as the connection name, which the attribute provider
     * uses to serve pool addresses. */
    peer_cfg = peer_cfg_create(conn->name,
        2, ike_cfg,
        conn->me.sendcert, unique,
        conn->rekey.tries, rekey, reauth, jitter, over,
        conn->mobike, conn->aggresive,
        conn->dpd.delay, conn->dpd.timeout,
        vip, conn->other.sourceip_mask ?
                            conn->name : conn->other.sourceip,
        conn->ikeme.mediation, mediated_by, peer_id);

    /* adding auth's to peer_cfg */
    /* build leftauth= */
    auth_cfg = build_auth_cfg(this, conn, TRUE, TRUE);
    if (auth_cfg)
    {
        peer_cfg->add_auth_cfg(peer_cfg, auth_cfg, TRUE);
    }
    else
    {   /* we require at least one config on our side */
        this->public.error_list->insert_last(this->public.error_list, strdup("Error: building"
                                              " auth config failed. Check logs for more info"));
		DBG1(DBG_CFG, "styx: at least one config is required on our side");
        peer_cfg->destroy(peer_cfg);
        return NULL;
    }
    /* build leftauth2= */
    auth_cfg = build_auth_cfg(this, conn, TRUE, FALSE);
    if (auth_cfg)
    {
        peer_cfg->add_auth_cfg(peer_cfg, auth_cfg, TRUE);
    }
    /* build rightauth= */
    auth_cfg = build_auth_cfg(this, conn, FALSE, TRUE);
    if (auth_cfg)
    {
        peer_cfg->add_auth_cfg(peer_cfg, auth_cfg, FALSE);
    }
    /* build rightauth2= */
    auth_cfg = build_auth_cfg(this, conn, FALSE, FALSE);
    if (auth_cfg)
    {
        peer_cfg->add_auth_cfg(peer_cfg, auth_cfg, FALSE);
    }
    return peer_cfg;
}

static void add_ts(private_styx_config_t *this,
				   stroke_end_t *end, child_cfg_t *child_cfg, bool local)
{
	traffic_selector_t *ts;

	if (end->tohost)
	{
		ts = traffic_selector_create_dynamic(end->protocol,
					end->port ? end->port : 0, end->port ? end->port : 65535);
		child_cfg->add_traffic_selector(child_cfg, local, ts);
	}
	else
	{
		host_t *net;

		if (!end->subnets)
		{
			net = host_create_from_string(end->address, 0);
			if (net)
			{
				ts = traffic_selector_create_from_subnet(net, 0, end->protocol,
														 end->port);
				child_cfg->add_traffic_selector(child_cfg, local, ts);
			}
		}
		else
		{
			char *del, *start, *bits;

			start = end->subnets;
			do
			{
				int intbits = 0;

				del = strchr(start, ',');
				if (del)
				{
					*del = '\0';
				}
				bits = strchr(start, '/');
				if (bits)
				{
					*bits = '\0';
					intbits = atoi(bits + 1);
				}

				net = host_create_from_string(start, 0);
				if (net)
				{
					ts = traffic_selector_create_from_subnet(net, intbits,
												end->protocol, end->port);
					child_cfg->add_traffic_selector(child_cfg, local, ts);
				}
				else
				{
					DBG1(DBG_CFG, "invalid subnet: %s, skipped", start);
				}
				start = del + 1;
			}
			while (del);
		}
	}
}



static child_cfg_t *build_child_cfg(private_styx_config_t *this,
									styx_conn_t *conn)
{
	child_cfg_t *child_cfg;
	lifetime_cfg_t lifetime = {
		.time = {
			.life = conn->rekey.ipsec_lifetime,
			.rekey = conn->rekey.ipsec_lifetime - conn->rekey.margin,
			.jitter = conn->rekey.margin * conn->rekey.fuzz / 100
		},
		.bytes = {
			.life = conn->rekey.life_bytes,
			.rekey = conn->rekey.life_bytes - conn->rekey.margin_bytes,
			.jitter = conn->rekey.margin_bytes * conn->rekey.fuzz / 100
		},
		.packets = {
			.life = conn->rekey.life_packets,
			.rekey = conn->rekey.life_packets - conn->rekey.margin_packets,
			.jitter = conn->rekey.margin_packets * conn->rekey.fuzz / 100
		}
	};
	mark_t mark_in = {
		.value = conn->mark_in.value,
		.mask = conn->mark_in.mask
	};
	mark_t mark_out = {
		.value = conn->mark_out.value,
		.mask = conn->mark_out.mask
	};

	child_cfg = child_cfg_create(
				conn->name, &lifetime, conn->me.updown,
				conn->me.hostaccess, conn->mode, ACTION_NONE,
				map_action(conn->dpd.action),
				map_action(conn->close_action), conn->ipcomp,
				conn->inactivity, conn->reqid,
				&mark_in, &mark_out, conn->tfc);
	child_cfg->set_mipv6_options(child_cfg, conn->proxy_mode,
											conn->install_policy);
	add_ts(this, &conn->me, child_cfg, TRUE);
	add_ts(this, &conn->other, child_cfg, FALSE);

    if (conn->algorithms.esp)
    {
        char *single, *strict, *string;
        proposal_t *proposal;
        string = conn->algorithms.esp;
        strict = string + strlen(string) - 1;
        if (*strict == '!')
            *strict = 0;
        else
            strict = NULL;

        while ((single = strsep(&string, ",")))
        {
            proposal = proposal_create_from_string(PROTO_ESP, single);
            if (proposal)
            {
                child_cfg->add_proposal(child_cfg, proposal);
                continue;
            }
            this->public.error_list->insert_last(this->public.error_list, strdup("Error: invalid proposal"
                                                " string for child_cfg esp configurations"));
			DBG1(DBG_CFG, "styx: failed on invalid proposal string: %s", single);
        }
        if (!strict)
        {
		    child_cfg->add_proposal(child_cfg, proposal_create_default(PROTO_ESP));
            DBG1(DBG_CFG, "styx: added default proposal due to non strict requirements");
        }
    }

	return child_cfg;
}



METHOD(styx_config_t, add, void,
        private_styx_config_t *this, styx_conn_t *conn)
{
    ike_cfg_t *ike_cfg, *existing_ike;
    peer_cfg_t *peer_cfg, *existing;
    child_cfg_t *child_cfg;
    enumerator_t *enumerator;
    bool use_existing = FALSE;

    ike_cfg = build_ike_cfg(this, conn);
    if (!ike_cfg)
    {
        this->public.error_list->insert_last(this->public.error_list, strdup("Error: internal error"
                                                " while building ike_cfg"));
        DBGX(DBG_CFG, "ike_cfg is NULL");
        return;
    }

    peer_cfg = build_peer_cfg(this, conn, ike_cfg);
    if (!peer_cfg)
    {
        ike_cfg->destroy(ike_cfg);
        return;
    }

	enumerator = create_peer_cfg_enumerator(this, NULL, NULL);
	while (enumerator->enumerate(enumerator, &existing))
	{
		existing_ike = existing->get_ike_cfg(existing);
		if (existing->equals(existing, peer_cfg) &&
			existing_ike->equals(existing_ike, peer_cfg->get_ike_cfg(peer_cfg)))
		{
			use_existing = TRUE;
			peer_cfg->destroy(peer_cfg);
			peer_cfg = existing;
			peer_cfg->get_ref(peer_cfg);
			DBG1(DBG_CFG, "styx: added child to existing configuration '%s'",
				 peer_cfg->get_name(peer_cfg));
			break;
		}
	}
	enumerator->destroy(enumerator);

    child_cfg = build_child_cfg(this, conn);
    if (!child_cfg)
    {
        peer_cfg->destroy(peer_cfg);
        return;
    }
    peer_cfg->add_child_cfg(peer_cfg, child_cfg);

	if (use_existing)
	{
		peer_cfg->destroy(peer_cfg);
	}
	else
	{
		/* add config to backend */
		DBG1(DBG_CFG, "styx: added configuration '%s'", conn->name);
		this->list->insert_last(this->list, peer_cfg);
	}
}

METHOD(styx_config_t, removeConfig, void,
        private_styx_config_t *this, const char *name)
{
    enumerator_t *enumerator;
    peer_cfg_t *cfg;
    const char *cfg_name;
    bool found = FALSE;
    enumerator = this->list->create_enumerator(this->list);

    while(enumerator->enumerate(enumerator, &cfg))
    {
        cfg_name = cfg->get_name(cfg);
        if (streq(cfg_name, name))
        {
            cfg->destroy(cfg);
            this->list->remove_at(this->list, enumerator);
            found = true;
            break;
        }
    }
    enumerator->destroy(enumerator);

    if (found)
		DBG1(DBG_CFG, "styx: removed configuration '%s'", name);
    else
    {
		DBG1(DBG_CFG, "styx: configuration not found'%s'", name);
        this->public.error_list->insert_last(this->public.error_list, strdup("Error: Connection name"
                                                " not found"));
    }
}

METHOD(styx_config_t, destroy, void,
       private_styx_config_t *this)
{
    lib->credmgr->remove_set(lib->credmgr, &this->cred->set);
    lib->credmgr->remove_set(lib->credmgr, &this->ca->set);
    this->cred->destroy(this->cred);
    this->ca->destroy(this->ca);
    free(this);
}

styx_config_t *styx_config_create(linked_list_t *list,
                                  linked_list_t *error_list,
                                  mutex_t *mutex)
{
    private_styx_config_t *this;
    styx_cred_t *cred;
    styx_ca_t *ca;

    cred = styx_cred_create();
    ca = styx_ca_create(cred);

    INIT(this,
         .public = {
            .backend = {
                .create_peer_cfg_enumerator = _create_peer_cfg_enumerator,
                .create_ike_cfg_enumerator = _create_ike_cfg_enumerator,
                .get_peer_cfg_by_name = _get_peer_cfg_by_name,
            },
            .add = _add,
            .removeConfig = _removeConfig,
            .destroy = _destroy,
            .error_list = error_list,
         },
         .list = list,
         .mutex = mutex,
         .cred = cred,
         .ca = ca,
    );

    lib->credmgr->add_set(lib->credmgr, &this->cred->set);
    lib->credmgr->add_set(lib->credmgr, &this->ca->set);

    return &this->public;
}

