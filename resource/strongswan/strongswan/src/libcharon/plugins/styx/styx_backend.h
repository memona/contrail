/*
 * Copyright (C) 2012 Razvan Ghitulete
 * Vrije Universiteit Amsterdam / Universitatea Politehnica Bucuresti
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

/**
 * @defgroup styx_backend styx_backend
 * @{ @ingroup styx
 */
#ifndef STYX_BACKEND_H_
#define STYX_BACKEND_H_

#include <threading/mutex.h>
#include <utils/linked_list.h>
#include <utils/enumerator.h>
#include <config/backend.h>
#include <hydra.h>
#include "styx.h"

           
typedef struct styx_config_t styx_config_t;

struct styx_config_t {
	/**
	 * Implementing backend_t interface
	 */
	backend_t backend;
	
	/**
	 * Add a runtime configuration to the backend
	 *
	 */
	void (*add)(styx_config_t *this, styx_conn_t *conn);

	/**
	 * Remove a runtime configuration to the backend
	 *
	 */
	void (*removeConfig)(styx_config_t *this, const char *name);


	/**
	 * Destroy the styx_config_t instance
	 */
	void (*destroy)(styx_config_t *this);

    /**
     * list of errors encountered while trying to add a config
     */
    linked_list_t *error_list;
};

/**
 * Create a styx_config backend instance
 *
 * @param list			the list where the configurations are stored
 * @param mutex			the mutex to avoid concurrency on the list
 * @param error_list    the list where to append errors
 * @return				desired styx_config_t instance
 */
styx_config_t *styx_config_create(linked_list_t *list, linked_list_t *error_list, mutex_t *mutex);

#endif /** STYX_BACKEND_H_ @}*/   
