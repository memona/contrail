package org.ow2.contrail.common.headNodeRestProxy.monitoring;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class ProxyXmlRpcClient extends XmlRpcClient {
	
	private static final String MONITORING_XMLRPC_IP = "localhost";
	private static final String MONITORING_XMLRPC_PORT = "8081";
	
	private static XmlRpcClient proxyClient;
	
	public static synchronized XmlRpcClient getInstance() {
		if(proxyClient == null) {
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			try {
				config.setServerURL(new URL("http://" + MONITORING_XMLRPC_IP + ":" + MONITORING_XMLRPC_PORT));
				proxyClient = new XmlRpcClient();
			    proxyClient.setConfig(config);
			} catch (MalformedURLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return proxyClient;
	}
}
