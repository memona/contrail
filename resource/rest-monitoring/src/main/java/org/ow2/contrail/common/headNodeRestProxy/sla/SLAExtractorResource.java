package org.ow2.contrail.common.headNodeRestProxy.sla;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.ow2.contrail.monitoring.slaextractor.model.MonitoringConfiguration;
import org.ow2.contrail.monitoring.slaextractor.slasoiy2.SlaExtractor;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/slaextractor")
public class SLAExtractorResource {

    @Context
    HttpServletRequest request;

    @POST
    @Path("/createMonConf")
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    public Response createMonConf(String slaDocument) {
        try {
            SlaExtractor slaExtractor = new SlaExtractor(slaDocument);
            MonitoringConfiguration monConf = slaExtractor.extractQoSConstraints();
            return Response.ok(monConf.toXml()).build();
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity(String.format("Failed to generate monitoring configuration: %s",
                                    e.getMessage())).
                            build());
        }
    }

    @POST
    @Path("/getOVFs")
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOVFs(String slaDocument) {
        try {
            SlaExtractor slaExtractor = new SlaExtractor(slaDocument);
            List<String> ovfURLs = slaExtractor.getOVFs();
            JsonArray json = new JsonArray();
            for (String url : ovfURLs) {
                json.add(new JsonPrimitive(url));
            }
            return Response.ok(json.toString()).build();
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity(String.format("Failed to extract OVFs: %s",
                                    e.getMessage())).
                            build());
        }
    }
}
