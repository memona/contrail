package org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources;

public class VmNetwork {
	private String networkName;
	private String ipAddress;
	
	public VmNetwork(String networkName) {
		this.setNetworkName(networkName);
	}

	public String getNetworkName() {
		return networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
}
