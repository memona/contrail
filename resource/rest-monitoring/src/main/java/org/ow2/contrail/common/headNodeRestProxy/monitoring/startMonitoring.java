package org.ow2.contrail.common.headNodeRestProxy.monitoring;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("/startMonitoring")
public class startMonitoring {
	
	// Maximum time for VEP to return the response (iaas_id of the provisioned VM)
	private static final int VEP_TIMEOUT_SECONDS = 60;
	// Retry interval between HTTP requests to VEP
	private static final int VEP_RETRY_SECONDS = 5;
	private static final String VEP_HOST = "localhost";
    private static final String VEP_PROTO = "http";
    private static final int VEP_PORT = 10500;
	
    private static Logger log = Logger.getLogger(startMonitoring.class);
    
//	private static final String VEP_SAMPLE_RESPONSE = "{" +
//			"\"vnc_ip\": \"localhost\"," +
//			"\"id\": 1," +
//			"\"title\": \"Virtual Machine Detail\"," +
//			"\"host_id\": 1," +
//			"\"name\": \"Ubuntu HTTP Server\"," +
//			"\"state\": \"RN\"," +
//			"\"iaas_id\": 2372," +
//			"\"template_id\": 1," +
//			"\"user_id\": 2," +
//			"\"controller\": \"OpenNebula\"," +
//			"\"vnc_port\": \"5988\"," +
//			"\"ip\": \"192.168.0.100\"" +
//		"}";
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String Test()  {
		return "it works";
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response startMonitoringRequest(String jsonData) throws InterruptedException {
		if(jsonData.isEmpty())
			return Response.status(400).build();
		
		final JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject)parser.parse(jsonData);

		String oneId = null;
		final String ovfId = obj.get("ovfId").getAsString();
		final String vepId = obj.get("vepId").getAsString();
		
		if(
			//oneId == null || oneId.isEmpty() ||
			ovfId == null || ovfId.isEmpty()	
		) {
			return Response.status(400).build();
		}
		
		ExecutorService executor = Executors.newFixedThreadPool(1);
		FutureTask<String> future = new FutureTask<String>(
                new Callable<String>() {
                    public String call() {
                    	log.trace("Async call received");
                    	String oneId = null;
                    	try {
                    		URIBuilder builder = new URIBuilder();
//                        	builder.setScheme(VEP_PROTO).setHost(VEP_HOST).setPort(VEP_PORT).setPath("/ovf/" + vepId);
                        	builder.setScheme(VEP_PROTO).setHost(VEP_HOST).setPort(VEP_PORT).setPath("/vm/" + vepId);
							URI uri = builder.build();
							HttpGet httpget = new HttpGet(uri);
							httpget.setHeader("X-Username", "gregorb");
							httpget.setHeader("Accept", "application/json");
							httpget.setHeader("Content-type", "application/xml");
	                    	DefaultHttpClient httpclient = new DefaultHttpClient();
	                    	StringWriter writer = new StringWriter();
	                    	HttpResponse response;
	                    	int i = 0;
	                    	long startTime = System.currentTimeMillis();
	                    	while(true) {
	                    		if(System.currentTimeMillis() - startTime > VEP_TIMEOUT_SECONDS * 1000)
	                    			break;
	                    		
	                    		try {
	                    			String vepData = "";
//	                    			if(i++ > 5) {
//	                    				vepData = VEP_SAMPLE_RESPONSE;
//	                    			}
//	                    			else {
	                    				try {
											response = httpclient.execute(httpget);
											if(response.getStatusLine().getStatusCode() == 200) {
			    	                    		HttpEntity entity = response.getEntity();
				    	                    	InputStream is = entity.getContent();
				    	                    	IOUtils.copy(is, writer, "UTF-8");
				    	                    	vepData = writer.toString();
			    	                    	}	
										} catch (ClientProtocolException e) {
											log.error(e.getLocalizedMessage());
										} catch (IOException e) {
											log.error(e.getLocalizedMessage());
										}
//	                    			}
	                    			
	    	                    	if(!vepData.isEmpty()) {
	    	                    		log.trace(vepData);
	    	                    		JsonObject vepObj = (JsonObject)parser.parse(vepData);
	    	                    		if(!vepObj.get("iaas_id").getAsString().isEmpty()) {
	    	                    			oneId = vepObj.get("iaas_id").getAsString();
	    	                    			break;
	    	                    		}
	    	                    	}
	    	                    	// Try again a bit later
	    	                    	Thread.sleep(VEP_RETRY_SECONDS * 1000);
									log.trace("VEP thread is running");
	                    		} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
	                    	}
	                    	
						} catch (URISyntaxException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							log.error(e1.getLocalizedMessage());
						}
                	
                    	return oneId;
                    }
                });
        executor.execute(future);

        while (!future.isDone()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                log.trace("Will check after 1/2 sec.");
            }
        }
    	try {
    		oneId = future.get();
        } catch (ExecutionException e) {
        	log.error(e.getCause());
        }
        executor.shutdown();
        log.trace("Executor shutdown");

        if(oneId == null)
        	return Response.status(500).build();

        log.trace("OneId was found: " + oneId);
        
        // Call one-monitor to return the RabbitMq topic
        XmlRpcClient client = ProxyXmlRpcClient.getInstance();
		if(client == null)
			return Response.status(500).build();
		
		String res = null;
		try {
			res = (String) client.execute(
					"monitoring.startMonitoring",
					new Object[]{oneId, ovfId, vepId}
					);
		} catch (XmlRpcException ex) {
			return Response.status(500).entity(ex.getLocalizedMessage()).build();
		}
		if(!res.startsWith("ok;"))
			return Response.status(400).entity(res).build();
		
		String[] split = res.split(";");
		
		return Response.status(200).entity(split[1]).build();
	}
}
