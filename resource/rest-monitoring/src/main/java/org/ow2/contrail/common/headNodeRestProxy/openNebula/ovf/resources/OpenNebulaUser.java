package org.ow2.contrail.common.headNodeRestProxy.openNebula.ovf.resources;

public class OpenNebulaUser {
	private String username;
	private String password;
	
	public OpenNebulaUser(String username, String password) {
		this.setUsername(username);
		this.setPassword(password);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
