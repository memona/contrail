package org.ow2.contrail.common.headNodeRestProxy.monitoring;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("/stopMonitoring")
public class stopMonitoring {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String Test()  {
		return "it works";
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response stopMonitoringRequest(String jsonData)  {
		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject) parser.parse(jsonData);
		String ovfId = null;
		if(obj.has("ovfId")) {
			ovfId = obj.get("ovfId").getAsString();
		}
		else
			return Response.status(400).build();
		
		XmlRpcClient client = ProxyXmlRpcClient.getInstance();
		if(client == null)
			return Response.status(500).build();
		
		String res = "";
		try {
			res = (String) client.execute(
					"monitoring.stopMonitoringOvf",
					new Object[]{ovfId}
					);
		} catch (XmlRpcException e) {
			Response.status(500).entity(e.getLocalizedMessage()).build();
		}
		if(!res.startsWith("ok;"))
			return Response.status(400).entity(res).build();

		String[] split = res.split(";");
		return Response.status(200).entity(split[1]).build();
	}
}