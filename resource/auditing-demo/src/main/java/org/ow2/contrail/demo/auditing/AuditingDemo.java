package org.ow2.contrail.demo.auditing;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONObject;
import org.ow2.contrail.common.oauth.client.AccessToken;
import org.ow2.contrail.common.oauth.client.CCFlowClient;
import org.ow2.contrail.common.oauth.client.OAuthHttpClient;
import org.ow2.contrail.common.oauth.client.utils.UriUtils;
import org.ow2.contrail.demo.auditing.utils.Conf;
import org.ow2.contrail.demo.auditing.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Date;

public class AuditingDemo {
    private static Logger log = LoggerFactory.getLogger(Conf.class);

    private static final int TIME_TOLERANCE = 30;

    public static void main(String[] args) throws Exception {
        if (args.length != 2 ||
                !args[0].equals("--config")) {
            System.out.println("Usage: AuditingDemo --config <file>");
            System.exit(1);
        }

        String confFile = args[1];

        Conf.load(confFile);
        new AuditingDemo().run();
    }

    public void run() throws Exception {
        Date startTime = new Date();
        AccessToken accessToken = getAccessToken();
        getAccountingReport(accessToken);
        getOAuthTokenAccessLog(accessToken);
        Date endTime = new Date();
        log.info("Waiting some time for audit events...");
        Thread.sleep(10000);
        getAuditEvents(
                new Date(startTime.getTime() - TIME_TOLERANCE * 1000),
                new Date(endTime.getTime() + TIME_TOLERANCE * 1000),
                accessToken);
    }

    private AccessToken getAccessToken() throws Exception {
        log.info("Requesting OAuth access token from the Authorization Server token endpoint {} " +
                "on behalf of the user {}",
                Conf.getOAuthASTokenEndpoint(), Conf.getUserUUID());
        CCFlowClient ccFlowClient = new CCFlowClient(Conf.getOAuthASTokenEndpoint(),
                Conf.getClientKeystoreFile(), Conf.getClientKeystorePass(),
                Conf.getClientTruststoreFile(), Conf.getClientTruststorePass());
        ccFlowClient.setClientId(Conf.getClientID());
        ccFlowClient.setClientSecret(Conf.getClientSecret());

        AccessToken accessToken = ccFlowClient.requestAccessToken(Conf.getUserUUID(), null);
        log.info("Received access token: {}", accessToken.toJson());
        return accessToken;
    }

    private void getAccountingReport(AccessToken accessToken) throws Exception {
        URI uri = Conf.getFederationApiUri().resolve(
                String.format("providers/%s/accounting/historical_metrics_data", Conf.getProviderUUID()));

        log.info("Requesting accounting data from the federation-api ({}) using access token {}",
                uri, accessToken.getValue());

        OAuthHttpClient oAuthHttpClient = new OAuthHttpClient(
                Conf.getClientKeystoreFile(),
                Conf.getClientKeystorePass(),
                Conf.getClientTruststoreFile(),
                Conf.getClientTruststorePass()
        );

        HttpEntity entity = new StringEntity(Conf.getAccountingRequestData(), ContentType.APPLICATION_JSON);
        HttpResponse response = oAuthHttpClient.post(uri, accessToken.getValue(), entity);
        log.info("Received response: " + response.getStatusLine());
        if (response.getStatusLine().getStatusCode() != 201) {
            throw new Exception("Invalid response received from the federation-accounting: "
                    + response.getStatusLine());
        }
        URI reportUri = new URI(response.getFirstHeader("Location").getValue());
        log.info("Report URI: " + reportUri);

        JSONObject reportStatus;
        String jobStatus;
        Date t0 = new Date();
        do {
            log.info("Requesting report status...");
            HttpResponse statusResponse = oAuthHttpClient.get(reportUri, accessToken.getValue());
            log.info("Received response: " + statusResponse.getStatusLine());
            if (statusResponse.getStatusLine().getStatusCode() != 200) {
                throw new Exception("Invalid response received from the federation-accounting: "
                        + statusResponse.getStatusLine());
            }
            String statusContent = oAuthHttpClient.getContent(statusResponse);
            reportStatus = new JSONObject(statusContent);
            log.info("Report status:\n{}", reportStatus.toString(2));
            jobStatus = reportStatus.getString("jobStatus");
            if (jobStatus.equals("RUNNING")) {
                log.info("Waiting for the report to be finished...");
                Thread.sleep(1000);
            }
            else {
                break;
            }
        } while (new Date().getTime() - t0.getTime() < 10000);

        if (!jobStatus.equals("SUCCESS")) {
            throw new Exception("Invalid report status: " + jobStatus);
        }
        else {
            log.info("Report was generated successfully.");
        }

        log.info("Requesting report content...");
        URI reportContentUri = new URI(reportStatus.getString("reportUri"));
        HttpResponse contentResponse = oAuthHttpClient.get(reportContentUri, accessToken.getValue());
        log.info("Received response: " + contentResponse.getStatusLine());
        if (contentResponse.getStatusLine().getStatusCode() != 200) {
            throw new Exception("Invalid response received from the federation-accounting: "
                    + contentResponse.getStatusLine());
        }
        String content = oAuthHttpClient.getContent(contentResponse);
        JSONObject contentJson = new JSONObject(content);
        log.info("Accounting report:\n{}", contentJson.toString(2));
    }

    private void getOAuthTokenAccessLog(AccessToken accessToken) throws Exception {
        OAuthHttpClient oAuthHttpClient = new OAuthHttpClient(
                Conf.getClientKeystoreFile(),
                Conf.getClientKeystorePass(),
                Conf.getClientTruststoreFile(),
                Conf.getClientTruststorePass()
        );

        URI tokenUri = UriUtils.append(
                Conf.getOAuthASAdminUri(),
                String.format("/access_tokens/%s", accessToken.getValue()));

        log.info("Requesting access token {} info from the OAuth Authorization Server {}",
                accessToken.getValue(), tokenUri);

        HttpResponse tokenResponse = oAuthHttpClient.get(tokenUri, accessToken.getValue());
        log.info("Received response: " + tokenResponse.getStatusLine());
        if (tokenResponse.getStatusLine().getStatusCode() != 200) {
            throw new Exception("Invalid response received from the OAuth Authorization Server: "
                    + tokenResponse.getStatusLine());
        }
        String tokenInfo = oAuthHttpClient.getContent(tokenResponse);
        JSONObject tokenInfoJson = new JSONObject(tokenInfo);
        log.info("Access token {} info:\n{}", accessToken.getValue(), tokenInfoJson.toString(2));

        URI accessLogUri = UriUtils.append(
                Conf.getOAuthASAdminUri(), tokenInfoJson.getString("access_log")
        );
        log.info("Requesting access log for the access token {} from the OAuth Authorization Server ({})",
                accessToken.getValue(), accessLogUri);

        HttpResponse accessLogResponse = oAuthHttpClient.get(accessLogUri, accessToken.getValue());
        log.info("Received response: " + accessLogResponse.getStatusLine());
        if (accessLogResponse.getStatusLine().getStatusCode() != 200) {
            throw new Exception("Invalid response received from the OAuth Authorization Server: "
                    + accessLogResponse.getStatusLine());
        }
        String accessLog = oAuthHttpClient.getContent(accessLogResponse);
        JSONArray accessLogJson = new JSONArray(accessLog);
        log.info("Access log for the access token {}:\n{}", accessToken.getValue(),
                accessLogJson.toString(2));
    }

    private void getAuditEvents(Date startTime, Date endTime, AccessToken accessToken) throws Exception {
        OAuthHttpClient oAuthHttpClient = new OAuthHttpClient(
                Conf.getClientKeystoreFile(),
                Conf.getClientKeystorePass(),
                Conf.getClientTruststoreFile(),
                Conf.getClientTruststorePass()
        );

        URI targetUri = UriUtils.append(Conf.getAuditManagerUri(), "/audit_events");

        log.info("Requesting audit events from the audit-manager ({}) using access token {}.",
                targetUri, accessToken.getValue());

        JSONObject requestData = new JSONObject();
        requestData.put("searchCriteria", new JSONObject());
        requestData.put("startTime", DateUtils.format(startTime));
        requestData.put("endTime", DateUtils.format(endTime));
        HttpEntity entity = new StringEntity(requestData.toString(), ContentType.APPLICATION_JSON);
        HttpResponse postResponse = oAuthHttpClient.post(targetUri, accessToken.getValue(), entity);
        log.info("Received response: " + postResponse.getStatusLine());
        if (postResponse.getStatusLine().getStatusCode() != 201) {
            throw new Exception("Invalid response received from the audit-manager: "
                    + postResponse.getStatusLine());
        }

        URI reportUri = new URI(postResponse.getFirstHeader("Location").getValue());
        log.info("Report URI: " + reportUri);

        JSONObject reportStatus;
        String jobStatus;
        Date t0 = new Date();
        do {
            log.info("Requesting report status...");
            HttpResponse statusResponse = oAuthHttpClient.get(reportUri, accessToken.getValue());
            log.info("Received response: " + statusResponse.getStatusLine());
            if (statusResponse.getStatusLine().getStatusCode() != 200) {
                throw new Exception("Invalid response received from the audit-manager: "
                        + statusResponse.getStatusLine());
            }
            String statusString = oAuthHttpClient.getContent(statusResponse);
            reportStatus = new JSONObject(statusString);
            log.info("Report status:\n{}", reportStatus.toString(2));
            jobStatus = reportStatus.getString("jobStatus");
            if (jobStatus.equals("RUNNING")) {
                log.info("Waiting for the report to be finished...");
                Thread.sleep(1000);
            }
            else {
                break;
            }
        } while (new Date().getTime() - t0.getTime() < 10000);

        if (!jobStatus.equals("SUCCESS")) {
            throw new Exception("Invalid report status: " + jobStatus);
        }
        else {
            log.info("Audit events report was generated successfully.");
        }

        log.info("Requesting report content...");
        URI reportContentUri = UriUtils.append(
                Conf.getAuditManagerUri(), reportStatus.getString("reportUri")
        );
        HttpResponse contentResponse = oAuthHttpClient.get(reportContentUri, accessToken.getValue());
        log.info("Received response: " + contentResponse.getStatusLine());
        if (contentResponse.getStatusLine().getStatusCode() != 200) {
            throw new Exception("Invalid response received from the audit-manager: "
                    + contentResponse.getStatusLine());
        }
        String content = oAuthHttpClient.getContent(contentResponse);
        JSONArray contentJson = new JSONArray(content);
        if (contentJson.length() > 0) {
            log.info("Audit events report:\n{}", contentJson.toString(2));
        }
        else {
            log.error("No audit events were found.");
        }
    }
}
