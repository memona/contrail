XtreemFS-SSL configuration:

You need to :

- install the latest GAFS/XtreemFS packages from http://download.opensuse.org/repositories/home:/xtreemfs:/unstable/

- create a jks-container which contains the trusted CA-certificate(s): trusted.jks

- a certificate for the GAFS-manager "pathToCert/cert.p12"

- certificates for the GAFS components: MRC, OSD, DIR (those can be the same certificates):  dir.p12, mrc.p12, osd.p12



Configuration of the GAFS services for grid-ssl mode is described in:
http://www.xtreemfs.org/xtfs-guide-1.4/index.html#sec:cfg_ssl

The short version: 

Configure the GAFS-MRC:

Edit the configuration of the MRC. 

vi /etc/xos/xtreemfs/mrconfig.properties
authentication_provider = org.xtreemfs.common.auth.FederationIdX509AuthProvider

# specify whether SSL is required
ssl.grid_ssl = true 
ssl.enabled = true

# server credentials for SSL handshakes
ssl.service_creds = /etc/xos/xtreemfs/truststore/certs/dir.p12
ssl.service_creds.pw = passphrase
ssl.service_creds.container = pkcs12

# trusted certificates for SSL handshakes
ssl.trusted_certs = /etc/xos/xtreemfs/truststore/certs/trusted.jks
ssl.trusted_certs.pw = kem9Ey7p
ssl.trusted_certs.container = jks

# administrator password for privileged operations
admin_password = a_passphrase



Configure the GAFS-OSD:

Edit the configuration of the OSD.

vi /etc/xos/xtreemfs/osdconfig.properties

# specify whether SSL is required
ssl.grid_ssl = true 
ssl.enabled = true

# server credentials for SSL handshakes
ssl.service_creds = /etc/xos/xtreemfs/truststore/certs/dir.p12
ssl.service_creds.pw = passphrase
ssl.service_creds.container = pkcs12

# trusted certificates for SSL handshakes
ssl.trusted_certs = /etc/xos/xtreemfs/truststore/certs/trusted.jks
ssl.trusted_certs.pw = kem9Ey7p
ssl.trusted_certs.container = jks

# administrator password for privileged operations
admin_password = a_passphrase



Configure the GAFS-DIR:

Edit the configuration of the DIR.

vi /etc/xos/xtreemfs/dirconfig.properties

# specify whether SSL is required
ssl.grid_ssl = true 
ssl.enabled = true

# server credentials for SSL handshakes
ssl.service_creds = /etc/xos/xtreemfs/truststore/certs/dir.p12
ssl.service_creds.pw = passphrase
ssl.service_creds.container = pkcs12

# trusted certificates for SSL handshakes
ssl.trusted_certs = /etc/xos/xtreemfs/truststore/certs/trusted.jks
ssl.trusted_certs.pw = kem9Ey7p
ssl.trusted_certs.container = jks

# administrator password for privileged operations
admin_password = a_passphrase



GAFS-manager configuration:

The GAFS-manager has to be configured for the use of ssl.

edit webapp/WEB-INF/default_dir

# administrator password for privileged operations
# set this to the same value as in the mrcconfig.properties
admin_password = a_passphrase

# specify whether SSL is required
ssl.enabled = true

# server credentials for SSL handshakes
ssl.service_creds = pathToCert/cert.p12
ssl.service_creds.pw = passphrase
ssl.service_creds.container = pkcs12





The fuse client commands:

The syntax slightly changes for the fuse client when using ssl. See http://www.xtreemfs.org/xtfs-guide-1.4/index.html#sec:cfg_ssl for further details

Short version:

mkfs.xtreemfs --admin_password a_passphrase --pkcs12-file-path user.p12 --pkcs12-passphrase pass pbrpcg://localhost:32636/contrail


mount.xtreemfs --pkcs12-file-path user.p12 --pkcs12-passphrase pass pbrpcg://localhost:32638/test2 /mnt/xtreemfs/


lsfs.xtreemfs --admin_password passphrase --pkcs12-file-path user.p12 --pkcs12-passphrase pass pbrpcg://localhost:32636

