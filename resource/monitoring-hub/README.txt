Monitoring HUB
==============

Running
-------

Dependencies
++++++++++++

- Redis for DB (http://redis.io/)
- Kestrel for message queue (https://github.com/robey/kestrel)
- RabbitMQ (optional)

Package
+++++++

$ mvn package

Run
+++

$ cd path/to/monitoring-hub
$ ./run

Development
-----------

Dependencies
++++++++++++

- SBT (https://github.com/harrah/xsbt/wiki)

Develop
+++++++

Update libraries.

$ sbt update

Generate Eclipse project files.

$ sbt eclipse

Run.

$ sbt run # default config (in code)
or
$ sbt -Dhub.config=config.json run


Generate POM
++++++++++++

$ extra/make-pom

Provider API
------------

Register
++++++++

PUT /offers/route/{providerRoute} (headers: X-Provider-ID: {providerId})

Example:

$ curl -X PUT -H "X-Provider-ID: 42" http://localhost:8342/offers/route/host.n0013-xc1-xlab-lan.disk
...
{"id":"hub:offer:42:host.n0013-xc1-xlab-lan.disk","provId":42,"provRoute":"host.n0013-xc1-xlab-lan.disk","federRoute":"providerid.42.host.n0013-xc1-xlab-lan.disk","sub":false,"dt":1331891485441}

(*) If does not exist, adds in DB and waits for Federation then returns JSON
(*) If exists and not subscribed, waits for Federation then returns JSON
(*) If exists and subscribed returns JSON

Send metrics
++++++++++++

PUT /metrics/route/{providerRoute} (headers: X-Provider-ID: {providerId})

Example:

$ curl -X PUT -H "X-Provider-ID: 42" http://localhost:8342/metrics/route/host.n0013-xc1-xlab-lan.disk -d "</metric>"

Federation API
--------------

List of unsubscribed offers
+++++++++++++++++++++++++++

GET /offers/unsubscribed

Example:

$ curl -X GET http://localhost:8342/offers/unsubscribed
[{"id":"hub:offer:42:host.n0013-xc1-xlab-lan.disk","provId":42,"provRoute":"host.n0013-xc1-xlab-lan.disk","federRoute":"providerid.42.host.n0013-xc1-xlab-lan.disk","sub":false,"dt":1331891485441}]

Details of an offer
+++++++++++++++++++

GET /offers/get/{offerId}

Example:

$ curl http://localhost:8342/offers/get/hub:offer:42:host.n0013-xc1-xlab-lan.disk
{"id":"hub:offer:42:host.n0013-xc1-xlab-lan.disk","provId":42,"provRoute":"host.n0013-xc1-xlab-lan.disk","federRoute":"providerid.42.host.n0013-xc1-xlab-lan.disk","sub":true,"dt":1331891485441}

Subscribe to offer
++++++++++++++++++

GET /offers/subscribe/{offerId}

Example:

$ curl -X PUT http://localhost:8342/offers/subscribe/hub:offer:42:host.n0013-xc1-xlab-lan.disk

Get single metric
+++++++++++++++++

GET /metrics/{federRoute}

Note: This call will block until there is a metric available in queue.

Example:

curl -X GET http://localhost:8342/metrics/providerid.42.host.n0013-xc1-xlab-lan.disk

Get metrics stream
++++++++++++++++++

GET /metrics/{federRoute}

Note: This API endpoint is not on same TPC/IP port.

Example:

curl -N -X GET http://localhost:8343/metrics/providerid.42.host.n0013-xc1-xlab-lan.disk/stream
