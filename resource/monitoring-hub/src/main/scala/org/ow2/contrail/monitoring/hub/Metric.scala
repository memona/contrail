package org.ow2.contrail.monitoring.hub

import java.util.Date
import org.slf4j.LoggerFactory
import org.jboss.netty.buffer.ChannelBuffers.copiedBuffer
import org.jboss.netty.util.CharsetUtil.UTF_8
import com.twitter.util.{Future, JavaTimer}
import com.twitter.conversions.time._
import com.twitter.finagle.service.Backoff
import com.codahale.jerkson.Json._
import org.jboss.netty.buffer.ChannelBuffer

object Metric {
  private val log = LoggerFactory.getLogger(getClass)
  
  def apply(route: String) = new Metric(route)
  
  def kestrelEscapeKey(key: String) = key.replaceAll("\\.", "\\#")
  
  def metricId(route: String) = Offer.kestrelEscapeKey(route)
}

class Metric(route: String) {
  val id = Metric.metricId(route)
  
  def add(content: ChannelBuffer) {
    Hub.kestrel.clientForWriting set(id, content)
    Hub.rabbit ! Message(route, content.toString(UTF_8))
  }
  
  def readHandle = {
    val timer = new JavaTimer(isDaemon = true)
    val retryBackoffs = Backoff.const(10.milliseconds)
    Hub.kestrel.clientForReading.readReliably(id, timer, retryBackoffs)
  }
  
  def getSingle: Future[String] = {
    readHandle.messages() flatMap { msg =>
      try {
        Future.value(msg.bytes.toString(UTF_8))
      } finally {
        msg.ack()
        readHandle.close
      }
    }
  }
}
