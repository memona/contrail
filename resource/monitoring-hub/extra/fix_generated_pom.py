#!/usr/bin/env python

import sys

if __name__ == '__main__':
    pom = sys.argv[1]
    
    with open(pom) as f:
        content = f.read()
    
    content = content.replace('    ', '  ')
    
    lines = content.splitlines()
    
    lines[0:2] = [
        '<?xml version="1.0" encoding="UTF-8"?>',
        '<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">'
    ]
    
    lines.append('')
    
    with open(pom, 'w') as f:
        f.write('\n'.join(lines))
