/* File: error.h $ */

#ifndef _INC_ERROR_H
#define _INC_ERROR_H

#include <stdio.h>
#include "tmdefs.h"
#include "origin.h"


extern void internal_error( const char *msg, ... ) ATTRIBUTE_PRINTF(1,2);
extern void origin_internal_error( const origin *org, const char *msg, ... ) ATTRIBUTE_PRINTF(2,3);

extern void origin_fatal_error( const origin *org, const char *msg, ... ) ATTRIBUTE_PRINTF(2,3);
extern void fatal_error( const char *msg, ... ) ATTRIBUTE_PRINTF(1,2);
extern void error( const char *msg, ... ) ATTRIBUTE_PRINTF(1,2);
extern void fileline_error( const char *fnm, unsigned int lineno, const char *msg, ... ) ATTRIBUTE_PRINTF(3, 4);
extern void origin_error( const origin *s, const char *msg, ... ) ATTRIBUTE_PRINTF(2,3);
extern void sys_error( int no, const char *msg, ... ) ATTRIBUTE_PRINTF(2,3);
extern void origin_sys_error( const origin *org, int no, const char *msg, ... );

extern void errcheck( void );
extern bool error_seen( void );

#endif
