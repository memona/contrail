/* File: global.h */

#ifndef _INC_GLOBAL_H
#define _INC_GLOBAL_H

#include <stdio.h>

extern bool vartr;
extern bool noerrorline;
extern bool fntracing;
extern bool sevaltr;
extern bool dryrun;
extern bool debug;
extern bool showrun;
extern bool hide_regex_error;

extern FILE *tracestream;

#endif
