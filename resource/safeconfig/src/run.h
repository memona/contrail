// File: run.h

#ifndef _INC_RUN_H
#define _INC_RUN_H

#include "line.h"

extern void run_script(const line *script, const word *argv);

#endif
