/* File var.h
 *
 * Extern routines for variable management.
 */

#ifndef _INC_VAR_H
#define _INC_VAR_H

extern void set_var_string( const char *nm, const char *v );
extern const char *getvar( const char *nm );
extern void init_var( void );
extern void end_var( void );

#endif
