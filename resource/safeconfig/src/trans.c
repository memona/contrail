/* File: trans.c
 *
 * Translate input file into output file given datastructure
 * description.
 */

/* Standard UNIX libraries */
#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <string.h>

/* local definitions */
#include "tmdefs.h"
#include "origin.h"
#include "sgstring.h"
#include "expr.h"
#include "error.h"
#include "global.h"
#include "util.h"
#include "trans.h"
#include "var.h"

/******************************************************
 *    Input of template lines                         *
 ******************************************************/

/* Given a pointer to an input string 'spi', a pointer to an output string
   'spo', and a stop character 'sc', copy and evaluate the string to the
   stop character 'sc'. Update the position of '*spi' to point to the stop
   character or '\0', and return the evaluated string.
 */
static char *alevalto( const origin *org, const char **spi, const int sc )
{
    char var1[2];		/* buffer for 1 char variable */
    char *fnval;
    char *ans;
    size_t len;

    const char *si = *spi;
    char *cp = new_string( si );    // pointer to constructed string
    size_t croom = (unsigned int) strlen( si );	// room in cp
    size_t six = 0;		        // index in cp
    if( sevaltr ){
        if( sc == '\0' ){
            fprintf( tracestream, "alevalto: '%s'\n", si );
        }
        else {
            fprintf( tracestream, "alevalto: '%s' to char '%c'\n", si, sc );
        }
    }
    while( *si != '\0' && *si != sc ){
        /* make sure that 1 character will always fit */
        if( six>=croom ){
            croom += STRSTEP;
            cp = realloc_string( cp, croom+1 );
        }
        if( *si != VARCHAR ){
            cp[six++] = *si++;
            continue;
        }
        si++;
        if( *si == ORBRAC ){
            si++;
            *spi = si;
            ans = alevalto( org, spi, CRBRAC );
            si = *spi;
            if( *si != CRBRAC ){
                origin_error( org, "missing close bracket '%c'", CRBRAC );
                fre_string( ans );
                continue;
            }
            si++;
            const char * const v = getvar( ans );
            if( v == stringNIL ){
                origin_error( org, "variable '%s' not found", ans );
                fre_string( ans );
                continue;
            }
            fre_string( ans );
            len = six + (unsigned int) strlen( v ) + (unsigned int) strlen( si );
            if( len > croom ){
                croom = len;
                cp = realloc_string( cp, croom+1 );
            }
            const char *p = v;
            while( *p!='\0' ) cp[six++] = *p++;
            continue;
        }
        if( *si == OSBRAC ){
            si++;
            *spi = si;
            ans = alevalto( org, spi, CSBRAC );
            si = *spi;
            if( *si != CSBRAC ){
                origin_error( org, "missing close bracket '%c'", CSBRAC );
                fre_string( ans );
                continue;
            }
            si++;
            fnval = evalexpr( org, ans );
            fre_string( ans );
            if( fnval == stringNIL ){
                continue;
            }
            len = six + (unsigned int) strlen( fnval ) + (unsigned int) strlen( si );
            if( len > croom ){
                croom = len;
                cp = realloc_string( cp, croom+1 );
            }
            {
                const char *v = fnval;
                while( *v!='\0' ){
                    cp[six++] = *v++;
                }
            }
            fre_string( fnval );
            continue;
        }
        if( *si == '\0' ){
            cp[six++] = VARCHAR;
            continue;
        }
        if( !isalnum( *si ) ){
            cp[six++] = *si++;
            continue;
        }
        var1[0] = *si++;
        var1[1] = '\0';
        {
            const char *const v = getvar( var1 );
            if( v == stringNIL ){
                origin_error( org, "variable '%s' not found", var1 );
                continue;
            }
            const char *p = v;
            unsigned int len = six + (unsigned int) strlen( v ) + (unsigned int) strlen( si );
            if( len > croom ){
                croom = len;
                cp = realloc_string( cp, croom+1 );
            }
            while( *p!='\0' ){
                cp[six++] = *p++;
            }
        }
    }
    cp[six]='\0';
    *spi = si;
    if( sevaltr ){
        fprintf( tracestream, "value is: '%s'\n", cp );
    }
    return cp;
}

char *evaluate_string(const origin *org, const char *p)
{
    return alevalto(org, &p, '\0');
}

