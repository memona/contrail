/* File: printarg.h
 *
 * Print an argument for use in a script.
 */

extern void print_arg(const origin *org, FILE *f, const char *p);
extern void print_unquoted_arg(const origin *org, FILE *f, const char *p);
