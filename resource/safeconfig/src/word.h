/*
 * word.h
 *
 *  Created on: Mar 13, 2012
 *      Author: Kees van Reeuwijk
 */

#ifndef WORD_H_
#define WORD_H_

#include <stdio.h>
#include "word.h"

struct str_word {
	struct str_word *next;
	char *string;
};

typedef struct str_word word;

#define wordNIL ((word *) 0)

extern word *new_word(const char *s);
extern void rfre_word_list(word *w);
extern word *rdup_word_list(const word *w);
extern void print_word_list(FILE *f, const word *v);

#endif /* LINE_H_ */
