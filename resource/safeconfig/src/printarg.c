/* File: printarg.c
 *
 * Translate input file into output file given datastructure
 * description.
 */

/* Standard UNIX libraries */
#include <stdbool.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <string.h>

/* local definitions */
#include "tmdefs.h"
#include "origin.h"
#include "sgstring.h"
#include "expr.h"
#include "error.h"
#include "global.h"
#include "util.h"
#include "trans.h"
#include "var.h"

/******************************************************
 *    Input of template lines                         *
 ******************************************************/

/* Given a pointer to an input string 'spi', a pointer to an output string
   'spo', and a stop character 'sc', copy and evaluate the string to the
   stop character 'sc'. Update the position of '*spi' to point to the stop
   character or '\0', and return the evaluated string.
 */
static void printto(FILE *f, const origin *org, const char **spi, const int sc )
{
    const char *si = *spi;
    if( sevaltr ){
        if( sc == '\0' ){
            fprintf( tracestream, "printto: '%s'\n", si );
        }
        else {
            fprintf( tracestream, "printto: '%s' to char '%c'\n", si, sc );
        }
    }
    while( *si != '\0' && *si != sc ){
        /* make sure that 1 character will always fit */
        if( *si != VARCHAR ){
            int c = *si++;
            fputc(c,f);
            continue;
        }
        si++;
        if( *si == ORBRAC ){
            si++;
            *spi = si;
            fputc(VARCHAR,f);
            fputc('{',f);
            printto(f, org, spi, CRBRAC );
            si = *spi;
            if( *si != CRBRAC ){
                origin_error( org, "missing close bracket '%c'", CRBRAC );
                continue;
            }
            fputc('}',f);
            si++;
            continue;
        }
        if( *si == OSBRAC ){
            origin_fatal_error(org, "No square brackets allowed for script generation");
        }
        if( *si == '\0' ){
            fputc(VARCHAR,f);
            continue;
        }
        if( !isalnum( *si ) ){
            fputc(*si++,f);
            continue;
        }
        fputc(VARCHAR,f);
        fputc(*si++,f);
    }
    *spi = si;
}

void print_unquoted_arg(const origin *org, FILE *f, const char *p)
{
    printto(f, org, &p, '\0');
}

static bool needs_quoting(const char *arg){
    const char *p = arg;
    if(*arg == '\0'){
        // An empty string needs quoting.
        return true;
    }
    while(*p != '\0'){
        int c = *p++;
        if(c == '$'||!isalnum(c)){
            return true;
        }
    }
    return false;
}

void print_arg(const origin *org, FILE *f, const char *p)
{
    if(needs_quoting(p)){
        fputc('"',f);
        printto(f, org, &p, '\0');
        fputc('"',f);
    }
    else {
        printto(f, org, &p, '\0');
    }
}

