/*
 * parseconfig.h
 *
 *  Created on: Mar 13, 2012
 *      Author: Kees van Reeuwijk
 */

#ifndef PARSECONFIG_H_
#define PARSECONFIG_H_

#include "line.h"

extern line *parseConfiguration(const char *config, const char *fnm);

#endif /* PARSECONFIG_H_ */
