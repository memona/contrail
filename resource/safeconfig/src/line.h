/*
 * line.h
 *
 *  Created on: Mar 13, 2012
 *      Author: Kees van Reeuwijk
 */

#ifndef LINE_H_
#define LINE_H_

#include <stdio.h>
#include "origin.h"
#include "word.h"

enum Command {
	AssertMatch,
        AssertFile,
	ErrorPrint,
	DebugPrint,
	Print,
        SetParameters,
	Run,
	DebugRun,
	Set,
        Load,
	Empty
};

struct str_line {
	struct str_line *next;
    enum Command command;
    origin *org;
    word *words;
};

typedef struct str_line line;

#define lineNIL ((struct str_line *) 0)

extern line *new_line(enum Command cmd, const origin *org, const word *params);
extern void rfre_line(line *l);
extern void rfre_line_list(line *l);

#endif /* LINE_H_ */
