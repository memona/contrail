// File: translate.c

#include <stdbool.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <regex.h>
#include <sys/wait.h>

#include "translate.h"
#include "line.h"
#include "error.h"
#include "word.h"
#include "trans.h"
#include "sgstring.h"
#include "var.h"
#include "util.h"
#include "global.h"
#include "printarg.h"
#include "fileproperty.h"

#define ERRBUFSIZE 4000

static void generate_todo_command(const origin *org, const char *command, word *params, FILE *f)
{
    fputs("# TODO: implement ",f);
    fputs(command, f);
    for(word *p=params; p!=wordNIL; p=p->next){
        putc(' ',f);
        print_unquoted_arg(org, f,p->string);
    }
    putc('\n',f);
}

static void translate_print_command(const origin *org, word *params, FILE *f)
{
    fputs("echo",f);
    for(word *p=params; p!=wordNIL; p=p->next){
        putc(' ',f);
        print_arg(org, f,p->string);
    }
    putc('\n',f);
}

static void translate_command(const origin *org, word *params, FILE *f)
{
    bool first = true;
    for(word *p=params; p!=wordNIL; p=p->next){
        if(first){
            first = false;
            print_unquoted_arg(org,f,p->string);
        } else {
            putc(' ',f);
            print_arg(org,f,p->string);
        }
    }
    putc('\n',f);
}

static void translate_set(const origin *org, word *params, FILE *f)
{
    bool first = true;
    if(params == wordNIL){
        origin_fatal_error(org,"No variable to set");
        return;
    }
    fputs(params->string,f);
    fputs("=\"",f);
    for(word *p=params->next; p!=wordNIL; p=p->next){
        if(first){
            first = false;
        } else {
            putc(' ',f);
        }
        print_unquoted_arg(org,f,p->string);
    }
    fputs("\"\n",f);
}

static void translate_load(const origin *org, word *params, FILE *f)
{
    if(params == wordNIL){
        origin_fatal_error(org,"No variable to load into");
    }
    if(params->next == wordNIL){
        origin_fatal_error(org, "No file to load specified");
    }
    if(params->next->next != wordNIL){
        origin_fatal_error(org, "Superfluous parameters for load command");
    }
    fprintf(f,"%s=`cat %s`\n",params->string,params->next->string);
}

static void translate_assert_match_command(const origin *org, word *params, FILE *f)
{
    if(params == wordNIL){
        origin_fatal_error(org, "No pattern specified");
    }
    const char *rawpat = params->string;
    if(params->next == wordNIL){
        origin_fatal_error(org, "No test string specified");
    }
    const char *testString = params->next->string;
    if(params->next->next != wordNIL){
        origin_fatal_error(org, "Superfluous parameters for load command");
    }
    fputs("echo ",f);
    print_arg(org,f,testString);
    fputs(" | grep -E -q '^",f);
    print_unquoted_arg(org,f,rawpat);
    fputs("$' || die \"Failed assertion\"\n",f);
}

static void translate_assert_file_command(const origin *org, word *params, FILE *f)
{
    generate_todo_command(org,"assertfile",params, f);
}

static int count_argv(const word * const argv)
{
    int n = 0;
    const word *arg = argv;
    while(arg != wordNIL){
        n++;
        arg = arg->next;
    }
    return n;
}

static void translate_set_parameters_command(const origin *org, word *params, FILE *f)
{
    int i = 1;
    word *arg = params;
    int n_param = count_argv(params);
    fprintf(f,"[ \"$#\" -eq %d ] || die \"%d parameter%s required, but $# provided: [$@]\"\n", n_param, n_param, n_param==1?"":"s");
    while(arg != wordNIL){
        print_unquoted_arg(org,f,arg->string);
        fprintf(f,"=\"$%d\"\n", i);
        arg = arg->next;
        i++;
    }
}

static void translate_script_line(const line *l, FILE *f)
{
    switch(l->command){
    case AssertMatch:
        translate_assert_match_command(l->org, l->words,f);
        break;

    case AssertFile:
        translate_assert_file_command(l->org, l->words, f);
        break;

    case ErrorPrint:
        translate_print_command(l->org,l->words, f);
        break;

    case Print:
        translate_print_command(l->org,l->words, f);
        break;

    case DebugPrint:
        if(debug){
            translate_print_command(l->org,l->words, f);
        }
        break;

    case SetParameters:
        translate_set_parameters_command(l->org, l->words, f);
        fputc('\n',f);
        break;

    case Run:
        translate_command(l->org, l->words,f);
        break;

    case DebugRun:
        if(debug){
            translate_command(l->org, l->words,f);
        }
        break;

    case Set:
        translate_set(l->org, l->words,f);
        break;

    case Load:
        translate_load(l->org, l->words,f);
        break;

    case Empty:
        // Nothing to do.
        break;
    }
}

void translate_script(const line *script, const char *fnm)
{
    const line *p = script;

    FILE *f = ckfopen(fnm,"w");
    fputs("#!/bin/sh\n\n",f);
    fputs("set -u -e\n",f);
    fputs("die() {\n",f);
    fputs("    echo>& 2 \"$@\"\n",f);
    fputs("    exit 1\n",f);
    fputs("}\n\n",f);
    while(p != lineNIL){
        translate_script_line(p, f);
        p = p->next;
    }
    fclose(f);
}
