function getCEEList()
{
    userid = $("#userid-hidden").val();
    username = $("#username-hidden").val();
    
    $.ajax({url : "/api/cimi/user/"+userid+"/cees",
    type : "GET",
    headers : {"Content-Type" : "application/json",
    "X-Username" : username},
    async: false,
    success: function( data )
    {
        $(data.CEEs).each(function(index,element)
        {
            splitResult = element.href.split("/");
            ceeID = splitResult[splitResult.length-1];
            curElem = "<li>";
            curElem += "<a href='#' onclick='goToEditCEE("+ceeID+");return false;'>";
            curElem += element.name;
            curElem += "</a></li>";
            $("#cee-list").append(curElem); 
        });
    },
    error : function( jqXHR, status, error)
    {
        alert("Cannot get the data from the server");
    }
    });
}


function getCert(){
    $("#getCert").submit();
}









$(document).ready(function() {
     getCEEList();
});



