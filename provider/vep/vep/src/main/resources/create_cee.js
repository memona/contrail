/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


//Basic path, must be set to "" after uploading to the server
var basicPath = "";
var vslist;
var constraintList;
var countrySelect;

function generateVMHandlerCheckbox(name, href)
{
    hrefEscaped = href.replace(new RegExp("/",'g'),"_");
    checkboxID = "vmhandler" + hrefEscaped + "_checkbox";
    checkbox = "<input id=\"" + checkboxID + "\" type=\"checkbox\" data-href=\"" + href + "\" name=\"" + name + "\" />";
    labelID = "vmhandler" + hrefEscaped + "_label";
    label = "<label id=\"" + labelID + "\" for=\"" + checkboxID + "\">" + name + "</label>";
    result = "<tr><td style=\"width: 0px\">" + checkbox +"</td><td>" + label + "</td><td style=\"width: 0px\">" + generateVMHandlerInfobox(href) + "</td></tr>";
    return result;
}

function vmHandlerInfoSplash(minDisk, maxDisk, minFreq, maxFreq, minRAM, maxRAM, minCores, maxCores, name, href)
{
    html = "<div style='margin : 10px;'>";
    html += "<h1>"+name+"</h1>";
    html += "<table>";
    html += "<tr><td>href</td><td>" + href + "</td></tr>";
    html += "<tr><td>Disk size</td><td>" + minDisk + "</td></tr>";
    html += "<tr><td>CPU frequency</td><td>" + minFreq + " - " + maxFreq + "</td></tr>";
    html += "<tr><td>RAM size</td><td>" + minRAM + " - " + maxRAM + "</td></tr>";
    html += "<tr><td>Core count</td><td>" + minCores + " - " + maxCores + "</td></tr>";
    html += "</table>";
    html += "</div>";
    $.colorbox({html:html, opacity: 0.5});
}

function generateVMHandlerInfobox(href)
{
    a = "<a href=\"#\" onclick=\"vmHandlerInfoSplash(";
    $.ajax({url : href,
        type : "GET",
        headers : {"Content-Type" : "application/json",
                    "X-Username" : "semen"},
        async:false,
        success: function( data )
        {
            a += "'" + data.disksize_low + "',";
            a += "'" + data.disksize_high + "',";
            a += "'" + data.cpufreq_low + "',";
            a += "'" + data.cpufreq_high + "',";
            a += "'" + data.ram_low + "',";
            a += "'" + data.ram_high + "',";
            a += "'" + data.corecount_low + "',";
            a += "'" + data.corecount_high + "',";
            a += "'" + data.name + "',";
            a += "'" + data.id + "'";
        },
        error : function( jqXHR, status, error)
        {
            alert(status);
            alert(error);
        }
    });
    
    
    a += ");return false;\">";
    a +=  "<img src=\"http://myriads-serv2.irisa.fr:8000/images/info.png\" alt=\"info\"/>";
    a += "</a>"
    return a;
}

function parseVMHandlers(data)
{
    $("#vmhandlers-table").empty();
    $.each(data.vmhandlers, function(key, val) {
        curItem = generateVMHandlerCheckbox(val.name,val.href);
        $("#vmhandlers-table").append(curItem);        
    });
    
}

function generateConstraintOption(name, href)
{
    return "<option value=\"" + href + "\">"+name+"</option>";
}

function parseConstraints(data)
{
    constraintList = "<select data-role=\"constraint-type\">";
    $.each(data.constraints, function(key, val) {
        constraintList += generateConstraintOption(val.name,val.href);
        
    });
    constraintList += "</select>";
    
}

function getVMHandlers()
{
    var pathVMHandlers="/api/cimi/VMHandlers";
    $.getJSON(basicPath+pathVMHandlers,function(data){parseVMHandlers(data);}).error(function() {  APIError(); });
}

function getConstraints()
{
    
    var pathConstraints="/api/cimi/constraints";
    $.getJSON(basicPath+pathConstraints,function(data){parseConstraints(data);}).error(function() { APIError(); });
    
}

function addVM()
{
    
}

function getVMHandlersCombobox()
{
    comboBoxElems = "";
    app=$("#vmhandlers-table").find("tr > td > input:checked");
    $.each(app,function(index, cb)
        {
            cb2 = $(cb);
            name = cb2.attr("name");
            href = cb2.attr("data-href");
            curOption = "<option value=\"" + href + "\" >" + name + "</option>";
            comboBoxElems += curOption;
        }
    );
    comboBoxElems = "<select>" + comboBoxElems + "</select>";
    return comboBoxElems;
}

function addVirtualSystemToMappingsTable(vsName,handlersCombobox)
{
    row = "<tr data-vsname=\"" + vsName + "\"><td>";
    row += vsName;
    row += "</td><td>";
    row += handlersCombobox;
    row += "</td><td><input id=\"" + vsName + "-freq\" type=\"text\"></td>";
    row += "<td><input id=\"" + vsName + "-cores\" type=\"text\"></td>";
    row += "<td><input id=\"" + vsName + "-ram\" type=\"text\"></td></tr>";
    $("#vm-mapping-table-body").append(row);
}

function parseOVF()
{
    handlersCombobox = getVMHandlersCombobox();
    
    if(handlersCombobox==="<select></select>")
    {
        alert("You have to choose at least one VM handler");
        return;
    }
    
    ovf_text = $("#ovf-textarea").val();
    
    if(ovf_text==="" || ovf_text===undefined)
    {
        alert("OVF cannot be empty");
        return;
    }
    
    
    vslist=new Array();
    
    $("#vm-mapping-table-body").empty();
    $("#vm-reservation-table-body").empty();
    $("#vm-constraint-table-body").empty();
    
    
    
    $("#add-reservation-link").css("visibility", "hidden");
    $("#add-constraint-link").css("visibility", "hidden");
    
    xmlOvf = $.parseXML($("#ovf-textarea").val());
    xmlEnvelope = $(xmlOvf).find("Envelope")[0];
    xmlVSCollection = $(xmlEnvelope).find("VirtualSystemCollection")[0];
        
    $(xmlVSCollection).find("VirtualSystem").each(function(index, vs){
        vsname = $(vs).attr("ovf:id");
        addVirtualSystemToMappingsTable(vsname,handlersCombobox);
        vslist.push(vsname);
    }    
    );
    $("#submit-button").removeAttr("disabled");
    
    
    $("#add-reservation-link").css("visibility", "visible");
    $("#add-constraint-link").css("visibility", "visible");
    
}

function APIError()
{
    $("#parse-ovf-button").attr('disabled', 'disabled');
    $("#submit-button").attr('disabled', 'disabled');
    alert("Error getting data from the API");
}

function fillCountrySelect()
{
    countryList = getCountryList();
    countrySelect = "<select data-role=\"country-selector\">";
    $.each(countryList,function(i,val)
    {
        countrySelect += "<option value=\"" + val[1] + "\">" + val[0] + "</option>";
    });
    countrySelect+="</select>";
       
    
}

$(document).ready(function() {
    getVMHandlers();
    getConstraints();
    fillCountrySelect(); 
     
});


function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16)+"000000000").substr(2,8);
        return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

function addReservation()
{
    curGuid = guid();
    reservation = "<tr id=\"";
    reservation += curGuid;
    reservation += "\" ><td><select>";
    
    $.each(vslist,function(index,value)
        {
            reservation += "<option value=\"";
            reservation += value;
            reservation += "\">";
            reservation += value;
            reservation += "</option>";
        }
    );
    
    reservation += "</select></td>";
    reservation += "<td><input type=\"text\" data-role=\"quantity\"></td>";
    reservation += "<td><input id=\"" + curGuid + "-date\" type=\"text\" data-role=\"date\"></td>";
    reservation += "<td>";
    reservation += "<a href=\"#\" onclick=\"deleteReservation('";
    reservation += curGuid;
    reservation += "');return false;\">";
    reservation +=  "<img src=\"http://myriads-serv2.irisa.fr:8000/images/delete.png\" alt=\"delete\"/>";
    reservation += "</a></td></tr>";
    
    $("#vm-reservation-table-body").append(reservation);
    $(function() {
        $("#" + curGuid + "-date").datepicker({ dateFormat: "dd/mm/yy", minDate: 0, firstDay : 1 });
    });
}

function deleteReservation(rowGuid)
{
    $("#"+rowGuid).remove();
}

function addConstraint()
{
    curGuid = guid();
    constraint = "<tr id=\"";
    constraint += curGuid;
    constraint += "\" ><td><select data-role=\"virtual-system\">";
    
    $.each(vslist,function(index,value)
        {
            constraint += "<option value=\"";
            constraint += value;
            constraint += "\">";
            constraint += value;
            constraint += "</option>";
        }
    );
    
    constraint += "</select></td><td>";
    constraint += constraintList;
    constraint += "</td><td>";
    constraint += countrySelect;
    constraint += "</td><td><a href=\"#\" onclick=\"deleteConstraint('";
    constraint += curGuid;
    constraint += "');return false;\">";
    constraint +=  "<img src=\"http://myriads-serv2.irisa.fr:8000/images/delete.png\" alt=\"delete\"/>";
    constraint += "</a></td></tr>";
    
    $("#vm-constraint-table-body").append(constraint);
}

function deleteConstraint(rowGuid)
{
    $("#"+rowGuid).remove();
}

function isInt(num)
{
    numberRegEx = /^(\d)+$/;
    if(num.search(numberRegEx)==-1) return false;
    else return true;
}


function jsonGetReservations()
{
    reservationsJSON = new Array();
    
    reservations = $("#vm-reservation-table-body").find("tr");
    $.each(reservations,function(index,r)
    {
        if(errorOccured) return;
        vsname = $($(r).find("td > select")[0]).val();
        
        reservation={};
        vs = {};
        vs["href"]="#"+vsname;
        reservation["virtualSystem"] = vs;
                
        vsquantity = $($(r).find("td > input[data-role='quantity']")).val();
        
        if(!isInt(vsquantity))
        {
            alert("Quantity must be a positive integer number");
            errorOccured = true;
            return;
        }
        vsquantity = parseInt(vsquantity);
        reservation["count"]=vsquantity;
        
        date = $.datepicker.formatDate("dd/mm/yy", $($(r).find("td > input[data-role='date']")[0]).datepicker( "getDate" ));
        reservation["end-date"]=date;
        
        reservationsJSON.push(reservation);
    });
   
    if(errorOccured) return undefined;
    else return reservationsJSON;
}

function jsonGetApplications()
{
    applications = new Array();
    
    app_name = $("#app-name-text").val();
    
    application1 = {};
    application1["name"]=app_name;
    application1["OVFDeployment"]=false;
    
    OVFDescriptors = new Array();
    OVFDescriptor={};
    OVFDescriptor["OVFFile"]=$("#ovf-textarea").val();
    OVFDescriptors.push(OVFDescriptor);
    
    application1["OVFDescriptors"]=OVFDescriptors;
    reservations = jsonGetReservations();
    
    if(reservations==undefined) return undefined;
    else application1["reservations"] = reservations;
    
    applications.push(application1);
    
    return applications;
}

function jsonGetVMHandlers()
{
    
    vmhandlers = new Array();
    vmh=$("#vmhandlers-table").find("tr > td > input:checked");
    $.each(vmh,function(index, cb)
        {
            cb2 = $(cb);
            href = cb2.attr("data-href");
            jsonObjVMH = {};
            jsonObjVMH["href"]=href;
            vmhandlers.push(jsonObjVMH);
            
            
        });
    return vmhandlers;       
}

function jsonGetDefaultMapping()
{
    errorOccured = false;
    
    mappingsJSON = new Array();
    
    mappings = $("#vm-mapping-table-body").find("tr");
    $.each(mappings,function(index,m)
    {
        if(errorOccured) return;
        vsname = $(m).attr("data-vsname");
        
        mapping={};
        mapping["type"]="VM";
        
        vs = {};
        vs["href"] = "#" + vsname;
        mapping["virtualSystem"] = vs;
        
        handler = {};
        handler["href"] = $($(m).find("td > select")[0]).val();
        mapping["handler"] = handler;
        
        freq = $("#" + vsname + "-freq").val();
        cores = $("#" + vsname + "-cores").val();
        ram = $("#" + vsname + "-ram").val();
        
        if(freq)
        {
            if(!isInt(freq))
            {
                alert("Frequency must be a positive integer number (or empty)");
                errorOccured = true;
                return;
            }
            mapping["cpuFreq"]=parseInt(freq);
        }
        
        if(cores)
        {
            if(!isInt(cores))
            {
                alert("Number of cores must be a positive integer number (or empty)");
                errorOccured = true;
                return;
            }
            mapping["corecount"]=parseInt(cores);
        }
        
        if(ram)
        {
            if(!isInt(ram))
            {
                alert("RAM size must be a positive integer number (or empty)");
                errorOccured = true;
                return;
            }
            mapping["ram"]=parseInt(ram);
        }
        
        mappingsJSON.push(mapping);
    });
    
    
    return (errorOccured) ? undefined : mappingsJSON;
    
    
}

function jsonGetConstraintsMapping()
{
    constraintsMapping = new Array();
    
    constraints = $("#vm-constraint-table-body").find("tr");
    $.each(constraints,function(index,c)
    {
        vs = $($(c).find("td > select[data-role='virtual-system']")[0]).val();
        href = $($(c).find("td > select[data-role='constraint-type']")[0]).val();
        country = $($(c).find("td > select[data-role='country-selector']")[0]).val();
        
        curElement = {};
        
        constraint = {};
        constraint["href"] = href;
        curElement["constraint"]=constraint;
        
        virtualSystem = {};
        virtualSystem["href"]="#"+vs;
        curElement["virtualSystem"]=[virtualSystem];
        
        constraint_desc = $($(c).find("td > select > option[value='"+href+"']")[0]).text();
        
        regExp = /COUNTRY/;
        if(constraint_desc.search(regExp) != -1)
        {
            curElement["parameter"]=country;
        }
        
        constraintsMapping.push(curElement);
        
    });
    
    return constraintsMapping;
    
    
    
}

function submit()
{
    
    errorOccured = false;
    
    jsonObj = {};
    
    username = $("#username-hidden").val();
        
    cee_name = $("#cee-name-text").val();
    cee_desc = $("#cee-description-text").val();
    cee_stat = $("#cee-state-select").val();
    
    jsonObj["name"] = cee_name;
    jsonObj["state"] = cee_stat;
    jsonObj["VMHandlers"]=jsonGetVMHandlers();
    
    applications = jsonGetApplications();
    if(applications==undefined) return;
    else jsonObj["applications"]=applications;
    
    defaultMapping = jsonGetDefaultMapping();
    if(defaultMapping==undefined) return;
    else jsonObj["defaultMapping"]=defaultMapping;
    
    jsonObj["constraintsMapping"] = jsonGetConstraintsMapping();
    
    
    
    //alert(JSON.stringify(jsonObj));
    $.ajax({url : "/api/cimi/cee",
        type : "POST",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,   
        data : JSON.stringify(jsonObj),
        success: function( data )
        {
            html = "<div style='margin : 10px;'>";
            html += "<h1>Success</h1>";
            html += "<p>" + status + "</p></div>";
            $.colorbox({html:html,
                        opacity: 0.5,
                        overlayClose: false,
                        onClosed: function()
                        {
                            if($("#cee-state-select").val()=="ACTIVE") goToListCEE();
                        }
                        });
            
        },
        error : function( jqXHR, status, error)
        {
            html = "<div style='margin : 10px;'>";
            html += "<h1>Error</h1>";
            html += "<p>Status: "+ status + "</p>";
            html += "<p>Error: "+ error + "</p></div>";
            $.colorbox({html:html, opacity: 0.5, overlayClose: false});
        }
    });
}