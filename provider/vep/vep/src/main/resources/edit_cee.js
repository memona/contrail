/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var base_url = '';
var cee_id;
var username;
//!!!! YOU MAY NEED TO DELETE IT AFTER changing VEP code
var apicimi = "/api/cimi";

var application_href;
var vsOptionsList;

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function sendAJAXRequest(method, url)
{
    $.ajax({url : url,
    type : method,
    headers : {"Content-Type" : "application/json",
    "X-Username" : username},
    async: false,
    success: function( data )
    {
        alert("DONE");
    },
    error : function( jqXHR, status, error)
    {
        alert("FAIL");
    }
    });
    
    goToEditCEE(cee_id);
}

function vmHandlerInfoSplash(minDisk, maxDisk, minFreq, maxFreq, minRAM, maxRAM, minCores, maxCores, name, href)
{
    html = "<div style='margin : 10px;'>";
    html += "<h1>"+name+"</h1>";
    html += "<table class='table table-striped'>";
    html += "<tr><td>href</td><td>" + href + "</td></tr>";
    html += "<tr><td>Disk size</td><td>" + minDisk + "</td></tr>";
    html += "<tr><td>CPU frequency</td><td>" + minFreq + " - " + maxFreq + "</td></tr>";
    html += "<tr><td>RAM size</td><td>" + minRAM + " - " + maxRAM + "</td></tr>";
    html += "<tr><td>Core count</td><td>" + minCores + " - " + maxCores + "</td></tr>";
    html += "</table>";
    html += "</div>";
    $.colorbox({html:html, opacity: 0.5});
}

function parseVMHandler(data)
{
    a = "<tr><td>" + data.name + "</td><td style='width: 0px;'>";
    a += "<a href=\"#\" onclick=\"vmHandlerInfoSplash(";
    a += "'" + data.disksize_low + "',";
    a += "'" + data.disksize_high + "',";
    a += "'" + data.cpufreq_low + "',";
    a += "'" + data.cpufreq_high + "',";
    a += "'" + data.ram_low + "',";
    a += "'" + data.ram_high + "',";
    a += "'" + data.corecount_low + "',";
    a += "'" + data.corecount_high + "',";
    a += "'" + data.name + "',";
    a += "'" + data.id + "'";
    a += ");return false;\">";
    a +=  "<img src=\"http://myriads-serv2.irisa.fr:8000/images/info.png\" alt=\"info\"/>";
    a += "</a></td></tr>";
    $("#vmhandlers-table-body").append(a);
}

function parseVMHandlers(data)
{
    $(data.vmhandlers).each(function(i,handler)
    {
        $.ajax({url : handler.href,
        type : "GET",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,
        success: function( data )
        {
            parseVMHandler(data);
        },
        error : function( jqXHR, status, error)
        {
            getCEEFail();
        }
        });
    });
    
    
}

function netHandlerInfoSplash(id, name, pubIP, dhcp)
{
    html = "<div style='margin : 10px;'>";
    html += "<h1>"+name+"</h1>";
    html += "<table class='table table-striped'>";
    html += "<tr><td>id</td><td>" + id + "</td></tr>";
    html += "<tr><td>Public IP support</td><td>" + ((pubIP)?"Yes":"No") + "</td></tr>";
    html += "<tr><td>DHCP support</td><td>" + ((dhcp)?"Yes":"No") + "</td></tr>";
    html += "</table>";
    html += "</div>";
    $.colorbox({html:html, opacity: 0.5});
}

function parseNetworkHandler(data)
{
    a = "<tr><td>" + data.name + "</td><td style='width: 0px;'>";
    a += "<a href=\"#\" onclick=\"netHandlerInfoSplash(";
    a += "'" + data.id + "',";
    a += "'" + data.name + "',";
    a += "" + data.publicIpSupport + ",";
    a += "" + data.dhcpSupport;
    a += ");return false;\">";
    a +=  "<img src=\"http://myriads-serv2.irisa.fr:8000/images/info.png\" alt=\"info\"/>";
    a += "</a></td></tr>";
    $("#network-handlers-table-body").append(a);
    
}

function parseNetworkHandlers(data)
{
    $(data.nethandlers).each(function(i,handler)
    {
        $.ajax({url : handler.href,
        type : "GET",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,
        success: function( data )
        {
            parseNetworkHandler(data);
        },
        error : function( jqXHR, status, error)
        {
            getCEEFail();
        }
        });
    });
}

function fillVSList(data)
{
    xmlEnvelope = $(data).find("Envelope")[0];
    xmlVSCollection = $(xmlEnvelope).find("VirtualSystemCollection")[0];
        
    $(xmlVSCollection).find("VirtualSystem").each(function(index, vs){
        vsname = $(vs).attr("ovf:id");
        vsname = "#" + vsname;
        vsOptionsList += "<option value='" + vsname + "'>"+vsname+"</option>";
    }    
    );    
}

function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16)+"000000000").substr(2,8);
        return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}


function addVM()
{
    curGuid = guid();
    
    row = "<tr class='warning' id='" + curGuid + "'><td><input class='span12' data-role='vm-name' type='text'></td>";
    row += "<td><select class='span12' data-role='vs-handler'>" + vsOptionsList + "</select></td>";
    row += "<td></td><td></td><td></td>";
    row += "<td><input class='span12' data-role='context' type='text' placeholder='\"key1\":\"val1\",\"key2\":\"val2\",...'></td>";
    row += "<td><input data-role='to-delete' type='hidden'>";
    row += "<a href=\"#\" onclick=\"$('#";
    row += curGuid;
    row += "').remove();return false;\">";
    row +=  "<img src=\"http://myriads-serv2.irisa.fr:8000/images/delete.png\" alt=\"delete\"/>";
    row += "</a></td></tr>";
    $("#vm-table-body").append(row);
}

function parseVM(data)
{
    
    var vmhandler_name;
    
    $.ajax({url : apicimiFix(data.vmhandler.href),
        type : "GET",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,
        success: function( data )
        {
            vmhandler_name = data.name;
        },
        error : function( jqXHR, status, error)
        {
            getCEEFail();
        }
        });
        
    row = "<tr><td>" + data.name + "<input data-role='vm-name' ";
    row += "type='hidden' value='" + data.name + "'></td>";
    row += "<td>" + data.virtualSystem.href + "<input data-role='vs-handler' ";
    row += "type='hidden' value='" + data.virtualSystem.href + "'></td>";
    row += "<td>" + vmhandler_name + "</td>";
    row += "<td>" + data.state + "</td>";
    row +="<td>"+data.ip+"</td>";
    row += "<td></td>";
    row += "<td><input data-role='to-delete' type='checkbox'></td></tr>";
    
    $("#vm-table-body").append(row);
}

//workaround for the wrong urls
function apicimiFix(url)
{
    if(!url.match(/^\/api\/cimi/)) url = apicimi + url;
    if(url.match(/^\/api\/cimi\/api\/cimi/)) url = url.substring(9);
    return url;
}

function parseApplication(data)
{
    $("#app-name-p").text("Application: " + data.name);
    
    $(data.reservation.reservations).each(function(i,r)
    {
        row = "<tr><td>";
        row += r.virtualSystem.href;
        row += "</td><td>";
        row += r.count;
        row += "</td><td>";
        row += r.free;
        row += "</td></tr>";
        $("#reservation-table-body").append(row);
    }
    );
        
    $(data.VMs).each(function(i,r)
    {
        $.ajax({url : apicimiFix(r.href),
        type : "GET",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,
        success: function( data )
        {
            parseVM(data);
        },
        error : function( jqXHR, status, error)
        {
            getCEEFail();
        }
        });
        
    }
    );
    
    url = data.OVFs[0].href;
    $.ajax({url : url,
        type : "GET",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,
        success: function( data )
        {
            fillVSList(data);
        },
        error : function( jqXHR, status, error)
        {
            getCEEFail();
        }
    });
    
    $(data.operations).each(function(i,op)
    {
        if(op.rel=="edit")
        {
            //TODO
        }
        else if(op.rel=="delete")
        {
            //TODO
        }
        else if(op.rel=="/VEP/actions/start")
        {
            url = apicimiFix(op.href);
            $("#start-btn").attr("onclick","sendAJAXRequest('POST','"+url+"')");
        }
        else if(op.rel=="/VEP/actions/stop")
        {
            url = apicimiFix(op.href);
            $("#stop-btn").attr("onclick","sendAJAXRequest('POST','"+url+"')");
        }
    }
    );      
}



function parseApplications(data)
{
    application_href = data.applications[0].href;
    $.ajax({url : application_href,
        type : "GET",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,
        success: function( data )
        {
            parseApplication(data);
        },
        error : function( jqXHR, status, error)
        {
            getCEEFail();
        }
    });
}

function parseCEE(data)
{
    $("#cee-name-p").text(data.name);
    url = data.VMHandlers.href;
    $.ajax({url : apicimiFix(url),
        type : "GET",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,
        success: function( data )
        {
            parseVMHandlers(data);
        },
        error : function( jqXHR, status, error)
        {
            getCEEFail();
        }
    });
    
    url = data.networkHandlers.href;
    $.ajax({url : apicimiFix(url),
        type : "GET",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,
        success: function( data )
        {
            parseNetworkHandlers(data);
        },
        error : function( jqXHR, status, error)
        {
            getCEEFail();
        }
    });
    
        
    url = data.applications.href;
    $.ajax({url : apicimiFix(url),
        type : "GET",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,
        success: function( data )
        {
            parseApplications(data);
        },
        error : function( jqXHR, status, error)
        {
            getCEEFail();
        }
    });
    
    
}


//not only URL, historical name
function parseUrlVars()
{
    urlVars = getUrlVars();
    cee_id = urlVars["cee_id"];
    username = $("#username-hidden").val();
}

function getCEE()
{
    url = base_url + '/api/cimi/cee/' + cee_id;
    $.ajax({url : url,
        type : "GET",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,
        success: function( data )
        {
            parseCEE(data);
        },
        error : function( jqXHR)
        {
            getCEEFail();
        }
    });
}

function getCEEFail()
{
    alert("Failed to get information about CEE. CEE ID=" + cee_id);
}


$(document).ready(function() {
     parseUrlVars();
     getCEE();
     
});

function saveApp()
{
    result = {};
    vms = $("#vm-table-body").find("tr");
    
    vm_array = [];
    
    everythingIsOk = true;
    
    $.each(vms,function(index,m)
    {
        curvm={};
        
        if($(m).find("td > input:checked").length != 0) return;
        vmname = $($(m).find("td > input[data-role='vm-name']")[0]).val();
        if(vmname == "" || vmname == undefined)
        {
            everythingIsOk = false;
            return;
        }
        curvm["name"]=vmname;
        
        curvs={};
        
        vshandler = $($(m).find("td > [data-role='vs-handler']")[0]).val();
        curvs["href"]=vshandler;
        curvm["virtualSystem"]=curvs;
        
        context_field = $(m).find("td > [data-role='context']");
        if(context_field.length != 0)
        {
            curvm["contextualization"]=$.parseJSON('{' + $(context_field[0]).val() + '}');
        }
                
        vm_array.push(curvm);
    }
    );
    
    result["VMs"]=vm_array;
    
    if(!everythingIsOk) 
    {
        alert("Name must not be void");
        return;
    }
    
    $.ajax({url : application_href,
    type : "POST",
    headers : {"Content-Type" : "application/json",
    "X-Username" : username},
    data: JSON.stringify(result),
    async: false,
    success: function( data )
    {
        refillVMTable();
    },
    error : function( jqXHR, status, error)
    {
        alert("Failed!");
    }
    });
}

function refillVMTable()
{
    $("#vm-table-body").empty();
    
    $.ajax({url : application_href,
        type : "GET",
        headers : {"Content-Type" : "application/json",
        "X-Username" : username},
        async: false,
        success: function( data )
        {
            $(data.VMs).each(function(i,r)
            {
                $.ajax({url : apicimiFix(r.href),
                type : "GET",
                headers : {"Content-Type" : "application/json",
                "X-Username" : username},
                async: false,
                success: function( data )
                {
                    parseVM(data);
                },
                error : function( jqXHR, status, error)
                {
                    getCEEFail();
                }
                });

            }
            );
        },
        error : function( jqXHR, status, error)
        {
            getCEEFail();
        }
    });
}