package org.ow2.contrail.provider.vep;

public class netHandlerElement 
{
	public int ceeNetHandlerId;
	public String name;
	public int publicIpSupport;
	public int vlanSupport;
	public int dhcpSupport;
	public int cloudTypeId;
	public int score;
	
	public netHandlerElement()
	{
		ceeNetHandlerId = -1;
		name = "";
		publicIpSupport = -1;
		vlanSupport = -1;
		dhcpSupport = -1;
		cloudTypeId = -1;
		score = 0;
	}
}
