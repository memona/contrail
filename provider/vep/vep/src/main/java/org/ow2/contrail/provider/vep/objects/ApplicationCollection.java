package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ApplicationCollection extends VEPCollection {

	private String ceeId;
	private ArrayList<Application> apps = new ArrayList<Application>();
	
	public ApplicationCollection(String ceeId)
	{
		super("ApplicationCollection", true);
		this.ceeId = ceeId;
		this.setId(ceeId + "/applications");
		this.setResourceUri("VEP/ApplicationCollection");
	}
	
	public void retrieveAppCollection() throws SQLException 
	{
		ResultSet rs;
		logger.debug("Retrieving Applications for cee " + ceeId);
		rs = db.query("select", "id, name", "application", "where ceeid = " + ceeId);
		while (rs.next()){
			Application a = new Application(false);
			a.setName(rs.getString("name"));
			a.setId("/cee/" + ceeId + "/application/" + rs.getInt("id"));
			apps.add(a);
			this.incrementCount();
		}
	}

	public ArrayList<Application> getApps() {
		return apps;
	}
}
