package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NetHandler extends VEPObject {

	private boolean pubipsupport;
	private boolean vlantagsupport;
	private boolean dhcpsupport;
	private int cloudnetworkid;
	private String ceeId;
	private String netHandlerId;
	
	public NetHandler(boolean instantiate) {
		super("NetHandler", instantiate);
	}
	
	public NetHandler(String ceeId, String netHandlerId) {
		super("NetHandler", true);
		this.ceeId = ceeId; //not useful?
		this.netHandlerId = netHandlerId;
		this.setId("/cee/"+ceeId+"/networkHandler/"+netHandlerId);
		this.setResourceUri("VEP/NetworkHandler");
	}
	
	public NetHandler(String netHandlerId) {
		super("NetHandler", true);
		this.netHandlerId = netHandlerId;
		this.setId("/networkHandler/"+netHandlerId);
		this.setResourceUri("VEP/NetworkHandler");
	}
	
	public boolean retrieveNetHandler() throws SQLException
	{
		boolean success = false;
		ResultSet rs = db.query("select", "*", "vnethandler", "where id="+netHandlerId);
		if (rs.next())
		{
			this.setName(rs.getString("name"));
			this.setCloudnetworkid(rs.getInt("cloudnetworkid"));
			this.setVlantagsupport(rs.getBoolean("vlantagsupport"));
			this.setPubipsupport(rs.getInt("pubipsupport")!=0);
			this.setDhcpsupport(rs.getBoolean("dhcpsupport"));
			success = true;
		}
		return success;
	}



	public boolean isVlantagsupport() {
		return vlantagsupport;
	}

	public void setVlantagsupport(boolean vlantagsupport) {
		this.vlantagsupport = vlantagsupport;
	}

	public boolean isDhcpsupport() {
		return dhcpsupport;
	}

	public void setDhcpsupport(boolean dhcpsupport) {
		this.dhcpsupport = dhcpsupport;
	}

	public int getCloudnetworkid() {
		return cloudnetworkid;
	}

	public void setCloudnetworkid(int cloudnetworkid) {
		this.cloudnetworkid = cloudnetworkid;
	}

	public boolean isPubipsupport() {
		return pubipsupport;
	}

	public void setPubipsupport(boolean pubipsupport) {
		this.pubipsupport = pubipsupport;
	}

}
