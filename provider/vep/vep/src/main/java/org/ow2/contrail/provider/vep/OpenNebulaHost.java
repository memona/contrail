package org.ow2.contrail.provider.vep;

public class OpenNebulaHost 
{
	public String id;
    public String name;
    public String state;
    public String im_mad;
    public String vm_mad;
    public String tm_mad;
    public String vn_mad; //opennebula 3 virtual network manager
    public String cluster;
    public String mon_time;
    public String hid;
    public String disk_usage;
    public String mem_usage;
    public String cpu_usage;
    public String max_disk;
    public String max_mem;
    public String max_cpu;
    public String free_disk;
    public String free_mem;
    public String free_cpu;
    public String used_disk;
    public String used_mem;
    public String used_cpu;
    public String running_vms;
    public String hostName;
    public String hypervisor;
    public String arch;
    public String modelname;
    public String cpuspeed;
    public int number_cores;
    public String oneVersion; //opennebula version
}
