/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep.SchedulerClient;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author fgaudenz
 */
public class ConstraintTranslator {
     public Map<String,Constraint> translation;
    public ConstraintTranslator() {
        
        translation=new HashMap<String, Constraint>();
        Constraint halfconstraint=new Constraint("country", "not");
        translation.put("NOT IN COUNTRY",halfconstraint);
        halfconstraint=new Constraint("country","in");
         translation.put("IN COUNTRY",halfconstraint);
          halfconstraint=new Constraint("rack","same");
          translation.put("SAME RACK",halfconstraint);
          halfconstraint=new Constraint("host","not");
          translation.put("NOT SAME HOST",halfconstraint);
          
    }
   
    

        public Constraint createConstraintFromVEP(String descp, String param, List<String> on) {
          Constraint app=translation.get(descp);
          return new Constraint(app.getType(),param,app.getOperator(),on);
        //return null;
        }

   
                  
    
    
}
