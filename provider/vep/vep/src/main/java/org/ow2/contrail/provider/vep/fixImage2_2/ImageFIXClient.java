/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep.fixImage2_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author fgaudenz
 */
public class ImageFIXClient
{
 
   private Socket connection;     
   private BufferedReader fromServer;
   PrintStream toServer;
   Logger logger;
public ImageFIXClient(String addressIP,String port)
 {
            try {
                // Apre una connessione verso un server in ascolto
                // sulla porta 7777. In questo caso utilizziamo localhost
                // che corrisponde all’indirizzo IP 127.0.0.1
                logger = Logger.getLogger("VEP.RestUserDoAction");

                // Ricava lo stream di input dal socket s1
                // ed utilizza un oggetto wrapper di classe BufferedReader
                // per semplificare le operazioni di lettura
                        
                connection = new Socket(addressIP, Integer.parseInt(port));      
                fromServer = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                toServer = new PrintStream(connection.getOutputStream());
            } catch (IOException ex) {
                logger.info("Exception"+ex);
            }
   
   
 }

        
 
public void closeClient(){
            try {
                fromServer.close();
                toServer.close();
                connection.close();
            } catch (IOException ex) {
                
            }
 }
 
 public String downloadImage(String msg, String user) throws IOException{
     toServer.println("hello");
     
     String getIN;
     getIN= fromServer.readLine();
     if(getIN.compareTo("hello")==0){
         toServer.println("oneadminContrail");
         getIN= fromServer.readLine();
         if(getIN.compareTo("ok")==0){
             toServer.println("oneadmin");
             getIN= fromServer.readLine();
             if(getIN.compareTo("ok")==0){
                 toServer.println("SRC="+msg+" USR="+user);
                 getIN= fromServer.readLine();
                  String[] app=getIN.split(" ");
                 if(app[0].compareTo("ok")==0){
                        String[] app2=app[1].split("PATH=");
                        return app2[1];
                 }
                 
             }
         }
     }
     return null;
 }
 


        public boolean setImage(String idPathImage) throws IOException {
          toServer.println("DST="+idPathImage);
          String getIN= fromServer.readLine();
          if(getIN.compareTo("ok")==0){
              getIN= fromServer.readLine();
              if(getIN.compareTo("quit")==0){
                  this.closeClient();
                  return true;
              }
              
          }
          return false;
        }
    }
