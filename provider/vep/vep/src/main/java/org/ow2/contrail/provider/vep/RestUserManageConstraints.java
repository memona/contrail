package org.ow2.contrail.provider.vep;

import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class RestUserManageConstraints extends ServerResource
{
	private Logger logger;
	private DBHandler db;
	private SSLCertHandler certHandler;
	
	public RestUserManageConstraints()
	{
		logger = Logger.getLogger("VEP.RestUserManageConstraints");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestUserManageConstraints", dbType);
        certHandler = new SSLCertHandler(true); //bypasses the creation of CA service objects
	}
	
	@Post
	public Representation showConsole(Representation entity) throws ResourceException
	{
		Form form = new Form(entity);
		Map<String, String> values = form.getValuesMap();
		Set<String> keys = values.keySet();
		Collection<String> keyval = values.values();
		Iterator<String> keyIter = keys.iterator();
		Iterator<String> valIter = keyval.iterator();
		String username = "";
		String password = "";
		int ceeId = -1;
		
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			if(key.equalsIgnoreCase("username")) username = value;
			if(key.equalsIgnoreCase("password")) password = value;
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
		}
		
		//first verify whether the username and password are correct, then proceed for certificate verification
		boolean proceed = false;
		int userId = -1;
        try
        {
	        if(!VEPHelperMethods.testDBconsistency())
	        {
	        	//only grant temporary access for the predetermined user
	        	proceed = false;
	        }
	        else
	        {
	        	ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
	        	if(rs.next())
	        	{
	        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")))
	        		{
	        			userId = rs.getInt("id");
	        			proceed = true;
	        		}
	        		else
	        			proceed = false;
	        	}
	        	else
	        	{
	        		proceed = false;
	        	}
	        }
        }
        catch(Exception ex)
        {
        	logger.warn("User authentication resulted in exception.");
        	//if(logger.isDebugEnabled())
        	//	ex.printStackTrace(System.err);
        	logger.debug("Exception Caught: ", ex);
        	proceed = false;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        if(proceed)
        {
        	//now verify the certificate information
        	String certUser = "";
        	@SuppressWarnings("unchecked")
			List<X509Certificate> certs = (List<X509Certificate>)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
            for(int i=0; certs != null && i < certs.size() && proceed; i++)
            {
                X509Certificate Cert = certs.get(i);
                proceed = SSLCertHandler.isCertValid(Cert);
                certUser = SSLCertHandler.getCertDetails(Cert, "CN");
                logger.info("Certificate User name: " + certUser + " Validity [" + proceed + "]");
            }
            
            if(certUser.equalsIgnoreCase(username))
            {
            	String action = ((String) getRequest().getAttributes().get("action"));
            	if(action == null)
            		displayConstraintConsole(stringBuilder, username, password, userId, ceeId);
            	else
            	{
            		doAction(stringBuilder, username, password, userId, form, action);
            	}
            }
            else
            {
            	stringBuilder.append("<b>Welcome to VEP CEE Constraints Management Tool</b><br>").append(username).append(", access to this page <b>has been blocked</b> due to certificate error!<hr>");
    	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
    	        
    	        stringBuilder.append("<font color='red'>Invalid Certificate! Please re-install your certificate and try again. You will be forced to logout. Please re-login to download your user certificate.</font><br><br>");
    	        
    	        stringBuilder.append("<form name='reset' action='../' method='get' style='font-family:Verdana;font-size:8pt;'>");
    	        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
    	        stringBuilder.append("<tr><td align='left'><input type='submit' value='logout'>");
    	        stringBuilder.append("</table></form>");
    	        stringBuilder.append("<p style='font-family:Verdana;font-size:8pt;background:#FFD1B7;;padding:5px;'>It is advised to close your browser after you logout in order to clear the old certificate selection from the browser cache.</p>");
    	        stringBuilder.append("</table>");
            }
        }
        else
        {
        	stringBuilder.append("<b>Welcome to VEP CEE Constraints Management Tool</b><br>").append(username).append(", your login attempt <b>failed</b>!<hr>");
	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
	        
	        stringBuilder.append("<font color='red'>Login Failed! Please try again.</font><br><br>");
	        
	        stringBuilder.append("<form name='reset' action='../' method='get' style='font-family:Verdana;font-size:8pt;'>");
	        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
	        stringBuilder.append("<tr><td align='left'><input type='submit' value='retry'>");
	        stringBuilder.append("</table></form>");
	        
	        stringBuilder.append("</table>");
        }
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
		return value;
	}
	
	private void doAction(StringBuilder stringBuilder, String username, String password, int userid, Form form, String action)
	{
		Map<String, String> values = form.getValuesMap();
		Set<String> keys = values.keySet();
		Collection<String> keyval = values.values();
		Iterator<String> keyIter = keys.iterator();
		Iterator<String> valIter = keyval.iterator();
		boolean status = true;
		String message = "";
		int ceeId = -1;
		
		if(action.equalsIgnoreCase("modify"))
		{
			int constraintListId = -1;
			while(keyIter.hasNext() && valIter.hasNext())
			{
				String key = keyIter.next();
				String value = valIter.next();
				//System.err.println(key + ", " + value);
				if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
				if(key.equalsIgnoreCase("ceeconstraintlistid")) constraintListId = Integer.parseInt(value);
			}
			//now trying to delete the appropriate rule either from ceevmhandlerconstraint or vmslotconstraint table
			try
			{
				ResultSet rs = db.query("select", "count(*)", "ceevmhandlerconstraint", "WHERE ceeconstraintlistid=" + constraintListId);
				if(rs.next())
				{
					if(rs.getInt(1) > 0)
					{
						//constraint belongs to VMhandlerconstraints
						status = db.delete("ceevmhandlerconstraint", "WHERE ceeconstraintlistid=" + constraintListId);
						if(status)
							status = db.delete("ceeconstraintslist", "WHERE id=" + constraintListId);
					}
					else
					{
						status = db.delete("vmslotconstraint", "WHERE ceeconstraintlistid=" + constraintListId);
						if(status)
							status = db.delete("ceeconstraintslist", "WHERE id=" + constraintListId);
					}
				}
			}
			catch(Exception ex)
			{
				message += "Exception caught while processing your request. ";
				logger.info("Exception caught trying to delete the constraint rule from CEE " + ceeId);
				//if(logger.isDebugEnabled())
				//	ex.printStackTrace(System.err);
				logger.debug("Exception Caught: ", ex);
				status = false;
			}
			
		}
		else if(action.equalsIgnoreCase("add"))
		{	
			int ceeConstraintId = -1;
			int vmSlotId = -1;
			int vmHandlerId = -1;
			String param = "";
			String ceeElement = "";
			
			while(keyIter.hasNext() && valIter.hasNext())
			{
				String key = keyIter.next();
				String value = valIter.next();
				//System.err.println(key + ", " + value);
				if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
				if(key.equalsIgnoreCase("ceeconstraintid")) ceeConstraintId = Integer.parseInt(value);
				if(key.equalsIgnoreCase("cee-element")) ceeElement = value;
				if(key.equalsIgnoreCase("param")) param = value;
				if(key.equalsIgnoreCase("vmhandlerid")) vmHandlerId = Integer.parseInt(value);
				if(key.equalsIgnoreCase("vmslotid")) vmSlotId = Integer.parseInt(value);
			}
			if(ceeConstraintId == -1)
			{
				status = false;
				message += "You must choose a valid base constraint to construct the rule from. ";
			}
			if(param == null || (param != null && param.trim().length() == 0))
			{
				status = false;
				message += "You must provide a valid paramater for this constraint. Please consult the user guide to learn more about this. ";
			}
			if(ceeElement.equalsIgnoreCase("vmhandler") && vmHandlerId == -1)
			{
				status = false;
				message += "You want to attach a constraint rule to VM handler element, you must select a valid handler to attach this rule to. ";
			}
			if(ceeElement.equalsIgnoreCase("vmslot") && vmSlotId == -1)
			{
				status = false;
				message += "You want to attach a constraint rule to VM slot, you must select a valid VM slot to attach this rule to. ";
			}
			
			if(param != null) param = param.trim();
			
			if(status)
			{
				if(ceeElement.equalsIgnoreCase("vmhandler"))
				{
					try
					{
						ResultSet rs = db.query("select", "max(id)", "ceeconstraintslist", "");
	                    int id = 1;
	                    if(rs.next()) id = rs.getInt(1) + 1;
	                    rs.close();
	                    status = db.insert("ceeconstraintslist", "(" + id + ", " + ceeConstraintId + ")");
	                    if(status)
	                    {
	                    	rs = db.query("select", "max(id)", "ceevmhandlerconstraint", "");
	                    	int newid = 1;
	                    	if(rs.next()) newid = rs.getInt(1) + 1;
		                    rs.close();
		                    status = db.insert("ceevmhandlerconstraint", "(" + newid + ", " + vmHandlerId + ", " + id + ", '" + param + "')");
	                    }
					}
					catch(Exception ex)
					{
						message += "Exception caught while processing your request. ";
						logger.info("Exception caught during adding the vm handler constraint rule to CEE " + ceeId);
						//if(logger.isDebugEnabled())
						//	ex.printStackTrace(System.err);
						logger.debug("Exception Caught: ", ex);
						status = false;
					}
				}
				else
				{
					try
					{
						ResultSet rs = db.query("select", "max(id)", "ceeconstraintslist", "");
	                    int id = 1;
	                    if(rs.next()) id = rs.getInt(1) + 1;
	                    rs.close();
	                    status = db.insert("ceeconstraintslist", "(" + id + ", " + ceeConstraintId + ")");
	                    if(status)
	                    {
	                    	rs = db.query("select", "max(id)", "vmslotconstraint", "");
	                    	int newid = 1;
	                    	if(rs.next()) newid = rs.getInt(1) + 1;
		                    rs.close();
		                    status = db.insert("vmslotconstraint", "(" + newid + ", " + vmSlotId + ", " + id + ", '" + param + "')");
	                    }
					}
					catch(Exception ex)
					{
						message += "Exception caught while processing your request. ";
						logger.info("Exception caught during adding the vm handler constraint rule to CEE " + ceeId);
						//if(logger.isDebugEnabled())
						//	ex.printStackTrace(System.err);
						logger.debug("Exception Caught: ", ex);
						status = false;
					}
				}
			}
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../constraints' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void displayConstraintConsole(StringBuilder stringBuilder, String username, String password, int userid, int ceeId)
	{
		stringBuilder.append("Welcome <i><font color='green'>").append(username).append("</font></i> to VEP <b>Constrained Execution Environment</b> (CEE) Constraints Management Portal for CEE ").append(ceeId).append(".")
			.append("<br><hr>");
		stringBuilder.append("<table style='width:1000px;font-family:Verdana;font-size:10pt;'>")
			.append("<tr><td style='width:800px;font-family:Verdana;font-size:10pt;'>");
		//////////////////showing the list of constraints for this CEE next
		stringBuilder.append("<div>");
		stringBuilder.append("<table cellspacing='3' cellpadding='3' style='font-family:Verdana;font-size:9pt;border-style:dashed;border-width:1px;border-color:silver;'>");
		try
		{
			ResultSet rs = db.query("select", "*", "ceeconstraints", "WHERE ceeid=" + ceeId);
			int count=0;
			int count1=0;
			while(rs.next())
			{
				count++;
				int id = rs.getInt("id");
				int constraintType = rs.getInt("constrainttype");
				String constraintDescp = "";
				rs = db.query("select", "descp", "constraints", "WHERE id=" + constraintType);
				if(rs.next()) constraintDescp = rs.getString("descp");
				rs = db.query("select", "*", "ceeconstraintslist", "WHERE ceeconstraintsid=" + id);
				int counter = 0;
				while(rs.next())
				{
					counter++;
					count1++;
					if(count1 == 1)
					{
						stringBuilder.append("<form method='post' action='../constraints/modify' style='font-family:Verdana;font-size:9pt;'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
	    					.append("<input type='hidden' name='password' value='").append(password).append("'>")
	    					.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>");
						stringBuilder.append("<tr><th style='background:#B2C0F7;'>select <th style='background:#C7CDE6;'>rule attached to <th style='background:#B2C0F7;'>base constraint <th style='background:#C7CDE6;'>paramater <th style='background:#B2C0F7;'>cee element id");
					}
					int specificConstraintId = rs.getInt("id");
					rs = db.query("select", "*", "ceevmhandlerconstraint", "WHERE ceeconstraintlistid=" + specificConstraintId);
					String constraintParam = "";
					int ceeVmHandlerId = -1;
					int vmHandlerConstraintId = -1;
					int vmSlotConstraintId = -1;
					int ceeVmSlotId = -1;
					if(rs.next())
					{
						constraintParam = rs.getString("param");
						ceeVmHandlerId = rs.getInt("ceevmhandlerid");
						vmHandlerConstraintId = rs.getInt("id");
						if(count1 == 1)
							stringBuilder.append("<tr><td><input type='radio' name='ceeconstraintlistid' value='").append(specificConstraintId).append("' checked>");
						else
							stringBuilder.append("<tr><td><input type='radio' name='ceeconstraintlistid' value='").append(specificConstraintId).append("'>");
						stringBuilder.append("<td>VMHandler<td>").append(constraintDescp).append("<td>").append(constraintParam).append("<td>").append(ceeVmHandlerId);
					}
					rs = db.query("select", "*", "vmslotconstraint", "WHERE ceeconstraintlistid=" + specificConstraintId);
					if(rs.next())
					{
						constraintParam = rs.getString("param");
						ceeVmSlotId = rs.getInt("vmslotid");
						vmSlotConstraintId = rs.getInt("id");
						if(count1 == 1)
							stringBuilder.append("<tr><td><input type='radio' name='ceeconstraintlistid' value='").append(specificConstraintId).append("' checked>");
						else
							stringBuilder.append("<tr><td><input type='radio' name='ceeconstraintlistid' value='").append(specificConstraintId).append("'>");
						stringBuilder.append("<td>VMSlot<td>").append(constraintDescp).append("<td>").append(constraintParam).append("<td>").append(ceeVmSlotId);
					}
					
					//resetting the sql query
					int count3=0;
					rs = db.query("select", "*", "ceeconstraintslist", "WHERE ceeconstraintsid=" + id);
	            	while(count3 < counter){ rs.next(); count3++; }
				}
				
				//resetting the sql query
				int count2=0;
				rs = db.query("select", "*", "ceeconstraints", "WHERE ceeid=" + ceeId);
            	while(count2 < count){ rs.next(); count2++; }
			}
			if(count1 > 0)
			{
				stringBuilder.append("<tr><td colspan='5' align='right'><input type='submit' value='delete rule'>");
				stringBuilder.append("</form>");
			}
			else
				stringBuilder.append("<tr><td>No specific constraint rule found for this CEE.");
		}
		catch(Exception ex)
		{
			logger.info("Exception caught while listing the constraints for current CEE: " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception Caught: ", ex);
		}
		stringBuilder.append("</table></div><br>");
		////////////////now creating form to add a new constraint rule
		stringBuilder.append("<hr><div style='padding:0px;background:white;font-family:Verdana;font-size:9pt;border-style:solid;border-color:orange;border-width:0px;'>");
		stringBuilder.append("<form method='post' action='../constraints/add' style='font-family:Verdana;font-size:9pt;'>")
			.append("<input type='hidden' name='username' value='").append(username).append("'>")
			.append("<input type='hidden' name='password' value='").append(password).append("'>")
			.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>");
		stringBuilder.append("<table cellspacing='2' cellpadding='2' style='font-family:Verdana;font-size:9pt;background:#CAD7FF;'><tr><th colspan='2' align='left'>Add a new constraint rule using this form. Fill in appropriate data and click create rule button.")
			.append("<tr><td>Select a constraint type for generating the new rule: <td><select name='ceeconstraintid'><option value='-1'>Not selected</option>");
		try
		{
			ResultSet rs = db.query("select", "id, constrainttype", "ceeconstraints", "WHERE ceeid=" + ceeId);
			int count = 0;
			while(rs.next())
			{
				count++;
				int constraintId = rs.getInt("id");
				int constraintTypeId = rs.getInt("constrainttype");
				String constraintDescp = "";
				rs = db.query("select", "descp", "constraints", "WHERE id=" + constraintTypeId);
				if(rs.next()) constraintDescp = rs.getString("descp");
				stringBuilder.append("<option value='").append(constraintId).append("'>").append(constraintId).append(": ").append(constraintDescp).append("</option>");
				
				//resetting the sql query
				int counter=0;
				rs = db.query("select", "id, constrainttype", "ceeconstraints", "WHERE ceeid=" + ceeId);
            	while(counter < count){ rs.next(); counter++; }
			}
		}
		catch(Exception ex)
		{
			logger.info("Exception caught while generating the constraints list for current CEE: " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception Caught: ", ex);
		}
		stringBuilder.append("</select>");
		stringBuilder.append("<tr><td>Type of CEE element to attach this constraint: <td><input type='radio' name='cee-element' value='vmhandler' checked> VM hanlder <input type='radio' name='cee-element' value='vmslot'> VM slot");
		stringBuilder.append("<tr><td>Constraint Parameter: <td><input type='text' name='param' size='32'>");
		stringBuilder.append("<tr><td>If VM handler is selected, choose the specific handler to attach this rule to: <td><select name='vmhandlerid'><option value='-1'>Not selected</option>");
		try
		{
			ResultSet rs = db.query("select", "id, vmhandlerid", "ceevmhandlers", "WHERE ceeid=" + ceeId);
			int count = 0;
			while(rs.next())
			{
				count++;
				int vmHandlerId = rs.getInt("id");
				int vmHandlerTypeId = rs.getInt("vmhandlerid");
				String vmHandlerName = "";
				rs = db.query("select", "name", "vmhandler", "WHERE id=" + vmHandlerTypeId);
				if(rs.next()) vmHandlerName = rs.getString("name");
				stringBuilder.append("<option value='").append(vmHandlerId).append("'>").append(vmHandlerId).append(": ").append(vmHandlerName).append("</option>");
				
				//resetting the sql query
				int counter=0;
				rs = db.query("select", "id, vmhandlerid", "ceevmhandlers", "WHERE ceeid=" + ceeId);
            	while(counter < count){ rs.next(); counter++; }
			}
		}
		catch(Exception ex)
		{
			logger.info("Exception caught while generating the vm handlers list for current CEE: " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception Caught: ", ex);
		}
		stringBuilder.append("</select>");
		stringBuilder.append("<tr><td>If VM slot is selected, choose the specific slot to attach this rule to: <td><select name='vmslotid'><option value='-1'>Not selected</option>");
		try
		{
			ResultSet rs = db.query("select", "id", "vmslots", "WHERE ceeid=" + ceeId);
			while(rs.next())
			{
				stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
			}
		}
		catch(Exception ex)
		{
			logger.info("Exception caught while generating the vm slots list for current CEE: " + ceeId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception Caught: ", ex);
		}
		stringBuilder.append("</select>");
		stringBuilder.append("<tr><td colspan='2' align='right'><input type='submit' value='create rule'>");
		stringBuilder.append("</table></form></div>");
		
		stringBuilder.append("<td valign='top' style='font-family:Verdana;font-size:10pt;'>");
		///////////////////////// navigation buttons go here
		stringBuilder.append("<form name='goback' action='../doaction' method='post' style='font-family:Verdana;font-size:8pt;'>");
        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
		stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
		stringBuilder.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>");
		stringBuilder.append("<input type='hidden' name='requesttype' value='loadcee'>");
        stringBuilder.append("<input type='submit' value='cancel and go back'></form>");
        
		stringBuilder.append("</table>");
	}
}
