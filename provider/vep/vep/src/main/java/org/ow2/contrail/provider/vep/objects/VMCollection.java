package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class VMCollection extends VEPCollection {

	private String ceeId;
	private String applicationId;
	public ArrayList<VEPVirtualMachine> vms = new ArrayList<VEPVirtualMachine>();
	
	public VMCollection(String ceeId, String applicationId) {
		super("VMCollection", true);
		this.ceeId = ceeId;
		this.applicationId = applicationId;
		this.setResourceUri("VEP/VMCollection");
		this.setId(ceeId+"/vms");
	}

	/**
	 * Retrieve from VMSlots
	 * Selects all ovf from applicationid, select all vmslots from vmslots where ovf = one of the previous + ceeid = the requested ceeid
	 * @throws SQLException
	 */
	public void retrieveVMCollection() throws SQLException
	{
		ResultSet rs;
		logger.debug("Retrieving VirtualMachines collection for application " + applicationId);

		rs = db.query("select", "v.id", "vmslots as v, ovf as o, ovfmap as om", "where o.applicationid="  + applicationId + " and om.ovfcontainerid=o.id and v.ovfmapid=om.id");
		while (rs.next()){
			VEPVirtualMachine v = new VEPVirtualMachine(false);
			v.setId("/cee/" + ceeId + "/application/" + applicationId + "/vm/" + rs.getInt("id"));
			vms.add(v);
			this.incrementCount();
		}
	}
}
