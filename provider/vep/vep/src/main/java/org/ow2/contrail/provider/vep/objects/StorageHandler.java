package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.ow2.contrail.provider.vep.App;

public class StorageHandler extends VEPObject {

	private String ceeId;
	private String storageHandlerId;
	private String endpoint;
	private String storagetype;
	private int rackid;
	
	
	public StorageHandler(boolean instantiate) {
		super("StorageHandler", instantiate);
	}
	
	public StorageHandler(String ceeId, String storageHandlerId) {
		super("StorageHandler", true);
		this.ceeId = ceeId; //not useful?
		this.storageHandlerId = storageHandlerId;
		this.setId("/cee/"+ceeId+"/storageHandler/"+storageHandlerId);
		this.setResourceUri("VEP/StorageHandler");
	}
	
	public StorageHandler(String storageHandlerId) {
		super("NetHandler", true);
		this.storageHandlerId = storageHandlerId;
		this.setId("/storageHandler/"+storageHandlerId);
		this.setResourceUri("VEP/StorageHandler");
	}

	public void retrieveStorageHandler() throws SQLException
	{
		ResultSet rs = db.query("select", "storagehandler.name as shname, storagetypeid, endpoint, rackid", "storagehandler,storage", "where storagehandler.id="+storageHandlerId+" and storage.id=storagehandler.storageid");
		if (rs.next())
		{
			this.setName(rs.getString("shname"));
			switch (rs.getInt("storagetypeid")) {
			case App.GAFS_SHARE:
				storagetype = "GAFS Share";
				break;
			case App.LOCAL_DISK_STORAGE:
				storagetype = "Local Disk Storage";
				break;
			case App.NFS_SHARE:
				storagetype = "NFS Share";
				break;
			default:
				setError("Unknown Storage Type");
			}
			endpoint = rs.getString("endpoint"); // may be null
			rackid = rs.getInt("rackid");
		} else {
			setError("Unknown Storage Handler id");
		}
	}

	public String getEndPoint() {
		return endpoint;
	}

	public String getType() {
		return storagetype;
	}
}
