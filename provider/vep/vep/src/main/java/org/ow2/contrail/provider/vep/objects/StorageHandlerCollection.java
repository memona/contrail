package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StorageHandlerCollection extends VEPCollection {

	private String ceeId;
	private ArrayList<StorageHandler> shs = new ArrayList<StorageHandler>();
	
	public StorageHandlerCollection(String ceeId) {
		super("StorageHandlerCollection", true);
		this.ceeId = ceeId;
		this.setId("/cee/"+ceeId+"/storageHandlers");
		this.setResourceUri("VEP/StorageHandlerCollection");
	}

	public StorageHandlerCollection() {
		super("StorageHandlerCollection", true);
		this.setId("/storageHandlers");
		this.setResourceUri("VEP/StorageHandlerCollection");
	}

	public void retrieveStorageHandlerCollection() throws SQLException {
		ResultSet rs;
		if(ceeId != null)
		{
			logger.debug("Retrieving Storage Handlers for cee " + ceeId);
			//TODO:update request
			rs = db.query("select", "storagehandler.id as sid,storagehandler.name as sname", "ceestoragehandlers,storagehandler", "where ceeid = " + ceeId + " and storagehandler.id=ceestoragehandlers.storagehandlerid");
			while (rs.next()){
				StorageHandler n = new StorageHandler(false);
				n.setName(rs.getString("sname"));
				n.setId("/storageHandler/" + rs.getInt("sid"));
				shs.add(n);
				this.incrementCount();
			}
		}
		else 
		{
			logger.debug("Retrieving Storage Handler");
			rs = db.query("select", "id, name", "storagehandler", "");
			while (rs.next()){
				StorageHandler n = new StorageHandler(false);
				n.setName(rs.getString("name"));
				n.setId("/storageHandler/" + rs.getInt("id"));
				shs.add(n);
				this.incrementCount();
			}
		}
	}

	public ArrayList<StorageHandler> getShs() {
		return shs;
	}

}
