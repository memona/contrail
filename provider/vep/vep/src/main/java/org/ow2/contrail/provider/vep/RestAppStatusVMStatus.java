package org.ow2.contrail.provider.vep;

import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class RestAppStatusVMStatus extends ServerResource
{
	private Logger logger;
	private DBHandler db;
	private SSLCertHandler certHandler;
	
	public RestAppStatusVMStatus()
	{
		logger = Logger.getLogger("VEP.RestAppStatusVMStatus");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestAppStatusVMStatus", dbType);
        certHandler = new SSLCertHandler(true); //bypasses the creation of CA service objects
	}
	
	@Post
	public Representation showVMStatus(Representation entity) throws ResourceException
	{
		Form form = new Form(entity);
		Map<String, String> values = form.getValuesMap();
		Set<String> keys = values.keySet();
		Collection<String> keyval = values.values();
		Iterator<String> keyIter = keys.iterator();
		Iterator<String> valIter = keyval.iterator();
		String username = "";
		String password = "";
		int ceeId = -1;
		int vmSlotId = -1;
		
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			if(key.equalsIgnoreCase("username")) username = value;
			if(key.equalsIgnoreCase("password")) password = value;
			if(key.equalsIgnoreCase("ceeid")) ceeId = Integer.parseInt(value);
			if(key.equalsIgnoreCase("vmslotid")) vmSlotId = Integer.parseInt(value);
		}
		
		//first verify whether the username and password are correct, then proceed for certificate verification
		boolean proceed = false;
		int userId = -1;
        try
        {
	        if(!VEPHelperMethods.testDBconsistency())
	        {
	        	//only grant temporary access for the predetermined user
	        	proceed = false;
	        }
	        else
	        {
	        	ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
	        	if(rs.next())
	        	{
	        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")))
	        		{
	        			userId = rs.getInt("id");
	        			proceed = true;
	        		}
	        		else
	        			proceed = false;
	        	}
	        	else
	        	{
	        		proceed = false;
	        	}
	        }
        }
        catch(Exception ex)
        {
        	logger.warn("User authentication resulted in exception.");
        	//if(logger.isDebugEnabled())
        	//	ex.printStackTrace(System.err);
        	logger.debug("Exception caught ", ex);
        	proceed = false;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        if(proceed)
        {
        	//now verify the certificate information
        	String certUser = "";
        	@SuppressWarnings("unchecked")
			List<X509Certificate> certs = (List<X509Certificate>)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
            for(int i=0; certs != null && i < certs.size() && proceed; i++)
            {
                X509Certificate Cert = certs.get(i);
                proceed = SSLCertHandler.isCertValid(Cert);
                certUser = SSLCertHandler.getCertDetails(Cert, "CN");
                logger.info("Certificate User name: " + certUser + " Validity [" + proceed + "]");
            }
            
            if(certUser.equalsIgnoreCase(username))
            {
            	String action = ((String) getRequest().getAttributes().get("action"));
            	if(action == null)
            		displayVMConsole(stringBuilder, username, password, userId, ceeId, vmSlotId, form);
            	else
            	{
            		doVMAction(stringBuilder, username, password, userId, ceeId, vmSlotId, form, action);
            	}
            }
            else
            {
            	stringBuilder.append("<b>Welcome to VEP Virtual Machine Management Console</b><br>").append(username).append(", access to this page <b>has been blocked</b> due to certificate error!<hr>");
    	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
    	        
    	        stringBuilder.append("<font color='red'>Invalid Certificate! Please re-install your certificate and try again. You will be forced to logout. Please re-login to download your user certificate.</font><br><br>");
    	        
    	        stringBuilder.append("<form name='reset' action='../' method='get' style='font-family:Verdana;font-size:8pt;'>");
    	        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
    	        stringBuilder.append("<tr><td align='left'><input type='submit' value='logout'>");
    	        stringBuilder.append("</table></form>");
    	        stringBuilder.append("<p style='font-family:Verdana;font-size:8pt;background:#FFD1B7;;padding:5px;'>It is advised to close your browser after you logout in order to clear the old certificate selection from the browser cache.</p>");
    	        stringBuilder.append("</table>");
            }
        }
        else
        {
        	stringBuilder.append("<b>Welcome to VEP Virtual Machine Management Console</b><br>").append(username).append(", your login attempt <b>failed</b>!<hr>");
	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
	        
	        stringBuilder.append("<font color='red'>Login Failed! Please try again.</font><br><br>");
	        
	        stringBuilder.append("<form name='reset' action='../' method='get' style='font-family:Verdana;font-size:8pt;'>");
	        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
	        stringBuilder.append("<tr><td align='left'><input type='submit' value='retry'>");
	        stringBuilder.append("</table></form>");
	        
	        stringBuilder.append("</table>");
        }
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
		return value;
	}
	
	private void doVMAction(StringBuilder stringBuilder, String username, String password, int userid, int ceeId, int vmslotId, Form form, String action)
	{
		Map<String, String> values = form.getValuesMap();
		Set<String> keys = values.keySet();
		Collection<String> keyval = values.values();
		Iterator<String> keyIter = keys.iterator();
		Iterator<String> valIter = keyval.iterator();
		String requestType = "";
		//System.err.println("Function argument list: " + username + ", " + password + ", " + userid + ", " + ceeId + ", " + vmslotId + ", " + action);
		while(keyIter.hasNext() && valIter.hasNext())
		{
			String key = keyIter.next();
			String value = valIter.next();
			//System.err.println("Key: " + key + ", Value: " + value);
			if(key.equalsIgnoreCase("requesttype")) requestType = value;
		}
		
		if(requestType.equalsIgnoreCase("stop"))
		{
			stopVM(stringBuilder, username, password, userid, ceeId, vmslotId);
		}
		else if(requestType.equalsIgnoreCase("restart"))
		{
			restartVM(stringBuilder, username, password, userid, ceeId, vmslotId);
		}
		else if(requestType.equalsIgnoreCase("deleteslot"))
		{
			deleteVMslot(stringBuilder, username, password, userid, ceeId, vmslotId);
		}
	}
	
	private void stopVM(StringBuilder stringBuilder, String username, String password, int userid, int ceeId, int vmslotId)
	{
		String message = "";
		boolean status = true;
		try
		{
			ResultSet rs = db.query("select", "iaasid, hostid", "vmslots", "WHERE id=" + vmslotId);
			if(rs.next())
			{
				int vmIaasId = Integer.parseInt(rs.getString("iaasid"));
				int hostId = rs.getInt("hostid");
				rs = db.query("select", "cloudtypeid", "host", "WHERE id=" + hostId);
				if(rs.next())
				{
					int cloudTypeId = rs.getInt("cloudtypeid");
					rs = db.query("select", "*", "cloudtype", "WHERE id=" + cloudTypeId);
					if(rs.next())
					{
						String headIP = rs.getString("headip");
						int headPort = rs.getInt("headport");
						String cloudVersion = rs.getString("version");
						int type = rs.getInt("typeid");
						rs = db.query("select", "name", "supportedcloud", "WHERE id=" + type);
						if(rs.next())
						{
							String cloudType = rs.getString("name");
							rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + userid + " AND cloudtype=" + cloudTypeId);
							if(rs.next())
							{
								String iaasUser = rs.getString("iaasuser");
								String iaasPass = rs.getString("iaaspass");
								if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("3."))
								{
									One3XMLRPCHandler one3 = new One3XMLRPCHandler(headIP, String.valueOf(headPort), iaasUser, VEPHelperMethods.makeSHA1Hash(iaasPass), "RestAppStatusVM");
									try
									{
										if(vmIaasId != -1)
										{
											status = one3.shutdownVM(vmIaasId);
											if(!status)
											{
												message += "Probably the VM state does not permit SHUTDOWN operation. ";
											}
											db.update("vmslots", "status='SCH'", "WHERE id=" + vmslotId);
										}
										else
										{
											status = false;
											message += "Incorrect VM ID found. Shutdown operation can not be performed. ";
										}
									}
									catch(Exception ex)
									{
										status = false;
										message += "Exception caught during VM shutdown operation. ";
										logger.info("Exception caught during VM shutdown operation for VM slot: " + vmslotId);
										//if(logger.isDebugEnabled())
										//	ex.printStackTrace(System.err);
										logger.debug("Exception caught ", ex);
									}
								}
								else if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("2."))
								{
									status = false;
									message += "This cloud type is currently not supported. ";
									
									
								}
							}
							else
							{
								status = false;
								message += "Corresponding cloud account for this user is not found. ";
							}
						}
					}
					else
					{
						status = false;
						message += "VEP database error. Could not locate target cloud record. ";
					}
				}
				else
				{
					status = false;
					message += "VEP database error. Could not decipher the target cloud type. ";
				}
			}
			else
			{
				status = false;
				message += "VEP database error. Could not locate VM slot record. ";
			}
		}
		catch(Exception ex)
		{
			status = false;
			logger.info("Exception caught while performing VM shutdown for VM slot: " + vmslotId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The VM shutdown operation was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The VM shutdown operation was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void restartVM(StringBuilder stringBuilder, String username, String password, int userid, int ceeId, int vmslotId)
	{
		String message = "";
		boolean status = true;
		try
		{
			ResultSet rs = db.query("select", "iaasid, hostid", "vmslots", "WHERE id=" + vmslotId);
			if(rs.next())
			{
				int vmIaasId = Integer.parseInt(rs.getString("iaasid"));
				int hostId = rs.getInt("hostid");
				rs = db.query("select", "cloudtypeid", "host", "WHERE id=" + hostId);
				if(rs.next())
				{
					int cloudTypeId = rs.getInt("cloudtypeid");
					rs = db.query("select", "*", "cloudtype", "WHERE id=" + cloudTypeId);
					if(rs.next())
					{
						String headIP = rs.getString("headip");
						int headPort = rs.getInt("headport");
						String cloudVersion = rs.getString("version");
						int type = rs.getInt("typeid");
						rs = db.query("select", "name", "supportedcloud", "WHERE id=" + type);
						if(rs.next())
						{
							String cloudType = rs.getString("name");
							rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + userid + " AND cloudtype=" + cloudTypeId);
							if(rs.next())
							{
								String iaasUser = rs.getString("iaasuser");
								String iaasPass = rs.getString("iaaspass");
								if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("3."))
								{
									One3XMLRPCHandler one3 = new One3XMLRPCHandler(headIP, String.valueOf(headPort), iaasUser, VEPHelperMethods.makeSHA1Hash(iaasPass), "RestAppStatusVM");
									try
									{
										if(vmIaasId != -1)
										{
											status = one3.restartVM(vmIaasId);
											if(!status)
											{
												message += "Probably the VM state does not permit RESTART operation. ";
											}
										}
										else
										{
											status = false;
											message += "Incorrect VM ID found. Restart operation can not be performed. ";
										}
									}
									catch(Exception ex)
									{
										status = false;
										message += "Exception caught during VM restart operation. ";
										logger.info("Exception caught during VM restart operation for VM slot: " + vmslotId);
										//if(logger.isDebugEnabled())
										//	ex.printStackTrace(System.err);
										logger.debug("Exception caught ", ex);
									}
								}
								else if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("2."))
								{
									status = false;
									message += "This cloud type is currently not supported. ";
									
									
								}
							}
							else
							{
								status = false;
								message += "Corresponding cloud account for this user is not found. ";
							}
						}
					}
					else
					{
						status = false;
						message += "VEP database error. Could not locate target cloud record. ";
					}
				}
				else
				{
					status = false;
					message += "VEP database error. Could not decipher the target cloud type. ";
				}
			}
			else
			{
				status = false;
				message += "VEP database error. Could not locate VM slot record. ";
			}
		}
		catch(Exception ex)
		{
			status = false;
			logger.info("Exception caught while performing VM restart for VM slot: " + vmslotId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The VM restart operation was successful!</font><br><br>");
			stringBuilder.append("<form name='goback' action='../../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The VM restart operation was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void deleteVMslot(StringBuilder stringBuilder, String username, String password, int userid, int ceeId, int vmslotId)
	{
		String message = "";
		int ovfmapId = -1;
		boolean status = true;
		try
		{
			ResultSet rs = db.query("select", "iaasid, hostid, ovfmapid", "vmslots", "WHERE id=" + vmslotId);
			if(rs.next())
			{
				int vmIaasId = Integer.parseInt(rs.getString("iaasid"));
				int hostId = rs.getInt("hostid");
				ovfmapId = rs.getInt("ovfmapid");
				rs = db.query("select", "cloudtypeid", "host", "WHERE id=" + hostId);
				if(rs.next())
				{
					int cloudTypeId = rs.getInt("cloudtypeid");
					rs = db.query("select", "*", "cloudtype", "WHERE id=" + cloudTypeId);
					if(rs.next())
					{
						String headIP = rs.getString("headip");
						int headPort = rs.getInt("headport");
						String cloudVersion = rs.getString("version");
						int type = rs.getInt("typeid");
						rs = db.query("select", "name", "supportedcloud", "WHERE id=" + type);
						if(rs.next())
						{
							String cloudType = rs.getString("name");
							rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + userid + " AND cloudtype=" + cloudTypeId);
							if(rs.next())
							{
								String iaasUser = rs.getString("iaasuser");
								String iaasPass = rs.getString("iaaspass");
								if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("3."))
								{
									One3XMLRPCHandler one3 = new One3XMLRPCHandler(headIP, String.valueOf(headPort), iaasUser, VEPHelperMethods.makeSHA1Hash(iaasPass), "RestAppStatusVM");
									try
									{
										if(vmIaasId != -1)
										{
											status = one3.shutdownVM(vmIaasId);
											if(!status)
											{
												message += "Probably the VM state does not permit SHUTDOWN operation. ";
											}
										}
										else
										{
											status = false;
											message += "Incorrect VM ID found. Shutdown operation can not be performed. ";
										}
									}
									catch(Exception ex)
									{
										status = false;
										message += "Exception caught during VM shutdown operation. ";
										logger.info("Exception caught during VM shutdown operation for VM slot: " + vmslotId);
										//if(logger.isDebugEnabled())
										//	ex.printStackTrace(System.err);
										logger.debug("Exception caught ", ex);
									}
								}
								else if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("2."))
								{
									status = false;
									message += "This cloud type is currently not supported. ";
									
									
								}
							}
							else
							{
								status = false;
								message += "Corresponding cloud account for this user is not found. ";
							}
						}
					}
					else
					{
						status = false;
						message += "VEP database error. Could not locate target cloud record. ";
					}
				}
				else
				{
					status = false;
					message += "VEP database error. Could not decipher the target cloud type. ";
				}
			}
			else
			{
				status = false;
				message += "VEP database error. Could not locate VM slot record. ";
			}
		}
		catch(Exception ex)
		{
			status = false;
			logger.info("Exception caught while performing VM shutdown for VM slot: " + vmslotId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
		}
		if(!status)
		{
			stringBuilder.append("<font color='red'>The VM stop operation was un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("Will still continue with VM slot deletion operation. It will be wise to notify your system administrator of a possibility of an untracked VM.<br><br>");
        	status = true;
        	message = "";
		}
		//now deleting the corresponding connection info entry if any
		status = db.delete("connectioninfo", "WHERE vmslotid=" + vmslotId);
		if(status)
		{
			status = db.delete("vmslotconstraint", "WHERE vmslotid=" + vmslotId);
			if(status)
			{
				status = db.delete("vmslots", "WHERE id=" + vmslotId);
				if(!status)
				{
					message += "Could not remove this VM slot record from VEP database. ";
				}
				else
				{
					if(ovfmapId != -1)
						db.update("ovfmap", "status='RDY'", "WHERE id=" + ovfmapId);
				}
			}
			else
			{
				message += "Could not properly remove associated constraints for this VM slot. ";
				status = db.delete("vmslots", "WHERE id=" + vmslotId);
				if(!status)
				{
					message += "Could not remove this VM slot record from VEP database. ";
				}
				else
				{
					if(ovfmapId != -1)
						db.update("ovfmap", "status='RDY'", "WHERE id=" + ovfmapId);
				}
			}
		}
		else
		{
			message += "Could not properly remove associated connection records for this VM slot. ";
			status = db.delete("vmslotconstraint", "WHERE vmslotid=" + vmslotId);
			if(status)
			{
				status = db.delete("vmslots", "WHERE id=" + vmslotId);
				if(!status)
				{
					message += "Could not remove this VM slot record from VEP database. ";
				}
				else
				{
					if(ovfmapId != -1)
						db.update("ovfmap", "status='RDY'", "WHERE id=" + ovfmapId);
				}
			}
			else
			{
				message += "Could not properly remove associated constraints for this VM slot. ";
				status = db.delete("vmslots", "WHERE id=" + vmslotId);
				if(!status)
				{
					message += "Could not remove this VM slot record from VEP database. ";
				}
				else
				{
					if(ovfmapId != -1)
						db.update("ovfmap", "status='RDY'", "WHERE id=" + ovfmapId);
				}
			}
		}
		if(status)
		{
			stringBuilder.append("<font color='green'>The VM slot removal operation was successful!</font><br><br>");
			stringBuilder.append("<font color='orange'>Any potential warning messages: ").append(message).append("</font><br><br>");
			stringBuilder.append("<form name='goback' action='../../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='proceed'></form>");
		}
		else
		{
			stringBuilder.append("<font color='red'>The VM slot removal operation was partially un-successful!</font><br><br>");
        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	stringBuilder.append("<form name='goback' action='../../doaction' method='post'>")
				.append("<input type='hidden' name='username' value='").append(username).append("'>")
				.append("<input type='hidden' name='password' value='").append(password).append("'>")
				.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
				.append("<input type='hidden' name='requesttype' value='loadcee'>")
				.append("<input type='submit' value='go back'></form>");
		}
	}
	
	private void displayVMConsole(StringBuilder stringBuilder, String username, String password, int userid, int ceeId, int vmslotId, Form form)
	{
		stringBuilder.append("Welcome <i><font color='green'>").append(username).append("</font></i> to VEP <b>Virtual Machine</b> Status Management Portal. ")
			.append("You can manage your deployed virtual machine from this page.<br><hr>");
		stringBuilder.append("<table style='width:1000px;font-family:Verdana;font-size:10pt;'>")
			.append("<tr><td style='width:800px;font-family:Verdana;font-size:10pt;'>");
		///////////////////////// main VM management elements go next
		try
		{
			stringBuilder.append("<table style='width:799px;font-family:Verdana;font-size:10pt;'>")
				.append("<tr><td valign='top'><img src='https://www.cise.ufl.edu/~pharsh/public/vm_new.png'>");
			stringBuilder.append("<br><hr style='color:gray;border-style:dotted;border-top-width:1px;border-left-width:0px;border-right-width:0px;border-bottom-width:0px;height:0px;'>");
			//now generating the navigation panel
	        stringBuilder.append("<script type=\"text/javascript\">")
	        	.append("function submitForm(val) {")
	        	.append("document.forms['vmbar'].action = './getvmstatus/doaction';")
	        	.append("if(val == 1) document.forms['vmbar'].action = './getvmstatus/doaction';")
	        	.append("if(val == 1) document.forms['vmbar'].elements['requesttype'].value = 'stop';")
	        	.append("if(val == 2) document.forms['vmbar'].action = './getvmstatus/doaction';")
	        	.append("if(val == 2) document.forms['vmbar'].elements['requesttype'].value = 'restart';")
	        	.append("if(val == 3) document.forms['vmbar'].action = './getvmstatus/doaction';")
	        	.append("if(val == 3) document.forms['vmbar'].elements['requesttype'].value = 'deleteslot';")
	        	.append("var status = confirm(\"Do you really want to perform this action on the VM?\");")
	        	.append("if(status)")
	        	.append("document.vmbar.submit();")
	        	.append("} </script>");
	        stringBuilder.append("<form name='vmbar' method='post' action='./getvmstatus/doaction'>")
	        	.append("<input type='hidden' name='username' value='").append(username).append("'>")
    			.append("<input type='hidden' name='password' value='").append(password).append("'>")
    			.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
    			.append("<input type='hidden' name='vmslotid' value='").append(vmslotId).append("'>")
    			.append("<input type='hidden' name='userid' value='").append(userid).append("'>")
    			.append("<input type='hidden' name='requesttype' value='nop'>")
	        	.append("</form>");
	        stringBuilder.append("<table cellpadding='0' style='font-family:Verdana;color:#333366;font-size:9pt;'>")
	        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(1);'>Stop VM</a>")
	        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(2);'>Restart VM</a>")
	        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(3);'>Delete VM Slot</a>")
	        	.append("</table>");
			//////all status notes go here
			stringBuilder.append("<td align='left' valign='top'>");
			stringBuilder.append("<u>Virtual Machine corresponding to CEE VM Slot ID</u>: " + vmslotId + "<br><br>");
			ResultSet rs = db.query("select", "iaasid, hostid, status", "vmslots", "WHERE id=" + vmslotId);
			if(rs.next())
			{
				int iaasId = Integer.parseInt(rs.getString("iaasid"));
				int hostId = rs.getInt("hostid");
				String stat = rs.getString("status");
				if(stat.equalsIgnoreCase("INI"))
				{
					stringBuilder.append("Virtual Machine is in <font color='navy'>INITIALIZED</font> state.");
				}
				else if(stat.equalsIgnoreCase("SCH"))
				{
					stringBuilder.append("Virtual Machine is in <font color='navy'>SCHEDULED</font> state. It has been scheduled to run on physical host with ID: " + hostId);
				}
				else if(stat.equalsIgnoreCase("DEP"))
				{
					stringBuilder.append("Virtual Machine is in <font color='green'>DEPLOYED</font> state. It has been deployed on physical host with ID: " + hostId);
				}
				else if(stat.equalsIgnoreCase("ERR"))
				{
					stringBuilder.append("Virtual Machine is in <font color='green'>ERROR</font> state. Please try deploying your CEE again.");
				}
				rs = db.query("select", "cloudtypeid", "host", "WHERE id=" + hostId);
				if(rs.next())
				{
					int cloudTypeId = rs.getInt("cloudtypeid");
					rs = db.query("select", "*", "cloudtype", "WHERE id=" + cloudTypeId);
					if(rs.next())
					{
						String cloudName = rs.getString("name");
						String headIP = rs.getString("headip");
						int headPort = rs.getInt("headport");
						String cloudVersion = rs.getString("version");
						int type = rs.getInt("typeid");
						rs = db.query("select", "name", "supportedcloud", "WHERE id=" + type);
						if(rs.next())
						{
							String cloudType = rs.getString("name");
							stringBuilder.append("<br>Deployed on Cloud: ").append(cloudType).append(", Cloud Version: ").append(cloudVersion);
							//select the user iaas account to query the VM information
							rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + userid + " AND cloudtype=" + cloudTypeId);
							if(rs.next())
							{
								String iaasUser = rs.getString("iaasuser");
								String iaasPass = rs.getString("iaaspass");
								if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("3."))
								{
									One3XMLRPCHandler one3 = new One3XMLRPCHandler(headIP, String.valueOf(headPort), iaasUser, VEPHelperMethods.makeSHA1Hash(iaasPass), "RestAppStatusVM");
									try
									{
										if(iaasId != -1)
										{
											OpenNebulaVM temp = one3.getVmInfo(iaasId);
											if(temp != null)
											{
												//System.err.println(temp.rawMsg);
												String rawMsg = temp.rawMsg;
												rawMsg = rawMsg.trim().replaceAll("<","&lt;").replaceAll(">","&gt;");
												
												if(logger.isDebugEnabled())
												{
													stringBuilder.append("<br><br>Printing the RAW response from the Cloud (displayed in debug mode only):<br><div style='font-family:Courier;font-size:6pt;font-weight:bold;background:#C9DBFF;color:#444630;border-width:1px;border-style:dotted;padding:5px;'>");
													stringBuilder.append(rawMsg);
													stringBuilder.append("</div>");
												}
												
												stringBuilder.append("<br><br>Associated Details:<br><table cellspacing='2' cellpadding='3' style='font-family:Verdana;font-size:9pt;border-width:1px;border-style:dashed;'>");
												stringBuilder.append("<tr><td style='background:#EEEEEE;width:300px;'>Virual Machine Name<td style='background:#D7D7D7;width:499px;'>").append(temp.name);
												stringBuilder.append("<tr><td style='background:#EEEEEE;width:300px;'>VM (STATE, LCM_STATE)<td style='background:#D7D7D7;width:499px;'>(").append(temp.state).append(", ").append(temp.lcm_state).append(")");
												if(temp.state_value != null)
													stringBuilder.append(" OR (").append(temp.state_value).append(", ").append(temp.lcmstate_value).append(")");
												stringBuilder.append("<tr><td style='background:#EEEEEE;width:300px;'>Bytes Transmitted<td style='background:#D7D7D7;width:499px;'>").append(temp.net_tx);
												stringBuilder.append("<tr><td style='background:#EEEEEE;width:300px;'>Bytes Received<td style='background:#D7D7D7;width:499px;'>").append(temp.net_rx);
												stringBuilder.append("<tr><td style='background:#EEEEEE;width:300px;'>Virual Machine GRAPHICS HOST<td style='background:#D7D7D7;width:499px;'>").append(temp.graphics_host);
												stringBuilder.append("<tr><td style='background:#EEEEEE;width:300px;'>Virual Machine GRAPHICS PORT<td style='background:#D7D7D7;width:499px;'>").append(temp.graphics_port);
												stringBuilder.append("<tr><td style='background:#EEEEEE;width:300px;'>Virual Machine GRAPHICS TYPE<td style='background:#D7D7D7;width:499px;'>").append(temp.graphics_type);
												stringBuilder.append("<tr><td style='background:#EEEEEE;width:300px;'>Virual Machine IP<td style='background:#D7D7D7;width:499px;'>").append(temp.nic_ip);
												stringBuilder.append("<tr><td style='background:#EEEEEE;width:300px;'>Virual Machine MAC<td style='background:#D7D7D7;width:499px;'>").append(temp.nic_mac);
												long sTime = Long.parseLong(temp.start_time) * 1000;
												Date dt = new Date(sTime);
												stringBuilder.append("<tr><td style='background:#EEEEEE;width:300px;'>Virual Machine Start Time<td style='background:#D7D7D7;width:499px;'>").append(dt.toString());
												stringBuilder.append("</table>");
											}
											else
											{
												logger.warn("No response got for the VM from the parser.");
											}
										}
									}
									catch(Exception ex)
									{
										logger.warn("Exception caught while getting VM info on ONE 3");
										//if(logger.isDebugEnabled())
										//	ex.printStackTrace(System.err);
										logger.debug("Exception caught ", ex);
										stringBuilder.append("</table>");
									}
								}
								else if(cloudType.startsWith("OpenNebula") && cloudVersion.startsWith("2."))
								{
									//not supported yet
									stringBuilder.append("<br><font color='orange'>This OpenNebula version is not supported yet.</font>");
								}
							}
							else
							{
								logger.warn("No suitable user account map located to query VM information on the target cloud.");
							}
						}
					}
				}
			}
			stringBuilder.append("</table>");
		}
		catch(Exception ex)
		{
			logger.info("Exception caught while getting information on VMSlot: " + vmslotId);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
			stringBuilder.append("Can not retrive the VM information. Maybe the CEE has not been deployed yet. Or maybe the VM is waiting for the disk image to get ready. Please try after a few seconds again.");
			stringBuilder.append("</table>");
		}
		
		stringBuilder.append("<td valign='top' style='font-family:Verdana;font-size:10pt;'>");
		///////////////////////// navigation buttons go here
		stringBuilder.append("<form name='goback' action='../doaction' method='post' style='font-family:Verdana;font-size:8pt;'>");
        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
		stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
		stringBuilder.append("<input type='hidden' name='ceeid' value='").append(ceeId).append("'>")
			.append("<input type='hidden' name='requesttype' value='loadcee'>");
        stringBuilder.append("<input type='submit' value='cancel and go back'></form>");
        
		stringBuilder.append("</table>");
	}
}
