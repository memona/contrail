package org.ow2.contrail.provider.vep;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class RestCIMINetHandlerResource extends ServerResource {
	
	private Logger logger;
	private DBHandler db;
	
	public RestCIMINetHandlerResource() {
		logger = Logger.getLogger("VEP.NetHandlerResource");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestNetHandlerResource", dbType);
	}
	
	@Get("json")
	public Representation getValue() throws ResourceException
	{
		Representation response = null;
		JSONObject json = new JSONObject();
		ResultSet rs;
		//TODO: No Certificate hack for testing
//		String userName = "ghostwheel";
		Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
		String userName = requestHeaders.getFirstValue("X-Username");
		String ceeid = ((String) getRequest().getAttributes().get("ceeid"));
		String nid = ((String) getRequest().getAttributes().get("nid"));
		try {
		if (ceeid != null) {
			
				// returns data of a specific cee constraints
				json = CIMIParser.NetHandlerCollectionRetrieve(userName, ceeid);
		}
		else {
			if(nid == null)
			{
				//list available NetHandlers
				json = CIMIParser.NetHandlerCollectionRetrieve(userName);
			} else {
				json = CIMIParser.NetHandlerRetrieve(userName, nid);
			}
			
		}
		} catch (SQLException e)
		{
			json.put("error", "Exception while retrieving VNetHandlers");
			logger.debug("Error is:",e);
		}catch(UnauthorizedRestAccessException e){
                                         setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                                         return new StringRepresentation("{}", MediaType.APPLICATION_JSON);
                                    }
		response = new StringRepresentation(json.toJSONString(), MediaType.APPLICATION_JSON);
		return response;
	}
}
