package org.ow2.contrail.provider.vep;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.FileUtils;

import org.restlet.data.Disposition;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;

public class RestUserDoLoginOLD extends ServerResource
{
	private Logger logger;
	private DBHandler db;
	
	public RestUserDoLoginOLD()
	{
		logger = Logger.getLogger("VEP.RestUserDoLogin");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestUserDoLogin", dbType);
	}
	
	@Post
    public Representation getResult(Representation entity) throws ResourceException
    {
	Form form = new Form(entity);
	String username = form.getFirstValue("username");
        String password = form.getFirstValue("password");
        String actionType = ((String) getRequest().getAttributes().get("action"));
        boolean proceed = false;
        try
        {
	        if(!VEPHelperMethods.testDBconsistency())
	        {
	        	//only grant temporary access for the predetermined user
	        	proceed = false;
	        }
	        else
	        {
	        	ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
	        	if(rs.next())
	        	{
	        		if(VEPHelperMethods.makeSHA1Hash(password).equalsIgnoreCase(rs.getString("password")))
	        			proceed = true;
	        		else
	        			proceed = false;
	        	}
	        	else
	        	{
	        		proceed = false;
	        	}
	        }
        }
        catch(Exception ex)
        {
        	logger.warn("User authentication resulted in exception.");
        	//if(logger.isDebugEnabled())
        	//	ex.printStackTrace(System.err);
        	logger.debug("Exception Caught: ", ex);
        	proceed = false;
        }
        
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String acceptType = requestHeaders.getFirstValue("Accept");
        
        Representation response = null;
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml(proceed, username, password, actionType, form);
            else if(acceptType.contains("json"))
            {
                //response = toJson(proceed);
            }
        }
        else
        {
            //default rendering ...
            response = toHtml(proceed, username, password, actionType, form);
        }
        return response;
    }
	
	public Representation toHtml(boolean proceed, String username, String password, String action, Form form) throws ResourceException
    {
		StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        int userId = -1;
        if(proceed)
        {
        	byte[] temp = null;
                
                //FILIPPO:AUTHORIZED
                
        	stringBuilder.append("<b>Welcome to VEP Application Management Tool</b><br>").append(username).append(", you are now logged in.<hr>");
	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");

	        if(action == null)
	        {
	        	try
		        {
		        	ResultSet rs = db.query("select", "id,certificate", "user", "WHERE username='" + username + "'");
		        	if(rs.next())
		        	{
		        		userId = rs.getInt("id");
		        		stringBuilder.append("<div style='background:#2E61BF;color:white;font-family:Verdana;font-size:10pt;padding:10px;'>");
		        		temp = rs.getBytes("certificate");
		        		if(temp != null && temp.length > 1)
		        		{
		        			stringBuilder.append("Your user certificate file is ready for download! <a href='Javascript:void(0);' style='color:#B8CFF9;' onClick='Javascript:getCert();'>Click here</a> to download it.");
		        			stringBuilder.append("<form name='getcertificate' id='getcert' method='post' action='../dologin'>");
		        			stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
			    	        stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
			    	        stringBuilder.append("<input type='hidden' name='requesttype' value='getcert'></form>");
			    	        stringBuilder.append("<script type=\"text/javascript\">")
					        	.append("function getCert() {")
					        	.append("document.getcertificate.submit();")
					        	.append("} </script>");
		        		}
		        		else
		        		{
		        			stringBuilder.append("The system administrator has not generated a user certificate file for you! A download link will be made available here once the file is created for you.");
		        		}
		        		stringBuilder.append("</div><br>");
		        	}
		        }
		        catch(Exception ex)
		        {
		        	logger.warn("Exception caught while trying to retrieve the user certificate information.");
		        	//if(logger.isDebugEnabled())
		        	//	ex.printStackTrace(System.err);
		        	logger.debug("Exception Caught: ", ex);
		        }
		        
		        Map<String, String> values = form.getValuesMap();
				Set<String> keys = values.keySet();
				Collection<String> keyval = values.values();
				Iterator<String> keyIter = keys.iterator();
				Iterator<String> valIter = keyval.iterator();
				String requestType = "";
				while(keyIter.hasNext() && valIter.hasNext())
				{
					String key = keyIter.next();
					String value = valIter.next();
					if(key.equalsIgnoreCase("requesttype")) requestType = value;
				}
				//////////////// send the user certificate
				if(requestType.equalsIgnoreCase("getcert"))
				{
					logger.info("Certificate download request received for user: " + username);
					try
					{
						String scratchSpace = VEPHelperMethods.getProperty("vep.scratch", logger);
						if(!scratchSpace.endsWith(System.getProperty("file.separator"))) scratchSpace += System.getProperty("file.separator");
						File vepScratch = new File(scratchSpace);
						if(!(vepScratch.exists() && vepScratch.isDirectory()))
						{
							boolean dirStatus = (new File(scratchSpace)).mkdirs();
							if(dirStatus)
								logger.info("Created the vep scratch directory at " + scratchSpace);
							else
								logger.warn("Problem creating the vep scratch space. Check for permissions ...");
						}
						logger.info("Creating a temporary certificate file at " + scratchSpace + username + ".pfx");
						FileOutputStream fos = new FileOutputStream(scratchSpace + username + ".pfx");
						fos.write(temp);
						fos.close();
						
						File toSend = new File(scratchSpace + username + ".pfx");
						FileRepresentation rep = new FileRepresentation(toSend, MediaType.APPLICATION_ALL); 
						Disposition disp = new Disposition(Disposition.TYPE_ATTACHMENT); 
						disp.setFilename(toSend.getName());
						disp.setSize(toSend.length());
						rep.setDisposition(disp);
						//toSend.delete();
						return rep; 
					}
					catch(Exception ex)
					{
						logger.warn("Exception caught while sending the certificate file.");
						//if(logger.isDebugEnabled())
						//	ex.printStackTrace(System.err);
						logger.debug("Exception Caught: ", ex);
					}
				}
				
				stringBuilder.append("<div style='font-family:Verdana;font-size:9pt;'>Dear ").append(username).append(", you can perform several actions using this web interface.<br>")
					.append("<ol><li>Manage your account settings<li>Manage your virtual applications<li>Check runnings status of your applications</ol>Click on the appropriate links in the ")
					.append("left panel to access desired functions. <font color='red'>Please install your user certificate before proceeding.</font> Application management access is allowed ")
					.append("only through a valid SSL connection.</div><br>");
				////////////////////////////////////////main content panel variable data goes next
				if(requestType.equalsIgnoreCase("useraccount"))
				{
					stringBuilder.append("<div style='padding:10px;font-family:Verdana;font-size:9pt;background:#6D6784;color:white;'>User Account management panel allows you to change your password ")
						.append(" and link your existing cloud account to this account. You can also request an automated account linkage with the cloud in case you do not have one already!</div>");
					stringBuilder.append("<div style='padding:10px;font-family:Verdana;font-size:9pt;background:#CCC6E2;color:black;'>");
					/////////////////////displaying any existing cloud accounts for this user
					
					stringBuilder.append("<form name='changepass' action='../dologin/changepass' method='post' style='font-family:Verdana;font-size:9pt;'>");
	    	        stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;background:#CCC6E2;width:650px;border-style:solid;border-width:1px;border-color:#6D6784;'><tr><td>");
	    	        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
	    	        stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
	    	        stringBuilder.append("<input type='hidden' name='requesttype' value='").append(requestType).append("'>");
	    			stringBuilder.append("Current password <td><input type='password' name='currpass' size='26'><td>");
	    			stringBuilder.append("<tr><td>New password <td><input type='password' name='newpass1' size='26'><td> at least 8 characters");
	    			stringBuilder.append("<tr><td>Retype new password<td><input type='password' name='newpass2' size='26'><td>");
	    	        stringBuilder.append("<tr><td><td colspan='2' align='right'><input type='submit' value='change password'>");
	    	        stringBuilder.append("</table></form>");
	    	        //display linked cloud account for this user account
	    	        stringBuilder.append("<div style='font-family:Verdana;font-size:9pt;color:black;'>");
	    	        stringBuilder.append("Below are the various cloud accounts linked to your user id:<br><br>");
	    	        try
	    	        {
	    	        	ResultSet rs = db.query("select", "id", "user", "where username='" + username + "'");
	    	        	int vepuserId = -1;
	    	        	if(rs.next())
	    	        		vepuserId = rs.getInt("id");
	    	        	if(vepuserId == -1)
	    	        	{
	    	        		stringBuilder.append("<br><font color='red'>VEP DB is currupted. Please fix it!</font><br>");
	    	        	}
	    	        	else
	    	        	{
	    	        		stringBuilder.append("<table cellspacing='2' cellpadding='2' border='1' style='width:650px;font-family:Verdana;font-size:9pt;color:black;border-width:1px;border-style:solid;border-color:#6D6784;'><tr><th>map-id<th>cloud-type<th>iaas-user<th>iaas-uid<th>iaas-password</tr>");
	    	        		rs = db.query("select", "*", "accountmap", "where vepuser=" + vepuserId);
	    	        		int counter=0;
	    	        		while(rs.next())
	    	        		{
	    	        			int mapid = rs.getInt("id");
	    	        			String iaasUser = rs.getString("iaasuser");
	    	        			String iaasPass = rs.getString("iaaspass");
	    	        			String iaasId = rs.getString("iaasuid");
	    	        			int cloudTypeId = rs.getInt("cloudtype");
	    	        			String cloudType = "";
	    	        			counter++;
	    	        			rs = db.query("select", "name", "cloudtype", "WHERE id=" + cloudTypeId);
	            				if(rs.next())
	            				{
	            					cloudType = rs.getString("name");
	            				}
	            				rs.close();
	            				stringBuilder.append("<tr><td>").append(mapid).append("<td>").append(cloudType).append("<td>").append(iaasUser).append("<td>").append(iaasId).append("<td>").append(iaasPass).append("</tr>");
	            				rs = db.query("select", "*", "accountmap", "where vepuser=" + vepuserId);
	            				int count = 0;
	            				while(count < counter) { rs.next(); count++; }    				
	    	        		}
	    	        		stringBuilder.append("</table>");
	    	        	}
	    	        }
	    	        catch(Exception ex)
	    	        {
	    	        	logger.warn("Exception caught while requestion account map information for user account");
	    	        	//if(logger.isDebugEnabled())
	    	        	//	ex.printStackTrace(System.err);
	    	        	logger.debug("Exception Caught: ", ex);
	    	        }
	    	        stringBuilder.append("</div><br><div style='font-family:Verdana; font-size:9pt; color:black;'>");
	    	        stringBuilder.append("Please use the form below to update/add an existing cloud account to this account.<br><br>");
	        		stringBuilder.append("<form name='editcloudmap' action='../dologin/editcloudmap' method='post' style='font-family:Verdana;font-size:9pt;color:black;'>");
			        stringBuilder.append("<table style='width:650px;font-family:Verdana;font-size:9pt;background:#CCC6E2;color:black;border-color:#6D6784;border-width:1px;border-style:solid;'>");
			        stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
					stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
					stringBuilder.append("<input type='hidden' name='requesttype' value='").append(requestType).append("'>");
					stringBuilder.append("<tr style='background:#6D6784;color:white;'><td colspan='2'>Please check the box in case you are modifying an existing entry. If left unchecked the submit action will add a new cloud account mapping for your account.</tr>");
					stringBuilder.append("<tr><td>Check if modifying an entry<td align='right'><input type='checkbox' name='modifybox' value='yes'>");
					stringBuilder.append("<tr><td>Select the entry (if modifying)<td align='right'><select name='mapid'><option value='-1'>Not selected</option>");
					int vepuserId = -1;
					try
					{
						ResultSet rs = db.query("select", "id", "user", "where username='" + username + "'");
	    	        	if(rs.next())
	    	        		vepuserId = rs.getInt("id");
						rs = db.query("select", "id", "accountmap", "where vepuser=" + vepuserId);
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
						}
						
					}
					catch(Exception ex)
					{
						
					}
					stringBuilder.append("</select>");
					stringBuilder.append("<tr><td colspan='2'><hr></tr>");
					stringBuilder.append("<input type='hidden' name='vepuserid' value='").append(vepuserId).append("'>");
					stringBuilder.append("<tr><td>Destination Cloud<td align='right'><select name='cloudtypeid'><option value='-1'>Not selected</option>");
					try
					{
						ResultSet rs = db.query("select", "id, name", "cloudtype", "");
						while(rs.next())
						{
							stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getString("name")).append("</option>");
						}
					}
					catch(Exception ex)
					{
						
					}
					stringBuilder.append("<tr><td>IaaS Username<td align='right'><input type='text' name='iaasuser' size='34'>");
					stringBuilder.append("<tr><td>IaaS Password<td align='right'><input type='password' name='iaaspass' size='34'>");
					stringBuilder.append("<tr><td>IaaS ID<td align='right'><input type='text' name='iaasuid' size='34'>");
			        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
			        stringBuilder.append("</table></form>");
	    	        stringBuilder.append("</div>Please request automatic account creation and linkage using the form below.<br><br>");
					///////////////////now show control to automatic linkage of account in case an account does not exist already
					stringBuilder.append("<form name='automapaccounts' method='post' action='../dologin/automapaccount' style='font-family:Verdana;font-size:9pt;'>");
					stringBuilder.append("<table style='width:650px;font-family:Verdana;font-size:9pt;background:#CCC6E2;color:black;border-color:#6D6784;border-width:1px;border-style:solid;'>");
					stringBuilder.append("<input type='hidden' name='username' value='").append(username).append("'>");
					stringBuilder.append("<input type='hidden' name='password' value='").append(password).append("'>");
					stringBuilder.append("<input type='hidden' name='requesttype' value='").append(requestType).append("'>");
					try
					{
						
						ResultSet rs = db.query("select", "id, name", "cloudtype", "");
						int cloudId = -1;
						String cloudName = "";
						int counter = 0;
	    	        	while(rs.next())
	    	        	{
	    	        		cloudId = rs.getInt("id");
	    	        		cloudName = rs.getString("name");
	    	        		counter++;
	    	        		rs = db.query("select", "count(*)", "accountmap", "WHERE cloudtype=" + cloudId + " AND vepuser=" + userId);
	    	        		if(rs.next())
	    	        		{
	    	        			if(rs.getInt(1) > 0)
	    	        			{
	    	        				stringBuilder.append("<tr><td>Request an automatic mapping for ").append(cloudName).append("<td align='right'><input type='radio' name='createmap' value='").append(cloudId).append("' DISABLED>");
	    	        			}
	    	        			else
	    	        			{
	    	        				stringBuilder.append("<tr><td>Request an automatic mapping for ").append(cloudName).append("<td align='right'><input type='radio' name='createmap' value='").append(cloudId).append("'>");
	    	        			}
	    	        		}
	    	        		rs.close();
	    	        		int count = 0;
	    	        		rs = db.query("select", "id, name", "cloudtype", "");
	    	        		while(count < counter){rs.next(); count++;}
	    	        	}
	    	        	//vepuserId = rs.getInt("id");
						//rs = db.query("select", "id", "accountmap", "where vepuser=" + vepuserId);
						//while(rs.next())
						//{
						//	stringBuilder.append("<option value='").append(rs.getInt("id")).append("'>").append(rs.getInt("id")).append("</option>");
						//}
						
					}
					catch(Exception ex)
					{
						logger.warn("Exception caught while generating cloud account request form.");
						//if(logger.isDebugEnabled())
						//	ex.printStackTrace(System.err);
						logger.debug("Exception Caught: ", ex);
					}
					stringBuilder.append("<tr><td><td align='right'><input type='submit' value='submit'>");
					stringBuilder.append("</table></form>");
					stringBuilder.append("</div>");
				}
				
				stringBuilder.append("<td valign='top'>");
		        stringBuilder.append("<form name='dologout' action='../' method='get' style='font-family:Verdana;font-size:8pt;'>");
		        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
		        stringBuilder.append("<tr><td><td align='right'><input type='submit' value='logout'>");
		        stringBuilder.append("</table></form>");
		        //now generating the navigation panel
		        stringBuilder.append("<script type=\"text/javascript\">")
		        	.append("function submitForm(val) {")
		        	.append("document.forms['sidebar'].action = '../doaction';")
		        	.append("if(val == 1) document.forms['sidebar'].action = '../dologin';")
		        	.append("if(val == 1) document.forms['sidebar'].elements['requesttype'].value = 'useraccount';")
		        	.append("if(val == 2) document.forms['sidebar'].action = '../doaction';")
		        	.append("if(val == 2) document.forms['sidebar'].elements['requesttype'].value = 'manageapp';")
		        	.append("if(val == 3) document.forms['sidebar'].action = '../appstatus';")
		        	.append("if(val == 3) document.forms['sidebar'].elements['requesttype'].value = 'checkstatus';")
		        	.append("document.sidebar.submit();")
		        	.append("} </script>");
		        stringBuilder.append("<form name='sidebar' method='post' action='../doaction'>")
		        	.append("<input type='hidden' name='username' value='").append(username).append("'>")
        			.append("<input type='hidden' name='password' value='").append(password).append("'>")
        			.append("<input type='hidden' name='requesttype' value='nop'>")
		        	.append("</form>");
		        stringBuilder.append("<table cellpadding='2' style='font-family:Verdana;color:#333366;font-size:9pt;'>")
		        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(1);'>Manage Account</a>")
		        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(2);'>Manage Applications</a>")
		        	.append("<tr><td style='background:#FFFFFF;'><a href='Javascript:void(0);' onClick='Javascript:submitForm(3);'>Check Application Status</a>")
		        	.append("</table>");
		        stringBuilder.append("<p style='font-family:Verdana;font-size:8pt;background:#FFD1B7;;padding:5px;'>It is advised to close your browser after you logout in order to clear the certificate selection from the browser cache.</p>");
	        }
	        else
	        {
	        	Map<String, String> values = form.getValuesMap();
				Set<String> keys = values.keySet();
				Collection<String> keyval = values.values();
				Iterator<String> keyIter = keys.iterator();
				Iterator<String> valIter = keyval.iterator();
				String requestType = "";
	        	if(action.equalsIgnoreCase("changepass"))
	        	{
	        		String currpass = "";
	        		String newpass1 = "";
	        		String newpass2 = "";
					while(keyIter.hasNext() && valIter.hasNext())
					{
						String key = keyIter.next();
						String value = valIter.next();
						if(key.equalsIgnoreCase("currpass")) currpass = value;
						if(key.equalsIgnoreCase("newpass1")) newpass1 = value;
						if(key.equalsIgnoreCase("newpass2")) newpass2 = value;
						if(key.equalsIgnoreCase("requesttype")) requestType = value;
					}
					boolean status = true;
        	        if(password.equals(currpass) && newpass1.equals(newpass2) && newpass1.length() >= 8)
        	        {
        	        	try
        	        	{
        	        		db.update("user", "password='" + VEPHelperMethods.makeSHA1Hash(newpass1) + "'", "WHERE username='" + username + "'");
        	        	}
        	        	catch(Exception ex)
        	        	{
        	        		status = false;
        	        		//if(logger.isDebugEnabled())
        	        		//	ex.printStackTrace(System.err);
        	        		logger.debug("Exception Caught: ", ex);
        	        	}
        	        }
        	        else
        	        {
        	        	status = false;
        	        }
        	        if(!status)
    	        	{
    	        		stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
    	        	}
    	        	else
    	        	{
    	        		stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
    	        	}
        	        if(status)
        	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(newpass1).append("'>")
							.append("<input type='hidden' name='requesttype' value='").append(requestType).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        else
        	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='hidden' name='requesttype' value='").append(requestType).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
	        	}
	        	else if(action.equalsIgnoreCase("editcloudmap"))
        		{
        	        boolean status = true;
        	        boolean isModify = false;
        			int mapId = -1;
        			int cloudtypeId = -1;
        			String iaasUser = "";
        			String iaasPass = "";
        			String iaasUid = "";
        			int vepuserId = -1;
        			while(keyIter.hasNext() && valIter.hasNext())
        			{
        				String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("modifybox")) isModify = true;
        				if(key.equalsIgnoreCase("mapid")) mapId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("cloudtypeid")) cloudtypeId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("iaasuser")) iaasUser = value;
        				if(key.equalsIgnoreCase("iaaspass")) iaasPass = value;
        				if(key.equalsIgnoreCase("iaasuid")) iaasUid = value;
        				if(key.equalsIgnoreCase("requesttype")) requestType = value;
        				if(key.equalsIgnoreCase("vepuserid"))
        				{
        					try
        					{
        						vepuserId = Integer.parseInt(value.trim());
        					}
        					catch(Exception ex)
        					{
        						vepuserId = -1;
        						logger.warn("Exception caught while parsing the vep user account id.");
        					}
        				}
        			}
        			String message = "";
        			if(isModify && mapId == -1)
        			{
        				status = false;
        				message = "You must choose the cloud account entry to modify. ";
        			}
        			if(iaasUser == null || iaasPass == null || cloudtypeId == -1)
        			{
        				status = false;
        				message += "You must provide value for all the fields. No field can be left empty. ";
        			}
        			if(status)
        			{
        				iaasUser = iaasUser.trim();
        				iaasPass = iaasPass.trim();
        				if(iaasUid != null)
        					iaasUid = iaasUid.trim();
        				else
        					iaasUid = "";
            			if(iaasUser.length() == 0 || iaasPass.length() == 0)
            			{
            				status = false;
            				message += "You must provide value for all the fields. No field can be left empty. ";
            			}
        			}
        			if(status)
        			{
        				ResultSet rs;
        				if(isModify)
        				{
        					try
        					{
        						status = db.update("accountmap", "cloudtype=" + cloudtypeId + ", iaasuser='" + iaasUser + "', iaaspass='" + iaasPass + "', iaasuid='" + iaasUid + "', vepuser=" + vepuserId, "where id=" + mapId);
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while updating an existing cloud account mapping.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        				else
        				{
        					try
        					{
        						rs = db.query("select", "max(id)", "accountmap", "");
                                int id = 1;
                                if(rs.next()) id = rs.getInt(1) + 1;
                                rs.close();
                                status = db.insert("accountmap", "(" + id + ", " + cloudtypeId + ", '" + iaasUser + "', '" + iaasPass + "', '" + iaasUid + "', " + vepuserId + ")");
        					}
        					catch(Exception ex)
        					{
        						logger.warn("Exception caught while adding a new cloud account mapping.");
        						//if(logger.isDebugEnabled())
        						//	ex.printStackTrace(System.err);
        						logger.debug("Exception Caught: ", ex);
        						status = false;
        					}
        				}
        			}
        			if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='hidden' name='requesttype' value='").append(requestType).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='hidden' name='requesttype' value='").append(requestType).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
        		}
	        	else if(action.equalsIgnoreCase("automapaccount"))
	        	{
	        		int cloudId = -1;
	        		boolean status = false;
	        		String message = "";
	        		String[] fnMsg = new String[1];
	        		while(keyIter.hasNext() && valIter.hasNext())
        			{
	        			String key = keyIter.next();
        				String value = valIter.next();
        				if(key.equalsIgnoreCase("createmap")) cloudId = Integer.parseInt(value.trim());
        				if(key.equalsIgnoreCase("requesttype")) requestType = value;
        			}
	        		//now try to find a valid admin cloud account for the target iaas cloud to use for creating the user mapping
	        		if(cloudId != -1)
	        		{
	        			try
	        			{
	        				ResultSet rs = db.query("select", "id", "user", "WHERE role='administrator'");
	        				int counter=0;
	        				int vepUserId = -1;
        					String iaasUser = "";
        					String iaasPass = "";
	        				while(rs.next())
	        				{
	        					vepUserId = rs.getInt("id");
	        					counter++;
	        					rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + vepUserId + " AND cloudtype=" + cloudId);
	        					if(rs.next())
	        					{
	        						iaasUser = rs.getString("iaasuser");
	        						iaasPass = rs.getString("iaaspass");
	        						break;
	        					}
	        					rs.close();
	        					int count = 0;
	        					rs = db.query("select", "id", "user", "WHERE role='administrator'");
	        					while(count < counter) {rs.next(); count++;}
	        				}
	        				if(iaasUser.length() == 0 || iaasPass.length() == 0)
	        				{
	        					status = false;
	        				}
	        				else
	        				{
	        					//now find all desired parameters to pass to the mapping function
	        					//first replace the vepUserId with the current user account
	        					rs = db.query("select", "id", "user", "WHERE username='" + username + "'");
	        					if(rs.next())
	        						vepUserId = rs.getInt("id");
	        					else
	        						vepUserId = -1;
	        					
	        					String headIp = "";
	        					int headPort = -1;
	        					String version = "";
	        					int typeId = -1;
	        					String cloudName = "";
	        					rs = db.query("select", "*", "cloudtype", "where id=" + cloudId);
	        					if(rs.next())
	        					{
	        						headIp = rs.getString("headip");
	        						headPort = rs.getInt("headport");
	        						version = rs.getString("version");
	        						typeId = rs.getInt("typeid");
	        						rs = db.query("select", "name", "supportedcloud", "WHERE id=" + typeId);
	        						if(rs.next())
	        							cloudName = rs.getString("name");
	        						rs.close();
	        						status = doMapping(vepUserId, cloudId, cloudName, version, headIp, headPort, iaasUser, iaasPass, fnMsg);
	        					}
	        					else
	        					{
	        						status = false;
	        						message += "Inconsistent VEP database deteted. Contact administrator. ";
	        					}
	        					rs.close();
	        				}
	        			}
	        			catch(Exception ex)
	        			{
	        				logger.warn("Exception was caught while trying to map the user account.");
	        				//if(logger.isDebugEnabled())
	        				//	ex.printStackTrace(System.err);
	        				logger.debug("Exception Caught: ", ex);
	        			}
	        		}
	        		else
	        		{
	        			status = false;
	        			message += "You must select at least 1 cloud for automatic mapping. ";
	        		}
	        		if(status)
        	        {
        	        	stringBuilder.append("<font color='green'>The last action was successful!</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
							.append("<input type='hidden' name='username' value='").append(username).append("'>")
							.append("<input type='hidden' name='password' value='").append(password).append("'>")
							.append("<input type='hidden' name='requesttype' value='").append(requestType).append("'>")
							.append("<input type='submit' value='proceed'></form>");
        	        }
        	        else
        	        {
        	        	stringBuilder.append("<font color='red'>The last action was un-successful!</font><br><br>");
        	        	stringBuilder.append("<font color='orange'>Hint: ").append(message + "\n" + fnMsg[0]).append("</font><br><br>");
        	        	stringBuilder.append("<form name='goback' action='../dologin' method='post'>")
        					.append("<input type='hidden' name='username' value='").append(username).append("'>")
        					.append("<input type='hidden' name='password' value='").append(password).append("'>")
        					.append("<input type='hidden' name='requesttype' value='").append(requestType).append("'>")
        					.append("<input type='submit' value='proceed'></form>");
        	        }
	        	}
	        }
        	stringBuilder.append("</table>");
        }
        else
        {
        	 String str="";
        try {
            str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/wrongLogin.html"));
        } catch (IOException ex) {
            
        }
        
        
        //tringBuilder.append(VEPHelperMethods.getRESTwebFooter());
      /*  StringRepresentation value = new StringRepresentation(str, MediaType.TEXT_HTML);
            
            
                stringBuilder.append("<b>Welcome to VEP Application Management Tool</b><br>").append(username).append(", your login attempt <b>failed</b>!<hr>");
	        stringBuilder.append("<table><tr><td valign='top' width='790' style='font-family:Verdana; font-size:10pt;'>");
	        
	        stringBuilder.append("<font color='red'>Login Failed! Please try again.</font><br><br>");
	        
	        stringBuilder.append("<form name='reset' action='../' method='get' style='font-family:Verdana;font-size:8pt;'>");
	        stringBuilder.append("<table style='font-family:Verdana;font-size:8pt;background:#FFFFFF;'>");
	        stringBuilder.append("<tr><td align='left'><input type='submit' value='retry'>");
	        stringBuilder.append("</table></form>");
	        
	        stringBuilder.append("</table>");*/
                stringBuilder.append(str);
        }
        
        //stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
	
	public boolean doMapping(int vepUserId, int cloudId, String cloudName, String version, String headIp, int headPort, String iaasUser, String iaasPass, String[] message)
	{
		//iaasUser and iaasPass are administrator accounts using which a new user mapping is to be created and stored
		boolean status = false;
		message[0] = "";
		//make sure that a mapping does not already exist
		try
		{
			ResultSet rs = db.query("select", "count(*)", "accountmap", "WHERE vepuser=" + vepUserId + " AND cloudtype=" + cloudId);
			int count = 0;
			if(rs.next())
				count = rs.getInt(1);
			rs.close();
			if(count > 0)
			{
				status = false;
				message[0] += "Account mapping for the selected cloud type already exist. ";
			}
			else
			{
				//generating a random IaaS user ID
				int uname_size = 8;
				String tempUser = RandomStringUtils.randomAlphanumeric(uname_size);
				//now test to see if this is already in use
                boolean genNext = true;
                while(genNext)
                {
                    logger.info("Testing if iaas mapping by id " + tempUser + " already exists.");
                    rs = db.query("select", "*", "accountmap", "where iaasuser='" + tempUser + "' AND cloudtype=" + cloudId);
                    if(rs.next())
                    {
                        genNext = true;
                        logger.info("IaaS mapping by id " + tempUser + " already exists. Will try to create a new id.");
                    }
                    else
                    {
                        genNext = false;
                        logger.info("IaaS mapping by id " + tempUser + " does not exists. Will try to register with IaaS daemon.");
                    }
                    rs.close();
                    if(genNext)
                        tempUser = RandomStringUtils.randomAlphanumeric(uname_size);
                }
                //generating a random password for this account
                String tempPass = RandomStringUtils.randomAlphanumeric(32);
                int iaasId = -1;
				if(cloudName.equalsIgnoreCase("opennebula") && version.startsWith("2.2"))
				{
					try
					{
						One2XMLRPCHandler one2handle = new One2XMLRPCHandler(headIp, Integer.toString(headPort), iaasUser, iaasPass, "user account mapping");
						iaasId = one2handle.addUser(tempUser, VEPHelperMethods.makeSHA1Hash(tempPass));
					}
					catch(Exception ex)
					{
						iaasId = -1;
						message[0] += "Error connecting to the IaaS controller. ";
					}
				}
				else if(cloudName.equalsIgnoreCase("opennebula") && (version.startsWith("3.4") || version.startsWith("3.6")))
				{
					try
					{
						One3XMLRPCHandler one3handle = new One3XMLRPCHandler(headIp, Integer.toString(headPort), iaasUser, iaasPass, "user account mapping");
						iaasId = one3handle.addUser(tempUser, VEPHelperMethods.makeSHA1Hash(tempPass));
					}
					catch(Exception ex)
					{
						message[0] += "Error connecting to the IaaS controller. ";
						iaasId = -1;
					}
				}
				if(iaasId != -1)
                {
					rs = db.query("select", "max(id)", "accountmap", "");
                    int id = 1;
                    if(rs.next()) id = rs.getInt(1) + 1;
                    rs.close();
                    status = db.insert("accountmap", "(" + id + ", " + cloudId + ", '" + tempUser + "', '" + tempPass + "', '" + iaasId + "', " + vepUserId +  ")");
                }
				else
				{
					status = false;
				}
			}
		}
		catch(Exception ex)
		{
			logger.warn("Exception caught inside doMapping function.");
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception Caught: ", ex);
		}
		return status;
	}
}
