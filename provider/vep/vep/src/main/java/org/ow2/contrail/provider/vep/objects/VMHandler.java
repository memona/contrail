package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VMHandler extends VEPObject {
	
	private int cpu_l;
	private int cpu_h;
	private int ram_l;
	private int ram_h;
	private int corecount_l;
	private int corecount_h;
	private int disksize_l;
	private int disksize_h;
	
	private String vmHandlerId;
	private String ceeId;

	public VMHandler(boolean instantiate) {
		super("VMHandler", instantiate);
	}
	
	public VMHandler(String vmHandlerId) {
		super("VMHandler", true);
		this.vmHandlerId = vmHandlerId;
		this.setId("/vmhandler/"+vmHandlerId);
		this.setResourceUri("VEP/VMHandler");
	}
	
	public VMHandler(String ceeId, String vmHandlerId) {
		super("VMHandler", true);
		this.ceeId = ceeId; //not useful?
		this.vmHandlerId = vmHandlerId;
		this.setId("/cee/"+ceeId+"/vmhandler/"+vmHandlerId);
		this.setResourceUri("VEP/VMHandler");
	}
	
	public boolean retrieveVMHandler() throws SQLException
	{
		boolean success = false;
		ResultSet rs = db.query("select", "*", "vmhandler", "where id="+vmHandlerId);
		if (rs.next())
		{
			this.setName(rs.getString("name"));
			this.setCorecount_l(rs.getInt("corecount_low"));
			this.setCorecount_h(rs.getInt("corecount_high"));
			this.setDisksize_l(rs.getInt("disksize_low"));
			this.setDisksize_h(rs.getInt("corecount_high"));
			this.setCpu_l(rs.getInt("cpufreq_low"));
			this.setCpu_h(rs.getInt("cpufreq_high"));
			this.setRam_l(rs.getInt("ram_low"));
			this.setRam_h(rs.getInt("ram_high"));
			success = true;
		}
		return success;
	}
	
	public int getCpu_l() {
		return cpu_l;
	}

	public void setCpu_l(int cpu_l) {
		this.cpu_l = cpu_l;
	}

	public int getCpu_h() {
		return cpu_h;
	}

	public void setCpu_h(int cpu_h) {
		this.cpu_h = cpu_h;
	}

	public int getRam_l() {
		return ram_l;
	}

	public void setRam_l(int ram_l) {
		this.ram_l = ram_l;
	}

	public int getRam_h() {
		return ram_h;
	}

	public void setRam_h(int ram_h) {
		this.ram_h = ram_h;
	}

	public int getCorecount_l() {
		return corecount_l;
	}

	public void setCorecount_l(int corecount_l) {
		this.corecount_l = corecount_l;
	}

	public int getCorecount_h() {
		return corecount_h;
	}

	public void setCorecount_h(int corecount_h) {
		this.corecount_h = corecount_h;
	}

	public int getDisksize_l() {
		return disksize_l;
	}

	public void setDisksize_l(int disksize_l) {
		this.disksize_l = disksize_l;
	}

	public int getDisksize_h() {
		return disksize_h;
	}

	public void setDisksize_h(int disksize_h) {
		this.disksize_h = disksize_h;
	}

}
