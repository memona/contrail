package org.ow2.contrail.provider.vep;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class RestCIMIVMHandlerResource extends ServerResource 
{
	private Logger logger;
	private DBHandler db;
	
	public RestCIMIVMHandlerResource() {
		logger = Logger.getLogger("VEP.VMHandlerResource");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestCVMHandler", dbType);
	}
	
	@Get("json")
	public Representation getValue() throws ResourceException
	{
		Representation response = null;
		JSONObject json = new JSONObject();
		ResultSet rs;
		//TODO: No Certificate hack for testing
//		String userName = "ghostwheel";
		Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
		String userName = requestHeaders.getFirstValue("X-Username");
		String ceeid = ((String) getRequest().getAttributes().get("ceeid"));
		String vid = ((String) getRequest().getAttributes().get("vid"));
		try {
		if (ceeid != null) {
                                
				// returns data of a specific cee vmhandlers
				json = CIMIParser.VMHandlerCollectionRetrieve(userName, ceeid);		
                                
		}
		else {
			if(vid == null) {
			json = CIMIParser.VMHandlerCollectionRetrieve(userName);	
			} else {
				json = CIMIParser.VMHandlerRetrieve(userName, vid);
			}
		}
		} catch (SQLException e)
		{
			//TODO: add some errors to status and response if SQL request fails
			json.put("error", "Exception while retrieving VMHandlers");
			logger.debug("Error is:",e);
		}catch(UnauthorizedRestAccessException e){
                                         setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                                         return new StringRepresentation("{}", MediaType.APPLICATION_JSON);
                                    }
		
		response = new StringRepresentation(json.toJSONString(), MediaType.APPLICATION_JSON);
		return response;
	}
}
