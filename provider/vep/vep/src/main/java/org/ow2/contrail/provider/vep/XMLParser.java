package org.ow2.contrail.provider.vep;

import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.apache.log4j.Logger;

public class XMLParser extends DefaultHandler 
{
	private String inputValue;
	private int returnType;
	private Object returnObj;
	private String tempVal;
	private Logger logger;
	private OpenNebulaHost tempHost;
	private OpenNebulaNetwork tempNetwork;
	private OpenNebulaVM tempVM;
	private String cloudVersion;
	/**
	 * List of type values and parse object: (
	 * 1 = HOSTPOOL, 
	 * 2 = VIRTUALNETWORKPOOL
	 * 3 = VIRTUALMACHINE Information
	 * )
	 * @param input the input string to be parsed
	 * @param objectType the type of object to be returned after parsing
	 * @param retObj the object reference that will be filled and returned
	 * @param cloudVer cloud version number
	 */
	public XMLParser(String input, int objectType, Object retObj, String cloudVer)
	{
		inputValue = input;
		returnType = objectType;
		returnObj = retObj;
		tempVal = "";
		cloudVersion = cloudVer;
		logger = Logger.getLogger("VEP.XMLParser");
	}
	
	public Object parse()
	{
		logger.debug("Going to parse: " + inputValue);
		if(returnType == 1)
			returnObj = new LinkedList<OpenNebulaHost>();
		if(returnType == 2)
			returnObj = new LinkedList<OpenNebulaNetwork>();
		if(returnType == 3)
		{
			returnObj = new OpenNebulaVM();
			((OpenNebulaVM)returnObj).rawMsg = inputValue;
			//return returnObj;
		}
		SAXParserFactory spf = SAXParserFactory.newInstance();
        try
        {
            SAXParser sp = spf.newSAXParser();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(inputValue));
            sp.parse(is, this);
        }
        catch(SAXException se)
        {
            if(logger.isDebugEnabled())
                se.printStackTrace(System.err);
            else
                logger.warn(se.getMessage());
        }
        catch(ParserConfigurationException pce)
        {
            if(logger.isDebugEnabled())
                pce.printStackTrace(System.err);
            else
                logger.warn(pce.getMessage());
        }
        catch(IOException ie)
        {
            if(logger.isDebugEnabled())
                ie.printStackTrace(System.err);
            else
                logger.warn(ie.getMessage());
        }
        return returnObj;
	}
	
	@Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
    {
		if(qName.equalsIgnoreCase("HOST"))
        {
            tempHost = new OpenNebulaHost(); //blank it for new row
        }
		if(qName.equalsIgnoreCase("VNET"))
        {
            tempNetwork = new OpenNebulaNetwork(); //blank it for new row
        }
		if(qName.equalsIgnoreCase("VM"))
		{
			tempVM = new OpenNebulaVM();
			tempVM.rawMsg = inputValue;
		}
    }
	
	@Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {
		tempVal = new String(ch,start,length);
        if(tempVal.contains("CDATA"))
        {
            
        }
    }
	
	@SuppressWarnings("unchecked")
	@Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
		if(qName.equalsIgnoreCase("HOST"))
        {
			if(returnType == 1)
			{
				tempHost.oneVersion = cloudVersion;
				((LinkedList<OpenNebulaHost>)(returnObj)).add(tempHost);
				logger.debug("Found host: " + tempHost.hostName + " " + tempHost.id + " " + tempHost.cpuspeed);
			}
        }
		else if(qName.equalsIgnoreCase("VNET"))
        {
			if(returnType == 2)
			{
				tempNetwork.oneVersion = cloudVersion;
				((LinkedList<OpenNebulaNetwork>)(returnObj)).add(tempNetwork);
				logger.debug("Found network: " + tempNetwork.name + " " + tempNetwork.id + " " + tempNetwork.bridge);
			}
        }
        else if(qName.equalsIgnoreCase("ID"))
        {
        	if(returnType == 1)
        		tempHost.id = tempVal;
        	if(returnType == 2)
        		tempNetwork.id = tempVal;
        	if(returnType == 3)
        		tempVM.vmID = tempVal;
        }
        else if(qName.equalsIgnoreCase("NAME"))
        {
        	if(returnType == 1)
        		tempHost.name = tempVal;
        	if(returnType == 2)
        		tempNetwork.name = tempVal;
        	if(returnType == 3)
        		tempVM.name = tempVal;
        }
        else if(qName.equalsIgnoreCase("BRIDGE"))
        {
        	if(returnType == 2)
        		tempNetwork.bridge = tempVal;
        }
        else if(qName.equalsIgnoreCase("STATE"))
        {
        	if(returnType == 1)
        		tempHost.state = tempVal;
        	if(returnType == 3)
        		tempVM.state = tempVal;
        }
        else if(qName.equalsIgnoreCase("LCM_STATE"))
        {
        	if(returnType == 3)
        		tempVM.lcm_state = tempVal;
        }
        else if(qName.equalsIgnoreCase("NET_TX"))
        {
        	if(returnType == 3)
        		tempVM.net_tx = tempVal;
        }
        else if(qName.equalsIgnoreCase("NET_RX"))
        {
        	if(returnType == 3)
        		tempVM.net_rx = tempVal;
        }
        else if(qName.equalsIgnoreCase("LISTEN"))
        {
        	if(returnType == 3)
        		tempVM.graphics_host = tempVal;
        }
        else if(qName.equalsIgnoreCase("PORT"))
        {
        	if(returnType == 3)
        		tempVM.graphics_port = tempVal;
        }
        else if(qName.equalsIgnoreCase("TYPE"))
        {
        	if(returnType == 3)
        		tempVM.graphics_type = tempVal;
        }
        else if(qName.equalsIgnoreCase("STIME"))
        {
        	if(returnType == 3)
        		tempVM.start_time = tempVal;
        }
        else if(qName.equalsIgnoreCase("MAC"))
        {
        	if(returnType == 3)
        		tempVM.nic_mac = tempVal;
        }
        else if(qName.equalsIgnoreCase("IP"))
        {
        	if(returnType == 3)
        		tempVM.nic_ip = tempVal;
        }
        else if(qName.equalsIgnoreCase("IM_MAD"))
        {
        	if(returnType == 1)
        		tempHost.im_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("VM_MAD"))
        {
        	if(returnType == 1)
        		tempHost.vm_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("TM_MAD"))
        {
        	if(returnType == 1)
        		tempHost.tm_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("VN_MAD"))
        {
        	if(returnType == 1)
        		tempHost.vn_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("LAST_MON_TIME"))
        {
        	if(returnType == 1)
        		tempHost.mon_time = tempVal;
        }
        else if(qName.equalsIgnoreCase("CLUSTER"))
        {
        	if(returnType == 1)
        		tempHost.cluster = tempVal;
        }
        else if(qName.equalsIgnoreCase("FREE_MEM"))
        {
        	if(returnType == 1)
        		tempHost.free_mem = tempVal;
        }
        else if(qName.equalsIgnoreCase("USED_MEM"))
        {
        	if(returnType == 1)
        		tempHost.used_mem = tempVal;
        }
        else if(qName.equalsIgnoreCase("FREE_CPU"))
        {
        	if(returnType == 1)
        		tempHost.free_cpu = tempVal;
        }
        else if(qName.equalsIgnoreCase("USED_CPU"))
        {
        	if(returnType == 1)
        		tempHost.used_cpu = tempVal;
        }
        else if(qName.equalsIgnoreCase("CPUSPEED"))
        {
        	if(returnType == 1)
        		tempHost.cpuspeed = tempVal;
        }
        else if(qName.equalsIgnoreCase("HOSTNAME"))
        {
        	if(returnType == 1)
        		tempHost.hostName = tempVal;
        }
        else if(qName.equalsIgnoreCase("HYPERVISOR"))
        {
        	if(returnType == 1)
        		tempHost.hypervisor = tempVal;
        }
        else if(qName.equalsIgnoreCase("ARCH"))
        {
        	if(returnType == 1)
        		tempHost.arch = tempVal;
        }
        else if(qName.equalsIgnoreCase("TOTALMEMORY"))
        {
        	if(returnType == 1)
        		tempHost.max_mem = tempVal;
        }
        else if(qName.equalsIgnoreCase("TOTALCPU"))
        {
        	if(returnType == 1)
        		tempHost.number_cores = Integer.parseInt(tempVal) / 100;
        }
        else if(qName.equalsIgnoreCase("HOST_POOL"))
        {
            
        }
        else if(qName.equalsIgnoreCase("VM"))
        {
        	logger.debug("Finished parsing XML response for VM");
        	returnObj = tempVM;
        }
    }
}
