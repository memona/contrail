package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CEECollection extends VEPCollection{
	
	private ArrayList<CEE> cees = new ArrayList<CEE>();
	private String uid;
	
	
	public CEECollection(String uid) {
		super("CEECollection", true);
		this.uid = uid;
		this.setId("/user/"+uid+"/CEEs");
		this.setResourceUri("/VEP/CEECollection");
	}
	
	public void retrieveCEECollection() throws SQLException 
	{
		ResultSet rs;
		logger.debug("Retrieving CEEs for user" + uid);
		rs = db.query("select", "id, name", "cee", "where userid = " + uid);
		while (rs.next()){
			CEE c = new CEE(false);
			c.setName(rs.getString("name"));
			c.setId("/user/"+uid+"/cee/"+rs.getInt("id"));
			cees.add(c);
			this.incrementCount();
		}
	}

	public ArrayList<CEE> getCees() {
		return cees;
	}
	
}
