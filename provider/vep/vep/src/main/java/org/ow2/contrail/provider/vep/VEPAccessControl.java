package org.ow2.contrail.provider.vep;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 *
 * @author piyush
 */

public class VEPAccessControl 
{
    private static Logger logger;
    private static String[] adminGroups = {"admin", "cloudadministrator"};
    private static String[] localAdminGroups = {"admin"};
    
    VEPAccessControl()
    {
        logger = Logger.getLogger("VEP.AccessControl");
    }
    
    public static boolean isAdmin(String username) throws SQLException
    {
        DBHandler db = new DBHandler("VEPAccesControl", VEPHelperMethods.getProperty("vepdb.choice", logger)); 
        ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
        String[] groups = null;
        if(rs.next())
        {
            int uid = rs.getInt("uid");
            rs.close();
            rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
            String groupList = "";
            while(rs.next())
            {
                groupList += rs.getString("gname") + ",";
            }
            groups = groupList.split(","); //the last index will be empty because of the trailing ,
            logger.trace("GroupsList for user: " + username + " is: " + groupList);
        }
        return (belongsToAdminGroups(groups) || belongsToLocalAdminGroups(groups));
    }
    
    public static boolean isGroupMember(String user1, String user2)
    {
        return false;
    }
    
    public static boolean isLocalAdmin(String username) throws SQLException
    {
        DBHandler db = new DBHandler("VEPAccesControl", VEPHelperMethods.getProperty("vepdb.choice", logger)); 
        ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
        String[] groups = null;
        if(rs.next())
        {
            int uid = rs.getInt("uid");
            rs.close();
            rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
            String groupList = "";
            while(rs.next())
            {
                groupList += rs.getString("gname") + ",";
            }
            groups = groupList.split(","); //the last index will be empty because of the trailing ,
            logger.trace("GroupsList for user: " + username + " is: " + groupList);
        }
        return belongsToLocalAdminGroups(groups);
    }
    
    private static boolean belongsToAdminGroups(String[] list)
    {
        for(int i=0; list!=null && (i < list.length); i++)
        {
            for(int j=0; j < adminGroups.length; j++)
                if(list[i].equalsIgnoreCase(adminGroups[j]))
                    return true;
        }
        return false;
    }
    
    private static boolean belongsToLocalAdminGroups(String[] list)
    {
        for(int i=0; list!=null && (i < list.length); i++)
        {
            for(int j=0; j < localAdminGroups.length; j++)
                if(list[i].equalsIgnoreCase(localAdminGroups[j]))
                    return true;
        }
        return false;
    }
}
