/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep.SchedulerClient;

import org.ow2.contrail.provider.vep.DBHandler;
import org.ow2.contrail.provider.vep.VEPHelperMethods;
import org.ow2.contrail.provider.vep.objects.CEE;
import org.ow2.contrail.provider.vep.objects.CEE.ConstraintMap;
import org.ow2.contrail.provider.vep.objects.User;
import org.ow2.contrail.provider.vep.objects.VEPVirtualMachine;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.restlet.Client;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Preference;
import org.restlet.data.Protocol;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ClientResource;

/**
 *
 * @author fgaudenz
 */
public class SchedulerClient {

    public final static String CHECK = "check";
    public final static String ACTION = "action";

    public static void main(String[] args) throws IOException {
        
        
    deleteVM(96);
           
    /* Set<String> constraints=new HashSet<String>();
       constraints.add("elemento1");
       constraints.add("elemento2");
       constraints.add("elemento1");
       constraints.add("elemento4");
       constraints.add("elemento2");
       constraints.add("elemento3");
       constraints.add("elemento4");
       Iterator iterator = constraints.iterator();
        while(iterator.hasNext()){
         System.out.println((String) iterator.next());
        }
        /*  ArrayList<String> vmsConst=new ArrayList<String>();
        vmsConst.add("23");
        vmsConst.add("24");
        vmsConst.add("25");
        vmsConst.add("26");
        vmsConst.add("27");
        Constraint app;
        app=new Constraint("same rack","in",vmsConst);
       System.out.println(app.toJSON());*/
        /*
        List<VEPVirtualMachine> vmslot = new ArrayList<VEPVirtualMachine>();
        VEPVirtualMachine vepVM = new VEPVirtualMachine("83", "83", "85");
        vepVM.setCorecount(2);
        vepVM.setCpufreq(800);
        vepVM.setRam(512);
        vmslot.add(vepVM);

        vepVM = new VEPVirtualMachine("83", "83", "86");
        vepVM.setCorecount(2);
        vepVM.setCpufreq(1200);
        vepVM.setRam(512);
       
        vmslot.add(vepVM);
        SchedulerClient.deploy(vmslot);
        for(int i=0;i<vmslot.size();i++){
           System.out.println(vmslot.get(i).getVmId()+" - "+vmslot.get(i).getHostid());
        }
      */
        /*
        HashMap<VEPVirtualMachine, Integer> vmslot = new HashMap<VEPVirtualMachine, Integer>();
        VEPVirtualMachine vepVM = new VEPVirtualMachine("1", "1", "1");
        vepVM.setCorecount(2);
        vepVM.setCpufreq(800);
        vepVM.setRam(512);
        vmslot.put(vepVM, 1);
        

        vepVM = new VEPVirtualMachine("1", "2", "2");
        vepVM.setCorecount(2);
        vepVM.setCpufreq(800);
        vepVM.setRam(512);
        vmslot.put(vepVM, 1);
        
       HashMap<VEPVirtualMachine, Integer> vmslotCopy =  SchedulerClient.makeReservation(SchedulerClient.ACTION, 112, vmslot, new Date());
        for (Map.Entry<VEPVirtualMachine, Integer> entry : vmslotCopy.entrySet()) {
                    VEPVirtualMachine vm = entry.getKey();
                    System.out.println(vm.getVmId()+" ---- "+entry.getValue());
        }
        
        */
  /*     SchedulerClient.addDatacenter("1", "Datacenter-1", "FR");
        SchedulerClient.addCluster("1", "Cluster-1", "1");
        SchedulerClient.addRack("1","Rack-1","1","1");
        
        
//        
        SchedulerClient.addHost("1", "myriads-serv2.irisa.fr", "2261", "12", "48390","50000", "x86_64", "1","1" , "1", "1");
        SchedulerClient.addDatacenter("2", "Datacenter-2", "CN");
        SchedulerClient.addCluster("2", "Cluster-2", "2");
        SchedulerClient.addRack("2","Rack-1","2","2");
        SchedulerClient.addHost("2", "myriads-serv3.irisa.fr", "2260", "12", "48390","50000", "x86_64", "2","2" , "2", "2");
 */
        //id,name,cpuFreq,coreCount,ram,cpuArch,iaasId,idDatacenter,idCluster,idRack)
        
    }
    
     public static void deleteReservation(int id){
        SchedulerClient.delete("reservations/"+Integer.toString(id));
        
    }
    public static void deleteVM(int id){
        Logger logger;
        logger = Logger.getLogger("VEP.SchedulerClient");
        logger.debug("Request to delete VM:"+id);
        SchedulerClient.delete("deploy/vms/"+Integer.toString(id));
    }
    
    private static JSONObject GET(String url) {
        try {
           // Client client = new Client(Protocol.HTTP);
           // Request r = new Request();
            String urlScheduler = VEPHelperMethods.getProperty("scheduler.url", null);
            String portScheduler = VEPHelperMethods.getProperty("scheduler.port", null);
           // r.setResourceRef("http://localhost:8080/scheduler/datacenters");
          //  r.setResourceRef(urlScheduler+":"+portScheduler+"scheduler/"+url);
         //   r.setMethod(Method.GET);
            Client client = new Client(new Context(), Protocol.HTTP);
            ClientResource clientResource = new ClientResource(urlScheduler+":"+portScheduler+"/scheduler"+url);
            
            clientResource.setNext(client);
            
           Representation rep= clientResource.get();
            
        //    r.getClientInfo().getAcceptedMediaTypes().add(new Preference<MediaType>(MediaType.APPLICATION_JSON));
            //Representation rep=client.handle(r).getEntity();
             Object obj=JSONValue.parse(rep.getText());
            JSONObject jobj=(JSONObject)obj;
            return jobj;
        } catch (Exception ex) {
           
            //Logger.getLogger(SchedulerClient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static void addDatacenter(String id, String name, String country) {
        JSONObject obj = new JSONObject();
        obj.put("id", id);
        obj.put("name", name);
        obj.put("country", country);
        System.out.println(obj);
        post(obj, "datacenters/");
    }

    public static boolean deleteDatacenter(String id) {
        return delete("datacenters/" + id);
    }

    public static void addCluster(String id, String name, String idDatacenter) {
        JSONObject obj = new JSONObject();
        obj.put("id", id);
        obj.put("name", name);
        obj.put("interconnect", "2");
        post(obj, "datacenters/" + idDatacenter + "/clusters/");
    }

    public static boolean deleteCluster(String idDatacenter, String id) {
        return delete("datacenters/" + idDatacenter + "/clusters/" + id);
    }

    public static void addRack(String id, String name, String idDatacenter, String idCluster) {
        JSONObject obj = new JSONObject();
        obj.put("id", id);
        obj.put("name", name);
        post(obj, "datacenters/" + idDatacenter + "/clusters/" + idCluster + "/racks/");
    }

    public static boolean deleteRack(String idDatacenter, String idCluster, String id) {
        return delete("datacenters/" + idDatacenter + "/clusters/" + idCluster + "/racks/" + id);
    }

    public static void addHost(String id, String name, String cpuFreq, String coreCount, String ram, String diskSize, String cpuArch, String iaasId, String idDatacenter, String idCluster, String idRack) {
        JSONObject obj = new JSONObject();
        obj.put("id", id);
        obj.put("name", name);
        obj.put("cpuFreq", cpuFreq);
        obj.put("coreCount", coreCount);
        obj.put("ram", ram);
        obj.put("cpuArch", cpuArch);
        obj.put("iaasId", iaasId);
        obj.put("diskSize", diskSize);
        post(obj, "datacenters/" + idDatacenter + "/clusters/" + idCluster + "/racks/" + idRack + "/hosts/");
    }

    public static boolean deleteHost(String idDatacenter, String idCluster, String idRack, String id) {
        return delete("datacenters/" + idDatacenter + "/clusters/" + idCluster + "/racks/" + idRack + "/hosts/" + id);
    }

    private static boolean delete(String resource) {
         String urlScheduler = VEPHelperMethods.getProperty("scheduler.url", null);
         String portScheduler = VEPHelperMethods.getProperty("scheduler.port", null);
        
        
        
             Client client = new Client(Protocol.HTTP);
            Request r = new Request();
            r.setResourceRef(urlScheduler+":"+portScheduler+"/scheduler/"+resource);
            //http://localhost:8080/scheduler/"+resource);
            r.setMethod(Method.DELETE);
            client.handle(r).getEntity();
        
        
        
        /*   
        ClientResource itemsResource = new ClientResource(
                "http://myriads-serv2.irisa.fr:8080/scheduler/" + resource);
        ClientResource itemResource = null;
        
        //System.out.println( "http://myriads-serv2.irisa.fr:8080/scheduler/" + resource);
        // Create a new item
        //System.out.println(obj.toJSONString());
        //StringRepresentation stringRep = new StringRepresentation(obj.toJSONString());
        //stringRep.setMediaType(MediaType.APPLICATION_JSON);
        try{
        Representation r = itemsResource.delete();
        //Representation r = itemsResource.get();
        if (itemsResource.getStatus().isSuccess()) {
            return true;
        } else {
            return false;
        }
        }catch(Exception e){
             Logger logger;
             logger = Logger.getLogger("VEP.SchedulerClient");
             logger.error(e+":http://myriads-serv2.irisa.fr:8080/scheduler/" + resource);
            return false;
        }*/
        return true;
    }

    private static String post(JSONObject obj, String resource) {
        String urlScheduler = VEPHelperMethods.getProperty("scheduler.url", null);
        String portScheduler = VEPHelperMethods.getProperty("scheduler.port", null);
    	Client client = new Client(new Context(), Protocol.HTTP);
        ClientResource itemsResource = new ClientResource(
                urlScheduler+":"+portScheduler+"/scheduler/" + resource);
        itemsResource.setNext(client);
        ClientResource itemResource = null;
        // Create a new item
        Logger logger;
        logger = Logger.getLogger("VEP.SchedulerClient");
        logger.debug("POST TO SCHEDULER:"+obj.toJSONString());
        //.out.println(obj.toJSONString());
        StringRepresentation stringRep = new StringRepresentation(obj.toJSONString());
        stringRep.setMediaType(MediaType.APPLICATION_JSON);
        Representation r = itemsResource.post(stringRep);
        //Representation r = itemsResource.get();
        if (itemsResource.getStatus().isSuccess()) {
            try {
                String result = r.getText();
                //System.out.println("RESULT:"+result);
                return result;
                //itemResource = new ClientResource(r.getIdentifier());
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
        return null;
    }

    public static void deploy(VEPVirtualMachine vmToDeploy)
    {
    	ArrayList<VEPVirtualMachine> vmsToDeploy = new ArrayList<VEPVirtualMachine>();
    	vmsToDeploy.add(vmToDeploy);
    	deploy(vmsToDeploy,null);
    }
    
    public static void deploy(List<VEPVirtualMachine> vmsToDeploy,List<Constraint> constraints) {
        Logger logger;
        logger = Logger.getLogger("VEP.SchedulerClient");
        /*
         * {
         "VMs":[
         {"id":"4",
         "nCpu":"2",
         "fCpu":"800",
         "mem":"512",
         "disk":"1",
         "reservation_CEE":"1",
         "reservation_id":"1"
         },
         {"id":"5",
         "nCpu":"1",
         "fCpu":"1200",
         "mem":"256",
         "disk":"1",
         "reservation_CEE":"1",
         "reservation_id":"2"
         }
         ]
         }
         */
        JSONObject post = new JSONObject();
        JSONArray vms = new JSONArray();
        for (int i = 0; i < vmsToDeploy.size(); i++) {
            JSONObject obj = new JSONObject();
            VEPVirtualMachine vm = vmsToDeploy.get(i);
            obj.put("id", vm.getVmId());
            //obj.put("name",);
            obj.put("fCpu", vm.getCpufreq());
            obj.put("nCpu", vm.getCorecount());
            obj.put("mem", vm.getRam());
            //obj.put("", cpuArch);
            obj.put("disk", 1);
            if(vm.reservationId!=0){
                obj.put("reservation_CEE",vm.getApplicationId());
                obj.put("reservation_id", vm.reservationId);
            }
            vms.add(obj);
        }
        
        post.put("VMs", vms);
        
        logger.debug("constraints:"+constraints);
        if(constraints!=null){
            logger.debug("constraints.size:"+constraints);
         JSONArray cntr=new JSONArray();
         for(Constraint cc:constraints){
             logger.debug(cc.toJSON());
            cntr.add(cc.toJSON());
        }
        post.put("constraints", cntr); 
        }
        
        
        //System.out.println(post);
        logger.debug("POST TO SCHEDULER:"+post);
        String response=post(post, "deploy");
        if(response!=null){
            JSONArray res = (JSONArray) JSONValue.parse(response);

            for (Object objFor : res) {
                JSONObject singleRes = (JSONObject) objFor;
                if(singleRes.get("host")!=null){
                String uriVM =   singleRes.get("vm").toString();
               
                 String[] vm=uriVM.split("/");
                String vmID=vm[vm.length-1];
                
                String uriHOST = singleRes.get("host").toString();
                String[] host=uriHOST.split("/");
                String hostID=host[host.length-1];
                
                for(int i=0;i<vmsToDeploy.size();i++){
                    VEPVirtualMachine vmApp=vmsToDeploy.get(i);
                    if(vmApp.getVmId().compareTo(vmID)==0)
                        vmApp.setHostid(Integer.parseInt(hostID));
                }
                }
            }
            
        }
    }

    /*public static Representation getRepresentation() {
     // Gathering informations into a Web form.
     Form form = new Form();
     form.add("id", "12");
     form.add("name", "newData");
     form.add("country", "FR");
     return form.getWebRepresentation();
     }*/
    
    
    
     public static HashMap<VEPVirtualMachine, Integer> makeReservation(String status, HashMap<VEPVirtualMachine, Integer> vmslot, int reservationID,List<Constraint> constraints) throws java.text.ParseException {
         JSONObject post = new JSONObject();
        JSONArray vms = new JSONArray();
        Logger logger;
        logger = Logger.getLogger("VEP.SchedulerClient");
       
        HashMap<VEPVirtualMachine, Integer> vmslotCopy = new HashMap<VEPVirtualMachine, Integer>();
        for (Map.Entry<VEPVirtualMachine, Integer> entry : vmslot.entrySet()) {
           
            //System.out.println(entry.getKey().getId() + "=" + entry.getValue()); // Change this to whatever you need    
            VEPVirtualMachine vm = entry.getKey();
            logger.debug("RESERVATION FOR: VMSLOT ID:"+vm.getVmId()+" - "+entry.getValue());
            vmslotCopy.put(vm, 0);
            JSONObject obj = new JSONObject();
          /* if (status.compareTo(SchedulerClient.CHECK) == 0) //If there is a reservationId (such a for an ACTION reservation), use it, otherwise use the vm id (only used for a CHECK)
            	obj.put("id",-1 ); 
                 // obj.put("id", vm.getOvfmapid()); 
            else*/
            obj.put("id", vm.reservationId);
            //obj.put("name",);
            obj.put("fCpu", vm.getCpufreq());
            obj.put("nCpu", vm.getCorecount());
            obj.put("mem", vm.getRam());
            //obj.put("", cpuArch);
            obj.put("diskSize", 1);
            obj.put("count", entry.getValue());
            obj.put("startdate", DateUtils.serialize(new Date()));
            String[] date=vm.endDateOfReservation.split("/");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date d = sdf.parse(vm.endDateOfReservation);
            obj.put("enddate", DateUtils.serialize(d));
            vms.add(obj);
        }
         
        post.put("id", reservationID);
        //post.put("enddate", DateUtils.serialize(endDate));
        //post.put("startdate", DateUtils.serialize(new Date()));
        post.put("reservations", vms);
        
        
         logger.debug("constraints:"+constraints);
        if(constraints!=null){
            logger.debug("constraints.size:"+constraints);
         JSONArray cntr=new JSONArray();
         for(Constraint cc:constraints){
             logger.debug(cc.toJSON());
            cntr.add(cc.toJSON());
        }
        post.put("constraints", cntr); 
        }
        
        
       // post.put("constraints",new JSONArray());
        // System.out.println(post);
        String response = null;
        if (status.compareTo(SchedulerClient.ACTION) == 0) {
            response = post(post, "reservations/");
        }
        if (status.compareTo(SchedulerClient.CHECK) == 0) {
            response = post(post, "reservations/check/");
        }
       
      
        //post(post, "reservations/");
        //HashMap<VEPVirtualMachine, Integer> result = 
        if (response != null) {
            JSONObject jObj = (JSONObject) JSONValue.parse(response);
            JSONArray res = (JSONArray) jObj.get("reservations");

            for (Object objFor : res) {
                JSONObject singleRes = (JSONObject) objFor;
                
                String idVS =   singleRes.get("id").toString();
                String slotReserved = singleRes.get("count").toString();
                for (Map.Entry<VEPVirtualMachine, Integer> entry : vmslotCopy.entrySet()) {
                    VEPVirtualMachine vm = entry.getKey();
                    if(vm.reservationId != 0)
                    {
                    	 if (vm.reservationId == Integer.parseInt(idVS)) {
                             entry.setValue(Integer.parseInt(slotReserved));
                         }
                    } else {
                    	 if (vm.getVmId().compareTo(idVS) == 0) {
                             entry.setValue(Integer.parseInt(slotReserved));
                         }
                    }
                }
            }
            
        }
        logger.info("SCEHDULER RESERVE:"+ vmslot.size() + " "+post);
        return vmslotCopy;
        
     }
     
     
     /**
      * make reservation with constraints
      * @param status
      * @param reservationID
      * @param vmslot
      * @param endDate
      * @param cntrs
      * @return
      * @throws java.text.ParseException 
      */
     public static HashMap<VEPVirtualMachine, Integer> makeReservation(String status, int reservationID, HashMap<VEPVirtualMachine, Integer> vmslot, Date endDate,List<Constraint> constraints) throws java.text.ParseException {
         JSONObject post = new JSONObject();
        JSONArray vms = new JSONArray();
        Logger logger;
        logger = Logger.getLogger("VEP.SchedulerClient");
       
        HashMap<VEPVirtualMachine, Integer> vmslotCopy = new HashMap<VEPVirtualMachine, Integer>();
        for (Map.Entry<VEPVirtualMachine, Integer> entry : vmslot.entrySet()) {
           
            //System.out.println(entry.getKey().getId() + "=" + entry.getValue()); // Change this to whatever you need    
            VEPVirtualMachine vm = entry.getKey();
            logger.debug("RESERVATION FOR: VMSLOT ID:"+vm.getVmId()+" - "+entry.getValue());
            vmslotCopy.put(vm, 0);
            JSONObject obj = new JSONObject();
            if(vm.reservationId != 0)//If there is a reservationId (such a for an ACTION reservation), use it, otherwise use the vm id (only used for a CHECK)
            	obj.put("id", vm.reservationId); 
                 // obj.put("id", vm.getOvfmapid()); 
            else
            	obj.put("id", vm.getVmId());
            //obj.put("name",);
            obj.put("fCpu", vm.getCpufreq());
            obj.put("nCpu", vm.getCorecount());
            obj.put("mem", vm.getRam());
            //obj.put("", cpuArch);
            obj.put("diskSize", 1);
            obj.put("count", entry.getValue());
            
             obj.put("startdate", DateUtils.serialize(new Date()));
             Date d;
             if(vm.endDateOfReservation!=null){
            String[] date=vm.endDateOfReservation.split("/");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            d = sdf.parse(vm.endDateOfReservation);
             }else{
                 d=endDate;
             }
            obj.put("enddate", DateUtils.serialize(d));
            
            
            
            vms.add(obj);
        }
        post.put("id", reservationID);
        
        post.put("reservations", vms);
        
        logger.debug("constraints:"+constraints);
        if(constraints!=null){
            logger.debug("constraints.size:"+constraints);
         JSONArray cntr=new JSONArray();
         for(Constraint cc:constraints){
             logger.debug(cc.toJSON());
            cntr.add(cc.toJSON());
        }
        post.put("constraints", cntr); 
        }
        
        
        //post.put("constraints",new JSONArray());
        // System.out.println(post);
        String response = null;
        if (status.compareTo(SchedulerClient.ACTION) == 0) {
            response = post(post, "reservations/");
        }
        if (status.compareTo(SchedulerClient.CHECK) == 0) {
            response = post(post, "reservations/check/");
        }
       
      
        //post(post, "reservations/");
        //HashMap<VEPVirtualMachine, Integer> result = 
        if (response != null) {
            JSONObject jObj = (JSONObject) JSONValue.parse(response);
            JSONArray res = (JSONArray) jObj.get("reservations");

            for (Object objFor : res) {
                JSONObject singleRes = (JSONObject) objFor;
                
                String idVS =   singleRes.get("id").toString();
                String slotReserved = singleRes.get("count").toString();
                for (Map.Entry<VEPVirtualMachine, Integer> entry : vmslotCopy.entrySet()) {
                    VEPVirtualMachine vm = entry.getKey();
                    if(vm.reservationId != 0)
                    {
                    	 if (vm.reservationId == Integer.parseInt(idVS)) {
                             entry.setValue(Integer.parseInt(slotReserved));
                         }
                    } else {
                    	 if (vm.getVmId().compareTo(idVS) == 0) {
                             entry.setValue(Integer.parseInt(slotReserved));
                         }
                    }
                }
            }
            
        }
        return vmslotCopy;
     }
     
         static public HashMap<VEPVirtualMachine,Integer> getReservationInfo(String reservationID){
        //need a get on the Scheduler 
             if(reservationID!=null){
         HashMap<VEPVirtualMachine, Integer> result = new HashMap<VEPVirtualMachine, Integer>();
         JSONObject reservationJ=SchedulerClient.GET("/reservations/"+reservationID);
         if(reservationJ!=null){
         JSONArray singleReservationJ=(JSONArray)reservationJ.get("reservations");
         for(Object obJ:singleReservationJ){
             JSONObject rsJ=(JSONObject)obJ;
             VEPVirtualMachine vs = new VEPVirtualMachine();
//rsJ.get(""))
             vs.countToDeploy=((Long)rsJ.get("count")).intValue();
             vs.setCpufreq(((Long)rsJ.get("fCpu")).intValue());
             vs.setCorecount(((Long)rsJ.get("nCpu")).intValue());
             vs.setRam(((Long)rsJ.get("mem")).intValue());
             vs.endDateOfReservation=(String)rsJ.get("endate");
             vs.reservationId=((Long)rsJ.get("id")).intValue();
             Integer free=((Long)rsJ.get("free")).intValue();
             result.put(vs, free);
                     }
         return result;
         }else return null;
             }
             return null;
    }
     
     
     
     
     
     
     
    
    public static HashMap<VEPVirtualMachine, Integer> makeReservation(String status, int reservationID, HashMap<VEPVirtualMachine, Integer> vmslot, Date endDate) throws java.text.ParseException {
//{
//    "id": 512,
//    "startdate": "2013-04-30T15:52:48.149+0200",
//    "enddate": "2013-04-30T15:52:47.880+0200",
//    "reservations": [
//        {
//        NEW IDEA: "virtualSystemNAME
//            "id": "501",
//            "fCpu": 800,
//            "mem": 512,
//            "nCpu": 2,
//            "diskSize": 1,
//            "count":2
//        },
//        {
//            "id": "502",
//            "fCpu": 800,
//            "mem": 512,
//            "nCpu": 2,
//            "diskSize": 1,
//           "count":1
//
//        }
//    ]
//}
        JSONObject post = new JSONObject();
        JSONArray vms = new JSONArray();
        Logger logger;
        logger = Logger.getLogger("VEP.SchedulerClient");
       
        HashMap<VEPVirtualMachine, Integer> vmslotCopy = new HashMap<VEPVirtualMachine, Integer>();
        for (Map.Entry<VEPVirtualMachine, Integer> entry : vmslot.entrySet()) {
           
            //System.out.println(entry.getKey().getId() + "=" + entry.getValue()); // Change this to whatever you need    
            VEPVirtualMachine vm = entry.getKey();
            logger.debug("RESERVATION FOR: VMSLOT ID:"+vm.getVmId()+" - "+entry.getValue());
            vmslotCopy.put(vm, 0);
            JSONObject obj = new JSONObject();
            if(vm.reservationId != 0)//If there is a reservationId (such a for an ACTION reservation), use it, otherwise use the vm id (only used for a CHECK)
            	obj.put("id", vm.reservationId); 
                 // obj.put("id", vm.getOvfmapid()); 
            else
            	obj.put("id", vm.getVmId());
            //obj.put("name",);
            obj.put("fCpu", vm.getCpufreq());
            obj.put("nCpu", vm.getCorecount());
            obj.put("mem", vm.getRam());
            //obj.put("", cpuArch);
            obj.put("diskSize", 1);
            obj.put("count", entry.getValue());
            
             obj.put("startdate", DateUtils.serialize(new Date()));
             Date d;
             if(vm.endDateOfReservation!=null){
            String[] date=vm.endDateOfReservation.split("/");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            d = sdf.parse(vm.endDateOfReservation);
             }else{
                 d=endDate;
             }
            obj.put("enddate", DateUtils.serialize(d));
            
            
            
            vms.add(obj);
        }
        post.put("id", reservationID);
        
        post.put("reservations", vms);
        post.put("constraints",new JSONArray());
        // System.out.println(post);
        String response = null;
        if (status.compareTo(SchedulerClient.ACTION) == 0) {
            response = post(post, "reservations/");
        }
        if (status.compareTo(SchedulerClient.CHECK) == 0) {
            response = post(post, "reservations/check/");
        }
       
      
        //post(post, "reservations/");
        //HashMap<VEPVirtualMachine, Integer> result = 
        if (response != null) {
            JSONObject jObj = (JSONObject) JSONValue.parse(response);
            JSONArray res = (JSONArray) jObj.get("reservations");

            for (Object objFor : res) {
                JSONObject singleRes = (JSONObject) objFor;
                
                String idVS =   singleRes.get("id").toString();
                String slotReserved = singleRes.get("count").toString();
                for (Map.Entry<VEPVirtualMachine, Integer> entry : vmslotCopy.entrySet()) {
                    VEPVirtualMachine vm = entry.getKey();
                    if(vm.reservationId != 0)
                    {
                    	 if (vm.reservationId == Integer.parseInt(idVS)) {
                             entry.setValue(Integer.parseInt(slotReserved));
                         }
                    } else {
                    	 if (vm.getVmId().compareTo(idVS) == 0) {
                             entry.setValue(Integer.parseInt(slotReserved));
                         }
                    }
                }
            }
            
        }
        return vmslotCopy;
    }
    
   
     static public void addCloud(String id,String name,String version, String supportedCloud){
         JSONObject obj = new JSONObject();
        obj.put("id", id);
        obj.put("name", name);
        obj.put("version", version);
        obj.put("supportedCloud", name);
        //System.out.println(supportedCloud);
        //post(obj, "datacenters/");
     }

    
    public static List<org.ow2.contrail.provider.vep.SchedulerClient.Constraint> toSchedulerConstraints(List<CEE.ConstraintMap> cntrMap) {
            ArrayList<org.ow2.contrail.provider.vep.SchedulerClient.Constraint> result=new ArrayList<org.ow2.contrail.provider.vep.SchedulerClient.Constraint> ();
            for(CEE.ConstraintMap cc:cntrMap){
                 
                 String descp;
                
                    descp = cc.descp;
                  
                    
                    org.ow2.contrail.provider.vep.SchedulerClient.Constraint app=new ConstraintTranslator().createConstraintFromVEP(descp,cc.param,cc.on);
                    result.add(app);
                    
                
                 
                 
            }
            
            
            return result;
        }

    /*public static Constraint toSchedulerReservationConstraints(ArrayList<String> constraints) {
        
        int i;
        i=0;
        return null;
    }*/

  

    public static List<Constraint> toSchedulerReservationConstraints(ArrayList<ConstraintMap> cntrMap, HashMap<VEPVirtualMachine, Integer> map) throws SQLException {
        ArrayList<org.ow2.contrail.provider.vep.SchedulerClient.Constraint> result = new ArrayList<org.ow2.contrail.provider.vep.SchedulerClient.Constraint>();
        if(cntrMap!=null) for (CEE.ConstraintMap cc : cntrMap) {
            ArrayList<String> onTranslate = new ArrayList<String>();            
            for (Map.Entry<VEPVirtualMachine, Integer> entry : map.entrySet()) {
                //System.out.println(entry.getKey().getId() + "=" + entry.getValue()); // Change this to whatever you need    
                VEPVirtualMachine vm = entry.getKey();
                int k = 0;
                while ((k < cc.on.size())
                        && (vm.getOvfmapname().compareTo(cc.on.get(k)) != 0)) {
                    k++;                    
                }
                if (k < cc.on.size()) {
                     if(vm.reservationId != 0) {
                        onTranslate.add(String.valueOf(vm.reservationId));
                    }else {
                        onTranslate.add(vm.getVmId());
                    }
                }
            }
            
            
            String descp;
            
           
           String dbType = VEPHelperMethods.getProperty("vepdbVEPHelper.choice", null);
            DBHandler db = new DBHandler("SchedulerClient", dbType);
            ResultSet rs = db.query("select", "id,descp", "constraints", "where id="+cc.id);
            descp = rs.getString("descp");
            if(onTranslate.size()!=0){
                org.ow2.contrail.provider.vep.SchedulerClient.Constraint app = new ConstraintTranslator().createConstraintFromVEP(descp, cc.param, onTranslate);
            result.add(app);
            }
            
            
            
        }
        
        
        return result;
    }


        
    
    
  
     

    
    
    
    
        
}
