package org.ow2.contrail.provider.vep.objects;

public abstract class VEPCollection extends VEPObject{

	public VEPCollection(String name, boolean instantiate) {
		super(name, instantiate);
	}
	private int count = 0;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}	
	public void incrementCount() {
		count ++;
	}
}
