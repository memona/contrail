package org.ow2.contrail.provider.vep;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.restlet.data.Form;

public class RestRootResource extends ServerResource
{
    private Logger logger;
    public RestRootResource() {
        logger = org.apache.log4j.Logger.getLogger("VEP.RestUserDoLogin");
    }
    
    
    
    
	@Get
	public Representation getValue() throws ResourceException
	{
		Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String acceptType = requestHeaders.getFirstValue("Accept");
        Representation response = null;
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml();
            else if(acceptType.contains("json"))
            {
                //response = toJson();
            }
        }
        else
        {
            //default rendering ...
            response = toHtml();
        }
        return response;
	}
	
	public Representation toHtml() throws ResourceException 
    {
        StringBuilder stringBuilder = new StringBuilder();
       /* stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true, true, false));
        
        stringBuilder.append("<div style='font-family:Verdana;font-size:9pt;color:navy;padding:5px;'>");
        stringBuilder.append("Welcome to Contrail Virtual Execution Platform access point. Before you can proceed, you must login.<br>");
        stringBuilder.append("<form name='login-form' method='post' action='./dologin' style='font-family:Verdana;font-size:9pt;'>");
        stringBuilder.append("<table style='font-family:Verdana;font-size:9pt;color:navy;padding:5px;'>").append("<tr><td>Username<td><input name='username' type='text' size='20'>")
        	.append("<tr><td>Password<td><input name='password' type='password' size='20'><tr><td><td align='right'><input type='submit' value='login'></form>")
        	.append("</table>If you do not have an account, you can create one <a href='./newaccount'>here</a>.");
        stringBuilder.append("</div>");*/
        String str="";
        try {
           
           InputStream  myInputStream =this.getClass().getClassLoader().getResourceAsStream("login.html");  
           
           String myString = IOUtils.toString(myInputStream, "UTF-8");
           str=myString;
           // str = FileUtils.readFileToString(new File("/var/lib/outsideTester/contrail1/homeVEP/login.html"));
        } catch (IOException ex) {
            
        }
        logger.info(VEPHelperMethods.getProperty("webuser-interface.defaultHost", logger));
        str=str.replace("%WEBUSERHOST%", VEPHelperMethods.getProperty("webuser-interface.defaultHost", logger));
        str=str.replace("%WEBUSERPORT%", VEPHelperMethods.getProperty("webuser-interface.port", logger));
        //tringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(str, MediaType.TEXT_HTML);
        return value;
    }
}
