package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ConstraintCollection extends VEPCollection {

	private String ceeId;
	private ArrayList<Constraint> constraints = new ArrayList<Constraint>();
	
	public ConstraintCollection(String ceeId)
	{
		super("ConstraintCollection", true);
		this.ceeId = ceeId;
	}
	
	public void retrieveConstraintCollection() throws SQLException
	{
		ResultSet rs;
		logger.debug("Retrieving Constraints for cee " + ceeId);
		ArrayList<Integer> constraintsCEE = new ArrayList<Integer>();
		rs = db.query("select", "id", "ceeconstraints", "where ceeid = " + ceeId);
		while (rs.next()){
			constraintsCEE.add(rs.getInt("id"));
		}
		//avoid nested rs
		for(int ceeconstraintsid : constraintsCEE)
		{
			rs = db.query("select", "id", "ceeconstraintslist", "where ceeconstraintsid = " + ceeconstraintsid);
			while (rs.next()){
				Constraint c = new Constraint(false);
				c.setName("N/A");
				c.setId("/cee/" + ceeId + "/constraint/" + rs.getInt("id"));
				constraints.add(c);
				this.incrementCount();
			}
		}
	}

	public ArrayList<Constraint> getConstraints() {
		return constraints;
	}
	
}
