package org.ow2.contrail.provider.vep.objects;

import org.apache.log4j.Logger;

import org.ow2.contrail.provider.vep.DBHandler;
import org.ow2.contrail.provider.vep.VEPHelperMethods;

abstract class VEPObject {

	private String id;
	private String resourceUri;
	private String name;
	protected Logger logger;
	protected DBHandler db;
	private String error;
	
	public VEPObject(String name, boolean instantiate)
	{
		//only create the handlers if they are needed, not if they are not required and we only want an object without doing db requests
		if(instantiate)
		{
			logger = Logger.getLogger("VEP."+name);
			db = new DBHandler(name, VEPHelperMethods.getProperty("vepdb.choice", logger));
		}
	}
	
	public void instantiate(){
		if (logger == null) logger = Logger.getLogger("VEP."+name);
		if(db == null) db = new DBHandler(name, VEPHelperMethods.getProperty("vepdb.choice", logger));
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = "/api/cimi"+id;
	}

	public String getResourceUri() {
		return resourceUri;
	}
	public void setResourceUri(String resourceUri) {
		this.resourceUri = resourceUri;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public boolean isError(){
		return this.error == null ? false : true;
	}
}
