package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class NetHandlerCollection extends VEPCollection {

	private String ceeId;
	private ArrayList<NetHandler> neths = new ArrayList<NetHandler>();
	
	public NetHandlerCollection(String ceeId)
	{
		super("NetHandlerCollection", true);
		this.ceeId = ceeId;
		this.setId("/cee/"+ceeId+"/networkHandlers");
		this.setResourceUri("VEP/NetworkHandlerCollection");
	}
	
	public NetHandlerCollection()
	{
		super("NetHandlerCollection", true);
		this.setId("/networkHandlers");
		this.setResourceUri("VEP/NetHandlerCollection");
	}
	
	public void retrieveNetHandlerCollection() throws SQLException
	{
		ResultSet rs;
		if(ceeId != null)
		{
			logger.debug("Retrieving Network Handlers for cee " + ceeId);
			rs = db.query("select", "vnethandler.id as vid,vnethandler.name as vname", "ceenethandlers,vnethandler", "where ceeid = " + ceeId + " and vnethandler.id=ceenethandlers.vnethandlerid");
			while (rs.next()){
				NetHandler n = new NetHandler(false);
				n.setName(rs.getString("vname"));
				n.setId("/networkHandler/" + rs.getInt("vid"));
				neths.add(n);
				this.incrementCount();
			}
		}
		else 
		{
			logger.debug("Retrieving Network Handler");
			rs = db.query("select", "id, name", "vnethandler", "");
			while (rs.next()){
				NetHandler n = new NetHandler(false);
				n.setName(rs.getString("name"));
				n.setId("/networkHandler/" + rs.getInt("id"));
				neths.add(n);
				this.incrementCount();
			}
		}
		
	}

	public ArrayList<NetHandler> getNeths() {
		return neths;
	}
	
}
