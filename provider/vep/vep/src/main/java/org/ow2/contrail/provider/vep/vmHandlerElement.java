package org.ow2.contrail.provider.vep;

public class vmHandlerElement 
{
	public int ceeVMHandlerId;
	public int cpufreq_low;
	public int cpufreq_high;
	public int ram_low;
	public int ram_high;
	public int corecount_low;
	public int corecount_high;
	public int disksize_low;
	public int disksize_high;
	public int score;
	
	public vmHandlerElement()
	{
		score = 0;
		disksize_high = 0;
		disksize_low = 0;
		corecount_high = 0;
		corecount_low = 0;
		ram_high = 0;
		ram_low = 0;
		cpufreq_high = 0;
		cpufreq_low = 0;
		ceeVMHandlerId = -1;
	}
}
