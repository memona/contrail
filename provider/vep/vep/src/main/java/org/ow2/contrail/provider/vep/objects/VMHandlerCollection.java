package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class VMHandlerCollection extends VEPCollection {
	
	private String ceeId;
	private ArrayList<VMHandler> vmhs = new ArrayList<VMHandler>();
	
	public VMHandlerCollection(String ceeId)
	{
		super("VMHandlerCollection", true);
		this.setId("/cee/"+ceeId+"/VMHandlers");
		this.setResourceUri("VEP/VMHandlerCollection");
		this.ceeId = ceeId;
	}
	
	public VMHandlerCollection() {
		super("VMHandlerCollection", true);
		this.setId("/VMHandlers");
		this.setResourceUri("VEP/VMHandlerCollection");
	}
	
	public void retrieveVMHandlerCollection() throws SQLException
	{
		ResultSet rs;
		if(ceeId != null)
		{
			logger.debug("Retrieving VM Handlers for cee " + ceeId);
			rs = db.query("select", "vmhandler.id as vid, vmhandler.name as vname", "ceevmhandlers,vmhandler", "where ceevmhandlers.ceeid = " + ceeId + " and vmhandler.id=ceevmhandlers.vmhandlerid");
			while (rs.next()){
				VMHandler v = new VMHandler(false);
				v.setName(rs.getString("vname"));
				v.setId("/VMHandler/" + rs.getInt("vid"));
				vmhs.add(v);
				this.incrementCount();
			}
		} else {
			logger.debug("Retrieving VM Handlers");
			rs = db.query("select", "id, name", "vmhandler", "");
			while (rs.next()){
				VMHandler v = new VMHandler(false);
				v.setName(rs.getString("name"));
				v.setId("/VMHandler/" + rs.getInt("id"));
				vmhs.add(v);
				this.incrementCount();
			}
		}
		int k;
	}
	


	public ArrayList<VMHandler> getVmhs() {
		return vmhs;
	}
}
