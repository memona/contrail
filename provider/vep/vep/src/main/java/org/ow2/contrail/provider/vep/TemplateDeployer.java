package org.ow2.contrail.provider.vep;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.ow2.contrail.provider.vep.openstack.OpenStackConnector;



/**
*
* @author florian
*/
public class TemplateDeployer {

	private int typeId;
	private String cloudVersion;
	private String username;
	private String password;
	private String headip;
	private int headport;
	private String template;
	private int hostId;
	private String adminIaaSPassword;
	private String tenantName;
	private String tenantId;
	private String adminIaaSUname;
	private Integer vcpu;
	private Integer ram;
	private HashMap<String, String> osDisk;
	private String keyStoneUserAPI;
	private String keyStoneAdminAPI;
	private HashMap<String, Object> params;
	private String region;
	private String hostName;
	private final String defaultScript = "#!/bin/bash\n" +
			"privateKey=\"USERPRIVKEY\"\n" +
			"publicKey=\"USERPUBKEY\"\n" +
			"gafs=\"GAFSURL\"\n" +
			"echo \"$privateKey\">/tmp/cert.key\n" +
			"echo \"$publicKey\">/tmp/cert.pem\n" +
			"echo $gafs>/tmp/gafs.test";
                        /*
        
        
         "if [ -n \"$gafs\" ]; then\n"+

            "if [ ! -d /mnt/gafs ]; then\n"+
                "mkdir /mnt/gafs\n"+
            "fi\n"+
            "mount.xtreemfs --pem-certificate-file-path /tmp/cert.pem --pem-private-key-file-path /tmp/cert.key $gafs /mnt/gafs\n"+
        "fi\n"+

        "";
        
        
        */
        
        
        
        
        
        
	
	/**
	 * 
	 * @param template
	 * @param hostId IaasId of the host, not VEP host id
	 * @param cloudSpecs
	 * @throws Exception
	 */
	public TemplateDeployer(String template, int hostId, Map<String, Object> cloudSpecs) throws Exception
	{
		try {
			this.template = template;
			typeId = (Integer)cloudSpecs.get("typeId");
			username = (String)cloudSpecs.get("username"); //user
			password = (String)cloudSpecs.get("password"); //user
                        adminIaaSUname = (String)cloudSpecs.get("adminIaaSUname");
                        adminIaaSPassword = (String)cloudSpecs.get("adminIaaSPassword");
			this.hostId = hostId;
			cloudVersion = (String)cloudSpecs.get("cloudVersion");
			headip = (String)cloudSpecs.get("headip");
			headport = (Integer)cloudSpecs.get("headport");		
		} catch (Exception e) 
		{
			e.printStackTrace(System.err);
			throw new Exception("Missing or Invalid informations needed to deploy a template");
		}
	}
	
	public TemplateDeployer(String hostName, HashMap<String, Object> cloudSpecs, HashMap<String, Object> params) throws Exception {
		try {
			typeId = (Integer)cloudSpecs.get("typeId");
			username = (String)cloudSpecs.get("username"); //user
			password = (String)cloudSpecs.get("password"); //user
			this.hostName = hostName;
			adminIaaSPassword = (String)cloudSpecs.get("adminIaaSPassword");
			tenantName = (String)cloudSpecs.get("tenantname");
			tenantId = (String)cloudSpecs.get("tenantid");
			adminIaaSUname = (String)cloudSpecs.get("adminIaaSUname");
			String keystoneUserIp = (String)cloudSpecs.get("keystoneuserip");
			String keystoneAdminIp = (String)cloudSpecs.get("keystoneadminip");
			String keystoneUserPort = (String)cloudSpecs.get("keystoneuserport");
			String keystoneAdminPort = (String)cloudSpecs.get("keystoneadminport");
			keyStoneUserAPI = keystoneUserIp+":"+keystoneUserPort+"/v2.0/"; 
			keyStoneAdminAPI = keystoneAdminIp+":"+keystoneAdminPort+"/v2.0/";
			region = (String)cloudSpecs.get("region");
			
			
			this.params = params;
		} catch (Exception e) 
		{
			e.printStackTrace(System.err);
			throw new Exception("Missing or Invalid informations needed to deploy a template");
		}
	}


	public Object deployTemplate() throws Exception
	{
		try {
			switch(typeId){
			case App.OPENNEBULA3_HOST:
				int onevmid = -1;
				onevmid = deployOnOpenNebula();
				return onevmid;
			case App.OPENSTACK_HOST:
				String openstackid = deployOnOpenStack();
				return openstackid;
			default:
				throw new Exception("Unknown or Invalid cloud Type");
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
		
	}

	private String deployOnOpenStack() throws IOException, ParseException, Exception {
		
		OpenStackConnector osc = new OpenStackConnector(username,tenantName, password,
				keyStoneUserAPI, adminIaaSUname, tenantName, adminIaaSPassword,
				keyStoneAdminAPI, "VMCreate");
		String publicKey = (String)params.get("publicKey");
		String privateKey = (String)params.get("privateKey");
		String gafs = null;
		JSONObject metadata = new JSONObject();
		String context = (String)params.get("context");
		String[] contextKeyVals = context.split(",");
		for(String kv : contextKeyVals)
		{
			String[] kvTab = kv.split("=");
			if(kvTab.length == 2)
			{
				String key = kvTab[0].trim();
				String val = kvTab[1].trim();
				val = val.substring(1, val.length()-1);
				if(key.equals("GAFS_VOLUME_URL"))
					gafs = val;
				else
				{
					metadata.put(key, val);
				}
			}
		}
		String userdata = null;
		if(publicKey != null && privateKey != null)
		{
			userdata = defaultScript.replace("USERPUBKEY", publicKey).replace("USERPRIVKEY", privateKey);
			if(gafs != null)
			{
				userdata.replace("GAFSURL", gafs);
			}
		}
		
		String adminSSH = VEPHelperMethods.getProperty("vm.insertadminkey", null);
		boolean adminAuth = false;
		String adminKeyName = null;
		String adminKeyValue = null;
		if(adminSSH != null && Boolean.parseBoolean(adminSSH)==true)
		{
			adminKeyName=VEPHelperMethods.getProperty("vm.adminkeyname", null);
			adminKeyValue=VEPHelperMethods.getProperty("vm.adminkeyvalue", null);
			if(adminKeyName != null && adminKeyValue != null)
			{
				adminAuth = true;
			}
		}
		String vmid;
		if(adminAuth)
		{
			vmid = osc.addVM((String)params.get("name"), ((HashMap<String,String>)params.get("osDisk")).get("id"), 
					region, 0, 0, (Integer)params.get("ram"), (Integer)params.get("cores"), hostName,
					new LinkedList<String>(),new HashMap<String,String>(),adminKeyName, adminKeyValue,userdata,metadata);
		} else {
		
			vmid = osc.addVM((String)params.get("name"), ((HashMap<String,String>)params.get("osDisk")).get("id"), 
				region, 0, 0, (Integer)params.get("ram"), (Integer)params.get("cores"), hostName,
				new LinkedList<String>(),new HashMap<String,String>(),null, null,userdata,metadata);
		}
		
		return vmid;
	}

	private int deployOnOpenNebula() throws Exception{
		int onevmid = -1;
		if(cloudVersion.startsWith("2")){
			throw new Exception("This cloud version is not supported yet");
		} else if(cloudVersion.startsWith("3")){
			One3XMLRPCHandler one3 = new One3XMLRPCHandler(headip, String.valueOf(headport), username, password, "restAction:DeployTemplate");
			try {
				//deploys (to running state) but still gives an error user unable to perform admin vm. //TODO:Find the reason, in the meantime, give user admin vm rights through acl
				onevmid = one3.addVM(template);
				if(onevmid == -1)
					throw new Exception("Unable to add VM on opennebula");
				boolean status = one3.deployVM(onevmid, hostId);
				if(!status)
					throw new Exception("Unable to deploy VM on opennebula");
			} catch (Exception e){
				throw new Exception("Unable to connect to OpenNebula "+e.getMessage());
			}
		}
		return onevmid;
	}
}
