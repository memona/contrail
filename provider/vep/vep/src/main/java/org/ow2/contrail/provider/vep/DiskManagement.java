package org.ow2.contrail.provider.vep;

import org.ow2.contrail.provider.vep.ImageCopyServer.ImageDownloader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

public class DiskManagement implements Runnable
{
	private Thread t;
	private int ceeId;
	private String pName;
	private boolean isRunning;
	private Logger logger;
	private DBHandler db;
	private ResultSet rs;
	private String username;
            
	public DiskManagement(int ceeid, String processname, String user)
	{
		ceeId = ceeid;
		pName = processname;
		username = user;
		isRunning = false;
		t = new Thread(this);
		logger = Logger.getLogger("VEP.DiskManagement");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("DiskManagement", dbType);
	}
	
	public String getProcessName()
	{
		return pName;
	}
	
	public boolean getRunningState()
	{
		return isRunning;
	}
	
	public void start()
	{
		try
		{
			t.start();
		}
		catch(Exception ex)
		{
			logger.info("Exception caught inside disk-management thread, process name: " + pName);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
		}
	}

	public void run() 
	{
		logger.info("Starting the disk management thread - process name: " + pName);
		isRunning = true;
		try
		{
			rs = db.query("select", "*", "osdisk", "WHERE ceeid=" + ceeId + " AND (status='INI' OR status='ERR')");
			int count=0;
			while(rs.next())
			{
				count++;
				int osDiskId = rs.getInt("id");
				String source = rs.getString("ovfimagepath");
				
				String dest = VEPHelperMethods.getProperty("vep.scratch", logger) + System.getProperty("file.separator") + username + "-" + ceeId + "@" + rs.getString("ovfimagename") + ".img";
				
				//start the copy part
				try
				{
					/*String copyServerIp = VEPHelperMethods.getProperty("copyserver.ip", logger);
					int copyServerPort = Integer.parseInt(VEPHelperMethods.getProperty("copyserver.port", logger));
					Socket sock = new Socket(copyServerIp, copyServerPort);
					PrintWriter out = new PrintWriter(sock.getOutputStream(), true);
					BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
					out.println("hello");
					String response = in.readLine();*/
					
                                       ImageDownloader imgD=new ImageDownloader();
                                       String result=imgD.manageImage(source, dest); 
                                       db.update("osdisk", "status='CPY'", "WHERE id=" + osDiskId);
                                       if(result==null){
                                           //error
                                           db.update("osdisk", "status='ERR'" , "WHERE id=" + osDiskId);
                                       }else{ 
                                       
                                            //success
                                           db.update("osdisk", "status='RDY' , localpath='" + result+"'", "WHERE id=" + osDiskId);
                                       
                                       }
                                        
                                        
                                        
                                        
                                        
                                        //old comunication
                                        /*if(response.equalsIgnoreCase("hello"))
					{
						out.println(VEPHelperMethods.getProperty("copyserver.admin", logger));
						response = in.readLine();
						if(response.equalsIgnoreCase("ok"))
						{
							out.println(VEPHelperMethods.getProperty("copyserver.password", logger));
							response = in.readLine();
							if(response.equalsIgnoreCase("ok"))
							{
								out.println("SRC=" + source + " USR=" + username);
								response = in.readLine();
								if(response.startsWith("ok "))
								{
									out.println("DST=" + dest);
									response = in.readLine();
									if(response.startsWith("ok "))
									{
										db.update("osdisk", "status='RDY'", "WHERE id=" + osDiskId);
										response = in.readLine();
										if(response.equalsIgnoreCase("quit"))
										{
											try
											{
												sock.close();
											}
											catch(Exception ex)
											{
												
											}
										}
									}
									else
									{
										db.update("osdisk", "status='ERR'", "WHERE id=" + osDiskId);
									}
								}
								else
								{
									db.update("osdisk", "status='ERR'", "WHERE id=" + osDiskId);
								}
							}
							else
							{
								db.update("osdisk", "status='ERR'", "WHERE id=" + osDiskId);
							}
						}
						else
						{
							db.update("osdisk", "status='ERR'", "WHERE id=" + osDiskId);
						}
					}
					else
					{
						db.update("osdisk", "status='ERR'", "WHERE id=" + osDiskId);
					}*/
				}
				catch(Exception ex)
				{
					logger.info("Exception caught while trying to copy remote file from source=" + source + " to destination=" + dest);
					//if(logger.isDebugEnabled())
					//	ex.printStackTrace(System.err);
					logger.debug("Exception caught ", ex);
					db.update("osdisk", "status='ERR'", "WHERE id=" + osDiskId);
				}
				rs = db.query("select", "*", "osdisk", "WHERE ceeid=" + ceeId + " AND status='INI'");
			}
		}
		catch(Exception ex)
		{
			logger.info("Exception caught inside disk-management thread, process name: " + pName);
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception caught ", ex);
		}
		logger.info("Terminating the disk management thread - process name: " + pName);
		isRunning = false;
	}

}
