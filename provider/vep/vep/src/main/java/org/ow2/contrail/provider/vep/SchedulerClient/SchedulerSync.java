/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep.SchedulerClient;

import org.ow2.contrail.provider.vep.DBHandler;
import org.ow2.contrail.provider.vep.VEPHelperMethods;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author fgaudenz
 */
public class SchedulerSync {

    static public void syncScheduler() {
        DBHandler db;
        ResultSet rs;
        Logger logger = Logger.getLogger("VEP.SchedulerSyng");
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("SchedulerSync", dbType);
        System.out.println("SYNC WITH SCHEDULER");
        /*rs=db.query("select", "*", "cloudtype", "");
        try {
        while (rs.next()) {
              SchedulerClient.addCloud(rs.getString("id"),rs.getString("name"),rs.getString("version"),rs.getString("typeid"));
            }*/
        
        try {
        
        
        
        
        
        
        rs = db.query("select", "datacenter.name as dname,datacenter.id as did,countrylist.code as ccode ", "datacenter,countrylist", "where datacenter.countryid=countrylist.id");
        ArrayList<String> datacenters = new ArrayList<String>();
        
            while (rs.next()) {
                String name = rs.getString("dname");
                String id = rs.getString("did");
                String countryid = rs.getString("ccode");
                SchedulerClient.addDatacenter(id, name, countryid);
                datacenters.add(id);
            }
            for (int i = 0; i < datacenters.size(); i++) {
                ArrayList<String> clusters = new ArrayList<String>();
                rs = db.query("select", "*", "cluster", "where datacenterid="+datacenters.get(i));
                while (rs.next()) {
                    String name = rs.getString("name");
                    String id = rs.getString("id");
                    SchedulerClient.addCluster(id, name, datacenters.get(i));
                    clusters.add(id);
                }
                for(int j=0;j<clusters.size();j++){
                    ArrayList<String> racks = new ArrayList<String>();
                    rs = db.query("select", "*", "rack", "where clusterid="+clusters.get(j));
                    while (rs.next()) {
                    String name = rs.getString("name");
                    String id = rs.getString("id");
                    SchedulerClient.addRack(id, name, datacenters.get(i), clusters.get(j));
                    racks.add(id);
                    }
                    for(int k=0;k<racks.size();k++){ 
                      
                    rs = db.query("select", "*", "host", "where rackid="+racks.get(k));
                    while (rs.next()) {
                    String id = rs.getString("id");
                    String cpuFreq = rs.getString("cpufreq");
                    String coreCount=rs.getString("corecount");
                    /*rs.getString("ram");
                    rs.getString("disksize");
                    rs.getString("cpuArch");
                    rs.getString("");*/
                    SchedulerClient.addHost(id, rs.getString("hostname"), cpuFreq, coreCount, rs.getString("ram"), rs.getString("disksize"),  rs.getString("cpuArch"), rs.getString("iaasid"), datacenters.get(i), clusters.get(j), racks.get(k));
                    //racks.add(id);
                    System.out.println("Syncronization DONE");
                   }
                }
            }
          }
        } catch (SQLException ex) {
            logger.error(ex);
            ex.printStackTrace();
        }

    }
}
