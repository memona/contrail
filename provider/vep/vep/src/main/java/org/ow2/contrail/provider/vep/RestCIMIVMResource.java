package org.ow2.contrail.provider.vep;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

import org.ow2.contrail.provider.vep.objects.VEPVirtualMachine;

public class RestCIMIVMResource extends ServerResource {
	private Logger logger;
	private DBHandler db;

	public RestCIMIVMResource() {
		logger = Logger.getLogger("VEP.VMResource");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
		db = new DBHandler("RestVMResource", dbType);
	}


	@Delete
	public Representation deleteValue() throws ResourceException
	{
		Representation response = null;
		JSONObject json = new JSONObject();
		ResultSet rs;

		Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
		String userName = requestHeaders.getFirstValue("X-Username");

		//Authentication
		boolean auth = false;
		String requestor = VEPHelperMethods.checkCert((List) getRequest().getAttributes().get("org.restlet.https.clientCertificates"));
		if (requestor.equalsIgnoreCase(userName)) {
			rs = db.query("select", "id", "user", "where username='" + requestor + "'");
			try {
				if (rs.next()) {
					auth = true;
				}
				rs.close();
			} catch (SQLException ex) {
				logger.error("error in user retrieval for" + userName);
				//return not allowed
			}
		} else {
			String role = "";
			rs = db.query("select", "role", "user", "where username='" + requestor + "'");
			try {
				if (rs.next()) {
					role = rs.getString("role");
				}
				rs.close();
				if (role.equalsIgnoreCase("administrator")) {
					auth=true; 
				}
			} catch (SQLException ex) {
				logger.error("error in user retrieval for" + userName);
				//return not allowed
			}
		}
		if(!auth)
		{
			logger.warn("user without privilegies");
			setStatus(Status.CLIENT_ERROR_FORBIDDEN);
			return new StringRepresentation("{}",MediaType.APPLICATION_JSON);
		}

		String ceeid = ((String) getRequest().getAttributes().get("ceeid"));
		String appid = ((String) getRequest().getAttributes().get("applicationid"));
		String vmid = ((String) getRequest().getAttributes().get("vmid"));

		try {
			if (ceeid != null && appid != null && vmid != null) {
				VEPVirtualMachine v = new VEPVirtualMachine(ceeid, appid, vmid);
				v.retrieveVirtualMachine();
				v.delete();
				if(v.isError())
				{
					setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED);
					json.put("error", v.getError());
				} 
			} else
			{
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				json.put("error", "CEE, Application and VM ids must be present.");
			}
		} catch (SQLException e)
		{
			//TODO: add some errors to status and response if SQL request fails
		}
		response = new StringRepresentation(json.toJSONString(), MediaType.APPLICATION_JSON);
		return response;
	}

	@Get("json")
	public Representation getValue() throws ResourceException
	{
		Representation response = null;
		JSONObject json = new JSONObject();
		ResultSet rs;
		//TODO: No Certificate hack for testing
		//		String userName = "ghostwheel";
		Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
		String userName = requestHeaders.getFirstValue("X-Username");

		//Authentication
		boolean auth = false;
		String requestor = VEPHelperMethods.checkCert((List) getRequest().getAttributes().get("org.restlet.https.clientCertificates"));
		if (requestor.equalsIgnoreCase(userName)) {
			rs = db.query("select", "id", "user", "where username='" + requestor + "'");
			try {
				if (rs.next()) {
					auth = true;
				}
				rs.close();
			} catch (SQLException ex) {
				logger.error("error in user creation for" + userName);
						//return not allowed
			}
		} else {
			String role = "";
			rs = db.query("select", "role", "user", "where username='" + requestor + "'");
			try {
				if (rs.next()) {
					role = rs.getString("role");
				}
				rs.close();
				if (role.equalsIgnoreCase("administrator")) {
					auth=true; 
				}
			} catch (SQLException ex) {
				logger.error("error in user creation for" + userName);
				//return not allowed
			}
		}
		if(!auth)
		{
			logger.warn("user without privilegies");
			setStatus(Status.CLIENT_ERROR_FORBIDDEN);
			return new StringRepresentation("{}",MediaType.APPLICATION_JSON);
		}

		String ceeid = ((String) getRequest().getAttributes().get("ceeid"));
		String appid = ((String) getRequest().getAttributes().get("applicationid"));
		String vmid = ((String) getRequest().getAttributes().get("vmid"));

		try {
			if (ceeid != null && appid != null) {
				if(vmid == null)
				{
					// returns data of a specific application's vms
					//json = CIMIParser.VMCollectionRetrieve(userName, ceeid, appid);
					setStatus(Status.SERVER_ERROR_NOT_IMPLEMENTED);
				} else {
					try {
						json = CIMIParser.VMRetrieve(userName, ceeid, appid, vmid);
					} catch(UnauthorizedRestAccessException e){
						setStatus(Status.CLIENT_ERROR_FORBIDDEN);
						return new StringRepresentation("{}", MediaType.APPLICATION_JSON);
					}
				}
			}
			else {
				//error, request malformed
			}
		} catch (SQLException e)
		{
			//TODO: add some errors to status and response if SQL request fails
		}
		response = new StringRepresentation(json.toJSONString(), MediaType.APPLICATION_JSON);
		return response;
	}

}
