package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import org.ow2.contrail.provider.vep.DBHandler;
import org.ow2.contrail.provider.vep.VEPHelperMethods;

public class Authenticator {
	
	private static Logger logger = Logger.getLogger("VEP.Authenticator");;
	private static DBHandler db = new DBHandler("User", VEPHelperMethods.getProperty("vepdb.choice", logger));

	public static boolean checkResourceAccess(String requesterUname, String resourceId, String resourceType) throws SQLException {
		boolean access = false;
		
		if(resourceType.equalsIgnoreCase("CEE"))
		{
			access = checkCEEAccess(requesterUname, resourceId);
		}else{
                   if(resourceType.equalsIgnoreCase("User")){
                       access=true;
                   }else{
                       if(resourceType.equalsIgnoreCase("ProviderHandlersConstraints"))
                           access=true;
                   }    
                }
                //logger.info("ACCESS:"+access);
		return access;
		//return true;//TODO:Test Hack
	}

	private static boolean checkCEEAccess(String requesterUname, String resourceId) throws SQLException {
		boolean access = false;
		//retrieve userid and role
		ResultSet rs = db.query("select", "id, role", "user", "where username = '"+requesterUname+"'");
		if (rs.next())
		{
			
			int requesterId = rs.getInt("id");
			String role = rs.getString("role");
			
			if(resourceId == null)
			{
				//resource creation is ok for all users
				access = true;
			} else
			{
				//Check if user is an admin
				if (role.contains("administrator")) {
					access = true;
				} else {
					//If not, check if user is owner of CEE
					rs = db.query("select", "userid", "cee", "where id = "+resourceId);
					int ceeUserId = rs.getInt("userid");
					if (requesterId == ceeUserId)
					{
						access = true;
					} else {
						//If not check permission (later)
					}
				}	
			}	
		}	
		return access;
	}

	/**
	 * used to auth users to do get/post/put queries on specific resources
	 * Example : authorize(username, resource)
	 */
	
}
