package org.ow2.contrail.provider.vep;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import org.ow2.contrail.provider.vep.objects.Application;

public class RestCIMIApplicationAction extends ServerResource{

	private Logger logger;
	private DBHandler db;
	
	public RestCIMIApplicationAction() {
		logger = Logger.getLogger("VEP.ApplicationAction");
		String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
        db = new DBHandler("RestApplicationAction", dbType);
	}
	
	@Post
	public Representation doAction()
	{
		Representation response = null;
		JSONObject json = new JSONObject();
//		String userName = "ghostwheel";
		Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
		String userName = requestHeaders.getFirstValue("X-Username");
		String ceeid = ((String) getRequest().getAttributes().get("ceeid"));
		String appid = ((String) getRequest().getAttributes().get("applicationid"));
		String action = ((String) getRequest().getAttributes().get("action"));
		try
		{
			if(ceeid != null && appid != null && action != null)
			{
				Application a = CIMIParser.appCreate(userName, null, ceeid, appid);
				if (a!=null)
				{
					a.doAction(action);
					if(!a.isError())
					{
						setStatus(Status.SUCCESS_OK);
					} else {
						json.put("error", a.getError());
						setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED);
					}
				} else {
					setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				}
			} else {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			}
		} catch (Exception e)
		{
			logger.debug("SQLException: ",e);
			setStatus(Status.SERVER_ERROR_INTERNAL);
		}
		
		response = new StringRepresentation(json.toJSONString(), MediaType.APPLICATION_JSON);
		return response;
	}
	
	
}
