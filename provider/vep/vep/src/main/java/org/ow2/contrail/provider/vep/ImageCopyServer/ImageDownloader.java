/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep.ImageCopyServer;

import org.ow2.contrail.provider.vep.VEPHelperMethods;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author fgaudenz
 */
public class ImageDownloader {
    
    private Logger logger;
    public ImageDownloader(){
        logger = Logger.getLogger("VEP.DiskManagement");
    }
     
    public String manageImage(String source,String destination){
        String[] ctrl = source.split("http://");
        //String destination;
        if (ctrl.length > 1) {
            try {
                destination=download(source,destination);
                return destination;
            } catch (Exception ex) {
                logger.error("Impossible to download the image");
                return null;
            }
        }else{
            if(checkImage(source))
            return source;
            else return null;
        }
    }
    
      private String download(String source, String destination) throws MalformedURLException, FileNotFoundException, IOException {
       
              String[] ctrl = source.split("http://");
        //ftp??
        
        if (ctrl.length > 1) {
            logger.debug("Image from http ->"+source);
            URL u;
            InputStream is = null;
            BufferedInputStream bis;
            String s;
            u = new URL(source);

            //----------------------------------------------//
            // Step 3:  Open an input stream from the url.  //
            //----------------------------------------------//

            is = u.openStream();         // throws an IOException

            //-------------------------------------------------------------//
            // Step 4:                                                     //
            //-------------------------------------------------------------//
            // Convert the InputStream to a buffered DataInputStream.      //
            // Buffering the stream makes the reading faster; the          //
            // readLine() method of the DataInputStream makes the reading  //
            // easier.                                                     //
            //-------------------------------------------------------------//

            bis = new BufferedInputStream(is);

            //------------------------------------------------------------//
            // Step 5:                                                    //
            //-----------------------------------------------------//
            // Now just read each record of the input stream, and print   //
            // it out.  Note that it's assumed that this problem is run   //
            // from a command-line, not from an application or applet.    //
            //------------------------------------------------------------//

            FileOutputStream fout = new FileOutputStream(destination);

            byte data[] = new byte[1024];
            int count;
            while ((count = bis.read(data, 0, 1024)) != -1) {
                fout.write(data, 0, count);
            }
            bis.close();
            fout.close();


            logger.info("Image Downloading done,  source:"+source+" destination:"+destination);
            return destination;
        }
         return null;
           
            

    }

    private boolean checkImage(String source) {
        String folderP=VEPHelperMethods.getProperty("imageRepositoryDefault.path", logger);
        ArrayList<String> checker=new ArrayList<String>();
                
                
   File folder=new File(folderP);
   for (File fileEntry : folder.listFiles()) {
        if (!fileEntry.isDirectory());  
            if(source.equals(fileEntry.getAbsolutePath()))
                return true;
        }
    return false;
    }
    
    
    
}
