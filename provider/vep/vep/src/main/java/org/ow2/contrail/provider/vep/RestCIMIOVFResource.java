package org.ow2.contrail.provider.vep;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class RestCIMIOVFResource extends ServerResource {
		
		private Logger logger;
		private DBHandler db;
		
		public RestCIMIOVFResource() {
			logger = Logger.getLogger("VEP.OVFResource");
			String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger);
	        db = new DBHandler("RestOVFResource", dbType);
		}
		
		@Get("xml")
		public Representation getValue() throws ResourceException
		{
			Representation response = null;
			String ovf = "";
			ResultSet rs;
			Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
			String userName = requestHeaders.getFirstValue("X-Username");
			
			String ceeid = ((String) getRequest().getAttributes().get("ceeid"));
			String appid = ((String) getRequest().getAttributes().get("applicationid"));
			String ovfid = ((String) getRequest().getAttributes().get("ovfid"));
			
			try {
				if (ceeid != null && appid != null && ovfid != null) {
						ovf = CIMIParser.OVFRetrieve(userName, ceeid, appid, ovfid);
				}
				else {
					this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				}
			} catch (SQLException e)
			{
				this.setStatus(Status.SERVER_ERROR_INTERNAL);
				//TODO: add some errors to status and response if SQL request fails
			}catch(UnauthorizedRestAccessException e){
                                         setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                                         return new StringRepresentation("{}", MediaType.APPLICATION_JSON);
                                    }
			
			response = new StringRepresentation(ovf, MediaType.APPLICATION_XML);
			return response;
		}
}
