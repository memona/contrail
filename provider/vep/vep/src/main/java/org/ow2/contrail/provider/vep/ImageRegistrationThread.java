package org.ow2.contrail.provider.vep;

import java.io.IOException;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.jclouds.openstack.glance.v1_0.domain.ContainerFormat;
import org.jclouds.openstack.glance.v1_0.domain.DiskFormat;
import org.json.simple.parser.ParseException;

import org.ow2.contrail.provider.vep.openstack.OpenStackConnector;


public class ImageRegistrationThread {
//extends Thread{

	private OpenStackConnector osc;
	private String name;
	private DiskFormat diskFormat;
	private String region;
	private ContainerFormat bare;
	private String imagePath;
	private Logger logger;
	private DBHandler db;
	private String imageName;
	private int cloudtypeId;
	private String ceeId;
	private int diskID;
	private String vmId;


	public ImageRegistrationThread(OpenStackConnector osc, String name,
			String imagePath, String region, DiskFormat diskFormat, String imageName, int cloudtypeId, 
			String ceeId, int diskID, String vmId, Logger logger, DBHandler db) {
				this.osc = osc;
				this.name = name;
				this.imagePath = imagePath;
				this.region = region;
				this.diskFormat = diskFormat;
				this.imageName = imageName;
				this.cloudtypeId = cloudtypeId;
				this.ceeId = ceeId;
				this.diskID = diskID;
				this.vmId = vmId;
				this.logger = logger;
				this.db = db;
	}

	public void run()
	{
		logger.debug("Started Image registration thread for a new OpenStack image");
                String iaasMapId=null;
                String status;
                int newOsDiskMapId = db.insertAndGetKey("osdiskmap (cloudtypeid,iaasname,osdiskid,iaasid,status) ", "("+cloudtypeId+",'"+(ceeId+"@"+imageName)+"',"+diskID+",'"+iaasMapId+"', '"+"LCK"+"')");
                db.update("vmslots", "osdiskmapid=" + newOsDiskMapId, "WHERE id=" + vmId);
        try {
            iaasMapId = osc.addImage(name, imagePath, region, diskFormat, ContainerFormat.BARE);
            status = osc.imageStatus(iaasMapId, region);
        } catch (Exception ex) {
           status="err";
        }
		
		if(status.equals("UNK")||status.equals("err"))
		{
			logger.error("Could not add image name:"+imageName);
                        db.delete("osdiskmap", "where id="+newOsDiskMapId);
                        db.update("vmslots", "osdiskmapid=null", "WHERE id=" + vmId);
		}
		else
		{ 
                        db.update("osdiskmap", "iaasid='"+iaasMapId+"'","where id="+newOsDiskMapId);
                        db.update("osdiskmap","status='"+status+"'","where id="+newOsDiskMapId);
			//int newOsDiskMapId = db.insertAndGetKey("osdiskmap (cloudtypeid,iaasname,osdiskid,iaasid,status) ", "("+cloudtypeId+",'"+(ceeId+"@"+imageName)+"',"+diskID+",'"+iaasMapId+"', '"+"UP"+"')");
			//now link this diskmap entry to this osdiskmap entry
			//
		}
	}
}
