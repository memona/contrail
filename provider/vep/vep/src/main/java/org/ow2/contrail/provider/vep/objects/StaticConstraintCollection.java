package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.ow2.contrail.provider.vep.App;

public class StaticConstraintCollection extends VEPCollection {

	private ArrayList<StaticConstraint> scs = new ArrayList<StaticConstraint>();
	private String type;
	
	public StaticConstraintCollection() {
		super("StaticConstraintCollection", true);
		this.setId("/constraints");
		this.setResourceUri("VEP/ConstraintCollection");
	}

	public boolean retrieveStaticConstraintCollection() throws SQLException{
		boolean success = false;
		ResultSet rs = db.query("select", "*", "constraints", "");
		while (rs.next())
		{
			StaticConstraint s = new StaticConstraint(false);
			s.setName(rs.getString("descp"));
			s.setId("/constraint/"+rs.getInt("id"));
			int typeid = rs.getInt("type");
			if(typeid == App.VMCONSTRAINT)
				s.setType("VM CONSTRAINT");
			else if(typeid == App.STORAGECONSTRAINT)
				s.setType("STORAGE CONSTRAINT");
			else if(typeid == App.GENERALCONSTRAINT)
				s.setType("GENERAL CONSTRAINT");
			scs.add(s);
			success = true;
		}
		return success;
	}
	
	public ArrayList<StaticConstraint> getScs() {
		return scs;
	}


	
}
