/* Copyright (c) 2013, Inria, XLAB d.o.o., Consiglio Nazionale delle Ricerche (CNR).
All rights reserved.

This file is part of the Contrail system see <http://contrail-project.eu/>
for more details. The Contrail project has been developed with the
financial support of the European Commission's ICT FP7 program
under Grant Agreement # 257438.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

  -         Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

  -        Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

  -        Neither the name of Inria, XLAB d.o.o., Consiglio Nazionale delle Ricerche (CNR),
        nor the names of its contributors nor the name of the Contrail project may be used
        to endorse or promote products derived from this software without specific prior written
        permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Inria, XLAB d.o.o., Consiglio Nazionale delle Ricerche (CNR),
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,  PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;  OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,  WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

package org.ow2.contrail.provider.vep.openstack;

import java.io.IOException;
import java.util.LinkedList;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.commons.codec.binary.Base64;

import org.jclouds.ContextBuilder;
import org.jclouds.collect.PagedIterable;
import org.jclouds.io.Payload;
import org.jclouds.io.payloads.FilePayload;
import org.jclouds.openstack.glance.v1_0.GlanceApi;
import org.jclouds.openstack.glance.v1_0.GlanceApiMetadata;
import org.jclouds.openstack.glance.v1_0.domain.ContainerFormat;
import org.jclouds.openstack.glance.v1_0.domain.DiskFormat;
import org.jclouds.openstack.glance.v1_0.domain.Image;
import org.jclouds.openstack.glance.v1_0.domain.ImageDetails;
import org.jclouds.openstack.glance.v1_0.options.CreateImageOptions;
import org.jclouds.openstack.keystone.v2_0.KeystoneApi;
import org.jclouds.openstack.keystone.v2_0.KeystoneApiMetadata;
import org.jclouds.openstack.nova.v2_0.NovaApi;
import org.jclouds.openstack.nova.v2_0.NovaApiMetadata;
import org.jclouds.openstack.nova.v2_0.domain.Flavor;
import org.jclouds.openstack.nova.v2_0.domain.RebootType;
import org.jclouds.openstack.nova.v2_0.domain.Server;
import org.jclouds.openstack.nova.v2_0.domain.ServerCreated;
import org.jclouds.openstack.nova.v2_0.features.FlavorApi;

import com.woorea.openstack.base.client.OpenStackClientConnector;
import com.woorea.openstack.base.client.OpenStackSimpleTokenProvider;
import com.woorea.openstack.keystone.Keystone;
import com.woorea.openstack.keystone.api.UsersResource.Create;
import com.woorea.openstack.keystone.model.authentication.UsernamePassword;
import com.woorea.openstack.keystone.model.Access;
import com.woorea.openstack.keystone.model.Service;
import com.woorea.openstack.keystone.model.Services;
import com.woorea.openstack.keystone.model.User;
import com.woorea.openstack.keystone.model.Users;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
/*
 * Just to test and demonstrate the possibility
 */
public class osjct {

	
	
	private static void test_create_image()
    {
        String identity = "admin:admin"; // userName:tenant
        String username = "admin";
        String tenantname = "admin";
        String password = "verybadpass";
        String keystoneUserConnectionString = "http://127.0.0.1:5000/v2.0/";
        String keystoneAdminConnectionString = "http://127.0.0.1:35357/v2,0/";
        
        GlanceApi glanceApi = ContextBuilder.
                newBuilder(new GlanceApiMetadata()).
                endpoint(keystoneUserConnectionString).
                credentials(identity, password).
                buildApi(GlanceApi.class);
        
        String zoneString = glanceApi.getConfiguredZones().toArray()[0].toString();
        
        
        CreateImageOptions cio = new CreateImageOptions();
        cio.diskFormat(DiskFormat.ISO);
        cio.containerFormat(ContainerFormat.BARE);
        
        Payload payload = new FilePayload(new File("/root/Downloads/dsl-4.4.10.iso"));
        
        ImageDetails imageDetails = glanceApi.
                getImageApiForZone(zoneString).
                create("GlanceApiImageTest2",payload,cio);
        
        System.out.println(imageDetails.getId());
        
        if(glanceApi.
                getImageApiForZone(zoneString).
                delete(imageDetails.getId()))
            System.out.println("Successfully deleted");
        
        System.out.println();       
                
    }
    
    private static void test_create_instance()
    {
        String identity = "admin:admin"; // userName:tenant
        String username = "admin";
        String tenantname = "admin";
        String password = "verybadpass";
        String keystoneUserConnectionString = "http://127.0.0.1:5000/v2.0/";
        String keystoneAdminConnectionString = "http://127.0.0.1:35357/v2,0/";
        
        NovaApi novaApi = ContextBuilder.
                newBuilder(new NovaApiMetadata()).
                endpoint(keystoneUserConnectionString).
                credentials(identity, password).
                buildApi(NovaApi.class);
        
        String zoneString = novaApi.getConfiguredZones().toArray()[0].toString();
        
        String serverName = "testServer";
        Flavor someFlavour = novaApi.getFlavorApiForZone(zoneString).listInDetail().get(0).get(0);
        /*
        Flavor.Builder fb = new Flavor.Builder() {

            @Override
            protected Resource.Builder self() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        fb.disk(10240);
        fb.ephemeral(10240);
        fb.ram(1024);
        fb.vcpus(4);
        
        Flavor fv = novaApi.getFlavorApiForZone(zoneString).create(fb.build());
        */
        String flavourRef = "2"; //someFlavour.getId(); - it was Tiny (id="1") to small to run sth
        String imageRef = "730dad51-6dae-4f0c-a060-9733e24e8d9a"; //f17 jeos
        
        ServerCreated sc = novaApi.getServerApiForZone(zoneString).create(serverName, imageRef, flavourRef);
        
        System.out.print("Server admin password: ");
        System.out.println(sc.getAdminPass().toString());
        
        //novaApi.getServerApiForZone(zoneString).start(sc.getId()); //- we can't run it becaus it's always run    
    }
	
	
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception, IOException, ParseException {
        //test_resister_user(); - not implemented in jclouds, use OpenStack JSDK
        //test_create_image(); //- works (create and delete)
        //test_create_instance();
//        OpenStackConnector osc = new OpenStackConnector("admin","admin", "changeme",
//        		"http://paradent-42-kavlan-5.rennes.grid5000.fr:5000/v2.0/", "admin","admin", "changeme",
//        		"http://paradent-42-kavlan-5.rennes.grid5000.fr:35357/v2.0/", "null");
    	 OpenStackConnector osc = new OpenStackConnector("admin","admin", "password",
         		"http://131.254.201.3:5000/v2.0/", "admin","admin", "password",
         		"http://131.254.201.3:35357/v2.0/", "null");

        
        String result="";
        
        
        
        
        
        //Tested behaviour:
        LinkedList<OpenStackConnector.Hypervisor> list = osc.getHypervisorList("regionOne");
        OpenStackConnector.Hypervisor h = osc.getHypervisorInfo("regionOne", list.get(0).GetId());
        result=h.GetHostname();
        result=h.GetVcpus().toString();
//        result = osc.addImage("cirros","/home/fdudouet/imgtouse/cirros.img", "regionOne", DiskFormat.RAW, ContainerFormat.BARE);
//       Logger logger =null;
        //OpenStackClientConnector connector = new com.woorea.openstack.connector.JerseyConnector();
//        Access access;
//        Keystone keystone = new Keystone("http://localhost:35357/v2.0/");
//        UsernamePassword up = new UsernamePassword("admin", "password");
//       
//        access = keystone.tokens()
//                .authenticate(up)
//                .withTenantName("admin")
//                .execute();
//        keystone.setTokenProvider(new OpenStackSimpleTokenProvider(access.getToken().getId()));
//        System.out.println("\n\nServices:");
//        Services services = keystone.services().list().execute();
//        for (Service service : services) {
//                System.out.println("  " + service.getId() + ", " + service.getName() + ", " + service.getDescription());
//        }
//     
//        User bidule = new User();
//        bidule.setEmail("a@a.com");
//        bidule.setUsername("toto");
//        bidule.setName("Gros toto");
//        bidule.setPassword("password");
//        bidule.setEnabled(true);
//        
//        User toto = keystone.users().create(bidule).execute();
//        System.out.println(toto.getId());
//        
//        Users k = keystone.users().list().execute();
//        for(User u:k)
//        {
//        	System.out.println(u.getId() + " : "+u.getName());
//        	
//        }
         //result=osc.addUser("vvp", "vvp", "Vladimir V. Putin", "putin@gov.ru", "admin", "admin"); 
        //result = osc.addVM("test_machine4", "f795d0b6-07f3-45d7-9e9c-b8721b674684", "RegionOne", 0, 0, 512, 2, "parapluie-10-kavlan-4.rennes.grid5000.fr", new LinkedList<String>(),new HashMap<String,String>(),"key_cirros");
//        LinkedList<String> strings = osc.getVirtualNetworkList("RegionOne");
//        JSONObject jo = osc.getVirtualNetwork("RegionOne", strings.get(0));
//        result=(String)jo.get("id");
        //Server srv = osc.getVmInfo("1f394edc-34a2-4324-b183-c3b6f6c32263", "RegionOne");
        //LinkedList<String> strings = osc.getZonesList();
        //result = osc.imageStatus("64d80aa7-3c65-4afe-9638-5dc8ccad2704", "RegionOne");
        //osc.removeImage("64d80aa7-3c65-4afe-9638-5dc8ccad2704", "RegionOne");
        //Boolean test = osc.restartVM("502971eb-9a53-489b-a3af-c5193d332827", "RegionOne", RebootType.SOFT);
        //Boolean test = osc.restartVM("502971eb-9a53-489b-a3af-c5193d332827", "RegionOne", RebootType.HARD);
        //Boolean test = osc.shutdownVM("502971eb-9a53-489b-a3af-c5193d332827", "RegionOne");
        //Boolean test = osc.startVM("502971eb-9a53-489b-a3af-c5193d332827", "RegionOne");
        //result = test.toString();
        
        //Boolean test = osc.deleteVM("2c94cb24-9d74-40e0-b572-76917b9267d9", "RegionOne", false);
        
        //result = test.toString();
        /*for(String i : strings)
        {
            result+=i+"\n";
        }*/
        
        System.out.println(result);
        
    
    }
}
