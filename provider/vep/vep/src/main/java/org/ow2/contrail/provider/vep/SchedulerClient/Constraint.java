/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep.SchedulerClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author fgaudenz
 */
public class Constraint {
     private String type;
     private String value;
     private String operator;
     
     private List<String> vms;
     
     public Constraint(String type,
        String value,
        String operator,
        List<String> vms){
        this.type=type;
        this.operator=operator;
        this.value=value;
        this.vms=vms;
     }
     
     public Constraint(String type,
        String operator,
        List<String> vms){
        this.type=type;
        this.operator=operator;
        this.value=null;
        this.vms=vms;
     }
     
     public Constraint(String type,String operator){
         this.type=type;
         this.operator=operator;  
     }
     public JSONObject toJSON(){
         JSONObject obj=new JSONObject();
         obj.put("id", type);
         obj.put("operator", operator);
         if(value!=null){
             obj.put("value",value);
         }
         List vmsArray=new LinkedList();
         for(String vm:vms){
            // System.out.prinln()
         Map m2 = new HashMap();
         m2.put("id", vm);
         vmsArray.add(m2);
         }
         obj.put("VMs",vmsArray);
         return obj;        
     }

    public String getType() {
        return type;
    }

    public String getOperator() {
        return operator;
    }
     
     
}
