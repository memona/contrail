package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StaticConstraint extends VEPObject {

	private String constraintId;
	private String descp;
	private String type;
	public StaticConstraint(boolean instantiate) {
		super("StaticConstraint", instantiate);
	}
	
	public StaticConstraint(String constraitId) {
		super("StaticConstraint", true);
		this.constraintId = constraitId; //not useful?
		this.setId("/constraint/"+constraitId);
		this.setResourceUri("VEP/Constraint");
	}
	
	public boolean retrieveStaticConstraint() throws SQLException
	{
		boolean success = false;
		ResultSet rs = db.query("select", "descp", "application", "where id="+constraintId);
		if (rs.next())
		{
			this.setName(rs.getString("descp"));
			success = true;
		}
		return success;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
