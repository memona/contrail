package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Collection;
import java.util.HashMap;
import java.util.Map;



import org.ow2.contrail.provider.vep.openstack.OpenStackConnector;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import org.jclouds.openstack.glance.v1_0.domain.ContainerFormat;
import org.jclouds.openstack.glance.v1_0.domain.DiskFormat;
import org.jclouds.openstack.nova.v2_0.domain.Address;
import org.jclouds.openstack.nova.v2_0.domain.Server;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;

import org.ow2.contrail.provider.vep.App;
import org.ow2.contrail.provider.vep.CEEDeploymentThread;
import org.ow2.contrail.provider.vep.ImageRegistrationThread;
import org.ow2.contrail.provider.vep.One3XMLRPCHandler;
import org.ow2.contrail.provider.vep.OpenNebulaVM;
import org.ow2.contrail.provider.vep.VEPHelperMethods;
import org.ow2.contrail.provider.vep.SchedulerClient.SchedulerClient;
import org.ow2.contrail.provider.vep.fixImage2_2.ImageFIXClient;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VEPVirtualMachine extends VEPObject {

	private String ceeId;
	private String applicationId;
	private String vmId;

	public int countToDeploy = 0;
	public String endDateOfReservation;
	public int reservationId = 0;

	//Other data here.
	private int corecount;
	private int cpufreq;
	private int ram;
	private int ceevmhandlerid;
	private HashMap<String, String> context = new HashMap<String, String>();
	private String contextString;
	private String state;
	private int ovfmapid;
	private String ovfmapname;
	private int hostid = 0;
	private String iaasid=null;
	private int osdiskmapid;
//	private int starttime;
//	private int uptime;
//	private int endtime;
	private String userid;
	private String ip = null;



	public void setLogger(){
		logger = org.apache.log4j.Logger.getLogger("VEP.SchedulerClient"); 
	}


	public VEPVirtualMachine(boolean instantiate) {
		super("VEPVirtualMachine", instantiate);
		this.setResourceUri("VEP/VirtualMachine");
	}

	public VEPVirtualMachine() {
		super("VEPVirtualMachine", true);
		this.setResourceUri("VEP/VirtualMachine");
	}
	public VEPVirtualMachine(String ceeId, String applicationId, String vmId) {
		super("VEPVirtualMachine", true);
		this.ceeId = ceeId; //not useful?
		this.applicationId = applicationId;
		this.vmId = vmId;
		this.setId("/cee/"+ceeId+"/application/"+applicationId+"/vm/"+vmId);
		this.setResourceUri("VEP/VirtualMachine");
	}

	public void retrieveIdFromName() throws SQLException
	{
		instantiate();
		ResultSet rs = db.query("select", "id", "vmslots", "where ceeid="+ceeId+" and name='"+getName()+"'");
		if(rs.next()) vmId = String.valueOf(rs.getInt("id"));
	}

	public void retrieveVirtualMachine() throws SQLException 
	{

		instantiate();

		ResultSet rs = db.query("select", "*", "vmslots", "where id="+vmId);

		if (rs.next())
		{
			logger.debug("getting value VM"+ vmId);
			setState(rs.getString("status"));
			setRam(rs.getInt("ram"));
			setCpufreq(rs.getInt("cpufreq"));
			setCorecount(rs.getInt("corecount"));
			setCeevmhandlerid(rs.getInt("ceevmhandlerid"));
			setOvfmapid(rs.getInt("ovfmapid"));
			setName(rs.getString("name"));
			setHostid(rs.getInt("hostid"));
			setIaasid(rs.getString("iaasid"));
			setOsdiskmapid(rs.getInt("osdiskmapid"));
			contextString = rs.getString("context");
			if(ovfmapid != 0){
				rs = db.query("select", "ram, corecount, cpufreq, ovfid", "ovfmap", "where id="+ovfmapid);
				if(rs.next())
				{
					this.ovfmapname = rs.getString("ovfid");
					if(ram == 0)
						ram = rs.getInt("ram");
					if(corecount == 0)
						corecount = rs.getInt("corecount");
					if(cpufreq == 0)
						cpufreq = rs.getInt("cpufreq");
					if(ram == 0 || corecount == 0 || cpufreq == 0)
						setError("Wrongly configured VMSlot id '"+vmId+", CPUFreq/CPUCores/RAM are not correctly setup");
				} else {
					setError("Wrongly configured VMSlot id '"+vmId+", Linked to an unknown ovfmap entry");
				}
				rs = db.query("select", "id", "reservations", "where ovfmapid="+ovfmapid);
				if(rs.next())
				{
					reservationId = rs.getInt("id");
				}
			} else {
				setError("Wrongly configured VMSlot id '"+vmId+", No link to an ovfmap entry");
			}
			if(userid == null)
			{
				rs = db.query("select", "user.id as uid, username","user,cee","where cee.id="+ceeId+" and user.id=cee.userid");
				if(rs.next())
				{
					this.setUserid(String.valueOf(rs.getInt("uid")));
				}
			}
		} else {
			setError("Unknown vm id: "+vmId);
		}
	}

	/**
	 * To avoid doing unnecessary calls to the IaaS driver, only use this method when you need some data from the IaaS, such as IP of a VM
	 * @param username Will use this user's iaas credentials for the request
	 * @throws SQLException 
	 */
	public void retrieveVmInfo(String username) throws SQLException
	{
		ResultSet rs;
		rs = db.query("select", "*", "connectioninfo", "where vmslotid="+vmId);
		if(rs.next())
		{
			this.ip = rs.getString("ip");
		} else
		{
			if(iaasid !=null && !iaasid.equals("-1"))
			{
				rs = db.query("select", "cloudtypeid", "host", "where id="+hostid);
				if(rs.next())
				{
					int cloudtypeid = rs.getInt("cloudtypeid");
					if(cloudtypeid != 0)
					{
						rs = db.query("select", "*", "cloudtype", "where id="+cloudtypeid);
						if(rs.next())
						{
							int typeid = rs.getInt("typeid");
							if(typeid == App.OPENNEBULA3_HOST)
							{
								//OpenNebula3 Host: Use One3XMLRPCHandler

								//retrieve connection details
								String ip = rs.getString("headip");
								String port = String.valueOf(rs.getInt("headport"));
								//retrieve vep iaasuser's id from username
								logger.debug("Using VEP Username: " + username);
								rs = db.query("select", "id", "user", "where username=\""+username+"\"");
								if(rs.next())
								{
									int userid = rs.getInt("id");
									//retrieve IaaS UserName
									rs = db.query("select", "iaasuser,iaaspass", "accountmap", "where vepuser="+userid);
									if(rs.next())
									{
										String user = rs.getString("iaasuser");
										String pass = rs.getString("iaaspass");
										//instantiate handler
										One3XMLRPCHandler one3 = new One3XMLRPCHandler(ip, port, user, pass, "VEPVirtualMachine:retrieveVmInfo");
										//query one
										OpenNebulaVM temp = one3.getVmInfo(Integer.parseInt(iaasid));
										//parse response
										if(temp != null)
										{
											this.ip = temp.nic_ip;
											db.insert("connectioninfo(vmslotid,ip)", "("+vmId+",'"+this.ip+"')");
										} else {
											setError("No ip registered for this VM on OpenNebula. Does the VM template contains a NIC?");
										}
									} else {
										setError("User "+username+" is not registered on specified host.");
									}
								} else {
									setError("Unknown username");
								}
							} else if(typeid == App.OPENSTACK_HOST) {
								try {
									OpenStackConnector osc = VEPHelperMethods.getOSC(Integer.parseInt(userid), cloudtypeid);
									rs.close();
									rs = db.query("select", "region", "openstackcloud", "where openstackcloud.id="+cloudtypeid);
									//same id for OSC and Cloudtype, OSC is just to separate information between tables and not put every specific cloud info in the same table
									if(rs.next())
										{
											String zone = rs.getString("region");
											Server vm = osc.getVmInfo(iaasid, zone);
											Multimap<String, Address> addresses = vm.getAddresses();
											//we know there is only one address so break
											for(String key : addresses.keySet())
											{
												
												ImmutableList<Address> a = (ImmutableList<Address>) addresses.get(key);
												this.ip = a.get(0).getAddr();
												db.insert("connectioninfo(vmslotid,ip)", "("+vmId+",'"+this.ip+"')");
												break;
											}
										}
								} catch (Exception e) {
									logger.error("Exception caught ", e);
									setError("Could not create OpenStackConnector object or retrieve vm info from this object");
								}
							} // same for other types of cloud
						}
					} else {
						setError("No cloud linked to this host inside the database.");
					}
				} else {
					setError("Could not retrieve VM info");
				}
			} else {
				//setError("No host registered for this VM in our database. Has the VM been started?");
				logger.info("VM "+this.vmId+" not scheduled yet");
			}
		}
	}


	public void registerToDb() throws SQLException {
		ResultSet rs;
		//fist check if name and virtualsystem are both present
		if(this.getName() != null && this.ovfmapname != null)
		{
			//check if the virtual system name exists in the ovfmap table
			rs = db.query("select", "ovfmap.id as ovfmapid,ovfmap.ceevmhandlerid as handlerid, context", "ovfmap,ovf", "where ovf.applicationid="+applicationId+" and ovfmap.ovfcontainerid=ovf.id and ovfmap.ovfid='"+ovfmapname+"'");
			if(rs.next())
			{
				int ovfmapid = rs.getInt("ovfmapid");
				int handlerid = rs.getInt("handlerid");
				String ovfmapContext = rs.getString("context");
				int newVmSlotId = db.insertAndGetKey("vmslots(ceeid,name,ceevmhandlerid,ovfmapid,status)", "("+ceeId+",'"+getName()+"',"+handlerid+","+ovfmapid+",'INI')");
				this.vmId = String.valueOf(newVmSlotId);
				if(handlerid != 0){
					//specific handler required, everything must be checked here to be sure that handler fits the requirement, for now assumes user knows what he is doing
					db.update("vmslots", "ceevmhandlerid="+handlerid, "where id="+newVmSlotId);
					//update vmslot values to the lowest bounds of the handler
				}
				if(cpufreq!=0)
				{
					db.update("vmslots", "cpufreq="+cpufreq, "where id="+newVmSlotId);
				}
				if(ram != 0)
				{
					db.update("vmslots", "ram="+ram, "where id="+newVmSlotId);
				}
				if(corecount != 0)
				{
					db.update("vmslots", "corecount="+corecount, "where id="+newVmSlotId);
				}
				//check existence of vm contextualization information
				if(context != null)
				{
					//analyse the contextualisation data here
					//TODO:analyse existing context in the ovfmap table, for now add everything in context without checking previous existence
					if(ovfmapContext != null) ovfmapContext+="\n";
					for(Map.Entry<String, String> contextKeyVal : context.entrySet())
					{
						ovfmapContext+=contextKeyVal.getKey()+"=\""+contextKeyVal.getValue()+"\",\n";
					}
					//FLORIAN UPDATE CONTEXT
					//ovfmapContext = ovfmapContext.substring(0, ovfmapContext.length() - 2);
				}
				//once ovfMapContext is filled with the real ovfmap context data plus the specific vm context, store the string in the db
				if(ovfmapContext != null){
					db.updateContext(newVmSlotId, ovfmapContext, "vmslots");
				}
			} else {
				setError("Trying to register a VMSlot using an unknown ovf item");
			}
		} else {
			setError("Invalid data specified for a VM, name and virtualsystem href must be specified");
		}
	}

	/**
	 * Delete a VMSlot
	 * @throws SQLException 
	 */
	public void delete() throws SQLException {
		stop(); // TODO:test
		if(!isError())
			db.delete("vmslots", "where id="+vmId);
	}

	public void updateDiskMap() {
		try
		{
			//get ovfmapid from vmslots
			ResultSet rs = db.query("select", "ovfmapid, hostid", "vmslots", "WHERE id=" + vmId);
			rs = db.query("select", "osdiskid", "ovfmap", "WHERE id=" + ovfmapid);
			if(rs.next())
			{
				int osdiskId = rs.getInt("osdiskid");
				//now use this to find if a suitable osdiskmap entry exists, if not then create one
				//first determine the cloud controller type managing the host
				rs = db.query("select", "cloudtypeid", "host", "where id=" + hostid);
				if(rs.next())
				{
					int cloudtypeId = rs.getInt("cloudtypeid");
					//now find if a osdiskmap entry for this cloudtype exists
					rs = db.query("select", "id", "osdiskmap", "WHERE osdiskid=" + osdiskId + " AND cloudtypeid=" + cloudtypeId);
					if(rs.next())
					{
						//add the reference to vmslot entry
						int osdiskmapId = rs.getInt("id");
						if(!db.update("vmslots", "osdiskmapid=" + osdiskmapId, "WHERE id=" + vmId))
							setError("Could not set OSDiskMap for VM '"+vmId+"'");
					}
					else 
					{
						//look for the type of cloud this host belongs to
						rs = db.query("select", "typeid", "cloudtype", "where id="+cloudtypeId);
						int cloudManagerType = rs.getInt("typeid");
						if (cloudManagerType == App.OPENNEBULA3_HOST)
						{
							//add image
							rs=db.query("select", "hostid,localpath,ovfimagename,osdiskid", "vmslots,ovfmap,osdisk", "WHERE vmslots.id=" + vmId+" and osdisk.id=ovfmap.osdiskid and ovfmap.id=vmslots.ovfmapid");
							String imageName=null;
							String imagePath=null;
							int hostid=-1;
							int diskID;
							if(rs.next())
							{
								imageName=rs.getString("ovfimagename");
								imagePath=rs.getString("localpath");
								hostid=rs.getInt("hostid");
								diskID=rs.getInt("osdiskid");
								logger.debug(imageName + " " + imagePath);
								rs.close();
								//check cloud provider type from host
								rs=db.query("select", "cloudtypeid,headip,headport,version,sharedfolder", "cloudtype,host", "WHERE host.id=" + hostid+" and host.cloudtypeid=cloudtype.id");
								String cloudIP,cloudFolder,version;
								int cloudPort,cloudid;
								if(rs.next())
								{
									cloudIP=rs.getString("headip");
									cloudPort=rs.getInt("headport");
									cloudFolder=rs.getString("sharedfolder");
									cloudid=rs.getInt("cloudtypeid");
									version=rs.getString("version");
									logger.debug(cloudIP + " " + cloudPort);
									//administrator
									rs.close();
									rs = db.query("select", "iaasuser,iaaspass", "user,accountmap", "WHERE role='administrator' and accountmap.cloudtype="+cloudid+" and accountmap.vepuser=user.id");
									String iaasUser,iaasPass;
									if(rs.next())
									{
										iaasUser=rs.getString("iaasuser");
										iaasPass=rs.getString("iaaspass");
										logger.debug(iaasUser + " " + iaasPass);
										rs.close();     
										String template=null;      
										if(version.startsWith("2."))
										{
											ImageFIXClient fixImg;
											fixImg = new ImageFIXClient(cloudIP, "10556");
											String idPathImage = fixImg.downloadImage(imagePath,ceeId); 
											fixImg.setImage(idPathImage);
											fixImg.closeClient();     
										}
										//call right client based on cloud provider type
										//create and register a new entry on osdiskmap (physical and database)
										//version 3.4
										if(version.startsWith("3."))
										{
											One3XMLRPCHandler one3handle = new One3XMLRPCHandler(cloudIP, Integer.toString(cloudPort), iaasUser, iaasPass, "image managing");
											template = "NAME="+ceeId+"@"+imageName+"\n" +"PATH="+imagePath+"\n" +"TYPE=OS \n" +"PUBLIC=YES";                          
											logger.info(template);
											//unique image:TODO
											//getCEE id and add to image
											//String template="NAME='"+providerImageName+"' \nPATH="+imagePath+" \nTYPE=OS \n PUBLIC=YES ";
											int response=one3handle.addImage(template);
											if(response==-1)
											{
												logger.debug("Could not add image name:"+imageName);
												setError("Could not add image name:"+imageName);
											}
											else
											{
												rs = db.query("select", "max(id)", "osdiskmap", "");
												int osdiskmapid = 1;
												if(rs.next()) 
													osdiskmapid = rs.getInt(1) + 1;    
												db.insert("osdiskmap (id,cloudtypeid,iaasname,iaastemplate,osdiskid,iaasid,status) ", "("+osdiskmapid+","+cloudid+",'"+(ceeId+"@"+imageName)+"','"+template+"',"+diskID+",'"+response+"', 'LCK')");
												//now link this diskmap entry to this osdiskmap entry
												db.update("vmslots", "osdiskmapid=" + osdiskmapid, "WHERE id=" + vmId);
											}
										}
										else if(version.startsWith("2."))
										{
											throw new Exception("not implemented yet.");
										}
									}
									else
									{
										setError("Could not find admin username and password for cloud '"+cloudid+"'");
									}
								}
								else
								{
									setError("Could not find the cloud connection details for host id '"+hostid+"'");
								}
							}
							else
							{
								logger.debug("Error while adding image for vmslot:"+vmId);
								setError("Error while adding image for VM '"+vmId+"'");
							}
						} 
						else if (cloudManagerType == App.OPENSTACK_HOST)
						{
							rs.close();
							rs=db.query("select", "localpath,ovfimagename,osdiskid", "vmslots,ovfmap,osdisk", "WHERE vmslots.id=" + vmId+" and osdisk.id=ovfmap.osdiskid and ovfmap.id=vmslots.ovfmapid");
							String imageName=null;
							String imagePath=null;
							int diskID;
							if(rs.next())
							{
								imageName=rs.getString("ovfimagename");
								imagePath=rs.getString("localpath"); // Path must be local /!\
								if(!imagePath.startsWith("/")) 
								{
									setError("Trying to register a distant image on an OpenStack Host");
									return;
								}
								diskID=rs.getInt("osdiskid");
								logger.debug(imageName + " " + imagePath);
								rs.close();
								//check cloud provider type from host
								rs=db.query("select", "tenantname, tenantid, roleid, cloudtypeid,keystoneuserip, keystoneuserport, keystoneadminip, keystoneadminport, region,version,sharedfolder", "cloudtype,host,openstackcloud", "WHERE host.id=" + hostid+" and host.cloudtypeid=cloudtype.id and openstackcloud.id=cloudtype.id");
								String keystoneUserIp, keystoneAdminIp,version,cloudFolder,region;
								int keystoneUserPort, keystoneAdminPort;
								if(rs.next())
								{
									keystoneUserIp=rs.getString("keystoneuserip");
									keystoneUserPort=rs.getInt("keystoneuserport");
									String keyStoneUserAPI = keystoneUserIp+":"+keystoneUserPort+"/v2.0/"; 
									keystoneAdminIp=rs.getString("keystoneadminip");
									keystoneAdminPort = rs.getInt("keystoneadminport");
									String keyStoneAdminAPI = keystoneAdminIp+":"+keystoneAdminPort+"/v2.0/";
									region = rs.getString("region");//region used by default for VEP spawned VM's on this OpenStack cloud
									cloudFolder=rs.getString("sharedfolder");
									String tenant = rs.getString("tenantname");
//									String tenantid = rs.getString("tenantid");
//									String roleid = rs.getString("roleid");
									logger.debug("user: " + keystoneUserIp + ":" + keystoneUserPort + " - admin: "+keystoneAdminIp+":"+keystoneAdminPort);
									//find an administrator username:password and tenant for this openstack host
									rs.close();
									rs = db.query("select", "iaasuser,iaaspass", "user,accountmap", "WHERE role='administrator' and accountmap.cloudtype="+cloudtypeId+" and accountmap.vepuser=user.id");
									String iaasUser,iaasPass;
									if(rs.next())
									{
										iaasUser=rs.getString("iaasuser");
										iaasPass=rs.getString("iaaspass");
										logger.debug(iaasUser + ":" + iaasPass+" tenant:"+tenant);
										rs.close();
										//will use admin credential to create the image as a public image usable by anyone
										//will use same admin's tenant
										OpenStackConnector osc = new OpenStackConnector(iaasUser,tenant, iaasPass,
												keyStoneUserAPI, iaasUser, tenant, iaasPass,
												keyStoneAdminAPI, "ImageCreate");
										
										String name = ceeId+"@"+imageName;
										DiskFormat diskFormat;
										if(imagePath.contains(".qcow2")){
											diskFormat = DiskFormat.QCOW2;
										}else if(imagePath.contains(".img")){
											diskFormat = DiskFormat.RAW;
										}else if(imagePath.contains(".iso")){
											diskFormat = DiskFormat.ISO;
										}else{
											setError("Unknown image type, can not add image to OpenStack cloud, please use either qcow2, img or iso");
											return;
										}							
										ImageRegistrationThread im = new ImageRegistrationThread(osc, name, imagePath, region, diskFormat, imageName, cloudtypeId, ceeId, diskID, vmId, logger, db);
										//im.start();
                                                                                im.run();
										
									}
								}
							}
						}
						else
						{
							setError("Could not find the cloud controller type managing the chosen host for VM '"+vmId+"'");
						}
					}
				}
				else
				{
					setError("Could not find the ovfmapid referenced in VM '"+vmId+"' in the ovfmap table");
				}
			}
		}
		catch(Exception ex)
		{
			logger.debug("Exception caught ", ex);
			setError("Caught Exception while updating VM '"+vmId+"' diskmap");
		}
	}



	/**
	 * Action router of the VM
	 * @param action
	 * @throws SQLException 
	 */

	public void doAction(String action) throws SQLException {
		if(action.equalsIgnoreCase("start"))
			start();
		else if(action.equalsIgnoreCase("stop"))
			stop();
		else
			setError("Action: '"+action+"' does not exist or is not implemented yet.");
	}

	private void start() throws SQLException {
		retrieveVirtualMachine();
		//TODO:Check vm id existence in the db

		if(!state.equals("RN") && !state.equals("DEP"))
		{
			//Deploy request to the Scheduler
			//TODO:uncomment next line - DONE
			SchedulerClient.deploy(this);
			//TODO:Remove next line : COMMENTED
			//hostid=4;
			if(hostid == 0)
			{
				setError("The Scheduler could not deploy the VM "+this.getName());
			} else {
				//update the vmslot with the hostid
				db.update("vmslots", "hostid="+hostid, "where id="+vmId);
			}
			if(!isError())
			{
				//Host id is filled, now update the diskmap
				updateDiskMap();
				//now actually deploy the vm
				if(!isError())
				{
					//now start a CEEDeploymentThread with the list of vms
					//TODO:uncomment 3 next lines - DONE
					CEEDeploymentThread deploy = new CEEDeploymentThread(this);
					deploy.userid = Integer.parseInt(userid);
					deploy.start();
				}
			} 
		} else {
			setError("Cannot start VM "+vmId+" as it is already deployed.");
		}
	}
	private void stop() throws SQLException {
		//proceed only if the VM is running or deployed
		if(state.equals("RN") || state.equals("DEP") || state.equals("ERR"))
		{

			if(hostid != 0 && iaasid != null)
			{
				ResultSet rs = db.query("select", "cloudtypeid", "host", "WHERE id=" + hostid);
				if(rs.next())
				{
					int cloudTypeId = rs.getInt("cloudtypeid");
					rs = db.query("select", "*", "cloudtype", "WHERE id=" + cloudTypeId);
					if(rs.next())
					{
						int type = rs.getInt("typeid");
						if(type == App.OPENNEBULA3_HOST)
						{
							String headIP = rs.getString("headip");
							int headPort = rs.getInt("headport");
							String cloudVersion = rs.getString("version");

							rs = db.query("select", "*", "accountmap", "WHERE vepuser=" + userid + " AND cloudtype=" + cloudTypeId);
							if(rs.next())
							{
								String iaasUser = rs.getString("iaasuser");
								String iaasPass = rs.getString("iaaspass");
								if(cloudVersion.startsWith("3."))
								{
									One3XMLRPCHandler one3 = new One3XMLRPCHandler(headIP, String.valueOf(headPort), iaasUser, iaasPass, "RestAppStatusVM");
									try
									{
										boolean status = one3.shutdownVM(Integer.parseInt(iaasid));
										if(!status)
										{
											logger.error("Probably the VM state does not permit SHUTDOWN operation. ");
											setError("Probably the VM state does not permit SHUTDOWN operation. ");
										}else{
											db.update("vmslots", "status='INI', hostid=0, iaasid=null", "where id="+vmId);
											SchedulerClient.deleteVM(Integer.valueOf(vmId));
										} 
									}
									catch(Exception ex)
									{
										//warn system admin of a potential runaway VM
										try
										{
											int msgid = 1;
											rs = db.query("select", "max(id)", "errormessagelist", "");
											if(rs.next())
											{
												msgid = rs.getInt(1) + 1;
											}
											db.insert("errormessagelist", "(" + msgid + ", 'Possible Runaway VM', 'The VM IaaS Id is " + iaasid + " in cloud with id " + cloudTypeId + ". Exception message: " + ex.getMessage() + "', '" + userid + "', 'WARN', 'vmslots', '" + System.currentTimeMillis() + "', 0)");
										}
										catch(Exception ex1)
										{
											logger.debug("Could not save error message inside DB.");
										}
										setError("Exception caught during VM shutdown operation. ");
										logger.info("Exception caught during VM shutdown operation for VM slot: " + vmId);
										logger.debug("Exception caught ", ex);
									}
								}
								else if(cloudVersion.startsWith("2."))
								{
									setError("This cloud type is currently not supported. ");
								}
							}
							else
							{
								setError("Corresponding cloud account for this user is not found. ");
							}
						} else if(type == App.OPENSTACK_HOST)
						{
							OpenStackConnector osc = null;
							try {
								osc = VEPHelperMethods.getOSC(cloudTypeId);
							} catch (Exception e)
							{
								logger.error("Could not create OpenStackConnector", e);
								setError("Could not create OpenStackConnector");
							}
							if(osc != null)
							{
								rs.close();
								rs = db.query("select", "region", "openstackcloud", "where id="+cloudTypeId);
								if(rs.next())
								{
									String region = rs.getString("region");
									boolean status = osc.deleteVM(iaasid, region, true);
									if(!status)
									{
										logger.error("Probably the Openstack VM state does not permit SHUTDOWN operation. ");
										setError("Probably the Openstack VM state does not permit SHUTDOWN operation. ");
									}else{
										db.update("vmslots", "status='INI', hostid=0, iaasid=null", "where id="+vmId);
										SchedulerClient.deleteVM(Integer.valueOf(vmId));
									} 
								}
							}
						}
					}
				}
			} else {
				setError("VM is incorrectly registered on the database: HostId or IaasId missing");
			}
		}
	}


	//Getters+Setters

	public void setUserid(String userid) {
		this.userid = userid;
	}
	public int getCorecount() {
		return corecount;
	}

	public void setCorecount(int corecount) {
		this.corecount = corecount;
	}

	public int getCpufreq() {
		return cpufreq;
	}

	public void setCpufreq(int cpufreq) {
		this.cpufreq = cpufreq;
	}

	public int getRam() {
		return ram;
	}

	public void setRam(int ram) {
		this.ram = ram;
	}

	public String getVmId() {
		return vmId;
	}

	public String getCeeId() {
		return ceeId;
	}

	public void setCeeId(String ceeId) {
		this.ceeId = ceeId;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getOvfmapname() {
		return ovfmapname;
	}

	public void setOvfmapname(String ovfmapname) {
		this.ovfmapname = ovfmapname;
	}

	public void setVmId(String vmId) {
		this.vmId = vmId;
	}

	public int getOvfmapid() {
		return ovfmapid;
	}

	public void setOvfmapid(int ovfmapid) {
		this.ovfmapid = ovfmapid;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public HashMap<String, String> getContext() {
		return context;
	}

	public void setContext(HashMap<String, String> context) {
		this.context = context;
	}

	public String getIaasid() {
		return iaasid;
	}

	public void setIaasid(String iaasid) {
		this.iaasid = iaasid;
	}

	public int getOsdiskmapid() {
		return osdiskmapid;
	}

	public void setOsdiskmapid(int osdiskmapid) {
		this.osdiskmapid = osdiskmapid;
	}
	public int getHostid() {
		return hostid;
	}

	public void setHostid(int hostid) {
		this.hostid = hostid;
	}

	public int getCeevmhandlerid() {
		return ceevmhandlerid;
	}

	public void setCeevmhandlerid(int ceevmhandlerid) {
		this.ceevmhandlerid = ceevmhandlerid;
	}

	public String getContextString() {
		return contextString;
	}


	public String getIp() {
		return ip;
	}

}
