package org.ow2.contrail.provider.vep.SchedulerClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    private static final String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public static Date deserialize(String dateString) throws ParseException {
        if (dateString == null) {
            return null;
        }
        else {
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
            return sdf.parse(dateString);
        }
    }

    public static String serialize(Date date) {
        if (date == null) {
            return null;
        }
        else {
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
            return sdf.format(date);
        }
    }
}
