/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep.GAFSClient;

import java.net.URISyntaxException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.ow2.contrail.provider.vep.OAuthClient.OAuthClientFactory;
import org.ow2.contrail.provider.vep.VEPHelperMethods;

/**
 *
 * @author fgaudenz
 */
public class GAFSClient {
    private  String endpoint;
    private boolean cert=false;
    
    public GAFSClient(String endPoint,boolean security){
      if(security){
          cert=true;
      }
       this.endpoint=endPoint;
    }

    public String getEndpoint() {
        return endpoint;
    }
    
}
