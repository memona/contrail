package org.ow2.contrail.provider.vep.objects;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OVF extends VEPObject {

	private String ceeId;
	private String applicationId;
	private String ovfid;
	private String ovf;

	public OVF(String ceeId, String applicationId, String ovfid) {
		super("OVF", true);
		this.ceeId = ceeId; //not useful?
		this.applicationId = applicationId;
		this.ovfid = ovfid;
		this.setId("/cee/"+ceeId+"/application/"+applicationId+"/ovf/"+ovfid);
		this.setResourceUri("VEP/OVF");
	}
	
	public boolean retrieveOVF() throws SQLException
	{
		boolean success = false;
		
		//db request to retrieve the ovf
		ResultSet rs = db.query("select", "ovf.descp as ovfcontent", "ovf, application", "where application.ceeid="+ceeId+" and application.id="+applicationId+" and ovf.id="+ovfid);
		if(rs.next())
		{
			ovf = rs.getString("ovfcontent");
			success = true;
		}
		return success;
	}
	
	public String getOvf(){
		return ovf;
	}
}
