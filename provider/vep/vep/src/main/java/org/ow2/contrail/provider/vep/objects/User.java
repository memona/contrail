package org.ow2.contrail.provider.vep.objects;

import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;


import org.ow2.contrail.provider.vep.openstack.OpenStackConnector;

import org.ow2.contrail.provider.vep.App;
import org.ow2.contrail.provider.vep.DBHandler;
import org.ow2.contrail.provider.vep.One2XMLRPCHandler;
import org.ow2.contrail.provider.vep.One3XMLRPCHandler;
import org.ow2.contrail.provider.vep.VEPHelperMethods;
import org.apache.commons.lang.RandomStringUtils;

public class User extends VEPObject {
	
	private Logger logger = Logger.getLogger("VEP.User");;
	private DBHandler db = new DBHandler("User", VEPHelperMethods.getProperty("vepdb.choice", logger));
//	private String parent;
	private String userid;
	private String username;
	private String email;
	private String role;
	private String password;
	private ArrayList<CEE> cees = new ArrayList<CEE>();
//	private ArrayList<OVFTemplate> ovfs = new ArrayList<OVFTemplate>();
//	private ArrayList<CEETemplate> ceeTemplates = new ArrayList<CEETemplate>();
//	private ArrayList<Monitor> monitors;
//	private ArrayList<VO> vos;

	/**
	 * Builds a new user from scratch. 
	 * Use storeUser with the proper parameters to store the object's data into the database and getId to get the new user's id.
	 */
	public User(boolean instantiate){
		super("User", instantiate);
		this.setResourceUri("VEP/User");
		
	}
	
	/**
	 * Builds a user object from an existing id
	 * @param userid the uid of the user
	 */
	public User(String userid){
		super("User", true);
		this.userid = userid;
		this.setResourceUri("VEP/User");
		this.setId("/user/"+userid);
	}
	
	
	
	// May be useful in one needs to get all Users allowed by a specific role? 
	public User(String userid, String role){
		super("User", true);
		
	}
	
	/**
	 * Stores the current object's value inside the DB
	 * @return success as true/false boolean
	 * @throws SQLException 
	 */
	public boolean storeUser() throws SQLException{
		boolean success = false;
		ResultSet rs;
		//Check if the required fields are there
		if (username != null && password != null && email != null && role != null)
		{
			//db insertion into user
			rs = db.query("select", "max(id)", "user", "");
            int id = 1;
            if(rs.next()) 
            	id = rs.getInt(1) + 1;
            rs.close();
            int resetId = -1; //TODO:Use the resetId at some point to make something useful
            try {
				success = db.insert("user", "(" + id + ", '" + username + "', '" + VEPHelperMethods.makeSHA1Hash(password) + "', '" + email + "', " + resetId + ", '', '" + role + "')");
			} catch (NoSuchAlgorithmException e) {
				logger.debug("Could not use SHA1 function to hash password");
			}
            if(success)
            {
            	this.setId("/user/"+id);
            	this.setUserid(String.valueOf(id));
                //db insertion for CEE (not implemented)
            }
		}
		return success;
	}
	
	/**
	 * Retrieves User data from DB and fill the object
	 * @return success as true/false boolean
	 */
	public boolean retrieveFullUser() throws SQLException{
		ResultSet rs;
		boolean success = false;
		rs = db.query("select", "*", "user", "where id = " + userid);
		if (rs.next()){
			username = rs.getString("username");
			email = rs.getString("email");
			role = rs.getString("role");			
			success = true;
		}
		return success;
	}	

	public String getUserid() {
		return userid;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public String getRole() {
		return role;
	}
	public ArrayList<CEE> getCees() {
		return cees;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setCees(ArrayList<CEE> cees) {
		this.cees = cees;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public boolean storeUserOnCloud() {
		//TODO: Should register a user with a pass as SHA1Hash(pass), then store the temp password in the db for each user, instead of saving the hashed password. Hash should be regenerated from the password in the db when connecting to the cloud later.
		
		//TODO: check if the user needs to be registered in the IaaS or he has already an account
		int  headPort;
		String vepUserId=this.getUserid();
		ArrayList<Integer> cloudId = new ArrayList<Integer>();
		String cloudName="", version, headIp, iaasUser, iaasPass, tenant;
		try {
			ResultSet rs;
			//search for all registered cloud controllers
			rs = db.query("select", "id", "cloudtype", "");//commented for debug purpose: OpenStack Hosts only
			//rs = db.query("select", "id", "cloudtype", "where typeid="+App.OPENSTACK_HOST);
			
			while (rs.next()) {
				cloudId.add(rs.getInt("id"));
			}
			//loop over them
			rs.close();
			for (Integer cloudid : cloudId) {
				rs = db.query("select", "*", "cloudtype", "where id=" + cloudid);
				if (rs.next()) {
					headIp = rs.getString("headip");
					headPort = rs.getInt("headport");
					version = rs.getString("version");
					int typeId = rs.getInt("typeid");
					rs.close();
					//These two ifs refer to different methods to add users, depending on the cloud type (one or os)
					//they ALWAYS use the credentials of the first user in the user table registered as administrator, which should always be the by default 'admin' (or if not possessing an accountmap entry, the first one in the table by id). This account needs admin privileges on the cloud.
					if(typeId == App.OPENNEBULA3_HOST)
					{
						//retrieve admin user name on the cloud to id resources
						rs = db.query("select", "iaasuser,iaaspass", "user,accountmap", "WHERE role='administrator' and accountmap.cloudtype="+cloudid+" and accountmap.vepuser=user.id");
						if(rs.next())
						{
							iaasUser = rs.getString("iaasuser");
							iaasPass = rs.getString("iaaspass");
						} else {
							return false;
						}
						rs.close();
						rs = db.query("select", "name", "supportedcloud", "WHERE id=" + typeId);
						if (rs.next()) {
							cloudName = rs.getString("name");
						}
						rs.close();

						boolean status = true;

						//make sure that a mapping does not already exist
						//generating a random IaaS user ID
						int uname_size = 8;
						String tempUser = RandomStringUtils.randomAlphanumeric(uname_size);
						//now test to see if this is already in use
						boolean genNext = true;
						while (genNext) {
							logger.info("Testing if iaas mapping by id " + tempUser + " already exists.");
							rs = db.query("select", "*", "accountmap", "where iaasuser='" + tempUser + "' AND cloudtype=" + cloudid);
							if (rs.next()) {
								genNext = true;
								logger.info("IaaS mapping by id " + tempUser + " already exists. Will try to create a new id.");
							} else {
								genNext = false;
								logger.info("IaaS mapping by id " + tempUser + " does not exists. Will try to register with IaaS daemon.");
							}
							rs.close();
							if (genNext) {
								tempUser = RandomStringUtils.randomAlphanumeric(uname_size);
							}
						}
						//generating a random password for this account
						String tempPass = RandomStringUtils.randomAlphanumeric(32);
						int iaasId = -1;
						if (cloudName.equalsIgnoreCase("opennebula") && version.startsWith("2.2")) {
							try {
								One2XMLRPCHandler one2handle = new One2XMLRPCHandler(headIp, Integer.toString(headPort), iaasUser, iaasPass, "user account mapping");
								iaasId = one2handle.addUser(tempUser, VEPHelperMethods.makeSHA1Hash(tempPass));
							} catch (Exception ex) {
								iaasId = -1;
							}
						} else if (cloudName.equalsIgnoreCase("opennebula") && (version.startsWith("3"))) {
							try {
								One3XMLRPCHandler one3handle = new One3XMLRPCHandler(headIp, Integer.toString(headPort), iaasUser, iaasPass, "user account mapping");
								iaasId = one3handle.addUser(tempUser, VEPHelperMethods.makeSHA1Hash(tempPass));
							} catch (Exception ex) {

								iaasId = -1;
							}
						}
						if (iaasId != -1) {
							rs = db.query("select", "max(id)", "accountmap", "");
							int id = 1;
							if (rs.next()) {
								id = rs.getInt(1) + 1;
							}
							rs.close();

							status = db.insert("accountmap", "(" + id + ", " + cloudid + ", '" + tempUser + "', '" + VEPHelperMethods.makeSHA1Hash(tempPass) + "', '" + iaasId + "', " + vepUserId + ")");
						} else {
							status = false;
						}

						if (!status) {
							return status;
						}
					} else if (typeId == App.OPENSTACK_HOST)
					{
						rs.close();
						rs = db.query("select", "*", "openstackcloud", "where id = " + cloudid);
						if(rs.next())
						{
							String keystoneUserIp, keystoneAdminIp;
							int keystoneUserPort, keystoneAdminPort;
							keystoneUserIp=rs.getString("keystoneuserip");
							keystoneUserPort=rs.getInt("keystoneuserport");
							String keyStoneUserAPI = keystoneUserIp+":"+keystoneUserPort+"/v2.0/"; 
							keystoneAdminIp=rs.getString("keystoneadminip");
							keystoneAdminPort = rs.getInt("keystoneadminport");
							String keyStoneAdminAPI = keystoneAdminIp+":"+keystoneAdminPort+"/v2.0/";
							//String region = rs.getString("region");//region used by default for VEP spawned VM's on this OpenStack cloud
							
							String adminTenant = rs.getString("tenantname");
							String tenantId = rs.getString("tenantid");
							String roleId = rs.getString("roleid");
							
							//retrieve admin user name on the cloud to id resources
							rs = db.query("select", "iaasuser,iaaspass", "user,accountmap", "WHERE role='administrator' and accountmap.cloudtype="+cloudid+" and accountmap.vepuser=user.id");
							if(rs.next())
							{
								iaasUser = rs.getString("iaasuser");
								iaasPass = rs.getString("iaaspass");
															
								
								//make sure that a mapping does not already exist
								//generating a random IaaS user ID
								int uname_size = 8;
								String tempUser = RandomStringUtils.randomAlphanumeric(uname_size);
								//now test to see if this is already in use
								boolean genNext = true;
								while (genNext) {
									logger.info("Testing if iaas mapping by id " + tempUser + " already exists.");
									rs = db.query("select", "*", "accountmap", "where iaasuser='" + tempUser + "' AND cloudtype=" + cloudid);
									if (rs.next()) {
										genNext = true;
										logger.info("IaaS mapping by id " + tempUser + " already exists. Will try to create a new id.");
									} else {
										genNext = false;
										logger.info("IaaS mapping by id " + tempUser + " does not exists. Will try to register with IaaS daemon.");
									}
									rs.close();
									if (genNext) {
										tempUser = RandomStringUtils.randomAlphanumeric(uname_size);
									}
								}
								
								//generating a random password for this account
								String tempPass = VEPHelperMethods.makeSHA1Hash(RandomStringUtils.randomAlphanumeric(32));
								tempPass = "password";
								OpenStackConnector osc = new OpenStackConnector(iaasUser, adminTenant, iaasPass,
										keyStoneUserAPI, iaasUser, adminTenant, iaasPass,
										keyStoneAdminAPI, "ImageCreate");
								
								//creates a user with name:vepuserid and email=vepuserid@vep, using the same tenant as the administrator, with a predefined role as "user"
								try
								{
									String openstackUUID = osc.addUser(tempUser, tempPass, vepUserId, vepUserId+"@vep", tenantId, roleId);
									if(openstackUUID != null)
									{
										db.insert("accountmap(cloudtype, iaasuser, iaaspass, iaasuid, vepuser)", "(" + cloudid + ", '" + tempUser + "', '" + tempPass + "', '" + openstackUUID + "', " + vepUserId + ")");
										logger.debug("inserted user "+tempUser+" for vepuserid "+vepUserId+" in openstack cloud id "+cloudid);
										return true;
									} else {
										logger.error("could not create user in OpenStack cloud");
									}
								} catch(Exception ex) {
									logger.error("Cloud id " + cloudid + " looks down, trying the next one...");
								}
							} 
						}
					}
				}           
			}
			return false;
		} catch (Exception ex) {
			logger.warn("Exception caught inside doMapping function.");
			//if(logger.isDebugEnabled())
			//	ex.printStackTrace(System.err);
			logger.debug("Exception Caught: ", ex);
			return false;
		}
	}
}
