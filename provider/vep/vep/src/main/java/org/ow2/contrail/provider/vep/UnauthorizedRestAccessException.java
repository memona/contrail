/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

/**
 *
 * @author fgaudenz
 */
public class UnauthorizedRestAccessException extends Exception{

    public UnauthorizedRestAccessException(String message) {
        super(message);
    }
    
    
}
