package org.ow2.contrail.provider.vep;

public class OpenNebulaVM 
{
	public String rawMsg;
	public String vmID; //ID
	public String name; //NAME
	public String state; //STATE
	public String lcm_state; //LCM_STATE
	public String net_tx; //NET_TX
	public String net_rx; //NET_RX
	public String graphics_host; //LISTEN
	public String graphics_port; //PORT
	public String graphics_type; //TYPE
	public String nic_ip; //IP
	public String nic_mac; //MAC
	public String start_time; //STIME
	public String state_value;
	public String lcmstate_value;
	
	public OpenNebulaVM()
	{
		rawMsg = "";
		vmID = "";
		name = "";
		state = "";
		lcm_state = "";
		net_tx = "";
		net_rx = "";
		graphics_host = "";
		graphics_port = "";
		graphics_type = "";
		nic_ip = "";
		nic_mac = "";
		start_time = "";
		state_value = "";
		lcmstate_value = "";
	}
}
