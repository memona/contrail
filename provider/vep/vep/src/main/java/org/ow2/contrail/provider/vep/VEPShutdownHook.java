package org.ow2.contrail.provider.vep;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class VEPShutdownHook 
{
	public void attachShutdownHook()
	{
		Runtime.getRuntime().addShutdownHook(new Thread(){
			   @Override
			   public void run()
			   {
				   //this is the portion of the code that runs when the program shuts down
				   System.out.println("Inside Shutdown Hook - Performing process cleanup before termination.");
				   HashMap<String, Object> services = VEPHelperMethods.getServicesMap();
				   Set<String> keys = services.keySet();
				   Collection<Object> keyval = services.values();
				   Iterator<String> keyIter = keys.iterator();
				   Iterator<Object> valIter = keyval.iterator();
				   while(keyIter.hasNext() && valIter.hasNext())
				   {
					   String key = keyIter.next();
					   Object value = valIter.next();
					   System.out.println("Processing service: " + key);
				   }
			   }
			  });
			  System.out.println("Shut Down Hook Attached.");
	}
}
