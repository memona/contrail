# Initial setup 
class  vep($db=undef,$vepkeystore=undef,$running_dir=undef) {
   include deploywar
   include mysql
      notify {'vep::database overwriting':}
    notify {'vep::syncronization':}  
    notify{'vep::default initialazing':}
   notify{'vep::certificate overwriting':}
   notify{'vep::create imageRepository folder':}
   
   notice("setting up vep ${user_home}")
   exec{"init-vep":
      command => "java -jar ${running_dir}vep.jar -d",
      path    => "/usr/local/bin/:/usr/bin/",
      before => [Notify['vep::default initialazing'],Exec['sync']],
      }
   if($db!=undef){
    
     # overwrite database
    file { "vep_db": 
 #       notice("overwrite the vep database"),
        path=>"${user_home}/.vep/vep.db",
        source=>$db,
        owner=>root,
	ensure => "present",
	before=>[Notify['vep::database overwriting'],Exec["sync"]],
    }
   }
   
    #imageRepositoryDefault.path=
     # create a directory      
    file { "vepImageFolder":
        path=>"/opt/images",
	ensure => "directory",
	before=>Notify['vep::create imageRepository folder'],
    }
   
   if($vepkeystore!=undef){
     file { "vep_cert": 
 #       notice("overwrite the vep database"),
        path=>"${user_home}/.vep/VEPRestKeyStore.jks",
        source=>$vepkeystore,
	ensure => "present",
	require=>Exec["init-vep"],
	before=> Notify['vep::certificate overwriting'],
    }   
   }
   exec{"sync":
 #  notice("scheduler syncronization")
    command => "java -jar ${running_dir}vep.jar -u>schedueler.log",
 path    => "/usr/local/bin/:/usr/bin/",
    require => [Class["deploywar"],Class["mysql"]],
    before=> Notify['vep::syncronization'],
    }
} 

  
   
