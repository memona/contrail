# hardware_platform.rb

Facter.add("user_home") do
  setcode do
    Facter::Util::Resolution.exec('echo $HOME')
  end
end
