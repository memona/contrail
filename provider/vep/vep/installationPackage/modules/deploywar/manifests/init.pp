class deploywar($namewar,$war=undef,$version=$tomcatVersion) inherits deploywar::params {
notice("${webappdir}/${namewar}")
include tomcat

file{$war:
path=>"${webappdir}/${namewar}",
ensure=>present,
source=>$war,
require=>Class["tomcat"],
}

}
