#!/bin/bash

wget http://apt.puppetlabs.com/puppetlabs-release-precise.deb
sudo dpkg -i puppetlabs-release-precise.deb
sudo apt-get update
sudo apt-get install puppet

folder=$(pwd)
echo $folder
while [ $# -gt 0 ]
do
case "$1" in 

        -d | --database )      
                                db="$2"
                                shift 2
                                ;;

        -s | --private_key )  
                                private_key+="$2"
                                shift 2
                                ;;
        -p  | --public_key )
                                public_key="$2"
                                shift 2
                                ;;
        -c  | --cacert )
                                public_ca="$2"
                                shift 2
                                ;;
        -k | --ca_key)          private_ca="$2"
                                shift 2
	                        ;;

        *) echo "error"
           exit -1
           ;;
    esac
done



if [ -n "$private_key" ]; then
echo "here1"
cp "$private_key" files/vep_local.key
fi
if [ -n "$public_key" ]; then
echo "here2"
cp "$public_key" files/vep_local.crt
fi
if [ -n "$public_ca" ]; then
echo "here3"
cp "$public_ca"  files/ca.crt
fi
if [ -n "$db" ]; then
echo "here4"
echo "{ \"dbvep\" : \"$db\",\"installation_dir\": \"$folder\" }" > 'installation/hiera/common.json'
else

echo "{ \"installation_dir\": \"$folder\" }" > 'installation/hiera/common.json'
fi
sudo puppet apply --debug --modulepath=modules --hiera_config=hiera.yaml manifests/default.pp

