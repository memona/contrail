#!/bin/bash


if [ ! -d "certificates" ]; then
 mkdir certificates
fi
openssl genrsa -out certificates/$1.key
openssl req -new -key certificates/$1.key -out certificates/$1.csr
openssl x509 -req -days 365 -in certificates/$1.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out certificates/$1.crt
openssl pkcs12 -export -inkey certificates/$1.key  -in certificates/$1.crt  -name certificates/$1 -out certificates/$1.pfx
rm certificates/$1.key
rm certificates/$1.crt

