package move;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author fgaudenz
 */
public class StartClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        
        //check folder
        String defaultPath = System.getProperty("user.home") + System.getProperty("file.separator") + 
    			".moveServerVep" + System.getProperty("file.separator");
    	File dir = new File(defaultPath);
    	System.out.println("Trying to locate the MoveFileServerVEP system folder ...");
    	if(!dir.exists())
    	{
    		System.out.print(" ... MoveFileServerVEP configuration directory not found. Creating one now: ");
    		boolean result = dir.mkdir();  
    	    if(result)
    	    {    
    	       System.out.println("done.");  
    	    }
    	    else
    	    {
    	    	System.out.println("error! You may experience problem with MoveFileServerVEP module.");  
    	    }
    	}
    	else
    	{
    		System.out.println(" ... MoveFileServerVEP system folder located!");
    	}
        
        
        
        
        
        
        //set properties
        dir = new File(defaultPath+"/transferModuleVep.propreties");
        boolean logFilePresent=false;
        Properties pp=new Properties();
        if(!dir.exists()){    
            try {
                pp.setProperty("user","admin");
                pp.setProperty("password","pass1234");
                pp.setProperty("port","1556");
                pp.setProperty("temporary_memory","");
        System.out.println("Properties has to be set at "+dir.toString());
        FileOutputStream fos;
        pp.store((fos = new FileOutputStream(dir)), "Author: Filippo Gaudenzi");
        fos.close();
        return;
            } catch (Exception ex) {
                   System.out.println("Somthing went wrong - EXIT -1");
                return;
            }
       
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        //set logger
        dir = new File(defaultPath+"log4j.properties");
        //logFilePresent=dir.exists();
        Properties log4j=new Properties();
            
            try {
                log4j.setProperty("log4j.rootLogger", "info, R");
    

            log4j.setProperty("log4j.appender.R", "org.apache.log4j.RollingFileAppender");
            log4j.setProperty("log4j.appender.R.File","/var/tmp/move.log");
            // Keep up to 10 backup file
            log4j.setProperty("log4j.appender.R.MaxFileSize", Integer.toString(1000) + "KB");
            log4j.setProperty("log4j.appender.R.layout", "org.apache.log4j.PatternLayout");
            log4j.setProperty("log4j.appender.R.layout.ConversionPattern", "%d{ISO8601} %-16.16t %-5p %-15.15c{1} - %m%n");
        
      FileOutputStream fos;
   log4j.store((fos = new FileOutputStream(dir)), "Author: Filippo Gaudenzi");
    fos.close();
        
            } catch (Exception ex) {
                System.out.println("Somthing went wrong - EXIT -1");
                return;
            }
        
        
        
        
        Logger  rootlogger = Logger.getLogger("MoveFileServerVEP");
        rootlogger.setLevel(Level.ALL);
       
        org.apache.log4j.LogManager.resetConfiguration();
        PropertyConfigurator.configure(defaultPath+"/log4j.properties");
        //System.out.println(defaultPath+"/log4j.properties");
        //PropertyConfigurator.configure(log4j);
        Logger logger = Logger.getLogger("MoveFileServerVEP.start");
        
        
        
        
        
        
      //  rootlogger.
        
        
        
        
        //check prperties
        
        rootlogger.info("StartServer");
       
        // ONEImageServer server= new ONEImageServer("transferModuleVep.propreties");
         ONEImageServer.startCLIserver(defaultPath+"/transferModuleVep.propreties");
    }
}
