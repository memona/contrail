/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package move;

import java.io.*;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fgaudenz
 */
public class ONEImageCopy {

    String source;
    String destination;
     private ConnectionProperties connPP;
    public ONEImageCopy(ConnectionProperties connPP) {
        this.connPP=connPP;
    }

    public String move(String source, String destination, ServerSocket ssock) throws MalformedURLException, FileNotFoundException, IOException {
        System.out.println("source:"+source+" destination:"+destination);
        if(destination.startsWith("cert:")){
            //put the certificate in the right folder!!
           System.out.println("managing certificate");
            //String app[]=destination.split(" ");
            //String vmslot=app[2];
             String destinationCert=connPP.getCertPath();
             destination=destination.replaceAll("cert://", "");
             
             
             destination=destinationCert+"/"+destination;
            
            
             File dir=new File(destination);  
                                if ((dir != null) && dir.exists() && dir.isDirectory()) {
                                    System.out.println("EXIST DIRECTORY");
                                    deleteInside(dir);
                                    dir.mkdir();
                                }else{
                                    System.out.println("CREATE DIRECTORY");
                                    dir.mkdir();
                                }
                                
             destination+="/certificate.cert";
             System.out.println(destination);
        }
        
        
        
        
        
        
        String[] ctrl = source.split("http://");
        //ftp??
        
        if (ctrl.length > 1) {
            System.out.println("http");
            URL u;
            InputStream is = null;
            BufferedInputStream bis;
            String s;
            u = new URL(source);

            //----------------------------------------------//
            // Step 3:  Open an input stream from the url.  //
            //----------------------------------------------//

            is = u.openStream();         // throws an IOException

            //-------------------------------------------------------------//
            // Step 4:                                                     //
            //-------------------------------------------------------------//
            // Convert the InputStream to a buffered DataInputStream.      //
            // Buffering the stream makes the reading faster; the          //
            // readLine() method of the DataInputStream makes the reading  //
            // easier.                                                     //
            //-------------------------------------------------------------//

            bis = new BufferedInputStream(is);

            //------------------------------------------------------------//
            // Step 5:                                                    //
            //-----------------------------------------------------//
            // Now just read each record of the input stream, and print   //
            // it out.  Note that it's assumed that this problem is run   //
            // from a command-line, not from an application or applet.    //
            //------------------------------------------------------------//

            FileOutputStream fout = new FileOutputStream(destination);

            byte data[] = new byte[1024];
            int count;
            while ((count = bis.read(data, 0, 1024)) != -1) {
                fout.write(data, 0, count);
            }
            bis.close();
            fout.close();


            System.out.println("source:"+source+" destination:"+destination);
           

        } else {

            /*
             * Activate socket stream
             */
            System.out.println("PROBLEM WITH STREAM:"+source.startsWith("stream:"));
            if (source.startsWith("stream:")) {

                
                
                
                
               destination=destination.replaceAll("_stream:", "_cert.cert");
               System.out.println("stream");



                /*
                 * Activate socket stream
                 */
                Socket sock=ssock.accept();
                InputStream is = sock.getInputStream();
                BufferedReader in = new BufferedReader(new InputStreamReader(is));
                int bufferSize = sock.getReceiveBufferSize();

               
                //open new socket to stream down the certificate
                
                
                FileOutputStream fos = new FileOutputStream(destination);
                BufferedOutputStream bos = new BufferedOutputStream(fos);



                byte[] bytes = new byte[bufferSize];

                int count;

                while ((count = is.read(bytes)) > 0) {
                    bos.write(bytes, 0, count);
                }

                bos.flush();
                bos.close();

                sock.close();
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                 System.out.println("SCARICATO");
                
            } else {


 System.out.println("copying file");




                //Open a stream of bytes to read the file bytes into the program
                FileInputStream instream = new FileInputStream(source);
                //A stream of bytes from the program to the destination
                FileOutputStream outstream = new FileOutputStream(destination);
                byte[] buffer = new byte[4096];
                int bytesRead;
                //Write the bytes from the inputstream to the outputstream
                while ((bytesRead = instream.read(buffer)) != -1) {
                    outstream.write(buffer, 0, bytesRead);
                }



                // Finally close both streams to save garbage disposal memory
                instream.close();
                outstream.close();


                /*  String filename = source;
                 //is a file
                 FileInputStream fis = new FileInputStream(filename);

                 FileOutputStream fos = new FileOutputStream(destination);

                 byte[] dati = new byte[fis.available()];
                 fis.read(dati);
                 fos.write(dati);

                 fis.close();
                 fos.close();*/
                 System.out.println("source:"+source+" destination:"+destination);
                
            }
        }
        return destination;
       
    }

    public String download(String source, String destination, Socket sock) throws MalformedURLException, FileNotFoundException, IOException {
        String[] ctrl = source.split("http://");
        //ftp??

        if (ctrl.length > 1) {

            URL u;
            InputStream is = null;
            BufferedInputStream bis;
            String s;
            u = new URL(source);

            //----------------------------------------------//
            // Step 3:  Open an input stream from the url.  //
            //----------------------------------------------//

            is = u.openStream();         // throws an IOException

            //-------------------------------------------------------------//
            // Step 4:                                                     //
            //-------------------------------------------------------------//
            // Convert the InputStream to a buffered DataInputStream.      //
            // Buffering the stream makes the reading faster; the          //
            // readLine() method of the DataInputStream makes the reading  //
            // easier.                                                     //
            //-------------------------------------------------------------//

            bis = new BufferedInputStream(is);

            //------------------------------------------------------------//
            // Step 5:                                                    //
            //-----------------------------------------------------//
            // Now just read each record of the input stream, and print   //
            // it out.  Note that it's assumed that this problem is run   //
            // from a command-line, not from an application or applet.    //
            //------------------------------------------------------------//

            FileOutputStream fout = new FileOutputStream(destination);

            byte data[] = new byte[1024];
            int count;
            while ((count = bis.read(data, 0, 1024)) != -1) {
                fout.write(data, 0, count);
            }
            bis.close();
            fout.close();



            return "http";

        } else {

            /*
             * Activate socket stream
             */

            InputStream is = sock.getInputStream();
            int bufferSize = sock.getReceiveBufferSize();


            FileOutputStream fos = new FileOutputStream("destination");
            BufferedOutputStream bos = new BufferedOutputStream(fos);



            byte[] bytes = new byte[bufferSize];

            int count;

            while ((count = is.read(bytes)) > 0) {
                bos.write(bytes, 0, count);
            }

            bos.flush();
            bos.close();



          
            return "stream";
        }

    }

    public String oneImagePath() {
        String env = "IMAGE_REPOSITORY_PATH";
        String value = System.getenv(env);
        if (value != null) {
            System.out.format("%s=%s%n",
                    env, value);
        } else {
            env = "ONE_LOCATION";
            value = System.getenv(env) + "/var/images";
        }
        return value;
    }
    /* public String oneNFSPath(){
     String buf;    
     String value="";
     String filename = source;
     //is a file
     File file = new File(FILENAME);
     Scanner sc;
     try {
     sc = new Scanner(file);
     while( sc.hasNextLine() ) {
     buf= sc.nextLine();
     System.out.println(buf);
     String[] app=buf.split("=");
     if(app[0].trim().compareTo("NFS")==0){
     return app[1];
     }
            
     }
     } catch (FileNotFoundException ex) {
     Logger.getLogger(ONEImageCopy.class.getName()).log(Level.SEVERE, null, ex);
     }
 
            
     return "/";
     }*/

    public int countByte(String f1) throws Exception {
        FileInputStream fis = new FileInputStream(f1);
        byte[] dati = new byte[fis.available()];
        return dati.length;
    }
    
    private static void deleteInside(File file) throws IOException {
          if(file.isDirectory()){
 
    		//directory is empty, then delete it
    		if(file.list().length==0){
 
    		   return;
    		 //  System.out.println("Directory is deleted : " 
                   //                              + file.getAbsolutePath());
 
    		}else{
 
    		   //list all the directory contents
        	   String files[] = file.list();
 
        	   for (String temp : files) {
        	      //construct the file structure
        	      File fileDelete = new File(file, temp);
 
        	      //recursive delete
        	     deleteInside(fileDelete);
        	   }
 
        	   //check the directory again, if empty then delete it
        	   if(file.list().length==0){
           	     file.delete();
        	     //System.out.println("Directory is deleted : " 
                       //                           + file.getAbsolutePath());
        	   }
    		}
 
    	}else{
    		//if file, then delete it
    		file.delete();
    		//System.out.println("File is deleted : " + file.getAbsolutePath());
    	}
    
     }
    
    
   
    
    
   
    
    
    
    
    
    
    
    
    
    
    
}
