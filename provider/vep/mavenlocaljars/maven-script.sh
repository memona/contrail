#!/bin/bash
# This script registers the three non standard jars into the local maven repository
# Make sure that the Dversion values correspond to the correct values used in the maven project dependency list

clear				# clear terminal window
echo "Trying to register the non standard JARs into your local maven repo."

#mvn install:install-file -Dfile=OpenNebulaOVFParser.jar -DgroupId=fr.rennes.myriads.contrail -DartifactId=ovfparser -Dversion=1.0.3 -Dpackaging=jar
mvn install:install-file -Dfile=org.opennebula3.client.jar -DgroupId=org.opennebula3.client -DartifactId=oneclient -Dversion=3.4 -Dpackaging=jar
mvn install:install-file -Dfile=org.opennebula.client.jar -DgroupId=org.opennebula.client -DartifactId=oneclient -Dversion=2.2 -Dpackaging=jar

echo "The script run is over. Returning to the terminal prompt now."
echo
