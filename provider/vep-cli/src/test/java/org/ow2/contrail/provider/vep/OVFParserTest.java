/*
 * Copyright 2012 Contrail Consortium.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ow2.contrail.provider.vep;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import junit.framework.TestCase;
//import org.ow2.contrail.federation.opennebula.OpenNebulaOVFParser_VEP1;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.hamcrest.Matchers.*;
import org.ow2.contrail.common.exceptions.MalformedOVFException;
import org.ow2.contrail.common.ovf.opennebula.OpenNebulaOVFParser_VEP1;
import org.xml.sax.SAXException;

/**
 *
 * @author fdudouet
 */
public class OVFParserTest extends TestCase {
    
    public OVFParserTest() {
        super();
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    
    public void testParser() {
        
            FileReader fr;	
            String ovfValue = "";
            try {
                fr = new FileReader("src/test/resources/ovf2.xml");
                BufferedReader br = new BufferedReader(fr);
                StringBuffer sb = new StringBuffer();
                while(br.ready()){
                    sb.append(br.readLine());
                }
                ovfValue = sb.toString();
            } catch (FileNotFoundException ex) {
                fail("Test ovf file not found");
            } catch (IOException ex) {
                fail("Test ovf file could not be read");
            }
            if (ovfValue.equals(""))
                    fail("Test ovf file could not be read");
            
        try {
            OpenNebulaOVFParser_VEP1 ovfParser = new OpenNebulaOVFParser_VEP1(ovfValue);
            assertEquals(2, ovfParser.getCount());
            assertEquals("Contrail_Test_Application", ovfParser.getApplicationName());
            String[] ovfIds = ovfParser.getIDs();
            String ovf = "NAME = \"Ubuntu MySQL DB\" \n"+
                            "CPU = 1 \n"+
                            "VCPU = 1 \n"+
                            "MEMORY = 512\n"+
                            "OS = [ boot=\"hd\", arch=\"x86_64\" ]\n"+
                            "NIC = [NETWORK=\"test-net\"]\n"+
                            "GRAPHICS = [\n"+
                            "type=\"vnc\",\n"+
                            "listen=\"localhost\"\n"+
                            "]\n"+
                            "DISK = [IMAGE=\"ubuntu-1110\"]\n";
            Assert.assertThat(ovfParser.getVMTemplate("Ubuntu MySQL DB"), Matchers.containsString(ovf));
            
            assertEquals("NAME=\"ubuntu-1110\"\n"+
                            "PATH=/srv/cloud/images/ubuntu_1110.img", 
                        ovfParser.getImageTemplate("Ubuntu MySQL DB","ubuntu-1110"));
            assertEquals("/srv/cloud/images/ubuntu_1110.img", ovfParser.getImageDiskPath("Ubuntu MySQL DB","ubuntu-1110"));
            
            
        } catch (URISyntaxException ex) {
            fail("URI Syntax Exception");
        } catch (ParserConfigurationException ex) {
            fail("ParserConfigurationException");
        } catch (FileNotFoundException ex) {
            fail("File not Found Exception");
        } catch (SAXException ex) {
            fail("SAXException");
        } catch (IOException ex) {
            fail("IOException");
        } catch (XPathExpressionException ex) {
            fail("XPathExpressionException");
        } catch (MalformedOVFException ex) {
            fail("MalformedOVFException");
        }
    }
}
