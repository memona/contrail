/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.log4j.Logger;
import org.opennebula.client.Client;
import org.opennebula.client.OneResponse;
import org.opennebula.client.host.HostPool;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author piyush, florian
 */
public class CLIServer extends DefaultHandler implements Runnable //CLI Server is a blocking server, handles only one client at a time
{
    private int port;
    private boolean continueFlag;
    private Thread t;
    private Logger logger;
    private ServerSocket server;
    private Socket client;
    private dbHandler dbhandle;
    private PrintWriter out;
    private BufferedReader in;
    private String vepProperties;
    
    private LinkedList<ONEHost> rowList;
    private ONEHost tempHost;
    private String tempVal;
    private ClusterStats runningStat;
    private List<String> nodes;
    
    public CLIServer(int sPort, dbHandler dhandle, String vepProp) //make sure CLI server is started only once DB operations are enabled
    {
        port = sPort;
        dbhandle = dhandle;
        vepProperties = vepProp;
        continueFlag = true;
        logger = Logger.getLogger("VEP.CLIServer");
        client = null;
        try
        {
            server = new ServerSocket(port);
            t = new Thread(this);
        }
        catch(IOException ex)
        {
            t = null;
            server = null;
            logger.debug("Error while trying to create CLI server socket, maybe the port is not free: " + ex.getMessage());
            ex.printStackTrace(System.err);
        }
    }
    
    public void startCLIserver()
    {
        if(t != null)
        {
            continueFlag = true;
            t.start();
        }
    }
    
    public void stopCLIserver()
    {
        continueFlag = false;
        //just send a dummy connect if server is blocking on client incoming request
        try
        {
            if(client != null && client.isConnected()) client.close();
            Socket sock = new Socket("127.0.0.1", port);
            sock.close();
            server.close();
            t.join(1000);
        }
        catch(Exception ex)
        {
            
        }
    }
    
    public void run() 
    {
        logger.debug("CLI Server has started ...");
        while(continueFlag && (server != null))
        {       
            try
            {
                client = server.accept();
                client.setSoTimeout(1000*60*2); //2 minutes idle time
                out = new PrintWriter(client.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                
                boolean authPhase = true;
                boolean passPhase = false;
                boolean isAuthenticated = false;
                boolean forceQuit = false;
                String username = "";
                String password = "";
            
                String output = "Welcome to Contrail VEP Command Line Interface!\n";
                output += "You must be authenticated before you are granted access to modify the execution of VEP.\n";
                output += "------------------------------------\n";
                output += "Administrator Username: ";
                
                out.println(output);
                String userInput  = in.readLine();
                
                while(!forceQuit && !client.isClosed() && userInput != null && !userInput.equalsIgnoreCase("bye") && !userInput.equalsIgnoreCase("exit")) 
                {
                    output = "";
                    logger.debug("CLI incoming message: " + userInput);
                    if(authPhase)
                    {
                        username = userInput.trim();
                        authPhase = false;
                        passPhase = true;
                        output = "------------------------------------\nAdministrator " + username + "'s password: ";
                    }
                    else if(passPhase)
                    {
                        password = userInput.trim();
                        passPhase = false;
                        isAuthenticated = doAdminAuth(username, password, out, in);
                        username = "";
                        password = "";
                        if(isAuthenticated)
                            output = "------------------------------------\n"
                                    + "Authentication successful! List of commands:\n"
                                    + "\t show configuration\n"
                                    + "\t edit configuration\n"
                                    + "\t list hosts\n"
                                    + "\t add host\n"
                                    + "\t add fedadmin\n"
                                    + "\t add datacenter\n"
                                    + "\t add cluster\n"
                                    + "\t add rack\n"
                                    + "\t add admin\n"
                                    + "\t help\n"
                                    + "\t exit\n"
                                    + "------------------------------------";
                    }
                    else if(isAuthenticated)
                    {
                        if(userInput.equalsIgnoreCase("help"))
                            output = showHelp();
                        else if(userInput.equalsIgnoreCase("show configuration"))
                        {
                            output = "------------------------------------\n"
                                    + readConfiguration()
                                    + "------------------------------------";
                        }
                        else if(userInput.equalsIgnoreCase("edit configuration"))
                        {
                            editConfiguration(out, in);
                        }
                        else if(userInput.equalsIgnoreCase("add datacenter"))
                        {
                            addDatacenter(out, in);
                        }
                        else if(userInput.equalsIgnoreCase("add cluster"))
                        {
                            addCluster(out, in);
                        }                        
                        else if(userInput.equalsIgnoreCase("add rack"))
                        {
                            addRack(out, in);
                        }     
                        else if(userInput.equalsIgnoreCase("list hosts"))
                        {
                            listHost(out, in);
                        }
                        else if(userInput.equalsIgnoreCase("add host"))
                        {
                            addHost(out, in);
                        }
                        else if(userInput.equalsIgnoreCase("add fedadmin"))
                        {
                            addFedAdmin(out, in);
                        }
                        else if(userInput.equalsIgnoreCase("add admin"))
                        {
                            addAdmin(out, in);
                        }
                        else
                        {
                            output = "------------------------------------\n"
                                    + "\tNot yet implemented! You may type help or exit ...\n"
                                    + "------------------------------------";
                        }
                    }
                    else
                    {
                        output = "You do not have the proper authorization to continue.";
                        forceQuit = true;
                    }
                    out.println(output);
                    if(!forceQuit)
                        userInput  = in.readLine();
                }
                client.close();
            }
            catch(SocketException sockex)
            {
                logger.warn("CLI client read error, closing connection: " + sockex.getMessage());
                try
                {
                    if(client != null && client.isConnected())
                    {
                        out.println("VEP server shutting down. Closing connection now ...");
                        client.close();
                    }
                }
                catch(Exception ex)
                {
                    //ignore this exception
                }
            }
            catch(SocketTimeoutException timeout)
            {
                logger.warn("CLI client timeout, closing connection: " + timeout.getMessage());
                try
                {
                    if(client != null && client.isConnected())
                    {
                        out.println("You have been inactive for more than 2 minutes. For security reasons this connection will be closed now.");
                        client.close();
                    }
                }
                catch(Exception ex)
                {
                    //ignore this exception
                }
            }
            catch(Exception ex)
            {
                logger.warn("CLI server encountered an exception: " + ex.getMessage());
                ex.printStackTrace(System.err);
            }
        }
        logger.debug("CLI Server exitting now ...");
    }
    
    private String showHelp()
    {
        String output = "------------------------------------\nList of commands:\n"
            + "\t show configuration\n"
            + "\t edit configuration\n"
            + "\t list hosts\n"
            + "\t add host\n"
            + "\t add fedadmin\n"
            + "\t add datacenter\n"
            + "\t add cluster\n"
            + "\t add rack\n"
            + "\t add admin\n"
            + "\t help\n"
            + "\t exit\n"
            + "------------------------------------";
        return output;
    }
    
    private String readConfiguration()
    {
        String output = "";
        try
        {
//            BufferedReader fileReader = new BufferedReader(new FileReader("vep.properties"));
            BufferedReader fileReader = new BufferedReader(new FileReader(vepProperties));
            String strLine = "";
            while ((strLine = fileReader.readLine()) != null)
            {
                output += strLine + "\n";
            }
            fileReader.close();
        }
        catch(Exception ex)
        {
            logger.warn("Caught exception in CLI module while retrieving the VEP properties: " + ex.getMessage());
            ex.printStackTrace(System.err);
        }
        return output;
    }
    
    private boolean doAdminAuth(String user, String pass, PrintWriter out, BufferedReader in)
    {
        try
        {
            String userInput = "";
            String adminUser = "";
            String adminPass = "";
            ResultSet rs = dbhandle.query("select", "count(*)", "vepadmin", "");
            if(rs.next())
            {
                int count = rs.getInt(1);
                if(count < 1)
                {
                    rs.close();
                    out.println("It seems no local administrator has been setup! Would you like to setup one now (yes/no)?");
                    userInput  = in.readLine();
                    if(userInput.equalsIgnoreCase("yes"))
                    {
                        out.println("New administrator username:");
                        userInput  = in.readLine().trim();
                        if(!userInput.contains(" \t!@#$%^&*()+-=~`;:'\",<>?/") && userInput.length() > 0)
                        {
                            adminUser = userInput;
                            out.println("Provide a password for the new administrator [" + adminUser + "]'s account: ");
                            userInput  = in.readLine().trim();
                            adminPass = VEPHelperMethods.makeSHA1Hash(userInput);
                            return dbhandle.insert("vepadmin", "('" + adminUser + "', '" + adminPass + "')");
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    rs.close();
                }
            }
            //now proceed with the argument's validation
            rs = dbhandle.query("select", "*", "vepadmin", "where username='" + user + "'");
            if(rs.next())
            {
                adminPass = VEPHelperMethods.makeSHA1Hash(pass);
                if(rs.getString("password").equalsIgnoreCase(adminPass))
                {
                    rs.close();
                    logger.trace("Admin Authentication successful.");
                    return true;
                }
                else
                {
                    rs.close();
                    logger.trace("Admin Authentication failed.");
                    return false;
                }
            }
            else
            {
                rs.close();
                return false;
            }
        }
        catch(Exception ex)
        {
            logger.warn("Caught exception while performing admin authentication inside the CLI process: " + ex.getMessage());
            ex.printStackTrace(System.err);
            return false;
        }
    }

    private void editConfiguration(PrintWriter out, BufferedReader in) {
        try{
            String editChoice = "";
            String userInput = "";
            out.println("\nChoose an option to edit: ");
            while(!editChoice.equalsIgnoreCase("q")){
                out.println("\n[d]b configuration, [l]og configuration, [o]pen nebula configuration, [c]luster configuration, [r]est configuration, [q]uit");
                editChoice = in.readLine();
                if(editChoice.equalsIgnoreCase("d")){
                    out.println("\nChoose DB type: [m]ysql, [s]qlite. Current choice is "+VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if (userInput.equalsIgnoreCase("m"))
                        VEPHelperMethods.addProperty("vepdb.choice", "mysql", logger, vepProperties);
                    else if (userInput.equalsIgnoreCase("s"))
                        VEPHelperMethods.addProperty("vepdb.choice", "sqlite", logger, vepProperties);
                    out.println("\nConfiguration of the chosen database");
                    if (VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties).equals("sqlite")){
                        out.println("\nConfigure path to the database: "+VEPHelperMethods.getProperty("sqlite.db", logger, vepProperties)+".");
                        userInput = in.readLine();
                        if(!userInput.equals("")){
                            VEPHelperMethods.addProperty("sqlite.db", userInput, logger, vepProperties);
                        }
                    }
                    else if (VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties).equals("mysql")){
                        out.println("\nConfigure IP of the database: "+VEPHelperMethods.getProperty("mysql.ip", logger, vepProperties)+".");
                        userInput = in.readLine();
                        if(!userInput.equals("")){
                            VEPHelperMethods.addProperty("mysql.ip", userInput, logger, vepProperties);
                        }
                        out.println("\nConfigure port of the database: "+VEPHelperMethods.getProperty("mysql.port", logger, vepProperties)+".");
                        userInput = in.readLine();
                        if(!userInput.equals("")){
                            VEPHelperMethods.addProperty("mysql.port", userInput, logger, vepProperties);
                        }
                        out.println("\nConfigure chosen login of the database: "+VEPHelperMethods.getProperty("mysql.user", logger, vepProperties)+".");
                        userInput = in.readLine();
                        if(!userInput.equals("")){
                            VEPHelperMethods.addProperty("mysql.user", userInput, logger, vepProperties);
                        }
                        out.println("\nConfigure password of the chosen user of the database: "+VEPHelperMethods.getProperty("mysql.pass", logger, vepProperties)+".");
                        userInput = in.readLine();
                        if(!userInput.equals("")){
                            VEPHelperMethods.addProperty("mysql.pass", userInput, logger, vepProperties);
                        }
                    }
                    out.println("\nDB configured, going back to the first menu");
                } 
                else if(editChoice.equalsIgnoreCase("l")){
                    out.println("\nConfigure path to the log file: "+VEPHelperMethods.getProperty("veplog.file", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("veplog.file", userInput, logger, vepProperties);
                    }     
                    out.println("\nConfigure max size to the log file: "+VEPHelperMethods.getProperty("veplog.size", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("veplog.size", userInput, logger, vepProperties);
                    }
                    out.println("\nLog file configured, going back to the first menu");
                }
                else if(editChoice.equalsIgnoreCase("o")){
                    out.println("\nConfigure OpenNebula server's ip: "+VEPHelperMethods.getProperty("one.ip", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("one.ip", userInput, logger, vepProperties);
                    }    
                    out.println("\nConfigure OpenNebula server's port: "+VEPHelperMethods.getProperty("one.port", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("one.port", userInput, logger, vepProperties);
                    }                       
                    out.println("\nConfigure OpenNebula user: "+VEPHelperMethods.getProperty("one.user", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("one.user", userInput, logger, vepProperties);
                    }                       
                    out.println("\nConfigure OpenNebula chosen user's password: "+VEPHelperMethods.getProperty("one.pass", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("one.pass", userInput, logger, vepProperties);
                    }                       
                    out.println("\nOpenNebula configured, going back to the first menu");
                }
                else if(editChoice.equalsIgnoreCase("c")){
                    out.println("\nConfigure OpenNebula cluster id for contrail: "+VEPHelperMethods.getProperty("contrail.cluster", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("contrail.cluser", userInput, logger, vepProperties);
                    }   
                    out.println("\nCluster id configured, going back to the first menu");
                }
                else if(editChoice.equalsIgnoreCase("r")){
                    out.println("\nConfigure REST keystore: "+VEPHelperMethods.getProperty("rest.keystore", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("rest.keystore", userInput, logger, vepProperties);
                    }                       
                    out.println("\nConfigure REST keystore password: "+VEPHelperMethods.getProperty("rest.keystorepass", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("rest.keystorepass", userInput, logger, vepProperties);
                    }                   
                    out.println("\nConfigure REST certificate's password: "+VEPHelperMethods.getProperty("rest.keypass", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("rest.keypass", userInput, logger, vepProperties);
                    }     
                    out.println("\nConfigure REST HTTP Port: "+VEPHelperMethods.getProperty("rest.restHTTPPort", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("rest.restHTTPPort", userInput, logger, vepProperties);
                    }
                    out.println("\nConfigure REST HTTPS Port: "+VEPHelperMethods.getProperty("rest.restHTTPSPort", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("rest.restHTTPSPort", userInput, logger, vepProperties);
                    }
                    out.println("\nConfigure REST mandatory authentication: "+VEPHelperMethods.getProperty("rest.clientAuthSelected", logger, vepProperties)+".");
                    userInput = in.readLine();
                    if(!userInput.equals("")){
                        VEPHelperMethods.addProperty("rest.clientAuthSelected", userInput, logger, vepProperties);
                    }
                    out.println("\nREST Server configured, going back to the first menu");
                }
                else
                    out.println("\nExiting configuration's edit...");
            }
            logger.info("Configured local properties file");
        }
        catch(Exception ex){
            logger.warn("Caught exception while editing configuration inside the CLI process: " + ex.getMessage());
            ex.printStackTrace(System.err);
        }
    }

    private void addDatacenter(PrintWriter out, BufferedReader in) {
        boolean exit = false;
        String userInput = "";
        String[] codes = new String[] { "AFGHANISTAN ,AF", "ÅLAND ISLANDS ,AX", "ALBANIA ,AL", "ALGERIA ,DZ", "AMERICAN SAMOA ,AS", "ANDORRA ,AD", "ANGOLA ,AO", "ANGUILLA ,AI", "ANTARCTICA ,AQ", "ANTIGUA AND BARBUDA ,AG", "ARGENTINA ,AR", "ARMENIA ,AM", "ARUBA ,AW", "AUSTRALIA ,AU", "AUSTRIA ,AT", "AZERBAIJAN ,AZ ",
                    "BAHAMAS ,BS", "BAHRAIN ,BH", "BANGLADESH ,BD", "BARBADOS ,BB", "BELARUS ,BY", "BELGIUM ,BE", "BELIZE ,BZ", "BENIN ,BJ", "BERMUDA ,BM", "BHUTAN ,BT", "BOLIVIA; PLURINATIONAL STATE OF ,BO", "BONAIRE; SINT EUSTATIUS AND SABA ,BQ", "BOSNIA AND HERZEGOVINA ,BA", "BOTSWANA ,BW", "BOUVET ISLAND ,BV", "BRAZIL ,BR", "BRITISH INDIAN OCEAN TERRITORY ,IO", "BRUNEI DARUSSALAM ,BN", "BULGARIA ,BG",
                    "BURKINA FASO ,BF", "BURUNDI ,BI ", "CAMBODIA ,KH", "CAMEROON ,CM", "CANADA ,CA", "CAPE VERDE ,CV", "CAYMAN ISLANDS ,KY", "CENTRAL AFRICAN REPUBLIC ,CF","CHAD ,TD", "CHILE ,CL", "CHINA ,CN", "CHRISTMAS ISLAND ,CX", "COCOS (KEELING) ISLANDS ,CC", "COLOMBIA ,CO", "COMOROS ,KM", "CONGO ,CG", "CONGO; THE DEMOCRATIC REPUBLIC OF THE ,CD", "COOK ISLANDS ,CK", "COSTA RICA ,CR", "CÔTE D'IVOIRE ,CI",
                    "CROATIA ,HR", "CUBA ,CU", "CURAÇAO ,CW", "CYPRUS ,CY", "CZECH REPUBLIC ,CZ ", "DENMARK ,DK", "DJIBOUTI ,DJ", "DOMINICA ,DM", "DOMINICAN REPUBLIC ,DO ", "ECUADOR ,EC", "EGYPT ,EG", "EL SALVADOR ,SV", "EQUATORIAL GUINEA ,GQ", "ERITREA ,ER", "ESTONIA ,EE", "ETHIOPIA ,ET ", "FALKLAND ISLANDS (MALVINAS) ,FK","FAROE ISLANDS ,FO", "FIJI ,FJ", "FINLAND ,FI", "FRANCE ,FR", "FRENCH GUIANA ,GF",
                    "FRENCH POLYNESIA ,PF", "FRENCH SOUTHERN TERRITORIES ,TF ", "GABON ,GA", "GAMBIA ,GM", "GEORGIA ,GE", "GERMANY ,DE", "GHANA ,GH", "GIBRALTAR ,GI", "GREECE ,GR", "GREENLAND ,GL", "GRENADA ,GD", "GUADELOUPE ,GP", "GUAM ,GU", "GUATEMALA ,GT", "GUERNSEY ,GG", "GUINEA ,GN", "GUINEA-BISSAU ,GW", "GUYANA ,GY ", "HAITI ,HT", "HEARD ISLAND AND MCDONALD ISLANDS ,HM", "HOLY SEE (VATICAN CITY STATE) ,VA",
                    "HONDURAS ,HN", "HONG KONG ,HK", "HUNGARY ,HU ", "ICELAND ,IS", "INDIA ,IN", "INDONESIA ,ID", "IRAN; ISLAMIC REPUBLIC OF ,IR", "IRAQ ,IQ", "IRELAND ,IE", "ISLE OF MAN ,IM", "ISRAEL ,IL", "ITALY ,IT ", "JAMAICA ,JM", "JAPAN ,JP", "JERSEY ,JE", "JORDAN ,JO ", "KAZAKHSTAN ,KZ", "KENYA ,KE", "KIRIBATI ,KI", "KOREA; DEMOCRATIC PEOPLE'S REPUBLIC OF ,KP", "KOREA; REPUBLIC OF ,KR", "KUWAIT ,KW", "KYRGYZSTAN ,KG ",
                    "LAO PEOPLE'S DEMOCRATIC REPUBLIC ,LA", "LATVIA ,LV", "LEBANON ,LB", "LESOTHO ,LS", "LIBERIA ,LR", "LIBYAN ARAB JAMAHIRIYA ,LY", "LIECHTENSTEIN ,LI", "LITHUANIA ,LT", "LUXEMBOURG ,LU ", "MACAO ,MO", "MACEDONIA; THE FORMER YUGOSLAV REPUBLIC OF ,MK", "MADAGASCAR ,MG", "MALAWI ,MW", "MALAYSIA ,MY", "MALDIVES ,MV", "MALI ,ML", "MALTA ,MT", "MARSHALL ISLANDS ,MH", "MARTINIQUE ,MQ", "MAURITANIA ,MR", "MAURITIUS ,MU",
                    "MAYOTTE ,YT", "MEXICO ,MX", "MICRONESIA; FEDERATED STATES OF ,FM", "MOLDOVA; REPUBLIC OF ,MD", "MONACO ,MC", "MONGOLIA ,MN", "MONTENEGRO ,ME", "MONTSERRAT ,MS", "MOROCCO ,MA", "MOZAMBIQUE ,MZ", "MYANMAR ,MM ", "NAMIBIA ,NA", "NAURU ,NR", "NEPAL ,NP", "NETHERLANDS ,NL", "NEW CALEDONIA ,NC", "NEW ZEALAND ,NZ", "NICARAGUA ,NI", "NIGER ,NE", "NIGERIA ,NG", "NIUE ,NU", "NORFOLK ISLAND ,NF", "NORTHERN MARIANA ISLANDS ,MP",
                    "NORWAY ,NO ", "OMAN ,OM ", "PAKISTAN ,PK", "PALAU ,PW", "PALESTINIAN TERRITORY; OCCUPIED ,PS", "PANAMA ,PA", "PAPUA NEW GUINEA ,PG", "PARAGUAY ,PY", "PERU ,PE", "PHILIPPINES ,PH", "PITCAIRN ,PN", "POLAND ,PL", "PORTUGAL ,PT", "PUERTO RICO ,PR ", "QATAR ,QA ", "RÉUNION ,RE", "ROMANIA ,RO", "RUSSIAN FEDERATION ,RU", "RWANDA ,RW ", "SAINT BARTHÉLEMY ,BL", "SAINT HELENA; ASCENSION AND TRISTAN DA CUNHA ,SH",
                    "SAINT KITTS AND NEVIS ,KN", "SAINT LUCIA ,LC", "SAINT MARTIN (FRENCH PART) ,MF", "SAINT PIERRE AND MIQUELON ,PM", "SAINT VINCENT AND THE GRENADINES ,VC", "SAMOA ,WS", "SAN MARINO ,SM", "SAO TOME AND PRINCIPE ,ST", "SAUDI ARABIA ,SA", "SENEGAL ,SN", "SERBIA ,RS", "SEYCHELLES ,SC", "SIERRA LEONE ,SL", "SINGAPORE ,SG", "SINT MAARTEN (DUTCH PART) ,SX", "SLOVAKIA ,SK", "SLOVENIA ,SI", "SOLOMON ISLANDS ,SB",
                    "SOMALIA ,SO", "SOUTH AFRICA ,ZA", "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS ,GS", "SPAIN ,ES", "SRI LANKA ,LK", "SUDAN ,SD", "SURINAME ,SR", "SVALBARD AND JAN MAYEN ,SJ ", "SWAZILAND ,SZ", "SWEDEN ,SE", "SWITZERLAND ,CH", "SYRIAN ARAB REPUBLIC ,SY ", "TAIWAN; PROVINCE OF CHINA ,TW", "TAJIKISTAN ,TJ", "TANZANIA; UNITED REPUBLIC OF ,TZ", "THAILAND ,TH", "TIMOR-LESTE ,TL", "TOGO ,TG", "TOKELAU ,TK", "TONGA ,TO",
                    "TRINIDAD AND TOBAGO ,TT", "TUNISIA ,TN", "TURKEY ,TR", "TURKMENISTAN ,TM", "TURKS AND CAICOS ISLANDS ,TC", "TUVALU ,TV ", "UGANDA ,UG", "UKRAINE ,UA", "UNITED ARAB EMIRATES ,AE", "UNITED KINGDOM ,GB", "UNITED STATES ,US", "UNITED STATES MINOR OUTLYING ISLANDS ,UM", "URUGUAY ,UY", 
                    "UZBEKISTAN ,UZ ", "VANUATU ,VU ", "VENEZUELA; BOLIVARIAN REPUBLIC OF ,VE", "VIET NAM ,VN", "VIRGIN ISLANDS; BRITISH ,VG", "VIRGIN ISLANDS; U.S. ,VI ", "WALLIS AND FUTUNA ,WF", "WESTERN SAHARA ,EH ", "YEMEN ,YE ", "ZAMBIA ,ZM", "ZIMBABWE ,ZW " };
        Set<String> locCodes = new HashSet<String>(Arrays.asList(new String[] {"AF", "AX", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BQ", "BA", "BW", "BV", "BR", "IO", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CG", "CD", "CK", "CR", "CI", "HR", "CU", "CW", "CY", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG",
            "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG", "GN", "GW", "GY", "HT", "HM", "VA", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "JM", "JP", "JE", "JO", "KZ", "KE", "KI", "KP", "KR", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MY", "MV", "ML",
            "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "ME", "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "MP", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RE", "RO", "RU", "RW", "BL", "SH", "KN", "LC", "MF", "PM", "VC", "WS", "SM", "ST", "SA", "SN", "RS", "SC", "SL", "SG", "SX", "SK", "SI", "SB", "SO", "ZA", "GS", "ES",
            "LK", "SD", "SR", "SJ", "SZ", "SE", "CH", "SY", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB", "US", "UM", "UY", "UZ", "VU", "VE", "VN", "VG", "VI", "WF", "EH", "YE", "ZM", "ZW"}));
        
        try {
            while (!exit)
            {
                String dname = "";
                String dloc = "";
                String ddesc = "";
                out.println("\nAdd a new datacenter:");
                out.println("\nEnter a name - enter nothing to exit");
                userInput = in.readLine();
                if(userInput.equals(""))
                    exit = true;
                else 
                {
                    out.println("\nAdding a new datacenter called "+userInput);
                    dname = userInput;
                    while (dloc.equals("") && !exit)
                    {
                        out.println("\nEnter the code of its location (2 letters): Enter c to get a list of codes, otherwise enter the chosen code (2 letters)");
                        userInput = in.readLine();
                        if (userInput.equals("")){
                           exit = true;  
                           out.println("\nExiting... ");
                        }
                        else if(userInput.equalsIgnoreCase("c"))
                           out.println(Arrays.toString(codes));
                       else if(userInput.length() != 2)
                           out.println("\nCode should be 2 letters' length");
                       else if(!locCodes.contains(userInput.toUpperCase()))
                           out.println("\nthis code is not in the authorized list");
                       else 
                       {
                           out.println("\nChosen code: "+userInput);
                           dloc = userInput;
                           out.println("\nEnter a description on one line (facultative):");
                           ddesc = in.readLine();
                           out.println("\nAdding a new datacenter with these values:\nName: "+dname+"\nLoc: "+dloc+"\nDesc:"+ddesc);
                           userInput = "";
                           boolean recheck = true;
                           while ((userInput == null)||recheck){
                               out.println("\nConfirm? (y)es/(n)o");
                               userInput = in.readLine();
                               if(userInput.equalsIgnoreCase("y") || userInput.equalsIgnoreCase("n"))
                                   recheck = false;
                           }
                           if(userInput.equalsIgnoreCase("y")){
                                ResultSet rs = dbhandle.query("select", "max(did)", "datacenter", "");
                                int did = 0;
                                if(rs.next())
                                    did = rs.getInt(1) + 1;
                                logger.debug("New datacenter id to be assigned: " + did);
                                boolean status = dbhandle.insert("datacenter", "(" + did + ", '" + dloc.toUpperCase() + "', '" + dname + "', '" + ddesc + "')");
                                if(status)
                                {
                                    out.println("\nDatacenter was added successfully with datacenter ID: " + did + "\n");
                                    logger.debug("Datacenter was added successfully with datacenter ID: " + did);
                                }
                                else
                                {
                                        logger.error("Datacenter " + did + " addition failed. Error during insert operation into datacenter table.");
                                        exit = true;
                                }
                           }
                           else
                           {
                               exit = true;
                               out.println("\nExiting...");
                           }
                       }
                    }
               }
            }
        } catch (Exception ex) {
            logger.error("Caught exception while adding a datacenter inside the CLI process: " + ex.getMessage());
            ex.printStackTrace(System.err);            
        }
    }

    private void addCluster(PrintWriter out, BufferedReader in) {
        boolean exit = false;
        String userInput = "";
        String[] codes = new String[] { "000, no selection", "001, Gigabit Ethernet", "002, Gigabit Ethernet (Level 5 NICs)", "003, 10 Gigabit Ethernet", "004, Infiniband", "005, Infinipath", "006, Myrinet", "007, QsNet (Quadrics)", "008, SCI (Dolphin)" };
        Set<String> nics = new HashSet(Arrays.asList(new String[]{"000","001","002","003","004","005","006","007","008"}));
        Map<String, String> dcenters = new HashMap<String,String>();
        try {
            int count = 0;
            ResultSet rs = dbhandle.query("SELECT", "did, name", "datacenter", "");
            while(rs.next())
            {
                dcenters.put(Integer.toString(rs.getInt("did")), rs.getString("name"));
                count++;
            }
            if(count == 0)
            {
                logger.warn("No datacenter was found.");
                out.println("\nThere must be at least 1 datacenter to connect this cluster to, please add one.");
                exit = true;
            }
            while(!exit){
                String cname = "";
                String cnic = "";
                String did = "";
                String cdesc = "";
                out.println("\nAdd a new cluster:");
                out.println("\nEnter a name - enter nothing to exit");
                userInput = in.readLine();
                if(userInput.equals(""))
                    exit = true;
                else 
                {
                    out.println("\nAdding a new cluster called "+userInput);
                    cname = userInput;
                    while(cnic.equals("") && !exit){
                        out.println("\nChoose interconnection:  Enter c to get a list of codes, otherwise enter the chosen code (3 digits)");
                        userInput = in.readLine();
                        if (userInput.equals("")){
                           exit = true;  
                           out.println("\nExiting... ");
                        }
                        else if(userInput.equalsIgnoreCase("c"))
                           out.println(Arrays.toString(codes));
                       else if(userInput.length() != 3)
                           out.println("\nCode should be 3 digits' length");
                       else if(!nics.contains(userInput.toUpperCase()))
                           out.println("\nthis code is not in the authorized list");
                       else 
                       {
                           out.println("\nChosen code: "+userInput);
                           cnic = userInput;
                           while(did.equals("") && !exit){
                               out.println("\nChoose the datacenter this cluster belongs to: Enter c to get a list of codes, otherwise enter the chosen code (id)");
                               userInput = in.readLine();
                               if (userInput.equals("")){
                                  exit = true;   
                                  out.println("\nExiting... ");
                               }
                               else if(userInput.equalsIgnoreCase("c"))
                              {
                                  out.println("\nid : name");
                                  for (Map.Entry<String, String> e : dcenters.entrySet())
                                      out.println(e.getKey()+" : "+e.getValue());
                              }
                              else if(!dcenters.containsKey(userInput))
                                  out.println("\nthis code is not in the authorized list");
                              else
                              {
                                  out.println("\nChosen datacenter: "+userInput+", "+dcenters.get(userInput));
                                  did = userInput;
                                  out.println("\nEnter a description on one line (facultative):");
                                  cdesc = in.readLine();

                                  out.println("\nAdding a new cluster with these values:\r\nName: "+cname+"\r\nInterconnection: "+cnic
                                          +"\r\nDatacenter: "+did+"\r\nDesc:"+cdesc);
                                  userInput = "";
                                  boolean recheck = true;
                                  while ((userInput == null)||recheck){
                                      out.println("\nConfirm? (y)es/(n)o");
                                      userInput = in.readLine();
                                      if(userInput.equalsIgnoreCase("y") || userInput.equalsIgnoreCase("n"))
                                        recheck = false;
                                  }
                                  if(userInput.equalsIgnoreCase("y")){
                                        rs = dbhandle.query("select", "max(clid)", "cluster", "");
                                        int clid = 0;
                                        if(rs.next())
                                            clid = rs.getInt(1) + 1;
                                        logger.debug("New cluster id to be assigned: " + did);
                                        boolean status = dbhandle.insert("cluster", "(" + clid + ", '" + cnic + "', " + did + ", '" + cdesc + "', '" + cname + "')");
                                        if(status)
                                        {
                                            out.println("\nCluster was added successfully with cluster ID: " + clid + "\n");
                                            logger.debug("Cluster was added successfully with cluster ID: " + clid);
                                        }
                                        else
                                        {
                                            logger.error("cluster " + clid + " addition failed. Error during insert operation into datacenter table.");
                                            exit = true;
                                        }
                                  }
                                  else
                                  {
                                      exit = true;
                                      out.println("\nExiting...");
                                  }
                               }
                           }
                       }
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Caught exception while adding a cluster inside the CLI process: " + ex.getMessage());
            ex.printStackTrace(System.err);                
        }
    }

    private void addRack(PrintWriter out, BufferedReader in) {
        boolean exit = false;
        String userInput = "";
        Map<String, String> clusters = new HashMap<String,String>();
        try {
            int count = 0;
            ResultSet rs = dbhandle.query("SELECT", "clid, name", "cluster", "");
            while(rs.next())
            {
                clusters.put(Integer.toString(rs.getInt("clid")), rs.getString("name"));
                count++;
            }
            if(count == 0)
            {
                logger.warn("No cluster was found.");
                out.println("\nThere must be at least 1 cluster to connect this rack to, please add one.");
                exit = true;
            }
            while(!exit){
                String rname = "";
                String clid = "";
                String rdesc = "";
                out.println("\nAdd a new datacenter:");
                out.println("\nEnter a name - enter nothing to exit");
                userInput = in.readLine();
                if(userInput.equals(""))
                    exit = true;
                else 
                {
                    out.println("\nAdding a new rack called "+userInput);
                    rname = userInput;
                    while(clid.equals("") && !exit){
                        out.println("\nChoose the cluster this rack belongs to: Enter c to get a list of codes, otherwise enter the chosen code (id)");
                        userInput = in.readLine();
                        if (userInput.equals("")){
                            exit = true;   
                            out.println("\nExiting... ");
                        }
                        else if(userInput.equalsIgnoreCase("c"))
                        {
                            out.println("\nid : name");
                            for (Map.Entry<String, String> e : clusters.entrySet())
                                out.println(e.getKey()+" : "+e.getValue());
                        }
                        else if(!clusters.containsKey(userInput))
                            out.println("\nthis code is not in the authorized list");
                        else
                        {
                            out.println("\nChosen cluster: "+userInput+", "+clusters.get(userInput));
                            clid = userInput;
                            out.println("\nEnter a description on one line (facultative):");
                            rdesc = in.readLine();

                            out.println("\nAdding a new rack with these values:\r\nName: "+rname+"\r\ncluster: "+clid+"\r\nDesc:"+rdesc);
                            userInput = "";
                            boolean recheck = true;
                            while ((userInput == null)||recheck){
                                out.println("\nConfirm? (y)es/(n)o");
                                userInput = in.readLine();
                                if(userInput.equalsIgnoreCase("y") || userInput.equalsIgnoreCase("n"))
                                    recheck = false;
                            }
                            if(userInput.equalsIgnoreCase("y")){
                                rs = dbhandle.query("select", "max(rid)", "rack", "");
                                int rid = 0;
                                if(rs.next())
                                    rid = rs.getInt(1) + 1;
                                logger.debug("New rack id to be assigned: " + rid);
                                boolean status = dbhandle.insert("rack", "(" + rid + ", " + clid + ", '" + rname + "', '" + rdesc + "')");
                                if(status)
                                {
                                    out.println("\nRack was added successfully with rack ID: " + clid + "\n");
                                    logger.debug("Rack was added successfully with rack ID: " + clid);
                                }
                                else
                                {
                                    logger.error("rack " + rid + " addition failed. Error during insert operation into rack table.");
                                    exit = true;
                                }
                            }
                            else
                            {
                                exit = true;
                                out.println("\nExiting...");
                            }
                        }
                    }                    
                }
            }
        } catch (Exception ex) {
            logger.error("Caught exception while adding a rack inside the CLI process: " + ex.getMessage());
            ex.printStackTrace(System.err);    
        }                
    }
    
    /**
     * Gets all available nodes from provider
     * @return List of nodes' description
     */
    private List<String> getNodes(){
        //TODO : Actually add XML parsing
        //based on PeriodicThread. Why use a direct connection to one instead of the onexmlrpchandler class?
        Client oneClient;
        String oneUser = VEPHelperMethods.getProperty("one.user", logger, vepProperties);
        String onePass = VEPHelperMethods.getProperty("one.pass", logger, vepProperties);
        String oneIP = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
        String onePort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
        if (oneUser!= null && onePass!=null && oneIP!=null && onePort!=null){
            try
            {
                oneClient  = new Client(oneUser + ":" + onePass, "http://" + oneIP + ":" + onePort + "/RPC2");
            }
            catch(Exception ex)
            {
                logger.error("Unable to connect to OpenNebula. Check system properties and try again.");
                oneClient = null;
            }
            if(oneClient != null)
            {
                OneResponse val = HostPool.info(oneClient);
                String response = "";
                if(!val.isError())
                {
                    response = val.getMessage();
                }
                else
                {
                    logger.warn("OpenNebula XML-RPC connection error. Check OpenNebula connection seetings under system properties. Check if oned is running.");
                }
                //parse the XML response and populate the table next
                if(response.length() > 1)
                {
                    //parse XML here
                    createTable(response, this.nodes);
                }
            }
        } 
        else
        {
            logger.warn("One or more of the needed properties to connect to OpenNebula are missing.");
        }
        return this.nodes;
    }

    private void listHost(PrintWriter out, BufferedReader in) {
        boolean exit = false;
        String userInput = "";
        nodes = new ArrayList<String>();
        try {
            while(!exit){
                out.println("\nListing hosts");
                //TODO: Change text of next line
                out.println("1:List all available nodes registered on provider");
                out.println("2:List all nodes already in the federation");
                out.println("q:Quit listing");
                userInput = in.readLine();
                if(userInput.equals("1"))
                {
                    //TODO: Test getNodes method
                    nodes = getNodes();
                }
                else if(userInput.equals("2"))
                {
                    nodes = getFederationNodes();
                }
                else if(userInput.equals("q")){
                    out.println("\nExiting...");
                    exit = true;
                }
                if (!nodes.isEmpty()){
                    out.println("\nListing...");
                    out.println("NodeID : Hostname : CPU : Memory");
                    for (String node : nodes)
                        out.println(node);
                    nodes.clear();
                }
            }
        } catch (Exception ex) {
            logger.error("Caught exception while listing hosts inside the CLI process: " + ex.getMessage());
            ex.printStackTrace(System.err);                
        }
    }
    
    /**
     * Get Nodes already in federation
     * @return List of nodes' description
     */
    private List<String> getFederationNodes() {
        List<String> nodes = new ArrayList<String>();
        try {
            ResultSet rs = dbhandle.query("select", "cid,hostname,cpu,mem", "computenode", "");
            while (rs.next()){
                nodes.add(Integer.toString(rs.getInt("cid"))+" : " + rs.getString("hostname")+" : "+Integer.toString(rs.getInt("cpu"))+" : "+Integer.toString(rs.getInt("mem")));
            }
        } catch (Exception ex) {
            logger.error("Caught exception while getting federation nodes inside the CLI process: " + ex.getMessage());
            ex.printStackTrace(System.err);   
        }
        return nodes;
    }
    
       private void addHost(PrintWriter out, BufferedReader in) {
        boolean exit = false;
        ONExmlrpcHandler oneHandle = null;
        try
        {
            String oneUser = VEPHelperMethods.getProperty("one.user", logger, vepProperties);
            String onePass = VEPHelperMethods.getProperty("one.pass", logger, vepProperties);
            String oneIP = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
            String onePort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
            if (oneUser!= null && onePass!=null && oneIP!=null && onePort!=null){
                oneHandle = new ONExmlrpcHandler(oneIP, onePort, oneUser, onePass, "CLIServer:AddHost");
            } else {
                exit = true;
            }
           
            while (!exit)
            {
                String userInput = "";
                out.println("\nAdd a host : You will need hostname, information driver, virtualization driver, storage driver");

                String hostname = "";
                while(hostname.equals("") && !exit){
                    out.println("\nEnter host name, enter 'exit' to exit");
                    userInput = in.readLine();
                    if(userInput.equals("exit"))
                    {
                        exit = true;
                        out.println("Exiting...");
                    }
                    else if(!userInput.equals(""))
                    {
                        hostname = userInput;
                        userInput = "";
                        String infoD = "";
                        while(infoD.equals("") && !exit)
                        {
                            out.println("\nEnter information driver, enter 'exit' to exit");
                            userInput = in.readLine();
                            //TODO : provide list, for now it assumes user knows what he's doing
                            if(userInput.equals("exit"))
                            {
                                exit = true;
                                out.println("Exiting...");
                            }
                            else if(!userInput.equals(""))
                            {
                                infoD = userInput;
                                userInput = "";
                                String virtD = "";
                                while(virtD.equals("") && !exit)
                                {
                                    out.println("\nEnter virtualization driver, enter 'exit' to exit");
                                    userInput = in.readLine();
                                    if(userInput.equals("exit"))
                                    {
                                        exit = true;
                                        out.println("Exiting...");
                                    }
                                    else if(!userInput.equals(""))
                                    {
                                        virtD = userInput;
                                        userInput = "";
                                        String storD = "";
                                        while(storD.equals("") && !exit)
                                        {
                                            out.println("\nEnter storage driver, enter 'exit' to exit");
                                            userInput = in.readLine();
                                            if(userInput.equals("exit"))
                                            {
                                                exit = true;
                                                out.println("Exiting...");
                                            }
                                            else if(!userInput.equals(""))
                                            {
                                                storD = userInput;
                                                userInput = "";
                                                out.println("\nthis host will be added:");
                                                out.println(hostname+" : "+infoD+" : "+virtD+" : "+storD);
                                                boolean recheck = true;
                                                while ((userInput == null)||recheck)
                                                {
                                                    out.println("\nConfirm? (y)es/(n)o");
                                                    userInput = in.readLine();
                                                    if(userInput.equalsIgnoreCase("y") || userInput.equalsIgnoreCase("n"))
                                                        recheck = false;
                                                }
                                                if(userInput.equalsIgnoreCase("y"))
                                                {
                                                    boolean status = oneHandle.addHost(hostname, infoD, virtD, storD);
                                                    if(status)
                                                        out.println("Host was added successfully");
                                                    else
                                                        out.println("Host addition failed, please check the logs for more information");
                                                }
                                                else
                                                {
                                                    out.println("\nCancelling...");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch(Exception ex) {
            logger.error("Caught exception while getting federation nodes inside the CLI process: " + ex.getMessage());
            ex.printStackTrace(System.err);   
        }
    }

    /**
     * Add node to federation
     * @param out
     * @param in 
     */
    private void addFedAdmin(PrintWriter out, BufferedReader in) {
        
        ResultSet rs;
        VEPComputeNode VCNode = new VEPComputeNode();
        Map<String, String> rackList = new HashMap<String, String>();
        Map<String, String> vorgList = new HashMap<String, String>();
        Map<String, String> clMngrMap = new HashMap<String, String>();
        clMngrMap.put("-1", "No manager selected");
        clMngrMap.put("1", "OpenNebula");
        clMngrMap.put("2", "OpenStack");
        clMngrMap.put("3", "Nimbus");
        clMngrMap.put("4", "Eucalyptus");
       
        boolean exit = false;
        String oneUser = VEPHelperMethods.getProperty("one.user", logger, vepProperties);
        String onePass = VEPHelperMethods.getProperty("one.pass", logger, vepProperties);
        String oneIP = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
        String onePort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
        if (oneUser!= null && onePass!=null && oneIP!=null && onePort!=null){
            try {
                ONExmlrpcHandler oneHandle = new ONExmlrpcHandler(oneIP, onePort, oneUser, onePass, "CLIServer:AddFedAdmin");
                //check to see if cluster contrail is present or not
                String clusterId = VEPHelperMethods.getProperty("contrail.cluster", logger, vepProperties);
                int id = -1;
                if(clusterId == null || clusterId.length() == 0 || Integer.parseInt(clusterId) < 0)
                {
                    LinkedList<ONECluster> clusterList = oneHandle.getClusterList();
                    boolean found = false;
                    for(int i=0; i< clusterList.size(); i++)
                    {
                        if(clusterList.get(i).name.equalsIgnoreCase("contrail"))
                        {
                            found = true;
                            id = clusterList.get(i).id;
                            break;
                        }
                    }
                    if(!found)
                    {
                        id = oneHandle.addCluster("contrail");
                        if(id == -1)
                            logger.error("Could not create the contrail cluster. Check ONE settings and try again.");
                        else
                        {
                            boolean status = VEPHelperMethods.addProperty("contrail.cluster", Integer.toString(id), logger, vepProperties);
                            if(status == false)
                                logger.error("Could not insert contrail.cluster property in the settings file.");
                        }
                    }
                    else
                    {
                        boolean status = VEPHelperMethods.addProperty("contrail.cluster", Integer.toString(id), logger, vepProperties);
                        if(status == false)
                            logger.error("Could not insert contrail.cluster property in the settings file.");
                    }
                }
                else
                {
                    try
                    {
                        id = Integer.parseInt(clusterId);
                    }
                    catch(Exception ex)
                    {
                        logger.error("Format error in value contrail.cluster. Fix error and try again.");
                        exit = true;
                        return;
                    }
                }
                //now test to see if cluster is actually present
                String response = oneHandle.getClusterInfo(id);
                //if cluster is present then add this node to contrail cluster
                if(response != null && response.contains("contrail"))
                {
                    while (!exit)
                    {
                        String userInput = "";
                        String rackId = "";
                        String vorgId = "";
                        String clManager  = "";
                        String hostid = "";
                        String description = "";
                        while(!exit && hostid.equals("")){
                            out.println("Enter a hostid to add to federation, enter exit to exit:");
                            userInput = in.readLine();
                            if (userInput.equalsIgnoreCase("exit")){
                                exit = true;
                                out.println("Exiting...");
                            }
                            else
                            {
                                //test node existence
                                int hid = -1;
                                try{
                                    hid = Integer.parseInt(userInput);
                                }catch(Exception e){
                                    out.println("Caught exception while parsing the host id, exiting...");
                                    logger.error("Caught exception while parsing the host id, exiting...");
                                    e.printStackTrace(System.err);
                                    exit = true;
                                }
                                if(!exit)
                                {
                                    oneHandle.getHostInfo(Integer.parseInt(userInput), null, VCNode);
                                    if(VCNode.hostname == null)
                                        out.println("This host id does not exist, please try again"); 
                                    else
                                    {
                                        hostid = userInput;
                                        //test node existence
                                        oneHandle.getHostInfo(Integer.parseInt(hostid), null, VCNode);

                                        //Enter rack info
                                        // Do not update the rack list if already there, it should not be needed
                                        if (rackList.isEmpty()){
                                            rs = dbhandle.query("SELECT", "rid, name", "rack", "");
                                            int count = 0;
                                            while(rs.next())
                                            {
                                                String rid = Integer.toString(rs.getInt("rid"));
                                                String name = rs.getString("name");
                                                logger.trace("Composing racklist: found rack " + rid + "," + name);
                                                rackList.put(rid,name);
                                                count++;

                                            }
                                            if(count == 0){
                                                //There is no rack in the db, add one first
                                                out.println("There is no rack in the database, please register one");
                                                exit = true;
                                            }
                                        }
                                        //If rackList contains some elements

                                        while (!exit && rackId.equals(""))
                                        {
                                            out.println("\nChoose the rack id of this node : Enter c to get a list of codes, exit to exit, otherwise enter the chosen code (id)");
                                            userInput = in.readLine();
                                            if (userInput.equalsIgnoreCase("exit")){
                                                exit = true;
                                                out.println("Exiting...");
                                            }
                                            else if(userInput.equalsIgnoreCase("c")){
                                                //output the rack list
                                                out.println("\rid : name");
                                                for (Map.Entry<String, String> e : rackList.entrySet())
                                                    out.println(e.getKey()+" : "+e.getValue());
                                            }
                                            else 
                                            {
                                                rackId = userInput;
                                                out.println("\nChosen rack: "+userInput+", "+rackList.get(userInput));

                                                // Now to vorg choice
                                                // Populate vorgList if empty
                                                if (vorgList.isEmpty()){
                                                    rs = dbhandle.query("select DISTINCT", "vid, name", "vorg", "");
                                                    int count = 0;
                                                    while(rs.next())
                                                    {
                                                        String vid = Integer.toString(rs.getInt("vid"));
                                                        String name = rs.getString("name");
                                                        logger.trace("Composing vorglist: found vorg " + vid + "," + name);
                                                        vorgList.put(vid,name);
                                                        count++;
                                                    }
                                                    vorgList.put("-1", "No VOrg assigned");
                                                }
                                                //If vorgList contains some elements
                                                while (vorgId.equals("") && !exit)
                                                {
                                                    out.println("\nChoose the vorg id of this node : Enter c to get a list of codes, exit to exit, otherwise enter the chosen code (id)");
                                                    userInput = in.readLine();
                                                    if (userInput.equalsIgnoreCase("exit")){
                                                        exit = true;
                                                        out.println("Exiting...");
                                                    }
                                                    else if(userInput.equalsIgnoreCase("c")){
                                                        //output the vorg list
                                                        out.println("\nvid : name");
                                                        for (Map.Entry<String, String> e : vorgList.entrySet())
                                                            out.println(e.getKey()+" : "+e.getValue());
                                                    }
                                                    else 
                                                    {
                                                        vorgId = userInput;
                                                        out.println("\nChosen vorg: "+userInput+", "+vorgList.get(userInput));

                                                        //Now enter Cluster Manager
                                                        while(!exit && clManager.equals(""))
                                                        {
                                                            out.println("\nChoose the cluster manager id of this node : Enter c to get a list of codes, exit to exit, otherwise enter the chosen code (id)");
                                                            userInput = in.readLine();
                                                            if (userInput.equalsIgnoreCase("exit")){
                                                                exit = true;
                                                                out.println("Exiting...");
                                                            }
                                                            else if(userInput.equalsIgnoreCase("c")){
                                                                //output the cl mngr list
                                                                out.println("\nclid : name");
                                                                for (Map.Entry<String, String> e : clMngrMap.entrySet())
                                                                    out.println(e.getKey()+" : "+e.getValue());
                                                            }
                                                            else {
                                                                clManager = clMngrMap.get(userInput);
                                                                out.println("\nChosen cluster manager: "+userInput+", "+clMngrMap.get(userInput));

                                                                //add description
                                                                while(!exit && description.equals("")){
                                                                    out.println("Enter a description, exit to exit");
                                                                    userInput = in.readLine();
                                                                    if (userInput.equalsIgnoreCase("exit"))
                                                                    {
                                                                        exit = true;
                                                                        out.println("Exiting...");
                                                                    }
                                                                    else if(!dbhandle.validateSentence(userInput))
                                                                        out.println("Some characters are not allowed, please try again");
                                                                    else
                                                                    {
                                                                        description = userInput;
                                                                        //now do the final checks 
                                                                        //add this host to the cluster contrail and store its information in the VEP computenode table
                                                                        boolean recheck = true;
                                                                        while ((userInput == null)||recheck){
                                                                            out.println("\nConfirm? (y)es/(n)o");
                                                                            userInput = in.readLine();
                                                                            if(userInput.equalsIgnoreCase("y") || userInput.equalsIgnoreCase("n"))
                                                                                recheck = false;
                                                                        }
                                                                        if(userInput.equalsIgnoreCase("y")){
                                                                            try
                                                                            {
                                                                                VCNode.rackId = rackId;
                                                                                VCNode.virtualOrgId = vorgId;
                                                                                VCNode.clusterManagerType = clManager;
                                                                                VCNode.description = description;

                                                                                //Use insert or update from VEPComputeNode
                                                                                //TODO: Test the next 6 lines and uncomment
                                                                                if(VCNode.checkAction(dbhandle))
                                                                                    VCNode.update(dbhandle, logger);
                                                                                else
                                                                                    VCNode.insert(dbhandle, logger);
                                                                                //then one handle add host
                                                                                oneHandle.addHostToCluster(id, Integer.parseInt(hostid));
                                                                            }
                                                                            catch(Exception ex)
                                                                            {
                                                                                out.println("Host " + hostid + " could not be added to contrail cluster in OpenNebula.");
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            out.println("Cancelling...");
                                                                            description = ".";//workaround to not completely exit the loop
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                out.println("Caught exception while adding a node to federation inside the CLI process : ");
                logger.error("Caught exception while adding a node to federation inside the CLI process : ");
                e.printStackTrace(System.err);
                exit = true;
            }
        }
        else
        {
            logger.error("One or more of the needed properties to connect to OpenNebula are missing.");
            out.println("One or more of the needed properties to connect to OpenNebula are missing.");
        }
    }
 
    
    public void createTable(String input, List<String> nodes)
    {
        rowList = new LinkedList<ONEHost>();
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try
        {
            SAXParser sp = spf.newSAXParser();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(input));
            sp.parse(is, this);
        }
        catch(SAXException se)
        {
            if(logger.isDebugEnabled())
                se.printStackTrace(System.err);
            else
                logger.warn(se.getMessage());
	}
        catch(ParserConfigurationException pce)
        {
            if(logger.isDebugEnabled())
                pce.printStackTrace(System.err);
            else
                logger.warn(pce.getMessage());
	}
        catch(IOException ie)
        {
            if(logger.isDebugEnabled())
                ie.printStackTrace(System.err);
            else
                logger.warn(ie.getMessage());
	}
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
    {
        tempVal = "";
        if(qName.equalsIgnoreCase("HOST"))
        {
            tempHost = new ONEHost(); //blank it for new row
	}
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {
	tempVal = new String(ch,start,length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        if(qName.equalsIgnoreCase("HOST"))
        {
            rowList.add(tempHost);
        }
        else if(qName.equalsIgnoreCase("ID"))
        {
            tempHost.id = tempVal;
        }
        else if(qName.equalsIgnoreCase("NAME"))
        {
            tempHost.name = tempVal;
        }
        else if(qName.equalsIgnoreCase("STATE"))
        {
            tempHost.state = tempVal;
        }
        else if(qName.equalsIgnoreCase("IM_MAD"))
        {
            tempHost.im_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("VM_MAD"))
        {
            tempHost.vm_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("TM_MAD"))
        {
            tempHost.tm_mad = tempVal;
        }
        else if(qName.equalsIgnoreCase("LAST_MON_TIME"))
        {
            tempHost.mon_time = tempVal;
        }
        else if(qName.equalsIgnoreCase("CLUSTER"))
        {
            tempHost.cluster = tempVal;
        }
        else if(qName.equalsIgnoreCase("FREE_MEM"))
        {
            tempHost.free_mem = tempVal;
        }
        else if(qName.equalsIgnoreCase("USED_MEM"))
        {
            tempHost.used_mem = tempVal;
        }
        else if(qName.equalsIgnoreCase("FREE_CPU"))
        {
            tempHost.free_cpu = tempVal;
        }
        else if(qName.equalsIgnoreCase("USED_CPU"))
        {
            tempHost.used_cpu = tempVal;
        }
        else if(qName.equalsIgnoreCase("CPUSPEED"))
        {
            tempHost.cpuspeed = tempVal;
        }
        else if(qName.equalsIgnoreCase("MAX_CPU"))
        {
            tempHost.max_cpu = tempVal;
        }
        else if(qName.equalsIgnoreCase("MAX_MEM"))
        {
            tempHost.max_mem = tempVal;
        }
        else if(qName.equalsIgnoreCase("HOST_POOL"))
        {
            String outVal = "";
                       
            //resetting the running stat
            
            for(ONEHost temp : rowList)
            {
                //here also check if in contrail cluster then update the running stats
                if(temp.cluster.equalsIgnoreCase("contrail"))
                {
                    try
                    {
                        runningStat.freeMemory += Double.parseDouble(temp.free_mem);
                        runningStat.usedMemory += Double.parseDouble(temp.used_mem);
                        double cpSpeed = Double.parseDouble(temp.cpuspeed);
                        double maxcpu = Double.parseDouble(temp.free_cpu) + Double.parseDouble(temp.used_cpu);
                        runningStat.freeCPU = (cpSpeed * (maxcpu/100.0)) * (Double.parseDouble(temp.free_cpu)/maxcpu);
                        runningStat.usedCPU = (cpSpeed * (maxcpu/100.0)) * (Double.parseDouble(temp.used_cpu)/maxcpu);
//                        System.out.println("\n" + runningStat.freeMemory + "-" + runningStat.usedMemory + "|" + runningStat.freeCPU + "-" + runningStat.usedCPU + "\n");
                    }
                    catch(Exception ex)
                    {
                        //do nothing, ignore
                    }
                }
               outVal = temp.id + " : " + temp.name + " : " + temp.max_cpu + " : " + temp.max_mem;
                nodes.add(outVal);
                logger.trace("Found host: " + temp.id + " " + temp.name + " " + temp.state + " " + temp.im_mad + " " + temp.vm_mad + " " + temp.tm_mad + " " + temp.cluster + " " + temp.mon_time);
            }
        }
    }
    
    
    private void addAdmin(PrintWriter out, BufferedReader in) {
        boolean exit = false;
        try {
            while(!exit){
                String userInput = "";
                out.println("\nAdd a federation admin to VEP");
                String uname = "";
                while(uname.equalsIgnoreCase("") && !exit)
                {
                    out.println("Enter a name for this account, \"exit\" to exit");
                    userInput = in.readLine();
                    if(userInput.equalsIgnoreCase("exit"))
                    {
                        exit = true;
                        out.println("Exiting...");
                    }
                    else if (!userInput.equals(""))
                    {
                        uname = userInput;
                        out.println("\nA new admin will be created with the name: "+uname);
                        boolean recheck = true;
                        while ((userInput == null)||recheck){
                            out.println("\nConfirm? (y)es/(n)o");
                            userInput = in.readLine();
                            if(userInput.equalsIgnoreCase("y") || userInput.equalsIgnoreCase("n"))
                                recheck = false;
                        }
                        if(userInput.equalsIgnoreCase("y")){
                            int uid = 1;
                            ResultSet rs = dbhandle.query("select", "max(uid)", "user", "");
                            if(rs.next())
                            {
                                uid = rs.getInt(1) + 1;
                                logger.debug("User id to be assigned to the fed-admin account: " + uid + ".");
                            }
                            rs.close();
                            boolean status = dbhandle.insert("user", "('" + uname + "', " + uid + ", '-1', 'NIL', 'NIL', " + -1 + ",  'admin')");
                            if(status)
                            {
                                status = dbhandle.insert("ugroup", "('admin'," + uid + ")");
                                if(!status)
                                {
                                    logger.warn("Could not assign group `admin' to the federation admin account. Try assigning one by hand [ugroup] ...");
                                    out.println("Could not assign group `admin' to the federation admin account. Try assigning one by hand [ugroup] ...");
                                }
                                else
                                {
                                    logger.debug("VEP Admin account setup complete.");
                                    out.println("VEP Admin account setup complete.");
                                }
                            }
                            else
                            {
                                logger.warn("Could not initialize federation admin account. Try creating one by hand [user, ugroup] ...");
                                out.println("Could not initialize federation admin account. Try creating one by hand [user, ugroup] ...");
                            }
                        } else {
                            out.println("Cancelling...");
                        }
                    }
                }

            }
        } catch (Exception ex) {
            logger.error("Caught exception while adding a federation admin inside the CLI process: " + ex.getMessage());
            ex.printStackTrace(System.err);   
        }
    }
}