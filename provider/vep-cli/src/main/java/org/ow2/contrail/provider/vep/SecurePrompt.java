/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.vep;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import org.apache.log4j.Logger;
/**
 *
 * @author Jeff Heaton
 * @version 1.0
 */
/**
 *
 * @extension Piyush Harsh
 * @version 1.0
 */
public final class SecurePrompt extends javax.swing.JDialog 
{
    private String username;
    private String password;
    private Logger logger;
    public SecurePrompt(Frame parent, String title) 
    {
        super(parent, true);

        //{{INIT_CONTROLS
        setTitle(title);
        getContentPane().setLayout(null);
        setSize(330, 120);
        setVisible(false);
        JLabel1.setText("Username:");
        getContentPane().add(JLabel1);
        JLabel1.setBounds(12, 12, 100, 24);
        JLabel2.setText("Password:");
        getContentPane().add(JLabel2);
        JLabel2.setBounds(12, 48, 100, 24);
        _uid.setText("admin");
        getContentPane().add(_uid);
        _uid.setBounds(110, 12, 200, 24);
        _ok.setText("OK");
        getContentPane().add(_ok);
        _ok.setBounds(12, 84, 84, 24);
        getContentPane().add(_pwd);
        _pwd.setBounds(110, 48, 200, 24);
        _cancel.setText("Cancel");
        getContentPane().add(_cancel);
        _cancel.setBounds(100, 84, 84, 24);
        //}}

        //{{REGISTER_LISTENERS
        SymAction lSymAction = new SymAction();
        _ok.addActionListener(lSymAction);
        _cancel.addActionListener(lSymAction);
        //}}
        username = null;
        password = null;
        logger = Logger.getLogger("VEP.SecurePrompt");
    }

    @Override
    public void setVisible(boolean b) 
    {
//        if (b)
//        {
//          setLocation(50, 50);
//        }
        super.setVisible(b);
    }

    @Override
    public void addNotify() 
    {
        // Record the size of the window prior to calling parents addNotify.
        Dimension size = getSize();

        super.addNotify();

        if (frameSizeAdjusted)
        return;
        frameSizeAdjusted = true;

        // Adjust size of frame according to the insets
        Insets insets = getInsets();
        setSize(insets.left + insets.right + size.width, insets.top
        + insets.bottom + size.height);
    }
    
    public String getUserName()
    {
        return username;
    }
    
    public String getPassword()
    {
        String temp = null;
        try
        {
            if(password != null)
            temp = VEPHelperMethods.makeSHA1Hash(password);
        }
        catch(Exception ex)
        {
            logger.error("Error in extracting password.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.error(ex.getMessage());
        }
        return temp;
    }

  // Used by addNotify
  boolean frameSizeAdjusted = false;

  //{{DECLARE_CONTROLS
  javax.swing.JLabel JLabel1 = new javax.swing.JLabel();

  javax.swing.JLabel JLabel2 = new javax.swing.JLabel();

  /**
   * The user ID entered.
   */
  javax.swing.JTextField _uid = new javax.swing.JTextField();

  /**
   */
  javax.swing.JButton _ok = new javax.swing.JButton();

  /**
   * The password is entered.
   */
  javax.swing.JPasswordField _pwd = new javax.swing.JPasswordField();

  javax.swing.JButton _cancel = new javax.swing.JButton();

  //}}

  class SymAction implements java.awt.event.ActionListener {
    public void actionPerformed(java.awt.event.ActionEvent event) {
      Object object = event.getSource();
      if (object == _ok)
        Ok_actionPerformed(event);
      else if (object == _cancel)
        Cancel_actionPerformed(event);
    }
  }

  /**
   * Called when ok is clicked.
   * 
   * @param event
   */
  void Ok_actionPerformed(java.awt.event.ActionEvent event) {
    setVisible(false);
    username = _uid.getText();
    password = String.valueOf(_pwd.getPassword());
  }

  /**
   * Called when cancel is clicked.
   * 
   * @param event
   */
  void Cancel_actionPerformed(java.awt.event.ActionEvent event) {
    _uid.setText("");
    _pwd.setText("");
    setVisible(false);
    username = null;
    password = null;
  }
}