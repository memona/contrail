package eu.contrail.reservations.simple.core;

import java.util.GregorianCalendar;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.junit.Test;

import eu.contrail.reservations.simple.base.Host;
import eu.contrail.reservations.simple.base.Reservation;
import eu.contrail.reservations.simple.base.VirtualMachine;

public class TestReservations extends TestCase {
    Logger logger = Logger.getLogger(TestCase.class);

    @Test
    public void testInitialisation() {
	logger.info("Starting the simple test");

	Timetable timetable = new Timetable();
	Host host1 = new Host("n0002", "10.2.1.2", 4.0, 4);
	Host host2 = new Host("n0004", "10.2.1.4", 18.0, 6);

	assertNotNull("Host 1 not initialised.", host1);
	assertNotNull("Host 2 not initialised.", host2);

	int nSlots = timetable.getSlotCount(host1);
	logger.debug(nSlots + " slots.");
	assertFalse("Time table should not contain any items for Host 1",
		nSlots >= 0);

	timetable.addHost(host1);
	timetable.addHost(host2);

	nSlots = timetable.getSlotCount(host1);
	assertTrue("There should be 0 reservations for Host 1.", nSlots == 0);

	logger.info("Done with the simple test.");
    }

    @Test
    public void testOperations() throws Exception {
	logger.info("Starting the operations test");

	String user1Id = "user001";
	String user2Id = "user002";

	int res1lenMinutes = 35;
	GregorianCalendar now = new GregorianCalendar();
	GregorianCalendar start1 = (GregorianCalendar) now.clone();
	GregorianCalendar end1 = (GregorianCalendar) start1.clone();
	end1.add(GregorianCalendar.MINUTE, res1lenMinutes);

	GregorianCalendar start2 = (GregorianCalendar) end1.clone();
	start2.add(GregorianCalendar.MINUTE, res1lenMinutes);
	GregorianCalendar end2 = (GregorianCalendar) start2.clone();
	end2.add(GregorianCalendar.MINUTE, res1lenMinutes);

	GregorianCalendar startMix = (GregorianCalendar) start1.clone();
	startMix.add(GregorianCalendar.MINUTE, res1lenMinutes >> 1);
	GregorianCalendar endMix = (GregorianCalendar) end2.clone();
	endMix.add(GregorianCalendar.MINUTE, -(res1lenMinutes >> 1));

	VirtualMachine vm1 = new VirtualMachine("vm-deb-02", "10.0.0.22", 512);

	Timetable timetable = new Timetable();
	Host host1 = new Host("n0002", "10.2.1.2", 4.0, 4);
	Host host2 = new Host("n0004", "10.2.1.4", 18.0, 6);

	timetable.addHost(host1);
	timetable.addHost(host2);

	Reservation res1 = new Reservation(user1Id, start1, end1, vm1);
	Reservation res2 = new Reservation(user1Id, start2, end2, vm1);
	Reservation resMix = new Reservation(user1Id, startMix, endMix, vm1);

	assertTrue("Res1 and Res2 should be disjoint.", res1.disjoint(res2));
	assertFalse("Res1 and ResMix should intersect.", res1.disjoint(resMix));
	assertFalse("Res2 and ResMix should intersect.", res2.disjoint(resMix));

	boolean ok = timetable.addReservation(host1, res1);
	assertTrue("First reservation should be added ok.", ok);

	ok = timetable.addReservation(host1, res2);
	assertTrue("Second reservation should be added ok.", ok);

	ok = timetable.addReservation(host1, resMix);
	assertFalse("Mixed reservation should fail.", ok);

	logger.info("Done with the operations test.");
    }
}
