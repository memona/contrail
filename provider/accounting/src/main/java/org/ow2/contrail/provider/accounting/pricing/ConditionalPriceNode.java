package org.ow2.contrail.provider.accounting.pricing;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.db.DBConnector;

import java.util.*;


public class ConditionalPriceNode implements IPriceNode {
    static Logger logger = Logger.getLogger(ConditionalPriceNode.class);

    private static String[] _allowedFields = {"type", "fromDate", "toDate", "entities", "conditional1", "conditional2", "twise_operator", "operator", "limit", "forward", "submodel"};

    public enum OperatorDefinition {
        NDEF("ndef") {
            @Override
            public boolean apply(double x1, double x2) {
                return false;
            }
        },
        EQ("eq") {
            @Override
            public boolean apply(double x1, double x2) {
                return (x1 == x2);
            }
        },
        NEQ("neq") {
            @Override
            public boolean apply(double x1, double x2) {
                return (x1 != x2);
            }
        },
        LT("lt") {
            @Override
            public boolean apply(double x1, double x2) {
                return (x1 < x2);
            }
        },
        GT("gt") {
            @Override
            public boolean apply(double x1, double x2) {
                return (x1 > x2);
            }

        },
        LTE("lte") {
            @Override
            public boolean apply(double x1, double x2) {
                return (x1 <= x2);
            }
        },
        GTE("gte") {
            @Override
            public boolean apply(double x1, double x2) {
                return (x1 >= x2);
            }
        };

        private final String text;

        private OperatorDefinition(String text) {
            this.text = text;
        }

        // Yes, enums *can* have abstract methods. This code compiles...
        public abstract boolean apply(double result, double limit);

        @Override
        public String toString() {
            return text;
        }

    }

    ;

    public enum TwiseOperatorDefinition {
        NDEF("ndef") {
            @Override
            public double apply(double x1, double x2) {
                return Double.NaN;
            }
        },
        SUM("sum") {
            @Override
            public double apply(double x1, double x2) {
                return x1 + x2;
            }
        },
        SUBTRACT("subtract") {
            @Override
            public double apply(double x1, double x2) {
                return x1 - x2;
            }
        },
        MULTIPLY("mul") {
            @Override
            public double apply(double x1, double x2) {
                return x1 * x2;
            }
        },
        DIVIDE("div") {
            @Override
            public double apply(double x1, double x2) {
                if (x2 != 0)
                    return x1 / x2;
                else
                    return Double.NaN;
            }
        },
        MAX("max") {
            @Override
            public double apply(double x1, double x2) {
                if (x1 > x2)
                    return x1;
                else
                    return x2;
            }
        },
        MIN("min") {
            @Override
            public double apply(double x1, double x2) {
                if (x1 < x2)
                    return x1;
                else
                    return x2;
            }
        },;

        private final String text;


        private TwiseOperatorDefinition(String text) {
            this.text = text;
        }

        public abstract double apply(double x1, double x2);

        @Override
        public String toString() {
            return text;
        }
    }

    ;

    public enum ForwardDefinition {
        NDEF("ndef") {
            @Override
            public JSONObject forward(JSONObject obj, double x1, double result, double limit) {
                return Util.copyJSONwithMetric(obj, x1);
            }
        },
        ORIGINAL("original") {
            @Override
            public JSONObject forward(JSONObject obj, double x1, double result, double limit) {
                return Util.copyJSONwithMetric(obj, x1);
            }
        },
        RESULT("result") {
            @Override
            public JSONObject forward(JSONObject obj, double x1, double result, double limit) {
                return Util.copyJSONwithMetric(obj, result);
            }
        },
        ORIGINAL_MINUS_LIMIT("original_minus_limit") {
            @Override
            public JSONObject forward(JSONObject obj, double x1, double result, double limit) {
                return Util.copyJSONwithMetric(obj, x1 - limit);
            }
        },
        ORIGINAL_PLUS_LIMIT("original_plus_limit") {
            @Override
            public JSONObject forward(JSONObject obj, double x1, double result, double limit) {
                return Util.copyJSONwithMetric(obj, x1 + limit);
            }
        },
        RESULT_MINUS_LIMIT("result_minus_limit") {
            @Override
            public JSONObject forward(JSONObject obj, double x1, double result, double limit) {
                return Util.copyJSONwithMetric(obj, result - limit);
            }
        },
        RESULT_PLUS_LIMIT("result_minus_limit") {
            @Override
            public JSONObject forward(JSONObject obj, double x1, double result, double limit) {
                return Util.copyJSONwithMetric(obj, result + limit);
            }
        };

        private final String text;


        private ForwardDefinition(String text) {
            this.text = text;
        }

        public abstract JSONObject forward(JSONObject obj, double x1, double result, double limit);

        @Override
        public String toString() {
            return text;
        }

    }

    private static PriceNodeType _type = PriceNodeType.CONDITIONAL;

    private Date _fromDate;
    private Date _toDate;

    private ArrayList<String> _entities;

    private QueryPriceNode _query1;
    private QueryPriceNode _query2;

    private TwiseOperatorDefinition _twiseOperator;
    private OperatorDefinition _operator;

    private double _limit;

    private ForwardDefinition _forward;

    private ArrayList<IPriceNode> _childNodes;

    public ConditionalPriceNode() {
        _fromDate = null;
        _toDate = null;
        _entities = null;
        _query1 = null;
        _query2 = null;
        _twiseOperator = TwiseOperatorDefinition.NDEF;
        _operator = OperatorDefinition.NDEF;
        _limit = 0;
        _forward = ForwardDefinition.NDEF;
    }

    @Override
    public String parse(JSONObject input) {
        logger.debug("CONDITIONAL NODE :: Parsing string input ... ");
        logger.debug(input);

        if (!Util.checkFields(input, _allowedFields)) {
            logger.debug("CONDITIONAL NODE :: Node structure contains fields that are not allowed.");

            return "{error:per use node structure contains fields that are not allowed.}";
        }

        try {
            // if specified store the "from date"
            _fromDate = Util.parseDate("fromDate", input);
            logger.debug("CONDITIONAL NODE :: _fromDate = " + _fromDate);

            // if specified store the "to date"
            _toDate = Util.parseDate("toDate", input);
            logger.debug("CONDITIONAL NODE :: _toDate = " + _toDate);

            if ((_fromDate != null) && (_toDate != null))
                if (_fromDate.compareTo(_toDate) > 0) {
                    logger.error("CONDITIONAL NODE :: fromDate larger than toDate");
                    return new JSONObject().put("error", "toDate must be greater or equal to fromDate.").toString();
                }

            if (input.has("conditional1")) {
                _query1 = new QueryPriceNode();
                _query1.parse(input.getJSONObject("conditional1"));
            }
            else
                _query1 = null;

            logger.debug("CONDITIONAL NODE :: QUERY 1 ::" + _query1.toJSON().toString());

            if (input.has("conditional2")) {
                _query2 = new QueryPriceNode();
                _query2.parse(input.getJSONObject("conditional2"));
            }
            else
                _query2 = null;

            logger.debug("CONDITIONAL NODE :: QUERY 2 :: " + _query2.toJSON().toString());

            if ((_query1 == null) && (_query2 == null))
                return new JSONObject().put("error", "At least one conditional must be defined.").toString();
            else {
                if ((_query1 == null) && (_query2 != null)) {
                    _query1 = _query2;
                    _query2 = null;
                    _query1.setParent();
                }
            }

            if (input.has("twise_operator")) {
                if (input.get("twise_operator") instanceof String) {
                    String opName = input.getString("twise_operator");

                    if (opName.compareTo(TwiseOperatorDefinition.NDEF.toString()) == 0)
                        _twiseOperator = TwiseOperatorDefinition.NDEF;
                    else if (opName.compareTo(TwiseOperatorDefinition.DIVIDE.toString()) == 0)
                        _twiseOperator = TwiseOperatorDefinition.DIVIDE;
                    else if (opName.compareTo(TwiseOperatorDefinition.MAX.toString()) == 0)
                        _twiseOperator = TwiseOperatorDefinition.MAX;
                    else if (opName.compareTo(TwiseOperatorDefinition.MIN.toString()) == 0)
                        _twiseOperator = TwiseOperatorDefinition.MIN;
                    else if (opName.compareTo(TwiseOperatorDefinition.MULTIPLY.toString()) == 0)
                        _twiseOperator = TwiseOperatorDefinition.MULTIPLY;
                    else if (opName.compareTo(TwiseOperatorDefinition.SUBTRACT.toString()) == 0)
                        _twiseOperator = TwiseOperatorDefinition.SUBTRACT;
                    else if (opName.compareTo(TwiseOperatorDefinition.SUM.toString()) == 0)
                        _twiseOperator = TwiseOperatorDefinition.SUM;
                    else
                        _twiseOperator = TwiseOperatorDefinition.NDEF;
                }
                else
                    _twiseOperator = (TwiseOperatorDefinition) input.get("twise_operator");
            }
            else {
                // if two queries are defined then twise operator MUST be defined
                if (_query2 != null)
                    return new JSONObject().put("error", "TWise operator must be defined if two conditionals are defined.").toString();

                _twiseOperator = TwiseOperatorDefinition.NDEF;
            }

            logger.debug("CONDITIONAL NODE :: _twiseOperator :: " + _twiseOperator.text);

            if (input.has("operator")) {
                if (input.get("operator") instanceof String) {
                    String opname = input.getString("operator");
                    if (opname.compareTo(OperatorDefinition.EQ.toString()) == 0)
                        _operator = OperatorDefinition.EQ;
                    else if (opname.compareTo(OperatorDefinition.NEQ.toString()) == 0)
                        _operator = OperatorDefinition.NEQ;
                    else if (opname.compareTo(OperatorDefinition.LT.toString()) == 0)
                        _operator = OperatorDefinition.LT;
                    else if (opname.compareTo(OperatorDefinition.LTE.toString()) == 0)
                        _operator = OperatorDefinition.LTE;
                    else if (opname.compareTo(OperatorDefinition.GT.toString()) == 0)
                        _operator = OperatorDefinition.GT;
                    else if (opname.compareTo(OperatorDefinition.GTE.toString()) == 0)
                        _operator = OperatorDefinition.GTE;
                    else
                        _operator = OperatorDefinition.NDEF;

                }
                else
                    _operator = (OperatorDefinition) input.get("operator");
            }
            else {
                return new JSONObject().put("error", "Comparison operator must be defined").toString();
                //_operator = OperatorDefinition.NDEF;
            }
            logger.debug("CONDITIONAL NODE :: _operator :: " + _operator.text);

            if (input.has("forward")) {
                if (input.get("forward") instanceof String) {
                    String fwname = input.getString("forward");
                    if (fwname.compareTo(ForwardDefinition.NDEF.toString()) == 0)
                        _forward = ForwardDefinition.NDEF;
                    else if (fwname.compareTo(ForwardDefinition.ORIGINAL.toString()) == 0)
                        _forward = ForwardDefinition.ORIGINAL;
                    else if (fwname.compareTo(ForwardDefinition.ORIGINAL_MINUS_LIMIT.toString()) == 0)
                        _forward = ForwardDefinition.ORIGINAL_MINUS_LIMIT;
                    else if (fwname.compareTo(ForwardDefinition.ORIGINAL_PLUS_LIMIT.toString()) == 0)
                        _forward = ForwardDefinition.ORIGINAL_PLUS_LIMIT;
                    else if (fwname.compareTo(ForwardDefinition.RESULT.toString()) == 0)
                        _forward = ForwardDefinition.RESULT;
                    else if (fwname.compareTo(ForwardDefinition.RESULT_MINUS_LIMIT.toString()) == 0)
                        _forward = ForwardDefinition.RESULT_MINUS_LIMIT;
                    else if (fwname.compareTo(ForwardDefinition.RESULT_PLUS_LIMIT.toString()) == 0)
                        _forward = ForwardDefinition.RESULT_PLUS_LIMIT;
                    else
                        _forward = ForwardDefinition.NDEF;
                }
                else
                    _forward = (ForwardDefinition) input.get("forward");
            }
            else
                return new JSONObject().put("error", "Forward field must be defined").toString();
            //_forward = ForwardDefinition.NDEF;

            logger.debug("CONDITIONAL NODE :: _forward :: " + _forward.text);


            if (input.has("limit")) {
                _limit = input.getDouble("limit");
            }
            else
                _limit = 0;

            logger.debug("CONDITIONAL NODE :: _limit :: " + _limit);

            if (input.has("entities")) {
                Object o = input.get("entities");

                if (o instanceof JSONArray) {
                    _entities = new ArrayList<String>();
                    JSONArray a = input.getJSONArray("entities");

                    for (int i = 0; i < a.length(); i++) {
                        logger.debug("CONDITIONAL NODE :: _entities[" + i + "]=" + a.getString(i));
                        _entities.add(a.getString(i));
                    }
                }
                else if (o instanceof String) {
                    _entities = new ArrayList<String>();

                    _entities.add((String) o);

                    logger.debug("CONDITIONAL NODE :: _entities[0]=" + (String) o);
                }
            }
            else {
                _entities = null;
                logger.debug("CONDITIONAL NODE :: _entities=null");
            }

            if (input.has("submodel")) {
                // initialize child nodes
                _childNodes = new ArrayList<IPriceNode>();

                JSONArray submodel = input.getJSONArray("submodel");

                logger.debug("CONDITIONAL NODE :: submodel :: " + submodel);

                for (int i = 0; i < submodel.length(); i++) {
                    JSONObject o = (JSONObject) submodel.get(i);

                    if (o.has("type")) {
                        switch (IPriceNode.PriceNodeType.valueOf(o.getString("type"))) {
                            case FIXED_PRICE: {
                                IPriceNode p = new FlatRatePriceNode();
                                p.parse(o);
                                _childNodes.add(p);
                            }
                            break;
                            case PER_UNIT: {
                                IPriceNode p = new PerUnitPriceNode();
                                p.parse(o);
                                _childNodes.add(p);
                            }
                            break;
                            case PER_USE: {
                                IPriceNode p = new PerUsePriceNode();
                                p.parse(o);
                                _childNodes.add(p);
                            }
                            break;
                            default:
                                //unknown pricing type
                                logger.error("CONDITIONAL NODE :: Unknown pricing type." + IPriceNode.PriceNodeType.valueOf(o.getString("type")));
                                return "{error:Unknown or incorrect node type.}";
                        }
                    }
                    else {
                        // invalid structure
                        logger.error("CONDITIONAL NODE :: Unknown child node type :: " + o.getString("type"));
                        return "{error: CONDITIONAL NODE, unknown submodel node.}";
                    }
                }
            }
            else {
                logger.error("CONDITIONAL NODE :: this query node has no children.");

                _childNodes = null;
                return "{error:Empty pricing model}";
            }

        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            logger.error("CONDITIONAL NODE :: An exception was raised :: " + e.toString());

            return "{error:An exception has been raised during the parsing of a conditional node.}";
        }

        return "{status:OK}";
    }

    @Override
    public double getPrice(JSONArray input, JSONObject definitions) {
        // this node is not intended to be a child of any query node
        return 0;
    }

    @Override
    public double getPrice(DBConnector db, JSONObject definitions) {
        logger.debug("CONDITIONAL NODE :: Starting conditional node execution. \n \t Price query " + definitions);
        JSONObject new_definitions = new JSONObject();

        try {
            Date date1 = Util.parseDate("fromDate", definitions);
            Date ifromDate = (Date) Util.passThroughFromDate(_fromDate, date1);
            logger.debug("CONDITIONAL NODE :: fromDate resolution => " + date1 + " & " + _fromDate + " -> " + ifromDate);

            Date date2 = Util.parseDate("toDate", definitions);
            Date itoDate = (Date) Util.passThroughToDate(_toDate, date2);
            logger.debug("CONDITIONAL NODE :: toDate resolution => " + date2 + " & " + _toDate + " -> " + itoDate);

            // check if the from and to date range is valid -> if not return 0
            if ((itoDate != null) && (ifromDate != null) && (ifromDate.compareTo(itoDate) > 0)) {
                logger.error("CONDITIONAL NODE :: fromDate larger than toDate.");
                return 0;
            }

            Object entities = Util.passThroughEnitites(_entities, definitions.opt("entities"));
            logger.debug("CONDITIONAL NODE :: entities resolution => " + definitions.opt("entities") + " & " + _entities + " -> " + entities);

            // now we neatly package everything into new_definitions and send it to children, collect the prices and sum the results.
            if (ifromDate != null)
                new_definitions.put("fromDate", ifromDate);

            if (itoDate != null)
                new_definitions.put("toDate", itoDate);

            if (entities != null)
                new_definitions.put("entities", entities);

            List<String> sortedAllowedFields = new ArrayList<String>(Arrays.asList(_allowedFields));
            Collections.sort(sortedAllowedFields);

            Iterator it = definitions.keys();
            while (it.hasNext()) {
                String key = (String) it.next();
                // check if the key is one of the allowed keys
                //	- if yes, then it has already been taken care of in the above
                // 	- if no, then pass it to the children
                if (Collections.binarySearch(sortedAllowedFields, key) < 0) {
                    new_definitions.put(key, definitions.get(key));
                }
            }

            logger.debug("CONDITIONAL NODE :: pass to query nodes => " + new_definitions);

            JSONArray results1 = null;
            JSONArray results2 = null;

            if (_query1 != null) {
                results1 = _query1.getResults(db, new_definitions);

                //logger.debug("CONDITIONAL NODE :: Query 1 results :: " + results1.toString() );
            }
            if (_query2 != null) {
                results2 = _query2.getResults(db, new_definitions);

                //logger.debug("CONDITIONAL NODE :: Query 2 results :: " + results2.toString() );
            }


            logger.debug("CONDITIONAL NODE :: Performing time match of results. ");
            if ((results1 != null) && (results2 != null)) {
                // time to "time match" the results
                JSONArray forward1 = new JSONArray();
                JSONArray forward2 = new JSONArray();

                // first copy results2 into an array of JSONObjects
                ArrayList<JSONObject> list2 = new ArrayList<JSONObject>();
                for (int i = 0; i < results2.length(); i++)
                    list2.add(results2.getJSONObject(i));

                //now sort the array
                Collections.sort(list2, new Util.JSONTimeComparator());

                for (int i = 0; i < results1.length(); i++) {
                    JSONObject o1 = results1.getJSONObject(i);

                    int place = Collections.binarySearch(list2, o1, new Util.JSONTimeComparator());

                    JSONObject o2 = null;

                    if (place >= 0) {
                        // found exact match
                        o2 = list2.get(place);
                    }
                    else {
                        place += 1;
                        place *= -1;

                        if (place == list2.size()) {
                            // in this case o1 would be inserted at the end of the sorted list2.. the only potential candidate is already at the end of the list
                            o2 = list2.get(list2.size() - 1);

                            Date time1 = (Date) o1.get("time");
                            Date time2 = (Date) o2.get("time");

                            double timeDiff = Math.abs(time2.getTime() - time1.getTime());

                            if (timeDiff > IPriceNode.MONITORING_FREQUENCY_TOLERANCE)
                                o2 = null;
                        }
                        else if (place > 0) {
                            // in this case o1 would be inserted somewhere in the list with list2[place] being the first object greater than o1..
                            Date time1 = (Date) o1.get("time");

                            Date time21 = (Date) list2.get(place).get("time");
                            Date time22 = (Date) list2.get(place - 1).get("time");

                            if ((Math.abs(time22.getTime() - time1.getTime()) > Math.abs(time21.getTime() - time1.getTime())) &&
                                    (Math.abs(time21.getTime() - time1.getTime()) <= IPriceNode.MONITORING_FREQUENCY_TOLERANCE)) {
                                o2 = list2.get(place);
                            }
                            else if ((Math.abs(time22.getTime() - time1.getTime()) <= Math.abs(time21.getTime() - time1.getTime())) &&
                                    (Math.abs(time22.getTime() - time1.getTime()) <= IPriceNode.MONITORING_FREQUENCY_TOLERANCE)) {
                                o2 = list2.get(place - 1);
                            }
                            else
                                o2 = null;

                        }
                        else {
                            // in this case o1 would be inserted at beginning of the list.. the only potential candidate is already at the start
                            o2 = list2.get(0);

                            Date time1 = (Date) o1.get("time");
                            Date time2 = (Date) o2.get("time");

                            double timeDiff = Math.abs(time2.getTime() - time1.getTime());

                            if (timeDiff > IPriceNode.MONITORING_FREQUENCY_TOLERANCE)
                                o2 = null;
                        }
                    }

                    if ((o1 != null) && (o2 != null)) {
                        logger.debug("CONDITIONAL NODE :: Time matched pair :: " + o1 + " & " + o2);

                        // ok.. now we have a time-matched pair.. let's do the calculations
                        // first we get the two numbers in the metric
                        Iterator it1 = o1.getJSONObject("metrics").keys();
                        Iterator it2 = o2.getJSONObject("metrics").keys();

                        double m1 = 0;
                        double m2 = 0;

                        if (it1.hasNext())
                            m1 = o1.getJSONObject("metrics").getDouble((String) it1.next());
                        if (it2.hasNext())
                            m2 = o2.getJSONObject("metrics").getDouble((String) it2.next());

                        double result = _twiseOperator.apply(m1, m2);

                        logger.debug("CONDITIONAL NODE :: Time matched pair :: " + m1 + " " + _twiseOperator.text + " " + m2 + " -> " + result);

                        if (_operator.apply(result, _limit)) {
                            logger.debug("CONDITIONAL NODE :: Forward to child node 1 :: " + _forward.text + " -> " + _forward.forward(o1, m1, result, _limit));
                            forward1.put(_forward.forward(o1, m1, result, _limit));
                        }
                        else {
                            logger.debug("CONDITIONAL NODE :: Forward to child node 2 :: " + _forward.text + " -> " + _forward.forward(o1, m1, result, _limit));
                            forward2.put(_forward.forward(o1, m1, result, _limit));
                        }
                    }
                }

                double price = 0;

                if (_childNodes.size() > 0)
                    price += _childNodes.get(0).getPrice(results1, new_definitions);

                if (_childNodes.size() > 1)
                    price += _childNodes.get(1).getPrice(results2, new_definitions);

                logger.debug("CONDITIONAL NODE :: PRICE :: " + price);

                return price;

            }
            else {
                // if only one query (query1) is defined
                logger.error("CONDITIONAL NODE :: No results returned by at least one query.");

                return 0;
            }

        }
        catch (Exception ex) {
            logger.error("CONDITIONAL NODE :: An exception has been raised :: " + ex.getLocalizedMessage());
            ex.printStackTrace();
            return 0;
        }

    }

    @Override
    public PriceNodeType getType() {
        return _type;
    }

    @Override
    public boolean validation(JSONObject obj) {
        return Util.checkFields(obj, _allowedFields);
    }


    public JSONObject toJSON() {

        JSONObject output = new JSONObject();
        //{"type", "fromDate", "toDate", "entities", "conditional1", "conditional2", "twise_operator", "operator", "limit", "forward",  "submodel"};

        try {
            if (_toDate != null)
                output.put("toDate", _toDate);

            if (_fromDate != null)
                output.put("fromDate", _fromDate);

            if (_query1 != null)
                output.put("conditional1", _query1);

            if (_query2 != null)
                output.put("conditional2", _query2);

            if (_twiseOperator != null)
                output.put("twise_operator", _twiseOperator);

            if (_operator != null)
                output.put("operator", _operator);

            if (_limit != Double.NaN)
                output.put("limit", _limit);

            if (_forward != null)
                output.put("forward", _forward);

            output.put("type", _type);

            if (_entities != null) {
                JSONArray entities = new JSONArray();
                for (int i = 0; i < _entities.size(); i++) {
                    entities.put(_entities.get(i));
                }
                output.put("entities", entities);
            }

            if (this._childNodes != null) {
                JSONArray children = new JSONArray();
                for (int i = 0; i < _childNodes.size(); i++) {
                    children.put(_childNodes.get(i).toJSON());
                }
                output.put("submodel", children);
            }
        }
        catch (Exception e) {
            logger.error("CONDITIONAL NODE :: An exception has been raised :: " + e.getLocalizedMessage());
            e.printStackTrace();

            return null;
        }

        return output;
    }

}
