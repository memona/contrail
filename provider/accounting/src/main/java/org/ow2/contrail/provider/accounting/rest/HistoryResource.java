package org.ow2.contrail.provider.accounting.rest;

import com.mongodb.DB;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.utils.DateUtils;
import org.ow2.contrail.provider.accounting.utils.MongoDBConnection;
import org.ow2.contrail.provider.storagemanager.CondensedHistoryData;
import org.ow2.contrail.provider.storagemanager.HistoryRetriever;
import org.ow2.contrail.provider.storagemanager.common.Metric;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("/metrics_history")
public class HistoryResource {
    private static Logger log = Logger.getLogger(HistoryResource.class);

    @POST
    @Path("/condensed_data")
    @Consumes("application/json")
    @Produces("application/json")
    public StreamingOutput getCondensedHistory(String data) {
        if (log.isTraceEnabled()) {
            log.trace("getCondensedHistory() started. Data: " + data);
        }

        String source;
        List<String> sids;
        Date startTime;
        Date endTime;
        List<Metric> metrics;
        int numberOfIntervals;

        try {
            JSONObject json = new JSONObject(data);

            source = json.getString("source");
            numberOfIntervals = json.getInt("numberOfIntervals");

            if (json.has("startTime")) {
                startTime = DateUtils.parseDate(json.getString("startTime"));
            }
            else {
                startTime = null;
            }

            if (json.has("endTime")) {
                endTime = DateUtils.parseDate(json.getString("endTime"));
            }
            else {
                endTime = new Date();
            }

            JSONArray metricsArr = json.getJSONArray("metrics");
            metrics = new ArrayList<Metric>();
            for (int i=0; i<metricsArr.length(); i++) {
                Metric metric = new Metric(metricsArr.getString(i));
                metrics.add(metric);
            }

            JSONArray sidsArr = json.getJSONArray("sids");
            sids = new ArrayList<String>();
            for (int i=0; i<sidsArr.length(); i++) {
                sids.add(sidsArr.getString(i));
            }
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }

        try {
            log.trace("Retrieving metrics history from storage-manager...");
            DB db = MongoDBConnection.getDB();
            HistoryRetriever historyRetriever = new HistoryRetriever(db);
            final CondensedHistoryData history = historyRetriever.getCondensedHistory(metrics, source, sids,
                    startTime, endTime, numberOfIntervals);
            log.trace("Metrics history retrieved successfully.");

            log.trace("Serializing CondensedHistoryData to streaming output...");
            final ObjectMapper objectMapper = new ObjectMapper();

            return new StreamingOutput() {
                @Override
                public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                    objectMapper.writeValue(outputStream, history);
                }
            };
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.INTERNAL_SERVER_ERROR).
                            entity("Failed to retrieve metrics history: " + e.getMessage()).build());
        }
    }

}
