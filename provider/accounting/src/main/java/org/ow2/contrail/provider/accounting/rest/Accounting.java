package org.ow2.contrail.provider.accounting.rest;

import com.google.gson.Gson;
import com.mongodb.DB;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.db.DBConnector;
import org.ow2.contrail.provider.accounting.db.MongoDBConnector;
import org.ow2.contrail.provider.accounting.enums.Source;
import org.ow2.contrail.provider.accounting.utils.JsonUtils;
import org.ow2.contrail.provider.accounting.utils.MongoDBConnection;
import org.ow2.contrail.provider.storagemanager.DataRetriever;
import org.ow2.contrail.provider.storagemanager.HistoryRetriever;
import org.ow2.contrail.provider.storagemanager.MetricsHistoryData;
import org.ow2.contrail.provider.storagemanager.aggregates.AggregateFunction;
import org.ow2.contrail.provider.storagemanager.aggregates.MaxAggregate;
import org.ow2.contrail.provider.storagemanager.aggregates.MeanAggregate;
import org.ow2.contrail.provider.storagemanager.aggregates.MinAggregate;
import org.ow2.contrail.provider.storagemanager.common.Metric;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("/accounting")
public class Accounting {
    private static Logger log = Logger.getLogger(Accounting.class);
    private DataRetriever dataRetriever;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    public Accounting() throws IOException {
        DB db = MongoDBConnection.getDB();
        dataRetriever = new DataRetriever(db);
    }

    @GET
    @Path("/test")
    @Produces("application/json")
    public Response test() {
        return Response.status(Response.Status.OK).entity("{'status':'ok'}").build();
    }

    @POST
    @Path("/metric/{group}/{metricName}/aggregate/{aggregate}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response getMetricAggregate(@PathParam("group") String group, @PathParam("metricName") String metricName,
                                       @PathParam("aggregate") String aggregate, String requestJson) {
        if (log.isTraceEnabled()) {
            log.trace("getMetricAggregate() started. Data: " + requestJson);
        }

        try {
            JSONObject json = new JSONObject(requestJson);

            String source = json.getString("source");

            Date startTime = null;
            if (json.has("startTime")) {
                startTime = sdf.parse(json.getString("startTime"));
            }
            else {
                startTime = null;
            }

            Date endTime;
            if (json.has("endTime")) {
                endTime = sdf.parse(json.getString("endTime"));
            }
            else {
                endTime = new Date();
            }

            List<String> sids = new ArrayList<String>();
            if (json.get("sid") instanceof JSONArray) {
                JSONArray sidArr = json.getJSONArray("sid");
                for (int i = 0; i < sidArr.length(); i++) {
                    sids.add(sidArr.getString(i));
                }
            }
            else {
                String sid = json.getString("sid");
                sids.add(sid);
            }

            AggregateFunction aggregateFunction;
            if (aggregate.equalsIgnoreCase("max")) {
                aggregateFunction = new MaxAggregate(metricName);
            }
            else if (aggregate.equalsIgnoreCase("min")) {
                aggregateFunction = new MinAggregate(metricName);
            }
            else if (aggregate.equalsIgnoreCase("mean")) {
                aggregateFunction = new MeanAggregate(metricName);
            }
            else {
                throw new Exception(String.format("Aggregate function %s is not supported.", aggregate));
            }

            log.trace("Calling storage-manager getMetricAggregateValue()...");
            dataRetriever.calculateAggregateValue(group, metricName, source, sids,
                    startTime, endTime, aggregateFunction);
            Object aggregateValue = aggregateFunction.getAggregateValue();
            log.trace("Aggregate value retrieved successfully.");

            Gson gson = JsonUtils.getInstance().getGson();
            return Response.ok(gson.toJson(aggregateValue)).build();
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
        catch (ParseException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid date format: " + e.getMessage()).build());
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.INTERNAL_SERVER_ERROR).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
    }

    @POST
    @Path("/metrics_history")
    @Consumes("application/json")
    @Produces("application/json")
    public Response getMetricHistory(String data) {
        if (log.isTraceEnabled()) {
            log.trace("getMetricHistory() started. Data: " + data);
        }

        String source;
        String sid;
        Date startTime;
        Date endTime;
        List<Metric> metrics;

        try {
            JSONObject json = new JSONObject(data);

            source = json.getString("source");
            sid = json.getString("sid");

            if (json.has("startTime")) {
                startTime = sdf.parse(json.getString("startTime"));
            }
            else {
                startTime = null;
            }

            if (json.has("endTime")) {
                endTime = sdf.parse(json.getString("endTime"));
            }
            else {
                endTime = new Date();
            }

            JSONArray metricsArr = json.getJSONArray("metrics");
            metrics = new ArrayList<Metric>();
            for (int i=0; i<metricsArr.length(); i++) {
                Metric metric = new Metric(metricsArr.getString(i));
                metrics.add(metric);
            }
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }

        try {
            log.trace("Retrieving metric history from storage-manager...");
            DB db = MongoDBConnection.getDB();
            HistoryRetriever historyRetriever = new HistoryRetriever(db);
            MetricsHistoryData history = historyRetriever.getHistory(metrics, source, sid,
                    startTime, endTime);
            log.trace("Metric history retrieved successfully.");

            log.trace("Converting MetricsHistoryData to json...");
            Gson gson = JsonUtils.getInstance().getGson();
            log.trace("MetricsHistoryData converted to json successfully.");
            return Response.ok(gson.toJson(history)).build();
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.INTERNAL_SERVER_ERROR).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
    }

    @POST
    @Path("/metric/{group}/{metricName}/value")
    @Consumes("application/json")
    @Produces("application/json")
    public Response getMetricValueOnInterval(@PathParam("group") String group, @PathParam("metricName") String metricName,
                                             String requestJson) {
        if (log.isTraceEnabled()) {
            log.trace("getMetricValueOnInterval() started. Data: " + requestJson);
        }

        Source source;
        List<String> sids;
        Date startTime;
        Date endTime;
        try {
            JSONObject json = new JSONObject(requestJson);

            source = Source.fromString(json.getString("source"));

            if (json.has("startTime")) {
                startTime = sdf.parse(json.getString("startTime"));
            }
            else {
                startTime = null;
            }

            if (json.has("endTime")) {
                endTime = sdf.parse(json.getString("endTime"));
            }
            else {
                endTime = new Date();
            }

            sids = new ArrayList<String>();
            if (json.get("sid") instanceof JSONArray) {
                JSONArray sidArr = json.getJSONArray("sid");
                for (int i = 0; i < sidArr.length(); i++) {
                    sids.add(sidArr.getString(i));
                }
            }
            else {
                String sid = json.getString("sid");
                sids.add(sid);
            }
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }

        try {
            log.trace("Calculating metric value...");
            org.ow2.contrail.provider.accounting.metrics.Metric metric =
                    org.ow2.contrail.provider.accounting.metrics.Metric.create(group, metricName, source, sids);
            Object value = metric.getValue(startTime, endTime);
            log.trace("Success.");

            Gson gson = JsonUtils.getInstance().getGson();
            return Response.ok(gson.toJson(value)).build();
        }
        catch (Exception e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.INTERNAL_SERVER_ERROR).
                            entity("Failed to calculate metric value: " + e.getMessage()).build());
        }
    }


    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public String getMessage(String request) {
        try {
            JSONObject requestObject = null;

            if (request == null)
                return new JSONObject().put("Error", "Missing request.").toString();

            requestObject = new JSONObject(request);

            log.debug("ACCOUNTING REST INTERFACE \n\t Request :: " + requestObject);

            if (!requestObject.has("command")) {
                return new JSONObject().put("Error", "Missing command.").toString();
            }

            try {
                JSONObject command = requestObject.getJSONObject("command");
                if (command.has("get")) {
                    JSONObject getter = command.getJSONObject("get");
                    try {
                        //return db.query(getter).toString();
                        return null;
                    }
                    catch (Exception ex) {
                        return new JSONObject().put("Error", "Error retreiving results from the database.").toString();
                    }
                }
            }
            catch (JSONException ex) {
                // that means the command valule is not a JSONObject but a simple string.. -> follow the KISS
                if (requestObject.getString("command").compareTo("availability") == 0) {
                    JSONObject filter = new JSONObject();
                    JSONArray metrics = new JSONArray();

                    if ((requestObject.has("from")) || (requestObject.has("to"))) {
                        JSONArray timeConditions = new JSONArray();

                        if (requestObject.has("from")) {
                            Date fromDate = (Date) DBConnector.formatter.parse(requestObject.getString("from"));

                            timeConditions.put(new JSONObject().put("operator", "gt").put("limit", DBConnector.formatter.format(fromDate)));
                        }
                        if (requestObject.has("to")) {
                            Date toDate = (Date) DBConnector.formatter.parse(requestObject.getString("to"));

                            timeConditions.put(new JSONObject().put("operator", "lt").put("limit", DBConnector.formatter.format(toDate)));
                        }
                        metrics.put(new JSONObject().put("field", "time").put("conditions", timeConditions));
                    }

                    if (requestObject.has("host")) {
                        metrics.put(new JSONObject().put("field", "sid").put("conditions", requestObject.get("host")));

                        filter.put("resource", "info");
                        filter.put("filter", metrics);
                        filter.put("return", new JSONObject().put("available", "availability"));

                        try {
                            // TODO: Use StorageManagerConnector
                            MongoDBConnector db = new MongoDBConnector();

                            db.connect("localhost", "monitoring", 27017);

                            db.useCollection("data");

                            return db.query(filter).toString();
                        }
                        catch (Exception e) {
                            return new JSONObject().put("Error", "Error retreiving results from the database.").put("filter", filter).toString();
                        }
                    }
                }

                return new JSONObject().put("Error", "Unimplemented: Hello world.").toString();
            }
        }
        catch (Exception ex) {
            return "{Error :" + ex.getLocalizedMessage() + "}";
        }
        return "{Error :" + "Hello world" + "}";
    }
}
