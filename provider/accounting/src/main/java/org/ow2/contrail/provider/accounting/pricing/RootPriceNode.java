package org.ow2.contrail.provider.accounting.pricing;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.db.DBConnector;
import org.ow2.contrail.provider.accounting.pricing.Util.PassThroughException;

import java.util.*;

// EXAMPLE:
//
// { "type":"root", "vmIDs":["n0001","n0002"], "pricing:[
//		{"type":"resource", "resource":"memory", "pricing":[
//				{"type":"flat-rate", "validFrom":"2011.01.01.00.00", "validTo":"2011.02.27.00.00", value:"0.5"},
//				{"type":"if", "resource":"occupied", "function":"occupied-to-total-ratio", "break-point":"0.5", 
//				 "validFrom":"2011.02.27.00.01": "validTo":"2011.08.01.00.00", "value":[
//					{"type":"flat-rate", "value":"0.5"}, {"type":"flat-rate", "value":"0.8"}] },
//				{"type":"per-unit", "unit":"1024", "validFrom":"2011.08.01.00.01", "validTo":"2011.12.31.23.59", "value":"0.1"} ] }
//		{"type":"resource", "resource":"disk", "pricing:[
// 				{"type":"per-unit", "validFrom":"2011.01.01.00.00", "validTo":"2011.12.31.23.59", "unit":"1024", "value":"0.01"] },
//		{"type":"resource", "resource":"network", "pricing":[ 
//		 		{"type":"if", "resource":"transferRate", "function":"integral", "break-point":"500000000",
//				 "validFrom":"2011.01.01.00.00", "validTo":"2011.12.31.23.59", "value":[
//					{"type":"per-unit", "unit":"1024", "value":"0.1"},
//					{"type":"per-unit", "unit":"1024", "value":"0.5"} ] } ] } 
//		] 
// }
//
public class RootPriceNode implements IPriceNode {
    static Logger logger = Logger.getLogger(RootPriceNode.class);

    private static String[] _allowedFields = {"id", "version", "fromDate", "toDate", "entities", "type", "resource", "metrics", "submodel"};

    private String _toString;

    private static PriceNodeType _type = PriceNodeType.ROOT;
    private Date _fromDate;
    private Date _toDate;
    private String _id;
    private String _version;
    private ArrayList<String> _entities;
    private String _resource;
    private String _metric;

    private ArrayList<IPriceNode> _childNodes;

    // db.data.find({"sid":/^[\.-]*ENTITY_ID[\.-]/})

    public RootPriceNode() {
        _toString = null;
        _fromDate = null;
        _toDate = null;
        _id = null;
        _version = null;
        _childNodes = null;
        _entities = null;
        _resource = null;
        _metric = null;
    }

    //
    // PARSE THE PRICING MODEL
    //
    @Override
    public String parse(JSONObject input) {
        logger.debug("ROOT NODE :: Parsing string input ... ");
        _toString = input.toString();

        logger.debug(input);

        logger.debug("ROOT NODE :: Checking for allowed field ");
        if (!Util.checkFields(input, _allowedFields)) {
            logger.error("ROOT NODE :: Node structure contains fields that are not allowed");

            return "{error:Root node structure contains fields that are not allowed.}";
        }
        try {
            // get the pricing model ID
            _id = Util.parseString("id", input);

            logger.debug("ROOT NODE :: _id = " + _id);
            logger.debug(_id);

            // get the pricing model version
            _version = Util.parseString("version", input);
            logger.debug("ROOT NODE :: _version = " + _version);


            // if specified store the "from date"
            _fromDate = Util.parseDate("fromDate", input);
            logger.debug("ROOT NODE :: _fromDate = " + _fromDate);

            // if specified store the "to date"
            _toDate = Util.parseDate("toDate", input);
            logger.debug("ROOT NODE :: _toDate = " + _toDate);

            if ((_fromDate != null) && (_toDate != null))
                if (_fromDate.compareTo(_toDate) > 0) {
                    logger.error("ROOT NODE :: fromDate larger than toDate");
                    return new JSONObject().put("error", "toDate must be greater or equal to fromDate.").toString();
                }

            // if specified store the entities for which this pricing model is defined
            if (input.has("entities")) {
                Object o = input.get("entities");

                if (o instanceof JSONArray) {
                    _entities = new ArrayList<String>();
                    JSONArray a = input.getJSONArray("entities");

                    for (int i = 0; i < a.length(); i++) {
                        logger.debug("ROOT NODE :: _entities[" + i + "]=" + a.getString(i));
                        _entities.add(a.getString(i));
                    }
                }
                else if (o instanceof String) {
                    _entities = new ArrayList<String>();

                    _entities.add((String) o);
                    logger.debug("ROOT NODE :: _entities[0]=" + (String) o);
                }
            }
            else {
                logger.debug("ROOT NODE :: _entities=null");
                _entities = null;
            }

            // if specified store the "resource"
            _resource = Util.parseString("resource", input);
            logger.debug("ROOT NODE :: _resource = " + _resource);


            // if specified store the "from date"
            _metric = Util.parseString("metrics", input);
            logger.debug("ROOT NODE :: _metric = " + _metric);

            // now parse the submodel
            if (input.has("submodel")) {
                logger.debug("ROOT NODE :: Parsing child nodes.");

                // initialize child nodes
                _childNodes = new ArrayList<IPriceNode>();

                JSONArray submodel = input.getJSONArray("submodel");

                for (int i = 0; i < submodel.length(); i++) {
                    JSONObject o = (JSONObject) submodel.get(i);

                    if (o.has("type")) {
                        switch (IPriceNode.PriceNodeType.valueOf(o.getString("type"))) {
                            case PRICE: {
                                IPriceNode p = new PriceNode();
                                String status = p.parse(o);
                                if (status.compareTo("{status:OK}") != 0)
                                    return status;
                                _childNodes.add(p);
                            }
                            break;
                            case QUERY: {
                                IPriceNode q = new QueryPriceNode();
                                String status = q.parse(o);
                                if (status.compareTo("{status:OK}") != 0)
                                    return status;
                                _childNodes.add(q);
                            }
                            break;
                            default:
                                logger.error("ROOT NODE :: Unknown pricing type." + IPriceNode.PriceNodeType.valueOf(o.getString("type")));
                                //unknown pricing type
                                return "{error:Unknown or incorrect node type.}";
                        }
                    }
                    else {
                        logger.error("ROOT NODE :: Unknown child node type :: " + o.getString("type"));

                        // invalid structure
                        return "{error:Invalid pricing model structure.}";
                    }
                }
            }
            else {
                _childNodes = null;
                logger.debug("ROOT NODE :: The root node has no child nodes - the model is empty.");

                return "{error:Empty pricing model}";
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error("ROOT NODE :: An exception was raised :: " + e.toString());

            return "{error:An exception has been raised during the parsing of the root node.}";
        }

        logger.debug("ROOT NODE :: Parsing complete.");


        return "{status:OK}";
    }

    //
    // GET THE PRICE FROM THE PRICING MODEL
    //
    @Override
    public double getPrice(DBConnector db, JSONObject definitions) {
        JSONObject new_definitions = new JSONObject();

        logger.debug("ROOT NODE :: Starting price calculation \n \t Price query " + definitions);

        try {    // first we resolve possible conflicts between the pricing model and the query
            Date date1 = Util.parseDate("fromDate", definitions);
            Date ifromDate = (Date) Util.passThroughFromDate(_fromDate, date1);

            logger.debug("ROOT NODE :: fromDate resolution => " + date1 + " & " + _fromDate + " -> " + ifromDate);


            Date date2 = Util.parseDate("toDate", definitions);
            Date itoDate = (Date) Util.passThroughToDate(_toDate, date2);

            logger.debug("ROOT NODE :: toDate resolution => " + date2 + " & " + _toDate + " -> " + itoDate);

            // check if the from and to date range is valid -> if not return 0
            if ((itoDate != null) && (ifromDate != null) && (ifromDate.compareTo(itoDate) > 0)) {
                logger.error("ROOT NODE :: fromDate larger than toDate.");
                return 0;
            }

            Object resource = Util.passThroughResource(_resource, definitions.opt("resource"));
            logger.debug("ROOT NODE :: resource resolution => " + definitions.opt("resource") + " & " + _resource + " -> " + resource);

            Object metric = Util.passThroughMetric(_metric, definitions.opt("metrics"));
            logger.debug("ROOT NODE :: metric resolution => " + definitions.opt("metrics") + " & " + _metric + " -> " + metric);

            // resolve entities between entities defined in the pricing model node and the query entities
            Object entities = Util.passThroughEnitites(_entities, definitions.opt("entities"));
            logger.debug("ROOT NODE :: entities resolution => " + definitions.opt("entities") + " & " + _entities + " -> " + entities);


            //
            // now we neatly package everything into new_definitions and send it to children, collect the prices and sum the results.
            //
            if (ifromDate != null)
                new_definitions.put("fromDate", ifromDate);

            if (itoDate != null)
                new_definitions.put("toDate", itoDate);

            if (resource != null)
                new_definitions.put("resource", resource);

            if (metric != null)
                new_definitions.put("metrics", metric);

            if (entities != null)
                new_definitions.put("entities", entities);

            List<String> sortedAllowedFields = new ArrayList<String>(Arrays.asList(_allowedFields));
            Collections.sort(sortedAllowedFields);

            Iterator it = definitions.keys();
            while (it.hasNext()) {
                String key = (String) it.next();
                // check if the key is one of the allowed keys
                //	- if yes, then it has already been taken care of in the above
                // 	- if no, then pass it to the children
                if (Collections.binarySearch(sortedAllowedFields, key) < 0) {
                    new_definitions.put(key, definitions.get(key));
                }
            }

            logger.debug("ROOT NODE :: pass to children => " + new_definitions);

            // right now.. the new definitions are ready! GO!
            double price = 0;
            for (int i = 0; i < _childNodes.size(); i++) {
                price += _childNodes.get(i).getPrice(db, new_definitions);
            }

            logger.debug("ROOT NODE :: PRICE :: " + price);

            return price;
        }
        catch (JSONException ex) {
            logger.error("ROOT NODE :: An exception has been raised :: " + ex.toString());
            ex.printStackTrace();
            return 0;
        }
        catch (PassThroughException e) {
            e.printStackTrace();
            logger.error("ROOT NODE :: An exception has been raised :: " + e.toString());
            return 0;
        }
        catch (Exception e1) {
            e1.printStackTrace();
            logger.error("ROOT NODE :: An exception has been raised :: " + e1.toString());
            return 0;
        }

        //logger.error("ROOT NODE :: Well.. this is embarassing..." );
        //return 0;
    }

    @Override
    public PriceNodeType getType() {
        return _type;
    }

    @Override
    public boolean validation(JSONObject obj) {
        return Util.checkFields(obj, _allowedFields);
    }

    @Override
    public double getPrice(JSONArray input, JSONObject definitions) {
        return 0;
    }

    public JSONObject toJSON() {
        JSONObject output = new JSONObject();

        try {
            if (_id != null)
                output.put("id", _id);

            if (_version != null)
                output.put("version", _version);

            if (_toDate != null)
                output.put("toDate", _toDate);

            if (_fromDate != null)
                output.put("fromDate", _fromDate);

            if (_metric != null)
                output.put("metrics", _metric);

            if (_resource != null)
                output.put("resource", _resource);

            output.put("type", _type);

            if (this._childNodes != null) {
                JSONArray children = new JSONArray();
                for (int i = 0; i < _childNodes.size(); i++) {
                    children.put(_childNodes.get(i).toJSON());
                }
                output.put("submodel", children);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }

    @Override
    public String toString() {
        return _toString;
    }
}
