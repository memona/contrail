package org.ow2.contrail.provider.accounting.metrics;

import org.ow2.contrail.provider.accounting.enums.Source;

import java.util.Date;
import java.util.List;

public abstract class Metric {

    protected Metric() {
    }

    public static Metric create(String group, String name, Source source, List<String> sids) {
        if (group.equals("common")) {
            if (name.equals("availability") && source == Source.HOST) {
                return new AvailabilityHost(sids);
            }
        }

        throw new UnsupportedOperationException(
                String.format("Metric (group=%s, name=%s, source=%s) is not supported.", group, name, source));
    }

    public abstract Object getValue(Date startTime, Date endTime) throws Exception;
}
