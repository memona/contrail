package org.ow2.contrail.provider.accounting.aggregates;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListAggregate implements IAggregate {
    List<Object> list;

    public ListAggregate() {
        list = new ArrayList<Object>();
    }

    @Override
    public void addValue(DateValue dv) {
        list.add(dv.value());
    }

    @Override
    public JSONObject getValue() {
        try {
            JSONArray array = new JSONArray();
            for (int i = 0; i < list.size(); i++) {
                array.put(list.get(i));
            }
            return new JSONObject().put("list", array);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
