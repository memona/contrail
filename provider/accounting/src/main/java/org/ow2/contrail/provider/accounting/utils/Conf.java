package org.ow2.contrail.provider.accounting.utils;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Conf {
    private static Conf instance = new Conf();
    private static Logger log = Logger.getLogger(Conf.class);
    private Properties props;

    public static Conf getInstance() {
        return instance;
    }

    private Conf() {
    }

    public void load(String confFilePath) throws Exception {
        load(new File(confFilePath));
    }

    public void load(File confFile) throws Exception {
        props = new Properties();
        try {
            props.load(new FileInputStream(confFile));
            log.info(String.format("Provider-accounting configuration loaded successfully from file '%s'.", confFile));
        }
        catch (IOException e) {
            throw new Exception(String.format("Failed to read configuration file '%s': %s", confFile, e.getMessage()));
        }
    }

    public String getOauthFilterTokenValidationService() {
        return props.getProperty("oauthFilter.tokenValidationService");
    }

    public String getOauthFilterKeystoreFile() {
        return props.getProperty("oauthFilter.keystoreFile");
    }

    public String getOauthFilterKeystorePass() {
        return props.getProperty("oauthFilter.keystorePass");
    }

    public String getOauthFilterTruststoreFile() {
        return props.getProperty("oauthFilter.truststoreFile");
    }

    public String getOauthFilterTruststorePass() {
        return props.getProperty("oauthFilter.truststorePass");
    }

    public String getMongodbConnectionString() {
        return props.getProperty("mongodb.connectionString");
    }

    public String getMongodbDatabase() {
        return props.getProperty("mongodb.database");
    }
}
