package org.ow2.contrail.provider.accounting;

import org.apache.log4j.Logger;
import org.ow2.contrail.provider.accounting.utils.Conf;
import org.ow2.contrail.provider.accounting.utils.MongoDBConnection;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyServletContextListener implements ServletContextListener {
    private static Logger logger = Logger.getLogger(MyServletContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            ServletContext context = servletContextEvent.getServletContext();
            String configFilePath = context.getInitParameter("configuration-file");
            if (configFilePath == null) {
                throw new Exception("Missing setting 'configuration-file' in web.xml.");
            }

            // load storagemanager configuration
            org.ow2.contrail.provider.storagemanager.Conf.getInstance().load(configFilePath);

            // load accounting configuration
            Conf.getInstance().load(configFilePath);

            // initialize Mongo connection
            MongoDBConnection.init();
        }
        catch (Exception e) {
            logger.error("Accounting webapp failed to start: " + e.getMessage(), e);
            throw new RuntimeException(e);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        MongoDBConnection.close();
    }
}
