package org.ow2.contrail.provider.accounting.aggregates;

import org.json.JSONException;
import org.json.JSONObject;

public class MaxAggregate implements IAggregate {

    private double max;

    public MaxAggregate() {
        max = Double.MIN_VALUE;
    }

    @Override
    public void addValue(DateValue dv) {
        if ((Double) dv.value() > max)
            max = (Double) dv.value();
    }

    @Override
    public JSONObject getValue() {
        try {
            return new JSONObject().put("max", max);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
