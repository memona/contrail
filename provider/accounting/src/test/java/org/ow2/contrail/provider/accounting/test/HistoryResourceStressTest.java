package org.ow2.contrail.provider.accounting.test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.accounting.utils.DateUtils;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class HistoryResourceStressTest {
    private static final int TEST_DURATION = 3600;
    private static final String TARGET_URL = "http://10.31.1.1:8080/accounting";
    private static final int NUMBER_OF_THREADS = 10;

    public static void main(String[] args) throws Exception {
        new HistoryResourceStressTest().stressTest();
    }

    public void stressTest() {
        for (int i=0; i<NUMBER_OF_THREADS; i++) {
            StressTestThread stressTestThread = new StressTestThread(Integer.toString(i));
            Thread t = new Thread(stressTestThread);
            t.start();
        }
    }

    class StressTestThread implements Runnable {
        private String name;

        StressTestThread(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            try {
                Client client = Client.create();
                WebResource webResource = client.resource(TARGET_URL);

                JSONObject postData = new JSONObject();

                // sids
                postData.put("source", "host");
                JSONArray sids = new JSONArray();
                sids.put("host001.test.com");
                sids.put("host002.test.com");
                sids.put("host003.test.com");
                postData.put("sids", sids);

                // metrics
                JSONArray metrics = new JSONArray();
                metrics.put("cpu.load_one");
                metrics.put("cpu.load_five");
                metrics.put("cpu.idle");
                postData.put("metrics", metrics);

                // startTime and endTime
                Calendar startTimeCal = Calendar.getInstance();
                startTimeCal.set(2012, 5, 1, 0, 0, 0);
                Calendar endTimeCal = Calendar.getInstance();
                endTimeCal.set(2012, 5, 2, 0, 0, 0);
                postData.put("startTime", DateUtils.format(startTimeCal.getTime()));
                postData.put("endTime", DateUtils.format(endTimeCal.getTime()));

                // numberOfIntervals
                postData.put("numberOfIntervals", 5 * 24 * 60);

                Date testStartTime = new Date();
                do {
                    Date t0 = new Date();
                    ClientResponse response = webResource.path("/metrics_history/condensed_data")
                            .post(ClientResponse.class, postData);
                    Date t1 = new Date();
                    double time = (t1.getTime() - t0.getTime()) / 1000.0;
                    assertEquals(response.getStatus(), 200);
                    String content = response.getEntity(String.class);
                    System.out.println(name + "\t" +
                            response.getStatus() + "\t" +
                            String.format("%.3f", time) + "\t" +
                            content.length());
                } while ((new Date().getTime() - testStartTime.getTime()) < TEST_DURATION * 1000);
            }
            catch (Exception e) {
                System.out.println(String.format(
                        "Thread %s failed: %s", Thread.currentThread().getName(), e.getMessage()));
            }
        }
    }
}
