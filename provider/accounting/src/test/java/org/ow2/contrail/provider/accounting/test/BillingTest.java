package org.ow2.contrail.provider.accounting.test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import org.json.JSONArray;
import org.json.JSONObject;
import org.ow2.contrail.provider.accounting.pricing.IPriceNode;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertTrue;

public class BillingTest {

    // TODO: Modify tests so that are not machine dependent

    //@Test
    public void test() {
        SSLContext ctx = null;
        try {
            KeyStore trustStore;
            KeyStore keyStore;

            trustStore = KeyStore.getInstance("JKS");
            trustStore.load(new FileInputStream("/Users/ales/.ssh/cacerts.jks"), "contrail".toCharArray());
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(trustStore);

            keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream("/Users/ales/.ssh/client.jks"), "contrail".toCharArray());
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(keyStore, "contrail".toCharArray());

            ctx = SSLContext.getInstance("SSL");
            ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            System.out.println("SSL context initialized;");
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ClientConfig config = new DefaultClientConfig();
            config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(null, ctx));


            WebResource webResource = Client.create(config).resource("https://localhost:8443/contrail/accounting");

            {
                Calendar cal1 = new GregorianCalendar();
                cal1.set(2011, 6, 20);
                Calendar cal2 = new GregorianCalendar();
                cal2.set(2011, 7, 20);
                JSONObject request =
                        new JSONObject().put("type", IPriceNode.PriceNodeType.ROOT).put("entities", new JSONArray().put("n0001").put("n0002")).put("resource", "memory").put("pricing",
                                new JSONArray().put(
                                        new JSONObject().put("type", IPriceNode.PriceNodeType.PER_UNIT).put("fromDate", cal1.getTime()).put("toDate", cal2.getTime()).put("metrics", "")));


                String responseString = webResource.accept("application/json").type("application/json").post(String.class, request.toString());
            }


        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        assertTrue(true);
    }

}
