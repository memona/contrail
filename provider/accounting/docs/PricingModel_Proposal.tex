%
%  Accounting
%
%  Created by Ales Stimec on 2012-02-13.
%  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
%
\documentclass[11pt, a4paper]{article}

% Use utf-8 encoding for foreign characters
%\usepackage[cp1250]{inputenc}
\usepackage[utf8]{inputenc}

% Setup for fullpage use
\usepackage{fullpage}

 
% Uncomment some of the following if you use the features
%
% Running Headers and footers
%\usepackage{fancyhdr}

% Multipart figures
%\usepackage{subfigure}

% More symbols
%\usepackage{amsmath}
%\usepackage{amssymb}
%\usepackage{latexsym}

% Surround parts of graphics with box
\usepackage{boxedminipage}

\usepackage[osf,sc]{mathpazo} % Palatino
\usepackage{MnSymbol} % Minion Pro Symbols
\usepackage[babel=true]{microtype} % microtypographic enhancements
\usepackage{marvosym} % more symbols

\usepackage{xcolor}
% Package for including code in the document
\usepackage{listings}
\lstset{basicstyle=\ttfamily} % fixed-width font
%\lstset{tabsize=4}
%\lstset{gobble=4} % remove 4 leading characters (should be the indentation tab)
\lstset{backgroundcolor=\color{gray!10}} % background in a light gray
\lstset{frame=tb} % lines above and below
\lstset{rulecolor=\color{gray!50}} % colored in a slightly darker gray
%\lstset{keywordstyle=\bfseries} % keywords are bold
%\lstset{emphstyle=\itshape\color{green!30!black}} % emphasize style: italic, dark green
%\lstset{prebreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\rhookswarrow}}} % requires \usepackage{MnSymbol}
%\lstset{postbreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\rcurvearrowse\space}}} % requires \usepackage{MnSymbol}
\lstset{framexleftmargin=5pt}
\lstset{xleftmargin=5pt}
\lstset{breaklines=true, breakatwhitespace=true}
\lstset{numberstyle=\scriptsize}
\lstset{language=[LaTeX]TeX} % by default listings are LaTeX code
\lstset{texcsstyle=*\lst@keywordstyle} % emphasize backslash as part of the command but keep style as defined in \lstset{keywordstyle=...}
\lstset{moretexcs={lstset, rhookswarrow, rcurvearrowse, Righttorque, Lefttorque}} % add LaTeX commands which listings doesn't know
\lstset{emph={prebreak, postbreak, breaklines, breakatwhitespace, numbers, numberstyle, breakindent}} % emphasize parameters of listings package

% If you want to generate a toc for each chapter (use with book)
\usepackage{minitoc}


\setcounter{tocdepth}{5}
\setcounter{secnumdepth}{5}

% This is now the recommended way for checking for PDFLaTeX:
\usepackage{ifpdf}

%\newif\ifpdf
%\ifx\pdfoutput\undefined
%\pdffalse % we are not running PDFLaTeX
%\else
%\pdfoutput=1 % we are running PDFLaTeX
%\pdftrue
%\fi

\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi
\title{The Pricing Model Proposal}
\author{Aleš Štimec}

\date{\today}

\begin{document}

\ifpdf
\DeclareGraphicsExtensions{.pdf, .jpg, .tif}
\else
\DeclareGraphicsExtensions{.eps, .jpg}
\fi

\maketitle


\tableofcontents{}

\newpage

%
% INTRODUCTION
%
\section{Introduction}

This document will present a proposal for the pricing model structured
needed in the billing component. The pricing structure/document will
be presented to the accounting/billing component and will be used to
provide ``real-time'' pricing information to providers and their
customers.  The billing component will then be required to return
prices based on queries and information stored in the monitoring
database.

A few examples of queries expressed in natural language:
\begin{itemize} 
\item Calculate the price for virtual machines $<vm_1>$,
  $<vm_2>$, $<vm_3>$ and $<vm_4>$ based on pricing model $<model\_id>$. 
\item Calculate the price for virtual machine $<vm_1>$ running on host $<h_1>$ unti  today based on pricing model $<model\_id>$.
\item Calculate the price for virtual machine $<vm_1>$ between
  $<date_1>$ and $<date_2>$ based on pricing model $<model\_id>$.
\end{itemize}

\section{Prerequisites}

Before a final pricing model is defined we need to define a set of
resources and corresponding metrics that can be charged. As an
example, most cloud providers today use a flat-rate per-month price for
  offered instances types and additional charges for I/O
  operations, network transfer, storage etc. Once defined, these
  metrics will need to be implemented in Monitoring for use in
  Accounting and Billing - currently only instantaneous measurements
  are available from Monitoring (e.g. current free memory for a
  certain VM or host), but no integrative quantities are available
  (e.g. number of I/O operations over a certain time period, number of
  server HTTP requests over a certain time period).


\section{Pricing model} 

We propose to define the pricing model in a XML/JSON object in a tree
structure. During price calculatation the combined information from
the query and the pricing model is propagated towards the leaves of
the tree where queries into the monitoring database are made and the
price is calculated and then summed up on the return path to the root
of the tree.

\begin{figure}[hbp]
  \centering
  \includegraphics[width=0.75\textwidth]{figure1.pdf}
  \caption{Figure demonstrates how time validity is defined for
    different parts of the pricing model. }
  \label{figure:time}
\end{figure}

\subsection{A quick example}
As an example of information aggregation each subtree may have a time
interval over which it is valid for price calculation, which allows
providers to define a time-variable pricing model (e.g. higher prices
during business hours and lower nightly rates) as shown in Figure
\ref{figure:time}. Figure shows how information is propagated towards
leaves where the aggregated information is used to query into the
database. Complementary information from parent nodes is combined
with information stored in children, but information stored in any of the
children always takes precedence over information written in the
parent as it is propagated towards leaves. 
Example:
\begin{itemize}
\item If the parent node defines the ``from'' as $x.5.x$ (\emph{i.e.}
  from the fifth month) and the child node defines ``from'' as $1.x.x$
  (\emph{i.e.} from the first of the month) then the ``from'' field
  gets propagated as $1.5.x$ (\emph{i.e.} from the first of May).
\item If the parent node defines the ``from'' as $01.01.2012$ and
  the child node defines the ``from'' as $05.01.2012$, then the
  child's ``from'' would get propagated towards leaves of child's subtree.
\end{itemize}

\subsection{Node types}

Figure \ref{figure:price_model_example} shows and example of price
model definition. 

\begin{figure}[hb]
  \centering
  \includegraphics[height=.9\textheight]{PricingModelExample.pdf}
  \caption{Figure shows an example of a pricing model. }
  \label{figure:price_model_example}
\end{figure}

\subsubsection{The root node}

The root node of the price model definition may contain various
meta-information used by providers to identify the price model, its
versioning, etc. The root node may also contain the ``fromDate'' and
``toDate'' fields defining the validity period of the model.

The root node may contain (this list and list for other nodes will be
subject  to many changes in near future):
\begin{itemize}
\item \emph{id} - ID of the pricing model,
\item \emph{version} - version ID of the pricing model -- each pricing
  model may have one or more versions for use in different SLAs,
\item \emph{type} - always ``price\_model'',
\item \emph{fromDate} - date and time of the start of price model
  validity,
\item \emph{endDate} - date and time of the end of price model validity.
\item \emph{entities} - entities for which the pricing model is valid. May be hosts, VMs, etc.
\end{itemize}

\paragraph{Price calculation}

The price calculation of the root node is pretty
straighforward. First, the dates specified in the user query are
checked agains the validity period of the pricing model. If the two
date ranges do not overlap then $0$ is returned otherwise the
price query is simply forwarded to child nodes and the returned prices
are summed up and returned to the user.

\subsubsection{The price node}

Intermediate price nodes are used to separate price calculations
for different resources and possibly to add further restrictions to
underlying queries such as date-time restrictions, VM or host
restrictions etc.

The price node may contain:
\begin{itemize}
\item \emph{type} - always ``price\_node'',
\item \emph{fromDate} - date and time of the start of subtree
  validity,
\item \emph{endDate} - date and time of the end of price subtree
  validity,
\item \emph{resource} - definition of the resource to be evaluated in
  the subtree
\item \emph{metric} - metric of the resource to be evaluated in the
  subtree
\end{itemize}

\paragraph{Price calculation}

During the price calculation the price node augments the query with
the information it contains and then passes the query to its children
and then, like the root node, it sums up the prices returned by the
children. If the query contradicts any of the information
contained in the node then $0$ is returned to its parent (\emph{e.g.}
if the date intervals defined in the query and in the node do not
overlap - meaning that it is not relevant to the date interval
specified in the query).

\subsubsection{The query node} 

The query node preforms the actual query into the database using the
information passed to it by its parent node and then passes the
results of the query to its children for price calculation. 

This type of node is mainly used to reduce the number of queries into
the database and it is our plan to remove the need for this kind of
nodes in future versions of the billing system and have the system
itself determine at what point of the tree it would be best to perform
the query in order reduce the overall database load.

If the query contradicts any of the information contained in the node then $0$ is returned to its parent.

The query node may contain:
\begin{itemize}
\item \emph{type} - always ``query\_node'',
\item \emph{fromDate} - date and time of the start of subtree
  validity,
\item \emph{endDate} - date and time of the end of price subtree
  validity,
\item \emph{resource} - definition of the resource to be evaluated in
  the subtree
\item \emph{metric} - metric of the resource to be evaluated in the
  subtree
\end{itemize}

\subsubsection{Fixed-price node}

The fixed-price node is the simplest of the price-calculating
nodes. Using this node we can define a fixed price for a certain
resource/metric per certain time interval regardless of other metrics
(\emph{e.g.} availability of the VM). In this fashion we can define a
monthly, yearly or hourly cost for a certain resource. 

The price node may contain:
\begin{itemize}
\item \emph{type} - always ``fixes\_price\_node'',
\item \emph{fromDate} - date and time of the start of subtree
  validity,
\item \emph{endDate} - date and time of the end of price subtree
  validity,
\item \emph{unit} - definition of the time unit,
\item \emph{unit\_cost} - cost of the time unit of the specified resource. 
\end{itemize}

\paragraph{Price calculation}

Input to the fixed-price node price calculation is an array of JSON
objects returned by the query executed in the query node. Dates in
listed objects are compared against the time interval that may be
defined in this node, then the actual cost is calculated based on the
time unit and unit cost defined in the node.

\subsubsection{Per-use price node}

The idea behind the per-use price node is that we charge a certain
resource/metric per time unit of it's uptime, while downtime periods
are not charged. Such nodes contain the
definition of the time unit and the price of the resource/metric per
defined time unit.

The per-use price node may contain the following fields:
\begin{itemize}
\item \emph{type} - always ``per\_use\_price\_node'',
\item \emph{fromDate} - date and time of the start of subtree
  validity,
\item \emph{endDate} - date and time of the end of price subtree
  validity,
\item \emph{unit} - definition of the time unit,
\item \emph{unit\_cost} - cost of the time unit of the specified resource. 
\end{itemize} 

\paragraph{Price calculation}

Input to the per-use price node price calculation is an array of JSON
objects forwarded to it by the query node. Dates, that may be defined
in this node, are compared against the times defined in JSON Objects,
the list is sorted and uptime calculated. The actual cost of the
resource/metric is calculated based on the uptime, defined unit of
time and the defined cost per time unit.

\subsubsection{Per-unit price node}

The per-unit price node is used to define the price of a
resource/metric per consumed unit per time (\emph{e.g.} price of
allocated GB of memory per hour). 

The per-unit price node may contain the following fields:
\begin{itemize}
\item \emph{type} - always ``per\_unit\_price\_node'',
\item \emph{fromDate} - date and time of the start of subtree
  validity,
\item \emph{endDate} - date and time of the end of price subtree
  validity,
\item \emph{unit} - definition of metric unit and the time unit
  (\emph{e.g.} gigabyte per hour - GB/h),
\item \emph{unit\_cost} - cost of the time unit of the specified resource. 
\end{itemize} 

\paragraph{Price calculation}

Input to the per-use price node price calculation is an array of JSON
objects forwarded to it by the query node. Dates, that may be defined
in this node, are compared against the times defined in JSON Objects,
the array is sorted, elapsed time between entries calculated and
consumed units of resource/metric charged accordingly.

\subsubsection{The conditional node}

The most complex construct in the price mode is the conditional node
type. It enables the provider to define resource/metric price
calculation based on the consumption of another resource. The
conditional node performs one or two queries, matches entries of the
returned results based on time, performs defined binary operation
between matched entries and uses a defined comparison operator to
compare the result of the binary operation with the defined
constant value to determine to which of its child nodes the corresponding
entry should be forwarded. What gets forwarded to the child nodes is
determined by the  ``forward'' field as explained below.

The conditional node may contain: 
\begin{itemize}
\item \emph{type} - always ``if\_query\_node'',
\item \emph{fromDate} - date and time of the start of subtree
  validity,
\item \emph{endDate} - date and time of the end of price subtree
  validity,
\item \emph{conditional1} - definition of the first query
\item \emph{conditional2} - definition of the second query
  \textbf{Note:} Exactly one of the two conditionals must contain the
  ``\$parent" clause which signals the system that that conditional
  inherits query properties forwarded from the parent node augmented
  by other fields defined in the conditional,
\item \emph{twise\_operator} - definition of the binary operation to be
  performed on time-matched entries (each entry of results returned by
  the conditional containing the ``\$parent'' clause is matched
  time-wise to one entry returned by the other conditional). The exact
  set of these operators will be determined in the future but a short
  list of these operators might include 
  \begin{itemize}
    \item sum,
    \item subtraction,
    \item multiply,
    \item divide,
    \item max,
    \item min,
  \end{itemize} all of which are self-explanatory.
\item \emph{operator} - definition of the comparison operator. A short
  list of possible operators may be:
  \begin{itemize}
    \item eq - equal,
    \item neq - not equal,
    \item lt - less than,
    \item gt - greater than,
    \item lte - less than or equal,
    \item gte - greater than or equal.
  \end{itemize}
\item \emph{limit} - defined constant used in comparison 
\item \emph{forward} - definition of what is forwarded to the child
  nodes. May be one of the following:
  \begin{itemize}
    \item \$original - the entry returned by the conditiona containing
      ``\$parent'' clause,
    \item \$result - the result of the ``twise\_operator'', 
    \item \$original\_minus\_limit - same as \$original but the limit
      constant is subtracted from the metric of each entry,
    \item \$original\_plus\_limit,
    \item \$result\_minus\_limit, same as \$result but the limit
      constant is subtracted,
    \item \$result\_plus\_limit.
   \end{itemize}
\end{itemize}


\paragraph{Price calculation}

The conditional node executes two queries defined by ``conditional1''
and ``conditional2'' fields, then entries of the results of the
conditional containing the ``\$parent'' clause are time-wise matched
to the entries of the result of the other query.  The
``twise\_operator'' operation is performed between
matched entries and results compared using the ``operator'' comparison against the defined ``limit'' constant. Two arrays of
entries are formed, one for which the comparison returns ``true'' and
the other for which the comparison returns ``false''. Each array is
then forwarded to the corresponding child node and the resulting two
prices are summed up and returned to the parent node.

\section{Example}

What follows is a short explantion of the price model show in figure
\ref{figure:price_model_example}. The price model is, as
stated in the root node, valid until $31.12.2012$. The price
calculation is composed of price calculations for four different
resources: disk, memory, cpu and network.

For the disk, the price model prescribes a per-unit cost of
$0.005EUR$ per used GB per day. A query is made into the monitoring
database to retrieve all entries corresponding to the VMs listed in
the query and the ``used'' metric is retrieved for the ``disk''
resource. The price is then calculated as stated above.

For the memory the price calculcation is a bit more complex, since the
price depends on the ratio of the VM total memory against the host
total memory. If the VM takes up less than half of the host's memory
then the customer is charged $0.001EUR$ per allocated GB per hour. If on
the other hand, the VM takes up more or equal to one half of the
host's total memory then the customer is charged $0.003EUR$ per
allocated GB per hour.

The CPU is however charged based on the time-of-day. During business
hours (from $6am$ to $5pm$ the customer charged $0.1EUR$ per uptime
hour, during the rest of the day the charge is $0.03EUR$ per hour
making the VM uptime cheaper between $5pm$ and $6am$.

As for network, the customer pays $10EUR$ for $100GB$ of data transfer
per month. Any data above $100GB$ limit is charged $0.01EUR$ per GB.

\end{document}
