/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.test.rest;

import org.junit.Before;
import org.junit.Test;

public class EscapeOvfTest {

	private final String fileName = "C:/dev/Contrail/provider/provider-sla/planning-optimization/src/main/resources/sla.xml";

	private String ovfFile = null;

	private String ovfFile2 = null;

	@Before
	public void loadFile() {
		/*
		 * try { BufferedReader br=new BufferedReader(new
		 * InputStreamReader(this.
		 * getClass().getClassLoader().getResourceAsStream(fileName)));
		 * StringBuilder sb = new StringBuilder(); String line = br.readLine();
		 * while (line != null) { sb.append(line); line = br.readLine(); }
		 * ovfFile=sb.toString(); } catch (Exception e) { e.printStackTrace(); }
		 * try { ovfFile=FileUtils.readFileToString(new File(fileName)); } catch
		 * (IOException e) { e.printStackTrace(); } try { BufferedReader br=new
		 * BufferedReader(new FileReader(fileName)); StringBuffer sb=new
		 * StringBuffer(); while(br.ready()){ sb.append(br.readLine()); }
		 * ovfFile2=sb.toString(); } catch (FileNotFoundException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } catch (IOException
		 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 */

	}

	@Test
	public void testEscapeFile() {
		/*
		 * String u=StringEscapeUtils.unescapeJava(ovfFile);
		 * System.out.println("Before escape:"); System.out.println(u);
		 * System.out.println("\\\\"); System.out.println("\\");
		 * System.out.println("After escape:"); String
		 * m=StringEscapeUtils.escapeJava(ovfFile2); String
		 * z=StringEscapeUtils.escapeJavaScript
		 * (StringEscapeUtils.escapeJavaScript(ovfFile)); String
		 * s="\\"+"ciao\\\\\""+"\\"; System.out.println(z); String
		 * v=StringUtils.replace(s, "\\", "");
		 */
	}

}
