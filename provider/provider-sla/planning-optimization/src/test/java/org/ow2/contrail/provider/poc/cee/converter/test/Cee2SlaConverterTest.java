/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.cee.converter.test;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.provider.poc.cee.converter.Cee2SlaConverter;
import org.ow2.contrail.provider.poc.slatemplate.ContrailSlaTemplate;
import org.ow2.contrail.provider.poc.slatemplate.parser.ContrailSlaTemplateParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.sla.SLATemplate;

public class Cee2SlaConverterTest {

	private static SLATemplate slatProposal;

	private static ContrailSlaTemplate cst = null;

	@Before
	public void setUp() throws Exception {

		String proposalXml = FileUtils.readFileToString(new File("src/test/resources/slats/SlaTemplateFINAL.xml"));
		SLASOITemplateParser tp = new SLASOITemplateParser();
		slatProposal = tp.parseTemplate(proposalXml);
		cst = ContrailSlaTemplateParser.getContrailSlat(slatProposal);
		System.out.println("PROPOSAL: " + cst);
	}

	//@Test
	public void testCee2SlaConverter() {
		Cee2SlaConverter builder = new Cee2SlaConverter();
		builder.setCeeID("1234");
		builder.setContrailSlatemplate(cst);
		SLATemplate slaTemplate = builder.cee2Sla();
		System.out.println(slaTemplate);
	}
}
