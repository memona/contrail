/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.impl.parser.test;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.ow2.contrail.provider.poc.slatemplate.ContrailSlaTemplate;
import org.ow2.contrail.provider.poc.slatemplate.parser.ContrailSlaTemplateParser;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.sla.SLATemplate;

public class ContrailSlaTemplateParserTest {

	private static SLATemplate slatProposal;

	@Before
	public void setUp() throws Exception {

		String proposalXml = FileUtils.readFileToString(new File("src/test/resources/slats/SlaTemplateFINAL.xml"));
		SLASOITemplateParser tp = new SLASOITemplateParser();
		slatProposal = tp.parseTemplate(proposalXml);
	}

	@Test
	public void testParseSlaProposal() {
		ContrailSlaTemplate cst = ContrailSlaTemplateParser.getContrailSlat(slatProposal);
		System.out.println("PROPOSAL: " + cst);
	}

}
