/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.manager.resource;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AvailabilityRetriever {

	private URL url;

	private String path = "temp.ovf";

	public AvailabilityRetriever() {

	}

	public void retrieveAvailability(String url) {
		try {
			this.url = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) this.url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/xml");
			InputStream isr = connection.getInputStream();
			FileOutputStream fos = new FileOutputStream(path);
			int count;
			while ((count = isr.read()) != -1) {
				fos.write(count);
			}
			isr.close();
			fos.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setPath(String p) {
		path = p;
	}

	public String getPath() {
		return path;
	}

}
