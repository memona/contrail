/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.manager.resource;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.ow2.contrail.common.ParserManager;
import org.ow2.contrail.common.exceptions.MalformedOVFException;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.common.implementation.application.ApplicationDescriptor;
import org.ow2.contrail.common.implementation.ovf.OVFVirtualHardware;
import org.ow2.contrail.common.implementation.ovf.OVFVirtualSystem;
import org.ow2.contrail.common.implementation.ovf.SharedDisk;
import org.ow2.contrail.common.implementation.ovf.virtualhardware.OVFVirtualHwCpu;
import org.ow2.contrail.common.implementation.ovf.virtualhardware.OVFVirtualHwMemory;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

public class OvfResourceParser {

	private ParserManager parserManager;

	public OvfResourceParser(String ovfFilePath) {
		try {
			URI uri = new URI(ovfFilePath);
			parserManager = new ParserManager(uri);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedOVFException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<OVFVirtualSystem> getVirtualSystems() {
		ArrayList<OVFVirtualSystem> virtualSystems = new ArrayList<OVFVirtualSystem>();
		ApplicationDescriptor ad = parserManager.getApplication();
		Collection<ApplianceDescriptor> appliD = ad.getAllAppliances();
		for (ApplianceDescriptor a : appliD) {
			Collection<OVFVirtualSystem> ovc = parserManager.getApplianceVirtualSystem(a.getID());
			for (OVFVirtualSystem ovfVS : ovc) {
				virtualSystems.add(ovfVS);
			}
		}
		return virtualSystems;
	}

	public ArrayList<SharedDisk> getSharedDisks() {
		return (ArrayList<SharedDisk>) parserManager.getAllSharedDisks();
	}

	public HashMap<String, Double> getVirtualSystemNeed(OVFVirtualSystem vs) {
		Collection<OVFVirtualHardware> hardwares = vs.getRequiredHardware();
		if (hardwares.size() != 0) {
			HashMap<String, Double> vsNeed = new HashMap<String, Double>();
			// init map
			// TO DO set cpu speed
			// vsNeed.put("cpu_speed", -1);
			vsNeed.put("memory", (double) -1);
			vsNeed.put("vm_cores", (double) -1);
			for (OVFVirtualHardware h : hardwares) {
				if (h instanceof OVFVirtualHwCpu) {
					h = (OVFVirtualHwCpu) h;
					vsNeed.put("vm_cores", new Double(new Long(((OVFVirtualHwCpu) h).getVirtualQuantity()).toString()));

				}
				if (h instanceof OVFVirtualHwMemory) {
					h = (OVFVirtualHwMemory) h;
					vsNeed.put("memory", new Double(new Long(((OVFVirtualHwMemory) h).getVirtualQuantity()).toString()));
				}
			}
			return vsNeed;
		} else {
			return null;
		}
	}
}
