/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.provider.poc.cee;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.poc.enums.ContrailAgreementTerm;
import org.ow2.contrail.provider.poc.manager.resource.VepResourceManager;
import org.ow2.contrail.provider.poc.slatemplate.ContrailSlaTemplate;
import org.ow2.contrail.provider.poc.slatemplate.request.SharedDisk;
import org.ow2.contrail.provider.poc.slatemplate.request.VirtualSystem;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.GenericGuarantee;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.GenericGuarantee.Value;
import org.ow2.contrail.provider.poc.slatemplate.request.guarantee.ResourceGuarantee;

public class CEEObject {

	private JSONObject ceeJson;

	private ContrailSlaTemplate contrailslaTemplate = null;

	private HashMap<VirtualSystem, VMHandler> mapping = null;

	public CEEObject(Builder builder) {
		this.contrailslaTemplate = builder._contrailslaTemplate;
		this.mapping = builder._mapping;
		this.ceeJson = builder._ceeJson;
	}

	public JSONObject getCeeJson() {
		return ceeJson;
	}

	public void setStateCheck() {
		try {
			ceeJson.put("state", "CHECK");
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public ContrailSlaTemplate getContrailslaTemplate() {
		return contrailslaTemplate;
	}

	public HashMap<VirtualSystem, VMHandler> getMapping() {
		return mapping;
	}

	public static class Builder {

		private ContrailSlaTemplate _contrailslaTemplate = null;

		private JSONObject _ceeJson = null;

		private HashMap<VirtualSystem, VMHandler> _mapping = null;

		public Builder setContrailSlatemplate(ContrailSlaTemplate cst) {
			this._contrailslaTemplate = cst;
			return this;
		}

		public Builder setMapping(HashMap<VirtualSystem, VMHandler> m) {
			this._mapping = m;
			return this;
		}

		private Builder init(String name) {
			try {
				this._ceeJson = new JSONObject().put("name", name);
				this._ceeJson.put("state", "ACTIVE");
				this._ceeJson.put("VMHandlers", new JSONArray());
				this._ceeJson.put("NetworkHandlers", new JSONArray());
				this._ceeJson.put("StorageHandlers", new JSONArray());
				this._ceeJson.put("applications", new JSONArray());
				this._ceeJson.put("defaultMapping", new JSONArray());
				this._ceeJson.put("constraintsMapping", new JSONArray());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return this;
		}

		public Builder(String name) {
			init(name);
		}

		public CEEObject build() {
			for (VirtualSystem vs : _mapping.keySet()) {
				VMHandler vmh = _mapping.get(vs);
				addvmHandler(vmh);
				addApplication("Application SLA", _contrailslaTemplate.getOvfFile(), vs);
				for (GenericGuarantee gg : vs.getGenericGuarantees()) {
					addContraintsMappingVS(gg, vs);
				}
				double cpuFreq = 0, cores = 0, memory = 0;
				for (ResourceGuarantee rg : vs.getResourceGuarantees()) {
					if (rg.getAgreementTerm().equals(ContrailAgreementTerm.cpu_speed))
						cpuFreq = rg.getDefault();
					if (rg.getAgreementTerm().equals(ContrailAgreementTerm.memory))
						memory = rg.getDefault();
					if (rg.getAgreementTerm().equals(ContrailAgreementTerm.vm_cores))
						cores = rg.getDefault();
				}
				addDefaultMapping(vmh, "VM", vs.getOvfId(), cpuFreq, cores, memory);
			}
			// for SharedDisk
			for (SharedDisk sd : _contrailslaTemplate.getSharedDisks()) {
				for (GenericGuarantee gg : sd.getGenericGuarantees()) {
					addContraintsMappingSD(gg, sd);
				}
			}
			return new CEEObject(this);
		}

		public void addvmHandler(VMHandler vmH) {
			try {
				_ceeJson.accumulate("VMHandlers", new JSONObject().put("href", vmH.getHref()));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		public void addApplication(String applicationName, String ovfFile, VirtualSystem vs) {
			try {
				JSONArray applications = _ceeJson.optJSONArray("applications");
				JSONObject joApplication = new JSONObject();
				if (applications == null || applications.length() == 0) {
					joApplication.put("name", applicationName);
					joApplication.put("OVFDeployment", false);
					JSONArray ovfDescrArray = new JSONArray();
					JSONObject joFile = new JSONObject();
					// String s=escapeString(ovfFile);
					joFile.put("OVFFile", ovfFile);
					ovfDescrArray.put(joFile);
					joApplication.put("OVFDescriptors", ovfDescrArray);
					GenericGuarantee reservationGuarantee = vs.getReservationGuarantee();
					if (reservationGuarantee != null) {
						JSONArray resArray = new JSONArray();
						JSONObject jaReservation = new JSONObject();
						jaReservation.put("virtualSystem", new JSONObject().put("href", "#" + vs.getOvfId()));
						jaReservation.put("count", new Integer(reservationGuarantee.getValues().get(0).getValue()).intValue()); // reservation
																										// has
																										// unique
																										// value
						int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH), month = Calendar.getInstance().get(Calendar.MONTH) + 1, year = Calendar.getInstance().get(Calendar.YEAR) + 1;
						jaReservation.put("end-date", day + "/" + month + "/" + year);
						resArray.put(jaReservation);
						joApplication.put("reservations", resArray);
					}
				} else {
					for (int i = 0; i < applications.length(); i++) {
						JSONObject j = applications.optJSONObject(i);
						if (j != null && j.optJSONArray("reservations") != null) {
							GenericGuarantee reservationGuarantee = vs.getReservationGuarantee();
							if (reservationGuarantee != null) {
								JSONArray resArray = j.optJSONArray("reservations");
								JSONObject jaReservation = new JSONObject();
								jaReservation.put("virtualSystem", new JSONObject().put("href", "#" + vs.getOvfId()));
								jaReservation.put("count", new Integer(reservationGuarantee.getValues().get(0).getValue()).intValue()); // reservation
																												// has
																												// unique
																												// value
								int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH), month = Calendar.getInstance().get(Calendar.MONTH) + 1, year = Calendar.getInstance().get(Calendar.YEAR) + 1;
								jaReservation.put("end-date", day + "/" + month + "/" + year);
								resArray.put(jaReservation);
								// joApplication.put("reservations", resArray);
							}
						}
					}
				}
				_ceeJson.accumulate("applications", joApplication);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		public void addContraintsMappingVS(GenericGuarantee gg, VirtualSystem vs) {
			try {
				ArrayList<Value> values = (ArrayList<Value>) gg.getValues();
				for (Value v : values) {
					Constraint c = VepResourceManager.getConstraint(gg.getAgreementTerm(), v);
					if (c != null) {
						JSONObject constraintObject = new JSONObject().put("constraint", new JSONObject().put("href", c.getURI()));
						constraintObject.put("parameter", v.getValue());
						JSONArray ja = new JSONArray();
						ja.put(new JSONObject().put("href", "#" + vs.getOvfId()));
						constraintObject.put("virtualSystem", ja);
						_ceeJson.accumulate("constraintsMapping", constraintObject);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		public void addContraintsMappingSD(GenericGuarantee gg, SharedDisk sd) {
			try {
				ArrayList<Value> values = (ArrayList<Value>) gg.getValues();
				for (Value v : values) {
					Constraint c = VepResourceManager.getConstraint(gg.getAgreementTerm(), v);
					if (c != null) {
						JSONObject constraintObject = new JSONObject().put("constraint", new JSONObject().put("href", c.getURI()));
						constraintObject.put("parameter", v.getValue());
						JSONArray ja = new JSONArray();
						ja.put(new JSONObject().put("href", "#" + sd.getOvfId()));
						constraintObject.put("storage", ja);
						_ceeJson.accumulate("constraintsMapping", constraintObject);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		public void addDefaultMapping(VMHandler vmHandler, String type, String virtualSystem, double cpuFreq, double coreCount, double memory) {
			try {
				JSONObject mappingObject = new JSONObject().put("type", type);
				mappingObject.put("virtualSystem", new JSONObject().put("href", "#" + virtualSystem));
				mappingObject.put("handler", new JSONObject().put("href", vmHandler.getHref()));
				if (cpuFreq != -1)
					mappingObject.put("cpuFreq", cpuFreq);
				if (coreCount != -1)
					mappingObject.put("corecount", coreCount);
				if (memory != -1)
					mappingObject.put("ram", memory);
				_ceeJson.accumulate("defaultMapping", mappingObject);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String toString() {
		return ceeJson.toString();
	}
}
