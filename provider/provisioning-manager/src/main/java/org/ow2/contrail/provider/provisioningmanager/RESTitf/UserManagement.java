package org.ow2.contrail.provider.provisioningmanager.RESTitf;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.ProvisioningManagerController;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.UserPropagation;
import org.ow2.contrail.provider.provisioningmanager.utils.CertificateUtils;

import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.*;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.*;
 
@Path("/"+USER_MGMT_PATH)
public class UserManagement
{
	private static Logger logger = Logger.getLogger(ApplicationManagement.class);
	@PUT
	@Path(PROPAGATE_PATH)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject propagateUser(JSONObject inputBody, @Context HttpHeaders headers)
	{
		String loggerTag = "propagateUser: ";
		logger.info(loggerTag + "Received PUT user-mgmt/propagate");
		
		String uuid = "";
		String certString = null;
		try{
			certString = headers.getRequestHeader("X-Certificate").get(0);
		} catch (Exception e){
				logger.warn(loggerTag + "UserCertificate exception. No Certificate in the request header");
		}
		uuid=CertificateUtils.retrieveUserUuidFromCertificate(certString);
		
		JSONObject retValue = new JSONObject();
		try
		{
			String username = inputBody.getString(USERNAME);
			UserPropagation propagation = new UserPropagation(username, uuid);
			Future<Boolean> ret = ProvisioningManagerController.getInstance().exec(propagation);

			if (ret != null && ret.get())
			{
				retValue.put(RETURN_STATUS, "200");
				logger.info(loggerTag + "SUCCESS: user propagated: "+ inputBody.getString(USERNAME));
				return retValue;
			}
			else
			{
				logger.error(loggerTag + "ERROR: user  not propagated: "+ inputBody.getString(USERNAME));
				retValue.put(RETURN_STATUS, "500");
				return retValue;
			}

		}
		catch (JSONException e)	{ e.printStackTrace();	}
		catch (InterruptedException e){ e.printStackTrace(); }
		catch (ExecutionException e) { e.printStackTrace();	}

		return retValue;

	}

}
