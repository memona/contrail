package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.RETURN_STATUS;
import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.VM_IDS;

import java.util.List;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Application;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;

import com.sun.jersey.api.client.UniformInterfaceException;

public class ApplicationDeployment implements Callable<JSONObject> {

	private String ceeIdentifier;
	private String userUuid;
	private String applicationId;
	private JSONObject deploymentDocument;
	private static Logger logger = Logger.getLogger(ApplicationDeployment.class);
	
	public ApplicationDeployment(String ceeId, String appId, String uuid, JSONObject deploymentDoc) {
		this.ceeIdentifier = ceeId;
		this.userUuid = uuid;
		this.applicationId = appId;
		this.deploymentDocument = deploymentDoc;
	}
	
	
	
	@Override
	public JSONObject call() throws Exception {
		
		JSONObject returnStatus = new JSONObject();
		try{
			JSONObject ret = null;
			ret = VEPconnector.getInstance().deployApplication(this.ceeIdentifier, this.applicationId, this.deploymentDocument, this.userUuid);
			
			if(ret.get(RETURN_STATUS).equals("200")){
				returnStatus.put(RETURN_STATUS, ret.get(RETURN_STATUS) );
			}
			else
				returnStatus.put(RETURN_STATUS,  ret.get(RETURN_STATUS));

		} catch(UniformInterfaceException e){
		
			returnStatus.put(RETURN_STATUS, "UniformInterfaceException"+ e.getResponse().getClientResponseStatus().getStatusCode());
			e.printStackTrace();
			return returnStatus;
	
		} catch (JSONException e) {
			returnStatus.put(RETURN_STATUS, "500" );
			e.printStackTrace();
		} catch (Exception e) {
			returnStatus.put(RETURN_STATUS, "500" );
			e.printStackTrace();
		}
		
	
	
		return returnStatus;
	}

}
