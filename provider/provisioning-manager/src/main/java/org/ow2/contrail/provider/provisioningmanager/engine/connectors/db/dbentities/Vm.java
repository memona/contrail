package org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the VMS database table.
 * 
 */
@Entity
@Table(name="VMS")
public class Vm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(nullable=false)
	private long id;

	@Column(length=30)
	private String status;

	@Column(length=30)
	private String virtualSystem;
	
	

	@Column(name="VM_VEP_ID")
	private long vmVepId;

	@Column(name="VM_VIN_ID")
	private long vmVinId;

	@ManyToOne
	@JoinColumn(name="APPLICATION_LINK")
	private Application Application;
	
	
	//bi-directional many-to-one association to VepTemplate
    @ManyToOne
	@JoinColumn(name="VEP_TEMPLATE_LINK")
	private VepTemplate vepTemplate;

    public Vm() {
    }

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getVirtualSystem() {
		return virtualSystem;
	}

	public void setVirtualSystem(String virtualSystem) {
		this.virtualSystem = virtualSystem;
	}

	public Application getApplication() {
		return Application;
	}

	public void setApplication(Application application) {
		Application = application;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getVmVepId() {
		return this.vmVepId;
	}

	public void setVmVepId(long vmVepId) {
		this.vmVepId = vmVepId;
	}

	public long getVmVinId() {
		return this.vmVinId;
	}

	public void setVmVinId(long vmVinId) {
		this.vmVinId = vmVinId;
	}

	public VepTemplate getVepTemplate() {
		return this.vepTemplate;
	}

	public void setVepTemplate(VepTemplate vepTemplate) {
		this.vepTemplate = vepTemplate;
	}
	
	public String toString(){
		return "{\n VM_ID: "+ this.id + "\n VM_VEP_ID: "+ this.vmVepId +"\n Application Id: " +this.Application.getId()+"\n VirtualSystem: "+ this.virtualSystem+"\n status: "+ this.status+"\n}";
		
	}
	
}