package org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the SLAS database table.
 * 
 */
@Entity
@Table(name="SLAS")
public class Sla implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(nullable=false, length=60)
	private String id;

    @Lob()
	@Column(name="SLA_XML_DESCR")
	private String slaXmlDescr;

	//bi-directional many-to-one association to PmUser
    @ManyToOne
	@JoinColumn(name="USER_LINK")
	private PmUser pmUser;

    public Sla() {
    }

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSlaXmlDescr() {
		return this.slaXmlDescr;
	}

	public void setSlaXmlDescr(String slaXmlDescr) {
		this.slaXmlDescr = slaXmlDescr;
	}

	public PmUser getPmUser() {
		return this.pmUser;
	}

	public void setPmUser(PmUser pmUser) {
		this.pmUser = pmUser;
	}
	
}