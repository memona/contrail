package org.ow2.contrail.provider.provisioningmanager.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class CertificateUtils {
	
	private static Logger logger = Logger.getLogger(CertificateUtils.class);
	
	public static String retrieveUserUuidFromCertificate(String certString){
		//String loggerTag= "[retrieveUserUuidFromCertificate] ";
		//logger.info(certString);
		String uuid ="";
		X509Certificate cert=null;
		try {
			byte[] decoded = Base64.decodeBase64(certString);
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
			InputStream in = new ByteArrayInputStream(decoded);
			cert = (X509Certificate)certFactory.generateCertificate(in);
		} catch (CertificateException e1) {
			logger.warn("UserCertificate exception. Error in decode certificate");
		}
		try{
			String[] splittedSubject = ((String) cert.getSubjectAlternativeNames().iterator().next().get(1)).split(":");
			if(splittedSubject[1].equals("uuid")){
				uuid = splittedSubject[2];
			}
			else logger.warn("no uuid found");
		} catch (CertificateException e1) {
			logger.warn("UserCertificate exception. No UserUuid retrieved");	
		}
		return uuid;
	}

}
