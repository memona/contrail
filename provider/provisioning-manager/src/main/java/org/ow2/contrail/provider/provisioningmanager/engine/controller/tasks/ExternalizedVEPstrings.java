package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

public interface ExternalizedVEPstrings {

	// STANDARD VEP ADDRESSES
	public static final String stdHostName 		= "localhost";
	public static final String stdPortName 		= "10500";
	public static final String PROTOCOL_PREFIX	= "http://";
	
	// ACTIONS
	public static final String STOP_OP			= "stop";
	public static final String DEPLOY_OP		= "deploy";
	public static final String INITIALIZE_OP	= "initialize";
	
	// PATH ELEMENTS
	public static final String ACTION_PATH		= "action";
	public static final String ID_PATH			= "id";
	public static final String OVF_PATH			= "ovf";
	public static final String USER_PATH		= "user";
	
	
	// INPUT VALUES
	public static final String USERROLE_VALUE		= "role";
	public static final String USERVID_VALUE		= "vid";
	public static final String USERGROUPS_VALUE		= "groups";
	
	// RETURN VAULES
	public static final String SNO_VALUE		= "sno";
	public static final String VM_STATE_LIST	= "vm_state_list";
	
	// HEADERS
	public static final String USERNAME_HEADER 	= "X-Username";
}
