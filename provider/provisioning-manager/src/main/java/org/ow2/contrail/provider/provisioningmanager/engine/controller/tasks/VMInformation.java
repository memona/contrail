package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

import java.util.concurrent.Callable;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;

import org.ow2.contrail.provider.provisioningmanager.engine.controller.datamanagers.ApplicationsData;

public class VMInformation implements Callable<JSONObject> {

	Integer vmID = null;
    String user = null;
	
	public VMInformation(Integer VMid, String User){
		
		vmID = VMid;
        user = User;
	}
	
	
	@Override
	public JSONObject call() throws Exception {
		
            JSONObject ret = VEPconnector.getInstance().getVMinfo(vmID, user);        
            return ret;
	}

}
