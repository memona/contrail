package org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the APPLICATIONS database table.
 * 
 */
@Entity
@Table(name="APPLICATIONS")
public class Application implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(nullable=false)
	private long id;

	@Column(length=30)
	private String status;
	
	private String ceeId;
	
	@Column(unique=true)
	private String vepAppId;
	
	//bi-directional many-to-one association to Ovf
    @ManyToOne
	@JoinColumn(name="OVF_LINK")
	private Ovf ovf;

	//bi-directional many-to-one association to PmUser
    @ManyToOne
	@JoinColumn(name="USER_LINK")
	private PmUser pmUser;

    public Application() {
    }

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getCeeId() {
		return this.ceeId;
	}

	public void setCeeId(String ceeIdentifier) {
		this.ceeId = ceeIdentifier;
	}
	
	public String getVepAppId() {
		return this.vepAppId;
	}

	public void setVepAppId(String appId) {
		this.vepAppId = appId;
	}

	public Ovf getOvf() {
		return this.ovf;
	}

	public void setOvf(Ovf ovf) {
		this.ovf = ovf;
	}
	
	public PmUser getPmUser() {
		return this.pmUser;
	}

	public void setPmUser(PmUser pmUser) {
		this.pmUser = pmUser;
	}
	
}