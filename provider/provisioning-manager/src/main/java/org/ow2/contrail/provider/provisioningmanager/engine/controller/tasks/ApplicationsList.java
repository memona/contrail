package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;

/**
 * NEW CLASS: Callable class to call list applications from VEP Connector instance. @author: Marco
 */

public class ApplicationsList implements Callable<JSONObject> {

	private String ceeIdentifier;
	private String userUuid;
	
	private static Logger logger = Logger.getLogger(ApplicationsList.class);
	private static String loggerTag = "[ApplicationsListTask] ";
	
	public ApplicationsList(String uuid){

		this.userUuid = uuid;
	}
	
	
	@Override
	public JSONObject call() throws Exception {
		/*
		 * TODO: Retrieve CeeId from SLAM-P
		 */
		JSONObject retlist = new JSONObject();
		retlist = VEPconnector.getInstance().getApplications(this.ceeIdentifier, this.userUuid);
		return retlist;
	}

}