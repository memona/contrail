package org.ow2.contrail.provider.provisioningmanager.RESTitf;

public interface ExternalizedRESTstrings {

	// REST Paths
	public static final String APP_MGMT_PATH	=	"application-mgmt";
	public static final String START_PATH		=	"start";
	public static final String STOP_PATH		=	"stop";
	public static final String DELETE_PATH		=	"delete";
	public static final String SUBMIT_PATH		=	"submit";
	public static final String DEPLOY_PATH		=	"deploy";
	public static final String INFO_PATH		= 	"info";
	public static final String MANAGE_PATH		=	"manage";
    public static final String VM_MGMT_PATH     =   "vm-mgmt";
	
	public static final String PROVIDER_MGMT_PATH 	  = "provider-mgmt";
	public static final String PROVISIONING_MGMT_PATH = "provisioning-mgmt";
	public static final String STOP_PROVISIONING_PATH = "stop";
	
	
	public static final String USER_MGMT_PATH 	=	"user-mgmt";
	public static final String PROPAGATE_PATH 	= 	"propagate";
	
	public static final String VIN_MAPPINGS	=	"vin_ids";
	
	// User features
	public static final String USERNAME		=		"name";
	public static final String USERROLE		=		"role";
	public static final String USERVID		=		"vid";
	public static final String USERSLAID	=		"slaid";
	public static final String USERGROUPS	=		"groups";
	public static final String XUSERNAME	=		"xUser";
	
}
