package org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the PM_USERS database table.
 * 
 */
@Entity
@Table(name="PM_USERS")
public class PmUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(nullable=false, length=60)
	private String username;

	@Column(name="SLA_USER_ID")
	private int slaUserId;

	//bi-directional many-to-one association to Application
	@OneToMany(mappedBy="pmUser", cascade={CascadeType.PERSIST})
	private List<Application> applications;

	//bi-directional many-to-one association to Sla
	@OneToMany(mappedBy="pmUser", cascade={CascadeType.PERSIST})
	private List<Sla> slas;
	
	@Column(name = "userUuid", unique = true)
	private String userUuid;

	public String getUserUuid() {
		return this.userUuid;
	}

	public void setUserUuid(String uuid) {
		this.userUuid = uuid;
	}

	public PmUser() {
    }

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getSlaUserId() {
		return this.slaUserId;
	}

	public void setSlaUserId(int slaUserId) {
		this.slaUserId = slaUserId;
	}

	public List<Application> getApplications() {
		return this.applications;
	}

	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}
	
	public List<Sla> getSlas() {
		return this.slas;
	}

	public void setSlas(List<Sla> slas) {
		this.slas = slas;
	}
	
	public String toString(){
		return "USER: " + this.username + " : " + this.userUuid;
	}
	
}