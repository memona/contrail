package org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the VEP_TEMPLATES database table.
 * 
 */
@Entity
@Table(name="VEP_TEMPLATES")
public class VepTemplate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(nullable=false)
	private long id;

	@Column(length=30)
	private String status;

	@Column(name="VEP_ASSIGNED_ID")
	private long vepAssignedId;

	@Column(name="VIRTUAL_SYSTEM_NAME", length=60)
	private String virtualSystemName;

	//bi-directional many-to-one association to Ovf
    @ManyToOne
	@JoinColumn(name="OVF_LINK")
	private Ovf ovf;

	//bi-directional many-to-one association to Vm
	@OneToMany(mappedBy="vepTemplate")
	private List<Vm> vms;

    public VepTemplate() {
    }

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getVepAssignedId() {
		return this.vepAssignedId;
	}

	public void setVepAssignedId(long vepAssignedId) {
		this.vepAssignedId = vepAssignedId;
	}

	public String getVirtualSystemName() {
		return this.virtualSystemName;
	}

	public void setVirtualSystemName(String virtualSystemName) {
		this.virtualSystemName = virtualSystemName;
	}

	public Ovf getOvf() {
		return this.ovf;
	}

	public void setOvf(Ovf ovf) {
		this.ovf = ovf;
	}
	
	public List<Vm> getVms() {
		return this.vms;
	}

	public void setVms(List<Vm> vms) {
		this.vms = vms;
	}
	
}