package org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks;

import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.*;

import java.util.List;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.VepTemplate;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Vm;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.vep.VEPconnector;

import com.sun.jersey.api.client.UniformInterfaceException;



public class ApplicationStop implements Callable<JSONObject> {

	private String appID = null; 
	private String userUuid = null;
	private String ceeIdentifier= null;
	private static Logger logger = Logger.getLogger(ApplicationStop.class);
	
	public ApplicationStop(String uuid, String ceeId, String AppID){
		this.appID = AppID; 
		this.userUuid = uuid;
		this.ceeIdentifier = ceeId;
	}
 
	@Override
	public JSONObject call() throws Exception {
		/*
		 * TODO: Retrieve CeeId from SLAM-P
		 */
		JSONObject returnStatus = new JSONObject();
		
		try{			
			returnStatus = VEPconnector.getInstance().stopApplication(this.ceeIdentifier,this.appID, this.userUuid);	
		} catch(UniformInterfaceException e){
			
			returnStatus.put(RETURN_STATUS, e.getResponse().getClientResponseStatus().getStatusCode());
			e.printStackTrace();
			return returnStatus;
		}
			
		//returnStatus.put(RETURN_STATUS, 0);
		return returnStatus;
	}
	
}
