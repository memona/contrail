package org.ow2.contrail.provider.provisioningmanager.engine.checking;

import org.ow2.contrail.common.implementation.application.ApplicationDescriptor;
import org.ow2.contrail.common.implementation.ovf.OVFParser;

import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.SLA;

import static org.ow2.contrail.provider.provisioningmanager.engine.checking.ExternalizedOVFandSLAstrings.*;

public class ConsistencyCheck {
	
	private static ConsistencyCheck _instance;
	public static synchronized ConsistencyCheck getInstance() { 
		if (_instance == null) { 
			_instance = new ConsistencyCheck(); 
		} 
		return _instance; 
	}
	
	private ConsistencyCheck(){ }
	
	
	/** 
	 * Check OVF validity by parsing it
	 * */
	public ApplicationDescriptor checkOVF(String ovfDescr){
		
		ApplicationDescriptor ovf = null;
		
		try {
			ovf = OVFParser.ParseOVF(ovfDescr);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
		
		return ovf;
	}

	/** 
	 * Check OVF and SLA consistency
	 * */
	public boolean checkOVFSLAConsistency(ApplicationDescriptor ovf, SLA sla){
		
		STND[] keys = sla.getPropertyKeys();
        String OVFvalue = null;
        
        for(STND key : keys){
        	if(key.getValue().equalsIgnoreCase(OVF_ID_FIELD)){
        		 OVFvalue = sla.getPropertyValue(key).trim();
        	}
        }
        
        if(OVFvalue == null) return false;
        
		return OVFvalue.equals(ovf.getName().trim());
	}
}
