package org.ow2.contrail.provider.provisioningmanager.client;

import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERGROUPS;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERNAME;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERROLE;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.USERVID;
import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.XUSERNAME;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import com.sun.jersey.multipart.MultiPart;

/**
 * HttpsClient: test https connection to provisioning manager
 *
 * @author Marco Distefano
 */
public class HttpsClient {
    
    private static final String truststore_path = "src/main/resources/truststore_client";
    private static final String truststore_password = "contrail";
    private static final String keystore_path = "src/main/resources/keystore_client";
    private static final String keystore_password = "contrail";
    private static final String url = "https://146.48.81.255:8282/";
    public static String ovfFileName = "src/main/resources/ovf.ovf";
    private static HTTPSProperties prop;
    private static  ClientRequest.Builder requestBuilder;
    private static Client client;
    private static String userCertPath="./src/main/resources/cert.cer";
    private static String userCert;
    public static void main(String[] args) {
    	//enable this for ssl debug
    	//System.setProperty("javax.net.debug", "ssl"); 
    	userCert = serializeUserCertificate(getUserCertificateFromFile(userCertPath));
    	try{
            setConnection();
            String username="contrailuser";
            /***************** PROPAGATE USER ************
            
            propagateUser(username);
            
            
            /***************** SUBMIT ******************
            
            int ceeId = 9;
            
            JSONObject submitResult = submitRequest(createMultiPartSubmit(username,1,ceeId));
            
            int appIdSubmitted=submitResult.getInt("appId");
            
            System.out.println(appIdSubmitted);
            
    		/***************** DEPLOY *******************
    		String[] vsNames = new String[]{"ubu1","ubu2"};
            int[] vmNumber = new int[]{2,1};
            
            JSONObject deployResult = deployRequest(appIdSubmitted, vsNames, vmNumber);
           
            /****************** START *******************
            int appIdSubmitted=66;
            JSONObject startResult = startRequest(appIdSubmitted, username);
            
            
            /****************** STOP *******************/
            int appIdSubmitted=66;
            JSONObject stopResult = stopRequest(appIdSubmitted, username);
            
            /****************** DELETE ********************/
            //int appIdSubmitted=66;
            JSONObject deleteResult = deleteRequest(appIdSubmitted, username);
            /**************** MODIFY APP *****************
            String action ="DEL";
            String vmId=(String) ((JSONArray)startResult.get("vm_id")).getJSONArray(0).get(0);
            String vsName=(String) ((JSONArray)startResult.get("vm_id")).getJSONArray(0).get(1);

            JSONObject modifyAppResult = modifyAppRequest(appIdSubmitted, createModifyRequest(action,vsName,vmId));
            
            /**************** INFO APP *****************/
            
            //JSONObject appInfoResult = appInfoRequest(appIdSubmitted);
            
    	} catch (Exception e) {
            e.printStackTrace();
        }
    	
    }
    
    public static void propagateUser(String username){
    	JSONObject userProg = new JSONObject();
    	String uuid="";
		try
		{
			userProg.put("name", username);
		}catch (JSONException e){
			e.printStackTrace();
		} 
		UriBuilder uriBuilder = UriBuilder.fromUri(url+"user-mgmt/propagate");		
		
		requestBuilder.type("application/json");
        requestBuilder.header("X-Certificate", userCert);
        requestBuilder.accept("application/json");
        requestBuilder.entity(userProg);
        ClientRequest clientRequest = requestBuilder.build(uriBuilder.build(), "PUT");
        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
        ClientResponse response = client.handle(clientRequest);
        JSONObject result = response.getEntity(JSONObject.class);
        System.out.println("execute "+ response +"\nresponse body: " + result);
        
    }

    public static JSONObject submitRequest(MultiPart mp){
    	System.out.println("********************SUBMIT***********************");
        //POST REQUEST submit application
        UriBuilder uriBuilder = UriBuilder.fromUri(url+"application-mgmt/submit");
        
        requestBuilder.type("multipart/mixed");
        requestBuilder.header("X-Certificate", userCert);
        requestBuilder.accept("application/json");
        requestBuilder.entity(mp);
        ClientRequest clientRequest = requestBuilder.build(uriBuilder.build(), "PUT");
        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
        ClientResponse response = client.handle(clientRequest);
        //System.out.println("request executed");
        JSONObject result = response.getEntity(JSONObject.class);
        System.out.println("execute "+ response +"  (ceeid: " + mp.getBodyParts().get(4).getEntity() +")\nresponse body: " + result);
        return result;
    }
    
    public static MultiPart createMultiPartSubmit(String username, int userId, int ceeId){
    	String ovfFile = OVFToString(ovfFileName);
        MultiPart mp = new MultiPart();
        mp.bodyPart("ovfName", MediaType.TEXT_PLAIN_TYPE)
        .bodyPart(ovfFile, MediaType.TEXT_XML_TYPE)
        .bodyPart(username, MediaType.TEXT_PLAIN_TYPE)
        .bodyPart(Integer.toString(userId), MediaType.TEXT_PLAIN_TYPE)
        .bodyPart(Integer.toString(ceeId), MediaType.TEXT_PLAIN_TYPE);
        return mp;
    }
    
    
    public static JSONObject deployRequest(int appIdSubmitted, String[] vsNames, int[] vmNumber){
    	System.out.println("********************DEPLOY***********************");
        JSONObject deployRequest = createDeployRequest(vsNames, vmNumber);
        
        UriBuilder uriBuilder = UriBuilder.fromUri(url+"application-mgmt/deploy/"+appIdSubmitted);
        System.out.println(url+"application-mgmt/deploy/"+appIdSubmitted);
        requestBuilder.type("application/json");
        requestBuilder.header("X-Certificate", userCert);
        requestBuilder.accept("application/json");
        requestBuilder.entity(deployRequest);
        ClientRequest clientRequest = requestBuilder.build(uriBuilder.build(), "PUT");
        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
        ClientResponse response = client.handle(clientRequest);
        JSONObject result = response.getEntity(JSONObject.class);
        System.out.println("execute "+ response + "\n with request body =" + deployRequest +"\nresponse body: "+ result);
        System.out.println();
        
        return result;
    }
    
    public static JSONObject createDeployRequest(String[] vs, int[] vmNumber){
    	JSONObject deployRequest = new JSONObject();
    	try {
			deployRequest.put("name", "contrailuser");

			// for each appliance add its the name in the request to provisioning manager.
			JSONArray jsonArray = new JSONArray();
			for(int i=0; i<vs.length;i++){
				jsonArray.put(vs[i]);
			}
			JSONArray jsonArrayNumVm = new JSONArray();
			for(int i=0; i<vmNumber.length;i++){
				jsonArrayNumVm.put(vmNumber[i]+"");
			}
			JSONArray jsonVinIds = new JSONArray();
			JSONArray ibisServer = new JSONArray();
			JSONArray ibisPool = new JSONArray();
			JSONArray vinAgents = new JSONArray();
			JSONArray gafsVolumes = new JSONArray();
			int indexContext =0;
			for(int i=0; i<vmNumber.length;i++){
				for(int j=0; j < vmNumber[i];j++){
					jsonVinIds.put(indexContext);
					ibisServer.put("vin-ibis"+indexContext);
					ibisPool.put("vin-pool"+indexContext);
					vinAgents.put("vin-agent"+indexContext);
					gafsVolumes.put("gafsVolume"+indexContext);
					indexContext++;
				}
			}
			

			deployRequest.put("appliances", jsonArray);
			deployRequest.put("vmNumberForAppliance", jsonArrayNumVm);
			deployRequest.put("vin_ids", jsonVinIds);
			deployRequest.put("vin_ibis_servers", ibisServer);
			deployRequest.put("vin_pool_names", ibisPool);
			deployRequest.put("vin_agent_context", vinAgents);
			deployRequest.put("gafs_volume_urls", gafsVolumes);
			
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		System.out.println(deployRequest);
    	return deployRequest;
    	
    }
    
    public static JSONObject startRequest(int appIdSubmitted, String username){
    	 System.out.println("********************START APP***********************");
         UriBuilder uriBuilder = UriBuilder.fromUri(url+"application-mgmt/start/"+appIdSubmitted);
         System.out.println(url+"application-mgmt/start/"+appIdSubmitted);
         requestBuilder.type("application/json");
         requestBuilder.accept("application/json");
         requestBuilder.header("X-Certificate", userCert);
         requestBuilder.entity(createSimplyRequest(username));
         ClientRequest clientRequest = requestBuilder.build(uriBuilder.build(), "POST");
         clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
         ClientResponse response = client.handle(clientRequest);
         JSONObject result = response.getEntity(JSONObject.class);
         System.out.println("response "+ response +"\nresponse body: "+ result);
         System.out.println();
         
         return result;
    }
    
    public static JSONObject stopRequest(int appIdSubmitted, String username){
   	 System.out.println("********************START APP***********************");
        UriBuilder uriBuilder = UriBuilder.fromUri(url+"application-mgmt/stop/"+appIdSubmitted);
        System.out.println(url+"application-mgmt/stop/"+appIdSubmitted);
        requestBuilder.type("application/json");
        requestBuilder.accept("application/json");
        requestBuilder.header("X-Certificate", userCert);
        requestBuilder.entity(createSimplyRequest(username));
        ClientRequest clientRequest = requestBuilder.build(uriBuilder.build(), "POST");
        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
        ClientResponse response = client.handle(clientRequest);
        JSONObject result = response.getEntity(JSONObject.class);
        System.out.println("response "+ response +"\nresponse body: "+ result);
        System.out.println();
        
        return result;
   }
    
    public static JSONObject deleteRequest(int appIdSubmitted, String username){
      	 System.out.println("********************START APP***********************");
           UriBuilder uriBuilder = UriBuilder.fromUri(url+"application-mgmt/delete/"+appIdSubmitted);
           System.out.println(url+"application-mgmt/delete/"+appIdSubmitted);
           requestBuilder.type("application/json");
           requestBuilder.accept("application/json");
           requestBuilder.header("X-Certificate", userCert);
           requestBuilder.entity(createSimplyRequest(username));
           ClientRequest clientRequest = requestBuilder.build(uriBuilder.build(), "POST");
           clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
           ClientResponse response = client.handle(clientRequest);
           JSONObject result = response.getEntity(JSONObject.class);
           System.out.println("response "+ response +"\nresponse body: "+ result);
           System.out.println();
           
           return result;
      }
    public static JSONObject createSimplyRequest(String username){
    	
    	JSONObject simplyRequest = new JSONObject();
        try {
        	simplyRequest.put("name",username);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	return simplyRequest;
    }
    
    public static JSONObject modifyAppRequest(int appIdSubmitted, JSONObject modifyRequest){
    	System.out.println("********************DELETE VM***********************");
        UriBuilder uriBuilder = UriBuilder.fromUri(url+"application-mgmt/manage/"+appIdSubmitted);
        System.out.println(url+"application-mgmt/manage/"+appIdSubmitted);
        requestBuilder.type("application/json");
        requestBuilder.accept("application/json");
        requestBuilder.header("X-Certificate", userCert);
        requestBuilder.entity(modifyRequest);
        ClientRequest clientRequest = requestBuilder.build(uriBuilder.build(), "POST");
        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
        ClientResponse response = client.handle(clientRequest);
        JSONObject result = response.getEntity(JSONObject.class);
        System.out.println("execute "+ response + " with " + modifyRequest +"\nresponse body: "+ result);
        System.out.println();
        
        return result;
    }
    
    public static JSONObject createModifyRequest(String action, String vsName, String vmId){
    	
    	JSONObject modifyRequest = new JSONObject();
        try {
			modifyRequest.put("action",action);
			modifyRequest.put("VS_ID",vsName);
	        modifyRequest.put("VM_ID",vmId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	return modifyRequest;
    }
    
    public static JSONObject appInfoRequest(int appIdSubmitted){
    	System.out.println("********************GET APP INFO***********************");
        UriBuilder uriBuilder = UriBuilder.fromUri(url+"application-mgmt/info/"+appIdSubmitted);
        System.out.println(url+"application-mgmt/info/"+appIdSubmitted);
        requestBuilder.accept("application/json");
        ClientRequest clientRequest = requestBuilder.build(uriBuilder.build(),"GET");
        clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);
        ClientResponse response = client.handle(clientRequest);
        System.out.println("request executed");
        JSONObject result = response.getEntity(JSONObject.class);
        System.out.println("response GET to " + uriBuilder.build() + " =\n"+ response +"\n"+ result);
        System.out.println();
        
        return result;
    }
    
    public static String OVFToString(String OVFfile){
		
		StringBuffer sb = new StringBuffer();
		try{
			File file = new File(OVFfile);
			if(!file.exists()){
				throw new Exception("OVF does not exist.");
			}
			BufferedReader reader = new BufferedReader(new FileReader(file));
			sb = new StringBuffer();
			while(reader.ready()){
				sb.append(reader.readLine());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		String ovfFile = sb.toString();	
		return ovfFile;
	}
    
    protected static X509Certificate getUserCertificateFromFile(String certificateFile){
		X509Certificate cert = null;
		try {
			InputStream inStream = new FileInputStream(certificateFile);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			cert = (X509Certificate)cf.generateCertificate(inStream);
			inStream.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return cert;
	}
	
	protected static String serializeUserCertificate(X509Certificate cert){
		//return new String(cert.getEncoded());
		//return Base64.encodeBase64(cert.getEncoded());
		String serializedCert = "";
		try {
			serializedCert = new String(Base64.encodeBase64(cert.getEncoded()));
		} catch (CertificateEncodingException e) {
			e.printStackTrace();
		}
		
		return serializedCert;
	}

	public static void setConnection(){
		
		try {
			//setting client truststore and keystore 
	        TrustManager mytm[] = null;
	        KeyManager mykm[] = null;
	
	        try {
	            mytm = new TrustManager[]{new MyX509TrustManager(truststore_path, truststore_password.toCharArray())};
	            mykm = new KeyManager[]{new MyX509KeyManager(keystore_path, keystore_password.toCharArray())};
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	
	        //setting SSL context
	        SSLContext context = null;
	
	        try {
	            context = SSLContext.getInstance("SSL");
	            context.init(mykm, mytm, null);
	        } catch (NoSuchAlgorithmException nae) {
	            nae.printStackTrace();
	        } catch (KeyManagementException kme) {
	            kme.printStackTrace();
	        }
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
				@Override
				public boolean verify(String urlHostName, SSLSession session) {
					System.out.println("Warning: URL Host: " + urlHostName + " vs. "
		                    + session.getPeerHost());
					return true;
				}
	        };
	        
	        prop = new HTTPSProperties(allHostsValid, context);
	        requestBuilder = ClientRequest.create();
	        client = Client.create();
	        
		} catch (Exception e) {
	        e.printStackTrace();
	    }
	}	
    
    /**
     * Taken from http://java.sun.com/javase/6/docs/technotes/guides/security/jsse/JSSERefGuide.html
     *
     */
    static class MyX509TrustManager implements X509TrustManager {

         /*
          * The default PKIX X509TrustManager9.  We'll delegate
          * decisions to it, and fall back to the logic in this class if the
          * default X509TrustManager doesn't trust it.
          */
         X509TrustManager pkixTrustManager;

         MyX509TrustManager(String trustStore, char[] password) throws Exception {
             this(new File(trustStore), password);
         }

         MyX509TrustManager(File trustStore, char[] password) throws Exception {
             
        	 // create a "default" JSSE X509TrustManager.
             KeyStore ks = KeyStore.getInstance("JKS");
             ks.load(new FileInputStream(trustStore), password);
             TrustManagerFactory tmf = TrustManagerFactory.getInstance("PKIX");
             tmf.init(ks);
             return;
         }


         public X509Certificate[] getAcceptedIssuers() { return null; }
         public void checkClientTrusted(X509Certificate[] certs, String authType) {}
         public void checkServerTrusted(X509Certificate[] certs, String authType) {}

    }

    /**
     * Inspired from http://java.sun.com/javase/6/docs/technotes/guides/security/jsse/JSSERefGuide.html
     *
     */
    static class MyX509KeyManager implements X509KeyManager {

         /*
          * The default PKIX X509KeyManager.  We'll delegate
          * decisions to it, and fall back to the logic in this class if the
          * default X509KeyManager doesn't trust it.
          */
         X509KeyManager pkixKeyManager;

         MyX509KeyManager(String keyStore, char[] password) throws Exception {
             this(new File(keyStore), password);
         }

         MyX509KeyManager(File keyStore, char[] password) throws Exception {
             // create a "default" JSSE X509KeyManager.

             KeyStore ks = KeyStore.getInstance("JKS");
             ks.load(new FileInputStream(keyStore), password);

             KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509", "SunJSSE");
             kmf.init(ks, password);

             KeyManager kms[] = kmf.getKeyManagers();

             /*
              * Iterate over the returned keymanagers, look
              * for an instance of X509KeyManager.  If found,
              * use that as our "default" key manager.
              */
             for (int i = 0; i < kms.length; i++) {
                 if (kms[i] instanceof X509KeyManager) {
                     pkixKeyManager = (X509KeyManager) kms[i];
                     return;
                 }
             }

             /*
              * Find some other way to initialize, or else we have to fail the
              * constructor.
              */
             throw new Exception("Couldn't initialize");
         }
         
        public PrivateKey getPrivateKey(String arg0) {
            return pkixKeyManager.getPrivateKey(arg0);
        }

        public X509Certificate[] getCertificateChain(String arg0) {
            return pkixKeyManager.getCertificateChain(arg0);
        }

        public String[] getClientAliases(String arg0, Principal[] arg1) {
            return pkixKeyManager.getClientAliases(arg0, arg1);
        }

        public String chooseClientAlias(String[] arg0, Principal[] arg1, Socket arg2) {
            return pkixKeyManager.chooseClientAlias(arg0, arg1, arg2);
        }

        public String[] getServerAliases(String arg0, Principal[] arg1) {
            return pkixKeyManager.getServerAliases(arg0, arg1);
        }

        public String chooseServerAlias(String arg0, Principal[] arg1, Socket arg2) {
            return pkixKeyManager.chooseServerAlias(arg0, arg1, arg2);
        }
    }

}