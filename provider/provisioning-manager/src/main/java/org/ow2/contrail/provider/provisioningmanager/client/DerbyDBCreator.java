package org.ow2.contrail.provider.provisioningmanager.client;

import java.sql.Connection;
import java.sql.DriverManager;


public class DerbyDBCreator {

	
	public static void main (String[] args){
		
		
		 Connection con = null;
		    try {
		      Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
		      con = DriverManager.getConnection("jdbc:derby:databases/appLogDB;create=true");

		// Creating database tables
		      java.sql.Statement sta = con.createStatement(); 
		      sta.executeUpdate("CREATE TABLE OVFs (id BIGINT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) primary key, ovf_xml_descr clob, ovf_vep_id int, status varchar(30))");
		      sta.executeUpdate("CREATE TABLE PM_Users (username varchar(60) primary key, SLA_user_id int)");
		      sta.executeUpdate("CREATE TABLE Applications (id BIGINT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) primary key, status varchar(30), ovf_link BIGINT references OVFs(id), user_link varchar(60) references PM_Users(username))");
		      sta.executeUpdate("CREATE TABLE VEP_Templates (id BIGINT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) primary key, vep_assigned_id BIGINT, virtual_system_name varchar(60), ovf_link BIGINT references OVFs(id), status varchar(30))");
		      sta.executeUpdate("CREATE TABLE VMs (id BIGINT GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1) primary key, status varchar(30), vm_vep_id BIGINT, vm_vin_id BIGINT, vep_template_link BIGINT references VEP_Templates(id))");
		      sta.executeUpdate("CREATE TABLE SLAs(id varchar(60) primary key, SLA_xml_descr clob, user_link varchar(60) references PM_Users(username))");
		      System.out.println("Tables created.");
		      sta.close();        

		      con.close();        
		    } catch (Exception e) {
		      System.err.println("Exception: "+e.getMessage());
		    }
		
	}
	
}
