package org.ow2.contrail.provider.provisioningmanager.engine.controller;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.ow2.contrail.provider.provisioningmanager.engine.monitoring.LocalProviderWatcher;

public class ProvisioningManagerController {
	
	private LocalProviderWatcher lpw = null;
	private static ProvisioningManagerController _instance = null;
	private ExecutorService executors = null;
	//private long TIME_SLOT = 3000;
	
	public static synchronized ProvisioningManagerController getInstance() { 
		if (_instance == null) { 
			_instance = new ProvisioningManagerController(); 
		} 
		return _instance; 
	}
	
	private ProvisioningManagerController(){
		
		//boolean lpwStarted = false;
		
		//while(!lpwStarted){
			try {
				
				lpw = new LocalProviderWatcher();
				lpw.init();
		//		lpwStarted = true;
			
			} catch (Exception e) {	
		//		try {
					System.err.println("Monitoring not-online, caused by: "+e.getLocalizedMessage());
					System.err.println("The provisioning manager will run without any monitoring information,");
					
					//System.exit(1);
		//			System.err.println("Trying again in "+TIME_SLOT+" msec.");
		//			Thread.sleep(TIME_SLOT);
		//		} catch (InterruptedException e1) {
		//			e1.printStackTrace();
		//		}
			}
		//}
		executors =  Executors.newSingleThreadExecutor();	
	}
	
	public <T> Future<T> exec(Callable<T> task){
		
		return executors.submit(task);
		
	}
	
}
