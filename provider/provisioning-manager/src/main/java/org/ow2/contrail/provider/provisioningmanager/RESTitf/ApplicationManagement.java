package org.ow2.contrail.provider.provisioningmanager.RESTitf;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Application;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.ProvisioningManagerController;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationDelete;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationDeployment;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationSubmission;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationStart;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationChanges;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationStop;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationsList;

import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.MultiPart;

import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.*;

import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationInformation;
import org.ow2.contrail.provider.provisioningmanager.utils.CertificateUtils;

import static org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ExternalizedReturnJSONstrings.*;

/** 
 * CHANGES: re-implemented all interface to comunicate with VEP 2.0 @author: Marco Distefano
 */

@Path("/"+APP_MGMT_PATH)
public class ApplicationManagement {
	
	private static Logger logger = Logger.getLogger(ApplicationManagement.class);
	
	
	@PUT
	@Path(SUBMIT_PATH)
	@Consumes("multipart/mixed")
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject submitApp(MultiPart inputBody, @Context HttpHeaders headers){
		String loggerTag = "SubmitApp: ";
		logger.info(loggerTag + "Received PUT application-mgmt/submit");
		String uuid = "";
		String certString = null;
		try{
			certString = headers.getRequestHeader("X-Certificate").get(0);
		} catch (Exception e){
				logger.warn(loggerTag + "UserCertificate exception. No Certificate in the request header");
		}
		uuid=CertificateUtils.retrieveUserUuidFromCertificate(certString);
		
		JSONObject ret = new JSONObject();
		JSONObject appIDinJSON = null;
		List<BodyPart> bodyParts = null;
		String AppName = null;
		String OVFdescr = null;
		String User=null;
		Integer SLAUser= null;
		Integer ceeId= null;
		
		try {
			
			bodyParts = inputBody.getBodyParts();
			// Application Name
			if(bodyParts.get(0).getMediaType().isCompatible(MediaType.TEXT_PLAIN_TYPE))	
				AppName = bodyParts.get(0).getEntityAs(String.class);	
			else{
				try { ret.put(RETURN_STATUS, "415"); ret.put(APP_ID, "-1");return ret;} 
				catch (JSONException e) {	e.printStackTrace();}
			}
			
			// Application OVF
			if(bodyParts.get(1).getMediaType().isCompatible(MediaType.TEXT_XML_TYPE))	
				OVFdescr = bodyParts.get(1).getEntityAs(String.class);	
			else{
				try { ret.put(RETURN_STATUS, "415"); ret.put(APP_ID, "-1");return ret;} 
				catch (JSONException e) {	e.printStackTrace();}
			}
			// Username@federation
			if(bodyParts.get(2).getMediaType().isCompatible(MediaType.TEXT_PLAIN_TYPE))	
				User = bodyParts.get(2).getEntityAs(String.class);	
			else{
				try { ret.put(RETURN_STATUS, "415"); ret.put(APP_ID, "-1");return ret;} 
				catch (JSONException e) {	e.printStackTrace();}
			}
			// SLA@SOI UserID
			String SLAUserString;
			if(bodyParts.get(3).getMediaType().isCompatible(MediaType.TEXT_PLAIN_TYPE))	{
				SLAUserString = bodyParts.get(3).getEntityAs(String.class);
				SLAUser = Integer.parseInt(SLAUserString);
			}
			else{
				try { ret.put(RETURN_STATUS, "415"); ret.put(APP_ID, "-1");return ret;} 
				catch (JSONException e) {	e.printStackTrace();}
			}
			String ceeIdString;
			if(bodyParts.get(4).getMediaType().isCompatible(MediaType.TEXT_PLAIN_TYPE))	{
				ceeIdString = bodyParts.get(4).getEntityAs(String.class);
				ceeId = Integer.parseInt(ceeIdString);
			}
			else{
				try { ret.put(RETURN_STATUS, "415"); ret.put(APP_ID, "-1");return ret;} 
				catch (JSONException e) {	e.printStackTrace();}
			}
			
		} catch (Exception ex) {
			logger.error("Error: "+ ex.toString());
			
			try { ret.put(RETURN_STATUS, "415"); return ret;} 
			catch (JSONException e) {	e.printStackTrace();}
		}
		logger.info(loggerTag + "Received ceeId: " + ceeId);
		ApplicationSubmission app =  new ApplicationSubmission(AppName, OVFdescr, User, SLAUser, ceeId, uuid);
		Future<JSONObject> retValue = ProvisioningManagerController.getInstance().exec(app);
		
		try {
			ret = retValue.get();
		} catch (InterruptedException ex) {
			try { ret.put(RETURN_STATUS, "500"); return ret;} 
			catch (JSONException e) {	e.printStackTrace();}
		} catch (ExecutionException ex) {
			try { ret.put(RETURN_STATUS, "500"); return ret;} 
			catch (JSONException e) {   e.printStackTrace();}
		}
		try { ret.put(RETURN_STATUS, "200");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return ret;
	}
	
	@PUT
	@Path(DEPLOY_PATH+"/{appId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public synchronized JSONObject deployApp(@PathParam("appId") String appId, JSONObject inputBody, @Context HttpHeaders headers){
		String loggerTag = "DeployApp: ";
		String uuid = "";
		logger.info("Received PUT application-mgmt/deploy/"+appId);
		String certString = null;
		try{
			certString = headers.getRequestHeader("X-Certificate").get(0);
		} catch (Exception e){
				logger.warn(loggerTag + "UserCertificate exception. No Certificate in the request header");
		}
		uuid=CertificateUtils.retrieveUserUuidFromCertificate(certString);
		
		JSONObject ret = new JSONObject();
		JSONArray appliances = new JSONArray();
		JSONArray numVms = new JSONArray();
		JSONArray VinIDs = new JSONArray();
		JSONArray VinIbisServer = new JSONArray();
		JSONArray VinPoolName = new JSONArray();
		JSONArray VinAgentContext = new JSONArray();
		JSONArray GafsVolumeUrls = new JSONArray();
		String username = null;
		try {
			username = inputBody.getString(USERNAME);
		} catch (JSONException e1) {
			try { ret.put(RETURN_STATUS, "415"); return ret;} 
			catch (JSONException e) {	e.printStackTrace();}
		}
		try {
			appliances = inputBody.getJSONArray("appliances");
		} catch (JSONException e1) {
			try { ret.put(RETURN_STATUS, "415"); return ret;} 
			catch (JSONException e) { e.printStackTrace(); }
		}
		try {
			numVms = inputBody.getJSONArray("vmNumberForAppliance");
		} catch (JSONException e1) {
			try { ret.put(RETURN_STATUS, "415"); return ret;} 
			catch (JSONException e) { e.printStackTrace(); }
		}
		try {
			VinIDs = inputBody.getJSONArray(VIN_MAPPINGS);
		} catch (JSONException e1) {
			VinIDs = null;
		}
		try {
			VinIbisServer = inputBody.getJSONArray("vin_ibis_servers");
		} catch (JSONException e1) {
			VinIbisServer = null;
		}
		try {
			VinPoolName = inputBody.getJSONArray("vin_pool_names");
		} catch (JSONException e1) {
			VinPoolName = null;
		}
		try {
			VinAgentContext = inputBody.getJSONArray("vin_agent_context");
		} catch (JSONException e1) {
			VinAgentContext = null;
		}
		try {
			GafsVolumeUrls = inputBody.getJSONArray("gafs_volume_urls");
		} catch (JSONException e1) {
			GafsVolumeUrls = null;
		}
		JSONObject deploymentDocument = new JSONObject();
		JSONArray vms = new JSONArray();
		ApplicationDeployment deployApp =  null;
		try {
			int vmIterationCount=0;
			
			for(int i=0; i<appliances.length();i++){
				for(int j=0; j< numVms.getInt(i);j++){
					JSONObject vm = new JSONObject();
					vm.put("name", appliances.get(i) + "-" + (j+1));
					vm.put("virtualSystem", new JSONObject().put("href", "#"+appliances.get(i)));
					JSONObject context = new JSONObject();
					
					try {
						if(!VinIDs.get(vmIterationCount).equals(""))	context.put("VIN_ID", VinIDs.get(vmIterationCount)+""); 
						else context.put("VIN_ID", "0"); 
						
						if(!VinIbisServer.get(vmIterationCount).equals("")) context.put("VIN_IBIS_SERVER", VinIbisServer.get(vmIterationCount));
						if(!VinPoolName.get(vmIterationCount).equals("")) context.put("VIN_POOL_NAME", VinPoolName.get(vmIterationCount));
						if(!VinAgentContext.get(vmIterationCount).equals("")) context.put("VIN_AGENT_CONTEXT", VinAgentContext.get(vmIterationCount));
						if(!GafsVolumeUrls.get(vmIterationCount).equals("")) context.put("GAFS_VOLUME_URL", GafsVolumeUrls.get(vmIterationCount));
						vm.put("contextualization", context);
					} catch (Exception e) {
						logger.error(loggerTag + "Error on creating Deployment Document");
						e.printStackTrace();
					}
					vms.put(vm);
					vmIterationCount++;
				}
				
			}
		} catch (JSONException e) {
			logger.error(loggerTag + "Malformed Deploy Body Request \n"+ e.getMessage());
			try { ret.put(RETURN_STATUS,"415");}
			catch (JSONException e1) { e1.printStackTrace();}
			return ret;
		}
		try {
			deploymentDocument.put("VMs", vms);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		
		String ceeIdentifier = null;
		Application appl = null;
		try {
			appl = DBconnector.getInstance().getApplicationEntityByVepId(appId); 
			ceeIdentifier = appl.getCeeId();
		}catch (javax.persistence.NoResultException e) {
			logger.error(loggerTag +"Application with " + appId + "not submitted");
			try { ret.put(RETURN_STATUS,"404");}
			catch (JSONException e1) { e1.printStackTrace();}
			return ret;
		}
		if(!username.equals(appl.getPmUser().getUsername())){
			logger.error(loggerTag + "User not authorized to deploy app " + appId);
			try { ret.put(RETURN_STATUS,"401");}
			catch (JSONException e1) { e1.printStackTrace();}
			return ret;
		}
			
		
		
		try {			
			deployApp = new ApplicationDeployment(ceeIdentifier, appId, uuid, deploymentDocument);
			Future<JSONObject> retValue = ProvisioningManagerController.getInstance().exec(deployApp); 
			ret = retValue.get();
			if(ret.get(RETURN_STATUS).equals("200")){
				
			}
			try { ret.put(RETURN_STATUS,"200");}
			catch (JSONException e1) { e1.printStackTrace();}
		} catch (InterruptedException e) {
			try { ret.put(RETURN_STATUS,"500");}
			catch (JSONException e1) { e1.printStackTrace();}
		} catch (ExecutionException e) {
			try { ret.put(RETURN_STATUS,"500");}
			catch (JSONException e1) { e1.printStackTrace();}
		}catch (JSONException e) {
			try { ret.put(RETURN_STATUS,"500");}
			catch (JSONException e1) { e1.printStackTrace();}
		}
		return ret;
	}
	
	@POST
	@Path(START_PATH+"/{appId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject startApp(@PathParam("appId") String appId, JSONObject inputBody, @Context HttpHeaders headers) {
		String loggerTag = "StartApp: ";
		logger.info(loggerTag+ "Received POST application-mgmt/start/"+appId);
		String uuid = "";
		String certString = null;
		try{
			certString = headers.getRequestHeader("X-Certificate").get(0);
		} catch (Exception e){
				logger.warn(loggerTag + "UserCertificate exception. No Certificate in the request header");
		}
		uuid=CertificateUtils.retrieveUserUuidFromCertificate(certString);
		
		//FIXME: failure and error management to fix
		JSONObject ret = new JSONObject();
		String username = null;
		String ceeIdentifier = null;
		Application appl = null;
		try {
			username = inputBody.getString(USERNAME);
		} catch (JSONException e1) {
			try { ret.put(RETURN_STATUS, "415"); return ret;} 
			catch (JSONException e) {	e.printStackTrace();}
		}
		try {
			appl = DBconnector.getInstance().getApplicationEntityByVepId(appId); 
			ceeIdentifier = appl.getCeeId();
		}catch (javax.persistence.NoResultException e) {
			logger.error(loggerTag + "Application with " + appId + "not submitted");
			try { ret.put(RETURN_STATUS,"404");}
			catch (JSONException e1) { e1.printStackTrace();}
			return ret;
		}
		if(!username.equals(appl.getPmUser().getUsername())){
			logger.error(loggerTag + "User not authorized to start app " + appId);
			try { ret.put(RETURN_STATUS,"401");}
			catch (JSONException e1) { e1.printStackTrace();}
			return ret;
		}
		ApplicationStart appStart =  null;	
		appStart = new ApplicationStart(uuid, ceeIdentifier, appId);
		
		Future<JSONObject> retValue = ProvisioningManagerController.getInstance().exec(appStart); 
		
		try {
			return retValue.get();

		} catch (InterruptedException e) {
			e.printStackTrace();
			try { ret.put(RETURN_STATUS, "500"); return ret;} 
			catch (JSONException e1) {	e1.printStackTrace();}
		} catch (ExecutionException e) {
			e.printStackTrace();
			try { ret.put(RETURN_STATUS, "500"); return ret;} 
			catch (JSONException e1) {	e1.printStackTrace();}
		}
		
		return ret;
	}
	
	@POST
	@Path(STOP_PATH+"/{appId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject stopApp(@PathParam("appId") String appId, JSONObject inputBody, @Context HttpHeaders headers) {
		String loggerTag = "StopApp: ";
		logger.info(loggerTag+ "Received POST application-mgmt/stop/"+appId);
		String uuid = "";
		String certString = null;
		try{
			certString = headers.getRequestHeader("X-Certificate").get(0);
		} catch (Exception e){
				logger.warn(loggerTag + "UserCertificate exception. No Certificate in the request header");
		}
		uuid=CertificateUtils.retrieveUserUuidFromCertificate(certString);
		
		JSONObject ret = new JSONObject();
		String username = null;
		String ceeIdentifier = null;
		Application appl = null;
		try {
			username = inputBody.getString(USERNAME);
		} catch (JSONException e1) {
			try { ret.put(RETURN_STATUS, "415"); return ret;} 
			catch (JSONException e) {	e.printStackTrace();}
		}
		
		try {
			appl = DBconnector.getInstance().getApplicationEntityByVepId(appId); 
			ceeIdentifier = appl.getCeeId();
		}catch (javax.persistence.NoResultException e) {
			logger.info(loggerTag + "Application with " + appId + "not submitted");
			try { ret.put(RETURN_STATUS,"404");}
			catch (JSONException e1) { e1.printStackTrace();}
			return ret;
		}
		if(!username.equals(appl.getPmUser().getUsername())){
			logger.info(loggerTag + "User not authorized to stop app " + appId);
			try { ret.put(RETURN_STATUS,"401");}
			catch (JSONException e1) { e1.printStackTrace();}
			return ret;
		}
		
		ApplicationStop appStop =  new ApplicationStop(uuid, ceeIdentifier, appId);
		Future<JSONObject> retValue = ProvisioningManagerController.getInstance().exec(appStop);
		try {
			return retValue.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}	
		
		return ret;
	}
	
	@POST
	@Path(DELETE_PATH+"/{appId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject deleteApp(@PathParam("appId") String appId, JSONObject inputBody, @Context HttpHeaders headers) {
		String loggerTag = "DeleteApp: ";
		logger.info(loggerTag+ "Received POST application-mgmt/delete/"+appId);
		String uuid = "";
		String certString = null;
		try{
			certString = headers.getRequestHeader("X-Certificate").get(0);
		} catch (Exception e){
				logger.warn(loggerTag + "UserCertificate exception. No Certificate in the request header");
		}
		uuid=CertificateUtils.retrieveUserUuidFromCertificate(certString);
		
		JSONObject ret = new JSONObject();
		String username = null;
		String ceeIdentifier = null;
		Application appl = null;
		try {
			username = inputBody.getString(USERNAME);
		} catch (JSONException e1) {
			try { ret.put(RETURN_STATUS, "415"); return ret;} 
			catch (JSONException e) {	e.printStackTrace();}
		}
		
		try {
			appl = DBconnector.getInstance().getApplicationEntityByVepId(appId); 
			ceeIdentifier = appl.getCeeId();
		}catch (javax.persistence.NoResultException e) {
			logger.info(loggerTag + "Application with " + appId + "not submitted");
			try { ret.put(RETURN_STATUS,"404");}
			catch (JSONException e1) { e1.printStackTrace();}
			return ret;
		}
		if(!username.equals(appl.getPmUser().getUsername())){
			logger.info(loggerTag + "User not authorized to stop app " + appId);
			try { ret.put(RETURN_STATUS,"401");}
			catch (JSONException e1) { e1.printStackTrace();}
			return ret;
		}
		
		ApplicationDelete appStop =  new ApplicationDelete(uuid, ceeIdentifier, appId);
		Future<JSONObject> retValue = ProvisioningManagerController.getInstance().exec(appStop);
		try {
			return retValue.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}	
		
		return ret;
	}
	@GET
	@Path(INFO_PATH+"/{appId}")
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getAppInfo(@PathParam("appId") String appId, @Context HttpHeaders headers) {
		String loggerTag = "GetAppInfo: ";
		logger.info(loggerTag + "Received GET application-mgmt/info/"+appId);
		String uuid = "";
		String certString = null;
		try{
			certString = headers.getRequestHeader("X-Certificate").get(0);
		} catch (Exception e){
				logger.warn(loggerTag + "UserCertificate exception. No Certificate in the request header");
		}
		uuid=CertificateUtils.retrieveUserUuidFromCertificate(certString);
		
		JSONObject ret = new JSONObject();
		String ceeIdentifier = null;
		Application appl = null;
		try {
			appl = DBconnector.getInstance().getApplicationEntityByVepId(appId); 
			ceeIdentifier = appl.getCeeId();
		}catch (javax.persistence.NoResultException e) {
			logger.error(loggerTag+"Application with " + appId + "not submitted");
			try { ret.put(RETURN_STATUS,"404");}
			catch (JSONException e1) { e1.printStackTrace();}
			return ret;
		}
		ApplicationInformation appInfo = new ApplicationInformation(ceeIdentifier, appId, uuid);
		Future<JSONObject> applicationFuture = ProvisioningManagerController.getInstance().exec(appInfo);
		
		try {
			ret = applicationFuture.get();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			e1.printStackTrace();
		}		
		
		
		return ret;
	}
	
	
	@POST
	@Path(MANAGE_PATH+"/{appId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject modifyApp(@PathParam("appId") String appId, JSONObject inputBody, @Context HttpHeaders headers) {
		String loggerTag = "ModifyApp: ";
		JSONObject ret = new JSONObject();
		logger.info(loggerTag + "Received POST application-mgmt/manage/"+appId);
		String uuid = "";
		String certString = null;
		try{
			certString = headers.getRequestHeader("X-Certificate").get(0);
		} catch (Exception e){
				logger.warn(loggerTag + "UserCertificate exception. No Certificate in the request header");
		}
		uuid=CertificateUtils.retrieveUserUuidFromCertificate(certString);
		
		ApplicationChanges modify = new ApplicationChanges(appId, inputBody,uuid);
		Future<JSONObject> retFuture = ProvisioningManagerController.getInstance().exec(modify);
		String retValue = null;
		
		try {
			return retFuture.get();
		} catch (InterruptedException e) {
			retValue="500";
			e.printStackTrace();
		} catch (ExecutionException e) {
			retValue="500";
			e.printStackTrace();
		}

		try {
			ret.put(RETURN_STATUS, retValue);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	@GET
	@Path(INFO_PATH)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getAppList(@Context HttpHeaders headers) {
		String loggerTag = "GetAppsList: ";
		logger.info(loggerTag + "Received GET application-mgmt/info");
		String uuid = "";
		String certString = null;
		try{
			certString = headers.getRequestHeader("X-Certificate").get(0);
		} catch (Exception e){
				logger.warn(loggerTag + "UserCertificate exception. No Certificate in the request header");
		}
		uuid=CertificateUtils.retrieveUserUuidFromCertificate(certString);
		ApplicationsList appInfo = new ApplicationsList(uuid);
		Future<JSONObject> applicationFuture = ProvisioningManagerController.getInstance().exec(appInfo);
		JSONObject applicationsList = null;
		
		try {
			applicationsList = applicationFuture.get();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			e1.printStackTrace();
		}		
		
		return applicationsList;
	}
}
