/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.provisioningmanager.RESTitf;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.ProvisioningManagerController;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationChanges;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationInformation;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationStart;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationStop;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.ApplicationSubmission;

import static org.ow2.contrail.provider.provisioningmanager.RESTitf.ExternalizedRESTstrings.*;
import org.ow2.contrail.provider.provisioningmanager.engine.controller.tasks.VMInformation;

/**
 *
 * @author dazzi
 */
@Path("/"+VM_MGMT_PATH)
public class VMManagement {
	
	@PUT
	@Path(INFO_PATH+"/{vmId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getAppInfo(@PathParam("vmId") String vmId, JSONObject inputBody) {
        		
            
            JSONObject vmRetInfo = null;
         try {  
             VMInformation vmInfo = new VMInformation(Integer.parseInt(vmId), inputBody.getString(USERNAME));
            Future<JSONObject> vmFuture = ProvisioningManagerController.getInstance().exec(vmInfo);
        
                try {
                        vmRetInfo = vmFuture.get();
                } catch (InterruptedException e1) {
                        e1.printStackTrace();
                } catch (ExecutionException e1) {
                        e1.printStackTrace();
                }		
                
                
                
                
        } catch (JSONException ex) {
            Logger.getLogger(VMManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return vmRetInfo;
	}
	
}
