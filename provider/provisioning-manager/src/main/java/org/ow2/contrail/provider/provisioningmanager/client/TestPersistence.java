package org.ow2.contrail.provider.provisioningmanager.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.DBconnector;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Application;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Ovf;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.PmUser;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.Sla;
import org.ow2.contrail.provider.provisioningmanager.engine.connectors.db.dbentities.VepTemplate;


public class TestPersistence {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		DBconnector db = DBconnector.getInstance();
		Application item = new Application();
		
		Ovf OVF = new Ovf();
		PmUser user = new PmUser();
		VepTemplate template = new VepTemplate();
		
		List<Sla> SLAs =  new ArrayList<Sla>();
		List<VepTemplate> templates = new ArrayList<VepTemplate>();
		
		//template.setOVF_ID(OVF);
		template.setStatus("CREATED_TEMPLATE");
		Random r = new Random();
		
		//template.setTemplate_VEP_ID();
		template.setVepAssignedId(new Long(r.nextLong()));
		template.setVirtualSystemName("BelVS");
		
		db.storeObject(template);
		
		templates.add(template);
		
		OVF.setOvfVepId(1);
		OVF.setOvfXmlDescr("XML del OVF");
		OVF.setStatus("INITIALIZED_OVF");
		//OVF.setVEPTemplates(templates);
		
		db.storeObject(OVF);
		
		item.setOvf(OVF);
		
		Sla sla = new Sla();
		//sla.setSLAuserID(user);
		sla.setId(new Long(r.nextLong()).toString());
		//sla.setStatus("NOT_USED_SLA");
		
		db.storeObject(sla);
		
		SLAs.add(sla);
		
		//item.setSLAs(SLAs);
		
		String status = "OVERALL-STATUS";
		
		item.setStatus(status);
		
		user.setSlaUserId(new Integer(r.nextInt()));
		user.setUsername("pippo"+r.nextInt());
		
		db.storeObject(user);
		
		item.setPmUser(user);
		
		db.storeObject(item);
		
		
		
		
		Application itemBack = db.retrieveObject(item);
		
		System.out.println(item.getId()+" - "+itemBack.getId());
		
		for(Long l : db.getAllApplicationEntityIDs()){
		
			System.out.println(l);
		}
	}

}
