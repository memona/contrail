
package org.ow2.contrail.provider.provisioningmanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.ow2.contrail.common.implementation.application.ApplicationDescriptor;
import org.ow2.contrail.provider.provisioningmanager.engine.checking.ConsistencyCheck;
import org.ow2.contrail.provider.provisioningmanager.stubs.ProvisioningManagerSLARegistry;
import org.ow2.contrail.provider.provisioningmanager.utils.ProvisioningManagerProperties;
// import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;

import junit.framework.TestCase;


public class TestSLAandOVF extends TestCase {

	private String OVF_FILENAME			= "dsl-test-xlab.xml";
	private String SLA_ID 				= "SLASOI.xml";
	
	private String OVF_APP_NAME_TAG 	= "OVF-NAME";
	private String OVF_APP_NAME_VALUE 	= "Contrail_Test_Application";
	
	
	
    public TestSLAandOVF(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();

      
    }

    /*
    public void testSLARegistry() {
        
    	SLA[] slas = null;
    	String ovfString = null;
    	
    	try {
			slas = ProvisioningManagerSLARegistry.getInstance().getSLA(new UUID[]{new UUID(SLA_ID)});
		} catch (InvalidUUIDException e) {
			e.printStackTrace();
		}
    	
    	assertNotNull(slas);
    	assertNotNull(slas[0]);
    	
    	ovfString = slas[0].getPropertyValue(new STND(OVF_APP_NAME_TAG));
    	
    	assertNotNull(ovfString);
    	assertEquals(ovfString.trim(), OVF_APP_NAME_VALUE);
    
    }
     */
    
    public void testOVFParser() {
        
    	try {
    		
			File file = new File("src/main/resources/"+OVF_FILENAME);
			
			if(!file.exists()){
				file = new File(OVF_FILENAME);
			}
			
			if(!file.exists()){
				
				ProvisioningManagerProperties p = new ProvisioningManagerProperties();
				file = new File(p.getClass().getResource(OVF_FILENAME).toURI());
			}
			
			BufferedReader reader = new BufferedReader(new FileReader(file));
			StringBuffer sb = new StringBuffer();
			while(reader.ready()){
				sb.append(reader.readLine());
			}
    	
			ApplicationDescriptor ad = ConsistencyCheck.getInstance().checkOVF(sb.toString());
			
			assertEquals(ad.getName().trim(), OVF_APP_NAME_VALUE);
			
    	}catch(Exception e){
    		
    	}
    
    }
    
    public void testConsistency(){
    		

    	try {
    		
    		SLA[] slas = null;
			File file = new File("src/main/resources/"+OVF_FILENAME);
			
			if(!file.exists()){
				file = new File(OVF_FILENAME);
			}
			
			if(!file.exists()){
				ProvisioningManagerProperties p = new ProvisioningManagerProperties();
				file = new File(p.getClass().getResource(OVF_FILENAME).toURI());
			}
			
			BufferedReader reader = new BufferedReader(new FileReader(file));
			StringBuffer sb = new StringBuffer();
			while(reader.ready()){
				sb.append(reader.readLine());
			}
    	
			ApplicationDescriptor ad = ConsistencyCheck.getInstance().checkOVF(sb.toString());
			/*
	    	try {
				slas = ProvisioningManagerSLARegistry.getInstance().getSLA(new UUID[]{new UUID(SLA_ID)});
			} catch (InvalidUUIDException e) {
				e.printStackTrace();
			}
	    	*/
	    	boolean ret = ConsistencyCheck.getInstance().checkOVFSLAConsistency(ad, slas[0]);
			
	    	assertTrue(ret);
			
    	}catch(Exception e){
    		
    	}
    	
    }
    
}
