/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import org.ow2.contrail.common.ovf.opennebula.OpenNebulaOVFParser;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

/**
 *
 * @author piyush
 */
public class restServerAppControl extends ServerResource 
{
    private dbHandler db;
    private Logger logger;
    private String dbType;
    private String vepProperties;
    private String oneIp;
    private String onePort;
    private String oneVersion;
    private String oneAdmin;
    
    public restServerAppControl()
    {
        vepProperties = VEPHelperMethods.getPropertyFile();
        logger = Logger.getLogger("VEP.restAppControl");
        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        db = new dbHandler("restServerAppControl", dbType);
        oneIp = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
        onePort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
        oneVersion = VEPHelperMethods.getProperty("one.version", logger, vepProperties);
        oneAdmin = VEPHelperMethods.getProperty("one.user", logger, vepProperties);
    }
    
    @Post
    public Representation getResult(Representation entity) throws ResourceException
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        Representation response = null;
        String username = null;
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            SSLCertHandler.isCertValid(Cert);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true));
        
        if(username != null && username.trim().length() > 0)
        {
            String actionType = ((String) getRequest().getAttributes().get("type"));
            if(actionType.trim().length() > 0)
            {
                if(actionType.trim().equals("vm"))
                {
                    performVMAction(username, stringBuilder, entity);
                }
                else if(actionType.trim().equals("vmtemplate"))
                {
                    performTemplateAction(username, stringBuilder, entity);
                }
                else if(actionType.trim().equals("ovf"))
                {
                    performOVFAction(username, stringBuilder, entity);
                }
                else if(actionType.trim().equals("newovf"))
                {
                    performNewOVFAction(username, stringBuilder, entity);
                }
                else if(actionType.trim().equals("updateovf"))
                {
                    performUpdateOVFAction(username, stringBuilder, entity);
                }
                else
                {
                    stringBuilder.append("This action request is currently not supported :-(<br><br>");
                }
            }
            else
            {
                //error
                stringBuilder.append("Unrecognized action :-( I can not proceed.<br><br>");
            }
        }
        else
        {
            //reportError("NOUSER", stringBuilder);;
            stringBuilder.append("<hr><i>Your browser is probably not configured properly to use your certificate</i>. Please configure your browser properly and try again ...<br><br>");
        }
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        response = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return response;
    }
    
    void performVMAction(String username, StringBuilder out, Representation entity)
    {
        Form form = new Form(entity);
        int vmid = Integer.parseInt(form.getFirstValue("vmid"));
        String vmaction = form.getFirstValue("vmaction");
        out.append("<hr>Hello ").append(username.toUpperCase()).append("! Welcome to your application management interface.<br><br>");
        out.append("You requested action on VM ").append(vmid).append(", actiontype: ").append(vmaction).append("<br><br>");
        int uid = -1;
        String oneuser = "";
        String onepass = "";
        ResultSet rs;
        try
        {
            rs = db.query("select", "*", "user", "where username='" + username + "'");
            if(rs.next())
            {
                uid = rs.getInt("uid");
                oneuser = rs.getString("oneuser");
                onepass = rs.getString("onepass");
            }
            else
                uid = -2;
            rs.close();
        }
        catch(Exception ex)
        {
            logger.warn("Exception caught while retrieving UID for user " + username + " : " + ex.getMessage());
        }
        if(uid != -1 && uid != -2)
        {
            try
            {
                rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                String groupList = "";
                while(rs.next())
                {
                    groupList += rs.getString("gname") + ",";
                }
                rs.close();
                rs = db.query("select", "controller, onevmid, state, uid", "vmachine", "where id=" + vmid);
                int vmUid = -1;
                int oneid = -1;
                String controller = "";
                String state = "";
                if(rs.next())
                {
                    vmUid = rs.getInt("uid");
                    oneid = rs.getInt("onevmid");
                    controller = rs.getString("controller");
                    state = rs.getString("state");
                    rs.close();
                }
                boolean isOwner = false;
                boolean isAdmin = false;
                if(uid == vmUid) isOwner = true;
                String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                logger.trace("GroupsList: " + groupList);
                for(int i=0; i<groups.length; i++)
                {
                    if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                    {
                        isAdmin = true;
                        logger.trace("Setting isAdmin to true.");
                    }
                }
                if(isAdmin || isOwner)
                {
                    try
                    {
                        ONExmlrpcHandler oneHandle = null;
                        if(controller.equalsIgnoreCase("opennebula"))
                            oneHandle = new ONExmlrpcHandler(oneIp, onePort, oneuser, onepass, "restServerAppControl::VM action");
                        if(oneHandle == null)
                        {
                            //error ONE deamon not running probably
                            //error internal
                            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                            out.append("Internal Error Encountered, probably the IaaS controller is down ...");
                            out.append("</div>");
                        }
                        else
                        {
                            if(vmaction.equalsIgnoreCase("stop"))
                            {
                                boolean status = false;
                                if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("RN") || state.equalsIgnoreCase("BT"))
                                {
                                    status = oneHandle.shutdownVM(oneid);
                                }
                                if(!status)
                                {
                                    //error VM in incorrect state
                                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                    out.append("Could not complete VM stop request. Probably the VM is in incorrect state for this operation ...");
                                    out.append("</div>");
                                }
                                else
                                {
                                    //success
                                    out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                                    out.append("VM stop operation successful!");
                                    out.append("</div>");
                                }
                            }
                            else if(vmaction.equalsIgnoreCase("suspend"))
                            {
                                boolean status = false;
                                if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("RN") || state.equalsIgnoreCase("BT") || state.equalsIgnoreCase("DP"))
                                {
                                    status = oneHandle.suspendVM(oneid);
                                }
                                if(!status)
                                {
                                    //error VM in incorrect state
                                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                    out.append("Could not complete VM suspend request. Probably the VM is in incorrect state for this operation ...");
                                    out.append("</div>");
                                }
                                else
                                {
                                    //success
                                    out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                                    out.append("VM suspend operation successful!");
                                    out.append("</div>");
                                }
                            }
                            else if(vmaction.equalsIgnoreCase("resume"))
                            {
                                boolean status = false;
                                if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("SP") || state.equalsIgnoreCase("DP"))
                                {
                                    status = oneHandle.resumeVM(oneid);
                                }
                                if(!status)
                                {
                                    //error VM in incorrect state
                                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                    out.append("Could not complete VM resume request. Probably the VM is in incorrect state for this operation ...");
                                    out.append("</div>");
                                }
                                else
                                {
                                    //success
                                    out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                                    out.append("VM resume operation successful!");
                                    out.append("</div>");
                                }
                            }
                            else if(vmaction.equalsIgnoreCase("restart"))
                            {
                                boolean status = false;
                                if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("BT"))
                                {
                                    status = oneHandle.restartVM(oneid);
                                }
                                if(!status)
                                {
                                    //error VM in incorrect state
                                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                    out.append("Could not complete VM restart request. Probably the VM is in incorrect state for this operation ...");
                                    out.append("</div>");
                                }
                                else
                                {
                                    //success
                                    out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                                    out.append("VM restart operation successful!");
                                    out.append("</div>");
                                }
                            }
                            else
                            {
                                //not supported
                                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                out.append("Operation currently not supported ...");
                                out.append("</div>");
                                logger.debug("VM action received: " + vmaction + ", not supported.");
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                        out.append("Exception caught ...<br>").append(ex.getMessage());
                        out.append("</div>");
                    }
                }
                else
                {
                    //unauthorized
                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("You are unauthorized to perform this action ...");
                    out.append("</div>");
                }
            }
            catch(Exception ex)
            {
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Exception caught ...<br>").append(ex.getMessage());
                out.append("</div>");
            }
        }
        else
        {
            //error
            if(uid == -1)
            {
                //error internal
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Internal Server Error ...");
                out.append("</div>");
            }
            else
            {
                //unregistered user found
                logger.debug("User " + username + " does not exist. OVF initialization failed.");
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Username not registered with this provider! First register and then try again ...");
                out.append("</div>");
            }
        }
        out.append("<table style='border:0px;background:white;font-family:Verdana;font-size:10pt;'>");
        out.append("<tr><td><a href='../'><img src='https://www.cise.ufl.edu/~pharsh/public/next.png' width='24'></a><td>");
        out.append("<td valign='center'>Click the continue icon to proceed.</td></tr></table>");
        //out.append("Click <a href='../'><img src='https://www.cise.ufl.edu/~pharsh/public/next.png' width='24'></a> to continue ...<br><br>");
    }
    
    void performTemplateAction(String username, StringBuilder out, Representation entity)
    {
        Form form = new Form(entity);
        int templateid = Integer.parseInt(form.getFirstValue("vmid"));
        out.append("<hr>Hello ").append(username.toUpperCase()).append("! Welcome to your application management interface.<br><br>");
        out.append("You requested action on VM template (id) ").append(templateid).append(", actiontype: deploy<br><br>");
        
        int uid = -1;
        String oneuser = "";
        String onepass = "";
        ResultSet rs;
        try
        {
            rs = db.query("select", "*", "user", "where username='" + username + "'");
            if(rs.next())
            {
                uid = rs.getInt("uid");
                oneuser = rs.getString("oneuser");
                onepass = rs.getString("onepass");
            }
            else
                uid = -2;
            rs.close();
        }
        catch(Exception ex)
        {
            logger.warn("Exception caught while retrieving UID for user " + username + " : " + ex.getMessage());
        }
        if(uid != -1 && uid != -2)
        {
            try
            {
                rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                String groupList = "";
                while(rs.next())
                {
                    groupList += rs.getString("gname") + ",";
                }
                rs.close();
                rs = db.query("select", "*", "vmachinetemplate", "where vmid=" + templateid);
                String templateGroup = "";
                int templateUid = -1;
                int gPerm = 0;
                int oPerm = 0;
                String templateValue = null;
                String templatePerm = null;
                String appName = null;
                String ovfid = null;
                String templateState = null;
                if(rs.next())
                {
                    templateUid = rs.getInt("uid");
                    templateGroup = rs.getString("gname");
                    templateValue = rs.getString("descp");
                    templatePerm = rs.getString("perm");
                    appName = rs.getString("appname");
                    ovfid = rs.getString("ovfid");
                    templateState = rs.getString("state");
                    rs.close();
                    try
                    {
                        gPerm = Integer.parseInt(String.valueOf(templatePerm.charAt(1)));
                        oPerm = Integer.parseInt(String.valueOf(templatePerm.charAt(2)));
                    }
                    catch(Exception ex)
                    {
                        logger.warn("Exception caught while getting the template's group permission bits. Exception: " + ex.getMessage());
                        gPerm = oPerm = 0;
                    }
                }
                boolean isOwner = false;
                boolean isGrpMem = false;
                if(uid == templateUid) isOwner = true;
                String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                logger.trace("GroupsList: " + groupList);
                boolean isAdmin = false;
                for(int i=0; i<groups.length; i++)
                {
                    if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                    {
                        isAdmin = true;
                        logger.trace("Setting isAdmin to true.");
                    }
                    if(groups[i].equalsIgnoreCase(templateGroup)) isGrpMem = true;
                }
                if(isAdmin || isOwner || (isGrpMem && (gPerm == 1 || gPerm == 3 || gPerm == 5 || gPerm == 7)) || (!isGrpMem && (oPerm == 1 || oPerm == 3 || oPerm == 5 || oPerm == 7)))
                {
                    ONExmlrpcHandler onehandle = null;
                    ONE3xmlrpcHandler one3handle = null;
                    if(oneVersion.startsWith("2.2"))
                    {
                        logger.debug("Creating corresponding ONE account for user " + username + " [" + oneuser + ", " + onepass + "].");
                        onehandle = new ONExmlrpcHandler(oneIp, onePort, oneuser, onepass, "restServerAppControl:deployTemplate()");
                    }
                    else if(oneVersion.startsWith("3.4"))
                    {
                        logger.debug("Creating corresponding ONE3 account for user " + username + " [" + oneuser + ", " + onepass + "].");
                        one3handle = new ONE3xmlrpcHandler(oneIp, onePort, oneuser, onepass, "restServerAppControl:deployTemplate()");
                    }
                    //now get the disk image list associated with this vm template and if not registered with ONE then try to register it
                    rs = db.query("select", "id, localpath, name, state, oneimgdescp", "diskimage", "where vmid=" + templateid);
                    int counter = 0;
                    while(rs.next())
                    {
                        counter++;
                        String imgName = rs.getString("name");
                        String imgPath = rs.getString("localpath");
                        String imgState =rs.getString("state");
                        String imgTemplate = rs.getString("oneimgdescp");
                        int imgId = rs.getInt("id");
                        if(!imgState.equalsIgnoreCase("OR")) //if state is ORPHANed then do not do anything, it will be garbage collected soon
                        {
                            //getting the already registered ONE image list
                            LinkedList<ONEImage> imageList = null;
                            if(onehandle != null)
                                imageList = onehandle.getImageList();
                            else if(one3handle != null)
                                imageList = one3handle.getImageList();
                            
                            boolean found = false;
                            for(int i=0; i< imageList.size(); i++)
                            {
                                ONEImage img = imageList.get(i);
                                logger.debug("Got ONE Image Details: " + img.imageName + ", " + img.localPath + ", " + img.state);
                                if(img.imageName.equalsIgnoreCase(imgName) && img.localPath.equalsIgnoreCase(imgPath))
                                {
                                    //update the diskimage entry with missing details if not already updated
                                    if(!imgState.equalsIgnoreCase("RG"))
                                    {
                                        boolean status = db.update("diskimage", "oneimgname='" + img.imageName + "', oneimgid=" + img.id + ", state='RG'", "where id=" + imgId);
                                        if(status)
                                        {
                                            logger.debug("For discimage id=" + imgId + " corresponding ONE image with id " + img.id + " was found. VEP-DB updated.");
                                        }
                                        else
                                        {
                                            logger.warn("For discimage id=" + imgId + " corresponding ONE image with id " + img.id + " was found. VEP-DB update failed.");
                                        }
                                    }
                                    found = true;
                                    break;
                                }
                            }

                            if(!found)
                            {
                                //issue ONE image register command and store corresponding data
                                //for now we assume that the VM image resides locally.
                                int oneImgId = -1;
                                if(onehandle != null)
                                    oneImgId = onehandle.addImage(imgTemplate);
                                else if(one3handle != null)
                                    oneImgId = one3handle.addImage(imgTemplate);
                                
                                if(oneImgId != -1)
                                {
                                    ONEImage temp = null;
                                    if(onehandle != null)
                                        temp = onehandle.getImageInfo(oneImgId);
                                    else if(one3handle != null)
                                        temp = one3handle.getImageInfo(oneImgId);
                                    
                                    boolean status = db.update("diskimage", "oneimgname='" + temp.imageName + "', oneimgid=" + temp.id + ", state='RG'", "where id=" + imgId);
                                    if(status)
                                    {
                                        logger.debug("For discimage id=" + imgId + " corresponding ONE image with id " + temp.id + " was registered. VEP-DB updated.");
                                    }
                                    else
                                    {
                                        logger.warn("For discimage id=" + imgId + " corresponding ONE image with id " + temp.id + " was registered. VEP-DB update failed.");
                                    }
                                }
                                else
                                {
                                    logger.warn("Some error while registering image " + imgName + " disk-id: " + imgId + " with ONE.");
                                    //error internal
                                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                    out.append("Internal Server Error ...");
                                    out.append("</div>");
                                }
                            }
                        }
                        rs = db.query("select", "id, localpath, name, state, oneimgdescp", "diskimage", "where vmid=" + templateid);
                        for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after updates.
                    }
                    rs.close();
                    //now send start for the vmtemplate
                    if(!templateState.equalsIgnoreCase("OR")) //ignore OR state as it will be garbage collected soon
                    {
                        int onevmid = -1;
                        if(onehandle != null)
                            onevmid = onehandle.addVM(templateValue + "\nREQUIREMENTS = \"CLUSTER = contrail\"");
                        else if(one3handle != null)
                            onevmid = one3handle.addVM(templateValue + "\nREQUIREMENTS = \"CLUSTER = contrail\"");
                        
                        if(onevmid != -1)
                        {
                            ONEVm temp = null;
                            if(onehandle != null)
                                temp = onehandle.getVmInfo(onevmid);
                            else if(one3handle != null)
                                temp = one3handle.getVmInfo(onevmid);

                            //now store in the vmachine table, also change the state of the ovf entry to DP from ND
                            ResultSet rs1 = db.query("select", "max(id)", "vmachine", "");
                            int vid = 0;
                            if(rs1.next()) vid = rs1.getInt(1) + 1;
                            rs1.close();
                            boolean status = db.insert("vmachine", "(" + vid + ", " + uid + ", 'OpenNebula', " + temp.id + ", " + templateid + ", '" + temp.name + "', 'DP', -1, '" +
                                    temp.ip + "', '" + temp.graphics_port + "', '" + temp.graphics_ip + "', '')");
                            if(status)
                            {
                                if(one3handle != null)
                                {
                                    //extra step to deploy the VM
                                    int hid = VEPHostPool.getNextHost();
                                    if(hid != -1)
                                    {
                                        status = one3handle.deployVM(onevmid, hid);
                                        if(status)
                                        {
                                            logger.debug("VM with ONE3ID:" + onevmid + " was successfully allocated and deployed on machine with ONEhostID: " + hid + ".");
                                            out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                                            out.append("VM template deployment successful!");
                                            out.append("</div>");
                                        }
                                        else
                                        {
                                            logger.warn("VM with ONE3ID:" + onevmid + " was successfully allocated but deployment on machine with ONEhostID: " + hid + " FAILED.");
                                            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                            out.append("Template was allocated but resulted in error while deploying on target host!");
                                            out.append("</div>");
                                        }
                                    }
                                    else
                                    {
                                        logger.warn("VM with ONE3ID:" + onevmid + " was successfully allocated but deployment FAILED because of unavailability of host to deploy.");
                                        out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                        out.append("Template was allocated but resulted in error while deploying because of unavailability of target host ...");
                                        out.append("</div>");
                                    }
                                }
                                else
                                {
                                    logger.debug("Successfully deployed VM information - oneId:" + temp.id + " oneName:" + temp.name 
                                        + " Graphics:" + temp.graphics_type + "-" + temp.graphics_ip + "-" + temp.graphics_port);
                                    //success
                                    out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                                    out.append("VM template deployment successful!");
                                    out.append("</div>");
                                }
                                db.update("vmachinetemplate", "state='DP'", "where vmid=" + templateid);
                            }
                            else
                            {
                                logger.warn("Failed to store deployed VM information - oneId:" + temp.id + " oneName:" + temp.name 
                                    + " Graphics:" + temp.graphics_type + "-" + temp.graphics_ip + "-" + temp.graphics_port);
                                db.update("vmachinetemplate", "state='UN'", "where vmid=" + templateid);
                                //partial success, internal state could not be stored
                                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                out.append("Template was deployed but error while storing the state ...");
                                out.append("</div>");
                            }
                        }
                        else
                        {
                            logger.warn("Some error while deploying VM template " + templateid + ":" + ovfid + " with ONE.");
                            db.update("vmachinetemplate", "state='ER'", "where vmid=" + templateid);
                            //error internal
                            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                            out.append("Internal Server Error ...");
                            out.append("</div>");
                        }
                    }
                    else
                    {
                        //error template state is OR
                        out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                        out.append("Cannot deploy the template, internal state conflict detected ...");
                        out.append("</div>");
                    }
                }
                else
                {
                    //unauthorized
                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("You are unauthorized to perform this action ...");
                    out.append("</div>");
                }
            }
            catch(Exception ex)
            {
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Exception caught ...<br>").append(ex.getMessage());
                out.append("</div>");
            }
        }
        else
        {
            //error
            if(uid == -1)
            {
                //error internal
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Internal Server Error ...");
                out.append("</div>");
            }
            else
            {
                //unregistered user found
                logger.debug("User " + username + " does not exist. OVF initialization failed.");
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Username not registered with this provider! First register and then try again ...");
                out.append("</div>");
            }
        }
        
        out.append("<table style='border:0px;background:white;font-family:Verdana;font-size:10pt;'>");
        out.append("<tr><td><a href='../'><img src='https://www.cise.ufl.edu/~pharsh/public/next.png' width='24'></a><td>");
        out.append("<td valign='center'>Click the continue icon to proceed.</td></tr></table>");
        //out.append("Click <a href='../'><img src='https://www.cise.ufl.edu/~pharsh/public/next.png' width='24'></a> to continue ...<br><br>");
    }
    
    void performOVFAction(String username, StringBuilder out, Representation entity)
    {
        Form form = new Form(entity);
        String ovfid = form.getFirstValue("ovfid");
        String ovfaction = form.getFirstValue("ovfaction");
        out.append("<hr>Hello ").append(username.toUpperCase()).append("! Welcome to your application management interface.<br><br>");
        out.append("You requested action on OVF ").append(ovfid).append(", actiontype: ").append(ovfaction).append("<br><br>");
        if(ovfaction.trim().equalsIgnoreCase("initialize"))
        {
            initializeOVF(username, out, Integer.parseInt(ovfid));
        }
        else if(ovfaction.trim().equalsIgnoreCase("deploy"))
        {
            deployOVF(username, out, Integer.parseInt(ovfid));
        }
        else if(ovfaction.trim().equalsIgnoreCase("stop"))
        {
            stopOVF(username, out, Integer.parseInt(ovfid));
        }
        else if(ovfaction.trim().equalsIgnoreCase("edit"))
        {
            editOVF(username, out, Integer.parseInt(ovfid));
        }
        else if(ovfaction.trim().equalsIgnoreCase("delete"))
        {
            deleteOVF(username, out, Integer.parseInt(ovfid));
        }
        else
        {
            //unsupported action
        }
        out.append("<table style='border:0px;background:white;font-family:Verdana;font-size:10pt;'>");
        out.append("<tr><td><a href='../'><img src='https://www.cise.ufl.edu/~pharsh/public/next.png' width='24'></a><td>");
        if(ovfaction.trim().equalsIgnoreCase("edit"))
            out.append("<td valign='center'>Click the continue icon to cancel and go back to the application management page.</td></tr></table>");
        else
            out.append("<td valign='center'>Click the continue icon to proceed.</td></tr></table>");
        //out.append("Click <a href='../'><img src='https://www.cise.ufl.edu/~pharsh/public/next.png' width='24'></a> to continue ...<br><br>");
    }
    
    void editOVF(String username, StringBuilder out, int ovfid)
    {
        int uid = -1;
        ResultSet rs;
        //now get the uid for the username
        try
        {
            rs = db.query("select", "*", "user", "where username='" + username + "'");
            if(rs.next())
            {
                uid = rs.getInt("uid");
            }
            else
                uid = -2;
            rs.close();
        }
        catch(Exception ex)
        {
            //error message
            logger.warn("Exception caught while retrieving UID for user " + username + " : " + ex.getMessage());
        }
        if(uid != -1 && uid != -2)
        {
            try
            {
                rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                String groupList = "";
                while(rs.next())
                {
                    groupList += rs.getString("gname") + ",";
                }
                rs.close();
                rs = db.query("select", "*", "ovf", "where sno=" + ovfid);
                String ovfGroup = "";
                int ovfUid = -1;
                int gPerm = 0;
                int oPerm = 0;
                String ovfValue = null;
                String ovfPerm = null;
                String ovfAppName = null;
                if(rs.next())
                {
                    ovfUid = rs.getInt("uid");
                    ovfGroup = rs.getString("gname");
                    ovfValue = rs.getString("descp");
                    ovfPerm = rs.getString("perm");
                    ovfAppName = rs.getString("ovfname");
                    rs.close();
                    try
                    {
                        gPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(1)));
                        oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                    }
                    catch(Exception ex)
                    {
                        logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                        gPerm = oPerm = 0;
                    }
                }
                boolean isOwner = false;
                boolean isGrpMem = false;
                if(uid == ovfUid) isOwner = true;
                String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                logger.trace("GroupsList: " + groupList);
                boolean isAdmin = false;
                for(int i=0; i<groups.length; i++)
                {
                    if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                    {
                        isAdmin = true;
                        logger.trace("Setting isAdmin to true.");
                    }
                    if(groups[i].equalsIgnoreCase(ovfGroup)) isGrpMem = true;
                }
                if(isAdmin || isOwner || (isGrpMem && (gPerm == 2 || gPerm == 3 || gPerm == 6 || gPerm == 7)) || (!isGrpMem && (oPerm == 2 || oPerm == 3 || oPerm == 6 || oPerm == 7)))
                {
                    try
                    {
                        out.append("<table style='width:1014px;hspace:5px;background:silver;border:0px;cellspacing:2px;padding:2px;font-family:Courier;font-size:10pt;color:black;'>");
                        out.append("<tr>");
                        out.append("<td valign='top' align='center' style='width:128px;background:white;'><img style='width:128px;' src='https://www.cise.ufl.edu/~pharsh/public/template.png'><br><br>");
                        out.append("<div id='progressbar' style='visibility:hidden;'><img src='https://www.cise.ufl.edu/~pharsh/public/progress.gif'></div>");
                        out.append("<td valign='top' align='left' bgcolor='white' width='*'>");
                        out.append("<b>Update the existing OVF application</b>:");

                        out.append("<div style='background:#E3EDEB;font-weight:normal;margin:5px;'><form name='updateovf' action='../doaction/updateovf' method='post'>");
                        out.append("Application Name: <input type='text' name='appname' size='32' disabled='disabled' value='").append(ovfAppName).append("'><br>");
                        out.append("Group: <input type='text' name='group' value='").append(ovfGroup).append("'> ");
                        out.append("Permission: <input type='text' name='perm' size='3' value='").append(ovfPerm).append("'><br>");
                        out.append("<input type='hidden' name='ovfid' value='").append(ovfid).append("'>");
                        out.append("<input type='hidden' name='name' size='32' value='").append(ovfAppName).append("'>");
                        out.append("Modify the OVF content below:<br><textarea name='descp' rows='10' cols='85'>").append(ovfValue).append("</textarea><br>");
                        out.append("<input type='submit' value='update' onClick=\"javascript:showstuff('progressbar');\">");
                        out.append("</form></div>");

                        out.append("</table><br>");
                    }
                    catch(Exception ex)
                    {
                        out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                        out.append("Exception caught ...<br>").append(ex.getMessage());
                        out.append("</div>");
                    }
                }
                else
                {
                    //unauthorized
                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("You are unauthorized to perform this action ...");
                    out.append("</div>");
                }
            }
            catch(Exception ex)
            {
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Exception caught ...<br>").append(ex.getMessage());
                out.append("</div>");
            }
        }
        else
        {
            //error
            if(uid == -1)
            {
                //error internal
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Internal Server Error ...");
                out.append("</div>");
            }
            else
            {
                //unregistered user found
                logger.debug("User " + username + " does not exist. OVF initialization failed.");
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Username not registered with this provider! First register and then try again ...");
                out.append("</div>");
            }
        }
    }
    
    void deleteOVF(String username, StringBuilder out, int ovfid)
    {
        int uid = -1;
        ResultSet rs;
        String oneuser = "";
        String onepass = "";
        //now get the uid for the username
        try
        {
            rs = db.query("select", "*", "user", "where username='" + username + "'");
            if(rs.next())
            {
                uid = rs.getInt("uid");
                oneuser = rs.getString("oneuser");
                onepass = rs.getString("onepass");
            }
            else
                uid = -2;
            rs.close();
        }
        catch(Exception ex)
        {
            //error message
            logger.warn("Exception caught while retrieving UID for user " + username + " : " + ex.getMessage());
        }
        if(uid != -1 && uid != -2)
        {
            try
            {
                rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                String groupList = "";
                while(rs.next())
                {
                    groupList += rs.getString("gname") + ",";
                }
                rs.close();
                rs = db.query("select", "*", "ovf", "where sno=" + ovfid);
                String ovfGroup = "";
                int ovfUid = -1;
                int gPerm = 0;
                int oPerm = 0;
                String ovfValue = null;
                String ovfPerm = null;
                if(rs.next())
                {
                    ovfUid = rs.getInt("uid");
                    ovfGroup = rs.getString("gname");
                    ovfValue = rs.getString("descp");
                    ovfPerm = rs.getString("perm");
                    rs.close();
                    try
                    {
                        gPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(1)));
                        oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                    }
                    catch(Exception ex)
                    {
                        logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                        gPerm = oPerm = 0;
                    }
                }
                boolean isOwner = false;
                boolean isGrpMem = false;
                if(uid == ovfUid) isOwner = true;
                String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                logger.trace("GroupsList: " + groupList);
                boolean isAdmin = false;
                for(int i=0; i<groups.length; i++)
                {
                    if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                    {
                        isAdmin = true;
                        logger.trace("Setting isAdmin to true.");
                    }
                    if(groups[i].equalsIgnoreCase(ovfGroup)) isGrpMem = true;
                }
                if(isAdmin || isOwner || (isGrpMem && (gPerm == 1 || gPerm == 3 || gPerm == 5 || gPerm == 7)) || (!isGrpMem && (oPerm == 1 || oPerm == 3 || oPerm == 5 || oPerm == 7)))
                {
                    try
                    {
                        ONExmlrpcHandler onehandle = null;
                        ONE3xmlrpcHandler one3handle = null;
                        if(oneVersion.startsWith("2.2"))
                        {
                            logger.debug("Creating corresponding ONE account for user " + username + " [" + oneuser + ", " + onepass + "].");
                            onehandle = new ONExmlrpcHandler(oneIp, onePort, oneuser, onepass, "restServerAppControl:deleteOVF");
                        }
                        else if(oneVersion.startsWith("3.4"))
                        {
                            logger.debug("Creating corresponding ONE3 account for user " + username + " [" + oneuser + ", " + onepass + "].");
                            one3handle = new ONE3xmlrpcHandler(oneIp, onePort, oneuser, onepass, "restServerAppControl:deleteOVF");
                        }
                        
                        rs = db.query("select", "vmid", "vmachinetemplate", "where ovfsno=" + ovfid);
                        int counter = 0;
                        while(rs.next())
                        {
                            counter++;
                            int vmid = rs.getInt("vmid");
                            ResultSet rs1 = db.query("select", "id, onevmid, state", "vmachine", "where vmid=" + vmid);
                            while(rs1.next())
                            {
                                int id = rs1.getInt("id");
                                String state = rs1.getString("state");
                                if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("RN") || state.equalsIgnoreCase("BT"))
                                {
                                    if(onehandle != null)
                                        onehandle.shutdownVM(rs1.getInt("onevmid"));
                                    else if(one3handle != null)
                                        one3handle.shutdownVM(rs1.getInt("onevmid"));
                                    //wait for 5 seconds before sending next command
                                    Thread.sleep(5000);
                                }
                            }
                            rs1.close();
                            //delete diskimage entry corresponding to this vmid
                            db.delete("diskimage", "where vmid=" + vmid);
                            db.delete("vmachine", "where vmid=" + vmid);

                            rs = db.query("select", "vmid", "vmachinetemplate", "where ovfsno=" + ovfid);
                            for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after db operations.
                        }
                        rs.close();
                        //now remove all template entries that correspond to vmids corresponding to ovfsno
                        //also remove all vmachine entries that corresponds to the removed vmid
                        db.delete("vmachinetemplate", "where ovfsno=" + ovfid);

                        //now delete the OVF entry
                        boolean status = db.delete("ovf","where sno=" + ovfid);
                        if(status)
                        {
                            //success
                            out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                            out.append("Operation successful!");
                            out.append("</div>");
                        }
                        else
                        {
                            //internal server error
                            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                            out.append("Internal Server Error ...");
                            out.append("</div>");
                        }
                    }
                    catch(Exception ex)
                    {
                        out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                        out.append("Exception caught ...<br>").append(ex.getMessage());
                        out.append("</div>");
                    }
                }
                else
                {
                    //unauthorized
                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("You are unauthorized to perform this action ...");
                    out.append("</div>");
                }
            }
            catch(Exception ex)
            {
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Exception caught ...<br>").append(ex.getMessage());
                out.append("</div>");
            }
        }
        else
        {
            //error
            if(uid == -1)
            {
                //error internal
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Internal Server Error ...");
                out.append("</div>");
            }
            else
            {
                //unregistered user found
                logger.debug("User " + username + " does not exist. OVF initialization failed.");
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Username not registered with this provider! First register and then try again ...");
                out.append("</div>");
            }
        }
    }
    
    void stopOVF(String username, StringBuilder out, int ovfid)
    {
        int uid = -1;
        ResultSet rs;
        //now get the uid for the username
        try
        {
            rs = db.query("select", "*", "user", "where username='" + username + "'");
            if(rs.next())
                uid = rs.getInt("uid");
            else
                uid = -2;
            rs.close();
        }
        catch(Exception ex)
        {
            //error message
            logger.warn("Exception caught while retrieving UID for user " + username + " : " + ex.getMessage());
        }
        if(uid != -1 && uid != -2)
        {
            try
            {
                rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                String groupList = "";
                while(rs.next())
                {
                    groupList += rs.getString("gname") + ",";
                }
                rs.close();
                rs = db.query("select", "*", "ovf", "where sno=" + ovfid);
                String ovfGroup = "";
                int ovfUid = -1;
                int gPerm = 0;
                int oPerm = 0;
                String ovfValue = null;
                String ovfPerm = null;
                if(rs.next())
                {
                    ovfUid = rs.getInt("uid");
                    ovfGroup = rs.getString("gname");
                    ovfValue = rs.getString("descp");
                    ovfPerm = rs.getString("perm");
                    rs.close();
                    try
                    {
                        gPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(1)));
                        oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                    }
                    catch(Exception ex)
                    {
                        logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                        gPerm = oPerm = 0;
                    }
                }
                boolean isOwner = false;
                boolean isGrpMem = false;
                if(uid == ovfUid) isOwner = true;
                String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                logger.trace("GroupsList: " + groupList);
                boolean isAdmin;
                for(int i=0; i<groups.length; i++)
                {
                    if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                    {
                        isAdmin = true;
                        logger.trace("Setting isAdmin to true.");
                    }
                    if(groups[i].equalsIgnoreCase(ovfGroup)) isGrpMem = true;
                }
                if(isOwner || (isGrpMem && (gPerm == 1 || gPerm == 3 || gPerm == 5 || gPerm == 7)) || (!isGrpMem && (oPerm == 1 || oPerm == 3 || oPerm == 5 || oPerm == 7)))
                {
                    try
                    {
                        rs = db.query("select", "oneuser, onepass, oneid", "user", "where username='" + username + "'");
                        String oneUser = "";
                        String onePass = "";
                        int oneId = -1;

                        if(rs.next())
                        {
                            oneUser = rs.getString("oneuser");
                            onePass = rs.getString("onepass");
                            oneId = rs.getInt("oneid");
                        }
                        rs.close();
                        if(oneId != -1)
                        {
                            logger.debug("Retrieved corresponding ONE account for user " + username + " [" + oneUser + ", " + onePass + ", one-id: " + oneId + "].");
                            ONExmlrpcHandler onehandle = null;
                            ONE3xmlrpcHandler one3handle = null;
                            if(oneVersion.startsWith("2.2"))
                            {
                                logger.debug("Creating corresponding ONE account for user " + username + " [" + oneUser + ", " + onePass + "].");
                                onehandle = new ONExmlrpcHandler(oneIp, onePort, oneUser, onePass, "restServerAppControl:OVFaction-stop");
                            }
                            else if(oneVersion.startsWith("3.4"))
                            {
                                logger.debug("Creating corresponding ONE3 account for user " + username + " [" + oneUser + ", " + onePass + "].");
                                one3handle = new ONE3xmlrpcHandler(oneIp, onePort, oneUser, onePass, "restServerAppControl:OVFaction-stop");
                            }

                            //now retrieve the diskimages corresponding to the ovfsno and see if
                            //some are already in the ONE system, if they are update the db entries filling in necessary details
                            //for the remaining entries, issue image templates for registration to the ONE front end.
                            rs = db.query("select", "state", "vmachinetemplate", "where ovfsno=" + ovfid);
                            String vmtemplateState = "ND";
                            if(rs.next())
                            {
                                vmtemplateState = rs.getString("state");
                            }
                            rs.close();
                            if(vmtemplateState.equalsIgnoreCase("DP") || vmtemplateState.equalsIgnoreCase("IN"))
                            {
                                rs = db.query("select", "vmid", "vmachinetemplate", "where ovfsno=" + ovfid);
                                int counter = 0;
                                while(rs.next())
                                {
                                    counter++;
                                    int vmid = rs.getInt("vmid");
                                    //now reset this vmachine template to IN
                                    db.update("vmachinetemplate", "state='IN'", "where vmid=" + vmid);
                                    //get list of all VMs corresponding to this vmid
                                    ResultSet rs1 = db.query("select", "id, onevmid, state", "vmachine", "where vmid=" + vmid);
                                    while(rs1.next())
                                    {
                                        int oneid = rs1.getInt("onevmid");
                                        String state = rs1.getString("state");
                                        if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("RN") || state.equalsIgnoreCase("BT"))
                                        {
                                            if(onehandle != null)
                                                onehandle.shutdownVM(oneid);
                                            else if(one3handle != null)
                                                one3handle.shutdownVM(oneid);
                                            //wait for 5 seconds before sending the next shutdown command
                                            Thread.sleep(5000);
                                        }
                                    }
                                    rs1.close();
                                    rs = db.query("select", "vmid", "vmachinetemplate", "where ovfsno=" + ovfid);
                                    for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after db operations.
                                }
                                rs.close();
                                out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                                out.append("Operation successful!");
                                out.append("</div>");
                            }
                            else
                            {
                                //first issue a initialize and deploy command
                                logger.warn("Problem deploying the OVF as it was not initialized.");
                                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                out.append("This application has not been initialized and deployed. I can not stop a non-running application");
                                out.append("</div>");
                            }
                        }
                        else
                        {
                            logger.warn("Problem resolving one username and password for the user " + username);
                            //error user account not properly configured
                            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                            out.append("Your account is not configured properly, please contact your system administrator to fix the IaaS translation problem with your account ...");
                            out.append("</div>");
                        }
                    }
                    catch(Exception ex)
                    {
                        out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                        out.append("Exception caught ...<br>").append(ex.getMessage());
                        out.append("</div>");
                    }
                }
                else
                {
                    //error unauthorized
                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("You are unauthorized to perform this action ...");
                    out.append("</div>");
                }
            }
            catch(Exception ex)
            {
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Exception caught ...<br>").append(ex.getMessage());
                out.append("</div>");
            }
        }
        else
        {
            //error
            if(uid == -1)
            {
                //error internal
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Internal Server Error ...");
                out.append("</div>");
            }
            else
            {
                //unregistered user found
                logger.debug("User " + username + " does not exist. OVF initialization failed.");
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Username not registered with this provider! First register and then try again ...");
                out.append("</div>");
            }
        }
    }
    
    void initializeOVF(String username, StringBuilder out, int ovfid)
    {
        int uid = -1;
        ResultSet rs;
        //now get the uid for the username
        try
        {
            rs = db.query("select", "*", "user", "where username='" + username + "'");
            if(rs.next())
                uid = rs.getInt("uid");
            else
                uid = -2;
            rs.close();
        }
        catch(Exception ex)
        {
            //error message
            logger.warn("Exception caught while retrieving UID for user " + username + " : " + ex.getMessage());
        }
        if(uid != -1 && uid != -2)
        {
            try
            {
                rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                String groupList = "";
                while(rs.next())
                {
                    groupList += rs.getString("gname") + ",";
                }
                rs.close();
                rs = db.query("select", "*", "ovf", "where sno=" + ovfid);
                String ovfGroup = "";
                int ovfUid = -1;
                int gPerm = 0;
                int oPerm = 0;
                String ovfValue = null;
                String ovfPerm = null;
                if(rs.next())
                {
                    ovfUid = rs.getInt("uid");
                    ovfGroup = rs.getString("gname");
                    ovfValue = rs.getString("descp");
                    ovfPerm = rs.getString("perm");
                    rs.close();
                    try
                    {
                        gPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(1)));
                        oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                    }
                    catch(Exception ex)
                    {
                        logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                        gPerm = oPerm = 0;
                    }
                }
                boolean isOwner = false;
                boolean isGrpMem = false;
                if(uid == ovfUid) isOwner = true;
                String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                logger.trace("GroupsList: " + groupList);
                boolean isAdmin;
                for(int i=0; i<groups.length; i++)
                {
                    if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                    {
                        isAdmin = true;
                        logger.trace("Setting isAdmin to true.");
                    }
                    if(groups[i].equalsIgnoreCase(ovfGroup)) isGrpMem = true;
                }
                if(isOwner || (isGrpMem && (gPerm == 1 || gPerm == 3 || gPerm == 5 || gPerm == 7)) || (!isGrpMem && (oPerm == 1 || oPerm == 3 || oPerm == 5 || oPerm == 7)))
                {
                    try
                    {   
                        OpenNebulaOVFParser ovfParser = new OpenNebulaOVFParser(ovfValue, oneVersion, oneAdmin);
                        String appName = ovfParser.getApplicationName();
                        int vmCount = ovfParser.getCount();
                        String[] ovfIds = ovfParser.getIDs();
                        //first get a list of ovfids corresponding to this ovf which may be already present in the database
                        rs = db.query("select", "vmid, ovfid", "vmachinetemplate", "where ovfsno=" + ovfid + "");
                        while(rs.next())
                        {
                            //check if this ovfid is in the list of ovfids , if not mark as orphan for garbage collector to clean up periodically
                            boolean isFound = false;
                            for(int i=0; i<ovfIds.length; i++)
                            {
                                if(ovfIds[i].equalsIgnoreCase(rs.getString("ovfid"))) isFound = true;
                            }
                            if(!isFound)
                            {
                                //mark this entry as orphaned. 2 letter code: OR
                                db.update("vmachinetemplate", "state='OR'", "where vmid=" + rs.getInt("vmid") + " AND ovfid='" + rs.getString("ovfid") + "'");
                            }
                        }
                        rs.close();
                        //now update an existing entry or add a new one if needed.
                        for(int i=0; i<ovfIds.length; i++)
                        {
                            //first get the VM template
                            String vmTemplate = ovfParser.getVMTemplate(ovfIds[i]);
                            rs = db.query("select", "vmid, state", "vmachinetemplate", "where ovfsno=" + ovfid + " AND ovfid='" + ovfIds[i] + "'");
                            boolean status = false;
                            int vmid = 0;
                            if(rs.next())
                            {
                                //now update the entry ...
                                vmid = rs.getInt("vmid");
                                String state = rs.getString("state");
                                //if state was ER then change to IN
                                if(state.equalsIgnoreCase("er"))
                                    status = db.update("vmachinetemplate", "appname='" + appName + "', descp='" + vmTemplate + "', state='IN'", "where vmid=" + vmid);
                                else //do not change the state
                                    status = db.update("vmachinetemplate", "appname='" + appName + "', descp='" + vmTemplate + "'", "where vmid=" + vmid);
                                rs.close();
                                if(status) logger.debug("Vmachine Template was updated successfully for OVF Sno: " + ovfid + ", ovfid: " + ovfIds[i]);
                                else logger.warn("Vmachine Template could not be updated for OVF Sno: " + ovfid + ", ovfid: " + ovfIds[i]);
                            }
                            else
                            {
                                //create a new entry
                                rs = db.query("select", "max(vmid)", "vmachinetemplate", "");
                                if(rs.next()) vmid = rs.getInt(1) + 1;
                                status = db.insert("vmachinetemplate", "(" + vmid + ", " + ovfUid + ", '" + ovfGroup + "', '" + ovfPerm + "', '" +
                                        appName + "', '-1', '-1', 'IN', " + ovfid + ", '" + ovfIds[i] + "', -1, '" + vmTemplate + "')");
                                if(status) logger.debug("Vmachine Template was added successfully for OVF Sno: " + ovfid + ", ovfid: " + ovfIds[i]);
                                else logger.warn("Vmachine Template could not be added for OVF Sno: " + ovfid + ", ovfid: " + ovfIds[i]);
                                rs.close();
                            }
                            //now do the same for image templates and at the end return the list of template ids back to the federation.

                            String[] disks = ovfParser.getVMDisksId(ovfIds[i]);
                            rs = db.query("select", "*", "diskimage", "where vmid=" + vmid);
                            while(rs.next())
                            {
                                int imageid = rs.getInt("id");
                                String imageName = rs.getString("name");
                                logger.debug("Found disk: ImageID" + imageid + ", imageName: " + imageName);
                                boolean found = false;
                                for(int k=0; k<disks.length; k++)
                                    if(disks[k].equalsIgnoreCase(imageName)) found = true;
                                if(!found) db.update("diskimage", "state='OR'", "where id=" + imageid);
                            }
                            rs.close();

                            for(int j=0; j<disks.length; j++)
                            {
                                String diskTemplate = ovfParser.getImageTemplate(ovfIds[i], disks[j]);
                                String diskPath = ovfParser.getImageDiskPath(ovfIds[i], disks[j]);
                                //if image template has changed then set the image state to IN initialization
                                //IN provides hint that the image template has not been submitted to the ONE
                                rs = db.query("select", "*", "diskimage", "where vmid=" + vmid + " AND name='" + disks[j] + "'");
                                if(rs.next())
                                {
                                    int id = rs.getInt("id");
                                    if(!rs.getString("oneimgdescp").equalsIgnoreCase(diskTemplate))
                                    {
                                        db.update("diskimage", "state='IN', oneimgdescp='" + diskTemplate + "', localpath='" + diskPath + "'", "where id=" + id);
                                    }
                                    rs.close();
                                }
                                else
                                {
                                    //create a new entry
                                    int imgid = 0;
                                    rs = db.query("select", "max(id)", "diskimage", "");
                                    if(rs.next()) imgid = rs.getInt(1) + 1;
                                    status = db.insert("diskimage", "(" + imgid + ", '-1', '" + disks[j] + "', '-1', -1, " + vmid + ", 'IN', '" 
                                            + diskTemplate + "', '" + diskPath + "')");
                                    if(status) logger.debug("Disk Template was added successfully for OVF Sno: " + ovfid + ", ovfid: " + ovfIds[i] + " for diskId: " + disks[j]);
                                    else logger.warn("Disk Template could not be added for OVF Sno: " + ovfid + ", ovfid: " + ovfIds[i] + " for diskId: " + disks[j]);
                                    rs.close();
                                }
                            }
                        }
                        //success
                        out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                        out.append("Operation successful!");
                        out.append("</div>");
                    }
                    catch(Exception ex)
                    {
                        //internal server error
                        if(logger.isDebugEnabled())
                            ex.printStackTrace(System.err);
                        else
                            logger.warn(ex.getMessage());
                        
                        out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                        out.append("Exception caught ...<br>").append(ex.getMessage());
                        out.append("</div>");
                    }
                }
                else
                {
                    //unauthorized to proceed
                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("You are unauthorized to perform this action ...");
                    out.append("</div>");
                }
            }
            catch(Exception ex)
            {
                //internal error
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Exception caught ...<br>").append(ex.getMessage());
                out.append("</div>");
            }
        }
        else
        {
            //error
            if(uid == -1)
            {
                //error internal
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Internal Server Error ...");
                out.append("</div>");
            }
            else
            {
                //unregistered user found
                logger.debug("User " + username + " does not exist. OVF initialization failed.");
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Username not registered with this provider! First register and then try again ...");
                out.append("</div>");
            }
        }
    }
    
    void deployOVF(String username, StringBuilder out, int ovfid)
    {
        int uid = -1;
        ResultSet rs;
        //now get the uid for the username
        try
        {
            rs = db.query("select", "*", "user", "where username='" + username + "'");
            if(rs.next())
                uid = rs.getInt("uid");
            else
                uid = -2;
            rs.close();
        }
        catch(Exception ex)
        {
            //error message
            logger.warn("Exception caught while retrieving UID for user " + username + " : " + ex.getMessage());
            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
            out.append("Exception caught ...<br>").append(ex.getMessage());
            out.append("</div>");
        }
        if(uid != -1 && uid != -2)
        {
            try
            {
                rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                String groupList = "";
                while(rs.next())
                {
                    groupList += rs.getString("gname") + ",";
                }
                rs.close();
                rs = db.query("select", "*", "ovf", "where sno=" + ovfid);
                String ovfGroup = "";
                int ovfUid = -1;
                int gPerm = 0;
                int oPerm = 0;
                String ovfValue = null;
                String ovfPerm = null;
                if(rs.next())
                {
                    ovfUid = rs.getInt("uid");
                    ovfGroup = rs.getString("gname");
                    ovfValue = rs.getString("descp");
                    ovfPerm = rs.getString("perm");
                    rs.close();
                    try
                    {
                        gPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(1)));
                        oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                    }
                    catch(Exception ex)
                    {
                        logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                        gPerm = oPerm = 0;
                    }
                }
                boolean isOwner = false;
                boolean isGrpMem = false;
                if(uid == ovfUid) isOwner = true;
                String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                logger.trace("GroupsList: " + groupList);
                boolean isAdmin;
                for(int i=0; i<groups.length; i++)
                {
                    if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                    {
                        isAdmin = true;
                        logger.trace("Setting isAdmin to true.");
                    }
                    if(groups[i].equalsIgnoreCase(ovfGroup)) isGrpMem = true;
                }
                if(isOwner || (isGrpMem && (gPerm == 1 || gPerm == 3 || gPerm == 5 || gPerm == 7)) || (!isGrpMem && (oPerm == 1 || oPerm == 3 || oPerm == 5 || oPerm == 7)))
                {
                    rs = db.query("select", "oneuser, onepass, oneid", "user", "where username='" + username + "'");
                    String oneUser = "";
                    String onePass = "";
                    int oneId = -1;

                    if(rs.next())
                    {
                        oneUser = rs.getString("oneuser");
                        onePass = rs.getString("onepass");
                        oneId = rs.getInt("oneid");
                    }
                    rs.close();
                    if(oneId != -1)
                    {
                        logger.debug("Retrieved corresponding ONE account for user " + username + " [" + oneUser + ", " + onePass + ", one-id: " + oneId + "].");
                        ONExmlrpcHandler onehandle = null;
                        ONE3xmlrpcHandler one3handle = null;
                        if(oneVersion.startsWith("2.2"))
                        {
                            logger.debug("Creating corresponding ONE account for user " + username + " [" + oneUser + ", " + onePass + "].");
                            onehandle = new ONExmlrpcHandler(oneIp, onePort, oneUser, onePass, "restServerAppControl:OVFaction-deploy");
                        }
                        else if(oneVersion.startsWith("3.4"))
                        {
                            logger.debug("Creating corresponding ONE3 account for user " + username + " [" + oneUser + ", " + onePass + "].");
                            one3handle = new ONE3xmlrpcHandler(oneIp, onePort, oneUser, onePass, "restServerAppControl:OVFaction-deploy");
                        }

                        //now retrieve the diskimages corresponding to the ovfsno and see if
                        //some are already in the ONE system, if they are update the db entries filling in necessary details
                        //for the remaining entries, issue image templates for registration to the ONE front end.
                        rs = db.query("select", "vmid, state", "vmachinetemplate", "where ovfsno=" + ovfid);
                        String vmtemplateState = "ND";
                        String condition = "WHERE ";
                        if(rs.next())
                        {
                            vmtemplateState = rs.getString("state");
                            condition += "vmid=" + rs.getInt("vmid");
                        }
                        if(vmtemplateState.equalsIgnoreCase("IN") || vmtemplateState.equalsIgnoreCase("DP"))
                        {
                            while(rs.next())
                            {
                                condition += " OR vmid=" + rs.getInt("vmid");
                            }
                            rs.close();
                            rs = db.query("select", "id, localpath, name, state, oneimgdescp", "diskimage", condition);
                            int counter = 0;
                            while(rs.next())
                            {
                                counter++;
                                String imgName = rs.getString("name");
                                String imgPath = rs.getString("localpath");
                                String imgState =rs.getString("state");
                                String imgTemplate = rs.getString("oneimgdescp");
                                int imgId = rs.getInt("id");
                                if(!imgState.equalsIgnoreCase("OR")) //if state is ORPHANed then do not do anything, it will be garbage collected soon
                                {
                                    //System.err.println("DEBUG: imgName: " + imgName + " imgPath: " + imgPath + " imgState: " + imgState);
                                    //getting the already registered ONE image list
                                    LinkedList<ONEImage> imageList = null;
                                    if(onehandle != null)
                                        imageList = onehandle.getImageList();
                                    else if(one3handle != null)
                                        imageList = one3handle.getImageList();
                                    
                                    boolean found = false;
                                    for(int i=0; i< imageList.size(); i++)
                                    {
                                        ONEImage img = imageList.get(i);
                                        logger.debug("Got ONE Image Details: " + img.imageName + ", " + img.localPath + ", " + img.state);
                                        if(img.imageName.equalsIgnoreCase(imgName) && img.localPath.equalsIgnoreCase(imgPath))
                                        {
                                            //System.err.println("Comparision Matches, inside if.");
                                            //update the diskimage entry with missing details
                                            boolean status = db.update("diskimage", "oneimgname='" + img.imageName + "', oneimgid=" + img.id + ", state='RG'", "where id=" + imgId);
                                            if(status)
                                            {
                                                logger.debug("For discimage id=" + imgId + " corresponding ONE image with id " + img.id + " was found. VEP-DB updated.");
                                            }
                                            else
                                            {
                                                logger.warn("For discimage id=" + imgId + " corresponding ONE image with id " + img.id + " was found. VEP-DB update failed.");
                                            }
                                            //System.err.println("Setting found=true and beaking.");
                                            found = true;
                                            break;
                                        }
                                    }

                                    if(!found)
                                    {
                                        //issue ONE image register command and store corresponding data
                                        //for now we assume that the VM image resides locally.
                                        int oneImgId = -1;
                                        if(onehandle != null)
                                            oneImgId = onehandle.addImage(imgTemplate);
                                        else if(one3handle != null)
                                            oneImgId = one3handle.addImage(imgTemplate);
                                        
                                        if(oneImgId != -1)
                                        {
                                            ONEImage temp = null;
                                            if(onehandle != null)
                                                temp = onehandle.getImageInfo(oneImgId);
                                            else if(one3handle != null)
                                                temp = one3handle.getImageInfo(oneImgId);
                                            
                                            boolean status = db.update("diskimage", "oneimgname='" + temp.imageName + "', oneimgid=" + temp.id + ", state='RG'", "where id=" + imgId);
                                            if(status)
                                            {
                                                logger.debug("For discimage id=" + imgId + " corresponding ONE image with id " + temp.id + " was registered. VEP-DB updated.");
                                            }
                                            else
                                            {
                                                logger.warn("For discimage id=" + imgId + " corresponding ONE image with id " + temp.id + " was registered. VEP-DB update failed.");
                                            }
                                        }
                                        else
                                        {
                                            logger.warn("Some error while registering image " + imgName + " disk-id: " + imgId + " with ONE.");
                                            //internal error
                                            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                            out.append("Some error while registering the disk image with IaaS controller ...");
                                            out.append("</div>");
                                        }
                                    }
                                }
                                rs = db.query("select", "id, localpath, name, state, oneimgdescp", "diskimage", condition);
                                for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after updates.
                            }
                            rs.close();

                            //now issue start for each Vmachine template entry for this ovfsno
                            rs = db.query("select", "vmid, state, ovfid, appname, descp", "vmachinetemplate", "where ovfsno=" + ovfid);
                            counter = 0;
                            while(rs.next())
                            {
                                counter++;
                                String state = rs.getString("state");
                                String ovfid1 = rs.getString("ovfid");
                                String appname = rs.getString("appname");
                                String template = rs.getString("descp");
                                int vmid = rs.getInt("vmid");
                                if(!state.equalsIgnoreCase("OR")) //ignore OR state as it will be garbage collected soon
                                {
                                    //depending on the SLA placement restrictions, add placement section in the template part before deploying
                                    //ONEVm temp = onehandle.getVmInfo(44);
                                    int onevmid = -1;
                                    if(onehandle != null)
                                        onevmid = onehandle.addVM(template + "\nREQUIREMENTS = \"CLUSTER = contrail\"");
                                    else if(one3handle != null)
                                        onevmid = one3handle.addVM(template + "\nREQUIREMENTS = \"CLUSTER = contrail\"");
                                    
                                    Thread.sleep(2000); //two seconds gap between VM submission as a precaution
                                    if(onevmid != -1)
                                    {
                                        ONEVm temp = null;
                                        if(onehandle != null)
                                            temp = onehandle.getVmInfo(onevmid);
                                        else if(one3handle != null)
                                            temp = one3handle.getVmInfo(onevmid);

                                        //now store in the vmachine table, also change the state of the ovf entry to DP from ND
                                        ResultSet rs1 = db.query("select", "max(id)", "vmachine", "");
                                        int vid = 0;
                                        if(rs1.next()) vid = rs1.getInt(1) + 1;
                                        rs1.close();
                                        boolean status = db.insert("vmachine", "(" + vid + ", " + uid + ", 'OpenNebula', " + temp.id + ", " + vmid + ", '" + temp.name + "', 'DP', -1, '" +
                                                temp.ip + "', '" + temp.graphics_port + "', '" + temp.graphics_ip + "', '')");
                                        if(status)
                                        {
                                            if(one3handle != null)
                                            {
                                                //extra step to deploy the VM
                                                int hid = VEPHostPool.getNextHost();
                                                if(hid != -1)
                                                {
                                                    status = one3handle.deployVM(onevmid, hid);
                                                    if(status)
                                                    {
                                                        logger.debug("VM with ONE3ID:" + onevmid + " was successfully allocated and deployed on machine with ONEhostID: " + hid + ".");
                                                        out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                                                        out.append("Deploying VM:").append(ovfid1).append(" - operation successful!");
                                                        out.append("</div>");
                                                    }
                                                    else
                                                    {
                                                        logger.warn("VM with ONE3ID:" + onevmid + " was successfully allocated but deployment on machine with ONEhostID: " + hid + " FAILED.");
                                                        out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                                        out.append("Deploying VM:").append(ovfid1).append(" - VM was allocated successfully on ONE3, but deployment FAILED!");
                                                        out.append("</div>");
                                                    }
                                                }
                                                else
                                                {
                                                    logger.warn("VM with ONE3ID:" + onevmid + " was successfully allocated but deployment FAILED because of unavailability of host to deploy.");
                                                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                                    out.append("Deploying VM:").append(ovfid1).append(" - VM was allocated successfully on ONE3, but deployment FAILED because of unavailability of host to deploy ....");
                                                    out.append("</div>");
                                                }
                                            }
                                            else if(onehandle != null)
                                            {
                                                logger.debug("Successfully deployed VM information - oneId:" + temp.id + " oneName:" + temp.name 
                                                + " Graphics:" + temp.graphics_type + "-" + temp.graphics_ip + "-" + temp.graphics_port);
                                                out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                                                out.append("Deploying VM:").append(ovfid1).append(" - Operation successful!");
                                                out.append("</div>");
                                            }
                                            db.update("vmachinetemplate", "state='DP'", "where vmid=" + vmid);
                                        }
                                        else
                                        {
                                            //vid : ApplicationName : ovfid : ONE Name : STATE : ONE Id
                                            logger.warn("Failed to store deployed VM information - oneId:" + temp.id + " oneName:" + temp.name 
                                                + " Graphics:" + temp.graphics_type + "-" + temp.graphics_ip + "-" + temp.graphics_port);
                                            db.update("vmachinetemplate", "state='UN'", "where vmid=" + vmid);
                                            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                            out.append("Deploying VM:").append(ovfid1).append(" - VM was allocated on ONE3, but internal VEP state could not be updated properly!");
                                            out.append("</div>");
                                        }
                                    }
                                    else
                                    {
                                        logger.warn("Some error while deploying VM " + vmid + ":" + ovfid1 + " ovf-id: " + ovfid + " with ONE.");
                                        db.update("vmachinetemplate", "state='ER'", "where vmid=" + vmid);
                                        //error
                                        out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                                        out.append("Deploying VM:").append(ovfid1).append(" - Error encountered during VM deployment ...");
                                        out.append("</div>");
                                    }
                                }
                                rs = db.query("select", "vmid, state, ovfid, appname, descp", "vmachinetemplate", "where ovfsno=" + ovfid);
                                for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after updates.
                            }
                            rs.close();
                            db.update("ovf", "state='DP'", "where sno=" + ovfid);  
                        }
                        else
                        {
                            rs.close();
                            //first issue a initialize command
                            logger.warn("Problem deploying the OVF as it was not initialized.");
                            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                            out.append("This application has not been initialized. First initialize and then try again.");
                            out.append("</div>");
                        }
                    }
                    else
                    {
                        logger.warn("Problem resolving one username and password for the user " + username);
                        out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                        out.append("Your account is not configured properly, please contact your system administrator to fix the IaaS translation problem with your account ...");
                        out.append("</div>");
                    }
                }
                else
                {
                    //unauthorized to proceed
                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("You are unauthorized to perform this action ...");
                    out.append("</div>");
                }
            }
            catch(Exception ex)
            {
                //internal error
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Exception caught ...<br>").append(ex.getMessage());
                out.append("</div>");
            }
        }
        else
        {
            //error
            if(uid == -1)
            {
                //error internal
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Internal Server Error ...");
                out.append("</div>");
            }
            else
            {
                //unregistered user found
                logger.debug("User " + username + " does not exist. OVF deployment failed.");
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Username not registered with this provider! First register and then try again ...");
                out.append("</div>");
            }
        }
    }
    
    void performUpdateOVFAction(String username, StringBuilder out, Representation entity)
    {
        Form form = new Form(entity);
        String appname = form.getFirstValue("name");
        String group = form.getFirstValue("group");
        String perm = form.getFirstValue("perm");
        String descp = form.getFirstValue("descp");
        try
        {
            int ovfid = Integer.parseInt(form.getFirstValue("ovfid"));
            out.append("<hr>Hello ").append(username.toUpperCase()).append("! Welcome to your application management interface.<br><br>");
            out.append("You requested modification of an existing application ").append(appname).append(", with group: ").append(group).append(", and permissions: ").append(perm).append("<br><br>");
            if(username == null || descp == null || descp.trim().length() == 0 || appname == null || appname.trim().length() == 0 || descp.contains("Paste OVF description here ..."))
            {
                //error
                out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                out.append("Incorrect or missing data detected in the form entry. Please check your entry and try again ...");
                out.append("</div>");
            }
            else
            {
                //access control already took place earlier so it is safe to just update the ovf
                String updateVal = "";
                if(descp != null && descp.trim().length() > 0)
                {
                    updateVal = "descp='" + descp + "'";
                }
                if(group != null)
                {
                    if(updateVal.length() > 0)
                        updateVal += ", gname='" + group + "'";
                    else
                        updateVal = "gname='" + group + "'";
                }
                if(perm != null)
                {
                    if(updateVal.length() > 0)
                        updateVal += ", perm='" + perm + "'";
                    else
                        updateVal = "perm='" + perm + "'";
                }
                boolean status = db.update("ovf", updateVal, "where sno=" + ovfid);
                if(status)
                {
                    //success
                    out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("Operation successful!");
                    out.append("</div>");
                }
                else
                {
                    //internal error
                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("Internal Server Error ...");
                    out.append("</div>");
                }
            }
        }
        catch(Exception ex)
        {
            logger.warn("Exception caught while updating OVF entry for " + username + ", ovfname= " + appname + ": " + ex.getMessage());
            //exception caught
            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
            out.append("Exception caught ...<br>").append(ex.getMessage());
            out.append("</div>");
        }
        out.append("<table style='border:0px;background:white;font-family:Verdana;font-size:10pt;'>");
        out.append("<tr><td><a href='../'><img src='https://www.cise.ufl.edu/~pharsh/public/next.png' width='24'></a><td>");
        out.append("<td valign='center'>Click the continue icon to proceed.</td></tr></table>");
    }
    
    void performNewOVFAction(String username, StringBuilder out, Representation entity)
    {
        Form form = new Form(entity);
        String appname = form.getFirstValue("appname");
        String group = form.getFirstValue("group");
        String perm = form.getFirstValue("perm");
        String descp = form.getFirstValue("descp");
        out.append("<hr>Hello ").append(username.toUpperCase()).append("! Welcome to your application management interface.<br><br>");
        out.append("You requested registration of a new application ").append(appname).append(", with group: ").append(group).append(", and permissions: ").append(perm).append("<br><br>");
        if(username == null || descp == null || descp.trim().length() == 0 || appname == null || appname.trim().length() == 0 || descp.contains("Paste OVF description here ..."))
        {
            //error
            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
            out.append("Incorrect or missing data detected in the form entry. Please check your entry and try again ...");
            out.append("</div>");
        }
        else
        {
            //for every other parameters if null use default values
            int uid = -1;
            ResultSet rs;
            //now get the uid for the username
            try
            {
                rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                    uid = rs.getInt("uid");
                else
                    uid = -2;
                rs.close();
            }
            catch(Exception ex)
            {
                logger.warn("Exception caught while retrieving UID for user " + username + " : " + ex.getMessage());
            }
            if(uid != -1 && uid != -2)
            {
                try
                {
                    //rs = db.query("select", "*", "ovf", "where uid=" + uid + " AND ovfname='" + ovfname + "'");
                    rs = db.query("select", "*", "ovf", "where ovfname='" + appname + "' AND uid=" + uid);
                    boolean status = false;
                    if(rs.next())
                    {
                        //an entry already exists, can not proceed with this path
                        //error appname conflict
                        out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                        out.append("Conflict detected in the application name field ...");
                        out.append("</div>");
                    }
                    else
                    {
                        if(group == null || (group != null && group.trim().length() == 0)) group = username; //assign defalut group to be same as self
                        if(perm == null || (perm != null && perm.trim().length() == 0)) perm = "700"; //default access rights in the UNIX sense.
                        //states: ND = not yet deployed
                        //find max sno and then increment by i
                        int max_sno = 0;
                        rs = db.query("select", "max(sno)", "ovf", "");
                        if(rs.next())
                        {
                            max_sno = rs.getInt(1);
                            logger.debug("Serial number to be assigned to the ovf: " + max_sno + 1);
                            rs.close();
                        }
                        //  response.put("sno", (max_sno + 1));
                        
                        status = db.insert("ovf", "('" + appname + "', " + (max_sno+1) + ", -1, " + uid + ", 'ND', '" + group + "', '" + perm + "', '" + descp + "')");
                        if(status)
                        {
                            //success
                            out.append("<div style='background:#D7FCE6;font-weight:bold;margin:5px;padding:5px;'>");
                            out.append("Operation successful!");
                            out.append("</div>");
                        }
                        else
                        {
                            //internal error
                            out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                            out.append("Internal Server Error ...");
                            out.append("</div>");
                        }
                    }
                }
                catch(Exception ex)
                {
                    logger.warn("Exception caught while creating/updating OVF entry for " + username + ", ovfname= " + appname + ": " + ex.getMessage());
                    //exception caught
                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("Exception caught ...<br>").append(ex.getMessage());
                    out.append("</div>");
                }
            }
            else
            {
                if(uid == -1)
                {
                    //error internal
                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("Internal Server Error ...");
                    out.append("</div>");
                }
                else
                {
                    //unregistered user found
                    logger.debug("User " + username + " does not exist. OVF entry failed.");
                    out.append("<div style='background:#FFE7C3;font-weight:bold;margin:5px;padding:5px;'>");
                    out.append("Username not registered with this provider! First register and then try again ...");
                    out.append("</div>");
                }
            }
        }
        out.append("<table style='border:0px;background:white;font-family:Verdana;font-size:10pt;'>");
        out.append("<tr><td><a href='../'><img src='https://www.cise.ufl.edu/~pharsh/public/next.png' width='24'></a><td>");
        out.append("<td valign='center'>Click the continue icon to proceed.</td></tr></table>");
        //out.append("<b>Click <a href='../'><img src='https://www.cise.ufl.edu/~pharsh/public/next.png' width='24'></a> to continue ...</b><br><br>");
    }
    
    @Get
    public Representation getValue() throws ResourceException
    {
        //Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        Representation response = null;
        String username = null;
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        X509Certificate Cert = null;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(true));
        
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        else
        {
            if(!SSLCertHandler.isCertValid(Cert))
            {
                generateError(username, stringBuilder, 1);
            }
        }
        
        if(username != null && username.trim().length() > 0)
        {
            String actionType = ((String) getRequest().getAttributes().get("type"));
            if(actionType == null)
            {
                generateHtml(username, stringBuilder);
            }
            else
            {
                stringBuilder.append("<hr><b>WARNING</b> ::: <i>Abnormal routing attempt detected</i> :-( I can not proceed.<br><br>");
            }
        }
        else
        {
            //reportError("NOUSER", stringBuilder);;
            stringBuilder.append("<hr><i>Your browser is probably not configured properly to use your certificate</i>. Please configure your browser properly and try again ...<br><br>");
        }
        stringBuilder.append("Click <a href='../'><img src='https://www.cise.ufl.edu/~pharsh/public/home.png' width='24'></a> to go back to the main page<br>");
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        response = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return response;
    }
    
    void generateError(String username, StringBuilder out, int errorType)
            //errorType 1 = certificate error
            //errotType 2 = no user
    {
        out.append("<hr>Hello ").append(username.toUpperCase()).append("! ");
        out.append("<b>Your certificate is not valid. Unable to process your request correctly.</b><br>");
    }
    
    void generateHtml(String username, StringBuilder out)
    {
        ResultSet rs;
        LinkedList<Integer> vmList = new LinkedList<Integer>();
        LinkedList<Integer> ovfList = new LinkedList<Integer>();
        LinkedList<Integer> templateList = new LinkedList<Integer>();
        
        out.append("<hr>Hello ").append(username.toUpperCase()).append("! Welcome to your application management interface.<br><br>");
        out.append("<table style='width:1014px;hspace:5px;background:silver;border:0px;cellspacing:2px;padding:2px;font-family:Courier;font-size:10pt;color:black;'>");
        out.append("<tr>");
        out.append("<td valign='top' style='width:128px;background:white;'><img style='width:128px;' src='https://www.cise.ufl.edu/~pharsh/public/user.png'>");
        out.append("<td valign='top' align='left' bgcolor='white' width='*'>");
        try
        {
            rs = db.query("select", "*", "user", "where username='" + username + "'");
            if(rs.next())
            {
                int uid = rs.getInt("uid");
                //print list of VMs submitted on behalf of this user
                out.append("<b>List of virtual machines:</b><br>");
                out.append("<div style='background:white;'>");
                out.append("<table border=1 cellpadding=2 cellspacing=2 style=\"border:1px;border-color:#000000;font-family:Courier;font-size:9pt;text-align:right;color:white;\">");
                out.append("<tr><th align=right style=\"background:#333333;\">vmid</th>").append("<th align=right style=\"background:#333333;\">host</th>");
                out.append("<th align=right style=\"background:#333333;\">ovfid</th>");
                out.append("<th align=right style=\"background:#333333;\">application name</th>");
                out.append("<th align=right style=\"background:#333333;\">state</th>").append("<th align=right style=\"background:#333333;\">ovf serial</th>");
                out.append("<th align=right style=\"background:#333333;\">controller</th>").append("<th align=right style=\"background:#333333;\">vm ip address</th>");
                out.append("<th align=right style=\"background:#333333;\">allow vnc from</th>");
                out.append("<th align=right style=\"background:#333333;\">vnc port</th></tr>");

                int counter=0;
                int count = 0;
                rs = db.query("select", "*", "vmachine", "where uid=" + uid + " ORDER BY id DESC");
                String appname = "";
                String ovfid = "";
                int ovfsno = -1;
                while(rs.next())
                {
                    int vmid = rs.getInt("vmid");
                    int host_id = rs.getInt("cid");
                    ResultSet rs1 = db.query("select", "appname, ovfsno, ovfid", "vmachinetemplate", "where vmid=" + vmid + "");
                    if(rs1.next())
                    {
                        appname = rs1.getString("appname");
                        ovfsno = rs1.getInt("ovfsno");
                        ovfid = rs1.getString("ovfid");
                        rs1.close();
                    }
                    else
                    {
                        appname = "";
                        ovfsno = -1;
                        ovfid = "";
                    }
                    rs1.close();
                    //now locating the physical host name
                    String hostName = "UNKNOWN";
                    if(host_id >= 0)
                    {
                        rs1 = db.query("select", "hostname", "computenode", "where cid=" + host_id + "");
                        if(rs1.next())
                        {
                            hostName = rs1.getString("hostname");
                        }
                        rs1.close();
                    }
                    count++;
                    String color = "#FFFFFF";
                    if(count%2 == 1)
                        color = "#CCCCCC";
                    else
                        color = "#FFFFFF";
                    rs = db.query("select", "*", "vmachine", "where uid=" + uid + " ORDER BY id DESC");
                    counter = 0;
                    while(rs.next())
                    {
                        counter++;
                        if(counter < count)
                        {
                            continue;
                        }

                        String td = "<td style=\"background:" + color + ";color:black;font-weight:bold;\">";
                        vmList.add(Integer.valueOf(rs.getInt("id")));
                        String IPAddress = rs.getString("ipaddress");
                        IPAddress = IPAddress.replaceAll(",", "<br>");
                        out.append("<tr>").append(td).append(rs.getInt("id")).append("</td>").append(td).append(hostName).append("</td>").append(td).append(ovfid);
                        out.append("</td>").append(td).append(appname).append("</td>").append(td).append(rs.getString("state")).append("</td>").append(td).append(ovfsno);
                        out.append("</td>").append(td).append(rs.getString("controller")).append("</td>").append(td).append(IPAddress).append("</td>");
                        out.append(td).append(rs.getString("vncip")).append("</td>").append(td).append(rs.getString("vncport")).append("</td></tr>");
                        break;
                    }
                }
                rs.close();
                out.append("</table>");
                out.append("<b>Total VMs found: ").append(count);
                out.append("</b></div>");
                out.append("<br><b>List of registered OVFs:</b><br>");

                out.append("<div style='background:white;'>");
                out.append("<table border=1 cellpadding=2 cellspacing=2 style=\"border:1px;border-color:#000000;font-family:Courier;font-size:9pt;text-align:right;color:white;\">");
                out.append("<tr><th align=right style=\"background:#333333;\">ovf serial</th>").append("<th align=right style=\"background:#333333;\">ovfname</th>");
                out.append("<th align=right style=\"background:#333333;\">ceeid</th>");
                out.append("<th align=right style=\"background:#333333;\">state</th>");
                out.append("<th align=right style=\"background:#333333;\">group</th>").append("<th align=right style=\"background:#333333;\">permissions</th>");
                out.append("<th align=right style=\"background:#333333;\">link</th></tr>");
                count = 0;
                rs = db.query("select", "*", "ovf", "where uid=" + uid + " ORDER BY sno ASC");
                while(rs.next())
                {
                    count++;
                    String color = "#FFFFFF";
                    if(count%2 == 1)
                        color = "#CCCCCC";
                    else
                        color = "#FFFFFF";
                    String td = "<td style=\"background:" + color + ";color:black;font-weight:bold;\">";
                    ovfList.add(Integer.valueOf(rs.getInt("sno")));
                    String url = "<a href='../../ovf/id/" + rs.getInt("sno") + "'>/ovf/id/" + rs.getInt("sno") + "</a>";
                    out.append("<tr>").append(td).append(rs.getInt("sno")).append("</td>").append(td).append(rs.getString("ovfname")).append("</td>");
                    out.append(td).append(rs.getInt("ceeid")).append("</td>");
                    out.append(td).append(rs.getString("state")).append("</td>").append(td).append(rs.getString("gname")).append("</td>");
                    out.append(td).append(rs.getString("perm")).append("</td>").append(td).append(url).append("</td></tr>");
                }
                out.append("</table>");
                out.append("<b>Total registered OVF applications found: ").append(count);
                out.append("</b></div>");
                out.append("<br><b>List of VM templates:</b><br>");
                
                out.append("<div style='background:white;'>");
                out.append("<table border=1 cellpadding=2 cellspacing=2 style=\"border:1px;border-color:#000000;font-family:Courier;font-size:9pt;text-align:right;color:white;\">");
                out.append("<tr><th align=right style=\"background:#333333;\">ovf serial</th>").append("<th align=right style=\"background:#333333;\">template id</th>");
                out.append("<th align=right style=\"background:#333333;\">machine name</th>");
                out.append("<th align=right style=\"background:#333333;\">link</th>");
                count = 0;
                rs = db.query("select", "vmid, ovfsno, ovfid", "vmachinetemplate", "where uid=" + uid + " ORDER BY ovfsno, vmid ASC");
                while(rs.next())
                {
                    count++;
                    String color = "#FFFFFF";
                    if(count%2 == 1)
                        color = "#CCCCCC";
                    else
                        color = "#FFFFFF";
                    String td = "<td style=\"background:" + color + ";color:black;font-weight:bold;\">";
                    templateList.add(Integer.valueOf(rs.getInt("vmid")));
                    String url = "<a href='../../template/" + rs.getInt("vmid") + "'>/template/" + rs.getInt("vmid") + "</a>";
                    out.append("<tr>").append(td).append(rs.getInt("ovfsno")).append("</td>").append(td).append(rs.getInt("vmid")).append("</td>");
                    out.append(td).append(rs.getString("ovfid")).append("</td>");
                    out.append(td).append(url).append("</td></tr>");
                }
                out.append("</table>");
                out.append("<b>Total VM templates found: ").append(count);
                out.append("</b></div>");
            }
            else
            {
                out.append("No data found ...");
            }
        }
        catch(Exception ex)
        {
            out.append("</table><B>SQL Query Error!! Details of exception follows ...</B>");
            out.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
            out.append(ex.getMessage());
            logger.debug("Exception caught: " + ex.getMessage());
            //ex.printStackTrace(System.err);
            out.append("</div>");
        }
        out.append("</table><br>");
        out.append("<table style='width:1014px;hspace:5px;background:silver;border:0px;cellspacing:2px;padding:2px;font-family:Courier;font-size:10pt;color:black;'>");
        out.append("<tr>");
        out.append("<td valign='top' align='center' style='width:128px;background:white;'><img style='width:128px;' src='https://www.cise.ufl.edu/~pharsh/public/template.png'><br><br>");
        out.append("<div id='progressbar' style='visibility:hidden;'><img src='https://www.cise.ufl.edu/~pharsh/public/progress.gif'></div>");
        out.append("<td valign='top' align='left' bgcolor='white' width='*'>");
        out.append("<b>Choose the appropriate items and control option and press <i>submit</i>:</b><br>");
        
        out.append("<div style='background:#E3EDEB;font-weight:normal;margin:5px;'><form name='vm' action='doaction/vm' method='post'>");
        out.append("Choose your VM (id): <select name='vmid'>");
        for(int i=0; i<vmList.size(); i++)
        {
            out.append("<option value='").append(vmList.get(i).toString()).append("'>").append(vmList.get(i).toString()).append("</option>");
        }
        out.append("</select>").append(" Action: <input type='radio' name='vmaction' value='suspend' checked>suspend");
        out.append(" <input type='radio' name='vmaction' value='resume'>resume <input type='radio' name='vmaction' value='stop'>stop <input type='radio' name='vmaction' value='restart'>restart <input type='submit' value='submit' onClick=\"javascript:showstuff('progressbar');\">");
        out.append("</form></div>");
        
        out.append("<div style='background:#E3EDEB;font-weight:normal;margin:5px;'><form name='vm' action='doaction/vmtemplate' method='post'>");
        out.append("Choose your VM Template (id) to deploy: <select name='vmid'>");
        for(int i=0; i<templateList.size(); i++)
        {
            out.append("<option value='").append(templateList.get(i).toString()).append("'>").append(templateList.get(i).toString()).append("</option>");
        }
        out.append("</select>");
        out.append(" <input type='submit' value='deploy' onClick=\"javascript:showstuff('progressbar');\">");
        out.append("</form></div>");
        
        out.append("<div style='background:#E3EDEB;font-weight:normal;margin:5px;'><form name='ovf' action='doaction/ovf' method='post'>");
        out.append("Choose your application (ovf id): <select name='ovfid'>");
        for(int i=0; i<ovfList.size(); i++)
        {
            out.append("<option value='").append(ovfList.get(i).toString()).append("'>").append(ovfList.get(i).toString()).append("</option>");
        }
        out.append("</select>").append(" Action: <input type='radio' name='ovfaction' value='initialize' checked>initialize");
        out.append(" <input type='radio' name='ovfaction' value='deploy'>deploy <input type='radio' name='ovfaction' value='stop'>stop");
        out.append(" <input type='radio' name='ovfaction' value='edit'>edit <input type='radio' name='ovfaction' value='delete'>delete <input type='submit' value='submit' onClick=\"javascript:showstuff('progressbar');\">");
        out.append("</form></div><b>Create a new application</b>:");
        
        out.append("<div style='background:#E3EDEB;font-weight:normal;margin:5px;'><form name='newovf' action='doaction/newovf' method='post'>");
        out.append("Application Name: <input type='text' name='appname' size='32'><br>Group: <input type='text' name='group'> Permission: <input type='text' name='perm' size='3'><br>");
        out.append("Paste the OVF content below:<br><textarea name='descp' rows='10' cols='85'>Paste OVF description here ...</textarea><br>");
        out.append("<input type='submit' value='submit' onClick=\"javascript:showstuff('progressbar');\">");
        out.append("</form></div>");
        
        out.append("</table><br>");
    }
}
