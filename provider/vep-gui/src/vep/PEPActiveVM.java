package vep;
/*
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import org.ow2.contrail.authorization.cnr.pep.PepCallback;
import org.apache.axis2.context.MessageContext;
import org.apache.log4j.Logger;
import org.ow2.contrail.authorization.cnr.pep.PEP;*/

/**
 *
 * @author contrail
 */
/*
public class PEPActiveVM extends PepCallback{
    
  private Thread sessionExecutorThread;
    private String ovfsno;
    
    
    public PEPActiveVM(PEP pep, String ovfsno) {
        super(pep);
        this.ovfsno = ovfsno;
    }

    
    public void onRevokeAccess(){
        Logger logger = Logger.getLogger("VEP.PEPActiveVM");;
        try {
            //shutdown vms
                String vepProperties = VEPHelperMethods.getPropertyFile();
                String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
                String oneIp = VEPHelperMethods.getProperty("one.ip", logger, vepProperties);
                String onePort = VEPHelperMethods.getProperty("one.port", logger, vepProperties);
                String oneVersion = VEPHelperMethods.getProperty("one.version", logger, vepProperties);
                String oneAdmin = VEPHelperMethods.getProperty("one.user", logger, vepProperties);
                String onePass = VEPHelperMethods.getProperty("one.pass", logger, vepProperties);
                dbHandler db = new dbHandler("restServerOVFdetails", dbType);   
                ResultSet rs;
                ONExmlrpcHandler onehandle = null;
                ONE3xmlrpcHandler one3handle = null;
                //shutdown on behalf of admin
                if(oneVersion.startsWith("2.2"))
                {
                    onehandle = new ONExmlrpcHandler(oneIp, onePort, oneAdmin, onePass, "restServerOVFDetails:OVFaction-stop");
                }
                else if(oneVersion.startsWith("3.4"))
                {
                    one3handle = new ONE3xmlrpcHandler(oneIp, onePort, oneAdmin, onePass, "restServerOVFDetails:OVFaction-stop");
                }
                rs = db.query("select", "vmid", "vmachinetemplate", "where ovfsno=" + ovfsno);
                int counter = 0;
                while(rs.next())
                {
                    counter++;
                    int vmid = rs.getInt("vmid");
                    //now reset this vmachine template to IN
                    db.update("vmachinetemplate", "state='IN'", "where vmid=" + vmid);
                    //get list of all VMs corresponding to this vmid
                    ResultSet rs1 = db.query("select", "id, onevmid, state", "vmachine", "where vmid=" + vmid);
                    while(rs1.next())
                    {
                        int oneid = rs1.getInt("onevmid");
                        String state = rs1.getString("state");
                        if(state.equalsIgnoreCase("UN") || state.equalsIgnoreCase("RN") || state.equalsIgnoreCase("BT"))
                        {
                            if(onehandle != null)
                                onehandle.shutdownVM(oneid);
                            else if(one3handle != null)
                                one3handle.shutdownVM(oneid);
                            //wait for 5 seconds before sending the next shutdown command
                            sessionExecutorThread.sleep(5000);
                        }
                    }
                    rs1.close();
                    rs = db.query("select", "vmid", "vmachinetemplate", "where ovfsno=" + ovfsno);
                    for(int i=0;i<counter; i++) rs.next(); //hack to overcome rs being closed after db operations.
                }
                rs.close();
        } catch (SQLException ex) {
            logger.warn("Database connection error while trying to revoke application "+ovfsno+" following PEP revocation.");
        }
          catch (InterruptedException ex){
            logger.warn("Thread interrupt exception while trying to revoke application "+ovfsno+" following PEP revocation.");
        }

    }
    
    public void onEndAccess(){}

    
}
*/