/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;
import eu.contrail.security.DelegatedCertClient;
import eu.contrail.security.SecurityUtils;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import javax.security.auth.x500.X500Principal;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
/**
 *
 * @author piyush
 */
public class SSLCertHandler 
{
    private static Logger logger;
    private static DelegatedCertClient client;
    private static String[] safeCAList = {"INRIA-Myriads CA"};
    public SSLCertHandler()
    {
        logger = Logger.getLogger("VEP.CertHandler");
        Security.addProvider(new BouncyCastleProvider());
        String uri = VEPHelperMethods.getProperty("caservice.uri", logger, VEPHelperMethods.getPropertyFile());
        String keyStoreCertFile = VEPHelperMethods.getProperty("caservice.certificate", logger, VEPHelperMethods.getPropertyFile());
        String keyStorePass = VEPHelperMethods.getProperty("caservice.storepass", logger, VEPHelperMethods.getPropertyFile());
        String keyStoreFile = VEPHelperMethods.getProperty("caservice.keystore", logger, VEPHelperMethods.getPropertyFile());
        //System.out.println("CLIENT CA->"+keyStoreCertFile+"\n"+ keyStorePass+"\n"+keyStoreFile+"\n"+uri);
        
        
        try
        {
            client = new DelegatedCertClient(uri, true, keyStoreCertFile, keyStorePass, keyStoreFile);
            logger.info("CLIENT ............................."+client);
        }
        catch(Exception ex)
        {
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            client = null;
        }
    }
    
    public static KeyPair generateKeyPair()
    {
        try
        {
            return SecurityUtils.generateKeyPair("RSA", 2048);
        }
        catch(NoSuchAlgorithmException nalgex)
        {
            if(logger.isDebugEnabled())
                nalgex.printStackTrace(System.err);
            return null;
        }
    }
    
    public static X509Certificate generateCertificate(KeyPair kpair, String algo, String uuid)
    {
        if(client != null)
        {
            try
            {   
                return client.getCert(kpair, algo, uuid, true);
            }
            catch(Exception ex)
            {
                if(logger.isDebugEnabled())
                    System.out.println("ERRORE FILIPPO");
                    ex.printStackTrace(System.err);
                return null;
            }
        }
        else
            return null;
    }
    
    public static boolean storeCertificate(X509Certificate cert, String certFile)
    {
        try
        {
            
            SecurityUtils.writeCertificate(cert, certFile);
            return true;
        }
        catch(Exception ex)
        {
            if(logger.isDebugEnabled())
               ex.printStackTrace();
                //ex.printStackTrace(System.+ "..... " +ex.);
            return false;
        }
    }
    
    public static boolean storeKeyPair(KeyPair kpair, String keyFile)
    {
        //SecurityUtils.writeKeyPair(keyFile, kpair, keyPassphrase, "RSA");
        return true;
    }
    
    public static String getCertDetails(X509Certificate cert, String type)
    {
        String certName = cert.getSubjectX500Principal().getName();
        String uuid = "";
        String CN = "";
        try
        {
            Collection subjectAlternativeNames = cert.getSubjectAlternativeNames();
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    CN = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("Certificate CN field : " + CN);
            if(subjectAlternativeNames.size() > 0)
            {
                Iterator list = subjectAlternativeNames.iterator();
                while(list.hasNext())
                {
                    try
                    {
                        Object name = list.next();
                        uuid = name.toString();
                        logger.debug("Subject's Alternative Name: " + uuid);
                    }
                    catch(Exception ex)
                    {
                        if(logger.isDebugEnabled()) ex.printStackTrace(System.err);
                        return null;
                    }
                }
            }
            logger.debug("The certificate is valid between " + cert.getNotBefore() + " and " + cert.getNotAfter());
        }
        catch(Exception ex)
        {
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
                return null;
        }
        if(type.equalsIgnoreCase("cn"))
            return CN;
        else if (type.equalsIgnoreCase("uuid"))
            return uuid;
        else
            return null;
    }
    
    public static boolean isCertValid(X509Certificate cert)
    {
        X500Principal issuer = cert.getIssuerX500Principal();
        String issuerName = issuer.getName();
        String[] parts = issuerName.split(",");
        String CN = "";
        for(int i=0; i< parts.length; i++)
        {
            if(parts[i].startsWith("CN="))
            {
                CN = parts[i].split("=")[1];
                break;
            }
        }
        boolean caCheck = false;
        for(int i=0; i< safeCAList.length; i++)
        {
            if(safeCAList[i].contentEquals(CN))
            {
                caCheck = true;
                break;
            }
        }
        try
        {
            cert.checkValidity();
        }
        catch(CertificateExpiredException cee)
        {
            logger.error("The certificate is not valid.");
            return false;
        }
        catch(CertificateNotYetValidException cnyve)
        {
            logger.warn("The certificate is not valid yet.");
            return false;
        }
        logger.debug("The certificate was issued by CA: " + CN);
        return caCheck;
    }
}