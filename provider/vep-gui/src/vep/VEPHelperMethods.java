/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

//import org.ow2.contrail.provider.cnr_pep_java.Attribute;
//import org.ow2.contrail.provider.cnr_pep_java.PEP_callout;
//import org.ow2.contrail.provider.cnr_pep_java.XACMLType;
/*
import org.ow2.contrail.authorization.cnr.pep.PEP;
import org.ow2.contrail.authorization.cnr.utils.pep.PepRequestAttribute;
import org.ow2.contrail.authorization.cnr.pep.PEP_callout;
import org.ow2.contrail.authorization.cnr.utils.UconConstants;
import org.ow2.contrail.authorization.cnr.utils.UconConstants.Category;*/
/**
 * Provides several useful helper functions to rest of the application
 * 
 * @author piyush
 */
public class VEPHelperMethods 
{
    private static String vepProperties;
    private static Logger logger;
    
  /*  private static PEP pep = null;
    private static String pdp_endpoint = "http://146.48.96.76:8080/axis2/services/UconWs";
    private static String pep_host = "http://localhost";
    private static String pep_port = "3020";
*/
 
    
    /**
     * initializes the VEPHelperMethods object
     * 
     * @param vepProp   the full path of the VEP properties file
     */
    VEPHelperMethods(String vepProp)
    {
        vepProperties = vepProp;
        logger = Logger.getLogger("VEP.HelperMethods");
    }
    
    /**
     * Retrieves the VEP properties file path
     * 
     * @return      properties file path
     */
    public static String getPropertyFile()
    {
        return vepProperties;
    }
    
    /**
     * Performs an external authorization check for the call
     * 
     * @param uid       VEP user id
     * @param ovfsno    OVF serial number
     * @return          boolean true/false depending on whether the authorization check passed or failed
     * @throws SQLException 
     */
   /* public static boolean pdpCheck(int uid, String ovfsno) throws SQLException
    {
        String pdp = VEPHelperMethods.getProperty("pdp.use", logger, VEPHelperMethods.getPropertyFile());
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        dbHandler db = new dbHandler("VEPHelperMethods", dbType); 
        
        String activeVM = "0";
        PEP_callout pep_callout;
        
        List<Attribute> accessRequest = new ArrayList<Attribute>();
        String issuer = "CNR-Federation";
        //resource attributes
        Attribute attrResource = new Attribute(
                "urn:oasis:names:tc:xacml:1.0:resource:resource-id",
                XACMLType.STRING,
                "VMTemplate",
                issuer,
                Attribute.RESOURCE);
        accessRequest.add(attrResource);
        
        boolean usepdp = false;
        if(pdp != null)
            try
            {
                usepdp = Boolean.parseBoolean(pdp);
            }
            catch(Exception e)
            {
                usepdp = false;
            }
        else
            usepdp = false;
        
        /////////////////////////////////////temporary solution to disable PDP
        usepdp = false;
        
        if(!usepdp)
            return true;
        else
        {
            logger.debug("External access control through PDP module has been enabled.");
            if(ovfsno != null)
            {
                int ovfVM = 0;
                //Adding the number of VMs from the OVF to the total count, if there are more VMs defined in the OVF than the max number of VM allowed in PEP,
                //       VEP doesn't allow the instantiation of this OVF even if no VMs are active yet.
                ResultSet rs = db.query("select", "count(*) as no", "vmachinetemplate", "where ovfsno=" + ovfsno);
                if(rs.next())
                    ovfVM = rs.getInt("no");
                rs = db.query("select", "count(*) as no", " vmachine", "where uid="+uid+" and (state='UN' OR state='RN' OR state='PR' OR state='DP')");
                if(rs.next())
                    activeVM = String.valueOf(rs.getInt("no")+ovfVM-1);//-1, otherwise doesn't allow an ovf of 2 VMs to start even with no active VM
                else
                    activeVM = String.valueOf(ovfVM);
            }
            else
            {
                ResultSet rs = db.query("select", "count(*) as no", " vmachine", "where uid="+uid+" and (state='UN' OR state='RN' OR state='PR' OR state='DP')");
                if(rs.next())
                {
                    activeVM = String.valueOf(rs.getInt("no"));
                }
                else
                    activeVM = "0";
            }
            
            attrResource = new Attribute(
                    "urn:contrail:names:vep:resource:num-vm-running",
                    XACMLType.INT,
                    activeVM,
                    issuer,
                    Attribute.RESOURCE);
            accessRequest.add(attrResource);
            
            //action attributes
            Attribute attrAction = new Attribute(
                    "urn:contrail:vep:action:id",
                    XACMLType.STRING,
                    "deploy",
                    issuer,
                    Attribute.ACTION);
            accessRequest.add(attrAction);
            
            // Allocate PDP end point reference
            String pdp_endpoint = VEPHelperMethods.getProperty("pdp.endpoint", logger, VEPHelperMethods.getPropertyFile());
            if (pdp_endpoint == null)
                pdp_endpoint = "http://146.48.96.75:2000/contrailPDPwebApplication/contrailPDPsoap";
            pep_callout = new PEP_callout(pdp_endpoint);
            
            //invoke PDP for the access decision
            boolean accessDecision =  pep_callout.isPermit(accessRequest);
            // if true, the deploy action shoul be executed and denied otherwise 

            return accessDecision;
        }
    }*/
    
  /*  public static boolean usePDP(){      
        String pdp = VEPHelperMethods.getProperty("pdp.use", logger, VEPHelperMethods.getPropertyFile());
        boolean usepdp = false;
        if(pdp != null)
            try
            {
                usepdp = Boolean.parseBoolean(pdp);
            }
            catch(Exception e)
            {
                usepdp = false;
            }
        else
            usepdp = false;
        return usepdp;
    }
    
    public static PEP createPEP() throws AxisFault{
        //PEP is a static connector to an endpoint, we should be able to connect once for all users of vep and store the result
        if(pep == null)
            pep = new PEP(pdp_endpoint, pep_port, pep_host);
        return pep;
    }
    
    public static boolean pdpCheckVMDeployment(int uid, String ovfsno, String certificateUid) throws SQLException, AxisFault{
        boolean accessDecision = false;
        
        String dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        dbHandler db = new dbHandler("VEPHelperMethods", dbType); 
        String activeVM = "0";
        
        if(!usePDP())
            accessDecision = true;
        else
        {
        
            String type = UconConstants.XML_STRING;
            String issuer = "CNR-Federation";
            String SUBJECT_ATTR_ID = "urn:contrail:names:federation:subject:name";
            String GROUP_ATTR_ID = "urn:contrail:names:federation:subject:group";
            String RESOURCE_ATTR_ID = "urn:oasis:names:tc:xacml:1.0:resource:resource-id";
            String ACTION_ATTR_ID = "urn:contrail:vep:action:id";

            String subject_id_value = certificateUid; 
            if(subject_id_value.equals(""))
                subject_id_value = "1";//temp bug correction

            String subject_group_value = "silver";
            String resource_value = "OVFAPPLICATION";
            String action_value = "deploy";

            // prepare a list of attribute for the request
            List<PepRequestAttribute> req = new ArrayList<PepRequestAttribute>();
            PepRequestAttribute sub = new PepRequestAttribute(SUBJECT_ATTR_ID, type, subject_id_value, issuer, Category.SUBJECT);
            req.add(sub);
            PepRequestAttribute grup = new PepRequestAttribute(GROUP_ATTR_ID, type, subject_group_value, issuer, Category.SUBJECT);
            req.add(grup);
            PepRequestAttribute res = new PepRequestAttribute(RESOURCE_ATTR_ID, type, resource_value, issuer, Category.RESOURCE);
            req.add(res);
            PepRequestAttribute act = new PepRequestAttribute(ACTION_ATTR_ID, type, action_value, issuer, Category.ACTION);
            req.add(act);
        
          
            logger.debug("External access control through PDP module has been enabled.");
            if(ovfsno != null)
            {
                int ovfVM = 0;
                //Adding the number of VMs from the OVF to the total count, if there are more VMs defined in the OVF than the max number of VM allowed in PEP,
                //       VEP doesn't allow the instantiation of this OVF even if no VMs are active yet.
                ResultSet rs = db.query("select", "count(*) as no", "vmachinetemplate", "where ovfsno=" + ovfsno);
                if(rs.next())
                    ovfVM = rs.getInt("no");
                rs = db.query("select", "count(*) as no", " vmachine", "where uid="+uid+" and (state='UN' OR state='RN' OR state='PR' OR state='DP')");
                if(rs.next())
                    activeVM = String.valueOf(rs.getInt("no")+ovfVM-1);//-1, otherwise doesn't allow an ovf of 2 VMs to start even with no active VM
                else
                    activeVM = String.valueOf(ovfVM);
            }
            else
            {
                ResultSet rs = db.query("select", "count(*) as no", " vmachine", "where uid="+uid+" and (state='UN' OR state='RN' OR state='PR' OR state='DP')");
                if(rs.next())
                {
                    activeVM = String.valueOf(rs.getInt("no"));
                }
                else
                    activeVM = "0";
            }
                       
            PEP_callout pep_callout;

            PEP pep_o = createPEP();
            pep_callout = new PEP_callout(pep_o);
            
            accessDecision = pep_callout.tryaccess(req);

            if(accessDecision)
            {
                //updating pep_callout with ovf id to identify this object later
                pep_callout.mapId(ovfsno);
            }
            
            
            
        }
        
        return accessDecision;
    }
    
    public static void startaccessPDP(int ovfsno) {
        try {
            PEP pep = createPEP();
            //create callback object
            PEPActiveVM callbackVM = new PEPActiveVM(pep, String.valueOf(ovfsno));
            //create callout with ovfsno
            PEP_callout pep_callout = new PEP_callout(pep,  String.valueOf(ovfsno));
            //issue start access
            pep_callout.startaccess(callbackVM);
        } catch (AxisFault ex) {
            java.util.logging.Logger.getLogger(VEPHelperMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    */
    
    public static boolean pdpCheckVMDeployment(int uid, String ovfsno, String certificateUid){
        return true;
    }
    
    
    /**
     * Hashes the input string into SHA1 hash
     * 
     * @param input     the input string to hash
     * @return          the hex encoded SHA1 hash of the input
     * @throws NoSuchAlgorithmException 
     */
    public static String makeSHA1Hash(String input) throws NoSuchAlgorithmException
    {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.reset();
            byte[] buffer = input.getBytes();
            md.update(buffer);
            byte[] digest = md.digest();

            String hexStr = "";
            for (int i = 0; i < digest.length; i++) 
            {
                    hexStr +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
            }
            return hexStr;
    }
    
    /**
     * adds a new key:value property into the VEP properties file
     * 
     * @param key       the name of the key
     * @param val       the value of the key
     * @param logger    the logger reference from the calling environment
     * @param fileName  the full path of the VEP properties file
     * @return 
     */
    public static boolean addProperty(String key, String val, Logger logger, String fileName)
    {
        Properties props = new Properties();
        try
        {
            logger.trace("Loading system properties into memory.");
            FileInputStream fis = new FileInputStream(fileName);
            FileOutputStream fos = null;
            props.load(fis);
            logger.trace("System properties successfully loaded.");
            props.setProperty(key, val);
            logger.trace("Adding/modifying system property [key=" + key + ", value=" + val + "]");
            fis.close();
            props.store((fos = new FileOutputStream(fileName)), "Author: Piyush Harsh");
            fos.close();
            logger.trace("Stored system properties back to file successfully.");
        }
        catch(Exception ex)
        {
            logger.warn("Unable to write back system properties file.");
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
            return false;
        }
        return true;
    }
    
    /**
     * Retrieves the key value from the VEP properties file
     * 
     * @param key       the key name to be retrieved
     * @param logger    the logger reference from the calling environment
     * @param fileName  the full VEP properties file path
     * @return          the value for the requested key
     */
    public static String getProperty(String key, Logger logger, String fileName)
    {
        String result = null;
        Properties props = new Properties();
        try
        {
            if(logger!=null && logger.isTraceEnabled())
                logger.trace("Trying to read system properties file.");
            FileInputStream fis = new FileInputStream(fileName);
            props.load(fis);
            result = props.getProperty(key);
            fis.close();
            if(logger!=null && logger.isTraceEnabled())
                logger.trace("Closing system properties file. [key=" + key + ", value=" + result + "].");
            else
            {
                if(logger == null)
                    System.out.println("LOGGER NOT YET INITIALIZED: Closing system properties file. [key=" + key + ", value=" + result + "].");
            }
        }
        catch(Exception ex)
        {
            result = null;
            if(logger!=null && logger.isEnabledFor(Priority.WARN))
                logger.warn("Error accessing system properties file.");
            if(logger!=null && logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else if(logger != null)
                logger.warn(ex.getMessage());
            else
                ex.printStackTrace(System.out);
        }
        return result;
    }
    
    /**
     * It formats a long sentence to wrap into a desired width
     * 
     * @param text      The text to be wrapped
     * @param len       the character width size to wrap into
     * @param wordWrap  true for enabling word-wrap
     * @return          array of String containing the wrapped text
     */
    public static String[] wrapText(String text, int len, boolean wordWrap)
    {
      // return empty array for null text
      if(text == null)
        return new String[] {};

      // return text if len is zero or less
      if(len <= 0)
        return new String[] {text};

      // return text if less than length
      if(text.length() <= len)
        return new String[] {text};

      char [] chars = text.toCharArray();
      ArrayList lines = new ArrayList();
      StringBuilder line = new StringBuilder();
      StringBuffer word = new StringBuffer();

      for(int i = 0; i < chars.length; i++)
      {
        word.append(chars[i]);

        if(wordWrap && chars[i] == ' ')
        {
          if((line.length() + word.length()) > len)
          {
            lines.add(line.toString());
            line.delete(0, line.length());
          }
          line.append(word);
          word.delete(0, word.length());
        }
        else if(!wordWrap)
        {
            if(word.length() == len)
            {
                lines.add(word.toString());
                word.delete(0, word.length());
            }
        }
      }

      // handle any extra chars in current word
      if (word.length() > 0)
      {
        if((line.length() + word.length()) > len)
        {
          lines.add(line.toString());
          line.delete(0, line.length());
        }
        line.append(word);
      }
      // handle extra line
      if (line.length() > 0)
      {
        lines.add(line.toString());
      }

      String[] ret = new String[lines.size()];
      for(int i =0; i<lines.size(); i++)
      {
          ret[i] = (String)lines.get(i);
      }
      return ret;
    }
    
    /**
     * This methods returns the HTML code that generates the REST web-interface header
     * 
     * @param firstPage     boolean specifying whether the page is the home page or not
     * @return          String containing the HTML code for the header
     */
    public static String getRESTwebHeader(boolean firstPage)
    {
        String response = "";
        response += "<html>\n" +
        "<head>\n" +
        "<title>Contrail: Virtual Execution Platform</title>\n" +
        "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n" +
        "<META HTTP-EQUIV=\"Expires\" CONTENT=\"-1\">\n" +
        "<style type=\"text/css\">\n" +
        "   A:link {color: #999999; text-decoration: none}\n" +
        "   A:visited {color: #333333; text-decoration: none}\n" +
        "   A:active {color: #333333; text-decoration: none}\n" +
        "   A:hover {color: #999999; font-weight:bold; color: #000000;}\n" +
        "</style>\n" +
        "<script type=\"text/javascript\">\n" +
        "<!--\n" +
        "function showstuff(boxid){\n" +
        "   document.getElementById(boxid).style.visibility=\"visible\";\n" +
        "}\n" +
        "function hidestuff(boxid){\n" +
        "   document.getElementById(boxid).style.visibility=\"hidden\";\n" +
        "}\n" +
        "function setContentHeight()\n" +
        "{\n" +
        "   var viewportwidth;\n" +
        "   var viewportheight;\n" +
        "   // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight\n" +
        "   if (typeof window.innerWidth != 'undefined')\n" +
        "   {\n" +
        "       viewportwidth = window.innerWidth,\n" +
        "       viewportheight = window.innerHeight\n" +
        "   }\n" +
        "   // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)\n" +
        "   else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)\n" +
        "   {\n" +
        "       viewportwidth = document.documentElement.clientWidth,\n" +
        "       viewportheight = document.documentElement.clientHeight\n" +
        "   }\n" +
        "   // older versions of IE\n" +
        "   else\n" +
        "   {\n" +
        "       viewportwidth = document.getElementsByTagName('body')[0].clientWidth,\n" +
        "       viewportheight = document.getElementsByTagName('body')[0].clientHeight\n" +
        "   }\n" +
        "   if(viewportheight - 370 > 240)\n" +
        "       document.getElementById('content').style.height = (viewportheight - 370) + 'px';\n" +
        "   else\n" +
        "       document.getElementById('content').style.height = '240px';\n" +
        "}\n\n" +
        "function goBack()\n" +
        "{\n" +
        "   window.history.back();\n" +
        "}\n" +
        "//-->\n" +
        "</script>\n" +
        "</head>\n\n" +
        "<body style=\"background:#CED1D6;\" >\n\n" +
        //"<body style=\"background:#CCCCCC;\" onResize=\"javascript:setContentHeight();\" onLoad=\"javascript:setContentHeight();\">\n\n" + 
       

        "<div id=\"header\" style=\"width:1024px;margin:0 auto;background:#333333;color:#CCCCCC;font-family:Arial;font-size:10pt;padding:10px;\">\n";
        if(firstPage)
            response += "<img src=\"https://www.cise.ufl.edu/~pharsh/public/banner.png\">\n";
        else
            response += "<img src=\"https://www.cise.ufl.edu/~pharsh/public/banner.png\" onClick=\"javascript:goBack();\">\n";
        response += "</div>\n\n" +

        "<div id=\"content\" style=\"width:1024px;margin:0 auto;background:white;color:#000000;font-family:Arial;font-size:10pt;padding:10px;overflow:auto;\">\n" +
        "<b>Welcome to the Virtual Execution Platform REST Web-Interface</b><br><br>\n" +
        "<i>Click on the links displayed below to retrieve information on VEP resources. Certain resources will allow you to perform actions on them, for other resources " +
        "only a GET operation is allowed from this web interface. For more complete feature access you are advised to use a full-featured REST client.</i><br><br>";   
        return response;
    }
    
    /**
     * generates the REST web interface footer code
     * 
     * @return      string containing the HTML code for the footer
     */
    public static String getRESTwebFooter()
    {
        String response = "";
        response += "</div>\n\n" +

        "<div id=\"footer\" style=\"width:1024px;margin:0 auto;background:#333333;color:#CCCCCC;font-family:Arial;font-size:10pt;padding:10px;\">\n" +
        "<table style=\"border:0px;padding:0px;color:#FFFFFF;font-family:Arial;font-size:9pt;\">\n" +
        "<tr>\n" +
        "<td><img src=\"https://www.cise.ufl.edu/~pharsh/public/logo-footer.png\"></td>\n" + 
        "<td style=\"width:440px;text-align:justify;border-right:1px;border-left:0px;border-top:0px;border-bottom:0px;border-color:#FFFFFF;\" valign=\"top\">\n" +
        "This software is released under BSD license and is free to use. Contrail project is funded by European Commission under FP7 257438 directive.\n" +
        "The source code for Contrail VEP software can be downloaded from \n" +
        "<a href=\"http://websvn.ow2.org/listing.php?repname=contrail&path=%2Ftrunk%2Fprovider%2Fsrc%2Fvep%2F\" style=\"text-decoration:none;color:#CCCCFF;\" target=\"_blank\">OW2 repository</a>.\n" +
        "<br><br><font style=\"font-size:8pt;color:#CCCCCC;\">VEP REST Web-Interface and the software has been designed by Piyush Harsh with inputs from Florian Dudouet, Ales Cernivec, and Yvon Jegou.</font>\n" +
        "</td>\n" +
        "<td valign=\"top\">\n" +
        "   <table style=\"width:160px;border-left:1px;border-right:1px;border-top:0px;border-bottom:0px;border-style:dashed;border-color:#999999;\">\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Contrail consortium</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Contact us</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Contribute to VEP</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Features release timeline</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Documentation</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">VEP Wiki</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Download VEP</td></tr>\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">Key developers</td></tr>\n" +
        "   </table>\n" +
        "</td>\n" +
        "<td valign=\"top\">\n" +
        "   <table style=\"width:160px;\">\n" +
        "   <tr><td style=\"color:#CCCCCC;font-family:Arial;font-size:9pt;\">WP5 Deliverables</td></tr>\n" +
        "   </table>\n" +
        "</td>\n" +
        "</table>\n" +
        "</div>\n\n" +

        "</body>\n" +
        "</html>";
        return response;
    }
}
