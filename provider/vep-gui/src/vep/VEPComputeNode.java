/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author piyush
 */
public class VEPComputeNode 
{
    public int mem;
    public int no_cpu_cores;
    public long cpu_speed;
    public int hostID;
    public String cpuArch;
    public String rackId;
    public String virtualization;
    public String hostname;
    public String clusterManagerType;
    public String virtualOrgId;
    public String description;
    public boolean operationStatus;
    
    public void printInfo()
    {
        System.out.println("Printing the VEPComputeNode object details:");
        System.out.println("Host ID: " + hostID);
        System.out.println("Max Memory: " + mem);
        System.out.println("Number CPU cores (x100): " + no_cpu_cores);
        System.out.println("CPU Speed (MHz): " + cpu_speed);
        System.out.println("CPU Arch: " + cpuArch);
        System.out.println("Virtualization: " + virtualization);
        System.out.println("HostName: " + hostname);
        System.out.println("Description: " + description);
        System.out.println("Rack ID: " + rackId);
        System.out.println("Virtual Organization ID: " + virtualOrgId);
        System.out.println("Cluster Manager: " + clusterManagerType);
        System.out.println("--------------------");
    }
    
    public boolean checkAction(dbHandler handle) throws SQLException
    {
        ResultSet rs = handle.query("SELECT", "cid, hostname, cpuarch", "computenode", "where hostname='" + hostname + "'");
        if(rs.next())
            return true;
        else
            return false;
    }
    
    public void update(dbHandler handle, Logger logger, JTextArea logA) throws SQLException
    {
        //To not test null each time we want to show some log in the text area
        Boolean logCheck = false;
        if (logA != null)
            logCheck = true;
        
        //the host is already under federation control, I should update the details
        ResultSet rs = handle.query("SELECT", "cid, hostname, cpuarch", "computenode", "where hostname='" + hostname + "'");    
        if(rs.next())
        {
            int cid = rs.getInt("cid");
            logger.trace("Host " + hostname + " already under federation control. Initiating update.");
            //update the details
            
            String updateList = "";
            updateList += "mem=" + mem + ", cpu=" + no_cpu_cores + ", rid=" + rackId + ", virt='" + virtualization + "'";
            updateList += ", clmgrtype='" + clusterManagerType + "', vid='" + virtualOrgId + "', descp='" + description + "'";
            updateList += ", cpuarch='" + cpuArch + "', cpufreq=" + cpu_speed;
            boolean status = handle.update("computenode", updateList, "where cid=" + cid);
            if(status)
            {
                if(logCheck)
                    logA.append("Computenode " + hostname + " was updated successfully.\n");
                status = handle.update("clmanager", "hostid='" + hostID + "'", "where cid=" + cid);
                if(status)
                {
                    logger.trace("Host " + hostname + " already under federation control. Update successful.");
                    if(logCheck)
                        logA.append("Computenode " + hostname + "'s clmanager entry was updated successfully too.\n");
                    operationStatus = true;//Changed to true to re-add host to contrail even in case of updates.
                }
                else
                {
                    logger.error("Host " + hostname + " already under federation control. Partial update. Error while updating clmanager table.");
                }
            }
            else
            {
                logger.error("Host " + hostname + " already under federation control. Update failed. Error while updating computenode table.");
            }
        }
        
    }
    
    public void insert(dbHandler handle, Logger logger, JTextArea logA) throws SQLException
    {
        //To not test null each time we want to show some log in the text area
        Boolean logCheck = false;
        if (logA != null)
            logCheck = true;
        
        ResultSet rs = handle.query("select", "max(cid)", "computenode", "");
        int cid = 0;
        if(rs.next())
            cid = rs.getInt(1) + 1;
        logger.debug("New computenode table entry. ID to be assigned: " + cid);
        boolean status = handle.insert("computenode", "(" + cid + ", " + mem + ", " + no_cpu_cores + ", " + cpu_speed + ", '" + cpuArch + "', " + rackId + ", '" + 
                            virtualization + "', '" + hostname + "', '" + clusterManagerType + "', '" + virtualOrgId + "', '" + description + "')");
                       
        if(status)
        {
            if(logCheck)
                logA.append("Computenode " + hostname + " was updated successfully.\n");
            status = handle.insert("clmanager", "(" + cid + ", '" + hostID + "')");
            if(status)
            {
                if(logCheck)
                        logA.append("Computenode " + hostname + "'s clmanager entry was updated successfully too.\n");
                logger.trace("Host " + hostname + " brought successfully under federation control.");
                operationStatus = true;
            }
            else
            {
                logger.error("Host " + hostname + " brought partially under federation control. Error while adding entry to clmanager table.");
            }
        }
        else
        {
            logger.error("Host " + hostname + " could not be put under federation control. Error while adding entry to computenode table.");
        }
    }
}
