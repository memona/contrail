/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
/**
 *
 * @author piyush
 */
public class restServerOVFResource extends ServerResource {
    private dbHandler db;
    private Logger logger;
    private String dbType;
    private String vepProperties;
    
    public restServerOVFResource()
    {
        vepProperties = VEPHelperMethods.getPropertyFile();
        logger = Logger.getLogger("VEP.restOVFres");
//        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, "vep.properties");
        dbType = VEPHelperMethods.getProperty("vepdb.choice", logger, vepProperties);
        db = new dbHandler("restServerOVFoperations", dbType);
    }
    
    @Get("json")
    public Representation getValue() throws ResourceException
    {
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String contentType = requestHeaders.getFirstValue("Content-Type");
        String acceptType = requestHeaders.getFirstValue("Accept");
        Representation response = null;
        
        String username = requestHeaders.getFirstValue("X-Username");
        //username contained in the certificate overrides username in header
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        
        if(acceptType != null)
        {
            if(acceptType.contains("html"))
                response = toHtml(username);
            else if(acceptType.contains("json"))
                response = toJson(username);
        }
        else
        {
            //default rendering ...
            response = toHtml(username);
        }
        //System.out.println(contentType);
        return response;
    }
    
    public Representation toJson(String username)
    {
        JSONObject obj = new JSONObject();
        obj.put("title", "List of registered OVFs ");
        JSONArray arr = new JSONArray();
        JSONArray link = new JSONArray();
        if(username == null)
        {
            try
            {
                ResultSet rs = db.query("select", "*", "ovf", "");
                int count = 0;
                while(rs.next())
                {
                    logger.trace("Now performing checks for the OVF application: " + rs.getString("ovfname"));
                    //now for each individual OVF decide whether to generate the link or not
                    String ovfPerm = rs.getString("perm");
                    int oPerm = 0;
                    try
                    {
                        oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                    }
                    catch(Exception ex)
                    {
                        logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                        oPerm = 0;
                    }
                    if(oPerm >= 4)
                    {
                        arr.add(rs.getString("ovfname"));
                        link.add("/ovf/id/" + rs.getInt("sno"));
                        count++;
                    }
                }
                rs.close();
                obj.put("ovfs", arr);
                obj.put("links", link);
                obj.put("count", count);
            }
            catch(Exception ex)
            {
                obj.put("error", "SQL error occured.");
                this.setStatus(Status.SERVER_ERROR_INTERNAL);
                logger.debug("Exception caught: " + ex.getMessage());
            }
        }
        else
        {
            try
            {
                ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                {
                    int uid = rs.getInt("uid");
                    rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                    String groupList = "";
                    while(rs.next())
                    {
                        groupList += rs.getString("gname") + ",";
                    }
                    String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                    logger.trace("GroupsList: " + groupList);
                    boolean isAdmin = false;
                    for(int i=0; i<groups.length; i++)
                    {
                        if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                        {
                            isAdmin = true;
                            logger.trace("Setting isAdmin to true.");
                            break;
                        }
                    }
                    rs.close();
                    rs = db.query("select", "*", "ovf", "");
                    int count=0;
                    while(rs.next())
                    {
                        logger.trace("Now performing checks for the OVF application: " + rs.getString("ovfname"));
                        //now for each individual OVF decide whether to generate the link or not
                        if(isAdmin || rs.getInt("uid") == uid)
                        {
                            arr.add(rs.getString("ovfname"));
                            link.add("/ovf/id/" + rs.getInt("sno"));
                            count++;
                        }
                        else
                        {
                            String ovfGroup = rs.getString("gname");
                            String ovfPerm = rs.getString("perm");
                            int gPerm = 0;
                            int oPerm = 0;
                            try
                            {
                                gPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(1)));
                                oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                            }
                            catch(Exception ex)
                            {
                                logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                                gPerm = oPerm = 0;
                            }
                            boolean isMember = false;
                            for(int i=0; i<groups.length; i++)
                            {
                                if(groups[i].equalsIgnoreCase(ovfGroup))
                                {
                                    isMember = true;
                                    logger.trace("User " + username + " is member of OVF group.");
                                    break;
                                }
                            }
                            if(isMember)
                            {
                                if(gPerm >= 4)
                                {
                                    arr.add(rs.getString("ovfname"));
                                    link.add("/ovf/id/" + rs.getInt("sno"));
                                    count++;
                                }
                            }
                            else
                            {
                                if(oPerm >= 4)
                                {
                                    arr.add(rs.getString("ovfname"));
                                    link.add("/ovf/id/" + rs.getInt("sno"));
                                    count++;
                                }
                            }
                        }
                    }
                    rs.close();
                    obj.put("ovfs", arr);
                    obj.put("links", link);
                    obj.put("count", count);
                }
                else
                {
                    //no user exists at VEP, error
                    obj.put("error", "No such user exists.");
                    this.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
                    logger.debug("Error: Username not found at VEP.");
                }
            }
            catch(Exception ex)
            {
                obj.put("error", "SQL error occured.");
                this.setStatus(Status.SERVER_ERROR_INTERNAL);
                logger.warn("Caught exception while trying to display the list of OVFs. Exception: " + ex.getMessage());
            }
        }
        StringRepresentation value = new StringRepresentation(obj.toJSONString(), MediaType.APPLICATION_JSON);
        return value;
    }
    
    public Representation toHtml(String username)
    {  
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(VEPHelperMethods.getRESTwebHeader(false));
        
        if(username == null)
        {
            try
            {
                ResultSet rs = db.query("select", "*", "ovf", "");
                stringBuilder.append("List of registered OVFs<br>");
                stringBuilder.append("<ul style='font-family:Times;font-size:11pt;color:black;'>");
                while(rs.next())
                {
                    logger.trace("Now performing checks for the OVF application: " + rs.getString("ovfname"));
                    //now for each individual OVF decide whether to generate the link or not
                    String ovfPerm = rs.getString("perm");
                    int oPerm = 0;
                    try
                    {
                        oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                    }
                    catch(Exception ex)
                    {
                        logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                        oPerm = 0;
                    }
                    if(oPerm >= 4)
                    {
                        stringBuilder.append("<li>").append(rs.getString("ovfname")).append(" <a href='").
                            append("/ovf/id/").append(rs.getInt("sno")).append("'>").append("/ovf/id/").append(rs.getInt("sno")).append("</a>");
                    }
                }
                stringBuilder.append("</ul><br>");
            }
            catch(Exception ex)
            {
                stringBuilder.append("<B>SQL Query Error!! Details of exception follows ...</B>");
                stringBuilder.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
                stringBuilder.append(ex.getMessage());
                logger.debug("Exception caught: " + ex.getMessage());
                stringBuilder.append("</div>");
            }
            stringBuilder.append("<B>Partial Request!! Details of missing parameters follows ...</B>");
            stringBuilder.append("<div style='border:1px;background:silver;color:black;font-family:Times;font-size:9pt;'>");
            stringBuilder.append("Username not found in the header.");
            logger.debug("HTML Header error: Username missing.");
            stringBuilder.append("</div><br>");
        }
        else
        {   
            try
            {
                ResultSet rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                {
                    int uid = rs.getInt("uid");
                    rs = db.query("select", "*", "ugroup", "where uid=" + uid + "");
                    String groupList = "";
                    while(rs.next())
                    {
                        groupList += rs.getString("gname") + ",";
                    }
                    String[] groups = groupList.split(","); //the last index will be empty because of the trailing ,
                    logger.trace("GroupsList: " + groupList);
                    boolean isAdmin = false;
                    for(int i=0; i<groups.length; i++)
                    {
                        if(groups[i].equalsIgnoreCase("admin") || groups[i].equalsIgnoreCase("cloudadministrator"))
                        {
                            isAdmin = true;
                            logger.trace("Setting isAdmin to true.");
                            break;
                        }
                    }
                    rs = db.query("select", "*", "ovf", "");
                    stringBuilder.append("List of registered OVFs<br>");
                    stringBuilder.append("<ul style='font-family:Times;font-size:11pt;color:black;'>");
                    while(rs.next())
                    {
                        logger.trace("Now performing checks for the OVF application: " + rs.getString("ovfname"));
                        //now for each individual OVF decide whether to generate the link or not
                        if(isAdmin || rs.getInt("uid") == uid)
                        {
                            stringBuilder.append("<li>").append(rs.getString("ovfname")).append(" <a href='").
                                append("/ovf/id/").append(rs.getInt("sno")).append("'>").append("/ovf/id/").append(rs.getInt("sno")).append("</a>");
                        }
                        else
                        {
                            String ovfGroup = rs.getString("gname");
                            String ovfPerm = rs.getString("perm");
                            int gPerm = 0;
                            int oPerm = 0;
                            try
                            {
                                gPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(1)));
                                oPerm = Integer.parseInt(String.valueOf(ovfPerm.charAt(2)));
                            }
                            catch(Exception ex)
                            {
                                logger.warn("Exception caught while getting the OVF's group permission bits. Exception: " + ex.getMessage());
                                gPerm = oPerm = 0;
                            }
                            boolean isMember = false;
                            for(int i=0; i<groups.length; i++)
                            {
                                if(groups[i].equalsIgnoreCase(ovfGroup))
                                {
                                    isMember = true;
                                    logger.trace("User " + username + " is member of OVF group.");
                                    break;
                                }
                            }
                            if(isMember)
                            {
                                if(gPerm >= 4)
                                {
                                    stringBuilder.append("<li>").append(rs.getString("ovfname")).append(" <a href='").
                                        append("/ovf/id/").append(rs.getInt("sno")).append("'>").append("/ovf/id/").append(rs.getInt("sno")).append("</a>");
                                }
                                else
                                {
                                    //
                                }
                            }
                            else
                            {
                                if(oPerm >= 4)
                                {
                                    stringBuilder.append("<li>").append(rs.getString("ovfname")).append(" <a href='").
                                        append("/ovf/id/").append(rs.getInt("sno")).append("'>").append("/ovf/id/").append(rs.getInt("sno")).append("</a>");
                                }
                                else
                                {
                                    //
                                }
                            }
                        }
                    }
                    stringBuilder.append("</ul><br><br>");
                }
                else
                {
                    //no user exists at VEP, error
                    stringBuilder.append("<B>Exception!! Details of exception follows ...</B>");
                    stringBuilder.append("<div style='border:1px;background:red;color:black;font-family:Times;font-size:9pt;'>");
                    stringBuilder.append("User ").append(username).append(" not registered at this provider.");
                    logger.debug("Error: Username not found at VEP.");
                    stringBuilder.append("</div>");
                }
            }
            catch(Exception ex)
            {
                logger.warn("Caught exception while trying to display the list of OVFs. Exception: " + ex.getMessage());
            }
        }
        stringBuilder.append("Click on the banner image to go up one level<br>");
        stringBuilder.append(VEPHelperMethods.getRESTwebFooter());
        StringRepresentation value = new StringRepresentation(stringBuilder.toString(), MediaType.TEXT_HTML);
        return value;
    }
    
    @Put("xml")
    public Representation storeOvf(String ovfDesc)
    {
        String ovfname = ((String) getRequest().getAttributes().get("name"));
        //System.out.println("OVFAction: " + ovfaction);
        
        //is it a good idea to mix formats? part JSON rest XML?
        
        //accessing header information
        Form requestHeaders = (Form) getRequest().getAttributes().get("org.restlet.http.headers");
        String username = requestHeaders.getFirstValue("X-Username");
        String ovfgroup = requestHeaders.getFirstValue("X-Ovfgroup");
        String ovfperm = requestHeaders.getFirstValue("X-Ovfperm");
        
        //username contained in the certificate overrides username in header
        List<X509Certificate> certs = (List)getRequest().getAttributes().get("org.restlet.https.clientCertificates");
        for(int i=0; certs != null && i < certs.size(); i++)
        {
            X509Certificate Cert = certs.get(i);
            String certName = Cert.getSubjectX500Principal().getName();
            logger.info("Received certificate with name: " + certName);
            String[] certParts = certName.split(",");
            for(int j=0; j<certParts.length; j++)
            {
                if(certParts[j].startsWith("CN="))
                {
                    username = certParts[j].split("=")[1];
                    break;
                }
            }
            logger.info("REST request came from: " + username);
        }
        if(certs == null)
        {
            logger.warn("Client certificates list is empty. Unauthenticated client.");
        }
        else if(certs.isEmpty())
        {
            logger.warn("Client certificates list is empty. Unauthenticated client. Size = 0.");
        }
        
        logger.trace("Received PUT request for OVF: " + ovfname + " for user: " + username);
        
        JSONObject response = new JSONObject();
        response.put("title", "New OVF application submit request");
        ResultSet rs;
        
        if(username == null || ovfDesc == null || ovfDesc.trim().length() == 0 || ovfname == null || ovfname.trim().length() == 0)
        {
            response.put("error", "CLIENT_ERROR_BAD_REQUEST");
            this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        }
        else
        { 
            //for every other parameters if null use default values
            int uid = -1;
            //now get the uid for the username
            try
            {
                rs = db.query("select", "*", "user", "where username='" + username + "'");
                if(rs.next())
                    uid = rs.getInt("uid");
                else
                    uid = -2;
                rs.close();
            }
            catch(Exception ex)
            {
                logger.warn("Exception caught while retrieving UID for user " + username + " : " + ex.getMessage());
            }

            if(uid != -1 && uid != -2)
            {
                try
                {
                    //rs = db.query("select", "*", "ovf", "where uid=" + uid + " AND ovfname='" + ovfname + "'");
                    rs = db.query("select", "*", "ovf", "where ovfname='" + ovfname + "' AND uid=" + uid);
                    boolean status = false;
                    if(rs.next())
                    {
                        //an entry already exists, can not proceed with this path
                        int appInConflictSno = rs.getInt("sno");
                        response.put("ovfid", appInConflictSno);
                        response.put("error", "CLIENT_ERROR_CONFLICT");
                        this.setStatus(Status.CLIENT_ERROR_CONFLICT);
                        rs.close();
                    }
                    else
                    {
                        if(ovfgroup == null) ovfgroup = username; //assign defalut group to be same as self
                        if(ovfperm == null) ovfperm = "700"; //default access rights in the UNIX sense.
                        //states: ND = not yet deployed
                        //find max sno and then increment by i
                        int max_sno = 0;
                        rs = db.query("select", "max(sno)", "ovf", "");
                        if(rs.next())
                        {
                            max_sno = rs.getInt(1);
                            logger.debug("Serial number to be assigned to the ovf: " + max_sno + 1);
                            rs.close();
                        }
                        response.put("sno", (max_sno + 1));
                        
                        status = db.insert("ovf", "('" + ovfname + "', " + (max_sno+1) + ", -1, " + uid + ", 'ND', '" + ovfgroup + "', '" + ovfperm + "', '" + ovfDesc + "')");
                        if(status)
                        {
                            response.put("message", "SUCCESS_ACCEPTED");
                            this.setStatus(Status.SUCCESS_ACCEPTED);
                        }
                        else
                        {
                            response.put("error", "SERVER_ERROR_INTERNAL");
                            this.setStatus(Status.SERVER_ERROR_INTERNAL);
                        }
                    }
                }
                catch(Exception ex)
                {
                    logger.warn("Exception caught while creating/updating OVF entry for " + username + ", ovfname= " + ovfname + ": " + ex.getMessage());
                    response.put("error", "SERVER_ERROR_INTERNAL");
                    this.setStatus(Status.SERVER_ERROR_INTERNAL);
                }
            }
            else
            {
                if(uid == -1)
                {
                    response.put("error", "SERVER_ERROR_INTERNAL");
                    this.setStatus(Status.SERVER_ERROR_INTERNAL);
                }
                else
                {
                    response.put("error", "CLIENT_ERROR_PRECONDITION_FAILED"); //user must exist at VEP before hand
                    this.setStatus(Status.CLIENT_ERROR_PRECONDITION_FAILED);
                    logger.debug("User " + username + " does not exist. OVF entry failed.");
                }
            }
        }    
        //System.out.println(ovfDesc);
        StringRepresentation out = new StringRepresentation(response.toJSONString(), MediaType.APPLICATION_JSON);
        return out;
    }
}
