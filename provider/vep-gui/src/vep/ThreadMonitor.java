/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vep;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import org.apache.log4j.Logger;

/**
 *
 * @author piyush
 */
public class ThreadMonitor implements Runnable
{
    private Thread t;
    private String monitoredName;
    private String monitoredAction;
    private ThreadWorker object;
    private JTextArea logArea;
    private JPanel mainPanel;
    private JButton button;
    private JProgressBar progressBar;
    private Logger logger;
    
    public ThreadMonitor(String name, String act, ThreadWorker o, JProgressBar bar, JTextArea log, JPanel pane, JButton btn)
    {
        object = o;
        monitoredName = name;
        monitoredAction = act;
        logArea = log;
        mainPanel = pane;
        button = btn;
        progressBar = bar;
        progressBar.setMinimum(0);
        progressBar.setMaximum(100);
        progressBar.setValue(0);
        progressBar.setForeground(Color.BLACK);
        logger = Logger.getLogger("VEP.ThreadMonitor");
        if(monitoredAction.equalsIgnoreCase("validate"))
        {
            progressBar.setToolTipText("showing the progress of OVF file validation");
            progressBar.setName("OVF validation");
        }
        t = new Thread(this);
    }
    
    public void start()
    {
        logger.trace("Starting monitor thread [Name: " + monitoredName + ", Action: " + monitoredAction + "].");
        t.start();
    }
    
    @SuppressWarnings("SleepWhileInLoop")
    public void run() 
    {
        logger.trace("Monitor thread [Name: " + monitoredName + ", Action: " + monitoredAction + "] started.");
        try
        {
            while(!object.hasFinished()) 
            {
                logArea.append(".");
                progressBar.setValue(progressBar.getValue() + 1);
                Thread.sleep(2000);
            }
            progressBar.setValue(100);
            if(monitoredName.equalsIgnoreCase("xmlhandler"))
            {
                int statusCode = object.getReturnCode();
                if(statusCode == 0)
                {
                    JDialog errorMessage;
                    JOptionPane errMess = new JOptionPane("This OVF File is incorrect.\nI can not process it.", JOptionPane.ERROR_MESSAGE);
                    errorMessage = errMess.createDialog(mainPanel, "Invalid OVF");
                    logger.debug("OVF File is not valid.");
                    errorMessage.setVisible(true);
                    if(button != null)
                    {
                        button.setText("test");
                        button.setEnabled(true);
                    }
                    logArea.append(" error.\n");
                }
                else if(statusCode == -1)
                {
                    JDialog errorMessage;
                    JOptionPane errMess = new JOptionPane("Unknown error encountered while validating the OVF file, \n"
                                                        + "SAXException caught! I can not proceed with this OVF verification.\n"
                                                        + "You may restart the application and try again.", JOptionPane.ERROR_MESSAGE);
                    errorMessage = errMess.createDialog(mainPanel, "Internal OVF Fault");
                    logger.error("OVF validation process suffered internal exception, validation terminated.");
                    errorMessage.setVisible(true);
                    if(button != null)
                    {
                        button.setText("test");
                        button.setEnabled(true);
                    }
                    logArea.append(" error.\n");
                }
                else
                {
                    logArea.append("OVF file has been validated!\n");
                    JDialog infoMessage;
                    JOptionPane infoMess = new JOptionPane("The OVF file has been validated.\nNow you may deploy it!", JOptionPane.INFORMATION_MESSAGE);
                    infoMessage = infoMess.createDialog(mainPanel, "Validation success");
                    logger.debug("OVF file has been validated successfully.");
                    infoMessage.setVisible(true);
                    if(button != null)
                    {
                        button.setText("deploy");
                        button.setEnabled(true);
                    }
                }
            }
            progressBar.setValue(0);
            progressBar.setToolTipText("");
            progressBar.setName("");
        }
        catch(Exception ex)
        {
            if(logger.isDebugEnabled())
                ex.printStackTrace(System.err);
            else
                logger.warn(ex.getMessage());
        }
    }
    
}
