INTRODUCTION
------------

VEP is a management platform that is provided to the cloud provider
for integrating her services with the Contrail federation. One copy
of VEP will run at every Contrail provider. Currently VEP supports 
one cloud cluster per provider. In the future release, it will have
capability of integrating multiple cloud clusters of a provider.

COMPILATION
-----------
Minumum ANT version required: 1.8.0

VEP software is developed as a Java Desktop Applictaion. You must
have ANT build system to run the software, at the root of the VEP
directory use `ant run' to compile and execute the software. Once
finished, you can use `ant clean' to cleanup any class files
generated during build phase. To generate the executable jar file
use `ant jar' to create the jar file under `dist' subdirectory.

A command-line only version of the software exists as a maven 
project. Check out the contrail OW2 repositories for the source.

VEP Admin ACCESS
----------------
Certain functionality requires admin authentication before they can
be used, the software uses internal database for admin validation.
At least one account must be hand configured into the database,
additional admin accounts can be added using the GUI. Configure
your VEP database accordingly depending on whether you use sqlite
or mysql backend.

CONFIGURATION Settings
----------------------
Go to Edit -> Settings and populate the fields with appropriate
values. If no Contrail cluster exists in the OpenNebula environment
leave Contrail cluster ID field empty.

VEP supports both mysql and sqlite databases. If you wish to use mysql
then make sure that you have a database called `vepdb' existing in you
setup, if the schema format is missing the software will take care of
it during startup phase. Enter appropriate database user details in
the software settings panel.

In case you wish to use sqlite instead, read on:
------------------------------------------------
You must provide the database file path. The filename must end with
extension (.db) Once you save the Settings, you can initialize the
database to correct schema by simply clicking on the VEP DB Connection
button in the Status Indicators panel. Another way to initialize the
VEP DB after saving the settings parameters is to restart the VEP
application. NOTE: The path to DB file is allowed to point to a
non-existant file. Just type in the file name ending in a (.db)
extension at the file chooser prompt.

For proper REST server functionality, you must provide a valid Java
Key Store (JKS) file containing the server certificate that REST
server will use to establish a SSL connection and authenticating
itself. You must specify the store password and the server key
password in the VEP settings.

For testing a JKS file is included with the code. The store password
and the key password are both "pass1234" without the double-quotes. 
But feel free to use your own JKS file.

WARNING
-------
The VEP tool is currently under active development and many features
are missing. The source tree is very unstable now mith sometime
several commits occuring in a single day. It is meant for an early
feature preview purpose only.
