package org.ow2.contrail.provider.scheduler.entities;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
        @NamedQuery(name="Host.findAll",query="SELECT c FROM Host c")
        })
public class Host {
    @Id
    private Integer id;
    private String name;
    private Integer cpuFreq;
    private Integer coreCount;
    private Integer memorySize;
    private Integer diskSize;
    private String cpuArch;
    private String iaasid;
    private int cloudtype_id;
    @ManyToOne(optional = false)
    private Rack rack;

    public Host() {
    }
    
    public Host(JSONObject o) throws JSONException {
        this.id = o.getInt("id");
        this.name = o.getString("name");
        this.cpuFreq = o.getInt("cpuFreq");
        this.coreCount = o.getInt("coreCount");
        this.memorySize = o.getInt("ram");
        this.diskSize = o.getInt("diskSize");
        this.cpuArch = o.getString("cpuArch");
        this.iaasid=o.getString("iaasId");
        this.cloudtype_id=0;
    }

    public void setIaasid(String iaasid) {
        this.iaasid = iaasid;
    }

    public void setCloudtype_id(int cloudtype_id) {
        this.cloudtype_id = cloudtype_id;
    }

    public String getIaasid() {
        return iaasid;
    }

    public int getCloudtype_id() {
        return cloudtype_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCpuFreq() {
        return cpuFreq;
    }

    public void setCpuFreq(Integer cpuFreq) {
        this.cpuFreq = cpuFreq;
    }

    public Integer getCoreCount() {
        return coreCount;
    }

    public void setCoreCount(Integer coreCount) {
        this.coreCount = coreCount;
    }

    public Integer getMemorySize() {
        return memorySize;
    }

    public void setMemorySize(Integer memorySize) {
        this.memorySize = memorySize;
    }

    public Integer getDiskSize() {
        return diskSize;
    }

    public void setDiskSize(Integer diskSize) {
        this.diskSize = diskSize;
    }

    public String getCpuArch() {
        return cpuArch;
    }

    public void setCpuArch(String cpuArch) {
        this.cpuArch = cpuArch;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject o = new JSONObject();
        o.put("id", id);
        o.put("name", name);
        o.put("cpuFreq", cpuFreq);
        o.put("coreCount", coreCount);
        o.put("ram", memorySize);
        o.put("diskSize", diskSize);
        o.put("cpuArch", cpuArch);
        o.put("cloudType",cloudtype_id);
        o.put("iaasId",iaasid);
        
        return o;
    }

    public Rack getRack() {
        return rack;
    }

    public void setRack(Rack rack) {
        this.rack = rack;
    }
    
    
    
    public void updateCpuCores(int ncpu){
        this.coreCount-=ncpu;
    }
    public void updateDisksize(int diskS){
        this.diskSize-=diskS;
    }
    
    public void updateRam(int ram){
        this.memorySize-=ram;
    }
}
