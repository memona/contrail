/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.scheduler.utils;

import org.json.JSONException;
import org.ow2.contrail.provider.scheduler.entities.*;

/**
 *
 * @author fgaudenz
 */
public class DBManagement {
     public static void  deleteHost(Host host) throws JSONException{
         //check if it is possible to delete
         Scheduler.executeCommand(Scheduler.DELETEHOST, host);
     }
     public static void  deleteDatacenter(Datacenter datacenter) throws JSONException{
         //check if it is possible to delete
         Scheduler.executeCommand(Scheduler.DELETEDATACENTER, datacenter);
     }
     public static void  deleteCluster(Cluster cluster) throws JSONException{
         //check if it is possible to delete
         Scheduler.executeCommand(Scheduler.DELETECLUSTER, cluster);
     }
     public static void  deleteRack(Rack rack) throws JSONException{
         //check if it is possible to delete
         Scheduler.executeCommand(Scheduler.DELETERACK, rack);
     }
     
     
     
     
     
     
     
     
     
}
