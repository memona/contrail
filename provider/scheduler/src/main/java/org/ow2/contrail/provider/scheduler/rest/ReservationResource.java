package org.ow2.contrail.provider.scheduler.rest;

import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.scheduler.entities.Reservation;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

public class ReservationResource {
    private Reservation reservation;

    public ReservationResource(Reservation reservation) {
        this.reservation = reservation;
    }

    @GET
    @Produces("application/json")
    public Response getReservation() throws JSONException {
        JSONObject json = reservation.toJSON();
        return Response.ok(json.toString()).build();
    }

    @DELETE
    public Response removeReservation() throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            em.getTransaction().begin();
            reservation = em.merge(reservation);
            em.remove(reservation);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
}
