package org.ow2.contrail.provider.scheduler.rest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.scheduler.entities.Cluster;
import org.ow2.contrail.provider.scheduler.entities.Datacenter;
import org.ow2.contrail.provider.scheduler.utils.DBUtils;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URISyntaxException;


public class DatacenterResource {
    private static String URI_TEMPLATE = "/datacenters/%d/clusters/%d";
    private Datacenter datacenter;

    public DatacenterResource(Datacenter datacenter) {
        this.datacenter = datacenter;
    }

    @GET
    @Produces("application/json")
    public Response getDatacenter() throws JSONException {
        JSONObject json = datacenter.toJSON();
        return Response.ok(json.toString()).build();
    }

    @DELETE
    public Response removeDatacenter() throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            em.getTransaction().begin();
            datacenter = em.merge(datacenter);
            em.remove(datacenter);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Path("/clusters")
    @Produces("application/json")
    public Response getClusters() throws JSONException {
        JSONArray array = new JSONArray();
        for (Cluster cluster : datacenter.getClusters()) {
            URI uri = UriBuilder.fromResource(DatacentersResource.class)
                    .path(cluster.getId().toString())
                    .build();
            JSONObject o = new JSONObject();
            o.put("name", cluster.getName());
            o.put("uri", uri);
            array.put(o);
        }
        return Response.ok(array.toString()).build();
    }

    @POST
    @Path("/clusters")
    @Consumes("application/json")
    public Response addCluster(String request) throws URISyntaxException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            JSONObject json = new JSONObject(request);
            Cluster cluster = new Cluster(json);
            cluster.setDatacenter(datacenter);

            em.getTransaction().begin();
            em.persist(cluster);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", cluster.getId()));
            return Response.created(resourceUri).build();
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @Path("/clusters/{clusterId}")
    public ClusterResource findCluster(@PathParam("clusterId") int clusterId) {
        for (Cluster cluster : datacenter.getClusters()) {
            if (cluster.getId() == clusterId) {
                return new ClusterResource(datacenter, cluster);
            }
        }
        throw new WebApplicationException(Response.Status.NOT_FOUND);
    }
}
