package org.ow2.contrail.provider.scheduler.rest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.scheduler.entities.Datacenter;
import org.ow2.contrail.provider.scheduler.entities.Reservation;
import org.ow2.contrail.provider.scheduler.utils.DBUtils;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.core.MediaType;
import org.ow2.contrail.provider.scheduler.entities.VmReservationSlot;
import org.ow2.contrail.provider.scheduler.entities.VmSlot;
import org.ow2.contrail.provider.scheduler.utils.Scheduler;

@Path("/reservations")
public class ReservationsResource {

    @GET
    @Produces("application/json")
    public Response getReservations() throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            Query query = em.createNamedQuery("Reservation.findAll");
            List<Reservation> reservationList = query.getResultList();

            JSONArray jsonArray = new JSONArray();
            for (Reservation reservation : reservationList) {
                URI uri = UriBuilder.fromResource(ReservationsResource.class)
                        .path(reservation.getId().toString())
                        .build();
                
                //JSONObject o = reservation.toJSON();
                
                JSONObject o =new JSONObject(); 
                o.put("uri", uri);
                jsonArray.put(o);
            }
            return Response.ok(jsonArray.toString()).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON) 
    public Response addReservation(String request) throws URISyntaxException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {

             JSONObject json = new JSONObject(request);
             //System.out.println("REQUEST RESERVATION"+json);
             Reservation reservation = new Reservation(json);
             //System.out.println("HERE");
             Collection<VmReservationSlot> vms=reservation.getVmReservations();
            reservation=(Reservation)Scheduler.executeCommand(Scheduler.RESERVE,reservation);

            URI resourceUri = new URI(String.format("/%d", reservation.getId()));
            JSONObject response=new JSONObject();
            response=reservation.toJSON();
            //System.out.println("JSON:"+response);
          return Response.ok(response.toString()).build();
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @POST
    @Path("/check")
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON) 
    public Response checkReservation(String request) throws URISyntaxException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {

             JSONObject json = new JSONObject(request);
             //System.out.println("REQUEST RESERVATION"+json);
             Reservation reservation = new Reservation(json);
             //System.out.println("HERE");
             Collection<VmReservationSlot> vms=reservation.getVmReservations();
            reservation=(Reservation)Scheduler.executeCommand(Scheduler.CHECKRESERVE,reservation);

            URI resourceUri = new URI(String.format("/%d", reservation.getId()));
            JSONObject response=new JSONObject();
            response=reservation.toJSON();
            //System.out.println("JSON:"+response);
          return Response.ok(response.toString()).build();
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }
    
    
    @GET
    @Path("/{reservationId}")
    @Produces(MediaType.APPLICATION_JSON) 
    public Response findReservation(@PathParam("reservationId") long reservationId) throws JSONException {
        
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        Reservation reservation = em.find(Reservation.class, reservationId);
        JSONObject o = reservation.toJSON();
        PersistenceUtils.getInstance().closeEntityManager(em);
        return Response.ok(o.toString()).build();

    }
    
     @DELETE
     @Path("/{reservationId}")
     public Response deleteReservations(@PathParam("reservationId") long reservationId) throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        Reservation reservation = em.find(Reservation.class, reservationId);
        Collection<VmReservationSlot> vmReservation=reservation.getVmReservations();
          em.getTransaction().begin();
        for(VmReservationSlot app:vmReservation){
          
            em.remove(app);
            
        }
        em.remove(reservation);
        em.getTransaction().commit();
        PersistenceUtils.getInstance().closeEntityManager(em);
        return Response.ok().build();
     }
}
