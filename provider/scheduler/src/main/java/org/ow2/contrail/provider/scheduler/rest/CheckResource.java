/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.scheduler.rest;

import java.net.URISyntaxException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author fgaudenz
 */
public class CheckResource {
     public CheckResource() {
            }

    
    public Response checkreservation(String request) {
        //System.out.println("OUTPUT:"+request);
        try {
            JSONObject json = new JSONObject(request);
            new Reservation(json);
            return Response.status(Response.Status.OK).build();
        } catch (JSONException e) {
            //System.out.println("Exception"+e);
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                    entity("Invalid JSON data: " + e.getMessage()).build());
        }
    }

    class Reservation {
        List<ReservationSlot> rsvSlot;
        String startDate,endDate;
        
        //constraint to model
        public Reservation(JSONObject object) throws JSONException {

            //JSONObject object = new JSONObject(obj);
            JSONArray jArrayConstrain;
            //VMs
            if (object.has("VMs")) {
                JSONArray jArrayVMs = object.getJSONArray("VMs");
                for (int i = 0; i < jArrayVMs.length(); i++) {
                    JSONObject objVM = jArrayVMs.getJSONObject(i);  
                    String nCpu = objVM.getString("nCpu");
                    String mem = objVM.getString("mem");
                    String disk = objVM.getString("disk");
                    String fCpu=null;
                    if(objVM.has("fCpu"))
                        fCpu=objVM.getString("fCpu");                    
                }
            } else {
                System.out.println("no VMs");
            }



            //Constraints
            if (object.has("Constraints")) {
                jArrayConstrain = object.getJSONArray("Constraints");
            } else {
                System.out.println("no Constraints");
            }


            String startDate = null;
            //TIMEField
            if (object.has("StartDate")) {
                startDate = object.getString("startDate");
            } else {
                System.out.println("no StartData");
            }
            String endDate = null;
            if (object.has("endDate")) {
                endDate = object.getString("endDate");
            } else {
                System.out.println("no EtartData");
            }

        }
    }

    class ReservationSlot {
        int cpuCores;
        int mem;
        int disk;
        int cpuFreq;
        
        public ReservationSlot(int cpuCores,int mem,int disk,int cpuFreq) {
            this.cpuCores=cpuCores;
            this.mem=mem;
            this.disk=disk;
            this.cpuFreq=cpuFreq;
        }
    }
}
