/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.scheduler.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Query;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

/**
 *
 * @author fgaudenz
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Cloudtype.findAll", query = "SELECT d FROM Cloudtype d"),
        @NamedQuery(name = "Cloudtype.maxID", query = "SELECT max(d.id) FROM Cloudtype d")
})
public class Cloudtype{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String version;

    public Cloudtype() {
    }
    public Cloudtype(String name,String version){
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Cloudtype.maxID");
            List<Integer> idList = query.getResultList();
            if(idList.size()==0){
                this.id=1;    
            }else{
                this.id=idList.get(0)+1;
            }
    }
    
    public Cloudtype(int id,String name,String version){
        this.id=id;
        this.version=version;
        this.name=name;
    }
    
    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    
    
    
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    

    

    @Override
    public String toString() {
        return "org.ow2.contrail.provider.scheduler.entities.Cloudtype[ id=" + id + " ]";
    }
    
}
