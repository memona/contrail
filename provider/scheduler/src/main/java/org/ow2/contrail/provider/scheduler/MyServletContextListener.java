package org.ow2.contrail.provider.scheduler;

import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyServletContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        PersistenceUtils.createInstance("SchedulerPersistenceUnit");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
