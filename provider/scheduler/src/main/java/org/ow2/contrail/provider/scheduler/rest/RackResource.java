package org.ow2.contrail.provider.scheduler.rest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.contrail.provider.scheduler.entities.Cluster;
import org.ow2.contrail.provider.scheduler.entities.Datacenter;
import org.ow2.contrail.provider.scheduler.entities.Host;
import org.ow2.contrail.provider.scheduler.entities.Rack;
import org.ow2.contrail.provider.scheduler.utils.DBUtils;
import org.ow2.contrail.provider.scheduler.utils.PersistenceUtils;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

public class RackResource {
    private static String URI_TEMPLATE = "/datacenters/%d/clusters/%d/racks/%d";
    private Datacenter datacenter;
    private Cluster cluster;
    private Rack rack;
    private String uri;

    public RackResource(Datacenter datacenter, Cluster cluster, Rack rack) {
        this.datacenter = datacenter;
        this.cluster = cluster;
        this.rack = rack;
        this.uri = String.format(URI_TEMPLATE, datacenter.getId(), cluster.getId(), rack.getId());
    }

    @GET
    @Produces("application/json")
    public Response getRack() throws JSONException {
        JSONObject json = rack.toJSON();
        return Response.ok(json.toString()).build();
    }

    @DELETE
    public Response removeRack() throws JSONException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            em.getTransaction().begin();
            rack = em.merge(rack);
            em.remove(rack);
            em.getTransaction().commit();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @GET
    @Path("/hosts")
    @Produces("application/json")
    public Response getHosts() throws JSONException {
        JSONArray array = new JSONArray();
        for (Host host : rack.getHosts()) {
            JSONObject o = new JSONObject();
            o.put("name", host.getName());
            o.put("uri", uri + "/hosts/" + host.getId());
            array.put(o);
        }
        return Response.ok(array.toString()).build();
    }

    @POST
    @Path("/hosts")
    @Consumes("application/json")
    public Response addHost(String request) throws URISyntaxException {
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        try {
            JSONObject json = new JSONObject(request);
            Host host = new Host(json);
            host.setRack(rack);

            em.getTransaction().begin();
            em.persist(host);
            em.getTransaction().commit();

            URI resourceUri = new URI(String.format("/%d", host.getId()));
            return Response.created(resourceUri).build();
        }
        catch (JSONException e) {
            throw new WebApplicationException(
                    Response.status(Response.Status.BAD_REQUEST).
                            entity("Invalid JSON data: " + e.getMessage()).build());
        }
        catch (RollbackException e) {
            if (DBUtils.isIntegrityConstraintException(e)) {
                return Response.status(Response.Status.CONFLICT).build();
            }
            else {
                throw e;
            }
        }
        finally {
            PersistenceUtils.getInstance().closeEntityManager(em);
        }
    }

    @Path("/hosts/{hostId}")
    public HostResource findHost(@PathParam("hostId") int hostId) {
        for (Host host : rack.getHosts()) {
            if (host.getId() == hostId) {
                return new HostResource(datacenter, cluster, rack, host);
            }
        }
        throw new WebApplicationException(Response.Status.NOT_FOUND);
    }

}
