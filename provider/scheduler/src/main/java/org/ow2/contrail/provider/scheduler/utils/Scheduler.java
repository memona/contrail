/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ow2.contrail.provider.scheduler.utils;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.ow2.contrail.provider.scheduler.entities.*;

/**
 *
 * @author fgaudenz
 */
public class Scheduler {

    public static String DELETEHOST = "dhost", UPDATEHOST = "uhost", DELETEDATACENTER = "ddatacenter", DELETECLUSTER = "dcluster", DELETERACK = "drack";
    public static String RESERVE = "reserve", DEPLOY = "deploy", CHECKRESERVE = "checkreserve";

    public static synchronized Object executeCommand(String command, Object obj) throws JSONException {
        if (command == DELETEHOST) {
            Host host = (Host) obj;
            DBlock.deleteHost(host);
            return null;
        }
        if (command == DELETEDATACENTER) {
            Datacenter datacenter = (Datacenter) obj;
            DBlock.deleteDatacenter(datacenter);
            return null;
        }
        if (command == DELETECLUSTER) {
            Cluster cluster = (Cluster) obj;
            DBlock.deleteCluster(cluster);
            return null;
        }
        if (command == DELETERACK) {
            Rack rack = (Rack) obj;
            DBlock.deleteRack(rack);
            return null;
        }
        if (command == UPDATEHOST) {
            Host host = (Host) obj;
            DBlock.deleteHost(host);
            return null;
        }
        if (command == DEPLOY) {
            VmSlot[] vms = (VmSlot[]) obj;
            VmSlot[] result = Scheduler.deployRR(vms);
            return result;
        }
        if (command == RESERVE) {
            //from ReservationSlot to VMslot
            Reservation reservation = (Reservation) obj;
            Collection<VmReservationSlot> vmsCollection = reservation.getVmReservations();

            VmReservationSlot[] resVMs = new VmReservationSlot[vmsCollection.size()];
            int j = 0;
            for (VmReservationSlot elem : vmsCollection) {
                resVMs[j] = elem;
                j++;
            }
            int shift=0;
            ArrayList<VmSlot> array = new ArrayList<VmSlot>();
            for (int i = 0; i < resVMs.length; i++) {
                VmSlot[] vms;//=new VmSlot[resVMs.length];    
                
                vms = resVMs[i].toVmSlot(i+shift);
                shift=vms.length;
                for (VmSlot elem : vms) {
                    array.add(elem);
                }
            }
            VmSlot.processConstraintsforReservation(array, reservation.getConstraints());
            VmSlot[] vms = new VmSlot[array.size()];
            for (int i = 0; i < vms.length; i++) {
                vms[i] = array.get(i);
            }
            VmSlot[] result = Scheduler.deployRR(vms);
            //System.out.println("SCHEDULED");
            //System.out.println("NUMERO VMS:"+result.length);
            resVMs = VmSlot.toVMReservationSlot(result,resVMs);
            //System.out.println("PROCESSED");
            System.out.println("RESERVATION TO BE STORED:" + reservation.getId());
            System.out.println("RESERVATION NUMBER OF VMRESERVATION:" + reservation.getVmReservations().size());
            reservation.setVmReservations(java.util.Arrays.asList(resVMs));
            EntityManager em = PersistenceUtils.getInstance().getEntityManager();
            em.getTransaction().begin();
            em.persist(reservation);
            em.getTransaction().commit();
            PersistenceUtils.getInstance().closeEntityManager(em);
            // DBlock.storeReservation(reservation);
            return reservation;
            //from VMslot to reservationSlot
        }
        if (command == CHECKRESERVE) {
            //from ReservationSlot to VMslot
            Reservation reservation = (Reservation) obj;
            Collection<VmReservationSlot> vmsCollection = reservation.getVmReservations();

            VmReservationSlot[] resVMs = new VmReservationSlot[vmsCollection.size()];
            int j = 0;
            for (VmReservationSlot elem : vmsCollection) {
                resVMs[j] = elem;
                j++;
            }
            ArrayList<VmSlot> array = new ArrayList<VmSlot>();
            int shift=0;
            for (int i = 0; i < resVMs.length; i++) {
                VmSlot[] vms;//=new VmSlot[resVMs.length];    
                vms = resVMs[i].toVmSlot(i+shift);
                shift=vms.length;
                for (VmSlot elem : vms) {
                    array.add(elem);
                }
            }
            VmSlot[] vms = new VmSlot[array.size()];
            for (int i = 0; i < vms.length; i++) {
                vms[i] = array.get(i);
            }
            VmSlot[] result = Scheduler.deployRR(vms);
            //System.out.println("SCHEDULED");
            //System.out.println("NUMERO VMS:"+result.length);
            resVMs = VmSlot.toVMReservationSlot(result,resVMs);

            //System.out.println("PROCESSED");

            reservation.setVmReservations(java.util.Arrays.asList(resVMs));
            //DBlock.storeReservation(reservation);
            return reservation;
            //from VMslot to reservationSlot
        }
        return null;



    }

    public static void check() {
    }

    public static VmSlot[] deploySameRack(VmSlot vmStarter, List<Host> hosts) {
        System.out.println("DEPLOY OVER RACK");
        for (Host hosti : hosts) {
            System.out.println("HOST:" + hosti.getId() + " number of core" + hosti.getCoreCount() + " memory" + hosti.getMemorySize());
        }
        //check for reservation using that RACK
        VmSlot[] vms = vmStarter.getSameRackVMs();
        System.out.println("VMS TO DEPLOY IN ONE RACK:"+vms.length);
        Rack mainRack = null;
        EntityManager em = PersistenceUtils.getInstance().getEntityManager();
        for (int i = 0; i < vms.length; i++) {
            VmSlot vm = vms[i];
            if (vm.getReservationCEE() != -1) {



                VmReservationSlot reservation = em.find(VmReservationSlot.class, vm.getReservationID());
                List<SingleVMRes> slots = reservation.getVmslots();
                for (SingleVMRes single : slots) {
                    if (single.getVm() == null) {
                        single.setVm(vm);
                        vm.setHost(single.getHost());
                        vm.addReservation(single);
                        mainRack = single.getHost().getRack();
                        //DBlock.updateSingleReservation(single);
                        break;
                    }
                }
            }
            
            
            else {
                if (vm.getHost() != null) {
                    mainRack = vm.getHost().getRack();
                    break;
                }
            }
        }
        if (mainRack != null) {
            List<Host> hostToCheckFromRack = mainRack.getHosts();
            List<Host> hostToCheck = new ArrayList<Host>();
            //int i;
            for (Host hc : hostToCheckFromRack) {
                for (Host h : hosts) {
                    if (h.getId() == hc.getId()) {
                        hostToCheck.add(h);
                    }
                }
            }



            for (int i = 0; i < vms.length; i++) {
                VmSlot vm = vms[i];
                for (Host singleHost : hostToCheck) {
                    if (vm.getHost() == null) {
                        if ((((((singleHost.getCoreCount() >= vm.getCpuCount()) && (singleHost.getCpuFreq() >= vm.getCpuSpeed())) && (singleHost.getMemorySize() >= vm.getMemorySize())) && (singleHost.getDiskSize() >= vm.getDiskSize())) && (vm.checkCountry(singleHost.getRack().getCluster().getDatacenter().getCountry()))) && (vm.checkHost(singleHost))) {
                            singleHost.updateRam(vm.getMemorySize().intValue());
                            singleHost.updateDisksize(vm.getDiskSize().intValue());
                            singleHost.updateCpuCores(vm.getCpuCount());
                            vm.setHost(singleHost);

                            //result.put(String.valueOf(vm.getId()), String.valueOf(vm.getHost().getId()));
                            break;
                        }
                    }

                }
            }
            em.close();
            return vms;

        } else {

            //EntityManager em = PersistenceUtils.getInstance().getEntityManager();
            Query query = em.createNamedQuery("Rack.findAll");
            List<Rack> racks = query.getResultList();
            em.close();
            /*int n;
             Random rand = new Random();
             n = rand.nextInt(hosts.size());
             for (int i = 0; i < vms.length; i++) {
             VmSlot vm = vms[i];*/

            int n;
            Random rand = new Random();
            n = rand.nextInt(racks.size());
            for (int k = 0; k < racks.size(); k++) {
                Rack singleRack = racks.get((n + k) % racks.size());
                boolean flagRack = true;
                System.out.println("try to deploy on Rack"+singleRack.getName());
                //List<Host> hostToCheck = singleRack.getHosts();


                List<Host> hostToCheckFromRack = singleRack.getHosts();
                
                if (hostToCheckFromRack != null) {
                    List<Host> hostToCheck = new ArrayList<Host>();
                    //int i;
                    System.out.println("HOSTTOCHECKFROMRACK:"+hostToCheckFromRack.size()+ "  all hosts available:"+hosts.size());
                    for (Host hc : hostToCheckFromRack) {
                        for (Host h : hosts) {
                             System.out.println("COMPARE ID HOST: all "+h.getId()+" vs rack "+hc.getId()+"---->"+(h.getId() == hc.getId()));
                            if (h.getId().compareTo(hc.getId())==0) {
                                System.out.println("TRUE");
                                hostToCheck.add(h);
                                break;
                            }
                        }
                    }

                    System.out.println("CHECK ON HOST AVAILABE:number of host:"+hostToCheck.size());
                    for (Host hosti : hostToCheck) {
                        System.out.println("HOST:" + hosti.getId() + " number of core" + hosti.getCoreCount() + " memory" + hosti.getMemorySize());
                    }

                    for (int i = 0; i < vms.length; i++) {
                        VmSlot vm = vms[i];
                        boolean flagHost = false;
                        for (Host singleHost : hostToCheck) {
                            if (vm.getHost() == null) {
                                System.out.println("try to schedule: on host: id-" + singleHost.getId() + " vm: id-" + vm.getId());
                                System.out.println("COUNTRY CHECK:" + vm.getId() + "--" + vm.getCountry() + "////" + singleHost.getRack().getCluster().getDatacenter().getCountry() + " ----- " + vm.checkCountry(singleHost.getRack().getCluster().getDatacenter().getCountry()));
                                System.out.println("CPU_STATEMENT:" + vm.getCpuCount() + "->" + (singleHost.getCoreCount() >= vm.getCpuCount()));
                                if ((((((singleHost.getCoreCount() >= vm.getCpuCount()) && (singleHost.getCpuFreq() >= vm.getCpuSpeed())) && (singleHost.getMemorySize() >= vm.getMemorySize())) && (singleHost.getDiskSize() >= vm.getDiskSize())) && (vm.checkCountry(singleHost.getRack().getCluster().getDatacenter().getCountry()))) && (vm.checkHost(singleHost))) {
                                    singleHost.updateRam(vm.getMemorySize().intValue());
                                    singleHost.updateDisksize(vm.getDiskSize().intValue());
                                    singleHost.updateCpuCores(vm.getCpuCount());
                                    vm.setHost(singleHost);
                                    System.out.println("HOST:" + singleHost.getId() + " number of core" + singleHost.getCoreCount() + " memory" + singleHost.getMemorySize());
                                    flagHost = true;
                                    //result.put(String.valueOf(vm.getId()), String.valueOf(vm.getHost().getId()));
                                    break;
                                }
                            }
                        }
                        if (flagHost == false) {
                            //host not found!! - change rack
                            for (int j = 0; j < i; j++) {
                                Host restore = vms[j].getHost();
                                restore.setCoreCount(restore.getCoreCount() + vms[j].getCpuCount());
                                restore.setMemorySize(restore.getMemorySize() + vms[j].getMemorySize().intValue());
                            }
                            flagRack = false;
                            break;
                        }

                    }
                    // if(flagRack)
                    //   break;
                }
            }

            for (Host hosti : hosts) {
                System.out.println("HOST:" + hosti.getId() + " number of core" + hosti.getCoreCount() + " memory" + hosti.getMemorySize());
            }



        }
        /*      for(VmSlot oneVm:vms){
         System.out.println("SCHEDULED RACK ON:"+oneVm.getId()+" "+ oneVm.getHost().getId()+" " +oneVm.getHost().getRack().getId());
         }*/
        return null;

    }

    public static VmSlot[] deployRR(VmSlot[] vmslot) {
        if (vmslot.length == 0) {
            //Map<String,String> result=new HashMap<String, String>();
            return vmslot;
        } else {
            // Map<String,String> result=new HashMap<String, String>();
            //get all vmslot running or already scheduled

            EntityManager em = PersistenceUtils.getInstance().getEntityManager();
            Query query = em.createNamedQuery("VmSlot.findAll");

            //get all the VM running
            List<VmSlot> allVMslot = query.getResultList();
            System.out.println("AllVMslot--->" + allVMslot.size());
            if (allVMslot == null) {
                allVMslot = new ArrayList();
            }
            //query=em.create
            query = em.createNamedQuery("VmReservationSlot.activeReservation");
            // String date=DateUtils.serialize();
            query.setParameter("time", new Date());

            //get all reservation
            List<VmReservationSlot> vsReservation = query.getResultList();
            //List<VmSlot> stillValid=new ArrayList<Reservation>();
            //for (Reservation res : allreservation) {
            //Collection<VmReservationSlot> vsReservation = res.getVmReservations();
                for (VmReservationSlot vmS : vsReservation) {
                    List<SingleVMRes> singleRes = vmS.getVmslots();
                    for (SingleVMRes sr : singleRes) {
                        if (sr.getVm() == null) {
                            allVMslot.add(sr.toVmSlot());
                        }

                    }
                }
            //}

            //update host
            // all the host
            System.out.println("AllVMslot--->" + allVMslot.size());
            query = em.createNamedQuery("Host.findAll");
            List<Host> hosts = query.getResultList();
            for (int k = 0; k < hosts.size(); k++) {

                Host singleHost = hosts.get(k);//new Host(hostname, hostid,cpufreq,corecount,ram,disksize);
                for (int i = 0; i < allVMslot.size(); i++) {
                    VmSlot app = allVMslot.get(i);
                    int hid = app.getHost().getId();
                    if (hid == singleHost.getId()) {
                        singleHost.updateCpuCores(app.getCpuCount());
                        singleHost.updateDisksize(app.getDiskSize().intValue());
                        singleHost.updateRam(app.getMemorySize().intValue());
                    }
                }

                System.out.println("HOST:" + hosts.get(k).getId() + " number of core" + hosts.get(k).getCoreCount() + " memory" + hosts.get(k).getMemorySize());
            }
            int n;
            Random rand = new Random();
            n = rand.nextInt(hosts.size());
            for (int i = 0; i < vmslot.length; i++) {
                System.out.println("VMslot" + vmslot[i].getId() + ": reservation ID:" + vmslot[i].getReservationCEE() + " cpu:" + vmslot[i].getCpuCount() + " " + vmslot[i].getCpuSpeed() + "HZ  DISK" + vmslot[i].getDiskSize() + "RAM" + vmslot[i].getMemorySize());
                VmSlot vm = vmslot[i];

                //checking for reservation                 //checking for reservation 
                boolean toschedule=true;
                if  (vm.getReservationCEE()!=-1){
                    System.out.println("RESERVATION");
                        
                    VmReservationSlot reservation = em.find(VmReservationSlot.class, vm.getReservationID());
                    List<SingleVMRes> slots = reservation.getVmslots();
                    for (SingleVMRes single : slots) {
                        if (single.getVm() == null) {

                            single.setVm(vm);

                            vm.setHost(single.getHost());
                            toschedule=false;
                            vm.addReservation(single);
                            //DBlock.updateSingleReservation(single);
                            break;
                        }
                    }
                } 
                
                if(toschedule)       
                    {
                    System.out.println("NOT RESERVATION");
                    if (!vm.hasHost()) {

                        if (vm.rackConstraint()) {
                            System.out.println("rack constraint");
                            deploySameRack(vm, hosts);
                            for (Host hosti : hosts) {
                                System.out.println("HOST:" + hosti.getId() + " number of core" + hosti.getCoreCount() + " memory" + hosti.getMemorySize());
                            }
                        } else {
                            System.out.println("NOT rack contraints");
                            for (int k = 0; k < hosts.size(); k++) {
                                Host singleHost = hosts.get((n + i + k) % hosts.size());
                                System.out.println("Indice:" + (n + i + k));
                                System.out.println("HOST  : id " + singleHost.getId());
                                System.out.println("CHECK COUNTRY: required ->" + vm.getCountry() + " host:" + singleHost.getRack().getCluster().getDatacenter().getCountry() + " RESULT-> " + vm.checkCountry(singleHost.getRack().getCluster().getDatacenter().getCountry()));

                                if ((((((singleHost.getCoreCount() >= vm.getCpuCount()) && (singleHost.getCpuFreq() >= vm.getCpuSpeed())) && (singleHost.getMemorySize() >= vm.getMemorySize())) && (singleHost.getDiskSize() >= vm.getDiskSize())) && (vm.checkCountry(singleHost.getRack().getCluster().getDatacenter().getCountry()))) && (vm.checkHost(singleHost))) {
                                    singleHost.updateRam(vm.getMemorySize().intValue());
                                    singleHost.updateDisksize(vm.getDiskSize().intValue());
                                    singleHost.updateCpuCores(vm.getCpuCount());
                                    vm.setHost(singleHost);
                                    n = (n + k);
                                    //result.put(String.valueOf(vm.getId()), String.valueOf(vm.getHost().getId()));
                                    break;
                                }//end if


                            }

                        }//nd loop host
                    }
                }
            }//end loop scheduler 
            System.out.println("END SCHEDULING");
            PersistenceUtils.getInstance().closeEntityManager(em);
            /*  
             for(VmSlot vmPRINT:vmslot){
             System.out.println("VM:"+vmPRINT.getId()+" Host"+vmPRINT.getHost().getId());
             }*/
            return vmslot;
        }
    }

    /**
     *
     * @param vmslot
     * @param vmsSameHost
     * @param vmsSameHost
     * @return
     */
    public static VmSlot[] deployRR(VmSlot[] vmslot, List<VmSlot[]> vmsSameHost, List<VmSlot[]> vmsSameCluster) {

        EntityManager em = PersistenceUtils.getInstance().getEntityManager();




        //get all the VM running 
        Query query = em.createNamedQuery("VmSlot.findAll");
        List<VmSlot> allVMslot = query.getResultList();
        //get all valid reservation
        query = em.createNamedQuery("Reservation.activeReservation");
        query.setParameter("time", new Date());
        List<Reservation> allreservation = query.getResultList();


        //translate reservation into vmslot
        for (Reservation res : allreservation) {
            Collection<VmReservationSlot> vsReservation = res.getVmReservations();
            for (VmReservationSlot vmS : vsReservation) {
                List<SingleVMRes> singleRes = vmS.getVmslots();
                for (SingleVMRes sr : singleRes) {
                    //if it is not assigned to a vmslod already create a new vmslot
                    if (sr.getVm() == null) {
                        allVMslot.add(sr.toVmSlot());
                    }

                }
            }
        }

        //update host
        // all the host
        query = em.createNamedQuery("Host.findAll");
        List<Host> hosts = query.getResultList();
        for (int k = 0; k < hosts.size(); k++) {

            Host singleHost = hosts.get(k);//new Host(hostname, hostid,cpufreq,corecount,ram,disksize);
            for (int i = 0; i < allVMslot.size(); i++) {
                VmSlot app = allVMslot.get(i);
                int hid = app.getHost().getId();
                if (hid == singleHost.getId()) {
                    singleHost.updateCpuCores(app.getCpuCount());
                    singleHost.updateDisksize(app.getDiskSize().intValue());
                    singleHost.updateRam(app.getMemorySize().intValue());
                }

            }

            //System.out.println("HOST:"+hosts.get(k).getId()+" number of host"+hosts.get(k).getCoreCount());
        }





        return null;
    }

    private static boolean checkReservationAvailable(int reservationCEE) {
         if(reservationCEE==-1)
             return false;
         else{
         EntityManager em = PersistenceUtils.getInstance().getEntityManager();
         VmReservationSlot check=em.find(VmReservationSlot.class, reservationCEE);
         em.close();
            //get all the VM running
         List<SingleVMRes> reservs=check.getVmslots();
         boolean free=false;  
         for(SingleVMRes app:reservs){
             if(app.getVm()==null){
                 free=true;
                 break;
             }
         }
           
        return free;
         }
    }
}
