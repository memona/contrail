This explains the installation of  the OpenNebula XACML access control system. If you have any problems with installation, send an email to Aliksandr Lazouski <Aliksandr.Lazouski@iit.cnr.it>, Paolo Mori <Paolo.Mori@iit.cnr.it>

---------------------------------------------------------------------------------------
					INTRODUCTION
---------------------------------------------------------------------------------------

OpenNebula XACML access control system processes requests to OpenNebula resources adopting the OASIS XACML standard v2.0.  It consists of the PEP plugin for OpenNebula, web service PDP processing SOAP-XACMLReques/Response messages and evaluating access requests (based on SunXACML implementation, http://sunxacml.sourceforge.net/). and includes some policy examples.
						
			 _________________ 				     [policy]
			|		  |				        |
Cloud   [create VM]	|    OPENNEBULA	  |				        |
 User------------------>|   ___________	  |	SOAP[XACML Request]	   _____|_____
			|  | XACML PEP |--|------------------------------>| XACML PDP |
			|  |___________|<-|-------------------------------|___________|
			|    |     |      |     SOAP[XACML Response]
			| [VM-1] [VM-1]	  |
			|________________ |

The following basic attributes can be used creating a policy:
- subject-id, e.g. "oneadmin-0", "sasha-8", "paolo-17"
- resource-id, e.g. "VM-58", "HOST-4", "NET-3"
- resource owner, i.e. the subject-id created the resource
- resource visibility, can be public (value "1") or private ("0")
- action-id, e.g. "CREATE", "MANAGE", "DELETE"

---------------------------------------------------------------------------------------
					SOFTWARE PREREQUISITES
---------------------------------------------------------------------------------------

1) REQUIRED SOFTWARE

- Open Nebula 2.2.1. Notice, "oneadmin" does not follow external authorization systems thus some users should be created:
	$oneuser create user_name user_password

- Rybu 1.8. Some additional ruby-gems may be needed to meet runtime dependencies:
	$ gem install sequel json sinatra thin savon

- Tomcat 7 application server to host the PDP. Installation notes can be found here
	http://diegobenna.blogspot.com/2011/01/install-tomcat-7-in-ubuntu-1010.html

2) OPTIONAL SOFTWARE

- OpenNebula Sunstone: The Cloud Operations Center 2.2 simplifies the logging procedure to the system on behalf of other users and provides a welcome GUI to manage OpenNebula resources. Installation notes can be found here
	http://opennebula.org/documentation:rel2.2:sunstone
Notice, for Ubuntu 10 before starting Sunstone you need
	$export PATH="/var/lib/gems/1.8/bin":$PATH
	$ONE_LOCATION/bin/sunstone-server start
The SunStone will be listening at: http://localhost:4567

- HTTP/SOAP Membrane Monitor. This software behaves as a proxy between PDP and PEP and allows to visualise the traffic between these entities. The package is available for free here
	http://www.membrane-soa.org/soap-monitor/

3) PLATFORM NOTES

- Ubuntu 10. Make sure that the patch package is installed on your system. Otherwise, run
	$sudo aptitude install patch

---------------------------------------------------------------------------------------
					INSTALLATION NOTES
---------------------------------------------------------------------------------------

1) PAP

Examples of XACML security policies are placed in 
	/openNebula_xacml_access_control/PAP/contrail-policies/

- /1/001policy.xml: any subject can MANAGE any vm
- /1/002policy.xml: subject USER_ID can MANAGE any vm
- /1/003policy.xml: subject USER_ID can MANAGE vm VM_ID 
- /1/004policy.xml: subject USER_ID can MANAGE any vm if VM_IDc.OWNER=USER_ID
- /1/005policy.xml: subject USER_ID can MANAGE any PUBLIC vm 

- /2/001policy.xml: any subject can CREATE a new vm
- /2/002policy.xml: subject USER_ID can CREATE a new vm

- /3/001policy.xml: any subject can CREATE/MANAGE a new/any vm
- /3/002policy.xml: subject USER_ID can CREATE/MANAGE a new/any vm

2) PDP

Deploy the PDP  "contrailPDPwebApplication.war" in the Tomcat application server and make sure that it is running by checking the PDP WSDL file (assuming that the Tomcat and the OpenNebula host are installed on the same machine)
	http://localhost:8080/contrailPDPwebApplication/contrailPDPsoap?wsdl

You need to load a policy according to which the PDP will evaluate incoming requests. Go to 
	http://localhost:8080/contrailPDPwebApplication/index.isp
and insert the policy into the form, then click "upload the policy". Notice, you can copy and paste any of the policy examples shipped with the OpenNebula XACML access control system and stored in the PAP folder. 

3) PEP

You simply need to apply a patch which will automatically plug the XACML PEP into the OpenNebula and configure the OpenNebula XACML access control system as the primary one
	$patch -Np1 -d $ONE_LOCATION 
		< /openNebula_xacml_access_control/PEP/001_access_control_pep_cnr.patch

or make all the procedure manually,
- uncomment and set the following in $ONE_LOCATION/etc/oned.conf
	AUTH_MAD = [    
		executable = "$ONE/lib/mads/cnr_pep" ]
- add into the $ONE_LOCATION/etc/auth/auth.conf what PDP the PEP should contact: 
	:pdp: http://localhost:8080/contrailPDPwebApplication/contrailPDPsoap
- copy files "cnr_pep", and "cnr_pep.rb" into the "$ONE_LOCATION/lib/mads/"
    	
---------------------------------------------------------------------------------------
					OPEN ISSUES
---------------------------------------------------------------------------------------

- Security between PDP/PEP

- Adding SAML envelope

- Attribute Repository

