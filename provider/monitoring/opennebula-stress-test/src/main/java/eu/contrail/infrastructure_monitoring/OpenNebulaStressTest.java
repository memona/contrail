package eu.contrail.infrastructure_monitoring;

public class OpenNebulaStressTest {
	
	public static void main(String[] args) {
		StressTest st = new StressTest();
		String propsFile = null;
		if(args != null && args.length > 0)
			propsFile = args[0];
		
		st.testSynchronizedShutdown(propsFile);
	}
}
