package eu.contrail.infrastructure_monitoring;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.opennebula.client.OneResponse;

class StressMachinePool {
	private ArrayList<StressMachine> stressMachines;
	private boolean testStarted = false;
	private ArrayList<String> errors = new ArrayList<String>();
	private static Logger log = Logger.getLogger(StressMachinePool.class);
	
	public StressMachinePool() {
		this.stressMachines = new ArrayList<StressMachine>();
	}
	
	public boolean areAllRunning() {
		for(StressMachine sm : this.stressMachines) {
			if(!sm.isRunning())
				return false;
		}
		return true;
	}
	
	public boolean areAllFinished() {
		for(StressMachine sm : this.stressMachines) {
			if(!sm.isTestFinished())
				return false;
		}
		return true;
	}
	
	public boolean shutdownAll() {
		boolean ok = true;
		OneResponse rep = null;
		
		for(StressMachine sm : this.stressMachines) {
			rep = sm.destroy();
			if(rep.isError()) {
				if(ok)
					ok = false;
				
				errors.add(rep.getErrorMessage());
			}
		}
		
		return ok;
	}
	
	public ArrayList<String> getErrors() {
		return errors;
	}
	
	public String startNewVm(String name, String stressParams) {
		StressMachine sm = new StressMachine(name, stressParams);
		log.info("Provisioning new stress VM " + name);
		OneResponse rep = sm.provision();
		this.stressMachines.add(sm);
		if(rep.isError())
			return rep.getErrorMessage();
		
		return null;
	}
	
	public ArrayList<StressMachine> getStressMachines() {
		return stressMachines;
	}

	public boolean isTestStarted() {
		return testStarted;
	}

	public void setTestStarted(boolean testStarted) {
		this.testStarted = testStarted;
	}
}
