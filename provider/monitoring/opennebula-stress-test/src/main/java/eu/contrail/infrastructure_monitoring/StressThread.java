package eu.contrail.infrastructure_monitoring;

import org.apache.log4j.Logger;

public class StressThread extends Thread {
	private SshConnector ssh;
	private static final String command_pfx = "stress";
	private static String command;
	private static final float LOAD_AVG_CAP = (float) 1.0;
	private StressMachine sm;
	public boolean stressed;
	private int numMetrics;
	private boolean running;
	
	private static Logger log = Logger.getLogger(StressThread.class);
	
	public StressThread(StressMachine sm, String stressParams) {
		this.sm = sm;
		this.setNumMetrics(0);
		this.stressed = false;
		command = command_pfx + " " + stressParams;
		this.running = true;
	}
	
	public void storeLoadAvg() {
		float fVal = getLoadAvg();
		if(fVal >= 0) {
			sm.addLoadAvg(fVal);
			setNumMetrics(getNumMetrics() + 1);
			log.debug("Current load of " + StressMachine.VM_NAME_PFX + sm.getName() + ": " + sm.getLoadAvg());		
		}
	}

	public float getLoadAvg() {
		if(ssh != null && ssh.isReady()) {
			String[] vmResponse = null;
			vmResponse = ssh.exec("uptime", false, false);
			String repVal = "0";
			if(vmResponse != null && vmResponse.length == 1) {
				String find = "load average: ";
				if(vmResponse[0] != null && !vmResponse[0].isEmpty()) {
					String loadAvgRow = vmResponse[0].substring(vmResponse[0].indexOf(find) + find.length());
					String[] loadAvgs = loadAvgRow.split(",");
					repVal = loadAvgs[0].trim();
					return Float.parseFloat(repVal);
				}
				return 0;
			}
		}
		else {
			log.debug("SSH for " + sm.getName() + " not ready");
		}
		return -1;
	}
	
	public void run() {
		while(running) {
			log.trace("stress thread running...");
			if(ssh != null && ssh.isReady() && !stressed) {
				float la = getLoadAvg();
				// Do not stress the machine util it's load avg is above the cap
				if(la < LOAD_AVG_CAP) {
					sm.setStart(System.currentTimeMillis());
					this.stressed = true;
					log.debug("SSH for " + sm.getName() + " ready ...");
					log.info("Executing stress test for " + sm.getName() + " with '" + command + "'\n");
					running = false;
					this.ssh.exec(command, false, false);	
				}
				else
					log.info("Not stressing " + sm.getName() + " - load average still too high after boot: " + String.valueOf(la) + "\n");
			}
			else {
				try {
					log.debug("SSH for " + sm.getName() + " not ready");
					ssh = new SshConnector(sm.getIpAddress());
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
		}
	}

	public int getNumMetrics() {
		return numMetrics;
	}

	public void setNumMetrics(int numMetrics) {
		this.numMetrics = numMetrics;
	}
}
