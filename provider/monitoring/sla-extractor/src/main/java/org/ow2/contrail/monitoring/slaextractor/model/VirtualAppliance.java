package org.ow2.contrail.monitoring.slaextractor.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

public class VirtualAppliance implements SLOTarget {
    @Element
    private String sid;

    public VirtualAppliance(String sid) {
        this.sid = sid;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
