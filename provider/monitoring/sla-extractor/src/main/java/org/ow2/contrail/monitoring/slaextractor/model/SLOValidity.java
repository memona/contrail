package org.ow2.contrail.monitoring.slaextractor.model;

import org.simpleframework.xml.Element;

import java.util.Date;

public class SLOValidity {
    @Element
    private Date effectiveFrom;

    @Element
    private Date effectiveUntil;

    @Element
    private SLOValidityType schedule;

    public Date getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public Date getEffectiveUntil() {
        return effectiveUntil;
    }

    public void setEffectiveUntil(Date effectiveUntil) {
        this.effectiveUntil = effectiveUntil;
    }

    public SLOValidityType getSchedule() {
        return schedule;
    }

    public void setSchedule(SLOValidityType schedule) {
        this.schedule = schedule;
    }

    public static enum SLOValidityType {
        NON_STOP
    }
}
