package org.ow2.contrail.monitoring.slaextractor.slasoiy2;

import org.ow2.contrail.monitoring.slaextractor.exceptions.SlaParsingException;
import org.ow2.contrail.monitoring.slaextractor.model.*;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.*;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.ResourceType;
import org.slasoi.slamodel.sla.*;

import java.util.ArrayList;
import java.util.List;

public class SlaExtractor {
    private SLA sla;

    public SlaExtractor(String slaXml) throws SlaParsingException {
        SLASOIParser slasoiParser = new SLASOIParser();
        try {
            sla = slasoiParser.parseSLA(slaXml);
        }
        catch (Exception e) {
            throw new SlaParsingException("Failed to parse the SLA: " + e.getMessage(), e);
        }
    }

    public MonitoringConfiguration extractQoSConstraints() throws SlaParsingException {
        MonitoringConfiguration monConf = new MonitoringConfiguration();
        monConf.setUuid(sla.getUuid().toString());
        monConf.setEffectiveFrom(sla.getEffectiveFrom().getValue().getTime());
        monConf.setEffectiveUntil(sla.getEffectiveUntil().getValue().getTime());

        for (AgreementTerm agreementTerm : sla.getAgreementTerms()) {
            processAgreementTerm(agreementTerm, monConf);
        }

        return monConf;
    }

    /**
     * Extracts all corresponding OVF files for given SLA.
     *
     * @return List of OVF files URLs
     */
    public List<String> getOVFs() {
        List<String> ovfList = new ArrayList<String>();
        for (InterfaceDeclr interfaceDeclr : sla.getInterfaceDeclrs()) {
            Interface intrface = interfaceDeclr.getInterface();
            if (intrface instanceof ResourceType) {
                String type = ((ResourceType) intrface).getName();
                if (type.equals("OVFDescriptor")) {
                    String ovfURL = interfaceDeclr.getPropertyValue(new STND("OVF_URL"));
                    ovfList.add(ovfURL);
                }
            }
        }

        return ovfList;
    }

    private void processAgreementTerm(AgreementTerm agreementTerm, MonitoringConfiguration monConf) throws SlaParsingException {
        ServiceLevelObjective slo = new ServiceLevelObjective(agreementTerm.getId().toString());

        for (Guaranteed guaranteed : agreementTerm.getGuarantees()) {
            if (guaranteed instanceof Guaranteed.State) {
                processGuaranteedState(guaranteed, agreementTerm, slo);
            }
        }

        if (slo.getGuarantees().size() > 0) {
            monConf.addServiceLevelObjective(slo);
        }
    }

    private void processGuaranteedState(Guaranteed guaranteedState, AgreementTerm agreementTerm,
                                        ServiceLevelObjective slo) throws SlaParsingException {
        String qosTerm = guaranteedState.getId().toString();

        FunctionalExpr appliesToExpr =
                (FunctionalExpr) ((TypeConstraintExpr) (((Guaranteed.State) guaranteedState).getState())).getValue();
        ValueExpr[] parameters = appliesToExpr.getParameters();
        assert parameters.length == 1;
        String appliesTo = parameters[0].toString();
        String metricName = guaranteedState.getId().getValue();
        Metric metric = new Metric();
        metric.setName(metricName);

        SLOTarget target = getSLOTarget(qosTerm, agreementTerm, appliesTo);
        metric.setTarget(target);

        Guarantee guarantee = new Guarantee(guaranteedState.getId().toString());
        slo.addGuaranteed(guarantee);


        DomainExpr domain =
                ((TypeConstraintExpr) (((Guaranteed.State) guaranteedState).getState())).getDomain();
        String operatorUri = ((SimpleDomainExpr) domain).getComparisonOp().getValue().toString();
        String operatorName = operatorUri.substring(operatorUri.indexOf('#') + 1);
        Operator operator = getOperator(operatorName);

        ValueExpr valueExpr = ((SimpleDomainExpr) domain).getValue();
        String threshold;
        String dataType;
        if (valueExpr instanceof CONST) {
            threshold = ((CONST) valueExpr).getValue();
            dataType = ((CONST) valueExpr).getDatatype().getValue();
        }
        else if (valueExpr instanceof ID) {
            String variableName = ((ID) valueExpr).getValue();
            VariableDeclr variableDeclr = agreementTerm.getVariableDeclr(variableName);
            CONST customisedValue = ((Customisable) variableDeclr).getValue();
            threshold = customisedValue.getValue();
            dataType = customisedValue.getDatatype().getValue();
        }
        else {
            throw new SlaParsingException("SLA format is not supported.");
        }

        SLOValidity validity = new SLOValidity();
        guarantee.setValidity(validity);
        validity.setEffectiveFrom(sla.getEffectiveFrom().getValue().getTime());
        validity.setEffectiveUntil(sla.getEffectiveUntil().getValue().getTime());
        validity.setSchedule(SLOValidity.SLOValidityType.NON_STOP);

        String unit = "";
        if (dataType.startsWith("http://www.slaatsoi.org/coremodel/units#")) {
            unit = dataType.substring(dataType.indexOf('#') + 1);
        }

        Quantity quantity = new Quantity(threshold, unit);
        Expression expression = new Expression(operator, metric, quantity);

        Constraint constraint = new Constraint(expression);
        guarantee.setConstraint(constraint);
    }

    private SLOTarget getSLOTarget(String qosTerm, AgreementTerm agreementTerm,
                                   String appliesTo) throws SlaParsingException {
        if (sla.getInterfaceDeclr(appliesTo) != null) {
            InterfaceDeclr interfaceDeclr = sla.getInterfaceDeclr(appliesTo);
            Interface intrface = interfaceDeclr.getInterface();
            if (intrface != null && intrface instanceof ResourceType) {
                String type = ((ResourceType) interfaceDeclr.getInterface()).getName();
                if (type.equals("OVFDescriptor")) {
                    String ovfURL = interfaceDeclr.getPropertyValue(new STND("OVF_URL"));
                    return new OVF(ovfURL);
                }
            }
            throw new SlaParsingException(
                    String.format("Unexpected syntax of InterfaceDeclr with ID '%s'.", appliesTo));
        }
        else if (agreementTerm.getVariableDeclr(appliesTo) != null) {
            VariableDeclr variableDeclr = agreementTerm.getVariableDeclr(appliesTo);
            Expr expr = variableDeclr.getExpr();
            if (expr != null && expr instanceof FunctionalExpr) {
                FunctionalExpr functionalExpr = (FunctionalExpr) expr;
                if (functionalExpr.getOperator().getValue()
                        .equals("http://www.slaatsoi.org/coremodel#subset_of")) {
                    String interfaceId = functionalExpr.getParameters()[0].toString();
                    InterfaceDeclr interfaceDeclr = sla.getInterfaceDeclr(interfaceId);
                    if (interfaceDeclr != null && interfaceDeclr.getInterface() instanceof ResourceType) {
                        String type = ((ResourceType) interfaceDeclr.getInterface()).getName();
                        if (type.equals("OVFAppliance") && interfaceDeclr.getEndpoints().length == 1) {
                            String ovfID = interfaceDeclr.getEndpoints()[0]
                                    .getPropertyValue(new STND("OVF_VirtualSystem_ID"));
                            if (ovfID != null) {
                                return new VirtualAppliance(ovfID);
                            }
                        }
                    }
                    throw new SlaParsingException(
                            String.format("Unexpected syntax of InterfaceDeclr with ID '%s'.", interfaceId));
                }
            }
            throw new SlaParsingException(
                    String.format("Unexpected syntax of VariableDeclr '%s'.", appliesTo));
        }
        else {
            throw new SlaParsingException(
                    String.format("Failed to determine target of the QoS term '%s' of the " +
                            "AgreementTerm '%s'.", qosTerm, agreementTerm.getId()));
        }
    }

    private Operator getOperator(String text) throws SlaParsingException {
        if (text.equals("greater_than")) {
            return Operator.GREATER_THAN;
        }
        else if (text.equals("greater_than_or_equals")) {
            return Operator.LESS_THAN;
        }
        else if (text.equals("less_than")) {
            return Operator.LESS_THAN;
        }
        else if (text.equals("less_than_or_equals")) {
            return Operator.LESS_THAN;
        }
        else if (text.equals("equals")) {
            return Operator.EQUAL;
        }
        else {
            throw new SlaParsingException(String.format("Invalid operator encountered: %s.", text));
        }
    }
}
