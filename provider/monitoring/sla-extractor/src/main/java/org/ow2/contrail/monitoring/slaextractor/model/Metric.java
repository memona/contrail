package org.ow2.contrail.monitoring.slaextractor.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementUnion;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

@Root(name="metric")
public class Metric implements Operand {
    @Element
    private String name;

    @Element
    @Path("target")
    @ElementUnion({
            @Element(name = "OVF", type = OVF.class),
            @Element(name = "appliance", type = VirtualAppliance.class)
    })
    private SLOTarget target;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SLOTarget getTarget() {
        return target;
    }

    public void setTarget(SLOTarget target) {
        this.target = target;
    }
}
