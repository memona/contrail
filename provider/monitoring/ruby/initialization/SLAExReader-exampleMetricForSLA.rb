require 'rubygems'
require 'amqp'

EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)
        
        
    exchange2 = channel.topic("input.host", :auto_delete => true)
    metricRegMsg = File.open("metrics/opennebula-common-example.xml").read
    exchange2.publish(metricRegMsg, :routing_key => "input.host.n0008-xc1-xlab-lan.common.#")
    
   #let's wrap it up
    showStopper = Proc.new { connection.close { EventMachine.stop} }
    Signal.trap "TERM", showStopper
    EM.add_timer(5, showStopper)
  end
end