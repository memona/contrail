require 'rubygems'
require 'amqp'

EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)
        
        
    exchange2 = channel.topic("registration.metrics", :auto_delete => true)
    metricRegMsg = %{<Message resource="scalarix" sid="n0008.xc1.xlab.lan" source="host">
     <Metric id="mem_free">
       <Type>INTEGER</Type>
       <Unit>KB</Unit>
     </Metric>
     <Metric id="num_processes">
       <Type>INTEGER</Type>
       <Unit />
     </Metric>
    #</Message>}
    exchange2.publish(metricRegMsg, :routing_key => "registration.metrics.#")
    
   #let's wrap it up
    showStopper = Proc.new { connection.close { EventMachine.stop} }
    Signal.trap "TERM", showStopper
    EM.add_timer(5, showStopper)
  end
end    