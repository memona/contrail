require 'rubygems'
require 'pp'
require 'amqp'

load 'xml2ostruct2json.rb'

include XMLTranslations

=begin
part where metrics registration happens. Messages of the form:
<Message resource="scalarix" sid="n0008.xc1.xlab.lan" source="host">
 <Metric id="mem_free">
   <Type>INTEGER</Type>
   <Unit>KB</Unit>
 </Metric>
 <Metric id="num_processes">
   <Type>INTEGER</Type>
   <Unit />
 </Metric>
 ...
</Message>

Are translated into patterns:
host.scalarix.n0008-xc1-xlab-lan.mem_free
host.scalarix.n0008-xc1-xlab-lan.num_processes
...

This script does the given translation. In order for it to be used 
in a QueueManager, it must be loaded in its scope.
=end

#metricRegMsg = %{<Message resource="scalarix" sid="n0008.xc1.xlab.lan" source="host">
# <Metric id="mem_free">
#   <Type>INTEGER</Type>
#   <Unit>KB</Unit>
# </Metric>
# <Metric id="num_processes">
#   <Type>INTEGER</Type>
#   <Unit />
# </Metric>
#</Message>}

metricMsgTemplate = %{<Message resource="" sid="" source="">
 <Metric id="">
   <Type></Type>
   <Unit></Unit>
 </Metric>
</Message>}

@@metricsCount = 0

EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)

    #exchange = channel.topic("input.opennebula", :auto_delete => true)
    exchange = channel.topic("registration.metrics", :auto_delete => true)
    channel.queue().bind(exchange, :routing_key => "registration.metrics.#").subscribe do
      |metadata, metricRegMsg|
    
      metrics = []
      begin
        return if not XMLTranslations::conform2template?(metricMsgTemplate, metricRegMsg)
  
        objMsg = XMLTranslations::xml2ostruct(metricRegMsg)
        metrics = objMsg.message.metric.map { |m| m.id }
        source = objMsg.message.source
        sid = objMsg.message.sid.gsub(".", "-")
        #resource = objMsg.message.resource
        group = objMsg.message.group
        #metrics.map! { |m| "#{source}.#{resource}.#{sid}.#{m}" }
        metrics.map! { |m| "#{source}.#{sid}.#{group}.#{m}" }
        metrics << "#{source}.#{sid}.#{group}"
      rescue
        puts "error: wrong or incomplete xml"
        puts metricRegMsg
        metrics = nil
      end

      pp metrics
      
      #for each metrics create an input chanel
      #register a block for it
      exchangeBlock = channel.topic("blocks", :auto_delete => true)
      exchangeOutput = channel.topic("outputs", :auto_delete => true)
      
      metrics.each { 
        |metricKey|
        block = %q{ {
          |x| 
          puts "Metric msg: #{x}"
          x
          } 
        }
        exchangeBlock.publish(block, :routing_key => "blocks.metric#{@@metricsCount}.input.#{metricKey}.#")
        #automaticly connect output to the accounting component for storage.
        #exchangeOut.publish("input.accunting.#{metricKey}", :routing_key => "outputs.metric#{@@metricsCount}.input.#{metricKey}.#")    
        
        @@metricsCount += 1
      }
      
    end
    
    
  end
end

