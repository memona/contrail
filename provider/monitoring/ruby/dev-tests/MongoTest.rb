require 'rubygems'
require 'pp'
require 'mongoid'
 
Mongoid.configure do |config|
  name = "mongoid_test_db"
  host = "localhost"
  port = 27017
  config.database = Mongo::Connection.new.db(name)
end

class TestEntry
  include Mongoid::Document
  
  field :entry_id
  field :title, :type => String
  field :time, :type => Time
end

#entry = TestEntry.create({
#  :title => "Naslov",
#  :entry_id => "1",
#  :time => Time.new
#})

#TestEntry.create_indexes()

res = TestEntry.where(:time => {:$lt => Time.new} )
res.each {
  |entry|
  pp entry
}
pp res

class BasicMessageEntry
  include Mongoid::Document
  
  field :body, :type => String
  field :time, :type => Time
  field :resourceSource, :type => String
  field :traceQueues, :type => Array
  field :bodyType, :type => String
  field :sourceIp, :type => String
end

#BasicMessageEntry.create_indexes()

filters = { :bodyType => "event", :resourceSource => /vm1/ }
mf = Marshal.dump(filters)
ff = Marshal.load(mf)
pp mf
pp ff


res = BasicMessageEntry.where(ff).each {
  |entry|
  pp entry
}