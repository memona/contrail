require 'rubygems'
require 'pp'
require 'ostruct'

message = OpenStruct.new
message.body = "message to be sent"
message.sourceIp = "127.0.0.1"
message.userId = "John Doe"
message.resourceSource = "VM1"
message.bodyType = "event"
message.trace = ["soruce.queue"]

pp message.inspect

msg2str = message.to_s
pp msg2str

msg2hash = message.marshal_dump
pp msg2hash

msgDump = Marshal.dump(message)
msg = Marshal.load(msgDump)
msg.trace << "second.queue"
msg.userId = "Anon"

pp msg
pp msg.marshal_dump

storage = [message, msg]

filters = {
  :sourceIp => 'sourceIp == "127.0.0.1"',
  :userId => 'userId.include?("John")',
  :bodyType => 'bodyType == "event"',
  :resourceSource => 'resourceSource == "VM1"'
}

def filterToBlock(filters)
  properFilters = filters.map {
    |x|
    key = x[0].to_s.gsub(":", "")
    fil = x[1].gsub(key, "x.#{key}")
    "( #{fil} )"
  }  
  
  code = properFilters.join(" and ")
  #strBlock = "{\n|x|\n  pp x\n  res = nil\n"
  #strBlock += "res = x if ( #{code}  )\n"
  #strBlock += "res\n}"
  strBlock = %q{
    |x|
    res = nil
    pp x.marshal_dump
    res = x if ( } + code + %q{ )
    res
  }

  pp code
  pp strBlock

  filterBlock = eval("lambda {#{strBlock}}")
  pp filterBlock
  filterBlock
end

def filter(array, filters)  
  block = filterToBlock(filters)
  res = array.map { |x| block.call(x) }
  res.compact!
  #pp res
  res
end

filteredMsgs = filter(storage, 
  :sourceIp => 'sourceIp == "127.0.0.1"',
  :userId => 'userId.include?("John")',
  :bodyType => 'bodyType == "event"',
  :resourceSource => 'resourceSource == "VM1"')

pp filteredMsgs