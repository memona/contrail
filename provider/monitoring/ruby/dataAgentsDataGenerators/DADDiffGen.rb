require 'rubygems'
require 'amqp'
require 'pp'
require 'json'

# ===============================================================
# = Any variable that needs to be set is set in the config file =
# ===============================================================

# ========
# = Code =
# ========

def generateValues(previous, metricRules, fixedValues)
  previous = {} if previous.nil?
  current = JSON.parse(previous.to_json)
  fixedValues.each { |p, v| current[p] = v }
  metricRules[:order].each {
    |param|
    pRule = metricRules[:elms][param]
    pRule.each {
      |inputParam, block|
      rParams = inputParam.map { |x| current[x] }
      current[param] = block.call(rParams)
    }
  }
  current
end

@metrics = { 
  "common" => {
    :order => [],
    :elms => {
      "hostname" => :fixed,
      "availiability" => :fixed
    }
  },  
  "memory" => {
    :order => ["used", "free"],
    :elms => {
      "total" => :fixed,
      #used is a random number modulo total memory size
      "used" => { 
        ["used", "total"] => lambda { 
          |x| 
          used = x[0]
          if used.nil?
            used = 128 
          else
            sign = Random.rand(2)
            sign = -1 if sign == 0
            used += sign * 512
            used = 128 if used < 128
            used = x[1] if used > x[1]
          end
          used
        } 
      },
      #free is total - used
      "free" => { ["total", "used"] => lambda { |x| x[0] - x[1]} }
    }
  },
  "cpu" => {
    :order => ["user", "system", "idle"], 
    :elms => {
      "cores" => :fixed, 
      "speed" => :fixed, 
      #set user percentage for user load
      "user" => { 
        ["user"] => lambda { 
          |x|
          user = x[0]
          if user.nil?
            user = 0
          else
            sign = Random.rand(2)
            sign = -1 if sign == 0
            user += sign.to_f * Random.rand(30).to_f / 100.0
            user = 0.0 if user < 0.0
            user = 1.0 if user > 1.0
          end
          user            
        } 
      },
      #idle is random modulo 1 -  user_load
      "system" => { ["user"] => lambda { 
        |x|
        Random.rand(20).to_f / 100.0
        }
      },
      #system is 1 - user - idle
      "idle" => { ["system", "user"] => lambda { |x| 1 - x[0] - x[1] } }
    }
  },  
  "disk" => {
    :order => ["used", "available"],
    :elms => {
      "total" => :fixed,
      "used" => { ["used", "total"] => lambda { 
        |x| 
        used = x[0]
        if used.nil?
          used = 60
        else
            sign = Random.rand(2)
            sign = -1 if sign == 0
            used += sign * 100
            used = 60 if used < 60
            used = x[1] if used > x[1]
          end
          used          
        } 
      },
      "available" => { ["total", "used"] => lambda {|x| x[0] - x[1] }}
    }
  }  
}


#data = []
#100.times {
#  data << generateValues(data.last, metrics["memory"], "total" => 4096 )
#}
#pp data

def addTime(metricsData, secInterval)
  time = Time.new
  timedData = metricsData.map { 
    |x| 
    x[:time] = time.to_s 
    time += secInterval
    x
  }
end

def metricsToXML(metrics)
  s = "<Message time=\"#{metrics[:time]}\">\n"
  metrics[:metrics].each {
    |id, value|
    s += "\t<Value id=\"#{id}\">#{value}</Value>\n"
  }
  s += "</Message>\n"

  s
end

def genetateMetricsForAllHosts(repeatN, hosts)
  history = {}
  repeatN.times {
    hosts.each { 
      |sid, conf| 
      history[sid] = [] if not history.has_key?(sid)
      current = {}
      conf.each {
        |group, params|
        prev = history[sid].last
        prevVal = nil
        prevVal = prev[group] if not prev.nil?
        current[group] = generateValues(prevVal, @metrics[group], params )
      }
      history[sid] << current
    }
  }
  #pp history

  allData = []
  history.each {
    |sid, values|
    data = []
    values.each {
      |val|
      val.each {
        |group, metrics|
        data << {
          :source => "host",
          :sid => sid,
          :group => group,
          :metrics => metrics
        }
      }
    }
  
    data = addTime(data, 0)
  
    allData += data
  }
  allData
  
  xmls = allData.map { |d| 
    d[:xml] = metricsToXML(d) 
    d
  }  
  
  xmls
end

puts
puts
#pp allData

# =================================
# = Test parameters and execution =
# =================================

load "SourceConfig.rb"

data = genetateMetricsForAllHosts(@@N_OUTPUT, @hosts)


EventMachine.run do  
  AMQP.connect(:host => '127.0.0.1') do |connection|
    channel = AMQP::Channel.new(connection)
    
    data.each {
      |info|
      exchangeName = "input.#{info[:source]}"
      routingKey = "input.#{info[:source]}.#{info[:sid].gsub(".", "-")}.#{info[:group]}"
      
      pp exchangeName, routingKey, info[:xml]
      
      #exchange = channel.topic(exchangeName, :auto_delete => true)
      #exchange.publish(info[:xml], :routing_key => routingKey)
    }

    #let's wrap it up
    showStopper = Proc.new { connection.close { EventMachine.stop} }
    Signal.trap "TERM", showStopper
    EM.add_timer(5, showStopper)
  end
end