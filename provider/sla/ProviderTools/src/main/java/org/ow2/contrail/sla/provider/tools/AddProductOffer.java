/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.tools;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.cli.*;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.Products;


public class AddProductOffer {

	private CommandLine cmd; 

	private static Options options = null;
	private static final String cmdTxName = "n";
	private static final String cmdTxDesc = "d";
//	private static final String cmdRevId = "r";
//	private static final String cmdTxStatus = "s";
	private static final String cmdProdId = "p";	
	private static final String cmdSlatId = "t";
//	private static final String cmdBillFreq = "b";
	private static final String cmdDtValFrom = "df";
	private static final String cmdDtValTo = "dt";
	private static final String cmdParty = "pa";
	private static final String cmdDtInsDate = "di";

	static {
		options = new Options();
		options.addOption(cmdTxName, true, "Product Offer name (mandatory)");
		options.addOption(cmdTxDesc, true, "Product Offer description (mandatory)");
		options.addOption(cmdProdId, true, "Product ID (mandatory)");
		options.addOption(cmdSlatId, true, "SLAT ID (mandatory)");
		options.addOption(cmdDtInsDate, true, "Insertion date (format: YYYYMMDD; default: today)");
		options.addOption(cmdDtValFrom, true, "Valid from (format: YYYYMMDD; default: today)");
		options.addOption(cmdDtValTo, true, "Valid to (format: YYYYMMDD; default: 2 years later)");
		options.addOption(cmdParty, true, "Party (mandatory)");
	}


	private static String txName;
	private static String txDesc;
	private static String revId;
	private static String txStatus;
	private static long prodId;
	private static String slatId;
	private static long billFreq;
	private static Calendar dtValFrom;
	private static Calendar dtValTo;
	private static long party;
	private static Calendar dtInsDate;

	
	private void setDate(Calendar dtInsDate, String cmdDtInsDate) {
		dtInsDate.set(Integer.parseInt(cmdDtInsDate.substring(0, 4)), 
				(Integer.parseInt(cmdDtInsDate.substring(4, 6))-1), 
			Integer.parseInt(cmdDtInsDate.substring(6, 8)));
	}



	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){

		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}

		if (!cmd.hasOption(cmdTxName) || 
				!cmd.hasOption(cmdParty) ||
				!cmd.hasOption(cmdTxDesc) ||
				!cmd.hasOption(cmdProdId) ||
				!cmd.hasOption(cmdSlatId)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("AddProductOffer", options);
			System.exit(1);
		}

		txName = cmd.getOptionValue(cmdTxName);
		txDesc = cmd.getOptionValue(cmdTxDesc);
		prodId = Long.parseLong(cmd.getOptionValue(cmdProdId));
		slatId = cmd.getOptionValue(cmdSlatId);

//		if (cmd.hasOption(cmdRevId)) {
//			revId = cmd.getOptionValue(cmdRevId); 
//		} else {
			revId = "1";
//		}

//		if (cmd.hasOption(cmdBillFreq)) {
//			billFreq = Long.parseLong(cmd.getOptionValue(cmdBillFreq)); 
//		} else {
			billFreq = 1;
//		}


//		if (cmd.hasOption(cmdTxStatus)) {
//			txStatus = cmd.getOptionValue(cmdTxStatus); 
//		} else {
			txStatus = "A";
//		}
		
		dtInsDate = new GregorianCalendar();
		if (cmd.hasOption(cmdDtInsDate)) {
			setDate(dtInsDate, cmd.getOptionValue(cmdDtInsDate));
		} 

		dtValFrom = new GregorianCalendar();
		if (cmd.hasOption(cmdDtValFrom)) {
			setDate(dtValFrom, cmd.getOptionValue(cmdDtValFrom));
		} 

		dtValTo = new GregorianCalendar();
		if (cmd.hasOption(cmdDtValTo)) {
			setDate(dtValTo, cmd.getOptionValue(cmdDtValTo));
		} 

		party = Long.parseLong(cmd.getOptionValue(cmdParty));

		System.out.println("\nUsing: \nOffer name: " + txName + "\nParty: " + party +
				"\nOffer desc: " + txDesc + "\nSLAT ID: " + slatId +
				"\nProduct ID: " + prodId + "\nValid From:" + dtValFrom + "\nValid To:" + dtValTo);

	}	



	/**
	 * Main program 
	 * This program adds a new Product Offer in the business SLA template repository
	 * of SLA@SOI. The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI runs
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {

		System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));

		AddProductOffer inst = new AddProductOffer();
		inst.loadArgs(args);
		
		Products ssprods = new Products();
		try {
			ssprods.createProductOffer(prodId, txName, txDesc,dtInsDate, dtValFrom, dtValTo, 
					revId, txStatus, billFreq, slatId, party);
		} catch (Exception e) {
			System.err.println("Error while adding product offer");
			e.printStackTrace();
		}

	}





}
