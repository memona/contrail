/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.tools;

import java.io.IOException;

import org.apache.xmlbeans.XmlException;
import org.apache.commons.cli.*;

import org.ow2.contrail.sla.provider.slasoi.Products;
import org.slasoi.businessmanager.ws.impl.BusinessManager_QueryProductCatalogStub.Product;


public class GetProductList {

	private CommandLine cmd; 

	private static String host = "localhost";
	private static String port = "8080";
	private static String customerId = "0";

	private static Options options = null;
	private static final String cmdCustOpt = "c";
	private static final String cmdHostOpt = "host";
	private static final String cmdPortOpt = "port";

	static {
		options = new Options();
		options.addOption(cmdCustOpt, true, "Customer ID");
		options.addOption(cmdHostOpt, true, "SLA@SOI host (default: localhost)");
		options.addOption(cmdPortOpt, true, "SLA@SOI port (default: 8080)");
	}



	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){

		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}

		if (cmd.hasOption(cmdHostOpt))
			host = cmd.getOptionValue(cmdHostOpt);

		if (cmd.hasOption(cmdPortOpt))
			port = cmd.getOptionValue(cmdPortOpt);

		if (cmd.hasOption(cmdCustOpt))
			customerId = cmd.getOptionValue(cmdCustOpt);


		System.out.println("\nUsing: \nCustomer ID: " + customerId + 
				"\nHost: " + host + "\nPort: " + port);

	}	



	/**
	 * Main program 
	 * This program gets the list of products from the business SLA template repository
	 * of SLA@SOI, given the customer ID as input. The program connects to the web services
	 * running on the socket specified (by default this is localhost:8080) 
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {

		GetProductList inst = new GetProductList();
		inst.loadArgs(args);

		Products ssprods = new Products();
		Product[] prods = ssprods.getProducts(customerId, host, port);
		try {
			int i = 0;
			for (Product prod : prods) {
				System.out.println("_____________________________________________________");
				System.out.println("-----> Product " + ++i + " of " + prods.length);
				System.out.println("ID: " + prod.getId());
				System.out.println("Name: " + prod.getName());
				System.out.println("Description: " + prod.getDescription());
				System.out.println("Brand: " + prod.getBrand());

			}
		} catch (Exception e) {
			System.err.println("Error while retrieving products.");
			e.printStackTrace();
		}
	}



}
