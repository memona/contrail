/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.ow2.contrail.sla.provider.tools.pricing;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.PricedItems;

public class AddUnitResource {
	
private CommandLine cmd; 
	
	private static Options options = null;
	private static final String cmdURNameOpt = "n";
	private static final String cmdURDescrOpt = "d";
	private static final String cmdTypeOpt = "t";
	private static final String cmdInstanceId = "i";
	private static final String cmdUnit = "u";
	private static final String cmdReservation = "r";
		
	static {
		options = new Options();
		options.addOption(cmdURNameOpt, true, "Unit Resource Name (mandatory)");
		options.addOption(cmdURDescrOpt, true, "Unit Resource Description");
		options.addOption(cmdTypeOpt, true, "Unit Resource Type (mandatory)");
		options.addOption(cmdInstanceId, true, "Unit Resource InstanceId ");
		options.addOption(cmdUnit, true, "Unit Resource Unit");
		options.addOption(cmdReservation, true, "OVF Resource Reservation [yes or no] (default=no)");
	}
	
	
	private static String unitResourceName;
	private static String unitResourceDescr;
	private static String unitResourceType;
	private static String unitResourceInstanceId;
	private static String unitResourceUnit;
	private static boolean ovfReservation=false;
	
	
	
	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){
		
		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}
		
		if (!cmd.hasOption(cmdURNameOpt) || !cmd.hasOption(cmdTypeOpt)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("AddUnitResource", options);
			System.exit(1);
		}
		
		unitResourceName = cmd.getOptionValue(cmdURNameOpt);
		if(cmd.hasOption(cmdUnit))
			unitResourceDescr = cmd.getOptionValue(cmdURDescrOpt);
		unitResourceType = cmd.getOptionValue(cmdTypeOpt);
		if(cmd.hasOption(cmdReservation)){
			if(cmd.getOptionValue(cmdReservation).equalsIgnoreCase("yes"))
				ovfReservation=true;
		}
		unitResourceDescr = cmd.getOptionValue(cmdURDescrOpt);
		unitResourceInstanceId = cmd.getOptionValue(cmdInstanceId);
		if(cmd.hasOption(cmdUnit))
			unitResourceUnit = cmd.getOptionValue(cmdUnit);

		/*		ursIds = cmd.hasOption(cmdUnitResIds) ? 
			ContrailSlaManagerContext.getSlamDbName(cmd.getOptionValue(cmdSlamId)) : 
			ContrailSlaManagerContext.getSlamDbName(null);*/
		
		System.out.println("\nUsing: \nUnit Resource name: " + unitResourceName + 
			"\nUnit Resource description: " + unitResourceDescr + "\nUnit Resource type: " + unitResourceType +
			"\nUnit Resource Instance Id: " + unitResourceInstanceId + "\nUnit Resource Unit: " + unitResourceUnit +
			"\nUnit Resource reservation: " + ovfReservation );

	}
	
	
	/**
	 * Main program 
	 * This program adds a new UR Resource into repository
	 * of SLA@SOI. The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI is running
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {
		
		AddUnitResource inst = new AddUnitResource();
		
		inst.loadArgs(args);
		
        System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));
    
    	PricedItems pricedItems = new PricedItems();
    	try {
    		pricedItems.createUnitResource(unitResourceName, unitResourceDescr, ovfReservation, unitResourceInstanceId, unitResourceType, unitResourceUnit);
    	} catch (Exception e) {
    		System.err.println("Error while adding Unit Resource");
    		e.printStackTrace();
    	}
	}

}
