/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.sla.provider.slasoi;

import javax.annotation.Resource;

import org.slasoi.businessManager.common.service.GuaranteeManager;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Guarantees {
	
	@Resource
	private GuaranteeManager guaranteeManager;

	
	private ClassPathXmlApplicationContext context = null;
	
	public Guarantees() {
		context = new ClassPathXmlApplicationContext("META-INF/spring/contrail/bm-common-context.xml");
		guaranteeManager = (GuaranteeManager)context.getBean("guaranteeService");
	}
	
	public Long createGuarantee(String name, String slaTemplateId, String agreementTerm, String description, String type, String operator, String value, String valueType, double per_variation){
		return guaranteeManager.createGuarantee(name, slaTemplateId, agreementTerm, description, type, operator, value, valueType, per_variation);
	}
	
	public void associateProduct(Long productId, Long guaranteeId){
		guaranteeManager.associateProduct(productId, guaranteeId);
	}
	
	public void deleteGuarantee(Long id){
		guaranteeManager.deleteGuarantee(id);
	}

}
