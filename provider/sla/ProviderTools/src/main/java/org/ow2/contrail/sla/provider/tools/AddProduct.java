/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.tools;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.cli.*;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.Products;


public class AddProduct {

	private CommandLine cmd; 

	private static Options options = null;
	private static final String cmdTxName = "n";
//	private static final String cmdTxCat = "c";options.addOption(cmdTxCat, true, "Product category");
	private static final String cmdTxDesc = "d";
	private static final String cmdTxBrand = "b";
//	private static final String cmdTxRel = "r";options.addOption(cmdTxRel, true, "Release (default: 1.0)");
	private static final String cmdParty = "p";
//	private static final String cmdTxStatus = "s";options.addOption(cmdTxStatus, true, "Status (default: 'A')");
	private static final String cmdDtInsDate = "di";
	private static final String cmdDtValFrom = "df";
	private static final String cmdDtValTo = "dt";

	static {
		options = new Options();
		options.addOption(cmdTxName, true, "Product name (mandatory)");
		options.addOption(cmdTxDesc, true, "Product description (mandatory)");
		options.addOption(cmdTxBrand, true, "Product brand (mandatory)");
		options.addOption(cmdParty, true, "Party (mandatory)");
		options.addOption(cmdDtInsDate, true, "Insertion date (format: YYYYMMDD; default: today)");
		options.addOption(cmdDtValFrom, true, "Valid from (format: YYYYMMDD; default: today)");
		options.addOption(cmdDtValTo, true, "Valid to (format: YYYYMMDD; default: 2 years later)");
	}


	private static String txName;
	private static String txDesc;
	private static String txBrand;
	private static String txRel;
	private static long party;
	private static BigDecimal maxPenCust;
	private static BigDecimal maxPenCanc;
	private static BigDecimal maxPenRem;
	private static BigDecimal unsuccRounds;
	private static BigDecimal maxPrice;
	private static String txNegType;
	private static String txStatus;
	private static Calendar dtInsDate;
	private static Calendar dtValFrom;
	private static Calendar dtValTo;


	private void setDate(Calendar dtInsDate, String cmdDtInsDate) {
		dtInsDate.set(Integer.parseInt(cmdDtInsDate.substring(0, 4)), 
				(Integer.parseInt(cmdDtInsDate.substring(4, 6))-1), 
				Integer.parseInt(cmdDtInsDate.substring(6, 8)));
	}



	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){

		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}

		if (!cmd.hasOption(cmdTxName) || 
				!cmd.hasOption(cmdTxDesc) ||
				!cmd.hasOption(cmdTxBrand) ||
				!cmd.hasOption(cmdParty) ) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("AddProduct", options);
			System.exit(1);
		}

		txName = cmd.getOptionValue(cmdTxName);
		txDesc = cmd.getOptionValue(cmdTxDesc);
		txBrand = cmd.getOptionValue(cmdTxBrand);

//		if (cmd.hasOption(cmdTxRel)) {
//			txRel = cmd.getOptionValue(cmdTxRel); 
//		} else {
			
//		}
		txRel = "1";
		party = Long.parseLong(cmd.getOptionValue(cmdParty));
		unsuccRounds = new BigDecimal(100);
		maxPrice = new BigDecimal(100);


			txNegType = "A";
			maxPenCust=  new BigDecimal(100);
			maxPenCanc=  new BigDecimal(100);
			maxPenRem=  new BigDecimal(100);
			txStatus = "A";

//		if (cmd.hasOption(cmdTxStatus)) {
//			txStatus = cmd.getOptionValue(cmdTxStatus); 
//		} else {
			
//		}


			

		
		dtInsDate = new GregorianCalendar();
		if (cmd.hasOption(cmdDtInsDate)) {
			setDate(dtInsDate, cmd.getOptionValue(cmdDtInsDate));
		} 

		dtValFrom = new GregorianCalendar();
		if (cmd.hasOption(cmdDtValFrom)) {
			setDate(dtValFrom, cmd.getOptionValue(cmdDtValFrom));
		} 

		dtValTo = new GregorianCalendar();
		if (cmd.hasOption(cmdDtValTo)) {
			setDate(dtValTo, cmd.getOptionValue(cmdDtValTo));
		} 

		System.out.println("\nUsing: \nProduct name: " + txName +
				"\nProduct desc: " + txDesc + "\nBrand: " + txBrand + "\nParty: " + party + 
				 "\nInsertion date:" + dtInsDate + 
				"\nValid From:" + dtValFrom + "\nValid To:" + dtValTo);

	}	



	/**
	 * Main program 
	 * This program adds a new Product in the slasoi DB of SLA@SOI. 
	 * The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI framework runs
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {

		System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));

		AddProduct inst = new AddProduct();
		inst.loadArgs(args);

		Products ssprods = new Products();
		try {
			ssprods.createProduct(txName, txDesc, txBrand, txRel, party, maxPenCust, 
					maxPenCanc, maxPenRem, unsuccRounds, maxPrice, txNegType, txStatus, 
					dtInsDate, dtValFrom, dtValTo);
		} catch (Exception e) {
			System.err.println("Error while adding product");
			e.printStackTrace();
		}

	}


}
