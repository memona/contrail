/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.ow2.contrail.sla.provider.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.ow2.contrail.sla.provider.slasoi.Templates;
import org.apache.commons.cli.*;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;

import eu.slaatsoi.slamodel.SLATemplateDocument;

public class AddTemplate {

	private CommandLine cmd;

	private static Options options = null;
	private static final String cmdTemplateOpt = "t";
	private static final String cmdProviderOpt = "p";
	private static final String cmdRegisterOpt = "r";
	private static final String cmdDescriptionOpt = "d";
	private static final String cmdSlamId = "slam";

	static {
		options = new Options();
		options.addOption(cmdTemplateOpt, true, "SLA Template file (mandatory)");
		options.addOption(cmdProviderOpt, true, "Provider ID (mandatory)");
		options.addOption(cmdRegisterOpt, true,
				"Register name (default: Contrail)");
		options.addOption(cmdDescriptionOpt, true,
				"SLA Template description (mandatory)");
		options.addOption(cmdSlamId, true, "SLA@SOI SLAM DB (default: BSLAM)");
	}

	private static String templateFile;
	private static String providerId;
	private static String registerId;
	private static String description = "";
	private static String slamId;

	/**
	 * Check and set command line arguments
	 * 
	 * @param args
	 *            arguments passed in main()
	 */
	private void loadArgs(String[] args) {

		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}

		if (!cmd.hasOption(cmdTemplateOpt) || !cmd.hasOption(cmdProviderOpt)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("AddTemplate", options);
			System.exit(1);
		}

		templateFile = cmd.getOptionValue(cmdTemplateOpt);
		providerId = cmd.getOptionValue(cmdProviderOpt);
		registerId = cmd.hasOption(cmdRegisterOpt) ? cmd
				.getOptionValue(cmdRegisterOpt) : "Contrail";
		description = cmd.getOptionValue(cmdDescriptionOpt);

		slamId = cmd.hasOption(cmdSlamId) ? cmd.getOptionValue(cmdSlamId)
				: null;

		System.out.println("\nUsing: \nSLA template file: " + templateFile
				+ "\nProvider ID: " + providerId + "\nRegister name: "
				+ registerId + "\nSLA Template Description: " + description
				+ "\nSLAM DB: " + slamId);

	}


	/**
	 * Main program This program adds a new SLA template in the business SLA
	 * template repository of SLA@SOI. The program connects to the local DB and
	 * must run in the same provider's machine where SLA@SOI is running
	 * 
	 * @param args
	 * @throws IOException
	 * @throws XmlException
	 */
	public static void main(String[] args) throws XmlException, IOException {

		AddTemplate inst = new AddTemplate();

		inst.loadArgs(args);

		System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));

		File f = new File(templateFile);

		try {

			XmlOptions xmlOptions = new XmlOptions();
			xmlOptions.setDocumentType(SLATemplateDocument.type);

			SLATemplateDocument slaTemplateXml = SLATemplateDocument.Factory
					.parse(new FileInputStream(f), xmlOptions);

			Templates sstemps = new Templates(slamId);
			sstemps.addTemplate(slaTemplateXml.xmlText(), description, providerId, registerId);

		} catch (Exception e) {
			System.err.println("Error while adding SLA template");
			e.printStackTrace();
		}

	}

}
