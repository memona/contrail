/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */


package org.ow2.contrail.sla.provider.slasoi.context.mockup;

import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;


public class SLARegistryMockup implements SLARegistry, SLARegistry.IRegister, SLARegistry.IQuery {
    
    private SLA sla;

    public IQuery getIQuery() {
        return this;
    }

    public IRegister getIRegister() {
        return this;
    }

    public UUID register(SLA sla, UUID[] uuids, SLAState arg2) throws RegistrationFailureException {
        this.sla = sla;
        return null;
    }

    public UUID update(UUID uuid, SLA sla, UUID[] arg2, SLAState arg3) throws UpdateFailureException {
        this.sla = sla;
        return uuid;
    }

    public SLA[] getSLA(UUID[] arg0) throws InvalidUUIDException {
    	return null;
    }

    public UUID[] getDependencies(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLAMinimumInfo[] getMinimumSLAInfo(UUID[] arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public UUID[] getSLAsByState(SLAState[] arg0, boolean arg1) throws InvalidStateException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLAStateInfo[] getStateHistory(UUID arg0, boolean arg1) throws InvalidUUIDException, InvalidStateException {
        // TODO Auto-generated method stub
        return null;
    }

    public UUID[] getUpwardDependencies(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLA[] getSLAsByParty(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public SLA[] getSLAsByTemplateId(UUID arg0) throws InvalidUUIDException {
        // TODO Auto-generated method stub
        return null;
    }

    public UUID sign(UUID arg0) throws UpdateFailureException {
        // TODO Auto-generated method stub
        return null;
    }

    public IAdminUtils getIAdmin() {
        // TODO Auto-generated method stub
        return null;
    }
    
}
