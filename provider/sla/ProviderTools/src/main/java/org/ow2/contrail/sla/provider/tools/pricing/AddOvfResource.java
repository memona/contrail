/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.ow2.contrail.sla.provider.tools.pricing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.xmlbeans.XmlException;
import org.ow2.contrail.sla.provider.slasoi.PricedItems;

public class AddOvfResource {
	
private CommandLine cmd; 
	
	private static Options options = null;
	private static final String cmdOvfNameOpt = "n";
	private static final String cmdOvfDescrOpt = "d";
	private static final String cmdTypeOpt = "t";
	private static final String cmdInfoOpt = "i";
	private static final String cmdOvfId= "id";
	private static final String cmdInstanceId = "inst";
	private static final String cmdReservation = "r";
	private static final String cmdCompResOwnedIds = "c";
	private static final String cmdUnitResIds = "URs";
		
	static {
		options = new Options();
		options.addOption(cmdOvfNameOpt, true, "Ovf Resource Name (mandatory)");
		options.addOption(cmdOvfDescrOpt, true, "Ovf Resource Description ");
		options.addOption(cmdOvfId, true, "Ovf Id (mandatory)");
		options.addOption(cmdInstanceId, true, "Ovf Instance id (default=-1)");
		options.addOption(cmdTypeOpt, true, "Ovf Resource Type");
		options.addOption(cmdInfoOpt, true, "Ovf Resource Info");
		options.addOption(cmdReservation, true, "OVF Resource Reservation [yes or no] (default=no)");
		options.addOption(cmdCompResOwnedIds, true, "Ovf Resource Compound Resource Owned Ids (default=-1)");
		options.addOption(cmdUnitResIds, true, "Comma separated Ids of UnitResource (default=-1)");
	}
						
	
	private static String ovfResourceName;
	private static String ovfResourceDescr;
	private static String ovfResourceType;
	private static String ovfResourceInfo;
	private static String ovfId;
	private static String instanceId = "-1";
	private static String ovfResourceCROwnedIds = "-1";
	private static boolean ovfReservation = false;
	private static String ursIds = "-1";
	private static ArrayList<Long> ursIdsArray;
	private static ArrayList<Long> crsIdsArray;
	
	
	
	/**
	 * Check and set command line arguments
	 * @param args arguments passed in main()
	 */
	private void loadArgs(String[] args){
		
		CommandLineParser parser = new PosixParser();
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Error parsing arguments");
			e.printStackTrace();
			System.exit(1);
		}
		
		if (!cmd.hasOption(cmdOvfNameOpt) || !cmd.hasOption(cmdOvfId)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("AddOvfResource", options);
			System.exit(1);
		}
		
		ovfResourceName = cmd.getOptionValue(cmdOvfNameOpt);
		ovfId = cmd.getOptionValue(cmdOvfId);
		if(cmd.hasOption(cmdInstanceId))
			instanceId=cmd.getOptionValue(cmdInstanceId);
		if(cmd.hasOption(cmdOvfDescrOpt))
			ovfResourceDescr = cmd.getOptionValue(cmdOvfDescrOpt);
		if(cmd.hasOption(cmdTypeOpt))
			ovfResourceType = cmd.getOptionValue(cmdTypeOpt);
		ovfResourceInfo = cmd.getOptionValue(cmdInfoOpt);
		
		if(cmd.hasOption(cmdCompResOwnedIds))
			ovfResourceCROwnedIds = cmd.getOptionValue(cmdCompResOwnedIds);
		StringTokenizer st=new StringTokenizer(ovfResourceCROwnedIds,",");
		crsIdsArray=new ArrayList<Long>();
		while(st.hasMoreTokens()){
			crsIdsArray.add(new Long(st.nextToken()));
		}
		
		if(cmd.hasOption(cmdReservation)){
			if(cmd.getOptionValue(cmdReservation).equalsIgnoreCase("yes"))
				ovfReservation=true;
		}
		if(cmd.hasOption(cmdUnitResIds))
			ursIds = cmd.getOptionValue(cmdUnitResIds);
		
		StringTokenizer st2=new StringTokenizer(ursIds,",");
		ursIdsArray=new ArrayList<Long>();
		while(st2.hasMoreTokens()){
			ursIdsArray.add(new Long(st2.nextToken()));
		}
/*		ursIds = cmd.hasOption(cmdUnitResIds) ? 
			ContrailSlaManagerContext.getSlamDbName(cmd.getOptionValue(cmdSlamId)) : 
			ContrailSlaManagerContext.getSlamDbName(null);*/
		
		System.out.println("\nUsing: \nOvf Resource name: " + ovfResourceName + 
			"\nOvf Resource description: " + ovfResourceDescr + "\nOvf Resource type: " + ovfResourceType +
			"\nOvf Resource Info: " + ovfResourceInfo + "\nOvf Id: " + ovfId + "\nOvf Resource Compound Resource Own Ids: " + ovfResourceCROwnedIds + "\nOvf Resource reservation: " + ovfReservation + "\nUnit Resource ids: " + ursIds);

	}
	
	
	/**
	 * Main program 
	 * This program adds a new Ovf Resource into repository
	 * of SLA@SOI. The program connects to the local DB and must run in the same 
	 * provider's machine where SLA@SOI is running
	 * @param args
	 * @throws IOException 
	 * @throws XmlException 
	 */
	public static void main(String[] args) throws XmlException, IOException {
		
		AddOvfResource inst = new AddOvfResource();
		
		inst.loadArgs(args);
		
        System.out.println("SLA@SOI home: " + System.getenv("SLASOI_HOME"));
    
    	PricedItems pricedItems = new PricedItems();
    	try {
    		pricedItems.createOvfResource(ovfResourceName, ovfResourceDescr, ovfReservation, ovfId, instanceId, ovfResourceType, ovfResourceInfo, ursIdsArray, crsIdsArray);
    	} catch (Exception e) {
    		System.err.println("Error while adding Ovf Resource");
    		e.printStackTrace();
    	}
	}

}
