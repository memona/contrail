package com.hp.iic.rabbitmq;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;


public class Receive {

    private final static String QUEUE_NAME = "test-channel";

    public static void main(String[] argv) throws Exception {

    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("10.15.5.52");
	factory.setUsername("slasoi");
	factory.setPassword("slasoi");
	factory.setVirtualHost("/slasoi");

    
    
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    String queue = channel.queueDeclare().getQueue();
    channel.queueBind(queue, "chris-channel", "#");
    //channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    System.out.println("Waiting for messages. To exit press CTRL+C");
    System.out.println("Listening on Host:" + factory.getHost());
    System.out.println("Listening on Port:" + factory.getPort());

    
    QueueingConsumer consumer = new QueueingConsumer(channel);
    channel.basicConsume(QUEUE_NAME, true, consumer);
    
    while (true) {
      QueueingConsumer.Delivery delivery = consumer.nextDelivery();
      String message = new String(delivery.getBody());
      System.out.println(" [x] Received '" + message + "'");
    }
  }
}