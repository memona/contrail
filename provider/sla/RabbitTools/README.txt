The RabbitTools are used for troubleshooting the RabbitMQ bus. 

Installation
------------
Compile the tools:
$ mvn clean assembly:assembly -DskipTests

Usage
-----
Running the program without arguments will prompt you the list of required
arguments:
$ java -cp target\RabbitTools-1.0-SNAPSHOT-jar-with-dependencies.jar 
com.hp.iic.rabbitmq.Publisher

publish.sh contains an example of usage. It contains the following:
java -cp target/RabbitTools-1.0-SNAPSHOT-jar-with-dependencies.jar 
com.hp.iic.rabbitmq.Publisher -h 10.15.5.52 -user slasoi -passwd slasoi -e 
INFRASTRUCTURE_PROVISIONING_RESULT_EVENT_CHANNEL -f 
violationMessageInvocationCpuLoad.xml

Customize it according to your host and RabbitMq parameters