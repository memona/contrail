{
    "disksize_low": 1000,
    "id": "/api/cimi/vmhandler/3",
    "cpufreq_low": 800,
    "corecount_low": 1,
    "corecount_high": 4,
    "resourceURI": "VEP/VMHandler",
    "ram_high": 2048,
    "name": "AverageHandler",
    "cpufreq_high": 2400,
    "ram_low": 512,
    "disksize_high": 4
}