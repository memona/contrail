/**
 * Copyright 2012 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */
package org.contrail.vep.resource;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@Path("/api/cimi")
public class RestVepResourceMock {

	@GET
	@Produces("application/json")
	public JSONObject mockedRootResponse() {
		JSONObject reply = null;
		try {
			reply = new JSONObject(loadResponse("mock-test.txt"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reply;

	}

	@GET
	@Path("/VMHandlers")
	@Produces("application/json")
	public JSONObject mockedGetVmHanldersResponse() {
		JSONObject reply = null;
		try {
			reply = new JSONObject(loadResponse("mock-vmhandlers.txt"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reply;
	}

	@GET
	@Path("/networkHandlers")
	@Produces("application/json")
	public JSONObject mockedGetNetworkHanldersResponse() {
		JSONObject reply = null;
		try {
			reply = new JSONObject(loadResponse("mock-nethandlers.txt"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reply;
	}

	@GET
	@Path("/constraints")
	@Produces("application/json")
	public JSONObject mockedGetConstraintsResponse() {
		JSONObject reply = null;
		try {
			reply = new JSONObject(loadResponse("mock-constraints.txt"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reply;
	}

	@GET
	@Path("/VMHandler/{id}")
	@Produces("application/json")
	public JSONObject mockedGetVmHanlderResponse(@PathParam("id") String id) {
		JSONObject response = null;
		try {
			response = new JSONObject(loadResponse("mock-vmhandler" + id + ".txt"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	@GET
	@Path("/networkHandler/{id}")
	@Produces("application/json")
	public JSONObject mockedGetNetworkHanlderResponse(@PathParam("id") String id) {
		JSONObject response = null;
		try {
			response = new JSONObject(loadResponse("mock-nethandler" + id + ".txt"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
		    
		    
	@POST
	@Path("/cee")
	@Produces("application/json")
	@Consumes("application/json")
	public JSONObject mockedCEEResponse(JSONObject ceeJson) {
		JSONObject response = new JSONObject();;
		System.out.println("Inside CEE rest response");
		System.out.println(ceeJson.toString());
		try {
			int k=0;
			JSONArray jsonArRes=ceeJson.optJSONArray("applications").optJSONObject(k).optJSONArray("reservations");
			if (jsonArRes == null) {
				String str=ceeJson.optString("state");
				if (str.equals("ACTIVE")){				
					response.put("id", "api/cimi/cee/123456789");
				} else {
					JSONArray jArray = new JSONArray();
					JSONArray vsArray = ceeJson.getJSONArray("defaultMapping");
					for (int i = 0; i < vsArray.length(); i++) {
						String vs = (String) ((JSONObject) (vsArray.getJSONObject(i).get("virtualSystem"))).get("href");
						JSONObject element = new JSONObject();
						element.put("deployability", "true");
						element.put("count", "1");
						JSONObject hre = new JSONObject();
						hre.put("href", vs);
						element.put("virtualSystem", hre);
						jArray.put(element);
					}
					response.put("virtualSystems", jArray);
				}
			} else {
				if (ceeJson.get("state").equals("ACTIVE")) {
					response.put("id", "api/cimi/cee/123456789");
				} else {
					JSONArray jArray = new JSONArray();
					JSONArray vsArray = ceeJson.optJSONArray("defaultMapping");
					for (int i = 0; i < vsArray.length(); i++) {
						String vs = (String) ((JSONObject) (vsArray.optJSONObject(i).get("virtualSystem"))).get("href");
						JSONObject element = new JSONObject();
						element.put("deployability", "true");
						JSONObject hre = new JSONObject();
						hre.put("href", vs);
						element.put("virtualSystem", hre);
						jArray.put(element);
					}
					response.put("virtualSystems", jArray);
					response.put("reservations", jsonArRes);
				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
		    
		    
		    
		    
	private String loadResponse(String filename) {
		try {

			InputStream is = getClass().getClassLoader().getResourceAsStream(filename);

			BufferedReader r = new BufferedReader(new InputStreamReader(is));
			StringBuilder total = new StringBuilder();
			String line;
			while ((line = r.readLine()) != null) {
				total.append(line);
			}

			System.out.println("Message loaded");
			System.out.println(total.toString());
			is.close();
			r.close();
			return total.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}

	}
	
}
