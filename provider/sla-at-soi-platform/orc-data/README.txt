This folder hosts the templates and other configuration files needed to run the SLA@SOI platform integration tests.

This folder is referenced from within the source code of SLA@SOI platform in order to load the platform and ORC related configuration data.

In order to properly use it, please set the following environment variables so that they both point to this folder (trunk\common\osgi-config):

SLASOI_HOME
SLASOI_ORC_HOME 

