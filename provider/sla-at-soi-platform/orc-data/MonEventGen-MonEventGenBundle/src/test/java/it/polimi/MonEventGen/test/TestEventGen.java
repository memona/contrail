package it.polimi.MonEventGen.test;

import it.polimi.MonEventGen.impl.MonEventGenServiceImpl;
import it.polimi.MonEventGen.service.MonEventGenService;

public class TestEventGen {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MonEventGenService service = new MonEventGenServiceImpl();
		
		String channel = "TestingMonitoringEvents";
		int runs = 1000;
		double violationFrequency = 0.25;
		int maxResponseTime = 10;
		
		service.generateResponseTimeEventsBookSaleService(channel, runs, violationFrequency, maxResponseTime);
		
		

	}

}
