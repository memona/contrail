package it.polimi.MonEventGen.service;


public interface MonEventGenService {

	public void generateResponseTimeEventsPaymentService(String channel, int runs, double violationFrequency, int maxResponseTime);
	
	public void generateResponseTimeEventsBookSaleService(String channel, int runs, double violationFrequency, int maxResponseTime);
	
	public void generateArrivalRateViolationEventsPaymentService(String channel, int maxArrivalRate);
	
	public void generateArrivalRateViolationEventsBookSaleService(String channel, int maxArrivalRate);
	
	public void closeConnection();
	
	//Additional methods may be placed here in the future.
	
}


