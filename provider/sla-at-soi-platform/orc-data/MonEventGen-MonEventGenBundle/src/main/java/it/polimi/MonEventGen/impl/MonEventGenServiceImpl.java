package it.polimi.MonEventGen.impl;

import it.polimi.MonEventGen.service.MonEventGenService;
import it.polimi.MonEventGen.support.BookSaleArrivalRateGen;
import it.polimi.MonEventGen.support.EventConfiguration;
import it.polimi.MonEventGen.support.EventFactory;
import it.polimi.MonEventGen.support.PaymentArrivalRateGen;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;







/**
 * @author sam
 *
 */
public class MonEventGenServiceImpl implements MonEventGenService {
	
	private String PaymentServiceProcessName="";
	private String PaymentServiceOperationName="";
	private String PaymentServiceIp="";
	private String PaymentServiceEpr="";
	
	private String BookSaleServiceProcessName="";
	private String BookSaleServiceOperationName="";
	private String BookSaleServiceIp="";
	private String BookSaleServiceEpr="";
	
	private PubSubManager manager;
	private String channel = "TestingMonitoringEvents";
	
	
	//constructor
	
	
    public MonEventGenServiceImpl(){
    	System.out.println("*********** Constructing MonEventGenServiceImpl *************");
 
    	
    	Settings settings = this.getPubSubSettings();
    	try {
			manager = PubSubFactory.createPubSubManager(settings);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		try {
			manager.createChannel(new Channel(channel));
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
    }
    
    //public interface methods
    
	public void generateResponseTimeEventsBookSaleService(String channel, int runs, 
			double violationFrequency,  int maxResponseTime) {
		/*
		 * Need to produce start and end events for the booksale service.
		 * response time needs to exceed maxResponseTime amd maxArrivalRate needs
		 * to exceed maxarrivalRate
		 */
		
		
		//clean vFrequency for use
		int vFrequency;
		
		if (violationFrequency > 0 && violationFrequency < 1) {
			vFrequency = (int) (violationFrequency * 10);
		}
		else if (violationFrequency > 1 && violationFrequency < 10) {
			vFrequency = (int) (violationFrequency);
		}
		else if (violationFrequency > 10 && violationFrequency <100) {
			vFrequency = (int)(violationFrequency/100);
		}
		else {
			return;
		}
		
		for (int i = 0; i< runs; i++) {
			
			if (i%10 < vFrequency) {
				sendBookSaleResponseTimeViolation(channel, manager, maxResponseTime);
			}
			else {
				sendBookSaleValidResponsetime(channel, manager, maxResponseTime);
			}
			
		}
	}

	

	public void generateResponseTimeEventsPaymentService(String channel, int runs, 
			double violationFrequency, int maxResponseTime) {
		/*
		 * Need to produce start and end events for the payment service.
		 * response time needs to exceed maxResponseTime amd maxArrivalRate needs
		 * to exceed maxarrivalRate
		 */
		
		//clean vFrequency for use
		
		int vFrequency;
		
		if (violationFrequency > 0 && violationFrequency < 1) {
			vFrequency = (int) (violationFrequency * 10);
		}
		else if (violationFrequency > 1 && violationFrequency < 10) {
			vFrequency = (int) (violationFrequency);
		}
		else if (violationFrequency > 10 && violationFrequency <100) {
			vFrequency = (int)(violationFrequency/100);
		}
		else {
			return;
		}
		
		
		for (int i = 0; i< runs; i++) {
			
			if (i%10 < vFrequency) {
				sendPaymentResponsetimeViolation(channel, manager, maxResponseTime);
			}
			else {
				sendPaymentValidReponseTime(channel, manager, maxResponseTime);
			}
			
		}
		

		
	}


	public void generateArrivalRateViolationEventsPaymentService(
			String channel, int maxArrivalRate) {
		
		//Define five timestamps that are less than one second away
		
		long ts = new Date().getTime();
		
		long[] timestamp = new long[5];
		
		for (int i=0; i<5; i++) {
			timestamp[i] = ts + (i*100);
		}
		
		int numThreads = (int) ((maxArrivalRate * 1.20) / 10);
		int runsPerRunner = 10;
		
		ExecutorService service = Executors.newFixedThreadPool(numThreads);

		for (int i=0; i<numThreads; i++) {
			service.execute(new PaymentArrivalRateGen(manager, channel, runsPerRunner, timestamp));
		}
		
		//Must wait for threads to complete ???
				
	}
	
	public void generateArrivalRateViolationEventsBookSaleService(
			String channel, int maxArrivalRate) {
		
		//Define five timestamps that are less than one second away
		
		long ts = new Date().getTime();
		
		long[] timestamp = new long[5];
		
		for (int i=0; i<5; i++) {
			timestamp[i] = ts + (i*100);
		}
		
		int numThreads = (int) ((maxArrivalRate * 1.20) / 10);
		int runsPerRunner = 10;
		
		ExecutorService service = Executors.newFixedThreadPool(numThreads);

		for (int i=0; i<numThreads; i++) {
			service.execute(new BookSaleArrivalRateGen(manager, channel, runsPerRunner, timestamp));
		}
		
		//Must wait for threads to complete ???
		
		
		
	}


    


	//support methods
	
	
	
	private void sendPaymentValidReponseTime(String channel,
			PubSubManager manager, int maxResponseTime) {
		
		long timestamp = new Date().getTime();
		
		EventConfiguration configReceive = new EventConfiguration();
		
		configReceive.setTimestamp(timestamp);
		configReceive.setNotifier("sla@soi_bpel_instrumentation");
		configReceive.setServicename(PaymentServiceProcessName);
		configReceive.setOpName(PaymentServiceOperationName);
		
		configReceive.setSourceName("AnonymousName");
		configReceive.setSourceEpr("AnonymousEpr");
		configReceive.setSourceIp("AnonymousIp");
		
		configReceive.setDestName(PaymentServiceProcessName);
		configReceive.setDestIp(PaymentServiceIp);
		configReceive.setDestEpr(PaymentServiceEpr);
		
	
		String receiveMsg = EventFactory.createReceiveEvent(configReceive);
		//this.publishMessage(manager, channel, receiveMsg);
		
		PubSubMessage message = new PubSubMessage(channel, receiveMsg);
		try {
			manager.publish(message);
			System.out.println("[GEN] Receive event has been sent.");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// **************************
		
		EventConfiguration configReply = new EventConfiguration();
		
		configReply.setTimestamp(timestamp + (maxResponseTime/2));
		configReply.setNotifier("sla@soi_bpel_instrumentation");
		configReceive.setServicename(PaymentServiceProcessName);
		configReceive.setOpName(PaymentServiceOperationName);
		
		configReceive.setSourceName(PaymentServiceProcessName);
		configReceive.setSourceEpr(PaymentServiceEpr);
		configReceive.setSourceIp(PaymentServiceIp);
		
		configReceive.setDestName("AnonymousName");
		configReceive.setDestIp("AnonymousIp");
		configReceive.setDestEpr("AnonymousEpr");
		
		String responseMsg = EventFactory.createReplyEvent(configReply);
	//	this.publishMessage(manager, channel, responseMsg);
		
		message = new PubSubMessage(channel, responseMsg);
		try {
			manager.publish(message);
			System.out.println("[GEN] Receive event has been sent.");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void sendPaymentResponsetimeViolation(String channel,
			PubSubManager manager, int maxResponseTime) {
		
		long timestamp = new Date().getTime();
		
		EventConfiguration configReceive = new EventConfiguration();
		
		configReceive.setTimestamp(timestamp);
		configReceive.setNotifier("sla@soi_bpel_instrumentation");
		configReceive.setServicename(PaymentServiceProcessName);
		configReceive.setOpName(PaymentServiceOperationName);
		
		configReceive.setSourceName("AnonymousName");
		configReceive.setSourceEpr("AnonymousEpr");
		configReceive.setSourceIp("AnonymousIp");
		
		configReceive.setDestName(PaymentServiceProcessName);
		configReceive.setDestIp(PaymentServiceIp);
		configReceive.setDestEpr(PaymentServiceEpr);
		
	
		String receiveMsg = EventFactory.createReceiveEvent(configReceive);
	//	this.publishMessage(manager, channel, receiveMsg);
		
		PubSubMessage message = new PubSubMessage(channel, receiveMsg);
		try {
			manager.publish(message);
			System.out.println("[GEN] Receive event has been sent.");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// **************************
		
		EventConfiguration configReply = new EventConfiguration();
		
		configReply.setTimestamp(timestamp + (maxResponseTime*2));
		configReply.setNotifier("sla@soi_bpel_instrumentation");
		configReceive.setServicename(PaymentServiceProcessName);
		configReceive.setOpName(PaymentServiceOperationName);
		
		configReceive.setSourceName(PaymentServiceProcessName);
		configReceive.setSourceEpr(PaymentServiceEpr);
		configReceive.setSourceIp(PaymentServiceIp);
		
		configReceive.setDestName("AnonymousName");
		configReceive.setDestIp("AnonymousIp");
		configReceive.setDestEpr("AnonymousEpr");
		
		String responseMsg = EventFactory.createReplyEvent(configReply);
		//this.publishMessage(manager, channel, responseMsg);
		message = new PubSubMessage(channel, responseMsg);
		try {
			manager.publish(message);
			System.out.println("[GEN] Receive event has been sent.");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void sendBookSaleValidResponsetime(String channel,
			PubSubManager manager, int maxResponseTime) {
		
		long timestamp = new Date().getTime();
		
		EventConfiguration configReceive = new EventConfiguration();
		
		configReceive.setTimestamp(timestamp);
		configReceive.setNotifier("sla@soi_axis_instrumentation");
		configReceive.setServicename(BookSaleServiceProcessName);
		configReceive.setOpName(BookSaleServiceOperationName);
		
		configReceive.setSourceName(PaymentServiceProcessName);
		configReceive.setSourceEpr(PaymentServiceEpr);
		configReceive.setSourceIp(PaymentServiceIp);
		
		configReceive.setDestName(BookSaleServiceProcessName);
		configReceive.setDestIp(BookSaleServiceIp);
		configReceive.setDestEpr(BookSaleServiceEpr);
		
	
		String receiveMsg = EventFactory.createReceiveEvent(configReceive);
		
		PubSubMessage message = new PubSubMessage(channel, receiveMsg);
		try {
			manager.publish(message);
			System.out.println("[GEN] Receive event has been sent.");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// **************************
		
		EventConfiguration configReply = new EventConfiguration();
		
		configReply.setTimestamp(timestamp+ (maxResponseTime/2));
		configReply.setNotifier("sla@soi_axis_instrumentation");
		configReceive.setServicename(BookSaleServiceProcessName);
		configReceive.setOpName(BookSaleServiceOperationName);
		
		configReceive.setSourceName(BookSaleServiceProcessName);
		configReceive.setSourceEpr(BookSaleServiceEpr);
		configReceive.setSourceIp(BookSaleServiceIp);
		
		configReceive.setDestName(PaymentServiceProcessName);
		configReceive.setDestIp(PaymentServiceIp);
		configReceive.setDestEpr(PaymentServiceEpr);
		
		String responseMsg = EventFactory.createReplyEvent(configReply);
		
		message = new PubSubMessage(channel, responseMsg);
		try {
			manager.publish(message);
			System.out.println("[GEN] Reply event has been sent.");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void sendBookSaleResponseTimeViolation(String channel,
			PubSubManager manager, int maxResponseTime) {
		
		long timestamp = new Date().getTime();
		
		EventConfiguration configReceive = new EventConfiguration();
		
		configReceive.setTimestamp(timestamp);
		configReceive.setNotifier("sla@soi_axis_instrumentation");
		configReceive.setServicename(BookSaleServiceProcessName);
		configReceive.setOpName(BookSaleServiceOperationName);
		
		configReceive.setSourceName(PaymentServiceProcessName);
		configReceive.setSourceEpr(PaymentServiceEpr);
		configReceive.setSourceIp(PaymentServiceIp);
		
		configReceive.setDestName(BookSaleServiceProcessName);
		configReceive.setDestIp(BookSaleServiceIp);
		configReceive.setDestEpr(BookSaleServiceEpr);
		
	
		String receiveMsg = EventFactory.createReceiveEvent(configReceive);

		PubSubMessage message = new PubSubMessage(channel, receiveMsg);
		try {
			manager.publish(message);
			System.out.println("[GEN] Receive violation event has been sent.");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//this.publishMessage(manager, channel, receiveMsg);
		
		// **************************
		
		EventConfiguration configReply = new EventConfiguration();
		
		configReply.setTimestamp(timestamp + (maxResponseTime*2));
		configReply.setNotifier("sla@soi_axis_instrumentation");
		configReceive.setServicename(BookSaleServiceProcessName);
		configReceive.setOpName(BookSaleServiceOperationName);
		
		configReceive.setSourceName(BookSaleServiceProcessName);
		configReceive.setSourceEpr(BookSaleServiceEpr);
		configReceive.setSourceIp(BookSaleServiceIp);
		
		configReceive.setDestName(PaymentServiceProcessName);
		configReceive.setDestIp(PaymentServiceIp);
		configReceive.setDestEpr(PaymentServiceEpr);
		
		String responseMsg = EventFactory.createReplyEvent(configReply);
		
		message = new PubSubMessage(channel, responseMsg);
		try {
			manager.publish(message);
			System.out.println("[GEN] Reply violation event has been sent.");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private Settings getPubSubSettings() {
		Settings settings = new Settings();
		settings.setSetting(Setting.pubsub, "xmpp");
		settings.setSetting(Setting.xmpp_username, "primitive-ecf");
		settings.setSetting(Setting.xmpp_password, "primitive-ecf");
		settings.setSetting(Setting.xmpp_host, "testbed.sla-at-soi.eu");
		settings.setSetting(Setting.xmpp_port, "5222");
		settings.setSetting(Setting.messaging, "xmpp");
		settings.setSetting(Setting.pubsub, "xmpp");
		settings.setSetting(Setting.xmpp_service, "testbed.sla-at-soi.eu");
		settings.setSetting(Setting.xmpp_resource, "test");
		settings.setSetting(Setting.xmpp_pubsubservice, "pubsub.testbed.sla-at-soi.eu");
		
		return settings;
	}

	public void closeConnection() {
		// TODO Auto-generated method stub
		
		try {
			this.manager.deleteChannel(this.channel);
		} catch (MessagingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			this.manager.close();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	
	


	
	

	



}