package it.polimi.MonEventGen.support;

import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.GregorianCalendar;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.slasoi.common.eventschema.Entity;
import org.slasoi.common.eventschema.EventContextType;
import org.slasoi.common.eventschema.EventIdType;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.EventMetaDataType;
import org.slasoi.common.eventschema.EventNotifier;
import org.slasoi.common.eventschema.EventPayloadType;
import org.slasoi.common.eventschema.EventSource;
import org.slasoi.common.eventschema.EventTime;
import org.slasoi.common.eventschema.InteractionEventType;
import org.slasoi.common.eventschema.ObjectFactory;
import org.slasoi.common.eventschema.ServiceSourceType;



public class EventFactory {
	
	private static JAXBContext context = createContext();

	public static String createReplyEvent(EventConfiguration config) {
		
		ObjectFactory factory = new ObjectFactory();
		
		//create event
		EventInstance event = factory.createEventInstance();
		
		EventIdType eventID = factory.createEventIdType();
		event.setEventID(eventID);
						
		//set eventID content
		long generatedEventID =UUID.randomUUID().getMostSignificantBits();
		event.getEventID().setID(generatedEventID);
		event.getEventID().setEventTypeID("ServiceOperationCallEndEvent");
		
		
		//set event timestamps
		
		EventContextType eventContext = factory.createEventContextType();
		event.setEventContext(eventContext);
		EventTime eventTime = factory.createEventTime();
		event.getEventContext().setTime(eventTime);
		
		try {
			event.getEventContext().getTime().setCollectionTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
			event.getEventContext().getTime().setTimestamp(config.getTimestamp());
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//set event notifier
		
		EventNotifier eventNotifier = factory.createEventNotifier();
		event.getEventContext().setNotifier(eventNotifier);
		
		event.getEventContext().getNotifier().setName(config.getNotifier());

		InetAddress address=null;
		String ip = null;
		try {
			address = InetAddress.getLocalHost();
			ip = address.getHostAddress();
			event.getEventContext().getNotifier().setIP(ip);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
						
		long port = 8080;
		event.getEventContext().getNotifier().setPort(port);
		
		//set source information

		EventSource eventSource = factory.createEventSource();
		event.getEventContext().setSource(eventSource);
		ServiceSourceType serviceSource = factory.createServiceSourceType();
		event.getEventContext().getSource().setSwServiceLayerSource(serviceSource);
		Entity sender = factory.createEntity();
		event.getEventContext().getSource().getSwServiceLayerSource().setSender(sender);
		
		Entity receiver = factory.createEntity();
		event.getEventContext().getSource().getSwServiceLayerSource().setReceiver(receiver);
		
		event.getEventContext().getSource().getSwServiceLayerSource().getSender().setName(config.getSourceName());
		event.getEventContext().getSource().getSwServiceLayerSource().getSender().setIp(config.getSourceIp());
		event.getEventContext().getSource().getSwServiceLayerSource().getSender().setPort(port);
		event.getEventContext().getSource().getSwServiceLayerSource().getSender().setProcessID(Long.toString(generatedEventID));
		event.getEventContext().getSource().getSwServiceLayerSource().getSender().setServiceEPR(config.getSourceEpr());
		
		//set destination information
		
		
		event.getEventContext().getSource().getSwServiceLayerSource().getReceiver().setName(config.getDestName());
		event.getEventContext().getSource().getSwServiceLayerSource().getReceiver().setIp(config.getDestIp());
		event.getEventContext().getSource().getSwServiceLayerSource().getReceiver().setPort(8080);
		event.getEventContext().getSource().getSwServiceLayerSource().getReceiver().setServiceEPR(config.getDestEpr());
		
		
		EventPayloadType eventPayload = factory.createEventPayloadType();
		event.setEventPayload(eventPayload);
		InteractionEventType ievent = factory.createInteractionEventType();
		event.getEventPayload().setInteractionEvent(ievent);
		
		event.getEventPayload().getInteractionEvent().setOperationID(generatedEventID);
		event.getEventPayload().getInteractionEvent().setOperationName(config.getOpName());
		event.getEventPayload().getInteractionEvent().setStatus("RES-A");
		
		EventMetaDataType meta1 = factory.createEventMetaDataType();
		EventMetaDataType meta2 = factory.createEventMetaDataType();
		
		
		meta1.setKey("eventSourceDefinitionId");
		String defID = config.getServicename() + "_" + config.getOpName() + "_SOC_Def";
		meta1.setValue(defID);
		
		meta2.setKey("eventSourceInstanceId");
		meta2.setValue(Long.toString(generatedEventID));
		
		event.getEventMetadata().add(meta1);
		event.getEventMetadata().add(meta2);
		
		// set report time
		try {
			event.getEventContext().getTime().setReportTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String eventString = null;
		
		try {
			eventString = eventMarshaller(event);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		return eventString;
	}
	
	/**
	 * Pointcut per gestire l'emissione della variabile di output della invoke 
	 * @param timestamp 
	 * @param notifier 
	 * @param opName 
	 * @param aeAct
	 */
	

	

	public static String createReceiveEvent(EventConfiguration config) {
	
		
		ObjectFactory factory = new ObjectFactory();
		
		//create event
		EventInstance event = factory.createEventInstance();
		
		EventIdType eventID = factory.createEventIdType();
		event.setEventID(eventID);
						
		//set eventID content
		long generatedEventID =UUID.randomUUID().getMostSignificantBits();
		event.getEventID().setID(generatedEventID);
		
		String eventIDType = "ServiceOperationCallStartEvent";
		event.getEventID().setEventTypeID(eventIDType);
		
		
		//set event timestamps
		
		EventContextType eventContext = factory.createEventContextType();
		event.setEventContext(eventContext);
		EventTime eventTime = factory.createEventTime();
		event.getEventContext().setTime(eventTime);
		
		try {
			event.getEventContext().getTime().setCollectionTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
			event.getEventContext().getTime().setTimestamp(config.getTimestamp());
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//set event notifier
		
		EventNotifier eventNotifier = factory.createEventNotifier();
		event.getEventContext().setNotifier(eventNotifier);
		
		event.getEventContext().getNotifier().setName(config.getNotifier());
						
					
		InetAddress address=null;
		String ip = null;
		try {
			address = InetAddress.getLocalHost();
			ip = address.getHostAddress();
			event.getEventContext().getNotifier().setIP(ip);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
						
		long port = 8080;
		event.getEventContext().getNotifier().setPort(port);
		
		//set source information
				
		EventSource eventSource = factory.createEventSource();
		event.getEventContext().setSource(eventSource);
		ServiceSourceType serviceSource = factory.createServiceSourceType();
		event.getEventContext().getSource().setSwServiceLayerSource(serviceSource);
		Entity sender = factory.createEntity();
		event.getEventContext().getSource().getSwServiceLayerSource().setSender(sender);
		Entity receiver = factory.createEntity();
		event.getEventContext().getSource().getSwServiceLayerSource().setReceiver(receiver);
		
		event.getEventContext().getSource().getSwServiceLayerSource().getSender().setName(config.getSourceName());
		event.getEventContext().getSource().getSwServiceLayerSource().getSender().setIp(config.getSourceIp());
		event.getEventContext().getSource().getSwServiceLayerSource().getSender().setPort(8080);
		event.getEventContext().getSource().getSwServiceLayerSource().getSender().setServiceEPR(config.getSourceIp());
		
		//set destination information
		
		event.getEventContext().getSource().getSwServiceLayerSource().getReceiver().setName(config.getDestName());
		event.getEventContext().getSource().getSwServiceLayerSource().getReceiver().setIp(config.getDestIp());
		event.getEventContext().getSource().getSwServiceLayerSource().getReceiver().setPort(port);
		event.getEventContext().getSource().getSwServiceLayerSource().getReceiver().setProcessID(Long.toString(generatedEventID));
		event.getEventContext().getSource().getSwServiceLayerSource().getReceiver().setServiceEPR(config.getDestEpr());
		
		
		EventPayloadType eventPayload = factory.createEventPayloadType();
		event.setEventPayload(eventPayload);
		InteractionEventType ievent = factory.createInteractionEventType();
		event.getEventPayload().setInteractionEvent(ievent);
		
		event.getEventPayload().getInteractionEvent().setOperationID(generatedEventID);
		event.getEventPayload().getInteractionEvent().setOperationName(config.getOpName());
		event.getEventPayload().getInteractionEvent().setStatus("REQ-A");
		

		EventMetaDataType meta1 = factory.createEventMetaDataType();
		EventMetaDataType meta2 = factory.createEventMetaDataType();
		
		meta1.setKey("eventSourceDefinitionId");
		String defID = config.getServicename() + "_" + config.getOpName() + "_SOC_Def";
		meta1.setValue(defID);
					
		meta2.setKey("eventSourceInstanceId");
		meta2.setValue(Long.toString(generatedEventID));
		
		event.getEventMetadata().add(meta1);
		event.getEventMetadata().add(meta2);
		
		String eventMessage=null;
		
		// set report time
		try {
			event.getEventContext().getTime().setReportTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			eventMessage = eventMarshaller(event);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return eventMessage;
	}
	
	
	//private support methods
	
private static String eventMarshaller(EventInstance event) throws JAXBException{
		
		Marshaller marsh = null;
		try {
			marsh = context.createMarshaller();
			marsh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	
		JAXBElement<EventInstance> instance = (new ObjectFactory()).createEvent(event);
		
		StringWriter output = new StringWriter();
		
	    marsh.marshal(instance, output);
		
		String message = output.toString();	
		
		return message;
	}

private static JAXBContext createContext() {
	// TODO Auto-generated method stub
	
	JAXBContext slasoiContext = null;
	try {
		slasoiContext = JAXBContext.newInstance("org.slasoi.common.eventschema");
	} catch (JAXBException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return slasoiContext;
}
	
	

}
