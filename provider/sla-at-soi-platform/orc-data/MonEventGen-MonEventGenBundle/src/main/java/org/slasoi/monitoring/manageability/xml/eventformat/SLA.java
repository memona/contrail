//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0.3-b24-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.05.05 at 10:58:30 AM BST 
//


package org.slasoi.monitoring.manageability.xml.eventformat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SLA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SLA">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="slaID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="slaURI" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SLA", namespace = "http://slasoi.org/monitoring/xml/eventformat", propOrder = {
    "slaID",
    "slaURI"
})
public class SLA {

    @XmlElement(namespace = "http://slasoi.org/monitoring/xml/eventformat", required = true)
    protected String slaID;
    @XmlElement(namespace = "http://slasoi.org/monitoring/xml/eventformat", required = true)
    protected String slaURI;

    /**
     * Gets the value of the slaID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSlaID() {
        return slaID;
    }

    /**
     * Sets the value of the slaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSlaID(String value) {
        this.slaID = value;
    }

    /**
     * Gets the value of the slaURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSlaURI() {
        return slaURI;
    }

    /**
     * Sets the value of the slaURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSlaURI(String value) {
        this.slaURI = value;
    }

}
