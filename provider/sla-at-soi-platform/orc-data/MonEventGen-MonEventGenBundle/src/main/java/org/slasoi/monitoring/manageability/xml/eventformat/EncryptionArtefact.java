//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0.3-b24-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.05.05 at 10:58:30 AM BST 
//


package org.slasoi.monitoring.manageability.xml.eventformat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for encryptionArtefact complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="encryptionArtefact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="encryptionOrganisation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="encryptionReference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="encryptionInputDataType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "encryptionArtefact", propOrder = {
    "encryptionOrganisation",
    "encryptionReference",
    "encryptionInputDataType"
})
public class EncryptionArtefact {

    @XmlElement(required = true)
    protected String encryptionOrganisation;
    @XmlElement(required = true)
    protected String encryptionReference;
    @XmlElement(required = true)
    protected String encryptionInputDataType;

    /**
     * Gets the value of the encryptionOrganisation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncryptionOrganisation() {
        return encryptionOrganisation;
    }

    /**
     * Sets the value of the encryptionOrganisation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncryptionOrganisation(String value) {
        this.encryptionOrganisation = value;
    }

    /**
     * Gets the value of the encryptionReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncryptionReference() {
        return encryptionReference;
    }

    /**
     * Sets the value of the encryptionReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncryptionReference(String value) {
        this.encryptionReference = value;
    }

    /**
     * Gets the value of the encryptionInputDataType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncryptionInputDataType() {
        return encryptionInputDataType;
    }

    /**
     * Sets the value of the encryptionInputDataType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncryptionInputDataType(String value) {
        this.encryptionInputDataType = value;
    }

}
