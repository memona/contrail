//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0.3-b24-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.05.05 at 10:58:30 AM BST 
//


package org.slasoi.monitoring.manageability.xml.eventformat;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;


/**
 * <p>Java class for SourceKind.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SourceKind">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="est"/>
 *     &lt;enumeration value="meas"/>
 *     &lt;enumeration value="calc"/>
 *     &lt;enumeration value="req"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlEnum
public enum SourceKind {

    @XmlEnumValue("est")
    EST("est"),
    @XmlEnumValue("meas")
    MEAS("meas"),
    @XmlEnumValue("calc")
    CALC("calc"),
    @XmlEnumValue("req")
    REQ("req");
    private final String value;

    SourceKind(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SourceKind fromValue(String v) {
        for (SourceKind c: SourceKind.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v.toString());
    }

}
