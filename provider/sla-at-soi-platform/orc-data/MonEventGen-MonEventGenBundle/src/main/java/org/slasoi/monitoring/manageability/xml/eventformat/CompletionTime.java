//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.0.3-b24-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2009.05.05 at 10:58:30 AM BST 
//


package org.slasoi.monitoring.manageability.xml.eventformat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompletionTime complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompletionTime">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.slaatsoi.org/coremodel}Metric">
 *       &lt;sequence>
 *         &lt;element name="completiontime" type="{http://www.slaatsoi.org/coremodel}Duration"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompletionTime", propOrder = {
    "completiontime"
})
public class CompletionTime
    extends Metric
{

    @XmlElement(required = true)
    protected Duration completiontime;

    /**
     * Gets the value of the completiontime property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getCompletiontime() {
        return completiontime;
    }

    /**
     * Sets the value of the completiontime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setCompletiontime(Duration value) {
        this.completiontime = value;
    }

}
