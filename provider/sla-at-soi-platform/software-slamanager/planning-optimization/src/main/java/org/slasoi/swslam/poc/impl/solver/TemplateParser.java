package org.slasoi.swslam.poc.impl.solver;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;
import org.slasoi.slamodel.core.CompoundDomainExpr;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.LIST;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.vocab.core;

public class TemplateParser extends Base {

    private SLATemplate slaTemplate;
    public boolean ASSIGNMENT_MODE = false;
    Operation[] operations;
    private HashMap<String, SoftwareServiceOperation> mapOfSSOperations =
            new HashMap<String, SoftwareServiceOperation>();
    private static final String ARRIVAL_RATE = "ARRIVAL_RATE";
    private static final String RESPONSE_TIME = "RESPONSE_TIME";
    private static final String VM_QUANTITY = "VM_QUANTITY";
    private static final String CPU_SPEED = "CPU_SPEED";
    private static final String CPU_CORES = "CPU_CORES";
    private static final String MEMORY = "MEMORY";
    private static final String IMAGE = "IMAGE";
    private static final String AVAILABILITY = "AVAILABILITY";

    // SW params:
    private int arrivalRateBookSale, completionTimeBookSale, arrivalRateGetProductDetails,
            completionTimeGetProductDetails, arrivalRatePaymentService, completionTimePaymentService;
    // INF params:
    private int cpuSpeed, cpuCores, memory, quantity;
    private String vmImage;

    private InfrastructureServiceTerms iSTerms = new InfrastructureServiceTerms();

    public TemplateParser() {
        LOGGER.info("Inside TemplateParser()");
    }

    public HashMap getMapOfSSOperations(SLATemplate slaTemplate) {
        this.ASSIGNMENT_MODE = false;
        this.parseTemplate(slaTemplate);
        return this.mapOfSSOperations;
    }

    public InfrastructureServiceTerms getISTerms(SLATemplate slaTemplate) {
        this.ASSIGNMENT_MODE = false;
        this.parseTemplate(slaTemplate);
        return this.iSTerms;
    }

    public SLATemplate assignQoSValuesToTemplate(SLATemplate slaTemplate, InfrastructureServiceTerms iSTerms) {
        this.ASSIGNMENT_MODE = true; // We want to assign values now to passed in slaTemplate, so setting them in our
        // instance variables from iSTerms object before parsing.
        this.arrivalRateGetProductDetails = iSTerms.getProductDetailsOperation().getArrivalRate();
        this.completionTimeGetProductDetails = iSTerms.getProductDetailsOperation().getCompletionTime();

        this.arrivalRateBookSale = iSTerms.getBookSaleOperation().getArrivalRate();
        this.completionTimeBookSale = iSTerms.getBookSaleOperation().getCompletionTime();

        this.arrivalRatePaymentService = iSTerms.getPaymentServiceOperation().getArrivalRate();
        this.completionTimePaymentService = iSTerms.getPaymentServiceOperation().getCompletionTime();

        this.cpuCores = iSTerms.getCpuCores();
        this.cpuSpeed = iSTerms.getCpuSpeed();
        this.memory = iSTerms.getMemory();

        this.parseTemplate(slaTemplate); //method overloaded to parse/set values to both SW and Inf Templates.

        this.ASSIGNMENT_MODE = false;
        return this.slaTemplate;
    }

    /******************************** METHODS BELOW THIS LINE PERFORM PARSING ****************************************************/

    public void parseTemplate(SLATemplate slaTemplate) {
        if (slaTemplate != null) {
            if (slaTemplate instanceof SLA) {
                this.slaTemplate = slaTemplate;
                render();

            }
            else if (slaTemplate instanceof SLATemplate) {
                this.slaTemplate = slaTemplate;
                render();
            }
        }
    }

    private void render() {
        x_SLA();
    }

    private void x_SLA() {
        x_SLA_CONTENT();
    }

    private void x_SLA_CONTENT() {
        InterfaceDeclr[] idecs = this.slaTemplate.getInterfaceDeclrs();
        // ---- INTERFACE DECLARATIONS-------------------------------------------------
        if (idecs != null) {
            for (InterfaceDeclr idec : idecs) {
                this.x_RENDER(idec);
            }
        }

        // ---- AGREEMENT TERMS--------------------------------------------------------
        AgreementTerm[] terms = this.slaTemplate.getAgreementTerms();
        if (terms != null) {
            for (AgreementTerm t : terms) {
                try {
                    String termId = t.getId().getValue();
                    x_RENDER(t);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void x_RENDER(InterfaceDeclr idec) {
        Interface iface = idec.getInterface();

        if (iface instanceof Interface.Specification) {
            Interface.Specification interfaceSpec = (Interface.Specification) iface;
            // LOGGER.info("interfaceSpec.getName() = "+interfaceSpec.getName());
            // For Software Template:
            if (interfaceSpec.getName() != null
                    && (interfaceSpec.getName().equalsIgnoreCase("ORCInventoryService") || interfaceSpec.getName()
                            .equalsIgnoreCase("PaymentService"))) {
                // Add operationName:
                this.operations = interfaceSpec.getOperations();
                if (operations != null && operations.length > 0) {
                    for (Operation operation : operations) {
                        String operationName = operation.getName().getValue();
                        if (operationName != null
                                && (operationName.equalsIgnoreCase("getProductDetails")
                                        || operationName.equalsIgnoreCase("bookSale") || operationName
                                        .equalsIgnoreCase("PaymentServiceOperation"))) {
                            // Extract information for selected service/operation only:
                            SoftwareServiceOperation sSOperation = new SoftwareServiceOperation();
                            sSOperation.setServiceName(interfaceSpec.getName());
                            sSOperation.setOperationName(operationName);
                            this.mapOfSSOperations.put(operationName, sSOperation);
                        }
                    }
                }
            }
        }

    }

    private void x_RENDER(AgreementTerm term) throws Exception {

        VariableDeclr[] vdecs = term.getVariableDeclrs();
        if (vdecs != null)
            for (VariableDeclr v : vdecs) {
                x_RENDER(v, term);
            }

        Guaranteed[] gs = term.getGuarantees();
        for (Guaranteed g : gs) {
            if (g instanceof Guaranteed.State)
                x_RENDER((Guaranteed.State) g);
        }

    }

    private void x_RENDER(VariableDeclr vdec, AgreementTerm term) throws Exception {

        if (vdec instanceof Customisable) {
            Customisable c = (Customisable) vdec;
            // ID for Var
            ID var = c.getVar();
            // CONST for Value
            CONST valu = c.getValue();
            Expr exp = vdec.getExpr();
            SoftwareServiceOperation ssOp = null;// One ssOp relates with two unique AgreementTerms of SWSLAT.
            // InfrastructureServiceTerms iST = this.mapOfISTerms.get(term.getId().getValue());//One iST relates to one
            // AgreementTerm which defines complete VM

            // Terms for Software Template:
            // Term 2.1
            if (var.getValue().equalsIgnoreCase("Var_CustomerConstraintInventoryGetDetails")) {
                if (this.ASSIGNMENT_MODE) {
                    valu.setValue(this.arrivalRateGetProductDetails + ""); // simply set value with this.
                }
                String arrivalRateValue = valu.getValue();
                ssOp = this.mapOfSSOperations.get("getProductDetails");
                ssOp.setArrivalRate(Integer.parseInt(arrivalRateValue));
                addMinMaxBoundaryValuesToSSOperations(ssOp, exp, TemplateParser.ARRIVAL_RATE);
                // LOGGER.info(var.getValue() + " = " + arrivalRateValue);
            }
            // Term 2.2
            else if (var.getValue().equalsIgnoreCase("Var_ORCResponseInventoryProductDetails")) {
                if (this.ASSIGNMENT_MODE) {
                    valu.setValue(this.completionTimeGetProductDetails + ""); // simply set value with this.
                }
                String completionTimeValue = valu.getValue();
                ssOp = this.mapOfSSOperations.get("getProductDetails");
                ssOp.setCompletionTime(Integer.parseInt(completionTimeValue));
                addMinMaxBoundaryValuesToSSOperations(ssOp, exp, TemplateParser.RESPONSE_TIME);
                // LOGGER.info(var.getValue() + " = " + completionTimeValue);
            }
            // Term 3.1
            else if (var.getValue().equalsIgnoreCase("Var_CustomerConstraintInventoryBookSale")) {
                if (this.ASSIGNMENT_MODE) {
                    valu.setValue(this.arrivalRateBookSale + ""); // simply set value with this.
                }
                String arrivalRateValue = valu.getValue();
                ssOp = this.mapOfSSOperations.get("bookSale");
                ssOp.setArrivalRate(Integer.parseInt(arrivalRateValue));
                addMinMaxBoundaryValuesToSSOperations(ssOp, exp, TemplateParser.ARRIVAL_RATE);
                // LOGGER.info(var.getValue() + " = " + arrivalRateValue);
            }
            // Term 3.2
            else if (var.getValue().equalsIgnoreCase("Var_ORCResponseInventoryBookSale")) {
                if (this.ASSIGNMENT_MODE) {
                    valu.setValue(this.completionTimeBookSale + ""); // simply set value with this.
                }
                String completionTimeValue = valu.getValue();
                ssOp = this.mapOfSSOperations.get("bookSale");
                ssOp.setCompletionTime(Integer.parseInt(completionTimeValue));
                addMinMaxBoundaryValuesToSSOperations(ssOp, exp, TemplateParser.RESPONSE_TIME);
                // LOGGER.info(var.getValue() + " = " + completionTimeValue);
            }
            // Term 1.1
            else if (var.getValue().equalsIgnoreCase("Var_CustomerConstraintPayment")) {
                if (this.ASSIGNMENT_MODE) {
                    valu.setValue(this.arrivalRatePaymentService + ""); // simply set value with this.
                }
                String arrivalRateValue = valu.getValue();
                ssOp = this.mapOfSSOperations.get("PaymentServiceOperation");
                ssOp.setArrivalRate(Integer.parseInt(arrivalRateValue));
                addMinMaxBoundaryValuesToSSOperations(ssOp, exp, TemplateParser.ARRIVAL_RATE);
                // LOGGER.info(var.getValue() + " = " + arrivalRateValue);
            }
            // Term 1.2
            else if (var.getValue().equalsIgnoreCase("Var_ORCResponseTimePayment")) {
                if (this.ASSIGNMENT_MODE) {
                    valu.setValue(this.completionTimePaymentService + ""); // simply set value with this.
                }
                String completionTimeValue = valu.getValue();
                ssOp = this.mapOfSSOperations.get("PaymentServiceOperation");
                ssOp.setCompletionTime(Integer.parseInt(completionTimeValue));
                addMinMaxBoundaryValuesToSSOperations(ssOp, exp, TemplateParser.RESPONSE_TIME);
                // LOGGER.info(var.getValue() + " = " + completionTimeValue);
            }

            // Terms for Infrastructure Template:
            // VM_QUANTITY
            else if (var.getValue().equalsIgnoreCase("VM_QUANTITY_VAR")) {
                if (this.ASSIGNMENT_MODE) {
                    valu.setValue("1"); // simply set value with this.
                }
                String vmQuantityValue = valu.getValue();
                iSTerms.setVmQuantity(Integer.parseInt(vmQuantityValue));
                addMinMaxBoundaryValuesToISTerms(iSTerms, exp, TemplateParser.VM_QUANTITY);
            }
            // CPU_CORES
            else if (var.getValue().equalsIgnoreCase("CPU_CORES_VALUE")) {
                if (this.ASSIGNMENT_MODE) {
                    valu.setValue(this.cpuCores + ""); // simply set value with this.
                }
                String cpuCoresValue = valu.getValue();
                iSTerms.setCpuCores(Integer.parseInt(cpuCoresValue));
                addMinMaxBoundaryValuesToISTerms(iSTerms, exp, TemplateParser.CPU_CORES);
            }
            // CPU_SPEED
            else if (var.getValue().equalsIgnoreCase("CPU_SPEED_VALUE")) {
                if (this.ASSIGNMENT_MODE) {
                    valu.setValue(this.cpuSpeed + ""); // simply set value with this.
                }
                String cpuSpeedValue = valu.getValue();
                iSTerms.setCpuSpeed(Integer.parseInt(cpuSpeedValue));
                addMinMaxBoundaryValuesToISTerms(iSTerms, exp, TemplateParser.CPU_SPEED);
            }
            // MEMORY
            else if (var.getValue().equalsIgnoreCase("MEMORY_VALUE")) {
                if (this.ASSIGNMENT_MODE) {
                    valu.setValue(this.memory + ""); // simply set value with this.
                }
                String memory = valu.getValue();
                iSTerms.setMemory(Integer.parseInt(memory));
                addMinMaxBoundaryValuesToISTerms(iSTerms, exp, TemplateParser.MEMORY);
            }
            // IMAGE
            else if (var.getValue().equalsIgnoreCase("VM_IMAGE_VAR")) {
                if (this.ASSIGNMENT_MODE) {
                    // valu.setValue("90"); //simply set value with this.
                }
                String image = valu.getValue();
                iSTerms.setImage(image);
                addMinMaxBoundaryValuesToISTerms(iSTerms, exp, TemplateParser.IMAGE);
            }
            // AVAILABILITY
            else if (var.getValue().equalsIgnoreCase("Overall_Availability_VAR")) {
                if (this.ASSIGNMENT_MODE) {
                    // valu.setValue("90"); //simply set value with this.
                }
                String availabilityValue = valu.getValue();
                iSTerms.setAvailability(Integer.parseInt(availabilityValue));
                addMinMaxBoundaryValuesToISTerms(iSTerms, exp, TemplateParser.AVAILABILITY);
            }
            /*
             * if( ssOp != null ) { LOGGER.info(ssOp.toString()); } if( iSTerms != null ) {
             * LOGGER.info(iSTerms.toString()); }
             */
        }
    }

    private void addMinMaxBoundaryValuesToSSOperations(SoftwareServiceOperation ssOp, Expr exp, String property) {
        if (exp instanceof CompoundDomainExpr) {
            CompoundDomainExpr compoundDomainExpr = (CompoundDomainExpr) exp;
            STND logicalOperator = compoundDomainExpr.getLogicalOp();
            // LOGGER.info(" logicalOperator = " + logicalOperator.getValue());
            DomainExpr[] domainExpr = compoundDomainExpr.getSubExpressions();
            for (DomainExpr dE : domainExpr) {
                // LOGGER.info(" dE = " + dE.getClass());
                if (dE instanceof SimpleDomainExpr) {
                    SimpleDomainExpr simpleDomainExpr = (SimpleDomainExpr) dE;
                    STND comparisonOperator = simpleDomainExpr.getComparisonOp();
                    // LOGGER.info(" comparisonOperator = " + comparisonOperator.getValue());
                    if (comparisonOperator.getValue().equalsIgnoreCase(core.greater_than.getValue())) {
                        String greaterThan = ((CONST) simpleDomainExpr.getValue()).getValue();
                        if (property.equalsIgnoreCase(ARRIVAL_RATE)) {
                            ssOp.setMinArrivalRate(Integer.parseInt(greaterThan) + 1);
                        }
                        else if (property.equalsIgnoreCase(RESPONSE_TIME)) {
                            ssOp.setMinCompletionTime(Integer.parseInt(greaterThan) + 1);
                        }
                        // LOGGER.info(" GREATER THAN "+ssOp.getMinArrivalRate());
                    }
                    else if (comparisonOperator.getValue().equalsIgnoreCase(core.less_than_or_equals.getValue())) {
                        String lessThanOrEquals = ((CONST) simpleDomainExpr.getValue()).getValue();
                        if (property.equalsIgnoreCase(ARRIVAL_RATE)) {
                            ssOp.setMaxArrivalRate(Integer.parseInt(lessThanOrEquals));
                        }
                        else if (property.equalsIgnoreCase(RESPONSE_TIME)) {
                            ssOp.setMaxCompletionTime(Integer.parseInt(lessThanOrEquals));
                        }
                        // LOGGER.info(" LESS THAN OR EQUALS "+ssOp.getMaxArrivalRate());
                    }
                }
            }
        }
    }

    private void addMinMaxBoundaryValuesToISTerms(InfrastructureServiceTerms iSTerms, Expr exp, String property) {
        if (exp instanceof CompoundDomainExpr) {
            CompoundDomainExpr compoundDomainExpr = (CompoundDomainExpr) exp;
            STND logicalOperator = compoundDomainExpr.getLogicalOp();
            // Expected logicalOperator = and ONLY
            DomainExpr[] domainExpr = compoundDomainExpr.getSubExpressions();
            for (DomainExpr dE : domainExpr) {
                // LOGGER.info(" dE = " + dE.getClass());
                if (dE instanceof SimpleDomainExpr) {
                    SimpleDomainExpr simpleDomainExpr = (SimpleDomainExpr) dE;
                    STND comparisonOperator = simpleDomainExpr.getComparisonOp();
                    // LOGGER.info(" comparisonOperator = " + comparisonOperator.getValue());
                    if (comparisonOperator.getValue().equalsIgnoreCase(core.greater_than.getValue())) {
                        String greaterThan = ((CONST) simpleDomainExpr.getValue()).getValue();
                        if (property.equalsIgnoreCase(CPU_CORES)) {
                            iSTerms.setMinCpuCores(Integer.parseInt(greaterThan) + 1);
                        }
                        else if (property.equalsIgnoreCase(CPU_SPEED)) {
                            iSTerms.setMinCpuSpeed(Integer.parseInt(greaterThan) + 1);
                        }
                        else if (property.equalsIgnoreCase(MEMORY)) {
                            iSTerms.setMinMemory(Integer.parseInt(greaterThan) + 1);
                        }
                        else if (property.equalsIgnoreCase(VM_QUANTITY)) {
                            iSTerms.setMinVMQuantity(Integer.parseInt(greaterThan) + 1);
                        }
                        else if (property.equalsIgnoreCase(AVAILABILITY)) {
                            iSTerms.setMinAvailability(Integer.parseInt(greaterThan) + 1);
                        }
                    }
                    else if (comparisonOperator.getValue().equalsIgnoreCase(core.less_than_or_equals.getValue())) {
                        String lessThanOrEquals = ((CONST) simpleDomainExpr.getValue()).getValue();
                        if (property.equalsIgnoreCase(CPU_CORES)) {
                            iSTerms.setMaxCpuCores(Integer.parseInt(lessThanOrEquals));
                        }
                        else if (property.equalsIgnoreCase(CPU_SPEED)) {
                            iSTerms.setMaxCpuSpeed(Integer.parseInt(lessThanOrEquals));
                        }
                        else if (property.equalsIgnoreCase(MEMORY)) {
                            iSTerms.setMaxMemory(Integer.parseInt(lessThanOrEquals));
                        }
                        else if (property.equalsIgnoreCase(AVAILABILITY)) {
                            iSTerms.setMaxAvailability(Integer.parseInt(lessThanOrEquals));
                        }
                    }
                }
            }
        }
        else if (exp instanceof SimpleDomainExpr) {
            SimpleDomainExpr simpleDomainExpr = (SimpleDomainExpr) exp;
            STND comparisonOperator = simpleDomainExpr.getComparisonOp();
            if (comparisonOperator.getValue().equalsIgnoreCase(core.member_of.getValue())) {
                // LOGGER.info(" ================ "+simpleDomainExpr.getClass());
                ValueExpr valueExpr = simpleDomainExpr.getValue();
                // LOGGER.info(" ================ "+valueExpr.getClass());
                if (valueExpr instanceof LIST) {
                    LIST listValueExpresion = (LIST) valueExpr;
                    for (ValueExpr value : listValueExpresion) {
                        iSTerms.getListOfImages().add(((CONST) value).getValue());
                        // LOGGER.info(" ======= "+((CONST)value).getValue());
                    }
                }
            }
        }
    }

    private void x_RENDER(Guaranteed.State gs) throws Exception {
        ConstraintExpr state = gs.getState();
        x_RENDER(state);
    }

    private void x_RENDER(ConstraintExpr ce) throws Exception {
        // LOGGER.info("STATE.ConstraintExpr = \n" + ce.toString());//no need to process States.
    }

}
