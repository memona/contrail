/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.swslam.poc.monitoringfeature;

import org.slasoi.monitoring.common.features.Basic;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.Event;
import org.slasoi.monitoring.common.features.Function;
import org.slasoi.monitoring.common.features.MonitoringFeature;
import org.slasoi.monitoring.common.features.impl.FeaturesFactoryImpl;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.meta;

public class MonitoringFeatureCreator {
    public ComponentMonitoringFeatures[] componentMonitoringFeaturesCreator() {
        FeaturesFactoryImpl ffi = new FeaturesFactoryImpl();
        ComponentMonitoringFeatures[] cmfeatures = new ComponentMonitoringFeatures[2];
        // MyComponentMonitoringFeatures[] cmfeatures = new MyComponentMonitoringFeatures[2];

        try {

            // Extension Type Mappings
            String $arrayOfNUMBER = "http://www.slaatsoi.org/types#array_of_NUMBER";

            // ////////////////////////////////////////////////////////////////////////////////
            // Standard Events
            // ////////////////////////////////////////////////////////////////////////////////
            Event request_event = ffi.createEvent();
            Event response_event = ffi.createEvent();
            request_event.setType("REQUEST");
            response_event.setType("RESPONSE");

            // ////////////////////////////////////////////////////////////////////////////////
            // SENSOR Features
            // ////////////////////////////////////////////////////////////////////////////////
            cmfeatures[0] = ffi.createComponentMonitoringFeatures();
            // cmfeatures[0] = new MyComponentMonitoringFeatures();
            cmfeatures[0].setUuid("777e8400-sss2-41d4-a716-406075043333");
            cmfeatures[0].setType("SENSOR");
            // Arrival_Rate Sensor
            Basic arrival_rate_sensor = ffi.createPrimitive();
            // BasicMonitoringFeature arrival_rate_sensor = new BasicMonitoringFeature();
            arrival_rate_sensor.setName(common.$arrival_rate);
            arrival_rate_sensor.setDescription("dummy");
            // add REQUEST events as input to arrival_rate sensor
            Basic[] arrival_rate_inputs = new Basic[2];
            arrival_rate_inputs[0] = request_event;

            // Completion_Time Sensor
            Basic completion_time_sensor = ffi.createPrimitive();
            // BasicMonitoringFeature completion_time_sensor = new BasicMonitoringFeature();
            completion_time_sensor.setName(common.$completion_time);
            completion_time_sensor.setDescription("dummy");
            Basic[] completion_time_inputs = new Basic[2];
            completion_time_inputs[0] = request_event;
            completion_time_inputs[1] = response_event;

            // VM_Image Sensor
            Basic vmimage_sensor = ffi.createPrimitive();
            // BasicMonitoringFeature vmimage_sensor = new BasicMonitoringFeature();
            vmimage_sensor.setName("http://www.slaatsoi.org/commonTerms#vm_image");
            vmimage_sensor.setDescription("dummy");

            Basic request_sensor = ffi.createPrimitive();
            // BasicMonitoringFeature request_sensor = new BasicMonitoringFeature();
            request_sensor.setName("REQUEST");
            request_sensor.setDescription("Sends Request events for the service and/or operation");

            Basic response_sensor = ffi.createPrimitive();
            // BasicMonitoringFeature response_sensor = new BasicMonitoringFeature();
            response_sensor.setName("RESPONSE");
            response_sensor.setDescription("Sends response events for the service and/or operation");

            // ////////////////////////////////////////////////////////////////////////////////
            // REASONER Features
            // ////////////////////////////////////////////////////////////////////////////////
            // cmfeatures[1] = new MyComponentMonitoringFeatures();
            cmfeatures[1] = ffi.createComponentMonitoringFeatures();
            cmfeatures[1].setUuid("550e8400-e29b-41d4-a716-406075047400");
            cmfeatures[1].setType("REASONER");
            // ////////////////////////////////////////////////////////////////////////////////
            // LESS_THAN Reasoner
            // FunctionMonitoringFeature loe_impl = new FunctionMonitoringFeature();
            Function loe_impl = ffi.createFunction();
            loe_impl.setName(core.$less_than);
            loe_impl.setDescription("none");
            Basic[] inputs = new Basic[2];
            Basic input1 = ffi.createPrimitive();
            input1.setName("input1");
            input1.setType(meta.$NUMBER);
            Basic input2 = ffi.createPrimitive();
            input2.setName("input2");
            input2.setType(meta.$NUMBER);
            inputs[0] = input1;
            inputs[1] = input2;
            loe_impl.setInput(inputs);
            Basic output = ffi.createPrimitive();
            output.setName("output1");
            output.setType(meta.$BOOLEAN);
            loe_impl.setOutput(output);
            // ////////////////////////////////////////////////////////////////////////////////
            // GREATER_THAN_OR_EQUALS Reasoner
            Function gre_impl = ffi.createFunction();
            // FunctionMonitoringFeature gre_impl = new FunctionMonitoringFeature();
            gre_impl.setName(core.$greater_than);
            gre_impl.setDescription("none");
            Basic[] gre_impl_inputs = new Basic[2];
            Basic gre_impl_input1 = ffi.createPrimitive();
            gre_impl_input1.setName("param1");
            gre_impl_input1.setType(meta.$NUMBER);
            Basic gre_impl_input2 = ffi.createPrimitive();
            gre_impl_input2.setName("param2");
            gre_impl_input2.setType(meta.$NUMBER);
            gre_impl_inputs[0] = gre_impl_input1;
            gre_impl_inputs[1] = gre_impl_input2;
            gre_impl.setInput(gre_impl_inputs);
            Basic gre_impl_output = ffi.createPrimitive();
            gre_impl_output.setName(meta.$BOOLEAN);
            gre_impl.setOutput(gre_impl_output);
            // ////////////////////////////////////////////////////////////////////////////////
            // Availability Reasoner
            Function avail_impl = ffi.createFunction();
            // FunctionMonitoringFeature avail_impl = new FunctionMonitoringFeature();
            avail_impl.setName(common.$availability);
            avail_impl.setDescription("none");
            Basic[] avail_input = new Basic[1];
            Basic avail_input_1 = ffi.createPrimitive();
            avail_input_1.setName("param1");
            avail_input_1.setType($arrayOfNUMBER);
            avail_input[0] = avail_input_1;
            avail_impl.setInput(avail_input);
            Basic avail_output = ffi.createPrimitive();
            avail_output.setName(meta.$NUMBER);
            avail_impl.setOutput(output);
            // ////////////////////////////////////////////////////////////////////////////////
            // meta.NUMBER Equals Reasoner
            Function equals_impl = ffi.createFunction();
            // FunctionMonitoringFeature equals_impl = new FunctionMonitoringFeature();
            equals_impl.setName(core.$equals);
            equals_impl.setDescription("none");
            Basic[] equals_impl_inputs = new Basic[2];
            Basic equals_impl_input1 = ffi.createPrimitive();
            equals_impl_input1.setName("param1");
            equals_impl_input1.setType(meta.$NUMBER);
            Basic equals_impl_input2 = ffi.createPrimitive();
            equals_impl_input2.setName("param2");
            equals_impl_input2.setType(meta.$NUMBER);
            equals_impl_inputs[0] = equals_impl_input1;
            equals_impl_inputs[1] = equals_impl_input2;
            equals_impl.setInput(equals_impl_inputs);
            Basic equals_impl_output = ffi.createPrimitive();
            equals_impl_output.setName(meta.$BOOLEAN);
            equals_impl.setOutput(equals_impl_output);
            // ////////////////////////////////////////////////////////////////////////////////
            // meta.TEXT Equals Reasoner
            Function text_equals_impl = ffi.createFunction();
            // FunctionMonitoringFeature text_equals_impl = new FunctionMonitoringFeature();
            text_equals_impl.setName(core.$equals);
            text_equals_impl.setDescription("none");
            Basic[] text_equals_impl_inputs = new Basic[2];
            Basic text_equals_impl_input1 = ffi.createPrimitive();
            text_equals_impl_input1.setName("param1");
            text_equals_impl_input1.setType(meta.$TEXT);
            Basic text_equals_impl_input2 = ffi.createPrimitive();
            text_equals_impl_input2.setName("param2");
            text_equals_impl_input2.setType(meta.$TEXT);
            text_equals_impl_inputs[0] = text_equals_impl_input1;
            text_equals_impl_inputs[1] = text_equals_impl_input2;
            text_equals_impl.setInput(text_equals_impl_inputs);
            Basic text_equals_impl_output = ffi.createPrimitive();
            text_equals_impl_output.setName(meta.$BOOLEAN);
            text_equals_impl.setOutput(text_equals_impl_output);
            // ////////////////////////////////////////////////////////////////////////////////
            // MEAN Reasoner
            Function mean_impl = ffi.createFunction();
            // FunctionMonitoringFeature mean_impl = new FunctionMonitoringFeature();
            mean_impl.setName(core.$mean);
            mean_impl.setDescription("none");
            Basic[] mean_input = new Basic[1];
            Basic mean_input_1 = ffi.createPrimitive();
            mean_input_1.setName("param1");
            mean_input_1.setType($arrayOfNUMBER);
            mean_input[0] = mean_input_1;
            mean_impl.setInput(mean_input);
            Basic mean_output = ffi.createPrimitive();
            mean_output.setName("output1");
            mean_output.setType(meta.$NUMBER);
            mean_impl.setOutput(output);
            // ////////////////////////////////////////////////////////////////////////////////
            // SERIES Reasoner
            Function series_impl = ffi.createFunction();
            // FunctionMonitoringFeature series_impl = new FunctionMonitoringFeature();
            series_impl.setName(core.$series);
            series_impl.setDescription("none");
            Basic[] series_inputs = new Basic[2];
            Basic series_input_1 = ffi.createPrimitive();
            series_input_1.setName("param1");
            series_input_1.setType(meta.$NUMBER);
            series_inputs[0] = series_input_1;
            series_inputs[1] = response_event;
            series_impl.setInput(series_inputs);
            Basic series_output = ffi.createPrimitive();
            series_output.setName("output1");
            series_output.setType($arrayOfNUMBER);
            series_impl.setOutput(output);

            MonitoringFeature[] cmf1features = new MonitoringFeature[5];
            cmf1features[0] = arrival_rate_sensor;
            cmf1features[1] = completion_time_sensor;
            cmf1features[2] = request_sensor;
            cmf1features[3] = response_sensor;
            cmf1features[4] = vmimage_sensor;

            MonitoringFeature[] cmf2features = new MonitoringFeature[7];
            cmf2features[0] = loe_impl;
            cmf2features[1] = mean_impl;
            cmf2features[2] = series_impl;
            cmf2features[3] = gre_impl;
            cmf2features[4] = avail_impl;
            cmf2features[5] = equals_impl;
            cmf2features[6] = text_equals_impl;

            cmfeatures[0].setMonitoringFeatures(cmf1features);
            cmfeatures[1].setMonitoringFeatures(cmf2features);
        }
        catch (Exception E) {
            // System.out.println(E.getMessage());
        }

        return cmfeatures;
    }
}
