/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.swslam.poc.planhandler;

import org.apache.log4j.Logger;
import org.slasoi.gslam.commons.plan.Plan;
import org.slasoi.gslam.commons.plan.RootFoundException;
import org.slasoi.gslam.commons.plan.TaskFoundException;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.softwareservicemanager.IProvisioning;
import org.slasoi.softwareservicemanager.impl.SoftwareServiceManagerFacade;
import org.slasoi.softwareservicemanager.provisioning.ProvisionServiceStub;
import org.slasoi.swslam.commons.plan.SoftwareTask;

public class PlanHandlerImpl {
    private SLA sla;
    private Plan plan;
    private SoftwareTask node;
    private static int planId = 0;
    private static int nodeId = 0;
    private static final String PLAN_ID = "SWSLAM-Plan";
    private static final String TASK_ID = "SWSLAM-Task";
    private static final String PROVISION = "PROVISION";
    private static final String Service_Manager_ID = "SOFTWARE_SERVICE_MANAGER_ID";
    public static final String propertiesFile =
            "generic-slamanager" + System.getProperty("file.separator") + "provisioning-adjustment"
                    + System.getProperty("file.separator") + "provisioning_adjustment.properties";
    private SoftwareServiceManagerFacade softwareServiceManagerFacade;
    private static final Logger LOGGER = Logger.getLogger(PlanHandlerImpl.class);

    public PlanHandlerImpl(SLA sla) {
        this.sla = sla;
    }

    public Plan planMaker() {
        try {
            // create the plan
            String planID = this.sla.getPropertyValue(new STND("plan_id"));
            LOGGER.info("Plan-ID extracted from SLA = " + planID);
            plan = new Plan(planID);
            LOGGER.info("Plan is created.");
            // create the nodes
            node =
                    new SoftwareTask(this.getTaskId(), this.sla.getUuid().getValue(), PlanHandlerImpl.PROVISION,
                            PlanHandlerImpl.Service_Manager_ID);

            String service_buider_id = this.sla.getPropertyValue(org.slasoi.slamodel.vocab.sla.service_id);
            LOGGER.info("*** Before retrieving ServiceBuilder from SSM with ServiceID = " + service_buider_id);
            ServiceBuilder serviceBuilder = this.softwareServiceManagerFacade.getBuilder(service_buider_id);
            LOGGER.info("*** After retrieving ServiceBuilder from SSM");
            // pass the serviceBilder object into the Task
            node.setServiceBuilder(serviceBuilder);
            plan.setRoot(node);
            LOGGER.info("The main node of the plan is created and set into plan.");

            LOGGER.info("The children of the node are created and set into the node.");
            return plan;
        }
        catch (TaskFoundException e) {
            e.printStackTrace();
            return null;
        }
        catch (RootFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setSoftwareServiceManagerFacade(SoftwareServiceManagerFacade softwareServiceManagerFacade) {
        this.softwareServiceManagerFacade = softwareServiceManagerFacade;
    }

    private String getPlanId() {
        planId++;
        return (PLAN_ID + "_" + planId);
    }

    private String getTaskId() {
        nodeId++;
        return (TASK_ID + "_" + nodeId);
    }
}
