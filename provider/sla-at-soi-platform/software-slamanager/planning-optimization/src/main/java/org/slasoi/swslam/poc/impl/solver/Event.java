package org.slasoi.swslam.poc.impl.solver;

import java.util.ArrayList;
import java.util.List;

public class Event {

    private EventName name = EventName.QueryEvent;
    private SoftwareServiceOperation bookSaleOperation;
    private SoftwareServiceOperation productDetailsOperation;
    private SoftwareServiceOperation paymentServiceOperation;
    private List<InfrastructureServiceTerms> listOfIsTerms = new ArrayList<InfrastructureServiceTerms>();
    private RiskQuantifier riskQuantifier = RiskQuantifier.LOW;

    public Event(EventName name) {
        this.name = name;
    }

    public EventName getName() {
        return name;
    }

    public void setName(EventName name) {
        this.name = name;
    }

    public SoftwareServiceOperation getBookSaleOperation() {
        return bookSaleOperation;
    }

    public void setBookSaleOperation(SoftwareServiceOperation bookSaleOperation) {
        this.bookSaleOperation = bookSaleOperation;
    }

    public SoftwareServiceOperation getProductDetailsOperation() {
        return productDetailsOperation;
    }

    public void setProductDetailsOperation(SoftwareServiceOperation productDetailsOperation) {
        this.productDetailsOperation = productDetailsOperation;
    }

    public SoftwareServiceOperation getPaymentServiceOperation() {
        return paymentServiceOperation;
    }

    public void setPaymentServiceOperation(SoftwareServiceOperation paymentServiceOperation) {
        this.paymentServiceOperation = paymentServiceOperation;
    }

    public List<InfrastructureServiceTerms> getListOfIsTerms() {
        return listOfIsTerms;
    }

    public void setListOfIsTerms(List<InfrastructureServiceTerms> listOfIsTerms) {
        this.listOfIsTerms = listOfIsTerms;
    }

    public RiskQuantifier getRiskQuantifier() {
        return riskQuantifier;
    }

    public void setRiskQuantifier(RiskQuantifier riskQuantifier) {
        this.riskQuantifier = riskQuantifier;
    }
}
