/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * 
 */
package org.slasoi.swslam.pac;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.slasoi.gslam.core.control.Policy;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
public class PoliciesInterfaceTest {
    private static final Logger logger = Logger.getLogger(PoliciesInterfaceTest.class.getName());

    private static final String propertiesFile =
            "software-slamanager" + System.getProperty("file.separator") + "provisioning-adjustment"
                    + System.getProperty("file.separator") + "provisioning_adjustment.properties";

    private static final String POLICY_NAME_1 = "Policy1";
    private static final long ID_POLICY_1 = 1000;
    private static final String POLICY_CONTENT_1 =
            "rule \"Policy1\" \n when \n eval(true) \nthen \n System.out.println(\"Hello, here is the first test policy\"); \n end";
    private static final String POLICY_NAME_2 = "Policy2";
    private static final long ID_POLICY_2 = 2000;
    private static final String POLICY_CONTENT_2 =
            "rule \"Policy2\" \n when \n eval(true) \nthen \n System.out.println(\"Hello, here is the second test policy\"); \n end";

    @Before
    public void init() {

    }

    @Test
    public void policiesTest() {
        logger.info("getting policies");
        SoftwareProvisioningAdjustment pac = new SoftwareProvisioningAdjustment(propertiesFile);

        // First, get the original policies
        Policy[] policies = pac.getPolicies("Adjustment");
        printPolicies(policies);

        boolean sizeCheck = (policies.length > 0);
        assertTrue(sizeCheck);

        // Now, set policies for testing
        Policy[] testPolicies = createPolicies();
        logger.debug("Policies for testing:");
        printPolicies(testPolicies);
        pac.setPolicies("Adjustment", testPolicies);

        // Retrieve the policies to make some checks
        Policy[] test = pac.getPolicies("Adjustment");
        assertPolicies(test, testPolicies);

        // Restore the original policies
        pac.setPolicies("Adjustment", policies);
        pac.stop();
    }

    private Policy[] createPolicies() {
        Policy[] policies = new Policy[2];

        policies[0] = new Policy();
        policies[0].setId(ID_POLICY_1);
        policies[0].setName(POLICY_NAME_1);
        policies[0].setRule(POLICY_CONTENT_1);

        policies[1] = new Policy();
        policies[1].setId(ID_POLICY_2);
        policies[1].setName(POLICY_NAME_2);
        policies[1].setRule(POLICY_CONTENT_2);

        return policies;
    }

    private void printPolicies(Policy[] policies) {
        for (Policy policy : policies) {
            logger.info("Policy id = " + policy.getId());
            logger.info("Policy name " + policy.getName());
            logger.info("Policy content  = " + policy.getRule());
        }
    }

    private void assertPolicies(Policy[] policies1, Policy[] policies2) {
        // assertEquals(policies1.length, policies2.length);
        for (int i = 0; i < policies1.length; i++) {
            long id = policies1[i].getId();
            // search for the policy with the same id
            for (int j = 0; j < policies2.length; j++) {
                if (policies2[j].getId() == id) {
                    assertEquals(policies1[i].getName(), policies2[j].getName());
                    assertEquals(policies1[i].getRule(), policies2[j].getRule());
                }
            }
        }
    }

}
