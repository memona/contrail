/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * 
 */
package org.slasoi.swslam.pac;

/**
 * @author Beatriz Fuentes (TID)
 *
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.slasoi.gslam.commons.plan.Plan;
import org.slasoi.swslam.commons.plan.SoftwareTask;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
// @RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "classpath:/org/slasoi/swslam/pac/context-TestProvisioning.xml" })
public class ProvisioningTest {
    private static final Logger logger = Logger.getLogger(ProvisioningTest.class.getName());

    private static final String PLAN_ID = "MyPlan";
    private static final String TASK_ID = "MyTask";
    private static final String SLA_ID = "MySLA";
    private static final String SERVICE_MANAGER_ID = "INFRA_SERVICE_MANAGER_ID";
    private static final String PROVISION = "PROVISION";

    // @Autowired
    org.slasoi.swslam.pac.mockups.SoftwareServiceManagerMockup swServiceManager;

    // @Autowired
    org.slasoi.swslam.pac.SoftwareProvisioningAdjustment pac;

    // @Autowired
    org.slasoi.swslam.pac.mockups.ContextMockup context;

    private static int planId = -2;
    private static int nodeId = 0;

    private static String EVENT_BUS = "eventbus_properties_file";
    private static String CHANNEL = "channel";
    private static String configFilesPath = System.getenv("SLASOI_HOME") + System.getProperty("file.separator");
    private static String TEST_PROPERTIES_FILE =
            configFilesPath + "infrastructure-slamanager" + System.getProperty("file.separator")
                    + "provisioning-adjustment" + System.getProperty("file.separator") + "test"
                    + System.getProperty("file.separator") + "test.properties";
    private Properties properties = new Properties();

    @Before
    public void init() {
        // Read the properties file
        try {
            properties.load(new FileInputStream(TEST_PROPERTIES_FILE));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        // swServiceManager.setProvisionCalled(false);
        // swServiceManager.setEventBusProperties(configFilesPath + properties.getProperty(EVENT_BUS), properties
        // .getProperty(CHANNEL));
    }

    @Test
    public void executePlan1() {
        logger.info("Executing plan with only one node...");

        assert (true);
        return;

        // try {
        // pac.executePlan(createPlanOneNode());
        // }
        // catch (PlanFoundException e) {
        // e.printStackTrace();
        // }
        // catch (PlanFormatException e) {
        // e.printStackTrace();
        // }
        // org.junit.Assert.assertEquals(swServiceManager.isProvisionCalled(), true);

    }

    // @After
    public void stop() {
        pac.stop();
    }

    private Plan createPlanOneNode() {
        Plan plan = new Plan(getPlanId());
        SoftwareTask node = new SoftwareTask(getTaskId(), getSlaId(), PROVISION, SERVICE_MANAGER_ID);

        // ProvisionRequestType request = new ProvisionRequestType();

        // node.setProvisionRequest(request);
        // generateSLA(request);

        try {
            plan.setRoot(node);
        }
        catch (Exception e) {
            // Shouldn't happen, it's the first task we add to the plan
            e.printStackTrace();
        }

        return plan;
    }

    /*
     * private void generateSLA(ProvisionRequestType request) { SLA sla = new SLA();
     * 
     * UUID slaUuid = new UUID((java.util.UUID.randomUUID()).toString()); sla.setUuid(slaUuid); //
     * sla.setProvisionId(slaUuid.toString()); // sla.setProvisionRequestType(request);
     * 
     * logger.debug("Storing new SLA: "); logger.debug(sla.toString());
     * 
     * try { context.getSLARegistry().getIRegister().register(sla, null, null); } catch (RegistrationFailureException e)
     * { // TODO Auto-generated catch block e.printStackTrace(); } catch (SLAManagerContextException e) { // TODO
     * Auto-generated catch block e.printStackTrace(); } }
     */
    private String getPlanId() {
        planId++;
        return (PLAN_ID + "_" + planId);
    }

    private String getTaskId() {
        nodeId++;
        return (TASK_ID + "_" + nodeId);
    }

    private String getSlaId() {
        return (SLA_ID + "_" + nodeId);
    }

}
