/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.swslam.main.beans;

import java.util.Arrays;

import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.sa.pss4slam.core.ISubscribable;
import org.slasoi.sa.pss4slam.core.ISubscriber;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisement;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisementChannel;
import org.slasoi.sa.pss4slam.core.Template;
import org.slasoi.slamodel.sla.SLATemplate;

public class DynTemplateSubscriber implements Runnable
{
    public DynTemplateSubscriber( SLAManagerContext ctx )
    {
        context = ctx;
    }
    
    public void start()
    {
        Thread tr = new Thread( this );
        tr.start();
    }
    
    public void run()
    {
        try
        {
            ServiceAdvertisement sa = context.getServiceAdvertisement();
            ISubscribable iSubscribable = sa.getISubscribable();
            iSubscribable.subscribe( new ISubscriber()
            {
                public void notify( Template<?>[] templates )
                {
                    try
                    {
                        System.out.println( "DynTemplateSubscriber ::  getting >> " + templates.length + " templates!" );
                        System.out.println( "\t\t" + Arrays.toString( templates ) );
                        
                        SLATemplateRegistry slaTemplateRegistry = context.getSLATemplateRegistry();
                        // FIXME:  any filter should be executed ?
                        
                        for ( int i = 0; i < templates.length; i++ )
                        {
                            // FIXME:  insert into TemplateRegistry ::  what'S a Metadata ??
                            Template<SLATemplate> tmp = (Template<SLATemplate>)templates[ i ];
                            slaTemplateRegistry.addSLATemplate( tmp.getContent(), null );
                        }
                    }
                    catch ( Exception e )
                    {
                    }
                }
                
            }, ServiceAdvertisementChannel.SLAMODEL );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }
    
    protected String namesAsString( Template<?>[] templates )
    {
        StringBuffer sb = new StringBuffer();
        for ( Template<?> curr : templates )
        {
            SLATemplate t = (SLATemplate)curr.getContent();
            sb.append( t.getUuid().getValue() + ", " );
        }
        
        return sb.toString();
    }
    
    protected SLAManagerContext context;
}
