/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.swslam.main.beans;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.slasoi.gslam.core.builder.PlanningOptimizationBuilder;
import org.slasoi.gslam.core.context.GenericSLAManagerServices;
import org.slasoi.gslam.core.context.GenericSLAManagerServices.SLAMConfiguration;
import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.springframework.osgi.context.BundleContextAware;
import org.springframework.osgi.extensions.annotation.ServiceReference;

public class SwSLAMBean implements BundleContextAware
{
    public void start()
    {
        try
        {
            SLAMConfiguration swConfig = gslamServices.loadConfigurationFrom( "swslam.instance1.cfg" );
            
            swContext = gslamServices.createContext( osgiContext,
                                                     swConfig.name, 
													 swConfig.epr, 
													 swConfig.group,
													 swConfig.wsPrefix );
            
            swContext.setProperties( swConfig.properties );
            
            //  It overwrites the BasicAuthorization 
            //  provided by generic-slam.  SwAuthorization 
            //  will be used for negotiation in Sw-SLAM
            SwAuthorization auth = new SwAuthorization();
            swContext.setAuthorization( auth );
            
            // Inject the POC into the swContext
            swContext.setPlanningOptimization( swPOC );
            injectIntoContext( swPOC, swContext );
            
            // Inject the PAC into the swContext
            swContext.setProvisioningAdjustment( swPAC );
            injectIntoContext( swPAC, swContext );
            
            LOGGER.info( "\n\n \t*** :: start :: gslamServices >> \n" + gslamServices );
            LOGGER.info( "\n\n \t*** :: start :: SwSLAMBean >> \n"    + swContext     );
            
            /* FIXME:  commented since integration is in progress
            DynTemplatePublisher ghost = new DynTemplatePublisher( swContext );
            ghost.start();
            
            DynTemplateSubscriber watcher = new DynTemplateSubscriber( swContext );
            watcher.start();
            */
        }
        catch ( Exception e )
        {
            LOGGER.debug( e );
            e.printStackTrace();
        }
    }
    
    public void stop()
    {
    }
    
    public void setBundleContext( BundleContext osgiContext )
    {
        this.osgiContext = osgiContext;
        
        SwSLAMTracer tracer = new SwSLAMTracer();
        osgiContext.registerService( tracer.getClass().getName(), tracer, null );
    }
    
    protected void injectIntoContext( Object obj, SLAManagerContext context )
    {
        if ( obj instanceof SLAMContextAware )
        {
            ((SLAMContextAware)obj).setSLAManagerContext( context );
        }
    }
    
    @ServiceReference
    public void setGslamServices( GenericSLAManagerServices gslamServices )
    {
        LOGGER.info( "generic-slam injected successfully into sw-slam" );
        this.gslamServices = gslamServices;
    }

    @ServiceReference( filter = "(proxy=sw-poc)" )
    public void setPOC( PlanningOptimizationBuilder builder )
    {
        LOGGER.info( "sw-POC injected successfully into sw-slam" );
        
        this.swPOC = builder.create();
    }
    
    @ServiceReference( filter = "(proxy=sw-pac)" )
    public void setPAC( ProvisioningAdjustment swPAC )
    {
        LOGGER.info( "sw-PAC injected successfully into sw-slam" );
        this.swPAC = swPAC;
    }
    
    protected SLAManagerContext         swContext;
    protected GenericSLAManagerServices gslamServices;
    
    protected PlanningOptimization   swPOC;
    protected ProvisioningAdjustment swPAC;
    
    protected BundleContext osgiContext;
    
    protected static int INSTANCES = 0;

    private static final Logger LOGGER = Logger.getLogger( SwSLAMBean.class );
    
    public class SwSLAMTracer
    {
        /*
         *  method to be invoked from osgi-console via 'echo' command
         */
        public void context()
        {
            System.out.println( swContext );
        }
        
        /*
         *  method to be invoked from osgi-console via 'echo' command
         */
        public void slamID()
        {
            try
            {
                System.out.println( "\t\t swSLAM ID = "+swContext.getSLAManagerID() );
            }
            catch ( Exception e )
            {
            }
        }
        
        /*
         *  method to be invoked from osgi-console via 'invoke' command
         */
        public void values( Hashtable<String, String> params )
        {
            System.out.println( "\t\t swSLAM-params = " + params );
        }
        
        public void assertions()
        {
            String a = null;
            assert( a != null ) : "assertions ON";
            
            System.out.println( "assertions OFF" );
        }
    }
}
