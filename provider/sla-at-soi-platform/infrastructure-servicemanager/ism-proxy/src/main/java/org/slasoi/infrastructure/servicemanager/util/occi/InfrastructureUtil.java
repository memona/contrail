package org.slasoi.infrastructure.servicemanager.util.occi;

import static com.google.common.base.Throwables.propagate;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.jclouds.entities.IServices;
import org.jclouds.entities.domain.IGrpResourceAvailMetadata;
import org.jclouds.entities.domain.IPageSet;
import org.jclouds.entities.domain.IServiceStatus;
import org.jclouds.entities.io.ICategoryMetadata;
import org.slasoi.infrastructure.servicemanager.exceptions.ProvisionException;
import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;
import org.slasoi.infrastructure.servicemanager.impl.ImageRegistryImpl;
import org.slasoi.infrastructure.servicemanager.impl.LocRegistryImpl;
import org.slasoi.infrastructure.servicemanager.impl.MetricRegistryImpl;
import org.slasoi.infrastructure.servicemanager.impl.OSRegistryImpl;
import org.slasoi.infrastructure.servicemanager.occi.types.Terms;
import org.slasoi.infrastructure.servicemanager.prediction.PredictionQuery;
import org.slasoi.infrastructure.servicemanager.registry.Registry;
import org.slasoi.infrastructure.servicemanager.types.CapacityResponseType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;
import org.slasoi.infrastructure.servicemanager.types.ReservationResponseType;
import org.slasoi.infrastructure.servicemanager.types.SchemaConstants;

import eu.slasoi.infrastructure.model.Category;
import eu.slasoi.infrastructure.model.Kind;
import eu.slasoi.infrastructure.model.infrastructure.Compute;
import eu.slasoi.infrastructure.model.infrastructure.Service;

/**
 * Utility for Infrastructure Service Manager Implementation.
 * 
 * @author Patrick Cheevers
 * 
 */
public class InfrastructureUtil {
    /**
     * @return the serviceActions
     */
    public final Set<String> getServiceActions() {
        return serviceActions;
    }

    /**
     * @param serviceActions the serviceActions to set
     */
    public final void setServiceActions(Set<String> serviceActions) {
        this.serviceActions = serviceActions;
    }

    /**
     * @return the resourceActions
     */
    public final Set<String> getResourceActions() {
        return resourceActions;
    }

    /**
     * @param resourceActions the resourceActions to set
     */
    public final void setResourceActions(Set<String> resourceActions) {
        this.resourceActions = resourceActions;
    }

    /**
     * @return the osReg
     */
    public final Registry<?, ?> getOsReg() {
        return osReg;
    }

    /**
     * @param osReg the osReg to set
     */
    public final void setOsReg(final Registry<?, ?> osReg) {
        this.osReg = osReg;
    }

    /**
     * @return the locReg
     */
    public final Registry<?, ?> getLocReg() {
        return locReg;
    }

    /**
     * @param locReg the locReg to set
     */
    public final void setLocReg(final Registry<?, ?> locReg) {
        this.locReg = locReg;
    }

    /**
     * @return the metricReg
     */
    public final Registry<?, ?> getMetricReg() {
        return metricReg;
    }

    /**
     * @param metricReg the metricReg to set
     */
    public final void setMetricReg(Registry<?, ?> metricReg) {
        this.metricReg = metricReg;
    }

    /**
     * @return the imageReg
     */
    public final Registry<?, ?> getImageReg() {
        return imageReg;
    }

    /**
     * @param imageReg the imageReg to set
     */
    public final void setImageReg(Registry<?, ?> imageReg) {
        this.imageReg = imageReg;
    }

    /**
     * @return the predictionQuery
     */
    public final PredictionQuery getPredictionQuery() {
        return predictionQuery;
    }

    private static final String SPACE = " ";

    public static final String COMPUTE_MEMORY = "occi.compute.memory";
    public static final String HOSTNAME = "occi.compute.hostname";
    public static final String COMPUTE = "compute";
    public static final String GOLD = "gold";
    public static final String COMPUTE_SPEED = "occi.compute.speed";
    public static final String OCCI_COMPUTE_ARCHITECTURE = "occi.compute.architecture";
    public static final String OCCI_COMPUTE_STATUS = "occi.compute.status";

    private static final String EQUALS = " = ";
    private static final String OPEN_HYPHEN = "'";
    private static final String COMMA = ",";
    private static final String CLOSE_HYPHEN = OPEN_HYPHEN;

    private Set<String> serviceActions = null;
    private Set<String> resourceActions = null;
    private Registry<?, ?> osReg = new OSRegistryImpl();
    private Registry<?, ?> locReg = new LocRegistryImpl();
    private Registry<?, ?> metricReg = new MetricRegistryImpl();
    private Registry<?, ?> imageReg = new ImageRegistryImpl();
    /**
     * The logger.
     */
    private static final Logger LOGGER = Logger.getLogger(InfrastructureUtil.class.getName());

    /**
     * Prediction Instrumentation.
     */
    private PredictionQuery predictionQuery;

    /**
     * Verify the status returned from reservation is correct otherwise create an exception
     * 
     * @param status
     * @param id
     * @return ReservationResponseType
     * @throws UnknownIdException
     */
    public ReservationResponseType verifyReserveStatus(final boolean status, final String id) throws UnknownIdException {

        if ((Boolean.TRUE == status) && (id != null)) {
            ReservationResponseType resp = new ReservationResponseType();
            resp.setInfrastructureID(id);
            return resp;
        } else {
            throw new UnknownIdException();
        }
    }

    // Compute vmConfiguration1 = ismServices.createComputeConfiguration(
    // Images.Ubuntu_9_10_32, Slas.GOLD, Locations.IE, 2, 1.0f, 512,
    // monitoringRequest, hostname
    // + Integer.toString(rnd.nextInt(100)), notificationURI);

    /**
     * Creates a Compute Configuration which will be used lated in a provision/reservation Request.
     * 
     * @param osID String
     * @param sizeTemplateID String 
     * @param locTemplateID String
     * @param extras Hashtable<String, String>
     * @return Compute
     */
    public Compute createComputeConfiguration(final String osID, final String sizeTemplateID, final String locTemplateID,

   final Hashtable<String, String> extras) {
        LOGGER.debug("CreateComputeConfiguration :");
        LOGGER.debug("CreateComputeConfiguration sizeTemplateID:" + sizeTemplateID);
        LOGGER.debug("CreateComputeConfiguration osId:" + osID);
        LOGGER.debug("CreateComputeConfiguration locTemplateID:" + locTemplateID);
        Category computeCategory = new Category();
        computeCategory.setTerm(Terms.COMPUTE);
        computeCategory.setScheme(SchemaConstants.OCCI_INFRASTRUCTURE_SCHEME);
        computeCategory.setCoreClass(Terms.OCCI_KIND);
        Compute vmComputeConfiguration = new Compute();

        Set<Category> categories = new HashSet<Category>();
        categories.add(computeCategory);

        Category osCategory = osReg.getCategoryByID(osID);
        if (osCategory == null) {
            LOGGER.error("Could not find an Operating System matching a Category - " + osID);
            osCategory = osReg.getDefaultCategory();
        }
        categories.add(osCategory);
        Category sizeTemplateCategory = metricReg.getCategoryByID(sizeTemplateID);
        if (sizeTemplateCategory == null) {
            LOGGER.error("Could not find an Size Template matching a Category - ");
            sizeTemplateCategory = metricReg.getDefaultCategory();
        }
        categories.add(sizeTemplateCategory);

        Category locTemplateCategory = locReg.getCategoryByID(locTemplateID);
        if (locTemplateCategory == null) {
            LOGGER.error("Could not find an Location Template matching a Category - ");
            locTemplateCategory = locReg.getDefaultCategory();

        } else {
            categories.add(locTemplateCategory);
        }

        vmComputeConfiguration.setCategories(categories);
        Hashtable<String, String> extrasCopy = new Hashtable<String, String>();
        extrasCopy.putAll(extras);
        vmComputeConfiguration.setExtras(extrasCopy);
        LOGGER.debug("CreateComputeConfiguration result : " + vmComputeConfiguration);

        return vmComputeConfiguration;
    }

    /**
     * Gets the The default service Kind.
     * 
     * @return Kind
     */
    public Kind getDefaultServiceKind() {
        LOGGER.debug("GetDefaultServiceKind : ");

        Service defaultService = new Service();
        Set<Category> cats = new HashSet<Category>();
        Category serviceCategory = new Category();
        serviceCategory.setTerm(Terms.SERVICE);
        serviceCategory.setScheme(SchemaConstants.SLA_AT_SOI_INFRASTRUCTURE_SCHEME);
        serviceCategory.setCoreClass(Terms.OCCI_KIND);
        cats.add(serviceCategory);
        defaultService.setCategories(cats);
        LOGGER.debug("GetDefaultServiceKind : result " + defaultService);
        return defaultService;
    }

    /**
     * Call s the prediction Query Service.
     * 
     * @param id String
     * @param metricName String
     * @param minutes int
     * @return String
     * @throws UnknownIdException
     */
    public String predictionQuery(final String id, final String metricName, final int minutes) throws UnknownIdException {
        LOGGER.debug("predictionQuery " + "id=" + id + " metricname=" + metricName + " minutes=" + minutes);

        return predictionQuery.query(id, metricName, minutes);
    }

    /**
     * Ensures that the response received after reservation or provision is valid otherwise throw error
     * 
     * @param groupingResponse URI
     * @param provisionRequestType ProvisionRequestType
     * @throws ProvisionException
     */
    public void validateResponse(final URI groupingResponse, final ProvisionRequestType provisionRequestType)
            throws ProvisionException {
        LOGGER.info("This is the response from the ISM Server");
        LOGGER.info("resourceID -  " + groupingResponse.getPath());
        if (groupingResponse == null) {
            LOGGER.error("Provision was not sucessful");
            throw new ProvisionException("Provision was not successful");
        }
    }

    /**
     * Ensures that the response received after reservation update or provision update is valid otherwise throw error
     * 
     * @param groupingResponse
     * @param provisionRequestType
     * @throws ProvisionException
     */
    public void validateUpdateResponse(IServiceStatus groupingResponse, ProvisionRequestType provisionRequestType)
            throws ProvisionException {
        LOGGER.info("This is the response from the ISM Server");
        LOGGER.info("Response -  " + groupingResponse.getPayload());
        if (groupingResponse == null) {
            LOGGER.error("Update was not sucessful");
            throw new ProvisionException("Update was not successful");
        }
    }

    /**
     * Returns a string representing the reservation category pay-load part
     * 
     * @param randId
     * @return
     */
    public static String getReservationCategoryPayload(UUID randId) {
        String category =
                "Category: reservation; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'\r\n";
        StringBuffer attribsBuffer = new StringBuffer("X-OCCI-Attribute: ");
        attribsBuffer.append(" occi.core.id='");
        attribsBuffer.append(randId);
        attribsBuffer.append("'");
        return category + "\n" + attribsBuffer.toString();

    }

    // Multipart
    /**
     * Builds a CategoryPayload Object which contains all information necessary for creating a reservation or provision
     * request
     * 
     * @param provisionRequestType
     * @return
     */
    public CategoryPayload getCatgeorysForPayload(ProvisionRequestType provisionRequestType) {
        CategoryPayload payLoad = new CategoryPayload();

        // We need to convert the ProvisionRequestType to an OCCI request
        LOGGER.debug("getServicePayload ");

        Set<Kind> kinds = provisionRequestType.getKinds();

        StringBuffer occi_categorys = null;
        StringBuffer computeBuffer = null;
        for (Kind kind : kinds) {

            if (kind instanceof Compute) {
                computeBuffer = new StringBuffer();
                Compute computeKind = (Compute) kind;
                Set<Category> cats = computeKind.getCategories();
                occi_categorys = new StringBuffer();
                occi_categorys.append("Category: ");
                boolean firstcat = true;
                for (Category cat : cats) {

                    if (firstcat == false) {
                        occi_categorys.append(COMMA);

                    }
                    firstcat = false;
                    occi_categorys.append(getCat(cat));

                }
                computeBuffer.append(occi_categorys);
                computeBuffer.append("\n");
                computeBuffer.append(getComputeAttribs(computeKind, computeKind.getUniqueId()));
                payLoad.getComputeCatPayloadList().put(computeKind.getUniqueId().toString(),
                        computeBuffer.toString());
            }
            if ((kind instanceof Service)) {

                Service serviceKind = (Service) kind;
                String monitoringConfig = provisionRequestType.getMonitoringRequest();
                String serviceName = serviceKind.getResourceId();
                String serviceID = serviceKind.getServiceId().toString();
                payLoad.setSeviceCatPayLoad(getServiceCategoryPayload(serviceKind.getServiceId(), monitoringConfig,
                        serviceName));
                payLoad.setReservationCatKey(serviceID);
                payLoad.setReservationCatKey(serviceID);
                payLoad.setReservationCatPayLoad(getReservationCategoryPayload(serviceKind.getServiceId()));
            }

        }

        LOGGER.debug("getServicePayload  Result =" + payLoad);

        return payLoad;

    }

    /**
     * Returns a string representing the service category pay-load part
     * 
     * @param randId
     * @param monitoringConfig
     * @param serviceName
     * @return
     */
    public static String getServiceCategoryPayload(UUID randId, String monitoringConfig, String serviceName) {
        String category =
                "Category: service; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'; title='An infrastructure service'";
        StringBuffer attribsBuffer = new StringBuffer("X-OCCI-Attribute: ");
        StringBuffer monitorBuffer = new StringBuffer("");
        attribsBuffer.append(" occi.core.id='");
        attribsBuffer.append(randId);
        attribsBuffer.append("'");

        if (serviceName != null) {
            attribsBuffer.append(", eu.slasoi.infrastructure.service.name='");
            attribsBuffer.append("serviceName");
            attribsBuffer.append("'");
        }

        // eu.slasoi.infrastructure.service.monitoring.config=' '

        // if ((monitoringConfig != null) && !monitoringConfig.isEmpty()) {
        // // monitorBuffer
        //
        // monitorBuffer.append("<xml>");
        // monitorBuffer.append(monitoringConfig);
        // monitorBuffer.append("</xml>");
        // }

        return category + "\n" + attribsBuffer.toString() + "\n" + monitorBuffer + "\n";

    }

    // Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#';
    // class='kind', ubuntu_10-11;
    // scheme='http://sla-at-soi.eu/occi/templates#'; class='mixin'

    /**
     * Given a Category return a string which is acceptable to OCCI
     * 
     * @param cat
     * @return
     */
    private String getCat(Category cat) {

        StringBuffer catsBuffer = new StringBuffer();
        // Eg category= "Category: compute;
        // scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind',
        // Eg ubuntu_10-11; scheme='http://sla-at-soi.eu/occi/templates#';
        // class='mixin'";

        catsBuffer.append(cat.getTerm()); // eg compute
        catsBuffer.append(";");
        catsBuffer.append(SPACE);
        catsBuffer.append("scheme");
        catsBuffer.append(EQUALS);
        catsBuffer.append(OPEN_HYPHEN);
        catsBuffer.append(cat.getScheme());
        catsBuffer.append(CLOSE_HYPHEN);
        catsBuffer.append(";");
        catsBuffer.append(SPACE);
        catsBuffer.append("class");
        catsBuffer.append(EQUALS);
        catsBuffer.append(OPEN_HYPHEN);
        catsBuffer.append(cat.getCoreClass());
        catsBuffer.append(CLOSE_HYPHEN);
        catsBuffer.append(SPACE);
        // catsBuffer.append(";");

        /*
         * catsBuffer.append("title"); catsBuffer.append(EQUALS); catsBuffer.append(OPEN_HYPHEN);
         * catsBuffer.append(cat.getTitle()); catsBuffer.append(CLOSE_HYPHEN);
         */// Title not passed

        return catsBuffer.toString();
    }

    // X-OCCI-Attribute: occi.core.title='Title', occi.compute.cores=1,
    // occi.compute.state='active', occi.compute.architecture='x86',
    // occi.compute.memory=0.5, occi.compute.speed=2.4,
    // occi.core.summary='Summary', occi.compute.hostname=''

    /**
     * Using the computeKind builds a string of attributes in the OCCI format
     * 
     * @param computeKind
     * @param randId
     * @return
     */
    public static String getComputeAttribs(Compute computeKind, UUID randId) {
        StringBuffer occi_attribs = null;
        // String compute_memory = Integer.toString((int) computeKind
        // .getMemory_size());
        // String core_summary = "summary";
        String core_title = String.valueOf(computeKind.getUniqueId());
        // String compute_state = computeKind.getStatus();
        // String compute_hostname = computeKind.getHostname();
        // String compute_architecture = computeKind.getCpu_arch();
        // String compute_cores = Integer.toString(computeKind.getCpu_cores());
        // String compute_speed = Integer.toString((int) (1000.0f * computeKind
        // .getCpu_speed()));
        Hashtable<String, String> extras = computeKind.getExtras();

        occi_attribs = new StringBuffer("X-OCCI-Attribute: ");
        occi_attribs.append("occi.core.id");
        occi_attribs.append(EQUALS);
        occi_attribs.append(OPEN_HYPHEN);
        occi_attribs.append(randId);
        occi_attribs.append(CLOSE_HYPHEN);
        occi_attribs.append(COMMA);
        occi_attribs.append(Terms.OCCI_CORE_TITLE);
        occi_attribs.append(EQUALS);
        occi_attribs.append(OPEN_HYPHEN);
        occi_attribs.append(core_title);
        occi_attribs.append(CLOSE_HYPHEN);

        Set<String> set = extras.keySet();

        Iterator<String> itr = set.iterator();
        String key;
        while (itr.hasNext()) {
            key = itr.next();
            occi_attribs.append(COMMA);
            occi_attribs.append(key);
            occi_attribs.append(EQUALS);
            occi_attribs.append(OPEN_HYPHEN);
            occi_attribs.append(extras.get(key));
            occi_attribs.append(CLOSE_HYPHEN);

        }
        return occi_attribs.toString();
    }

    /**
     * Sets the @PredictionQuery.
     * 
     * @param predictionQuery
     *            The @PredictionQuery
     */
    public void setPredictionQuery(PredictionQuery predictionQuery) {
        this.predictionQuery = predictionQuery;
    }

    /******************************* Rewrite ********************/
    public static Properties getPropertiesFromResource(String fileName) {
        Properties properties = new Properties();

        try {
            FileInputStream inputStream = new FileInputStream(fileName);

            properties.load(inputStream);
        } catch (IOException e) {
            propagate(e);
        }
        properties.putAll(System.getProperties());
        return properties;
    }

    /**
     * Obtains A list of Capacity ResonsType Objects which describe the capacity available in the cloud
     * 
     * @param groupings
     * @return
     */
    public List<CapacityResponseType> queryCapacity(IServices groupings) {
        IPageSet<? extends IGrpResourceAvailMetadata> resources = groupings.listAvailResources("");
        MetricRegistryImpl metricreg = (MetricRegistryImpl) metricReg;
        return metricreg.getMetricDetailReg().setup(resources);
    }

    /**
     * Build a list of actions One can take on services in the cloud
     * 
     * @param resources
     */
    public void setupActionList(IPageSet<? extends IGrpResourceAvailMetadata> resources) {
        serviceActions = new HashSet<String>();
        resourceActions = new HashSet<String>();

        String scheme = null;
        String coreClass = null;
        String term = null;
        for (IGrpResourceAvailMetadata resourceMd : resources) {
            ICategoryMetadata catMd = resourceMd.getCategoryMetaData();
            scheme = catMd.getScheme();
            coreClass = catMd.getCoreClass();
            term = catMd.getTerm();

            if ("http://schemas.ogf.org/occi/infrastructure/compute/action#".equalsIgnoreCase(scheme)
                    && "action".equalsIgnoreCase(coreClass)) {
                resourceActions.add(term);
            }
            if ("http://sla-at-soi.eu/occi/infrastructure#".equalsIgnoreCase(scheme)
                    && "action".equalsIgnoreCase(coreClass)) {
                serviceActions.add(term);
            }

        }
        LOGGER.debug("Resource Actions" + resourceActions.toString());
        LOGGER.debug("Service Actions" + serviceActions.toString());

    }

    /**
     * Builds a provisionrequestType Given a monitoring request string a and a list of compute configurations
     * 
     * @param monitoringRequest
     * @param computeConfigurations
     * @return
     */
    public ProvisionRequestType createProvisionRequestType(String monitoringRequest,
            Set<Compute> computeConfigurations) {
        LOGGER.debug("CreateProvisionRequest  : ");
        LOGGER.debug("computeConfigurations  : " + computeConfigurations);
        // logger.debug("notificationURI  : " + notificationURI);

        ProvisionRequestType provisionRequestType = new ProvisionRequestType();
        Date startTime = new Date(System.currentTimeMillis());
        Date stopTime = startTime;
        provisionRequestType.setProvStartTime(startTime);
        provisionRequestType.setProvStopTime(stopTime);
        provisionRequestType.setMonitoringRequest(monitoringRequest);
        Set<Kind> kinds = new HashSet<Kind>();
        kinds.addAll(computeConfigurations);
        kinds.add(getDefaultServiceKind());
        provisionRequestType.setKinds(kinds);
        LOGGER.debug("CreateProvisionRequest result : " + provisionRequestType);

        return provisionRequestType;
    }

    /**
     * Create a ProvisionRequestType
     * 
     * @param resourceAvailRegistry
     * @param osID
     * @param sizeTemplateID
     * @param locTemplateID
     * @param monitoringRequest
     * @param notificationURI
     * @return
     */
    public ProvisionRequestType createProvisionRequestType(Registry<?, ?> resourceAvailRegistry, String osID,
            String sizeTemplateID, String locTemplateID, String monitoringRequest, String notificationURI) {
        LOGGER.debug("createProvisionRequestType : ");

        ProvisionRequestType provisionRequestType = new ProvisionRequestType();
        Date startTime = new Date(System.currentTimeMillis());
        Date stopTime = startTime;
        provisionRequestType.setProvStartTime(startTime);
        provisionRequestType.setProvStopTime(stopTime);

        provisionRequestType.setNotificationUri(notificationURI);

        Compute vmComputeConfiguration = createComputeConfiguration(osID, sizeTemplateID, locTemplateID,

        new Hashtable<String, String>());
        Set<Kind> kinds = new HashSet<Kind>();
        kinds.add(vmComputeConfiguration);
        kinds.add(getDefaultServiceKind());
        provisionRequestType.setKinds(kinds);

        // We add the monitoringRequest
        provisionRequestType.setMonitoringRequest(monitoringRequest);
        LOGGER.debug("createProvisionRequestType Result: " + provisionRequestType);

        return provisionRequestType;
    }
}
