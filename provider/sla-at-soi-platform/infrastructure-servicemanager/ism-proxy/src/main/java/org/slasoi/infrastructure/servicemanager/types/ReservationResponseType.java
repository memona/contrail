package org.slasoi.infrastructure.servicemanager.types;

/**
 * Representation of a future reservation of resources
 * 
 * @author sla
 * 
 */
public class ReservationResponseType {

    /**
     * The infrastructure ID associated with this @ReservationResponseType
     */
    private String infrastructureID;

    /**
     * Time stamp in milliseconds (since the beginning of time) when this @ReservationResponseType will expire
     */
    private long timeStamp;

    /**
     * Gets the time stamp in milliseconds of when this @ReservationResponseType expires
     * 
     * @return timeStamp
     */
    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     * Sets the time stamp in milliseconds of when this @ReservationResponseType expires.
     * 
     * @param timeStamp
     */
    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * Gets the infrastructure ID associated with this @ReservationResponseType
     * 
     * @return infrastructureID
     */
    public String getInfrastructureID() {
        return infrastructureID;
    }

    /**
     * Sets the infrastructure ID associated with this @ReservationResponseType
     * 
     * @param infrastructureID
     */
    public void setInfrastructureID(String infrastructureID) {
        this.infrastructureID = infrastructureID;
    }

}