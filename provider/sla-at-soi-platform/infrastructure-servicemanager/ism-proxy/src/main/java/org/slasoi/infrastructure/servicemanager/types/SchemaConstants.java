package org.slasoi.infrastructure.servicemanager.types;

public interface SchemaConstants {

     String HTTP_SLA_AT_SOI_EU_OCCI_OS_TEMPLATE = "http://sla-at-soi.eu/occi/os_templates#";
     String HTTP_SLA_AT_SOI_EU_OCCI_INFRASTRUCTURE_RES_TEMPLATE =
            "http://sla-at-soi.eu/occi/infrastructure/res_template#";
     String HTTP_SLA_AT_SOI_EU_OCCI_INFRASTRUCTURE_LOC_TEMPLATE =
            "http://sla-at-soi.eu/occi/infrastructure/location#";
     String OCCI_INFRASTRUCTURE_SCHEME = "http://schemas.ogf.org/occi/infrastructure#";
     String SLA_AT_SOI_INFRASTRUCTURE_SCHEME = "http://sla-at-soi.eu/occi/infrastructure#";
     String OCCI_COMPUTE_MEMORY = "occi.compute.memory";
     String OCCI_COMPUTE_SPEED = "occi.compute.speed";
     String OCCI_COMPUTE_CORES = "occi.compute.cores";
     String LARGE = "large";
     String MEDIUM = "medium";
     String SMALL = "small";
     String OCCI_AVAILABLE = "occi.available";
     String HTTP_SCHEMAS_OGF_ORG_OCCI_INFRASTRUCTURE_COMPUTE =
            "http://schemas.ogf.org/occi/infrastructure#compute";
     String HTTP_SLA_AT_SOI_EU_OCCI_INFRASTRUCTURE_START =
            "http://sla-at-soi.eu/occi/infrastructure#start";
     String HTTP_SCHEMAS_OGF_ORG_OCCI_INFRASTRUCTURE_START =
            "http://schemas.ogf.org/occi/infrastructure#start";

}
