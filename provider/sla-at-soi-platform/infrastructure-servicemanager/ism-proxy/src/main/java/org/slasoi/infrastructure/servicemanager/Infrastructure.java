package org.slasoi.infrastructure.servicemanager;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.jclouds.occi.v1.V1Client;
import org.slasoi.infrastructure.servicemanager.exceptions.DescriptorException;
import org.slasoi.infrastructure.servicemanager.exceptions.ProvisionException;
import org.slasoi.infrastructure.servicemanager.exceptions.StopException;
import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;
import org.slasoi.infrastructure.servicemanager.types.CapacityResponseType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionResponseType;
import org.slasoi.infrastructure.servicemanager.types.ReservationResponseType;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;

import eu.slasoi.infrastructure.model.infrastructure.Compute;

/**
 * Infrastructure Service Manger Proxy Interface.
 * <p/>
 * This API provides functionality to Obtaining Information from and OCCI complaint Cloud and The request reservation or
 * provisioning of services and resources on the cloud provider It is The gateway which allow SLA-SOI framework to
 * communicate over OCCI to A Restful Web Service {@link ExecutionException}
 * 
 * @author Patrick Cheevers
 * @see V1Client
 * 
 */
public interface Infrastructure {
    public static final String COMPUTE_KEY = "/compute/";
    public static final String SERVICE_KEY = "/service/";
    public static final String RESERVE_KEY = "/reservation/";

    /**
     * Returns a Registry of Operating systems supported by the cloud provider * @See OSRegistryImpl.
     */
    public org.slasoi.infrastructure.servicemanager.registry.Registry getOsregistry();

    /**
     * Returns a Registry of Locations supported by the cloud provider
     * 
     * @See LocRegistryImpl
     */
    public org.slasoi.infrastructure.servicemanager.registry.Registry getLocregistry();

    /**
     * Returns a Registry of Metrics supported by the cloud provider such as small medium large
     * 
     * @See MetricRegistryImpl
     */
    public org.slasoi.infrastructure.servicemanager.registry.Registry getMetricregistry();

    /**
     * Returns a Registry of all CategoryMetadata by the cloud
     */
    public org.slasoi.infrastructure.servicemanager.registry.Registry getImageregistry();

    /**
     * Returns a list Of ids of services or ressource currently exiting in the cloud provider
     * 
     * @param String
     *            path To see List Of Services pass {@code Infrastructure.SERVICE_KEY} To see List Of Resources pass
     *            {@code Infrastructure.RESOURCE_KEY}
     * 
     */
    public HashSet<String> listServicesOrResources(String path);

    /**
     * Query the service manager for resources. This interface is used for consumers of infrastructure type of resources
     * to check if the service managers have enough resources.
     * 
     * 
     * @return The amount and type resources that the service manager can provide @see CapacityResponseType
     * 
     *         Sample response would be CapacityResponse [avail=55, type=small, getCores()=1, getMemory()=1.0,
     *         getSpeed()=1.0, toString()=MetricImpl [cores=1, memory=1.0, speed=1.0], CapacityResponse [avail=27,
     *         type=medium, getCores()=2, getMemory()=2.0, getSpeed()=2.0, toString()=MetricImpl [cores=2, memory=2.0,
     *         speed=2.0], CapacityResponse [avail=11, type=large, getCores()=4, getMemory()=4.0, getSpeed()=4.0,
     *         toString()=MetricImpl [cores=4, memory=4.0, speed=4.0],
     * 
     * @throws DescriptorException
     *             An exception is thrown if error occurs
     */

    List<CapacityResponseType> queryCapacity() throws DescriptorException;

    /**
     * Reserves the specified amount of infrastructure resources specified by the @ProvisionRequest descriptor.
     * 
     * @param provisionRequest
     *            The @ProvisionRequest
     * 
     * @return the @ReservationResponseType
     * 
     * @throws DescriptorException
     *             An exception is thrown if the descriptor is invalid.
     */
    ReservationResponseType reserve(ProvisionRequestType provisionRequest) throws DescriptorException;

    /**
     * Releases resources previously reserved.
     * 
     * @param infrastructureID
     * 
     *            The infrastructure ID associated with the reservation.
     * 
     * @throws UnknownIdException
     *             An exception is thrown if there is not a previous commit associated with that provision ID.
     */
    void release(String infrastructureID) throws UnknownIdException;

    /**
     * Commits the resources previously reserved.
     * 
     * @param infrastructureID
     *            The infrastructure ID
     * 
     * @return the @ReservationResponseType
     * 
     * @throws UnknownIdException
     *             An exception is thrown if there is not a previous commit associated with that provision ID.
     */
    ReservationResponseType commit(String infrastructureID) throws UnknownIdException;

    /**
     * Provisions the resources specified. This method is used for provisioning resources without previous reservation
     * and commit.
     * 
     * @param provisioningRequestType
     *            The @ProvisionRequestType
     * 
     * @return The @ProvisionResponseType
     * 
     * @throws DescriptorException
     *             An exception is thrown if the descriptor is invalid.
     * 
     * @throws ProvisionException
     *             An exception is thrown if there was an error during the provision.
     */
    ProvisionResponseType provision(ProvisionRequestType provisioningRequestType) throws DescriptorException,
            ProvisionException;

    /**
     * Re Provisions the resources specified. This method is used for re-provision resources that are already
     * provisioned.
     * 
     * @param infrastructureID
     *            The infrastructure ID of the provision
     * 
     * @param newProvisioningRequest
     *            The new configuration of the resources specified by @ProvisionRequestType
     * 
     * @return The @ProvisionResponseType
     * 
     * @throws DescriptorException
     *             An exception is thrown if the descriptor is invalid.
     * 
     * @throws UnknownIdException
     *             An exception is thrown if there is not a previous provision associated with that infrastructure ID.
     * 
     * @throws ProvisionException
     *             An exception is thrown if there was an error during the re-provision.
     */
    ProvisionResponseType reprovision(String infrastructureID, ProvisionRequestType newProvisioningRequest)
            throws DescriptorException, UnknownIdException, ProvisionException;

    /**
     * Gets details of the running infrastructure.
     * 
     * @param infrastructureID
     *            The infrastructure ID.
     * 
     * @return The @ProvisionResponseType.
     * 
     * @throws UnknownIdException
     *             An exception is thrown if there is not a previous provision associated with the infrastructure ID.
     */
    ProvisionResponseType getDetails(String infrastructureID) throws UnknownIdException;

    /**
     * Starts the resource associated with resource ID. The resource ID can be obtained from the @ProvisionResponseType
     * 
     * @param resourceID
     *            The resource to be started
     * 
     * @throws UnknownIdException
     *             An exception is thrown if there is not resource associated with the resource ID.
     */
    void startResource(String resourceID) throws UnknownIdException;

    /**
     * Stops the resource associated with resource ID. The resource ID can be obtained from the @ProvisionResponseType
     * 
     * @param resourceID
     *            The resource to be stopped
     * 
     * @throws UnknownIdException
     *             An exception is thrown if there is not resource associated with the resource ID.
     */
    void stopResource(String resourceID) throws UnknownIdException;

    /**
     * Stops and releases all the resources associated with infrastructure ID.
     * 
     * @param infrastructureID
     *            The infrastructure to be stopped
     * 
     * @throws UnknownIdException
     *             An exception is thrown if there is not infrastructure associated with the infrastructure ID.
     * 
     * @throws StopException
     *             An exception is throw if there is an error released the resources associated with the infrastructure
     *             ID.
     * 
     */
    void stop(String infrastructureID) throws UnknownIdException, StopException;

    /**
     * Helper Method to create @Compute specifications.
     * 
     * @param imageID
     *            The image ID (e.g. VM001.img)
     * 
     * @param slaTypeID
     *            The SLA type (e.g. Gold, Silver, etc)
     * 
     * @param locationID
     *            Location
     * 
     * @param cores
     *            Number of cores
     * 
     * @param speed
     *            CPU Speed in Mhz
     * 
     * @param memory
     *            Memory size in KB
     * 
     * @param monitoringSystemConfiguration
     *            The @MonitoringSystemConfiguration
     * 
     * @param slaResourceID
     *            The SLA resource identifier
     * 
     * @param notificationURI
     *            The notification URI
     * 
     * @param categories
     *            The @Category @Set
     * 
     * @return The @Compute resource
     */
    // Prediction
    // for Querying Prediction Service and Historical Data (prediction related).
    // Domain Specific

    public String predictionQuery(String id, String metricName, int minutes) throws UnknownIdException;

    ComponentMonitoringFeatures[] getMonitoringFeatures(int sleep) throws Exception;

    /*
     * Helper Method to Create Provision Request Provision Requests are used by
     * 
     * @see Infrastructure#reserve
     * 
     * @see Infrastructure#provision
     * 
     * @see Infrastructure#reprovision
     * 
     * @param String MonitoringRequest JSon Serialised string which is passed as attribute to Restful Webserverinterface
     * of cloud Provider
     * 
     * @param Set<Compute> computeResources A Set Of Compute Resources that you would like To Provision Or Reserve
     * 
     * @return the generated type
     * 
     * @See ProvisionRequestType
     */
    public ProvisionRequestType createProvisionRequestType(String MonitoringRequest, Set<Compute> computeResources);

    /*
     * Helper Method to Create Provision Request Provision Requests are used by
     * 
     * @see Infrastructure#reserve
     * 
     * @see Infrastructure#provision
     * 
     * @see Infrastructure#reprovision
     * 
     * @param String MonitoringRequest JSon Serialised string which is passed as attribute to Restful Webserverinterface
     * of cloud Provider
     * 
     * @param String osID, Operating system Id which is a key to OsRegistry
     * 
     * @see getOsregistry() Eg Ubuntu
     * 
     * @param String sizeTemplateID, * Size Template Id which is a key to Metric Registry
     * 
     * @see Infrastructure#getMetricregistry() Eg small
     * 
     * @param String locTemplateID, * Location Id which is a key to location Registry
     * 
     * @see Infrastructure#getLocregistry() Eg IE
     * 
     * @return the generated type
     * 
     * @See ProvisionRequestType
     */
    public ProvisionRequestType createProvisionRequestType(

    String osID, String sizeTemplateID, String locTemplateID, String monitoringRequest, String notificationURI);

    /*
     * Helper Method to Create Compute Type which will be used in a Provison Request Requests are used by
     * 
     * @See ProvisionRequestType
     * 
     * @see Infrastructure#reserve
     * 
     * @see Infrastructure#provision
     * 
     * @see Infrastructure#reprovision
     * 
     * 
     * @param String osID, Operating system Id which is a key to OsRegistry
     * 
     * @see getOsregistry() Eg Ubuntu
     * 
     * @param String sizeTemplateID, * Size Template Id which is a key to Metric Registry
     * 
     * @see Infrastructure#getMetricregistry() Eg small
     * 
     * @param String locTemplateID, * Location Id which is a key to location Registry
     * 
     * @see Infrastructure#getLocregistry() Eg IE
     * 
     * extras Hashtable<String, String> extras) Extra information to add
     * 
     * @return the generated type
     * 
     * @See Compute
     */
    public Compute createComputeConfiguration(String osID, String sizeTemplateID, String locTemplateID,
            Hashtable<String, String> extras);

    /*
     * This method Is a helper method to create a compute Configuration It may be @deprecated very soon and one should
     * use the alternate createComputeConfiguration
     */
    public Compute createComputeConfiguration(int cores, float speed, int memory, String osID,
            String locTemplateID, Hashtable<String, String> extras);

}
