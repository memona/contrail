package org.slasoi.infrastructure.servicemanager.exceptions;

/**
 * Exception thrown when there is a problem with resources.
 * 
 * @author sla
 * 
 */
public class ResourceNotAvailableException extends Exception {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -7832945944239655667L;

}
