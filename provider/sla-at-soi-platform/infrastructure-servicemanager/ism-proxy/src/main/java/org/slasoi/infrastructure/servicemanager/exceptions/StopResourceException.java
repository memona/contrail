package org.slasoi.infrastructure.servicemanager.exceptions;

/**
 * Exception thrown when there is a problem with stopping a resource.
 * 
 * @author sla
 * 
 */
public class StopResourceException extends Exception {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 698591683751806988L;

}
