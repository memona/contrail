package org.slasoi.infrastructure.servicemanager.occi.types;

/**
 * Constants used by the Proxy.
 * 
 * @author Patrick Cheevers
 * 
 */
public interface Terms {

     String OCCI_CORE_TITLE = "occi.core.title";
     String OCCI_CORE_SUMMARY = "occi.core.summary";
     String OCCI_COMPUTE_HOSTNAME = "occi.compute.hostname";
     String OCCI_COMPUTE_SPEED = "occi.compute.speed";
     String OCCI_COMPUTE_CORES = "occi.compute.cores";
     String OCCI_COMPUTE_MEMORY = "occi.compute.memory";
     String OCCI_COMPUTE_ARCHITECTURE = "occi.compute.architecture";
     String OCCI_COMPUTE_STATUS = "occi.compute.status";

     String PROVISION_ID = "eu.slasoi.infrastructure.provisionid";

    // #
     String NOTIFICATION_URI = "eu.slasoi.task.notificationUri";

     String MONITORING_REQUEST = "eu.slasoi.infrastructure.monitoringrequest";
     String MONITORING_NOT_CONFIGURED = "MONITORING_NOT_CONFIGURED";
     String RESOURCE_ID = "eu.slasoi.infrastructure.resourceid";

     String COMPUTE = "compute";
     String SERVICE = "service";
     String OCCI_KIND = "kind";
     String GOLD = "gold";

    // New Terms
     String IRELAND = "ie";
     String IMAGE_UBUNTU_9_10 = "ubuntu-9.10";
     String UBUNTU_9_10_32 = "ubuntu-9.10-32";
     String UBUNTU_9_10_64 = "ubuntu-9.10-64";
     String WINDOWS_2003_32 = "windows-server-2003-32";
     String WINDOWS_2003_64 = "windows-server-2003-64";

}
