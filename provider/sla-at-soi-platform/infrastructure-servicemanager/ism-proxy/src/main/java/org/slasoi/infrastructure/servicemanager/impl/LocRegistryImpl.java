package org.slasoi.infrastructure.servicemanager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jclouds.entities.domain.IGrpResourceAvailMetadata;
import org.jclouds.entities.domain.IPageSet;
import org.jclouds.entities.io.ICategoryMetadata;
import org.slasoi.infrastructure.servicemanager.types.SchemaConstants;

import eu.slasoi.infrastructure.model.Category;

/**
 * Prototype Implementation of a OS Registry.
 * 
 * @author Patrick Cheevers
 * 
 */
public final class LocRegistryImpl extends Reg {
    /**
     * logger. 
     */
    private static final Logger LOGGER = Logger.getLogger(LocRegistryImpl.class.getName());

    // 'ie':[
    // scheme:'http://sla-at-soi.eu/occi/infrastructure/location#',
    // rel:[parent:"", kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
    // title:'Ireland Geo Location Template',
    // location:'/location/ie/',
    // attributes:[]

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.infrastructure.servicemanager.registry.Registry#setup(org.jclouds.entities.domain.IPageSet)
     */
    public void setup(IPageSet<? extends IGrpResourceAvailMetadata> resources) {

        String rel = null;
        String scheme = null;
        String coreClass = null;
        String term = null;
        Category cat = null;
        List<Category> newlist = new ArrayList<Category>();
        for (IGrpResourceAvailMetadata resourceMd : resources) {
            ICategoryMetadata md = resourceMd.getCategoryMetaData();
            rel = md.getRel();
            scheme = md.getScheme();
            coreClass = md.getCoreClass();
            term = md.getTerm();

            if (SchemaConstants.HTTP_SLA_AT_SOI_EU_OCCI_INFRASTRUCTURE_LOC_TEMPLATE.equalsIgnoreCase(scheme)) {
                cat = new Category();
                cat.setCoreClass(coreClass);
                cat.setScheme(scheme);
                cat.setTerm(term);
                cat.setTitle(md.getTitle());
                newlist.add(cat);
            }

        }
        LOGGER.debug("Location Templates" + newlist.toString());

        this.setList(newlist);

    }

}
