package org.slasoi.infrastructure.servicemanager.prediction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

/**
 * Prediction query.
 * @author Victor Molino
 *
 */
public class PredictionQueryImpl implements PredictionQuery {

    private static final Logger LOGGER = Logger.getLogger(PredictionQueryImpl.class.getName());
    private String hostname;
    private int port;
    private String resource;

    public int getPort() {
        return port;
    }

    public void setPort(final int port) {
        this.port = port;
    }

    public String query(final String id, final String metric, final int minutes) {
        if (id.equals("network") && metric.equals("network") && minutes == 0) {
            // It is a network query
            LOGGER.info("Network query");
            String endPoint = "http://" + hostname + ":" + port + "/network";
            return this.get(endPoint);
        } else {
            LOGGER.info("Instrumentation query");
            String endPoint =
                    "http://" + hostname + ":" + port + resource + "?hostName=" + id + "&metricName=" + metric
                            + "&minutes=" + Integer.toString(minutes);
            return this.get(endPoint);
        }
    }

    private String get(final String endPoint) {
        HttpClient httpClient = new DefaultHttpClient();
        httpClient.getParams().setParameter("http.useragent", "sla@soi Prediction Query Client");
        HttpGet httpGet = new HttpGet(endPoint);
        LOGGER.info("Executing request - " + httpGet.getRequestLine());
        HttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
        } catch (ClientProtocolException e) {
            LOGGER.error(e);
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            LOGGER.error(e);
            e.printStackTrace();
            return null;
        }
        StringBuilder sb = new StringBuilder();
        if (response != null) {
            HttpEntity entity = response.getEntity();

            LOGGER.info("----------------------------------------");
            LOGGER.info("Status - " + response.getStatusLine());
            LOGGER.info("----------------------------------------");
            if (entity != null) {
                LOGGER.info("Response content length: " + entity.getContentLength());
            }
            InputStream is = null;
            try {
                is = entity.getContent();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                LOGGER.error(e);
                return null;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                LOGGER.error(e);
                return null;
            }
            if (is != null) {

                String line;

                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                } catch (IOException e) {
                    LOGGER.error(e);
                    e.printStackTrace();
                    return null;
                } finally {
                    try {
                        is.close();
                    } catch (IOException e) {
                        LOGGER.error(e);
                        e.printStackTrace();
                        return null;
                    }
                }
                httpClient.getConnectionManager().shutdown();
                return sb.toString();
            } else {
                httpClient.getConnectionManager().shutdown();
                return null;
            }
        } else {
            return null;
        }
    }

    public String networkQuery() {
        return this.get("http://" + hostname + ":" + port + "/network");

    }

    public void setResource(final String resource) {
        this.resource = resource;
    }

    public String getResource() {
        return resource;
    }

    public void setHostname(final String hostname) {
        this.hostname = hostname;
    }

    public String getHostname() {
        return hostname;
    }

}
