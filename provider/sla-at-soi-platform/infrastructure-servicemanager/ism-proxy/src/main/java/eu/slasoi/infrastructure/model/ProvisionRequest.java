package eu.slasoi.infrastructure.model;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Set;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * ProvisionRequest representation. This object is a domain specific representation of a @ProvisionRequest. The object
 * is designed to facilitate the representation and translation of SLA functional terms to their equivalent OCCI
 * representation.
 * 
 * @author sla
 */
public class ProvisionRequest {

    /**
     * The id of this object.
     */
    private long id;

    /**
     * The @Category @Set associated with this @ProvisionRequest.
     */
    @Deprecated
    private Set<Category> categories;

    /**
     * The name of the service.
     */
    private String name;

    /**
     * The SLA of the service SLA.
     */
    private String sla;
    /**
     * The description of this service
     */
    private String description;
    /**
     * The @Set of @Kind associated with this @ProvisionRequest.
     * 
     * Only Compute supported on this implementation.
     */
    // TODO: Add Generic Type
    private Set kinds;

    /**
     * The notification address where once the ProvisionRequest is completed (successfully or unsuccessfully). Used for
     * monitoring
     */
    private String notificationUri;

    /**
     * The provision ID of this @ProvisionRequest.
     */
    private String provId;

    /**
     * The @Date when this @ProvisionRequest should start.
     */
    private java.util.Date provStartTime;

    /**
     * 
     * The @Date when this @ProvisionRequest should stop.
     */
    private java.util.Date provStopTime;

    /**
     * Each provision request must specified a monitoring configuration. This field contains a domain serialised
     * monitoring configuration.
     */
    private String monitoringRequest;

    /**
     * Version of this @ProvisionRequest.
     */
    private long version;

    /**
     * Returns the monitoring request.
     * 
     * @return The monitoring request.
     */
    public String getMonitoringRequest() {
        return monitoringRequest;
    }

    /**
     * Sets the monitoring request associated with this @ProvisionRequest.
     * 
     * @param monitoringRequest
     * 
     *            the monitoring request domain specific representation
     */
    public void setMonitoringRequest(final String monitoringRequest) {
        this.monitoringRequest = monitoringRequest;
    }

    /**
     * Returns the @Set of @Category associated with this @ProvisionRequest.
     * 
     * @return A @Set of @Category
     */
    @Deprecated
    public Set<Category> getCategories() {
        return categories;
    }

    @Deprecated
    /**
     * Sets the @Category @Set associated with this @ProvisionRequest.
     * 
     * @param categories
     * The @Category @Set
     */
    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    /**
     * Gets the id associated with this @ProvisionRequest.
     * 
     * @return The id
     */
    public long getId() {
        return id;
    }

    /**
     * Gets the id associated with this @ProvisionRequest
     * 
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets the @Kind @Set associated with this @ProvisionRequest.
     * 
     * @return The @Set of @Kind
     */
    // TODO. Fix generic type
    public Set getKinds() {
        return kinds;
    }

    /**
     * Sets the @Kind @Set associated with this @ProvisionRequest.
     * 
     * @param kinds
     *            The @Set of @Kind
     */
    // TODO: Fix generic type
    public void setKinds(Set kinds) {
        this.kinds = kinds;
    }

    /**
     * Gets the notification address associated with this @ProvisionRequest.
     * 
     * @return notificationUri
     */
    public String getNotificationUri() {
        return notificationUri;
    }

    /**
     * Sets the notification address associated with this @ProvisionRequest
     * 
     * @param notificationUri
     *            The notification address.
     */
    public void setNotificationUri(final String notificationUri) {
        this.notificationUri = notificationUri;
    }

    /**
     * Gets the provision ID associated with this @ProvisionRequest.
     * 
     * @return provId The provision ID.
     */
    public String getProvId() {
        return provId;
    }

    /**
     * Sets the provision ID associated with this @ProvisionRequest.
     * 
     * @param provId
     *            The provision Id.
     */
    public void setProvId(final String provId) {
        this.provId = provId;
    }

    /**
     * Gets the @ProvisionRequest start @Date.
     * 
     * @return The @ProvisionRequest start @Date.
     */
    public Date getProvStartTime() {
        return provStartTime;
    }

    /**
     * Sets the @ProvisionRequest start @Date.
     * 
     * @param provStartTime
     *            The @ProvisionRequest start @Date.
     */
    public void setProvStartTime(final Date provStartTime) {
        this.provStartTime = provStartTime;
    }

    /**
     * Gets the @ProvisionRequest stop @Date.
     * 
     * @return The @ProvisionRequest stop @Date.
     */
    public Date getProvStopTime() {
        return provStopTime;
    }

    /**
     * Sets the @ProvisionRequest stop @Date
     * 
     * @param provStopTime
     *            the @ProvisionRequest stop @Date
     */
    public void setProvStopTime(final Date provStopTime) {
        this.provStopTime = provStopTime;
    }

    /**
     * Gets the version associated with this @ProvisionRequest.
     * 
     * @return The version
     */
    public long getVersion() {
        return version;
    }

    /**
     * Sets the version associated with this @ProvisionRequest.
     * 
     * @param version
     *            The version
     */
    public void setVersion(final long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "ProvisionRequest [categories=" + categories + ", description=" + description + ", id=" + id
                + ", kinds=" + kinds + ", monitoringRequest=" + monitoringRequest + ", name=" + name
                + ", notificationUri=" + notificationUri + ", provId=" + provId + ", provStartTime="
                + provStartTime + ", provStopTime=" + provStopTime + ", sla=" + sla + ", version=" + version + "]";
    }

    /**
     * Serialises this @ProvisionRequest object and saves it to the absolute path specified.
     * 
     * @param path
     *            The absolute path, including the file name.
     * @throws IOException
     */
    public void toFile(final String path) throws IOException {
        String s = toString();
        BufferedWriter out = new BufferedWriter(new FileWriter(path));
        out.write(s);
        out.flush();
        out.close();
    }

    /**
     * Returns a @ProvisionRequest object, that has been serialised to the file system.
     * 
     * @param The
     *            absolute path to the serialised object
     * 
     * @return The @ProvisionRequest
     * 
     * @throws FileNotFoundException
     */
    public static ProvisionRequest fromFile(final String path) throws FileNotFoundException {
        // We try to load
        InputStream is = null;
        is = new BufferedInputStream(new FileInputStream(path));
        XStream xStream = new XStream(new DomDriver());
        return (ProvisionRequest) xStream.fromXML(is);

    }

    /**
     * @param description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSla(final String sla) {
        this.sla = sla;
    }

    public String getSla() {
        return sla;
    }
}
