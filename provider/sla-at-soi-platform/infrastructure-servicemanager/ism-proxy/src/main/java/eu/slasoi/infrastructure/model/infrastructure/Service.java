package eu.slasoi.infrastructure.model.infrastructure;

import java.util.UUID;

import eu.slasoi.infrastructure.model.Kind;

/**
 * Indicates That this Kind is an instance of Service.
 * 
 * @author Patrick Cheevers
 * 
 */
public class Service extends Kind {
    /**
     * Constructor.
     */
    public Service() {
        super();

    }

    private UUID serviceId = UUID.randomUUID();

    /**
     * @return the serviceId
     */
    public UUID getServiceId() {
        return serviceId;
    }

    /**
     * @param serviceId
     *            the serviceId to set
     */
    public void setServiceId(final UUID serviceId) {
        this.serviceId = serviceId;
    }

}
