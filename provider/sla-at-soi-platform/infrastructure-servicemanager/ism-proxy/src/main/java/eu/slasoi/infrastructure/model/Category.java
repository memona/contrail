package eu.slasoi.infrastructure.model;

/**
 * Category class represents the type of information required by OCCI When a Provision request is translated into a
 * restful web service call. It will take the format of several categories , typically a a service category and one or
 * more compute categories . Categories may represent predefined type specified by the cloud provider such as small
 * ,medium large ,Ubuntu ,windows , compute service etc.
 * 
 * @author Patrick Cheevers
 */
public class Category {
    private long id;
    private String scheme;
    private String term;
    private String title;
    private long version;
    private String coreClass;

    /**
     * @return the coreClass
     */
    public String getCoreClass() {
        return coreClass;
    }

    /**
     * @param coreClass
     *            the coreClass to set
     */
    public void setCoreClass(final String coreClass) {
        this.coreClass = coreClass;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((coreClass == null) ? 0 : coreClass.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((scheme == null) ? 0 : scheme.hashCode());
        result = prime * result + ((term == null) ? 0 : term.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + (int) (version ^ (version >>> 32));
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Category))
            return false;
        Category other = (Category) obj;
        if (coreClass == null) {
            if (other.coreClass != null)
                return false;
        } else if (!coreClass.equals(other.coreClass))
            return false;
        if (id != other.id)
            return false;
        if (scheme == null) {
            if (other.scheme != null)
                return false;
        } else if (!scheme.equals(other.scheme))
            return false;
        if (term == null) {
            if (other.term != null)
                return false;
        } else if (!term.equals(other.term))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        if (version != other.version)
            return false;
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(final String scheme) {
        this.scheme = scheme;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(final String term) {
        this.term = term;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(final long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Category [coreClass=" + coreClass + ", id=" + id + ", scheme=" + scheme + ", term=" + term
                + ", title=" + title + ", version=" + version + "]";
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
