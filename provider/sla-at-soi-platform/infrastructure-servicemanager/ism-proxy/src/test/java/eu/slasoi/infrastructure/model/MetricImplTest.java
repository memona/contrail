package eu.slasoi.infrastructure.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test The MetricImpl Type Class.
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class MetricImplTest {

    private static final int CORES = 2;

    private MetricImpl metricImpl2;
    private MetricImpl metricImpl1;

    @Before
    public void initialize() {
        // float memory, int cores, float speed
        metricImpl1 = new MetricImpl(Float.MAX_VALUE, CORES, Float.MIN_VALUE);
        metricImpl2 = new MetricImpl(Float.MAX_VALUE, CORES, Float.MIN_VALUE);

    }

    @Test
    public void computeSetGettest() {
        assertEquals(metricImpl1.getCores(), CORES);
        assertTrue(Float.MAX_VALUE == metricImpl1.getMemory());
        assertTrue(Float.MIN_VALUE == metricImpl1.getSpeed());
        assertEquals(metricImpl1, metricImpl2);
        assertTrue(metricImpl1.equals(metricImpl2));
        assertEquals(metricImpl1.toString(), metricImpl2.toString());

        metricImpl2.setMemory(Integer.MAX_VALUE);
        metricImpl2.setCores(Integer.MAX_VALUE);
        metricImpl2.setSpeed(Integer.MAX_VALUE);

        assertTrue(metricImpl2.getMemory() == Integer.MAX_VALUE);
        assertTrue(metricImpl2.getCores() == Integer.MAX_VALUE);
        assertTrue(metricImpl2.getSpeed() == Integer.MAX_VALUE);

    }
}
