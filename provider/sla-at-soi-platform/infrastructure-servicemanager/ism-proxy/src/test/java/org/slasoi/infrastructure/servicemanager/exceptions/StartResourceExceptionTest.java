package org.slasoi.infrastructure.servicemanager.exceptions;

import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Exception thrown when there is a problem with starting up a resource.
 * 
 * @author Patrick Cheevers
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class StartResourceExceptionTest {
    private static final Logger LOGGER = Logger.getLogger(StartResourceExceptionTest.class.getName());

    @Test
    public void testthrow() {

        try {
            throw new StartResourceException();
        } catch (Exception e) {
            if (!(e instanceof StartResourceException))

            {
                LOGGER.error("Exception is not Instance of StartResourceException");
                fail();
            }

        }

    }

}
