package org.slasoi.infrastructure.servicemanager;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/slasoi/infrastructure/servicemanager/context-TestInfrastructureMockup.xml" })
public class PredictionTest {

    private static final Logger LOGGER = Logger.getLogger(PredictionTest.class.getName());

    @Autowired
    public org.slasoi.infrastructure.servicemanager.Infrastructure infrastructure;

    private static String ident = "e0cb4eb02f47";
    private static String metricName = "cpu";
    private static int minutes = 2;

    @Ignore
    @Test
    public void testPredictionQuery() {
        assertNotNull("infrastructure is Null", infrastructure);

        String result = null;
        try {
            result = infrastructure.predictionQuery(ident, metricName, minutes);
            LOGGER.info(result);
        } catch (UnknownIdException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            LOGGER.error(e);
            fail();
        }
        assertNotNull("result is  null", result);

    }

}
