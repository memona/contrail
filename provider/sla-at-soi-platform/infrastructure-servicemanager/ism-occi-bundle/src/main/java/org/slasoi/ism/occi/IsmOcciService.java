package org.slasoi.ism.occi;

import org.slasoi.infrastructure.servicemanager.Infrastructure;

/**
 * OSGI Bundle Interface that wraps the ISM Proxy.
 * @author Victor
 *
 */
public interface IsmOcciService extends Infrastructure {

}
