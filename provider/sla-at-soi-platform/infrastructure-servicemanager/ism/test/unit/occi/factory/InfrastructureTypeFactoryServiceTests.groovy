/*
SVN FILE: $Id: InfrastructureTypeFactoryServiceTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/factory/InfrastructureTypeFactoryServiceTests.groovy $
*/

package occi.factory

import eu.slasoi.infrastructure.model.Service
import grails.test.GrailsUnitTestCase
import occi.infrastructure.mixins.IPNetworkInterfaceMixin
import occi.infrastructure.mixins.IPNetworkMixin
import occi.infrastructure.mixins.OsTemplateMixin
import occi.infrastructure.mixins.ResourceTemplateMixin
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import occi.core.*
import occi.infrastructure.*

class InfrastructureTypeFactoryServiceTests extends GrailsUnitTestCase {

    InfrastructureTypeFactoryService typeFactory
    
    protected void setUp() {

        super.setUp()
        mockDomain Kind
        mockDomain AttributeType
        mockDomain AttributeValue
        mockDomain Compute
        mockDomain Network
        mockDomain Storage
        mockDomain Action
        mockDomain occi.core.Category
        mockDomain Kind
        mockDomain occi.core.Mixin
        mockLogging InfrastructureTypeFactoryService

        def config = new ConfigObject()
        config.grails.serverURL = 'http://localhost:8080'

        config.eu.slasoi.infrastructure.resources = [
            enabled:[
                'compute':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Compute Type',
                    location:'/compute/',
                    attributes:[
                        'occi.compute.architecture':[default:'x86', required:false, mutable:true, type:'string', render:true],
                        'occi.compute.cores':[default:'1', required:false, mutable:true, type:'int', render:true],
                        'occi.compute.memory':[default:'0.5', required:false, mutable:true, type:'float', render:true],
                        'occi.compute.speed':[default:'2.4', required:false, mutable:true, type:'float', render:true],
                        'occi.compute.hostname':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.compute.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'start':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Start Compute Action'
                        ],
                        'stop':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Stop Compute Action',
                            attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
                        ],
                        'restart':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Restart Compute Action',
                            attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
                        ],
                        'suspend':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/compute/action#', title:'Suspend Compute Action',
                            attributes:['method':[default:'', required:false, mutable:true, type:'enum', render:true]]
                        ]
                    ]
                ],
                'service':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Service Type',
                    location:'/service/',
                    attributes:[
                        'eu.slasoi.infrastructure.service.name':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.service.monitoring.config':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.service.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'start':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Start Service Action'],
                        'stop':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Stop Service Action'],
                        'restart':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Restart Service Action'],
                        'suspend':[scheme:'http://sla-at-soi.eu/occi/infrastructure#', title:'Suspend Service Action']
                    ]
                ],
                'reservation':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Reservation Type',
                    location:'/reservation/',
                    attributes:[
                        //'eu.slasoi.infrastructure.reservation.state':[default:'active', required:false, mutable:false, type:'enum', render:true],
                        'eu.slasoi.infrastructure.reservation.leasePeriod':[default:'60', required:false, mutable:true, type:'int', render:true],
                        'eu.slasoi.infrastructure.reservation.created':[default:'', required:false, mutable:true, type:'int', render:true]
                    ],
                    actions:[
                        'commit':[scheme:'http://sla-at-soi.eu/occi/infrastructure/reservation#', title:'Commit Reservation Action']
                    ]
                ]
            ],
            disabled:[
                'network':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Network type',
                    location:'/network/',
                    attributes:[
                        'occi.network.vlan':[default:'0', required:false, mutable:true, type:'int', render:true],
                        'occi.network.label':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.network.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'up':[scheme:'http://schemas.ogf.org/occi/infrastructure/network/action#', title:'Up Network Action'],
                        'down':[scheme:'http://schemas.ogf.org/occi/infrastructure/network/action#', title:'Down Network Action']
                    ]
                ],
                'storage':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#resource', kind:''],
                    title:'Storage type',
                    location:'/storage/',
                    attributes:[
                        'occi.storage.size':[default:'10.0', required:false, mutable:true, type:'float', render:true],
                        'occi.storage.state':[default:'offline', required:false, mutable:false, type:'enum', render:true]
                    ],
                    actions:[
                        'online':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Online Storage Action'],
                        'offline':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Offline Storage Action'],
                        'backup':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Backup Storage Action'],
                        'snapshot':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Snapshot Storage Action'],
                        'resize':[
                            scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Resize Storage Action',
                            attributes:['size':[default:'', required:false, mutable:true, type:'float', render:true]]
                        ],
                        'degrade':[scheme:'http://schemas.ogf.org/occi/infrastructure/storage/action#', title:'Degrade Storage Action']
                    ]
                ],
            ]
        ]

        config.eu.slasoi.infrastructure.links = [
            enabled:[
                'storagelink':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#link', kind:''],
                    title:'Storage Link',
                    location:'/storage/link/',
                    attributes:[
                        'occi.storagelink.deviceid':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.storagelink.mountpoint':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.storagelink.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ]
                ],
                'networkinterface':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'http://schemas.ogf.org/occi/core#link', kind:''],
                    title:'Network Interface Link',
                    location:'/network/link/',
                    attributes:[
                        'occi.networkinterface.interface':[default:'eth0', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.mac':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.state':[default:'inactive', required:false, mutable:false, type:'enum', render:true]
                    ]
                ]
            ],
            disabled:[]
        ]

        config.eu.slasoi.infrastructure.mixins = [
            enabled:[
                'os_tpl':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'', kind:''],
                    title:'OS Template Mixin',
                    location:'',
                    attributes:[]
                ],
                'resource_tpl':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure#',
                    rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Resource Template Mixin',
                    location:'/template/',
                    attributes:[]
                ],
                'small':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/template#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Small Compute Resource Template',
                    location:'/template/small/',
                    attributes:[
                        'occi.compute.small.cores':[default:'1', required:false, mutable:false, type:'string', render:true],
                        'occi.compute.small.speed':[default:'1', required:false, mutable:false, type:'string', render:true],
                        'occi.compute.small.memory':[default:'1', required:false, mutable:false, type:'string', render:true],
                        'occi.small.available':[default:'60', required:false, mutable:false, type:'int', render:true]
                    ]
                ],
                'medium':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/template#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Medium Compute Resource Template',
                    location:'/template/medium/',
                    attributes:[
                        'occi.compute.medium.cores':[default:'2', required:false, mutable:false, type:'string', render:true],
                        'occi.compute.medium.speed':[default:'2', required:false, mutable:false, type:'string', render:true],
                        'occi.compute.medium.memory':[default:'2', required:false, mutable:false, type:'string', render:true],
                        'occi.medium.available':[default:'30', required:false, mutable:false, type:'int', render:true]
                    ]
                ],
                'large':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/template#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#resource_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Large Compute Resource Template',
                    location:'/template/large/',
                    attributes:[
                        'occi.compute.large.cores':[default:'4', required:false, mutable:false, type:'string', render:true],
                        'occi.compute.large.speed':[default:'4', required:false, mutable:false, type:'string', render:true],
                        'occi.compute.large.memory':[default:'4', required:false, mutable:false, type:'string', render:true],
                        'occi.large.available':[default:'15', required:false, mutable:false, type:'int', render:true]
                    ]
                ]
            ],
            disabled:[
                'ipnetwork':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure/network#',
                    rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#network'],
                    title:'IP Network Mixin',
                    location:'/ipnetwork/',
                    attributes:[
                        'occi.network.address':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.network.gateway':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.network.allocation':[default:'dynamic', required:false, mutable:true, type:'enum', render:true]
                    ]
                ],
                'ipnetworkinterface':[
                    scheme:'http://schemas.ogf.org/occi/infrastructure/networkinterface#',
                    rel:[parent:'', kind:'http://schemas.ogf.org/occi/infrastructure#networkinterface'],
                    title:'IP Network Interface Mixin',
                    location:'/ipnetwork/ipnetworkinterface/',
                    attributes:[
                        'occi.networkinterface.address':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.gateway':[default:'', required:false, mutable:true, type:'string', render:true],
                        'occi.networkinterface.allocation':[default:'dynamic', required:false, mutable:true, type:'string', render:true]
                    ]
                ],
                'metric':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure#',
                    rel:[parent:'', kind:''],
                    title:'Metric Mixin',
                    location:'',
                    attributes:[
                        'eu.slasoi.infrastructure.metric.timestamp':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.samplerate':[default:'', required:false, mutable:true, type:'int', render:true],
                        'eu.slasoi.infrastructure.metric.resolution':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.observing':[default:'', required:false, mutable:true, type:'string', render:true]
                    ]
                ],
                'user':[
                    scheme:'http://sla-at-soi.eu/occi/infrastructure/metric/compute/cpu#',
                    rel:[parent:'http://sla-at-soi.eu/occi/infrastructure#metric', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Metric Mixin',
                    location:'/metric/compute/cpu/user/',
                    attributes:[
                        'eu.slasoi.infrastructure.metric.timestamp':[default:'', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.samplerate':[default:'60', required:false, mutable:true, type:'int', render:true],
                        'eu.slasoi.infrastructure.metric.resolution':[default:'M', required:false, mutable:true, type:'string', render:true],
                        'eu.slasoi.infrastructure.metric.observing':[default:'occi.compute.cpu', required:false, mutable:true, type:'string', render:true]
                    ]
                ]
            ]
        ]

        config.eu.slasoi.infrastructure.templates = [
            enabled:[
                'ubuntu_9-10':[
                    scheme:'http://sla-at-soi.eu/occi/templates#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#os_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Base Ubuntu 9.10 LTS operating system',
                    location:'/os_tpl/ubuntu_9-10/',
                    attributes:[
                        'eu.slasoi.image.persistent':[default:'false', required:false, mutable:true, type:'boolean', render:true]
                    ]
                ],
                'ubuntu_10-11':[
                    scheme:'http://sla-at-soi.eu/occi/templates#',
                    rel:[parent:'http://schemas.ogf.org/occi/infrastructure#os_tpl', kind:'http://schemas.ogf.org/occi/infrastructure#compute'],
                    title:'Base Ubuntu 10.11 LTS operating system',
                    location:'/os_tpl/ubuntu_10-11/',
                    attributes:[
                        'eu.slasoi.image.persistent':[default:'false', required:false, mutable:true, type:'boolean', render:true]
                    ]
                ]
            ],
            disabled:[]
        ]

        ConfigurationHolder.config = config

        typeFactory = new InfrastructureTypeFactoryService()
        typeFactory.initDefaults()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testInitDefaults(){
        
        def counter = 0;
        assertEquals 21, (Mixin.list().size() + Kind.list().size())

        //Core types
        def k = Kind.findAllByTermAndScheme(Entity.term, Entity.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++
        k = Kind.findAllByTermAndScheme(Resource.term, Resource.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++
        k = Kind.findAllByTermAndScheme(Link.term, Link.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++

        //Infrastructure types
        k = Kind.findAllByTermAndScheme(Compute.term, Compute.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++
        k = Kind.findAllByTermAndScheme(Storage.term, Storage.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++
        k = Kind.findAllByTermAndScheme(Network.term, Network.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++

        //SLA@SOI types
        k = Kind.findAllByTermAndScheme(Service.term, Service.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++

        //Infrastructure Links
        k = Kind.findAllByTermAndScheme(NetworkLink.term, NetworkLink.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++
        k = Kind.findAllByTermAndScheme(StorageLink.term, StorageLink.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++

        //Infrastructure Mixins
        k = Mixin.findAllByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++
        k = Mixin.findAllByTermAndScheme(IPNetworkInterfaceMixin.term, IPNetworkInterfaceMixin.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++

        //Template Mixins
        k = Mixin.findAllByTermAndScheme(OsTemplateMixin.term, OsTemplateMixin.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++
        k = Mixin.findAllByTermAndScheme(ResourceTemplateMixin.term, ResourceTemplateMixin.scheme)
        assertNotNull k
        assertEquals 1, k.size()
        counter ++

        assertEquals 21, (Mixin.list().size() + Kind.list().size())
    }

    void testComputeKindConstruction(){

        //typeFactory.createComputeKind()

        Kind compute = Kind.findByTermAndScheme(Compute.term, Compute.scheme)

        assertNotNull compute
        assertTrue compute.related.size() == 1
        assertTrue compute.attributes.size() == 6
        assertTrue compute.actions.size() == 4

        assertNotNull compute.toTypeString()

    }

    void testNetworkKindConstruction(){

        //typeFactory.createNetworkKind()

        Kind network = Kind.findByTermAndScheme(Network.term, Network.scheme)

        assertNotNull network
        assertTrue network.related.size() == 1
        assertTrue network.attributes.size() == 3
        assertTrue network.actions.size() == 2

        assertNotNull network.toTypeString()
    }

    void testIPNetworkMixin(){

        //typeFactory.addIPNetworkMixin(CH.config.eu.slasoi.infrastructure.mixins.ipnetwork)

        Mixin ipnetwork = Mixin.findByTermAndScheme(IPNetworkMixin.term, IPNetworkMixin.scheme)
        assertNotNull ipnetwork

        assertEquals Network.term, ipnetwork.applicableKind.term
        assertEquals Network.scheme, ipnetwork.applicableKind.scheme
    }

    void testMixedInNetworkInterface(){

        //typeFactory.addIPNetworkLinkMixin(CH.config.eu.slasoi.infrastructure.mixins.ipnetworklink)

        Mixin ipIfaceNetwork = Mixin.findByTermAndScheme(IPNetworkInterfaceMixin.term, IPNetworkInterfaceMixin.scheme)
        assertNotNull ipIfaceNetwork

        assertEquals NetworkLink.term, ipIfaceNetwork.applicableKind.term
        assertEquals NetworkLink.scheme, ipIfaceNetwork.applicableKind.scheme
    }

    void testStorageKindConstruction(){

        //typeFactory.createStorageKind()

        Kind storage = Kind.findByTermAndScheme(Storage.term, Storage.scheme)

        assertNotNull storage
        assertTrue storage.related.size() == 1
        assertTrue storage.attributes.size() == 2
        assertTrue storage.actions.size() == 6

        assertNotNull storage.toTypeString()
    }

    void testServiceKindConstruction(){
        //typeFactory.createServiceKind()

        Kind service = Kind.findByTermAndScheme(Service.term, Service.scheme)

        assertNotNull service
        assertTrue service.related.size() == 1
        assertTrue service.attributes.size() == 3
        assertTrue service.actions.size() == 4

        assertNotNull service.toTypeString()
    }
}
