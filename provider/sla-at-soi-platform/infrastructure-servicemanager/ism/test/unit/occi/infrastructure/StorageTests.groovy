package occi.infrastructure

import grails.test.GrailsUnitTestCase
import occi.core.Action
import occi.core.AttributeType
import occi.core.Category
import occi.core.Kind
import occi.infrastructure.actions.storage.OnlineStorageAction
import occi.infrastructure.actions.storage.OfflineStorageAction
import occi.infrastructure.actions.storage.BackupStorageAction
import occi.infrastructure.actions.storage.SnapshotStorageAction
import occi.infrastructure.actions.storage.ResizeStorageAction

//TODO implement
class StorageTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstraints() {

        assertEquals 'storage', Storage.term
    }

    private def addKindAttributes(Kind storageKind) {

        def attr

        attr = new AttributeType(name: Storage.occi_storage_size, type: AttributeType.FLOAT,
                defaultValueExpr: "0.0").save()
        storageKind.addToAttributes attr
        attr = new AttributeType(name: Storage.occi_storage_state, type: AttributeType.STRING,
                defaultValueExpr: "${Storage.occi_storage_state_offline}").save()
        storageKind.addToAttributes attr
    }

    private def addKindActions(Kind storageKind){

        def onlineActionCat = new Category(term: OnlineStorageAction.term, scheme: OnlineStorageAction.scheme, title: 'Online Storage Action', location: '').save()
        def onlineAction = new Action(category: onlineActionCat).save()
        storageKind.addToActions onlineAction

        def offlineActionCat = new Category(term: OfflineStorageAction.term, scheme: OfflineStorageAction.scheme, title: 'Offline Storage Action', location: '').save()
        def offlineAction = new Action(category: offlineActionCat).save()
        storageKind.addToActions offlineAction

        def backupActionCat = new Category(term: BackupStorageAction.term, scheme: BackupStorageAction.scheme, title: 'Backup Storage Action', location: '').save()
        def backupAction = new Action(category: backupActionCat).save()
        storageKind.addToActions backupAction

        def snapshotActionCat = new Category(term: SnapshotStorageAction.term, scheme: SnapshotStorageAction.scheme, title: 'Snapshot Storage Action', location: '').save()
        def snapshotAction = new Action(category: snapshotActionCat).save()
        storageKind.addToActions snapshotAction

        def resizeActionCat = new Category(term: ResizeStorageAction.term, scheme: ResizeStorageAction.scheme, title: 'Resize Storage Action', location: '').save()
        def resizeAction = new Action(category: resizeActionCat).save()
        storageKind.addToActions resizeAction
    }
}
