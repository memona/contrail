/*
SVN FILE: $Id: ResourceTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/core/ResourceTests.groovy $
*/

package occi.core

import grails.test.GrailsUnitTestCase
import occi.lexpar.OcciParser

class ResourceTests extends GrailsUnitTestCase {

    protected void setUp() {
        super.setUp()
        mockDomain Resource
        mockDomain Kind
        mockDomain AttributeType
        mockDomain Link
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testCreation(){

        assertEquals 0, Resource.list().size()

        def resK = createResourceKind()
        def res = new Resource(o_id: UUID.randomUUID().toString(), kind: resK)
        assertNotNull res.save()

        resK.addToEntities res
        assertNotNull resK.save()
        
        assertEquals 1, Resource.list().size()

        res = Resource.list()[0]

        assertTrue res.kind.entities.size() == 1
    }

    void testConstraints() {

        def res = new Resource()
        assertFalse res.validate()
        assertEquals "nullable", res.errors["kind"]

        res = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull res.save()
        assertTrue res.validate()

        assertEquals Resource.term, Resource.term
    }

    void testRelatedTypes(){

        def res = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull res.save()
        
        assertTrue res.kind.related.size() == 1
        res.kind.related.each {
            assertEquals Entity.term, it.term
            
            assertNull it.related 
        }
    }

    void testAttributes(){
        
        def res = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull res.save()

        assertTrue res.kind.attributes.size() == 1
    }

    void testSerialisation(){
        
        def resK = createResourceKind()
        def res = new Resource(o_id: UUID.randomUUID().toString(), kind: resK)
        assertNotNull res.save()

        resK.addToEntities res
        assertNotNull resK.save()

        def ser = res.toString()
        assertNotNull ser

        def deser = OcciParser.getParser(ser).headers()
        assertNotNull deser
    }

    void testLinkCreationDeletion(){

        def resId

        def res = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind())
        assertNotNull res.save()

        assertNull res.links

        def targetResource = new Resource(o_id: UUID.randomUUID().toString(), kind: createResourceKind(), summary: 'to where the link goes')
        assertNotNull targetResource.save()

        def link = new Link(o_id: UUID.randomUUID().toString(), sou_rce: res, tar_get: targetResource, kind: createLinkKind())
        assertNotNull link.save()

        res.addToLinks link
        assertNotNull res.save()

        resId = res.o_id

        res = Resource.findByO_id(resId)

        assertNotNull res

        assertTrue res.links.size() == 1

        assertTrue Resource.list().size() == 2

        res.links.each {it.delete()}

        assertTrue Resource.list().size() == 2
    }
    
    private Kind createResourceKind() {

        //create parent kinds
        //we won't bother adding the attributes - see EntityTests
        def entKind = new Kind(term: Entity.term, scheme: Entity.scheme, title: 'Entity type', location: '').save()
        def resKind = new Kind(term: Resource.term, scheme: Resource.scheme, title: 'Resource type', location: '')
        resKind.addToRelated entKind

        //add attributes of resource
        def attr = new AttributeType(name: Resource.occi_core_summary, type: AttributeType.STRING).save()
        resKind.addToAttributes attr

        assertNotNull resKind.save()

        return resKind
    }

    private Kind createLinkKind(){

        def entKind = new Kind(term: Entity.term, scheme: Entity.scheme, title: 'Entity type', location: '').save()
        def linkKind = new Kind(term: Link.term, scheme: Link.scheme, title: 'A Link Type', location: '/link/')
        linkKind.addToRelated entKind

        def attr = new AttributeType(name: Link.occi_link_source, type:AttributeType.STRING).save()
        linkKind.addToAttributes attr
        attr = new AttributeType(name: Link.occi_link_target, type:AttributeType.STRING).save()
        linkKind.addToAttributes attr

        assertNotNull linkKind.save()
        return linkKind
    }
}
