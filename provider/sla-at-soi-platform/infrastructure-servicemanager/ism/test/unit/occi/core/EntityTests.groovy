/*
SVN FILE: $Id: EntityTests.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $

Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/test/unit/occi/core/EntityTests.groovy $

*/

package occi.core

import grails.test.GrailsUnitTestCase
import occi.lexpar.OcciParser

class EntityTests extends GrailsUnitTestCase {

    protected void setUp() {
        
        super.setUp()
        mockDomain Entity
        mockDomain Kind
        mockDomain AttributeType
        mockDomain AttributeValue
        mockDomain Mixin
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstraints() {

        def entity = new Entity()
        assertFalse entity.validate()
        assertEquals "nullable", entity.errors["kind"]

        entity = new Entity(o_id: UUID.randomUUID().toString(), kind: createEntityKind())
        assertTrue entity.validate()
        assertNotNull entity.save()
        
        assertEquals 'entity', Entity.term
    }

    void testConstruction() {

        def eKind = createEntityKind()
        def eMixin = createEntityMixin(eKind)

        def e = new Entity(o_id: UUID.randomUUID().toString(), kind: eKind)
        e.addToMixins eMixin
        
        e.addToAttrvals new AttributeValue(value: 'this_is_the_id', entity: e, attributeInfo: AttributeType.findByName(Entity.occi_core_id)).save()
        e.addToAttrvals new AttributeValue(value: "this_is_the_title", entity: e, attributeInfo: AttributeType.findByName(Entity.occi_core_title)).save()
        assertNotNull e.save()

        eKind.addToEntities e
        assertNotNull eKind.save()

        eMixin.addToEntities e
        assertNotNull eMixin.save()

        assertTrue eKind.entities.size() == 1
        assertTrue e.kind.entities.size() == 1

        assertTrue eMixin.entities.size() == 1
        assertTrue e.mixins.entities.size() == 1


        def entity = Entity.findByKind(Kind.findByTermAndScheme(Entity.term, Entity.scheme))
        assertNotNull entity

        assertNotNull entity.kind
        assertEquals Entity.term, entity.kind.term
        assertEquals Entity.scheme, entity.kind.scheme
        assertTrue entity.attrvals.size() == 2
    }

    void testRelatedTypes(){
        
        def entity = new Entity(o_id: UUID.randomUUID().toString(), kind: createEntityKind())
        assertNotNull entity.save()
        assertNull entity.kind.related
    }

    void testAttributes(){
        def entity = new Entity(o_id: UUID.randomUUID().toString(), kind: createEntityKind())
        assertNotNull entity.save()

        assertTrue entity.kind.attributes.size() == 2
    }

    void testSerialisaton(){
        
        def entity = new Entity(o_id: UUID.randomUUID().toString(), kind: createEntityKind())
        assertNotNull entity.save()
        
        def ser = entity.toString()
        assertNotNull ser

        def deser = OcciParser.getParser(ser).headers()
        assertNotNull deser
    }
    
    private Kind createEntityKind() {

        //An entities kind is not related to anything else
        def entKind = new Kind(term: Entity.term, scheme: Entity.scheme, title: 'Entity type', location: '')

        def attr = new AttributeType(name: Entity.occi_core_id, type: AttributeType.STRING, defaultValueExpr: "NONE").save()
        entKind.addToAttributes attr
        attr = new AttributeType(name: Entity.occi_core_title, type: AttributeType.STRING, defaultValueExpr: "Title").save()
        entKind.addToAttributes attr

        assertNotNull entKind.save()
        
        return entKind
    }

    private Mixin createEntityMixin(applicableKind) {

        //An entities kind is not related to anything else
        def entMixin = new Mixin(term: Entity.term, scheme: Entity.scheme, title: 'Entity type', location: '')
        entMixin.applicableKind = applicableKind
        assertNotNull entMixin.save()

        return entMixin
    }
}
