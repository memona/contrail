package eu.slasoi.infrastructure.controllers

/*
SVN FILE: $Id:$

Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author:$
@version        $Rev:$
@lastrevision   $Date:$
@filesource     $URL:$
*/

import eu.slasoi.infrastructure.model.Reservation
import eu.slasoi.infrastructure.model.Service
import grails.test.ControllerUnitTestCase
import occi.infrastructure.Compute

class   ReservationControllerTests extends ControllerUnitTestCase {

    ReservationController rc

    protected void setUp() {

        super.setUp()
        rc = new ReservationController()
        rc.request.contentType = 'text/plain'
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testReserveSingleSmallCompute() {

        String content = "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', small; scheme='http://sla-at-soi.eu/occi/infrastructure/res_template#'; class='mixin', reservation; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'"
        rc.request.content = content.getBytes()

        rc.doPost()

        assertEquals 1, Reservation.count()
        assertEquals HttpURLConnection.HTTP_CREATED, rc.response.status
        def loc = rc.response.getHeader('Location')
        assertNotNull loc
        assertEquals 'http://localhost:8080/reservation/'+Reservation.list()[0].o_id, loc
    }

    void testReserveSingleMediumCompute() {

        String content = "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', medium; scheme='http://sla-at-soi.eu/occi/infrastructure/res_template#'; class='mixin', reservation; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'"
        rc.request.content = content.getBytes()

        rc.doPost()

        assertEquals 1, Reservation.count()
        assertEquals HttpURLConnection.HTTP_CREATED, rc.response.status
        def loc = rc.response.getHeader('Location')
        assertNotNull loc
        assertEquals 'http://localhost:8080/reservation/'+Reservation.list()[0].o_id, loc
    }

    void testReserveSingleLargeCompute() {

        String content = "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', large; scheme='http://sla-at-soi.eu/occi/infrastructure/res_template#'; class='mixin', reservation; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'"
        rc.request.content = content.getBytes()

        rc.doPost()

        assertEquals 1, Reservation.count()
        assertEquals HttpURLConnection.HTTP_CREATED, rc.response.status
        def loc = rc.response.getHeader('Location')
        assertNotNull loc
        assertEquals 'http://localhost:8080/reservation/'+Reservation.list()[0].o_id, loc
    }

    void testReserveSingleComputeRelease(){

        String content = "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', small; scheme='http://sla-at-soi.eu/occi/infrastructure/res_template#'; class='mixin', reservation; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'"
        rc.request.content = content.getBytes()

        rc.doPost()

        assertEquals 1, Reservation.count()

        rc.params.instanceId = Reservation.list()[0].o_id

        rc.doDelete()
        assertEquals HttpURLConnection.HTTP_OK, rc.response.status
        assertEquals 0, Reservation.count()
    }

    void testReserveService2SmallComputes(){

        def content ='--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="1"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: service; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'; title='An infrastructure service'\r\n" +
                     "X-OCCI-Attribute: eu.slasoi.infrastructure.service.name='myService', eu.slasoi.infrastructure.service.monitoring.config='SOME_SERIALISED_STRING'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="2"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: reservation; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="3"\r\n'+
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', small; scheme='http://sla-at-soi.eu/occi/infrastructure/res_template#'; class='mixin'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="4"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', small; scheme='http://sla-at-soi.eu/occi/infrastructure/res_template#'; class='mixin'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn--\r\n'

        rc.request.content = content.bytes
        rc.request.contentType = 'multipart/form-data; boundary=55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn'
        rc.request.addHeader('content-type', 'multipart/form-data; boundary=55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn')

        rc.doPost()

        assertEquals 1, Reservation.count()
    }

    void testReserveService1LargeCompute(){

        def content ='--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="1"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: service; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'; title='An infrastructure service'\r\n" +
                     "X-OCCI-Attribute: eu.slasoi.infrastructure.service.name='myService', eu.slasoi.infrastructure.service.monitoring.config='SOME_SERIALISED_STRING'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="2"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: reservation; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="3"\r\n'+
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', large; scheme='http://sla-at-soi.eu/occi/infrastructure/res_template#'; class='mixin'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn--\r\n'

        rc.request.content = content.bytes
        rc.request.contentType = 'multipart/form-data; boundary=55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn'
        rc.request.addHeader('content-type', 'multipart/form-data; boundary=55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn')

        rc.doPost()

        assertEquals 1, Reservation.count()
    }

    void testReserviceSingleSmallComputeCommit(){
        //http://localhost:8080/reservation/96bf02c4-4e14-40bd-bf26-380fef7648db

        String content = "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', small; scheme='http://sla-at-soi.eu/occi/infrastructure/res_template#'; class='mixin', reservation; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'"
        rc.request.content = content.getBytes()

        rc.doPost()

        assertEquals 1, Reservation.count()

        rc.params.instanceId = Reservation.list()[0].o_id
        rc.request.queryString = 'action=commit'

        rc.doPost()
        assertEquals HttpURLConnection.HTTP_CREATED, rc.response.status
//        assertEquals 1, Compute.count() TODO reenable
    }

    void testReserveService2SmallComputesCommit(){

        def content ='--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="1"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: service; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'; title='An infrastructure service'\r\n" +
                     "X-OCCI-Attribute: eu.slasoi.infrastructure.service.name='myService', eu.slasoi.infrastructure.service.monitoring.config='SOME_SERIALISED_STRING'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="2"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: reservation; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="3"\r\n'+
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', small; scheme='http://sla-at-soi.eu/occi/infrastructure/res_template#'; class='mixin'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn\r\n' +
                     'Content-Disposition: form-data; name="4"\r\n' +
                     'Content-Type: text/plain; charset=US-ASCII\r\n' +
                     'Content-Transfer-Encoding: 8bit\r\n' +
                     '\r\n' +
                     "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', small; scheme='http://sla-at-soi.eu/occi/infrastructure/res_template#'; class='mixin'\r\n" +
                     '--55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn--\r\n'

        rc.request.content = content.bytes
        rc.request.contentType = 'multipart/form-data; boundary=55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn'
        rc.request.addHeader('content-type', 'multipart/form-data; boundary=55HF-lv9jFpd6YUtYc2K5IK6S2jfZdn')

        rc.doPost()

        assertEquals 1, Reservation.count()

        rc.params.instanceId = Reservation.list()[0].o_id
        rc.request.queryString = 'action=commit'

        rc.doPost()
        
        assertEquals HttpURLConnection.HTTP_CREATED, rc.response.status
        assertEquals 1, Service.count()
    }
}

 /*void testJcloudsReservation(){

        def content ='----JCLOUDS--\r\n' +
                     'Content-Disposition: form-data; name="container1"\r\n' +
                     'Content-Type: application/unknown\r\n' +
                     '\r\n' +
                     "Category: compute; scheme='http://schemas.ogf.org/occi/infrastructure#'; class='kind', ubuntu_9-10; scheme='http://sla-at-soi.eu/occi/templates#'; class ='mixin', small; scheme='http://sla-at-soi.eu/occi/infrastructure/template#'; class='mixin'\r\n" +
                     '----JCLOUDS--\r\n' +
                     'Content-Disposition: form-data; name="container2"\r\n' +
                     'Content-Type: application/unknown\r\n' +
                     '\r\n' +
                     "Category: reservation; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'\r\n" +
                     '----JCLOUDS--\r\n' +
                     'Content-Disposition: form-data; name="container0"\r\n'+
                     'Content-Type: application/unknown\r\n' +
                     '\r\n' +
                     "Category: service; scheme='http://sla-at-soi.eu/occi/infrastructure#'; class='kind'; title='An infrastructure service'\r\n" +
                     "X-OCCI-Attribute: occi.core.id='5f00375c-a4ce-42d8-b40d-16bddd78c774',  eu.slasoi.infrastructure.service.name='myService'\r\n" +
                     '----JCLOUDS----\r\n'

        rc.request.content = content.bytes
        rc.request.contentType = 'multipart/form-data; boundary=--JCLOUDS--'
        rc.request.addHeader('content-type', 'multipart/form-data; boundary=--JCLOUDS--')

        rc.doPost()

        assertEquals 1, Reservation.count()
    }*/
