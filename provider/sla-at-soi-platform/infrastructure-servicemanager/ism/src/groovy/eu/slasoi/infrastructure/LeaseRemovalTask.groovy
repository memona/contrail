package eu.slasoi.infrastructure

import org.codehaus.groovy.grails.commons.GrailsApplication
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationEventPublisher

/*
SVN FILE: $Id:$ 
 
Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author:$
@version        $Rev:$
@lastrevision   $Date:$
@filesource     $URL:$
*/

class LeaseRemovalTask extends TimerTask implements ApplicationEventPublisher{

    String resvId
    GrailsApplication grailsApplication
    //GrailsApplicationEventMulticaster applicationEventMulticaster
    //ReservationService resvSvc
    
//    public LeaseRemovalTask(){}
//
//    public LeaseRemovalTask(String resvId, ReservationService resvSvc){
//        this.resvId = resvId
//        this.resvSvc = resvSvc
//    }

    public void run() {
        //resvSvc.release(resvId)
        //applicationEventMulticaster.multicastEvent(event)

        publishEvent (new LeaseRemovalEvent(this))
    }

    public void publishEvent(ApplicationEvent applicationEvent) {
        grailsApplication.mainContext.publishEvent(applicationEvent)
    }
}

class LeaseRemovalEvent extends ApplicationEvent {
	LeaseRemovalEvent(LeaseRemovalTask source) {
		super(source)
	}
}
