/*
SVN FILE: $Id: resources.groovy 2773 2011-07-22 15:31:38Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2773 $
@lastrevision   $Date: 2011-07-22 17:31:38 +0200 (pet, 22 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/conf/spring/resources.groovy $

*/

//If there are decls here and also in resources.xml grails will not know which to use

import eu.slasoi.infrastructure.authentication.ISMUserDetailsContextMapper
import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

beans = {
    
    ldapUserDetailsMapper(ISMUserDetailsContextMapper) {}

    def psbProps = [
        'pubsub': CH.config.eu.slasoi.infrastructure.monitoring.llms.pubsubType ?: 'in_memory',
        'xmpp_username': CH.config.eu.slasoi.infrastructure.monitoring.llms.xmpp_username ?: 'test3',
        'xmpp_password': CH.config.eu.slasoi.infrastructure.monitoring.llms.xmpp_password ?: 'test3',
        'xmpp_host': CH.config.eu.slasoi.infrastructure.monitoring.llms.xmpp_host ?: 'slasoi.dnsalias.net',
        'xmpp_port': CH.config.eu.slasoi.infrastructure.monitoring.llms.xmpp_port ?: '5222',
        'xmpp_service': CH.config.eu.slasoi.infrastructure.monitoring.llms.xmpp_service ?: 'slasoi.dnsalias.net',
        'xmpp_resource': CH.config.eu.slasoi.infrastructure.monitoring.llms.xmpp_resource ?: 'test3',
        'xmpp_pubsubservice': CH.config.eu.slasoi.infrastructure.monitoring.llms.xmpp_pubsubservice ?: 'slasoi.dnsalias.net'
    ]
    
    pubSubDispatcher(org.slasoi.common.messaging.pubsub.PubSubFactory, psbProps) { bean ->
        bean.factoryMethodName = "createPubSubManager"
        bean.destroyMethodName = "close"
        bean.lazyInit = true
    }
}
