/*
SVN FILE: $Id: UrlMappings.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/conf/UrlMappings.groovy $

*/


class UrlMappings {

    static mappings = {

        //TODO these mappings need to be generated dynamically based on the config file
        
        "/$controller/$action?/$id?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(view: "/index")

        "400"(controller: "errorHandler", action: "badRequest")
        "404"(controller: "errorHandler", action: "notFound")
        "406"(controller: "errorHandler", action: "notAcceptable")
        "409"(controller: "errorHandler", action: "conflict")
        "500"(controller: "errorHandler", action: "internalError")
        "501"(controller: "errorHandler", action: "notImplemented")

        "/-/$rootTerm?/$query**?"(controller: "query") {
            action = [GET: "doGet"]
        }

        "/.well-known/org/ogf/occi/-/$rootTerm?/$query**?"(controller: "query") {
            action = [GET: "doGet"]
        }

        "/link/$instanceId?"(controller: "link") {
            action = [GET: "doGet", PUT: "doPut", DELETE: "doDelete", POST: "doPost"]
        }

        "/compute/$computeId?"(controller: "compute") {
            action = [GET: "doGet", PUT: "doPut", DELETE: "doDelete", POST: "doPost"]
        }

        "/service/$instanceId?"(controller: "service") {
            action = [GET: "doGet", PUT: "doPut", DELETE: "doDelete", POST: "doPost"]
        }

        "/reservation/$instanceId?"(controller: "reservation") {
            action = [GET: "doGet", DELETE: "doDelete", POST: "doPost"]
        }

        "/ipnetwork/$instanceId?"(controller: "mixin") {
            action = [GET: "doGet", PUT: "doPut", DELETE: "doDelete", POST: "doPost"]
        }

        //TODO ip link should be handled by LinkController
        "/iplink/$instanceId?"(controller: "mixin") {
            action = [GET: "doGet", PUT: "doPut", DELETE: "doDelete", POST: "doPost"]
        }

        "/os_tpl/$instanceId?"(controller: "mixin") {
            action = [GET: "doGet", PUT: "doPut", DELETE: "doDelete", POST: "doPost"]
        }

        "/user/$instanceId?"(controller: "mixin"){
            action = [GET: "doGet", PUT: "doPut", DELETE: "doDelete", POST: "doPost"]
        }

        "/hosts/$instanceId?"(controller: "host"){
            action = [GET: "doGet"]
        }

//        "/slasoicompute/$instanceId?"(controller: "mixin") {
//            action = [GET: "doGet", PUT: "doPut", DELETE: "doDelete", POST: "doPost"]
//        }
    }
}


//        "/ism/$category?"(controller: "query") {
//            action = [GET: "doProviderSpecificGet"]
//        }

//        "/jobs/$jobId?"(controller: "scheduler") {
//            action = [GET: "doGet"] //, DELETE: "doDelete"
//        }

//    "/storage/$storageId?"(controller: "storage") {
//      action = [GET: "doGet", PUT: "doPut", DELETE: "doDelete", POST: "doPost"]
//    }
//
//    "/network/$networkId?"(controller: "network") {
//      action = [GET: "doGet", PUT: "doPut", DELETE: "doDelete", POST: "doPost"]
//    }
