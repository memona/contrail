/* 
SVN FILE: $Id: BuildConfig.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/conf/BuildConfig.groovy $

*/

grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.war.file = "ROOT.war"

grails.project.dependency.resolution = {

    def slasoiVersion= "0.1-SNAPSHOT"

    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }

    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'

    repositories {

        grailsCentral()
        grailsPlugins()
        grailsHome()

        // uncomment the below to enable remote dependency resolution
        // from public Maven repositories
        ebr()
        mavenLocal()
        mavenCentral()
        mavenRepo "http://snapshots.repository.codehaus.org"
        mavenRepo "http://repository.codehaus.org"
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repository.jboss.com/maven2/"
        mavenRepo "http://snapshots.jboss.org/maven2"
        mavenRepo "http://sla-at-soi.sourceforge.net/maven2/"
        //mavenRepo "http://maven.springframework.org/milestone"
        //mavenRepo "http://snapshots.repository.codehaus.org"
    }
    // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
    dependencies {

        //If you wish to use MySQL as your ORM backend, uncomment this
        //runtime 'mysql:mysql-connector-java:5.1.13'

        runtime 'org.jetlang:jetlang:0.1.7'
        runtime "org.slasoi.models:scm:${slasoiVersion}"
        runtime "org.slasoi.common.messaging:messaging:${slasoiVersion}"
        runtime 'org.mod4j.org.eclipse.emf:common:2.5.0'
        runtime 'org.mod4j.org.eclipse.emf:ecore:2.5.0'
        runtime 'com.google.code.gson:gson:1.6'
        runtime 'com.thoughtworks.xstream:xstream:1.3.1'
        compile 'junit:junit:4.8.1'
    }

//    plugins {
//
//        runtime ':hibernate:latest.release'
//        runtime ':redis:latest.release'
//        runtime ':spring-security-core:latest.release'
//        runtime ':spring-security-ldap:latest.release'
//        runtime ':tomcat:latest.release'
//        runtime ':xmpp:latest.release'
//    }
}
