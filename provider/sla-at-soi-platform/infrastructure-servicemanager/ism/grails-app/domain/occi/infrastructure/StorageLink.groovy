package occi.infrastructure

class StorageLink {

    static String term = 'storagelink'
    static String scheme = 'http://schemas.ogf.org/occi/infrastructure#'
    static String occi_storagelink_deviceid = 'occi.storagelink.deviceid'
    static String occi_storagelink_mountpoint = 'occi.storagelink.mountpoint'
    static String occi_storagelink_state = 'occi.storagelink.state'
    static String occi_storagelink_state_active = 'active'
    static String occi_storagelink_state_inactive = 'inactive'

    static transients = ['term', 'scheme', 'occi_storagelink_deviceid', 'occi_storagelink_mountpoint', 'occi_storagelink_state', 'occi_storagelink_state_active', 'occi_storagelink_state_inactive']
    
    static constraints = {
    }

    def String toString() {
        return super.toString()
    }
}
