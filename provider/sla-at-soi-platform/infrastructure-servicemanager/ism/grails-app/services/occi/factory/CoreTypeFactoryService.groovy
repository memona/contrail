/*
SVN FILE: $Id: CoreTypeFactoryService.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $

Copyright (c) 2008-2011, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/services/occi/factory/CoreTypeFactoryService.groovy $
*/

package occi.factory

import occi.core.*
import occi.core.Mixin
import occi.core.Category

/*
This class ensures the correct creation and persistence of domain objects.
 */

class CoreTypeFactoryService {

    protected Category createCategory(String term, String scheme, String title, String location=''){

        log.info("Creating new instance of category: ${scheme}${term}")
        def cat = Category.findByTermAndScheme(term, scheme)

        if(!cat){
            cat = new Category(term: term, scheme: scheme, title: title, location: location, isEnabled: true)
            cat.save()
            checkErrors(cat)
        }

        return cat
    }

    protected Action createAction(String term, String scheme, String title){

        log.info("Creating new instance of action: ${scheme}${term}")

        Action a = Action.findByTermAndScheme(term, scheme)

        if(!a){
            a = new Action(term: term, scheme: scheme, location: '', title: title)
            a.save()
            checkErrors(a)
        }

        return a
    }

    protected Mixin createMixin(String term, String scheme, String title, String location, Kind applicableKind){

        if(applicableKind)
            log.info("Creating new instance of mixin: ${scheme}${term}. Will associate it with: ${applicableKind.scheme}${applicableKind.term}")
        else
            log.info("Creating new instance of mixin: ${scheme}${term}. Not associated with another kind.")

        def mixin = Mixin.findByTermAndScheme(term, scheme)

        if(!mixin){
            mixin = new Mixin(term: term, scheme: scheme, title: title, location: location, isEnabled: true)
            if(applicableKind)
                mixin.applicableKind = applicableKind
            mixin.save()
            checkErrors(mixin)
        }

        return mixin
    }

    protected AttributeType createAttributeType(Category cat, String name, String type, String defaultValue,
                                                    Boolean renderme, Boolean mutable, Boolean required){

        def attr = AttributeType.findByName(name) ?:
            new AttributeType(name: name, type: type,
                    defaultValueExpr: defaultValue,
                    renderme: renderme, mutable: mutable,
                    required: required)
        attr.save()
        checkErrors(attr)
        cat.addToAttributes attr
        return attr
    }

    private Kind createEntityKind(){

        log.info("Creating new instance of Entity kind")
        def entKind = Kind.findByTermAndScheme(Entity.term, Entity.scheme)

        if(!entKind){
            
            entKind = new Kind(term: Entity.term, scheme: Entity.scheme, title: 'Entity type', location: '', isEnabled: true)
            entKind.save()
            checkErrors(entKind)

            createAttributeType(entKind, Entity.occi_core_id, AttributeType.STRING, "NONE", false, true, false)
            createAttributeType(entKind, Entity.occi_core_title, AttributeType.STRING, "Title", true, true, false)

            entKind.save()
            checkErrors(entKind)
        }
        return entKind
    }
    
    protected Kind createResourceKind(){

        log.info("Creating new instance of Resource kind")
        def resourceKind = Kind.findByTermAndScheme(Resource.term, Resource.scheme)
        
        if(!resourceKind){

            def entityKind = createEntityKind()

            resourceKind = new Kind(term: Resource.term, scheme: Resource.scheme, title: 'Resource type', location: '', isEnabled: true)
            resourceKind.addToRelated entityKind

            createAttributeType(resourceKind, Resource.occi_core_summary, AttributeType.STRING, "Summary",true, true, false)

            resourceKind.save()
            checkErrors(resourceKind)
        }

        return resourceKind
    }

    protected Kind createLinkKind(){

        def linkKind = Kind.findByTermAndScheme(Link.term, Link.scheme)

        if(!linkKind){

            log.info("Creating new instance of Link kind")

            Kind entityKind = createEntityKind()

            linkKind = new Kind(term: Link.term, scheme: Link.scheme, title: 'Link type', location: '/link/', isEnabled: true)
            linkKind.addToRelated entityKind
            linkKind.save()
            checkErrors(linkKind)

            createAttributeType(linkKind, Link.occi_link_source, AttributeType.STRING, "${Link.occi_link_source}",
                true, true, true)

            createAttributeType(linkKind, Link.occi_link_target, AttributeType.STRING, "${Link.occi_link_target}",
                true, true, true)
            linkKind.save()
            checkErrors(linkKind)
        }

        return linkKind
    }

    protected checkErrors(persistedObject){

        if(persistedObject.hasErrors()) {
            log.error("There was an error persisting an object")
            def err = ''
            persistedObject.errors.each {
                err += it
                log.error(it)
            }
            throw new Exception("There was an error persisting an object: " + err)
        }
    }
}
