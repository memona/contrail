/* 
SVN FILE: $Id: MonitoringService.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/services/eu/slasoi/infrastructure/services/monitoring/MonitoringService.groovy $

*/

package eu.slasoi.infrastructure.services.monitoring

//import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest
//import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest.Vm


import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

import eu.slasoi.infrastructure.messaging.MessageDispatcher
import eu.slasoi.infrastructure.messaging.MessageHandler
import eu.slasoi.infrastructure.model.Service
import occi.infrastructure.Compute
import org.slasoi.common.messaging.pubsub.PubSubMessage
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest.Vm
import org.springframework.beans.factory.NoSuchBeanDefinitionException
import occi.core.AttributeValue

class MonitoringService {

    static scope = "singleton"

    //spring injected bean, see resources.groovy
    def pubSubDispatcher
    //Closure to handle monitoring provision responses from the LLMS
    def handleLLMSResponse = { message ->
        println message
    }

    def initMonitoringService(){
        messageHandlerService.registerHandler handleLLMSResponse
    }

    def provisionMonitoring(Service serviceToMonitor) {

        org.slasoi.monitoring.common.features.ComponentMonitoringFeatures

        def testbedDomain = CH.config.eu.slasoi.infrastructure.domain ?: 'openlab.com'
        def llmsChannel = CH.config.eu.slasoi.infrastructure.monitoring.llmsChannel ?: "llms@localhost"

        log.info "Provisioning monitoring..."

        RegisterServiceRequest rsr = new RegisterServiceRequest();

		rsr.messageId = UUID.randomUUID().toString()

        AttributeValue prop = serviceToMonitor.getAttrValue(Service.eu_slasoi_infrastructure_service_monitoringconfig)

		rsr.monSystemConfSerialized = prop.value

        prop = serviceToMonitor.getAttrValue(Service.eu_slasoi_infrastructure_service_name)
		rsr.serviceName = prop.value
        
		rsr.serviceUrl = CH.config.grails.serverURL + serviceToMonitor.kind.location + serviceToMonitor.o_id

            List<Vm> vmList = new ArrayList<Vm>()

            log.debug "There are ${serviceToMonitor.resources.size()} resources to monitor as part of this service."

            serviceToMonitor.resources.each { res ->
                if (res instanceof Compute) {
                    def hostname = res.getAttrValue(Compute.occi_compute_hostname).value
                    vmList.add(new Vm(hostname, "${hostname}.${testbedDomain}"))
                    log.debug "Adding ${hostname}.${testbedDomain} running on tashi to the list of resources to be monitored."
                }
            }

        rsr.vmList = vmList

        log.debug "Dispatching service monitoring request via message bus to LLMS..."
        log.debug "Final monitoring request:\n${rsr.toJson()}"

        //the messageDispatchService is not appropriate, no XEP0060 support
        //messageDispatchService.sendMessage(rsr.toJson(), llmsChannel)


        PubSubMessage message = new PubSubMessage(llmsChannel, rsr.toJson());
        pubSubDispatcher.publish message

        //TODO might not be required - add a response handler?
    }

    private MessageDispatcher getMessageDispatchService(){

        MessageDispatcher bean

        try{
            bean = (MessageDispatcher)grailsApplication.mainContext.getBean('messageDispatchService')
        }
        catch (NoSuchBeanDefinitionException nbdf){
            bean = null
            log.error "The message dispatcher bean was not found."
        }
        return bean
    }

    private MessageHandler getMessageHandlerService(){
        
        MessageHandler bean

        try{
            bean = (MessageDispatcher)grailsApplication.mainContext.getBean('messageHandlerService')
        }
        catch (NoSuchBeanDefinitionException nbdf){
            bean = null
            log.error "The message handler bean was not found."
        }
        return bean
    }
}
