package eu.slasoi.infrastructure.controllers

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

import eu.slasoi.infrastructure.exceptions.IsmHttpException
import eu.slasoi.infrastructure.model.Reservation
import eu.slasoi.infrastructure.model.Service
import occi.core.Resource
import occi.infrastructure.mixins.ResourceTemplateMixin
import occi.lexpar.OcciParser
import occi.lexpar.OcciParserException
import occi.core.Entity
import occi.infrastructure.Compute

class ReservationController {

    static def OCCI_VERSION = 'OCCI/1.1'
    
    def reservationService
    def multipartService
    def infrastructureInstanceFactoryService

    def beforeInterceptor = {
        response.setHeader('Server', OCCI_VERSION)
    }

    def doGet = {

        //lists reservation details
        //      show time left on lease
        //      show when the lease was first placed
        if(params.instanceId){

            log.info "Getting details on reservation ${params.instanceId}"
            Reservation resv = Reservation.findByO_id(params.instanceId)
            if(resv){
                withFormat {
                    all{ reservDetailsAsText(resv) }
                    text{ reservDetailsAsText(resv) }
                    occi{ reservDetailsAsOcci(resv) }
                    uriList{ reservDetailsAsUriList(resv) }
                    multipartForm{reservsDetailsAsMultipart(resv)}
                }
            }else{
                log.warn "There is no reservation instances running with the id of ${params.instanceId}. It is likely the lease has expired or has been committed to."
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "There is no reservation instances running with the id of ${params.instanceId}. It is likely the lease has expired or has been committed to."
            }
        }
        //list all current reservations
        else{
            log.info "Listing all current reservations."
            def resvs = Reservation.list()

            if(resvs?.size() > 0){
                withFormat {
                    all{ reservsListAsText(resvs) }
                    text{ reservsListAsText(resvs) }
                    occi{ reservsListAsOcci(resvs) }
                    uriList{ reservsListAsUrlList(resvs) }
                }
            }else {
                log.warn "There are no reservation instances available to list."
                response.sendError HttpURLConnection.HTTP_OK
            }
        }
    }

    //this is not REST and not really OCCI, it's a hack
    private def reservDetailsAsText(Reservation resv) {

        response.status = HttpURLConnection.HTTP_OK
        response.contentType = 'text/plain'

        def reservationId = ''
        def occiContent

        //get the content
        if(resv.isMultipart){
            def boundary = resv.provisioningRequest.split('\r\n')[0][2..-1]
            occiContent = multipartService.extractContent(resv.provisioningRequest, boundary)

            //Get the full service representation
            def svcContent = occiContent.findAll{
                it.contains(Service.scheme) && it.contains(Service.term)
            }

            //there should only be one service found
            if(svcContent.size() > 1){
                log.error "There was more than one service category found in the multipart request. Internal error: the POST controller should of prevented this."
                response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, "There was more than one service category found in the multipart request. Internal error: the POST controller should of prevented this."
            }
            else{
                
                svcContent = svcContent[0]

                def parsedAttrs

                try{
                   parsedAttrs = OcciParser.getParser(svcContent).headers()
                }catch (OcciParserException pe){
                    log.error pe.toString()
                    response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, pe.toString()
                    return
                }

                //assemble content to return
                reservationId = parsedAttrs[OcciParser.occi_attributes][Entity.occi_core_id]

                def svcLinks = "</service/${reservationId[0]}?action=stop>; rel='http://sla-at-soi.eu/occi/infrastructure#stop'"
                svcLinks += ", </service/${reservationId[0]}?action=start>; rel='http://sla-at-soi.eu/occi/infrastructure#start'"
                svcLinks += ", </service/${reservationId[0]}?action=suspend>; rel='http://sla-at-soi.eu/occi/infrastructure#suspend'"
                svcLinks += ", </service/${reservationId[0]}?action=restart>; rel='http://sla-at-soi.eu/occi/infrastructure#restart'"

                def resContent = occiContent.findAll {
                   it.contains(Compute.scheme) && it.contains(Compute.term)
                }

                def compLinks = ''

                resContent.each {
                    
                    try{
                       parsedAttrs = OcciParser.getParser(svcContent).headers()
                    }catch (OcciParserException pe){
                        log.error pe.toString()
                        response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, pe.toString()
                        return
                    }

                    //assemble content to return
                    reservationId = parsedAttrs[OcciParser.occi_attributes][Entity.occi_core_id]
                    compLinks += ", </compute/${reservationId[0]}>; rel='http://schemas.ogf.org/occi/infrastructure#compute'; self='/link/${reservationId[0]}'; category='http://schemas.ogf.org/occi/core#link'"
                }

                render svcContent + 'Link: ' + svcLinks + compLinks+ '\r\n'
            }
        }
        else{ //is not multipart
            render resv.provisioningRequest
        }
    }

    private def reservDetailsAsOcci(Reservation resv) {

        response.status = HttpURLConnection.HTTP_OK
        response.contentType = 'text/occi'

        def headers = resv.toHeaderArray()

        response.setHeader('Category', headers['categories'])
        response.setHeader('X-OCCI-Attribute', headers['attributes'])
        response.setHeader('Link', headers['links'])

        render "ok\r\n"
    }

    private def reservDetailsAsUriList(Reservation resv){

        response.sendError HttpURLConnection.HTTP_NOT_ACCEPTABLE, "Requested content type text/occi for an instance. Not acceptable."
        log.warn "Requested content type text/occi for an instance. Not acceptable."
    }

    private def reservsDetailsAsMultipart(Reservation resv){
        
        response.status = HttpURLConnection.HTTP_OK

        def rawContent = resv.provisioningRequest
        def boundary = rawContent.split('\r\n')[0][2..-1]
        
        response.contentType = "multipart/form-data; boundary=${boundary}"

        render rawContent
    }

    private def reservsListAsText(resvs){
        response.contentType = 'text/plain'
        resvs.each{
            render 'X-OCCI-Location: ' + CH.config.grails.serverURL + it.kind.location + it.o_id + "\n"
        }
    }

    private def reservsListAsOcci(resvs){

        response.contentType = 'text/occi'
        def value = ''
        resvs.each{
            value += CH.config.grails.serverURL + it.kind.location + it.o_id + ", "
        }
        response.setHeader('X-OCCI-Location', value.trim()[0..-2])
    }

    private def reservsListAsUrlList(resvs){

        response.contentType = 'text/uri-list'
        resvs.each{
            render CH.config.grails.serverURL + it.kind.location + it.o_id + "\n"
        }
    }

    //needs to handle both compute and service provisioning types
    def doPost = {

        //commits a reservation
        //      implemented with OCCI action 'commit'
        //      must be done within the lease period
        if(params.instanceId){

            if(request?.queryString?.startsWith('action=')){
                
                log.info "Executing an action on the reservation"
                def actionName = request.queryString.split('action=')[1]

                if(actionName == 'commit'){
                    try{
                        Resource res = reservationService.commitLease(params.instanceId)
                        response.status = HttpURLConnection.HTTP_CREATED
                        response.setHeader('Location', CH.config.grails.serverURL + res.kind.location + res.o_id)
                        response.setContentType('text/plain')

                        render "ok\r\n"
                    }catch(Exception e){
                        log.error e.toString()
                        response.sendError HttpURLConnection.HTTP_NOT_FOUND, e.toString()
                        return
                    }
                }
                else{
                    log.error "Unrecognised action: ${actionName}"
                    response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "Unrecognised action: ${actionName}"
                }
            }
            else{
                log.error 'Partial updates to reservations are not supported'
                response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED , 'Partial updates to reservations are not supported'
            }
        }
        //creates reservation
        //      can respond that reservation cannot be done, even though QI previously showed room
        //      response is the location of the reservation
        //      response contains the lease life and when the lease began running
        else{

            log.info "Creating a reservation"
            //if the request only contains one type then it fails
            //  e.g. if there's only a compute type and no reservation type, fail
            //  e.g. if there's only a reservation type and no resource type, fail
            withFormat {
                all{ reservationCreateAsText() }
                text{ reservationCreateAsText() }
                multipartForm{reservationCreateAsMultipart()}
            }
        }
    }

    //creates reservation from one resource
    private def reservationCreateAsText(){

        def rawContent = request.reader?.text

        if(!rawContent){
            log.error "Bad Request: No content supplied in request."
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "Bad Request: No content supplied in request."
        }
        else{
            
            def parsedAttrs

            try{
                parsedAttrs = OcciParser.getParser(rawContent).headers()
            }catch (OcciParserException pe){
                log.error pe.toString()
                response.sendError HttpURLConnection.HTTP_BAD_REQUEST, pe.toString()
                return
            }

            Reservation resv = null

            //this is ugly - should be removed.
            rawContent = injectResourceId(rawContent)

            if(infrastructureInstanceFactoryService.containsCategory(parsedAttrs, Reservation.term, Reservation.scheme)){
                try{
                    resv = reservationService.reserve(rawContent, [parsedAttrs])
                }catch(IsmHttpException ie){
                    log.error ie.message
                    response.sendError ie.httpErrorCode, ie.message
                }
            }
            else{
                log.error "The request did not include a Reservation category (${Reservation.scheme}${Reservation.term})"
                response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "The request did not include a Reservation category (${Reservation.scheme}${Reservation.term})"
            }

            if(resv){

                log.info "Created the reservation ${resv.o_id}"
                response.status = HttpURLConnection.HTTP_CREATED
                response.setHeader('Location', CH.config.grails.serverURL + resv.kind.location + resv.o_id)
                response.setContentType('text/plain')

                render "ok\r\n"
            }
            else{
                log.error "The reservation was not successfully created."
                response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, "The reservation was not successfully created."
            }
        }
    }

    private String injectResourceId(rawContent) {
        
        if (!rawContent.contains(Entity.occi_core_id)) {

            log.info "No occi.core.id is in the request. Will generate one and insert into raw content."
            
            if(!rawContent.contains('X-OCCI-Attribute:')){
                if (rawContent.endsWith('\r\n'))
                    rawContent += "X-OCCI-Attribute: occi.core.id='${UUID.randomUUID().toString()}'\r\n"
                else
                    rawContent += "\r\nX-OCCI-Attribute: occi.core.id='${UUID.randomUUID().toString()}'\r\n"
            }
            else{
                def tmp = ''
                def lines = rawContent.split('\r\n')
                lines.each {
                    if (it.contains('X-OCCI-Attribute:')) {
                        it = it + ", occi.core.id='${UUID.randomUUID().toString()}'"
                    }
                    tmp += it + '\r\n'
                }

                rawContent = tmp
            }
//            log.debug "Raw content updated. Now: ${rawContent}"
        }
        return rawContent
    }

    //creates reservation from many reources (multipart)
    private def reservationCreateAsMultipart(){

        def contentHeader = request.getHeader('content-type')
        def boundary = contentHeader.split('boundary=')
        if(boundary && boundary.size() >= 2)
            boundary = boundary[1]
        else{
            log.error "The multipart content-type value did not include a boundary value."
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "The multipart content-type value did not include a boundary value."
        }

        def rawContent = request.reader?.text
        def occiContent = multipartService.extractContent(rawContent, boundary)

        if(occiContent.size() <= 0 || !rawContent){
            log.error "There was no body-content in the request."
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "There was no body-content in the request."
        }
        else{

            def validRequest = false
            def serviceContent = []
            def resourceContent = []
            def tmpRaw = []
            def resvId = ''
            
            occiContent.each {

                //if it's a service or compute without an id, inject one
                //again this is lame and ugly
                if(     (it.contains(Service.term) && it.contains(Service.scheme))
                    ||  (it.contains(Compute.term) && it.contains(Compute.scheme))){

                    tmpRaw.add injectResourceId(it)
                }
                else
                    tmpRaw.add it

                def parsedAttrs

                try{
                    parsedAttrs = OcciParser.getParser(it).headers()
                }catch (OcciParserException pe){
                    response.sendError HttpURLConnection.HTTP_BAD_REQUEST, pe.toString()
                    return
                }

                if(infrastructureInstanceFactoryService.containsCategory(parsedAttrs, Reservation.term, Reservation.scheme)){
                    validRequest = true
                    if(parsedAttrs[OcciParser.occi_attributes][0] && parsedAttrs[OcciParser.occi_attributes][0][Entity.occi_core_id]?.length() > 0)
                        resvId = parsedAttrs[OcciParser.occi_attributes][0][Entity.occi_core_id]
                }

                if(infrastructureInstanceFactoryService.containsCategory(parsedAttrs, Service.term, Service.scheme)){
                    serviceContent.add parsedAttrs
                }

                //TODO remove literal
                if(infrastructureInstanceFactoryService.containsCategory(parsedAttrs, '', 'http://sla-at-soi.eu/occi/infrastructure/res_template#')){
                    resourceContent.add parsedAttrs
                }
            }

            rawContent = multipartService.createMultipart(tmpRaw, boundary)

            if(validRequest){

                Reservation resv = null
                
                try{
                    resv = reservationService.reserve(rawContent, resourceContent, resvId)
                }catch(IsmHttpException ie){
                    log.error ie.message
                    response.sendError ie.httpErrorCode, ie.message
                }

                if(resv){
                    
                    resv.isMultipart = true
                    resv.save()

                    log.info "Created the reservation ${resv.o_id}"
                    response.status = HttpURLConnection.HTTP_CREATED
                    response.setHeader('Location', CH.config.grails.serverURL + resv.kind.location + resv.o_id)
                    response.setContentType('text/plain')

                    render "ok\r\n"
                }
                else{
                    log.error "Reservation creation failed."
                    response.sendError HttpURLConnection.HTTP_INTERNAL_ERROR, "Reservation creation failed."
                }
            }
            else{
                log.error "Bad Request: No reservation category supplied in request."
                response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "Bad Request: No reservation category supplied in request."
            }
        }
    }

    //releases a reservation
    //      if user does not release and does not commit a reservation the lease with expire and reserved
    //      resources will be returned to the pool
    def doDelete = {

        if(params.instanceId){
            
            log.info "Releasing the reservation ${params.instanceId}"
            try{
                reservationService.release(params.instanceId)
            }catch (IsmHttpException e){
                log.error e.message
                response.sendError e.httpErrorCode, e.message
                return
            }

            response.status = HttpURLConnection.HTTP_OK
            response.setContentType('text/plain')

            render "ok\r\n"
        }
        else{
            log.error "Cannot delete the collection of reservations"
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, "Cannot delete the collection of reservations"
        }
    }
}
