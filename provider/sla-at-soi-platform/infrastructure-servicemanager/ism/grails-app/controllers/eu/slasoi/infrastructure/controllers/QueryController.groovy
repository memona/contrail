/* 
SVN FILE: $Id: QueryController.groovy 2539 2011-07-06 10:19:58Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Intel Performance Learning Solutions Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Intel Performance Learning Solutions Ltd. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Intel Performance Learning Solutions Ltd. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 2539 $
@lastrevision   $Date: 2011-07-06 12:19:58 +0200 (sre, 06 jul 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/ism/grails-app/controllers/eu/slasoi/infrastructure/controllers/QueryController.groovy $

*/

package eu.slasoi.infrastructure.controllers

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

import occi.core.Kind
import occi.core.Mixin

//TODO implement filters
class QueryController {

    static def OCCI_VERSION = 'OCCI/1.1'

    def beforeInterceptor = {
        response.setHeader('Server', OCCI_VERSION)
    }

    def doGet = {

        def cats = []

        cats += Kind.list()
        cats += Mixin.list()

        def disabledList = CH.config.eu.slasoi.infrastructure.resources.disabled +
                CH.config.eu.slasoi.infrastructure.mixins.disabled

        disabledList.each { disabled ->
            log.info "Disabling: ${disabled.value.scheme}${disabled.key}"

            def found = cats.find {(disabled.key == it.term) && (disabled.value.scheme == it.scheme)}

            if(found){
                cats = cats - found
            }
        }

        if (cats.size() > 0) {
            withFormat {
                all {renderTextCategories(cats)}
                text {renderTextCategories(cats)}
                occi {renderHeaderCategories(cats)}
                uriList {renderURIListCategories(cats)}
            }
        }
        else {
            response.sendError HttpURLConnection.HTTP_NOT_FOUND, "No categories were found."
        }
    }

    private def renderHeaderCategories(categories) {

        response.status = HttpURLConnection.HTTP_OK
        response.setContentType("text/occi")
        def headerVals = ''

        categories.each {
            headerVals += it.toTypeString() + ', '
        }

        categories.each{
            it.actions.each{
                headerVals += it.toTypeString() + ', '
            }
        }

        response.setHeader('Category', headerVals.trim()[0..-2])
        render "ok\n"
    }

    private def renderTextCategories(categories) {

        response.setContentType("text/plain")
        response.status = HttpURLConnection.HTTP_OK

        //render types
        categories.each {
            render it.toString() + '\n'
        }

        //render actions
        categories.each {
            it.actions.each{
                render it.toString() + '\n'
            }
        }
    }

    private def renderURIListCategories(categories) {

        response.status = HttpURLConnection.HTTP_OK
        response.setContentType("text/uri-list")

        categories.each {
            render "${it.scheme}${it.term}\n"
        }

        categories.each {
            it.actions.each{
                render "${it.scheme}${it.term}\n"
            }
        }
    }

    def doPost = {
        response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED
        //record tag mixin against a user id
        //category my_tag; scheme="http://localhost:8080/mixins/tag#"; class="mixin"; location="/$USER/my_tag/"
    }

    def doPut = {
        response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED
    }

    def doDelete = {
        response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED
    }
}

/*
//        def filter = request.getHeader('category')
//
//        if(filter?.length() > 0){
//
//            filter = 'Category: ' + filter
//
//            try{
//                filter = OcciParser.getParser(filter).headers()
//            }catch (OcciParserException pe){
//                response.sendError HttpURLConnection.HTTP_BAD_REQUEST, pe.toString()
//                return
//            }
//
//            if(filter[OcciParser.occi_categories][0].size() > 0){
//
//                String term = filter[OcciParser.occi_categories][0]['occi.core.term'][0]
//                String scheme = filter[OcciParser.occi_categories][0]['occi.core.scheme'][0]
//
//                log.info "Filtering: ${scheme}${term}"
//
//                cats = Category.findAllBySchemeAndTerm(scheme, term)
//            }
//        }
//        else{

//            cats += Kind.findAllByIsEnabled(true)
//            cats += Mixin.findAllByIsEnabled(true)

//            CH.config.eu.slasoi.infrastructure.mixins.disabled.each{ disabled_type ->
//                log.info "Disabling: ${disabled_type.value.scheme}${disabled_type.key}"
//
//                cats += Mixin.list().findAll{
//                    it.term != disabled_type.key && it.scheme != disabled_type.value.scheme
//                }
//            }

//            cats += Action.list()
//        }
*/
