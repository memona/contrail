package eu.slasoi.infrastructure.controllers

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

import occi.core.Entity
import occi.core.Mixin
import occi.lexpar.OcciParser
import occi.lexpar.OcciParserException

//TODO filters to be implemented

class MixinController {

    static def OCCI_VERSION = 'OCCI/1.1'
    
    def beforeInterceptor = {
        response.setHeader('Server', OCCI_VERSION)
    }

    def route = { op ->
        op(request.getRequestURI())
    }

    def doGet = {
        route(renderMixins)
    }

    private def renderMixins = { String location ->
        
        def mixins = null

        mixins = Mixin.findAllByLocation(location)

        if(mixins.size() > 0){
            withFormat {
                all{mixinListAsText(mixins)}
                text{mixinListAsText(mixins)}
                occi{mixinListAsOcci(mixins)}
                uriList{mixinListAsUriList(mixins)}
            }
        }
        else{

            mixins = Mixin.findAllByLocationLike(location+'%')
            if(mixins.size() > 0){
                //render ?
                response.status = HttpURLConnection.HTTP_OK
                withFormat {
                    all{mixins.each{render 'X-OCCI-Location: ' + CH.config.grails.serverURL + it.location + "\n"}}
                    text{mixins.each{render 'X-OCCI-Location: ' + CH.config.grails.serverURL + it.location + "\n"}}
                    occi{
                        def header = ''
                        mixins.each{header += CH.config.grails.serverURL + it.location + ', '}
                        response.setHeader('X-OCCI-Location', header[0..-3])
                    }
                    uriList{mixins.each{render CH.config.grails.serverURL + it.location + '\n'}}
                }
            }
            else{
                log.warn "Mixin at ${location} not found."
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "Mixin at ${location} not found."
            }

        }
    }

    private def mixinListAsText(mixins){

        response.contentType = 'text/plain'
        
        mixins.each{ mixin ->
            response.status = HttpURLConnection.HTTP_OK
            if(mixin?.entities?.size() > 0){
                mixin.entities.each {
                    render 'X-OCCI-Location: ' + CH.config.grails.serverURL + it.kind.location + '/' + it.o_id + "\n"
                }
            }
            else{
                log.warn "There are no associated entities with the mixin: ${mixin.scheme}${mixin.term}"
                render ""
            }
        }
    }

    private def mixinListAsOcci(mixins){

        response.contentType = 'text/occi'
        def headerVal = ''

        mixins.each{ mixin ->
            response.status = HttpURLConnection.HTTP_OK
            if(mixin?.entities?.size() > 0){
                mixin.entities.each {
                    headerVal += CH.config.grails.serverURL + it.kind.location + '/' + it.o_id + ", "
                }
                response.addHeader('X-OCCI-Location', headerVal[0..-2])
                render 'ok'
            }
            else{
                log.warn "There are no associated entities with the mixin: ${mixin.scheme}${mixin.term}"
                render "ok"
            }
        }
    }

    private def mixinListAsUriList(mixins){

        response.contentType = 'text/uri-list'

        mixins.each{ mixin ->
            response.status = HttpURLConnection.HTTP_OK
            if(mixin?.entities?.size() > 0){
                mixin.entities.each {
                    render CH.config.grails.serverURL + it.kind.location + '/' + it.o_id + "\n"
                }
            }
            else{
                render ""
                log.warn "There are no associated entities with the mixin: ${mixin.scheme}${mixin.term}"
            }

        }
    }

    def doPost = {
        //add/associate 1 or more resources to a mixin
        //content will be an X-OCCI-Location or more
        //for each resource add the specified mixin
        //if resource doesn't exist 404
        //response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED

        route(addMixin)
    }

    private def addMixin = { String location ->

        def mixin = Mixin.findByLocation(location)

        def content
        try{
            content = getContent()
        }
        catch (OcciParserException pe){
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, pe.toString()
            return
        }

        def resources = []
        def ok = true

        content[OcciParser.occi_locations][0].each { loc ->

            def id = loc.substring(loc.lastIndexOf('/')+1, loc.length())

            def res = Entity.findByO_id(id)

            if(res)
                resources.add res
            else{
                ok = false
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "Resource not found: ${id}. Request failed."
                log.error "Resource not found: ${id}. Request failed."
            }
        }

        if(mixin && ok){
            withFormat {
                all{addMixinAsText(resources, mixin)}
                text{addMixinAsText(resources, mixin)}
                occi{addMixinAsOcci(resources, mixin)}
                uriList{addMixinAsUriList(resources, mixin)}
            }
        }
        else{
            if(!mixin){
                log.warn "Mixin not found."
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "Mixin not found."
            }
            if(!ok){
                log.error "Resource not found. Request failed."
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "Resource not found. Request failed."
            }
        }
    }

    private def addMixinAsText(resources, mixin){

        //TODO adding a mixin may imply an action issued to the backend
        resources.each { res ->
            res.addToMixins mixin
        }
        response.status = HttpURLConnection.HTTP_OK
        response.setContentType('text/plain')
        render "ok"
    }

    private def addMixinAsOcci(resources, mixin){

        //TODO adding a mixin may imply an action issued to the backend
        resources.each { res ->
            res.addToMixins mixin
        }
        response.status = HttpURLConnection.HTTP_OK
        response.setContentType('text/plain')
        render "ok"
    }

    private def addMixinAsUriList(resources, mixin){

        log.warn "Not acceptable content type."
        response.sendError HttpURLConnection.HTTP_NOT_ACCEPTABLE, "Not acceptable content type for this request. text/plain, text/occi are only allowed."
    }
    
    def doPut = {
        response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED
    }
    
    def doDelete = {
        //remove/disassociate 1 or more resources to a mixin
        route(removeMixin)
    }

    private def removeMixin = { location ->

        def mixin = Mixin.findByLocation(location)

        def content
        try{
            content = getContent()
        }
        catch (OcciParserException pe){
            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, pe.toString()
            return
        }

        def resources = []
        def ok = true

        content[OcciParser.occi_locations][0].each { loc ->

            def id = loc.substring(loc.lastIndexOf('/')+1, loc.length())

            def res = Entity.findByO_id(id)

            if(res)
                resources.add res
            else{
                ok = false
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "Resource not found: ${id}. Request failed."
                log.error "Resource not found: ${id}. Request failed."
            }
        }

        if(mixin && ok){
            withFormat {
                all{removeMixinAsTextOrOcci(resources, mixin)}
                text{removeMixinAsTextOrOcci(resources, mixin)}
                occi{removeMixinAsTextOrOcci(resources, mixin)}
                uriList{removeMixinAsUriList(resources, mixin)}
            }
        }
        else{
            if(!mixin){
                log.warn "Mixin not found."
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "Mixin not found."
            }
            if(!ok){
                log.error "Resource not found. Request failed."
                response.sendError HttpURLConnection.HTTP_NOT_FOUND, "Resource not found. Request failed."
            }
        }
    }

    private def removeMixinAsTextOrOcci(resources, mixin){

        //TODO removing a mixin may imply an action issued to the backend
        resources.each {res ->
            res.removeFromMixins mixin
        }
        response.status = HttpURLConnection.HTTP_OK
        response.setContentType('text/plain')
        render "ok"
    }

    private def removeMixinAsUriList(resources, mixin){

        log.warn "Not acceptable content type."
        response.sendError HttpURLConnection.HTTP_NOT_ACCEPTABLE, "Not acceptable content type for this request. text/plain, text/occi are only allowed."
    }

    private def getContent() throws OcciParserException{

        def content = ''
        withFormat {
            all{content = getBodyContent()}
            text{content = getBodyContent()}
            occi{content = getHeaderContent()}
            //uriList{addMixinAsUriList(resources, mixin)}
        }

        return OcciParser.getParser(content).headers()
    }

    private def getBodyContent(){
        return request.reader?.text
    }

    //TODO extract possible attributes
    private def getHeaderContent(){
        return request.getHeader('Category')
    }
}

//        def filter = request.getHeader('category')
//
//        if(filter?.length() > 0){
//            mixins = filterMixins(filter)
//        }
//        else
//    private def filterMixins(filter){
//
//        //account for one or a combination of the supplied following:
//        // a kind
//        // a mixin
//        // an attribute
//        // select from Entity where (kind or mixin) or attribute is equal to X
//
//        filter = 'Category: ' + filter
//
//        try{
//            filter = OcciParser.getParser(filter).headers()
//        }catch (OcciParserException pe){
//            response.sendError HttpURLConnection.HTTP_BAD_REQUEST, pe.toString()
//            return
//        }
//
//        if(filter[OcciParser.occi_categories][0].size() > 0){
//
//            String term = filter[OcciParser.occi_categories][0]['occi.core.term'][0]
//            String scheme = filter[OcciParser.occi_categories][0]['occi.core.scheme'][0]
//
//            log.info "Filtering: ${scheme}${term}"
//
//            def kind = Mixin.findBySchemeAndTerm(scheme, term)
////            computes = Compute.findAllByKind(kind)
//        }
//    }
