package eu.slasoi.infrastructure.controllers

class LinkController {

    //TODO implement me
    static def OCCI_VERSION = 'OCCI/1.1'

    def beforeInterceptor = {
        response.setHeader('Server', OCCI_VERSION)
    }
    
    def doGet = {
        log.warn 'The handler for links is not implemented'
        response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED, 'The handler for links is not implemented'
    }

    def doPost = {

        log.warn 'The handler for links is not implemented'
        response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED, 'The handler for links is not implemented'
    }

    def doPut = {

        log.warn 'The handler for links is not implemented'
        response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED, 'The handler for links is not implemented'
    }

    def doDelete = {

        log.warn 'The handler for links is not implemented'
        response.sendError HttpURLConnection.HTTP_NOT_IMPLEMENTED, 'The handler for links is not implemented'
    }
}
