# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author         Miha Stopar - miha.stopar@xlab.si
# @version        $Rev: 304 $
# @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
# @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/scripts/fabfile.py $

from fabric.api import run, env, sudo

# This file can be used to simplify development process on Tashi. Configure the methods and 
# parameters as needed for your cluster.

env.disable_known_hosts = True
# If you want to have the latest SVN version on the Tashi cluster, put the 
# hosts inside env.hosts list and configure the methods accordingly
env.hosts = ['mihas@localhost', 'mihas@10.100.0.3', 'mihas@10.100.0.4']

def updateSvn():
    """ fab updateSvn command executed inside folder with fabric file will execute svn up on all Tashi
        machine specified in env.hosts
    """
    run('cd /srv/tashi-slasoi/tashi/src;svn up')

def switchTashi(which):
    """ fab switchTashi:slasoi or fab switchTashi:original command executed inside folder with fabric file 
        will switch PYTHONPATH from SLA@SOI version to the original Tashi version or the other way around
    """
    sudo('cd /usr/lib/python2.6/;sudo rm -fr tashi')
    if which == "slasoi":
        print "switching to slasoi tashi"
        sudo('cd /usr/lib/python2.6/; ln -s /srv/tashi-slasoi/tashi/src/tashi tashi')
    else:
        print "switching to original tashi"
        sudo('cd /usr/lib/python2.6/; ln -s /srv/tashi/src/tashi tashi')

