SLA@SOI project is using Apache Tashi project as a cluster management system. You can find more about 
Apache Tashi on http://incubator.apache.org/tashi/.

A short description for Tashi is available on the official web page:
The Tashi project aims to build a software infrastructure for 
cloud computing on massive internet-scale datasets (what we call 
Big Data). The idea is to build a cluster management system 
that enables the Big Data that are stored in a cluster/data center 
to be accessed, shared, manipulated, and computed on by remote users 
in a convenient, efficient, and safe manner.

SLA@SOI project is using a modified Apache Tashi code (for original code see http://svn.apache.org/repos/asf/incubator/tashi). In order to integrate Tashi with the SLA@SOI framework, a message 
sink process that receives messages from the ISM and processes them was 
implemented. This process or daemon runs in the background on the Cluster 
Manager node of a Tashi deployment. It receives provisioning requests as XMPP 
messages, extracts the requirements and using the Tashi client, issues requests 
for VM creation. Besides the provisionig requests, the daemon is also listening to 
the requests for reprovisioning and user registration. The daemon will not return 
a response immediately to the ISM but rather will notify the ISM asynchronously 
once the provisioning process has completed.

Some extensions were made to Tashi to meet the SLA@SOI needs. These extensions are:
  - OCCI XMPP daemon was added which enables remote Tashi controlling.
  - Dynamic adaptation of CPU and memory share for virtual machines.
  - The existing scheduler was rewritten to enable other scheduler implementations. There are three scheduler implementations available now. The first one is PrimitiveScheduler which works the same as original Tashi Primitive scheduler. In addition there are BalancedScheduler and ConsolidatedScheduler where some additional features are implemented (virtual machine priorities, CPU reservation).
  - User registration was implemented, but it is available only in the SQL storage implementation for now.
  - The automatic host registration was added. Now when installing Tashi there is no need to manually add hosts to the database. Just start the nodemanager and the host will be registered automatically if it was not already.
  - Some fixes were applied, for example migrations state exceptions, registering VM instances with already used ID (after restarting cluster manager), getHosts method minor fix in FromConfig.py.


