# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 304 $
# @lastrevision $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/trunk/infrastructure-servicemanager/tashi/src/tashi/agents/priorityscheduler.py $

import sys
import os
import subprocess
from multiprocessing import Process
import random
import time
import json
import traceback
import glob
import socket

from tashi.rpycservices.rpyctypes import *
from tashi import createClient, getConfig
from tashi.resources import *
from tashi.util import instantiateImplementation, getFqdn

def randomMac():
    return ("52:54:00:%2.2x:%2.2x:%2.2x" % (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))

def startScheduler(schedulerImpl, schedulerDelay, rescheduleAfterStart, cpuCoresOverRate, cpuSpeedOverRate, loadExpression, autoMigrations, guaranteedServers, optimalConsolidation, minimalVmMemory, autoPowering):
    subprocess.call("python ../scheduler/schedulermanager.py %s %s %s %s %s %s %s %s %s %s %s" % (schedulerImpl, schedulerDelay, rescheduleAfterStart, cpuCoresOverRate, cpuSpeedOverRate, loadExpression, autoMigrations, guaranteedServers, optimalConsolidation, minimalVmMemory, autoPowering), shell=True)
    
def createInstance(resourceReq, config, prot, user):
                d = DiskConfiguration()                
                
                dfs = instantiateImplementation(config.get("ClusterManager", "dfs"), config)
                image = resourceReq['imageIdentifier']
                imageLocal = dfs.getLocalHandle("images/" + image)
                
                if(os.path.exists(imageLocal)):
                    print "Image exists"    
                    d.uri = image
                else: 
                    print "The supplied image does not exist. Requested image: " + resourceReq['imageIdentifier']
                    prot.send_error("@error requesting the provisioning failed: %s does not exits" % image, endpoint=user)
                    return
                
                if 'diskPersistence' not in resourceReq:
                    d.persistent = False
                else:
                    d.persistent = resourceReq['diskPersistence']

                n = NetworkConfiguration()
                n.mac = randomMac()
                n.network = 1
                
                i = Instance()
                
                if 'userId' not in resourceReq:
                    i.userId = 1000
                else:
                    i.userId = resourceReq['userId'] #should validate this ID
                    
                if 'hostname' not in resourceReq:
                    i.name = resourceReq['resourceId']
                else:
                    i.name = resourceReq['hostname']
                    
                if 'cpu_cores' not in resourceReq:
                    i.cores = 1
                else:
                    i.cores = resourceReq['cpu_cores']
                    
                if 'cpu_speed' not in resourceReq:
                    i.cpuShare = 1024
                else:
                    i.cpuShare = int(resourceReq['cpu_speed'] * 1000) # needed in Hz as integer
                    
                if 'memory_size' not in resourceReq:
                    i.memory = 256
                else:
                    i.memory = resourceReq['memory_size']
                    
                if 'groupName' not in resourceReq:
                    i.groupName = "noGroupName"
                else:
                    i.groupName = resourceReq['groupName']
                    
                if 'downspeed' not in resourceReq:
                    i.downspeed = 256
                else:
                    i.downspeed = resourceReq['downspeed']
                    
                if 'upspeed' not in resourceReq:
                    i.upspeed = 128
                else:
                    i.upspeed = resourceReq['upspeed']
                    
                if 'diskBandwidthWeight' not in resourceReq:
                    i.diskBandwidthWeight = 500
                else:
                    i.diskBandwidthWeight = resourceReq['diskBandwidthWeight']
                
                if 'auditability' not in resourceReq:
                    i.auditability = 0
                else:
                    i.auditability = resourceReq['auditability']
                    
                if 'location' not in resourceReq:
                    i.location = 0
                else:
                    i.location = resourceReq['location']
                    
                if 'sas70' not in resourceReq:
                    i.sas70 = 0
                else:
                    i.sas70 = resourceReq['sas70']
                    
                if 'ccr' not in resourceReq:
                    i.ccr = 0
                else:
                    i.ccr = resourceReq['ccr']
                    
                if 'dataClass' not in resourceReq:
                    i.dataClass = 1
                else:
                    i.dataClass = resourceReq['dataClass']
                    
                if 'hwLevel' not in resourceReq:
                    i.hwLevel = 1
                else:
                    i.hwLevel = resourceReq['hwLevel']
                    
                if 'hwLevel' not in resourceReq:
                    i.diskThroughput = 1
                else:
                    i.diskThroughput = resourceReq['diskThroughput']
                    
                if 'netThroughput' not in resourceReq:
                    i.netThroughput = 1
                else:
                    i.netThroughput = resourceReq['netThroughput']
                    
                if 'dataEncryption' not in resourceReq:
                    i.dataEncryption = 0
                else:
                    i.dataEncryption = resourceReq['dataEncryption']
                    
                if 'isolation' not in resourceReq:
                    i.isolation = 0
                else:
                    i.isolation = resourceReq['isolation']
                    
                if 'dataRetention' not in resourceReq:
                    i.dataRetention = 7
                else:
                    i.dataRetention = resourceReq['dataRetention']
                    
                if 'deleteMethod' not in resourceReq:
                    i.deleteMethod = 1
                else:
                    i.deleteMethod = resourceReq['deleteMethod']
                    
                if 'powerCapping' not in resourceReq:
                    i.powerCapping = 0
                else:
                    i.powerCapping = resourceReq['powerCapping']
                    
                if 'snapshotBackup' not in resourceReq:
                    i.snapshotBackup = 0
                else:
                    i.snapshotBackup = resourceReq['snapshotBackup']
                    
                if 'snapshotRetention' not in resourceReq:
                    i.snapshotRetention = 0
                else:
                    i.snapshotRetention = resourceReq['snapshotRetention']    
                
                i.disks = [d]
                i.nics = [n]
                i.hints = {}
                return i
                
def checkIfVmIsRunning(client, queuedInstance):
    for i in range(0, 3): 
        ins = client.getInstances()
        for i in ins:
            if(i.id == queuedInstance.id): 
                print 'State of the queued instance %s is: %s' % (i.id, i.state)
                if(i.state == InstanceState.Running):
                    print "requested VM %s is created and is now running" % i.name
                    return i
                else:
                    if(i.state == InstanceState.Held or i.state == InstanceState.Pending):
                        print "The virtual machine " + str(queuedInstance.id) + " is being held/pending"
                        print "The likely cause for this is that there are not enough resources to service the request"
                        print "Requested VM still not created"
                        time.sleep(10)
    return False
    
def parseIdFromMessage(resourceReq, client, fqdn_identifier):
    vmId = None
    if 'id' in resourceReq:
        vmId = int(resourceReq['id'])
    elif(fqdn_identifier in resourceReq):
        instances = client.getInstances()
        fqdnReq = resourceReq[fqdn_identifier]
        for i in instances:
            fqdn = getFqdn(i.name)
            if(fqdn == fqdnReq):
                vmId = i.id
                break
    return vmId