#!/bin/bash

#Runs a xmpp-occi bridge in daemon mode

#LOG_DIR=/var/log
LOG_DIR=.

if [ -r ../bin ]; then
	twistd -l $LOG_DIR/tashi-occi-xmpp.log -y ../src/tashi/occixmpp/tashi_occi.py
else
	twistd -l $LOG_DIR/tashi-occi-xmpp.log -y ./tashi_occi.py
fi