# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 685 $
# @lastrevision $Date: 2011-02-11 13:37:41 +0100 (Fri, 11 Feb 2011) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/infrastructure-servicemanager/tashi/src/tashi/resources.py $  

import sys
import xmpp
import subprocess
from xml.dom.minidom import Document
from tashi.util import createClient, getIsoTime, getFqdn
import socket
from tashi import vmStates
from tashi.rpycservices.rpyctypes import *
import time, threading

class XmppSensor(object):
	def __init__(self, config):
		self.config = config
		self.xmpp_enabled = config.get("XMPP", "enabled")
		jid = config.get("XMPP", "jid")
		self.pwd = config.get("XMPP", "pwd")
		self.recipient = config.get("XMPP", "recipient")
		self.jid = xmpp.protocol.JID(jid)
		self.cl = xmpp.Client(self.jid.getDomain(), debug=[])
		self.canConnect = self.cl.connect() != "" and self.cl.auth(self.jid.getNode(), self.pwd) != None
		self.cl.sendInitPresence()
		# this is to prevent xmpp send error which starts to appear sometimes
		threading.Thread(target=self.maintainConnection).start()

	def publish(self, message):
		if self.xmpp_enabled and self.canConnect:
			try:
				self.cl.send(xmpp.protocol.Message(self.recipient, message))
			except:
				print "XMPP message failed to be sent: %s" % sys.exc_info()[0]
				
	def publishAction(self, text, userId, id):
		doc = Document()
		message = doc.createElement("Message")
		message.setAttribute("type", "auditRecord")
		message.setAttribute("userId", str(userId))
		message.setAttribute("id", str(id))
		message.setAttribute("time", getIsoTime())
		doc.appendChild(message)
		action = doc.createElement("Action")
		textNode = doc.createTextNode(text)
		action.appendChild(textNode)	
		message.appendChild(action)
		
		prettyxml = doc.toprettyxml()
		print prettyxml
		self.publish(prettyxml)
	
	def publishVmLayout(self):
		prettyxml = self.createVmLayout()
		self.publish(prettyxml)
		
	def getClusterConfig(self):
		client = createClient(self.config)
		hosts = client.getHosts()
		doc = Document()
		message = doc.createElement("Message")
		message.setAttribute("type", "ClusterConfiguration")
		message.setAttribute("time", getIsoTime())
		doc.appendChild(message)
		clusterConf = doc.createElement("ClusterConfiguration")
		message.appendChild(clusterConf)
		domain = ".".join(socket.getfqdn().split('.')[1:])
		clusterConf.setAttribute("fqdn", "tashi." + domain)
		for h in hosts:
			hostEl = doc.createElement("Host")
			hostEl.setAttribute("id", str(h.id))
			hostEl.setAttribute("fqdn", getFqdn(h.name))
			location = doc.createElement("Location")
			code = doc.createElement("CountryCode")
			textNode = doc.createTextNode((h.location).upper())
			code.appendChild(textNode)
			location.appendChild(code)
			hostEl.appendChild(location)
			
			auditability = doc.createElement("Auditability")
			textNode = doc.createTextNode(str(bool(h.auditability)))
			auditability.appendChild(textNode)
			hostEl.appendChild(auditability)
			
			sas70 = doc.createElement("SAS70")
			textNode = doc.createTextNode(str(bool(h.sas70)))
			sas70.appendChild(textNode)
			hostEl.appendChild(sas70)
			
			ccr = doc.createElement("CCR")
			textNode = doc.createTextNode(str(bool(h.ccr)))
			ccr.appendChild(textNode)
			hostEl.appendChild(ccr)
			
			value = ""
			dataclass = doc.createElement("DataClassification")
			for i in dir(DataClassification):
				if getattr(DataClassification, i) == h.dataClass:
					value = i
					break
			textNode = doc.createTextNode(value)
			dataclass.appendChild(textNode)
			hostEl.appendChild(dataclass)
			
			hwlevel = doc.createElement("HWRedundancyLevel")
			for i in dir(HardwareRedundancyLevel):
				if getattr(HardwareRedundancyLevel, i) == h.hwLevel:
					value = i
					break
			textNode = doc.createTextNode(value)
			hwlevel.appendChild(textNode)
			hostEl.appendChild(hwlevel)
			
			disk = doc.createElement("DiskThroughput")
			for i in dir(DiskThroughputLevel):
				if getattr(DiskThroughputLevel, i) == h.diskThroughput:
					value = i
					break
			textNode = doc.createTextNode(value)
			disk.appendChild(textNode)
			hostEl.appendChild(disk)
			
			net = doc.createElement("NetThroughput")
			for i in dir(NetThroughputLevel):
				if getattr(NetThroughputLevel, i) == h.netThroughput:
					value = i
					break
			textNode = doc.createTextNode(value)
			net.appendChild(textNode)
			hostEl.appendChild(net)
			
			dataencryption = doc.createElement("DataEncryption")
			textNode = doc.createTextNode(str(bool(h.dataEncryption)))
			dataencryption.appendChild(textNode)
			hostEl.appendChild(dataencryption)
			
			clusterConf.appendChild(hostEl)
		prettyxml = doc.toprettyxml()
		print prettyxml

		return prettyxml
		
	def createVmLayout(self):
		client = createClient(self.config)
		_hosts = client.getHosts()
		_instances = client.getInstances()
		hosts = {}
		for h in _hosts:
			h.usedMemory = 0
			h.usedCores = 0
			h.usedCpu = 0
			hosts[h.id] = h
		for i in _instances:
			if (i.hostId in hosts):
				hosts[i.hostId].usedMemory += i.memory
				hosts[i.hostId].usedCores += i.cores
				hosts[i.hostId].usedCpu += i.cpuShare
				
		doc = Document()
		message = doc.createElement("Message")
		message.setAttribute("type", "VmLayout")
		message.setAttribute("time", getIsoTime())
		doc.appendChild(message)
		entities = doc.createElement("NetworkEntities")
		
		for h in hosts.keys():
			host = hosts[h]
			hostEl = doc.createElement("NetworkEntity")
			hostEl.setAttribute("type", "host")
			hostEl.setAttribute("name", host.name)
			hostEl.setAttribute("fqdn", getFqdn(host.name))
			hostEl.setAttribute("up", str(host.up))
			hostEl.setAttribute("poweredOn", str(host.poweredOn))
			hostEl.setAttribute("memory", str(host.memory))
			hostEl.setAttribute("cpu_cores", str(host.cores))
			hostEl.setAttribute("cpu_speed", str(host.cpuSpeed))
			hostEl.setAttribute("used_cpu_speed", str(host.usedCpu))
			hostEl.setAttribute("used_cpu_cores", str(host.usedCores))
			hostEl.setAttribute("used_memory", str(host.usedMemory))
			
			filesystem = self.config.get("Vfs", "prefix")
			df = subprocess.Popen(["df", filesystem], stdout=subprocess.PIPE)
			output = df.communicate()[0]
			device, size, used, available, percent, mountpoint = output.split("\n")[1].split()
			hostEl.setAttribute("disk_used", str(used))
			hostEl.setAttribute("disk_available", str(available))
			
			for inst in _instances:
				if (inst.hostId == host.id):
					instEl = doc.createElement("NetworkEntity")
					instEl.setAttribute("type", "vm")
					instEl.setAttribute("name", inst.name)
					instEl.setAttribute("fqdn", getFqdn(inst.name))
					instEl.setAttribute("state", vmStates[inst.state])
					instEl.setAttribute("userId", str(inst.userId))
					disks = []
					for disk in inst.disks:
						d = {}
						d["uri"] = disk.uri
						if disk.persistent:
							d["persistent"] = "True"
						else:
							d["persistent"] = "False"
						disks.append(d)
					instEl.setAttribute("disks", str(disks))
					instEl.setAttribute("memory", str(inst.memory))
					instEl.setAttribute("cpu_speed", str(inst.cpuShare))
					instEl.setAttribute("cpu_cores", str(inst.cores))
					
					hostEl.appendChild(instEl)		
					
			entities.appendChild(hostEl)
		message.appendChild(entities)
		
		prettyxml = doc.toprettyxml()
		return prettyxml

	def publishList(self, messages):
		for message in messages:
			self.publish(message)
	
	def maintainConnection(self):
		while True:
			time.sleep(30)
			self.cl.sendPresence()

			
