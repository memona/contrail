# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License. 

# This file contains modifications by XLAB d.o.o. (Copyright (c) 
# 2008-2010) as part of work in the SLA@SOI FP7 project (www.sla-at-soi.eu). 
# Details of the license (BSD 3 clause) can be found in the SLA-SOI-LICENSE 
# file.

# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 1551 $
# @lastrevision $Date: 2011-05-03 15:18:06 +0200 (tor, 03 maj 2011) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/src/tashi/clustermanager/data/sql.py $ 

import logging
import threading
import time
import types
from tashi.rpycservices.rpyctypes import *
from tashi.clustermanager.data.datainterface import DataInterface
from tashi.util import stringPartition, boolean

class SQL(DataInterface):
	def __init__(self, config):
		DataInterface.__init__(self, config)
		self.uri = self.config.get("SQL", "uri")
		self.log = logging.getLogger(__name__)
		if (self.uri.startswith("sqlite://")):
			import sqlite
			self.conn = sqlite.connect(self.uri[9:], autocommit=1)
		elif (self.uri.startswith("mysql://")):
			import MySQLdb
			uri = self.uri[8:]
			(user, _, hostdb) = stringPartition(uri, '@')
			(host, _, db) = stringPartition(hostdb, '/')
			self.password = self.config.get('SQL', 'password')
			self.conn = MySQLdb.connect(host=host, user=user, passwd=self.password, db=db)
		else:
			raise ValueException, 'Unknown SQL uri: %s' % (self.uri)
		
		self.instanceOrder = ['id', 'vmId', 'hostId', 'decayed', 'state', 'userId', 'name', 'cores',\
							'cpuShare', 'memory', 'disks', 'nics', 'hints', 'groupName', 'downspeed',\
							'upspeed', 'diskBandwidthWeight',\
							'auditability', 'location', 'sas70', 'ccr', 'dataClass', 'hwLevel',\
							'diskThroughput', 'netThroughput', 'isolation', 'dataRetention',\
							'deleteMethod', 'powerCapping', 'snapshotBackup', 'snapshotRetention']
		self.hostOrder = ['id', 'name', 'poweredOn', 'up', 'decayed', 'state', 'memory', 'cores',\
						 'cpuSpeed', 'version', 'auditability', 'location', 'sas70', 'ccr',\
						  'dataClass', 'hwLevel', 'diskThroughput', 'netThroughput', 'dataEncryption']
		self.userOrder = ['id', 'name', 'passwd', 'priority']
		self.instanceLock = threading.Lock()
		self.instanceIdLock = threading.Lock()
		self.instanceLocks = {}
		self.hostLock = threading.Lock()
		self.hostLocks = {}
		self.userLock = threading.Lock()
		self.idLock = threading.Lock()
		self.sqlLock = threading.Lock()
		self.verifyStructure()
		
		cur = self.executeStatement("SELECT * from sinstances")
		res = cur.fetchall()
		maxId = 0
		for r in res:
			if r[0] > maxId:
				maxId = r[0]
		self.maxInstanceId = maxId + 1

	def executeStatement(self, stmt):
		self.sqlLock.acquire()
		try:
			cur = self.conn.cursor()
			try:
				cur.execute(stmt)
			except:
				self.log.exception('Exception executing SQL statement')
		finally:
			self.sqlLock.release()
		return cur
		
	def getNewInstanceId(self):
		self.instanceIdLock.acquire()
		instanceId = self.maxInstanceId
		self.maxInstanceId = self.maxInstanceId + 1
		self.instanceIdLock.release()
		return instanceId
	
	def verifyStructure(self):
		self.executeStatement("CREATE TABLE IF NOT EXISTS sinstances (id int(11) NOT NULL, vmId int(11), hostId int(11), decayed tinyint(1) NOT NULL, state int(11) NOT NULL, userId int(11), name varchar(256), cores int(11) NOT NULL, cpuShare int(11) NOT NULL, memory int(11) NOT NULL, disks varchar(1024) NOT NULL, nics varchar(1024) NOT NULL, hints varchar(1024) NOT NULL, groupName varchar(1024), downspeed int(11), upspeed int(11), diskBandwidthWeight int(11), auditability tinyint(1) DEFAULT 1, location varchar(256), sas70 tinyint(1) DEFAULT 0, ccr tinyint(1) DEFAULT 0, dataClass int(11), hwLevel int(11), diskThroughput int(11), netThroughput int(11), isolation tinyint(1) DEFAULT 0, dataRetention int(11), deleteMethod tinyint(1) DEFAULT 1, powerCapping tinyint(1) DEFAULT 0, snapshotBackup tinyint(1) DEFAULT 0, snapshotRetention int(11))")
		self.executeStatement("CREATE TABLE IF NOT EXISTS shosts (id INTEGER PRIMARY KEY, name varchar(256) NOT NULL, poweredOn tinyint(1) DEFAULT 0, up tinyint(1) DEFAULT 0, decayed tinyint(1) DEFAULT 0, state int(11) DEFAULT 1, memory int(11), cores int(11), cpuSpeed int(11), version varchar(256), auditability tinyint(1) DEFAULT 1, location varchar(256) NOT NULL, sas70 tinyint(1) DEFAULT 0, ccr tinyint(1) DEFAULT 0, dataClass int(11) NOT NULL, hwLevel int(11) NOT NULL, diskThroughput int(11) NOT NULL, netThroughput int(11) NOT NULL, dataEncryption tinyint(1) DEFAULT 0)")
		self.executeStatement("CREATE TABLE IF NOT EXISTS snetworks (id int(11) NOT NULL, name varchar(256) NOT NULL)")
		self.executeStatement("CREATE TABLE IF NOT EXISTS susers (id int(11) NOT NULL, name varchar(256) NOT NULL, passwd varchar(256), priority int(11) NOT NULL)")
	
	def sanitizeForSql(self, s):
		if (s == '"True"'):
			return '"1"'
		if (s == '"False"'):
			return '"0"'
		if (s == '"None"'):
			return 'NULL'
		return s
	
	def makeInstanceList(self, i):
		l = []
		for e in range(0, len(self.instanceOrder)):
			l.append(i.__dict__[self.instanceOrder[e]])
		return map(lambda x: self.sanitizeForSql('"' + str(x) + '"'), l)
	
	def makeListInstance(self, l):
		i = Instance()
		for e in range(0, len(self.instanceOrder)):
			i.__dict__[self.instanceOrder[e]] = l[e]
		i.state = int(i.state)
		i.decayed = boolean(i.decayed)
		i.disks = map(lambda x: DiskConfiguration(d=x), eval(i.disks))
		i.nics = map(lambda x: NetworkConfiguration(d=x), eval(i.nics))
		i.hints = eval(i.hints)
		return i
	
	def makeHostList(self, h):
		l = []
		for e in range(0, len(self.hostOrder)):
			l.append(h.__dict__[self.hostOrder[e]])
		return map(lambda x: self.sanitizeForSql('"' + str(x) + '"'), l)
	
	def makeListHost(self, l):
		h = Host()
		for e in range(0, len(self.hostOrder)):
			h.__dict__[self.hostOrder[e]] = l[e]
		h.state = int(h.state)
		return h
	
	def registerInstance(self, instance):
		self.instanceLock.acquire()
		try:
			if (instance.id is not None and instance.id not in self.getInstances()):
				self.instanceIdLock.acquire()
				if (instance.id >= self.maxInstanceId):
					self.maxInstanceId = instance.id + 1
				self.instanceIdLock.release()
			else:
				instance.id = self.getNewInstanceId()
			instance._lock = threading.Lock()
			self.instanceLocks[instance.id] = instance._lock
			instance._lock.acquire()
			l = self.makeInstanceList(instance)
			self.executeStatement("INSERT INTO sinstances VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" % tuple(l))
		finally:
			self.instanceLock.release()
		return instance
	
	def acquireInstance(self, instanceId):
		self.instanceLock.acquire()
		try:
			cur = self.executeStatement("SELECT * from sinstances WHERE id = %d" % (instanceId))
			l = cur.fetchone()
			if (not l):
				raise TashiException(d={'errno':Errors.NoSuchInstanceId,'msg':"No such instanceId - %d" % (instanceId)})
			instance = self.makeListInstance(l)
			self.instanceLocks[instance.id] = self.instanceLocks.get(instance.id, threading.Lock())
			instance._lock = self.instanceLocks[instance.id]
			instance._lock.acquire()
		finally:
			self.instanceLock.release()
		return instance
	
	def releaseInstance(self, instance):
		self.instanceLock.acquire()
		try:
			l = self.makeInstanceList(instance)
			s = ""
			for e in range(0, len(self.instanceOrder)):
				s = s + self.instanceOrder[e] + "=" + l[e]
				if (e < len(self.instanceOrder)-1):
					s = s + ", "
			self.executeStatement("UPDATE sinstances SET %s WHERE id = %d" % (s, instance.id))
			instance._lock.release()
		finally:
			self.instanceLock.release()
	
	def removeInstance(self, instance):
		self.instanceLock.acquire()
		try:
			self.executeStatement("DELETE FROM sinstances WHERE id = %d" % (instance.id))
			instance._lock.release()
			del self.instanceLocks[instance.id]
		finally:
			self.instanceLock.release()
	
	def acquireHost(self, hostId):
		host = self.getHost(hostId)
		self.hostLock.acquire()
		self.hostLocks[host.id] = self.hostLocks.get(host.id, threading.Lock())
		self.hostLock.release()
		host._lock = self.hostLocks[host.id]
		host._lock.acquire()
		return host
	
	def releaseHost(self, host):
		l = self.makeHostList(host)
		s = ""
		for e in range(0, len(self.hostOrder)):
			s = s + self.hostOrder[e] + "=" + l[e]
			if (e < len(self.hostOrder)-1):
				s = s + ", "
		self.executeStatement("UPDATE shosts SET %s WHERE id = %d" % (s, host.id))
		host._lock.release()
	
	def getHosts(self):
		cur = self.executeStatement("SELECT * FROM shosts")
		res = cur.fetchall()
		hosts = {}
		for r in res:
			host = self.makeListHost(r)
			hosts[host.id] = host
		return hosts
	
	def getHost(self, id):
		cur = self.executeStatement("SELECT * FROM shosts WHERE id = %d" % (id))
		r = cur.fetchone()
		if (r == None):
			raise TashiException(d={'errno':Errors.NoSuchHostId,'msg':"No such hostId - %s" % (id)})
		host = self.makeListHost(r)
		return host
	
	def getInstances(self):
		cur = self.executeStatement("SELECT * FROM sinstances")
		res = cur.fetchall()
		instances = {}
		for r in res:
			instance = self.makeListInstance(r)
			instances[instance.id] = instance
		return instances
	
	def getInstance(self, id):
		cur = self.executeStatement("SELECT * FROM sinstances WHERE id = %d" % (id))
		r = cur.fetchone()
		if (not r):
			raise TashiException(d={'errno':Errors.NoSuchInstanceId, 'msg':"No such instanceId - %d" % (id)})
		instance = self.makeListInstance(r)
		return instance
	
	def getNetworks(self):
		cur = self.executeStatement("SELECT * FROM snetworks")
		res = cur.fetchall()
		networks = {}
		for r in res:
			network = Network(d={'id':r[0], 'name':r[1]})
			networks[network.id] = network
		return networks
	
	def getNetwork(self, id):
		cur = self.executeStatement("SELECT * FROM snetworks WHERE id = %d" % (id))
		r = cur.fetchone()
		network = Network(d={'id':r[0], 'name':r[1]})
		return network
	
	def getUsers(self):
		cur = self.executeStatement("SELECT * from susers")
		res = cur.fetchall()
		users = {}
		for r in res:
			user = User(d={'id':r[0], 'name':r[1], 'passwd':r[2], 'priority': r[3]})
			users[user.id] = user
		return users
	
	def getUser(self, id):
		cur = self.executeStatement("SELECT * FROM susers WHERE id = %d" % (id))
		r = cur.fetchone()
		user = User(d={'id':r[0], 'name':r[1], 'passwd':r[2], 'priority' : r[3]})
		return user
	
	def makeUserList(self, user):
		l = []
		for e in range(0, len(self.userOrder)):
			l.append(user.__dict__[self.userOrder[e]])
		return map(lambda x: self.sanitizeForSql('"' + str(x) + '"'), l)
	
	def registerUser(self, name, passwd, priority):
		self.userLock.acquire()
		id = self.getNewId("susers")
		user = User(d={'id': id, 'name': name, 'passwd': passwd, 'priority' : priority})
		l = self.makeUserList(user)
		self.executeStatement("INSERT INTO susers VALUES (%s, %s, %s, %s)" % tuple(l))
		self.userLock.release()
		return id
	
	def unregisterUser(self, userId):
		self.userLock.acquire()
		cur = self.executeStatement("SELECT * from susers")
		res = cur.fetchall()
		for r in res:
			if r[0] == userId:
				self.executeStatement("DELETE FROM susers WHERE id = %d" % userId)
		self.userLock.release()

	def registerHost(self, hostname, memory, cores, cpuSpeed, version):
		self.hostLock.acquire()
		cur = self.executeStatement("SELECT * from shosts")
		res = cur.fetchall()
		for r in res:
			if r[1] == hostname:
				id = r[0]
				print "Host %s already registered, it will be updated now" % id
				s = ""
				host = Host(d={'id': id, 'poweredOn':1, 'up': 0, 'decayed': 0, 'state': 1, 'name': hostname, 'memory':memory, 'cores': cores, 'cpuSpeed':cpuSpeed, 'version':version, 'auditability': 1, 'location':IsoCountryCode.Slovenia, 'sas70':0, 'ccr':0, 'dataClass':DataClassification.Public, 'hwLevel':HardwareRedundancyLevel.Standard, 'diskThroughput': DiskThroughputLevel.Standard, 'netThroughput':NetThroughputLevel.Standard, 'dataEncryption': 0})
				l = self.makeHostList(host)
				for e in range(0, len(self.hostOrder)):
					s = s + self.hostOrder[e] + "=" + l[e]
					if (e < len(self.hostOrder)-1):
						s = s + ", "
				self.executeStatement("UPDATE shosts SET %s WHERE id = %d" % (s, id))
				self.hostLock.release()
				return r[0], True
		id = self.getNewId("shosts")
		host = Host(d={'id': id, 'poweredOn': 1, 'up': 0, 'decayed': 0, 'state': 1, 'name': hostname, 'memory':memory, 'cores': cores, 'cpuSpeed':cpuSpeed, 'version':version, 'auditability': 1, 'location':IsoCountryCode.Slovenia, 'sas70':0, 'ccr':0, 'dataClass':DataClassification.Public, 'hwLevel':HardwareRedundancyLevel.Standard, 'diskThroughput': DiskThroughputLevel.Standard, 'netThroughput':NetThroughputLevel.Standard, 'dataEncryption': 0})
		l = self.makeHostList(host) 
		self.executeStatement("INSERT INTO shosts VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" % tuple(l))
		self.hostLock.release()
		return id, False
	
	def unregisterHost(self, hostId):
		self.hostLock.acquire()
		cur = self.executeStatement("SELECT * from shosts")
		res = cur.fetchall()
		for r in res:
			if r[0] == hostId:
				self.executeStatement("DELETE FROM shosts WHERE id = %d" % hostId)
		self.hostLock.release()

	def getNewId(self, table):
		""" Generates id for a new object. For example for hosts and users.  
		"""
		self.idLock.acquire()
		cur = self.executeStatement("SELECT * from %s" % table)
		res = cur.fetchall()
		maxId = 0 # the first id would be 1
		l = []
		for r in res:
			id = r[0]
			l.append(id)
			if id >= maxId:
				maxId = id
		l.sort() # sort to enable comparing with range output
		# check if some id is released:
		t = range(maxId + 1)
		t.remove(0)
		if l != t and l != []:
			releasedIds = filter(lambda x : x not in l, t)
			self.idLock.release()
			return releasedIds[0]
		else:
			self.idLock.release()
			return maxId + 1