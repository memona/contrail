from redisstore import RedisStore, RedisInstance, RedisUtils
from redis import Redis
from tashi.rpycservices.rpyctypes import *
import unittest
#import mox

#TODO: integrate facilities of Mox with redis
class RedisStoreTests(unittest.TestCase):

	def setUp(self):
		# Create an instance of Mox
		# self.redisConnectionMock = mox.Mox()
		self.conn = Redis('slug')
		#clears the redis store each time a test is run
		self.conn.flushall()
		self.rs = RedisStore(None)

	# ----- Utility methods -----

	def tashiInstance(self):
		
		d = DiskConfiguration()
		d.uri = 'tashi23.img'
		d.persistent = False
		
		n = NetworkConfiguration()
		n.mac = '12:12:12:12:12:12'
		n.network = 1
		
		i = Instance()
		i.userId = 1000
		i.name = 'tizzy'
		i.cores = 1
		i.cpuShare = 1024
		i.memory = 256
		i.disks = [d]
		i.nics = [n]
		i.hints = {}
		return i
	
	def tashiHost(self):
		h = Host()
		h.up = 1
		h.decayed = 0
		h.state = 2
		h.notes = 'kickass'
		h.reserved = []
		h.name = 'fatty'
		h.memory = 64000
		h.cores = 16
		h.cpuSpeed = 34000
		h.version = 'some weird tashi version'
		h.poweredOn = 1
		h.auditability = 1
		h.location = IsoCountryCode.Slovenia
		h.sas70 = 0
		h.ccr = 0
		h.dataClass = DataClassification.Public
		h.hwLevel = HardwareRedundancyLevel.Standard
		h.diskThroughput = DiskThroughputLevel.Standard
		h.netThroughput = NetThroughputLevel.Standard
		h.dataEncryption = 0
		return h
	
	def tashiNetwork(self):
		n = Network()
		n.name = 'DMZ'
		return n
	
	def tashiUser(self):
		u = User()
		u.name = 'andy'
		u.passwd = 'iAmNotTellingYou'
		u.priority = 5
		return u
	
	# ----- RedisInstance tests -----
	
	def testRedisInstanceCreate(self):
		#mock this
			
		ri = RedisInstance(self.tashiInstance(), self.conn, 'be.edmonds.Instance')
		self.assertTrue(RedisInstance.CLASS != '')
		self.assertTrue(isinstance(ri.instance, Instance))
		ri.save()
		
		ri = RedisInstance(self.tashiHost(), self.conn, 'be.edmonds.Host')
		self.assertTrue(RedisInstance.CLASS != '')
		self.assertTrue(isinstance(ri.instance, Host))
		ri.save()
		
		ri = RedisInstance(self.tashiUser(), self.conn, 'be.edmonds.User')
		self.assertTrue(RedisInstance.CLASS != '')
		self.assertTrue(isinstance(ri.instance, User))
		ri.save()
		
		ri = RedisInstance(self.tashiNetwork(), self.conn, 'be.edmonds.Network')
		self.assertTrue(RedisInstance.CLASS != '')
		self.assertTrue(isinstance(ri.instance, Network))
		ri.save()
	
	def testRedisInstanceUpdate(self):
		
		ri = RedisInstance(self.tashiInstance(), self.conn, 'be.edmonds.Instance')
		ri.save()
		oldCS = ri.instance.cpuShare
		ri.instance.cpuShare = 2048
		
		#TODO should 'get' the instance before updating so we're working with the DB version
		
		ri.save()
		self.assertTrue(ri.instance.cpuShare != oldCS)
		self.assertTrue(ri.instance.cpuShare == 2048)
	
	def testRedisInstanceDelete(self):
		ri = RedisInstance(self.tashiInstance(), self.conn, 'be.edmonds.Instance')
		ri.save()
		ri.delete()
	
	def testRedisInstanceGetAll(self):
		ri = RedisInstance(self.tashiInstance(), self.conn, 'be.edmonds.Instance')
		self.assertTrue(RedisInstance.CLASS != '')
		self.assertTrue(isinstance(ri.instance, Instance))
		ri.save()
		
		instances = RedisUtils.get(self.conn, 'be.edmonds.Instance')
		print instances
		self.assertTrue(len(instances) > 0)

	# ----- RedisStore Tashi Host tests -----
	
	def testRedisStoreRegisterUnregisterHost(self):
		host = self.tashiHost()
		hostId, bool = self.rs.registerHost(host.name, host.memory, host.cores, host.cpuSpeed, host.version)
		self.assertEquals(hostId, 1)
		
		self.rs.unregisterHost(hostId)
		host = self.rs.getHost(id=hostId)
		self.assertEquals(host, None)
	
	def testRedisStoreAcquireReleaseHost(self):
		host = self.tashiHost()
		hostId, bool = self.rs.registerHost(host.name, host.memory, host.cores, host.cpuSpeed, host.version)
		
		self.assertEqual(hostId, 1)
		
		host = self.rs.acquireHost(hostId)
		self.assertFalse(host._lock.acquire(0))
		
		self.rs.releaseHost(host)
		self.assertTrue(host._lock.acquire(0))
		#clean up
		host._lock.release()
	
	def testRedisStoreGetAllHosts(self):
		host = self.tashiHost()
		self.rs.registerHost(host.name, host.memory, host.cores, host.cpuSpeed, host.version)
		self.rs.registerHost('a.crappy.one', host.memory, host.cores, host.cpuSpeed, host.version)
		hostList = self.rs.getHosts()
		self.assertEquals (len(hostList), 2)

	def testRedisStoreGetHost(self):
		host = self.tashiHost()
		hostId, bool = self.rs.registerHost(host.name, host.memory, host.cores, host.cpuSpeed, host.version)
		
		self.assertEqual(hostId, 1)
		
		saved_host = self.rs.getHost(id=hostId)
		
		self.assertEquals(host.name, saved_host.name)
		self.assertEquals(host.memory, saved_host.memory)
		self.assertEquals(host.cores, saved_host.cores)
		self.assertEquals(host.cpuSpeed, saved_host.cpuSpeed)
		self.assertEquals(host.version, saved_host.version)
	
	def testRedisStoreUpdateHost(self):
		host = self.tashiHost()
		hostId, bool = self.rs.registerHost(host.name, host.memory, host.cores, host.cpuSpeed, host.version)
		self.assertEqual(hostId, 1)
		
		i = self.rs.acquireHost(hostId)
		
		i.cores = 16
		i.memory = 32000
		
		self.rs.releaseHost(i)
		
		t_i = self.rs.getHost(i.id)
		
		self.assertEquals(t_i.cores, 16)
		self.assertEquals(t_i.memory, 32000)
	
	# ----- RedisStore Tashi Instance tests -----
	
	def testRedisStoreRegisterRemoveInstance(self):
		instance = self.tashiInstance()
		saved_instance = self.rs.registerInstance(instance)
		
		self.assertEquals(saved_instance.id, 1)
		
		self.rs.removeInstance(saved_instance)
		
		no_instance = self.rs.getInstance(id=saved_instance.id)
		self.assertFalse(no_instance)
	
	def testRedisStoreAcquireReleaseInstance(self):
		instance = self.tashiInstance()
		saved_instance = self.rs.registerInstance(instance)
		self.assertEqual(saved_instance.id, 1)
		
		#the instance will be locked on return
		self.assertFalse(saved_instance._lock.acquire(0))
		
		#we have to release as the instance that comes back is locked
		self.rs.releaseInstance(saved_instance)
		self.assertTrue(saved_instance._lock.acquire(0))
		#cleanup
		saved_instance._lock.release()
		
		instance = self.rs.acquireInstance(saved_instance.id)
		#instance is locked on return, request for lock fails
		self.assertFalse(saved_instance._lock.acquire(0))
		
		self.rs.releaseInstance(instance)
		self.assertTrue(saved_instance._lock.acquire(0))
		#cleanup
		saved_instance._lock.release()
	
	def testRedisStoreGetAllInstances(self):
		self.rs.registerInstance(self.tashiInstance())
		self.rs.registerInstance(self.tashiInstance())
		
		insList = self.rs.getInstances()
		self.assertEquals (len(insList), 2)

	def testRedisStoreGetInstance(self):
		s_i = self.tashiInstance()
		s_i = self.rs.registerInstance(s_i)
		self.assertEqual(s_i.id, 1)
		
		i = self.rs.getInstance(s_i.id)
		
		self.assertEquals(s_i.userId, i.userId)
		self.assertEquals(s_i.name, i.name)
		self.assertEquals(s_i.cores, i.cores)
		self.assertEquals(s_i.cpuShare, i.cpuShare)
		self.assertEquals(s_i.memory, i.memory)
		
		self.assertEquals(s_i.disks[0].uri, i.disks[0].uri)
		self.assertEquals(s_i.disks[0].persistent, i.disks[0].persistent)
		
		self.assertEquals(s_i.nics[0].ip, i.nics[0].ip)
		self.assertEquals(s_i.nics[0].mac, i.nics[0].mac)
		self.assertEquals(s_i.nics[0].network, i.nics[0].network)
	
	def testRedisStoreUpdateInstance(self):
		s_i = self.tashiInstance()
		s_i = self.rs.registerInstance(s_i)
		self.assertEqual(s_i.id, 1)
		self.rs.releaseInstance(s_i)
		
		i = self.rs.acquireInstance(s_i.id)
		
		i.cores = 4
		i.memory = 2048
		
		self.rs.releaseInstance(i)
		
		t_i = self.rs.getInstance(i.id)
		
		self.assertEquals(t_i.cores, 4)
		self.assertEquals(t_i.memory, 2048)

	# ----- RedisStore Tashi User tests -----

	def testRedisStoreRegisterUser(self):
		id = self.rs.registerUser('bob', 'passwd', 4)
		self.assertEquals(id, 1)
	
	def testRedisStoreUnregisterUser(self):
		id = self.rs.registerUser('bob', 'passwd', 4)
		self.assertEquals(id, 1)
		
		self.rs.unregisterUser(id)
		
		user = self.rs.getUser(id)
		self.assertFalse(user)

	def testRedisStoreGetAllUsers(self):
		self.rs.registerUser('bob', 'passwd', 4)
		self.rs.registerUser('bob', 'passwd', 4)
		
		users = self.rs.getUsers()
		self.assertEquals(2, len(users))

	def testRedisStoreGetUser(self):
		id = self.rs.registerUser('bob', 'passwd', 4)
		self.assertEquals(id, 1)
		
		user = self.rs.getUser(id)
		self.assertTrue(user)
		
		self.assertEquals('bob', user.name)
		self.assertEquals('passwd', user.passwd)
		self.assertEquals(4, user.priority)

	# ----- RedisStore Tashi Network tests -----

	def testRedisStoreGetAllNetworks(self):
		#TODO
		pass

	def testRedisStoreGetNetwork(self):
		#TODO
		pass	

if __name__ == '__main__':
	unittest.main()
	
	#suiteFew = unittest.TestSuite()
	#suiteFew.addTest(RedisStoreTests("testCreate"))
	#unittest.TextTestRunner(verbosity=2).run(suiteFew)