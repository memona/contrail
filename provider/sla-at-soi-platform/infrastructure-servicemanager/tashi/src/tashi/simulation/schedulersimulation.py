#!/usr/bin/env python

# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
# 
#   http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License. 

# This file contains modifications by XLAB d.o.o. (Copyright (c) 
# 2008-2010) as part of work in the SLA@SOI FP7 project (www.sla-at-soi.eu). 
# Details of the license (BSD 3 clause) can be found in the SLA-SOI-LICENSE 
# file.

# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 409 $
# @lastrevision $Date: 2011-01-05 18:17:52 +0100 (Wed, 05 Jan 2011) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/trunk/infrastructure-servicemanager/tashi/src/tashi/agents/scheduler.py $

import time
import sys
import logging.config
import ConfigParser

from tashi.rpycservices.rpyctypes import *
from tashi.util import instantiateImplementation, boolean, scrubString
from clientsimulation import Client
from tashi.scheduler.schedulerconf import SchedulerConf
import tashi
from tashi.scheduler.xmpphandler import XmppHandler

class SchedulerManager(object):
    def __init__(self, config, client, scheduler, schedulerConf):
        self.client = client
        self.scheduler = scheduler
        self.hooks = []
        f = "%(asctime)s - %(levelname)s - %(message)s"
        logging.basicConfig(format=f,level=logging.INFO)
        self.log = logging.getLogger(__file__)
        handler = XmppHandler(config)
        self.log.addHandler(handler)
        self.scheduleDelay = schedulerConf.scheduleDelay
        self.rescheduleAfterStart = schedulerConf.rescheduleAfterStart
        self.autoMigrations = schedulerConf.autoMigrations
        self.alreadyCheckedRequests = []
        items = config.items("Scheduler")
        items.sort()
        for item in items:
            (name, value) = item
            name = name.lower()
            if (name.startswith("hook")):
                try:
                    self.hooks.append(instantiateImplementation(value, config, client, False))
                except:
                    self.log.exception("Failed to load hook %s" % (value))
   
    def start(self):
        oldInstances = {}
        freedHostId = -1 
        
        if(self.rescheduleAfterStart):
            hosts = {}
            load = {}
            instances = {}
            self.refreshObjects(hosts, load, instances)
            self.scheduler.reschedule(instances, hosts, load)
            # execute all migrations that are needed for rescheduling before going further:
            while True:
                furtherMigrations = self.scheduler.handleMigration(instances)
                if not furtherMigrations:
                    break
        while True:
            try:
                hosts = {}
                load = {}
                instances = {}
                self.refreshObjects(hosts, load, instances)
                # Check for VMs that have exited
                for i in oldInstances:
                    if (i not in instances and oldInstances[i].state != InstanceState.Pending):
                        for hook in self.hooks:
                            hook.postDestroy(oldInstances[i])
                            freedHostId = oldInstances[i].hostId  
                            msg = "Vm %s exited on host %s." % (oldInstances[i].id, freedHostId)
                            self.log.info(msg)
                            # try to restore adapted VMs first:
                            self.scheduler.checkAdaptedVms(hosts, load)
                            # get updated instances, could be that checkAdaptedVms resumed some of them...
                            self.refreshObjects(hosts, load, instances)
                            if self.autoMigrations:
                                self.scheduler.setVmsForMigration(hosts, instances, freedHostId, load)
                            # clear alreadyCheckedRequests to enable new checking if the provisioning is possible:
                            self.alreadyCheckedRequests = []
                          
                furtherMigrations = False  
                if self.autoMigrations:
                    furtherMigrations = self.scheduler.handleMigration(instances)
                    self.refreshObjects(hosts, load, instances)
                        
                # Schedule new VMs
                oldInstances = instances
                if (len(load.get(None, [])) > 0):
                    # requests with highest priority first:
                    sortedReqs = sorted(load[None], key=lambda k: self.scheduler.getVmPriority(instances[k].userId), reverse=True)
                    for i in sortedReqs:
                        if i in self.alreadyCheckedRequests:
                            continue
                        inst = instances[i]
                        try:
                            self.log.info("#################### new provisioning request - memory: %s, CPU speed: %s, CPU cores: %s" % (inst.memory, inst.cpuShare, inst.cores))
                            minMaxHost = None
                            targetHost = None
                            try:
                                targetHost = inst.hints.get("targetHost", None)
                            except:
                                self.log.info("No target host specified for this instance")
                            try:
                                allowElsewhere = boolean(inst.hints.get("allowElsewhere", "False"))
                            except Exception, e:
                                allowElsewhere = False

                            minMaxHost = self.scheduler.determineHost(hosts, load, instances, inst, targetHost, allowElsewhere)
                            
                            if minMaxHost:
                                self.log.info("#################### determineHost returned host id: %s" % minMaxHost.id)
                            else:
                                self.log.info("#################### determineHost: no id returned")
                                # check over-provisioning if guaranteed servers are on:
                                # guaranteed servers are NOT ON at the stage only if there 
                                # was not an appropriate host found for powering on
                                # inside determineHost
                                if self.scheduler.guaranteedServersOn(hosts):
                                    minMaxHost = self.scheduler.getMostSuitableHost(hosts, load, instances, inst)
                                    if minMaxHost == None:
                                        self.log.info("#################### getMostSuitableHost: no id returned")
                                        self.log.info("Instance cannot be provisioned also using CPU over-provisioning")
                                    else:
                                        self.log.info("#################### getMostSuitableHost returned host id: %s" % minMaxHost.id)
                            
                            if (not minMaxHost):
                                self.log.info("Failed to find a suitable place to schedule %s" % (inst.name))
                                newHost = None
                                if self.scheduler.guaranteedServersOn(hosts):
                                    # also over-provisioning was not successful - try to
                                    # power on additional server
                                    newHost = self.scheduler.powerOnAppropriateHost(hosts, inst, instances, load)
                                    if newHost:
                                        minMaxHost = newHost
                                if not minMaxHost:
                                    isFreedUp = self.scheduler.freeUpResources(inst, instances, hosts, load)
                                    if isFreedUp:
                                        self.log.info("#################### freeUpResources: some resources were freed up - VM %s should be provisioned in the next step" % inst.id)
                                        self.refreshObjects(hosts, load, instances)
                                        minMaxHost = self.scheduler.getMostSuitableHost(hosts, load, instances, inst)
                                        if (minMaxHost):
                                            self.log.info("#################### getMostSuitableHost (second run) returned host id: %s" % minMaxHost.id)
                                        else:
                                            self.log.info("#################### getMostSuitableHost (second run): no id returned")
                                    else:
                                        self.log.info("#################### freeUpResources: no resources were freed up - there are no enough VMs with lower priority to enable the provisioning of the VM %s" % inst.id)
                            if (minMaxHost):
                                if (not inst.hints.get("__resume_source", None)):
                                    for hook in self.hooks:
                                        hook.preCreate(inst)
                                self.log.info("Scheduling instance %s (%d mem, %d cores, %d uid) on host %s" % (inst.name, inst.memory, inst.cores, inst.userId, minMaxHost.name))    
                                self.client.activateVm(i, minMaxHost)
                                load[minMaxHost.id] = load[minMaxHost.id] + [i]
                            else:
                                self.alreadyCheckedRequests.append(inst.id)
                                self.log.info("Failed to schedule or activate %s" % (inst.name))
                        except Exception, e:
                            self.alreadyCheckedRequests.append(inst.id)
                            self.log.exception("Failed to schedule or activate %s" % (inst.name))
                # check if machines can get back memory that were freed up - only
                # if there are no pending provisioning requests 
                if len(load.get(None, [])) == 0 and not furtherMigrations:
                    # check if some vm is still in migration process:
                    self.refreshObjects(hosts, load, instances)
                    migrating = False
                    for i in instances.itervalues():
                        if i.state == InstanceState.MigratePrep or i.state == InstanceState.MigrateTrans:
                            migrating = True
                            break
                    if not migrating:
                        self.scheduler.powerOffHosts(hosts, load)
                time.sleep(self.scheduleDelay)
            except TashiException, e:
                self.log.exception("Tashi exception")
                time.sleep(self.scheduleDelay)
            except Exception, e:
                self.log.exception("General exception")
                time.sleep(self.scheduleDelay)
                
    def refreshObjects(self, hosts, load, instances):
        hosts.clear()
        instances.clear()
        load.clear()
        _instances = {}
        for h in self.client.getHosts():
            hosts[h.id] = h
            load[h.id] = []
            load[None] = []
            _instances = self.client.getInstances()
        for i in _instances:
            instances[i.id] = i
        for i in instances.itervalues():
            if (i.hostId or i.state == InstanceState.Pending):
                load[i.hostId] = load[i.hostId] + [i.id]

def main():
    config = ConfigParser.ConfigParser()
    config.read("Simulation.cfg")
    client = Client(config)
    
    schedulerConf = SchedulerConf() 
    # use command line arguments for scheduler configuration, if there are no command line
    # arguments, use config file:
    if len(sys.argv) > 1:
        schedulerConf.setFromArgv(sys.argv)
    else:
        schedulerConf.setFromConfig(config)

    scheduler = instantiateImplementation(schedulerConf.schedulerImpl, client, config, schedulerConf)
    logging.basicConfig(level=logging.DEBUG)

    agent = SchedulerManager(config, client, scheduler, schedulerConf)
    agent.start()

if __name__ == "__main__":
    main()
