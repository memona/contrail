# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev$
# @lastrevision $Date$ 
# @filesource $URL$

import xmpp
import logging
import ConfigParser
import sys
import time, threading
from xml.dom.minidom import Document
from tashi.util import getIsoTime

class XmppHandler(logging.Handler):
    def __init__(self, config):
        logging.Handler.__init__(self)
        f = "%(message)s"
        formatter = logging.Formatter(f)
        self.setFormatter(formatter)
        
        self.xmpp_enabled = config.get("XMPP", "enabled")
        jid = config.get("XMPP", "scheduler-jid")
        self.pwd = config.get("XMPP", "scheduler-pwd")
        self.recipient = config.get("XMPP", "scheduler-recipient")
        self.jid = xmpp.protocol.JID(jid)
        domain = self.jid.getDomain()
        self.cl = xmpp.Client(domain,debug=[])
        connect = self.cl.connect() != ""
        node = self.jid.getNode()
        auth = self.cl.auth(node, self.pwd) != None
        self.canConnect = connect and auth
        self.cl.sendInitPresence()
        # this is to prevent xmpp send error which starts to appear sometimes
        threading.Thread(target=self.maintainConnection).start()

    def emit(self, record):
        if self.canConnect:
            try:
                msg = self.format(record)
                doc = Document()
                message = doc.createElement("Message")
                message.setAttribute("type", "scheduler")
                message.setAttribute("time", getIsoTime())
                doc.appendChild(message)
                action = doc.createElement("Action")
                textNode = doc.createTextNode(msg)
                action.appendChild(textNode)    
                message.appendChild(action)
        
                prettyxml = doc.toprettyxml()
                print prettyxml
                self.cl.send(xmpp.protocol.Message(self.recipient, prettyxml))
            except:
                print "XMPP message failed to be sent: %s" % sys.exc_info()[0]
        
    def maintainConnection(self):
        while True:
            time.sleep(30)
            self.cl.sendPresence()

                
    