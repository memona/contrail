# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author Miha Stopar - miha.stopar@xlab.si
# @version $Rev: 304 $
# @lastrevision $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
# @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/trunk/infrastructure-servicemanager/tashi/src/tashi/agents/priorityscheduler.py $

import ConfigParser
import sys
import xmpp
import time 
import logging

from tashi.rpycservices.rpyctypes import HostState, InstanceState
from schedulercommon import SchedulerCommon
from tashi.util import boolean

class BalancedScheduler(SchedulerCommon):
    """
    When VMs are provisioned, they are spread evenly across the number of available hosts. 
    Hosts with highest efficiency rating are filled first. VMs priorities come into account 
    when an unsuccessful provisioning request happens - in this case 
    and if provisioning request is of a higher priority - resources are taken from the VMs 
    with the lowest priority to provide resources for a request. When some resources are released 
    the VMs with the highest priority are migrated to better hosts if possible. When rescheduling 
    is executed the scheduler tries to balance VMs distribution across the servers. In general
    too many migrations are avoided, because migrations besides other effects also require 
    two VMs instead of one (source and destination VM) for the time of migration.
    """
    def __init__(self, client, config, schedulerConf):
        SchedulerCommon.__init__(self, client, config, schedulerConf)

    def determineHost(self, hosts, load, instances, inst, targetHost, allowElsewhere):
        """
        This function determines the host with the highest efficiency rating between hosts
        with no VMs provisioned. It powers on an appropriate host if the guaranteed servers 
        are not powered on. If no host with no VMs exists, the host with the smallest 
        load / usage is chosen.
        """
        self.vmPriorities[inst.id] = self.getVmPriority(inst.userId)
        minMaxHost = None
        if (targetHost != None):
            minMaxHost = self.checkTargetHost(hosts, load, instances, inst, targetHost)
        if ((targetHost == None or allowElsewhere) and minMaxHost == None):
            availableHost = -1
            # get hosts with the highest efficiency first:
            sHosts = sorted(self.serversEfficiency.keys(), key=lambda k: self.serversEfficiency[k], reverse=True)
            for hsId in sHosts:
                h = hosts[hsId]
                if len(load[h.id]) == 0:
                    if self.checkHost(h, inst, instances, load) and not self.guaranteedServersOn(hosts) and h.poweredOn == False:
                        self.powerOnHost(h)
                        if h.poweredOn == True: # could be False if autoPowering was disabled
                            availableHost = h.id
                            break 
            if availableHost == -1:
                hostsUsage = {}
                for h in hosts.values():
                    if (h.poweredOn == True and h.up == True and h.state == HostState.Normal):
                        memUsage, coreUsage, cpuUsage = self.getUsageByMetric(inst, instances, load, h.id)
                        hostsUsage[h.id] = self.getLoad(memUsage, coreUsage, cpuUsage) 
                # hosts with the smallest load / usage will be checked first:
                for hId in sorted(hostsUsage.keys(), key=lambda k: hostsUsage[k]):
                    h = hosts[hId]
                    if (self.isHostAppropriate(inst, instances, h, load, False) and len(h.reserved) == 0):
                        msg = "Vm can be provisioned on a host id (sorted by efficiency rating): %s" % h.id
                        self.log.info(msg)
                        availableHost = h.id
                        break 
            if availableHost > -1:
                hostId = availableHost        
                self.log.info("Instance %s can be provisioned on host %s" % (inst.id, hostId))
                minMaxHost = hosts[hostId]
            else: # find the host with the smallest over-provisioning rate
                self.log.info("Without over-provisioning there are no resources available for provisioning instance %s" % inst.id)
        return minMaxHost
    
    def reschedule(self, instances, hosts, load):
        """
        This function checks if distribution of instances across servers complies to the
        Balanced scheduler type and triggers migrations if needed to comply to this scheduler 
        type. Be aware that this does not take into account VM priorities, because for this
        too many migrations would be needed in some cases.
        """
        hostsUsage = {}
        for h in hosts.values():
            if (h.poweredOn == True and h.up == True and h.state == HostState.Normal):
                memUsage = reduce(lambda x, y: x + instances[y].memory, load[h.id], 0)
                coreUsage = reduce(lambda x, y: x + instances[y].cores, load[h.id], 0)
                cpuUsage = reduce(lambda x, y: x + instances[y].cpuShare, load[h.id], 0)
                hostsUsage[h.id] = self.getLoad(memUsage, coreUsage, cpuUsage)
        if len(hostsUsage) == 0:
            return
        poweredOnHosts = len(hostsUsage)
        eHosts = sorted(self.serversEfficiency.keys(), key=lambda k: self.serversEfficiency[k], reverse=True)
        for hId in eHosts:
            # power on some hosts to have the guaranteed number of servers available (
            # if there are instances to be balanced across the hosts):
            if poweredOnHosts >= self.guaranteedServers and poweredOnHosts >= len(instances):
                break
            host = hosts[hId]
            if host.poweredOn == False:
                poweredOn = self.powerOnHost(host)
                if poweredOn:
                    poweredOnHosts += 1
                    memUsage = reduce(lambda x, y: x + instances[y].memory, load[host.id], 0)
                    coreUsage = reduce(lambda x, y: x + instances[y].cores, load[host.id], 0)
                    cpuUsage = reduce(lambda x, y: x + instances[y].cpuShare, load[host.id], 0)
                    hostsUsage[host.id] = self.getLoad(memUsage, coreUsage, cpuUsage)
                    
        averageHostUsage = reduce(lambda x, y: x + hostsUsage[y], hostsUsage, 0) / len(hostsUsage)
                
        # hosts with the heaviest load / usage will be attempted to be released first
        # the loop aims to move the VMs from the heavy used hosts to the hosts with the smallest load
        for hId in sorted(hostsUsage.keys(), key=lambda k: hostsUsage[k], reverse = True):
            if hostsUsage[hId] >= averageHostUsage:
                # hosts with the smallest load first:
                sortedHosts = sorted(hostsUsage.keys(), key=lambda k: hostsUsage[k])                
                index = 0
                maxInstances = 0
                for hostId in sortedHosts:
                    if len(load[hostId]) > maxInstances:
                        maxInstances = len(load[hostId])
                while True:
                    hostTarget = sortedHosts[index % len(sortedHosts)] 
                    index += 1
                    if index > maxInstances * len(sortedHosts): # could be improved
                        break
                    if hostsUsage[hostTarget] > averageHostUsage or hostTarget == hId:
                        continue
                    for iId in load[hId]:
                        i = instances[iId]
                        if (hostsUsage[hostTarget] + self.getLoad(i.memory, i.cores, i.cpuShare) <= averageHostUsage and i.id not in map(lambda x: x[0], self.toMigrate)):
                            self.log.info("Instance %s can be moved to host %s" % (i.id, hostTarget))
                            self.toMigrate.append([i.id, hostTarget])
                            hostsUsage[hostTarget] += self.getLoad(i.memory, i.cores, i.cpuShare)
                            break # move only one instance, then jump and try to move next instance on next host
                    