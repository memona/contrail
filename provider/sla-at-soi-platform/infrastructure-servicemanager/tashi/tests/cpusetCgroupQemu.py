# Copyright (c) 2008-2010, XLAB d.o.o.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of SLASOI nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @author         Miha Stopar - miha.stopar@xlab.si
# @version        $Rev: 304 $
# @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
# @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-servicemanager/tashi/tests/cpusetCgroupQemu.py $

import os
import subprocess
import sys
from multiprocessing import Process, Queue

def main():
    q = Queue()
    p = Process(target = qemuParentProcess, args=(q,), name = "parentQemuProcess")
    p.start()
    
def qemuParentProcess(q):
    pid = os.getpid()
    if not os.path.exists("/dev/cpu/tasks"):
        print "mounting Cgroups CPU..."
        subprocess.call("sudo mkdir -p /dev/cpu", shell=True)
        subprocess.call("mount -t cgroup -ocpu cpu /dev/cpu", shell=True)
    if not os.path.exists("/dev/memory/tasks"):
        print "mounting Cgroups memory..."
        subprocess.call("sudo mkdir -p /dev/memory", shell=True)
        subprocess.call("mount -t cgroup none /dev/memory -o memory", shell=True)
    # create CPU Cgroup for this virtual machine:
    subprocess.call("sudo mkdir -p /dev/cpu/test%s" % pid, shell=True)    
    print "CPU Cgroup test%s created" % pid
    # specify CPU share for this group:
    subprocess.call("echo 1024 | sudo tee /dev/cpu/test%s/cpu.shares" % pid, shell=True)
    subprocess.call("echo %s | sudo tee /dev/cpu/test%s/tasks" % (pid, pid), shell=True)

    # create memory Cgroup for this virtual machine
    subprocess.call("sudo mkdir -p /dev/memory/test%s" % pid, shell=True)    
    print "Memory Cgroup test%s created" % pid

    if not os.path.exists("/dev/cpuset/tasks"):
        print "mounting Cgroups cpuset..."
        subprocess.call("sudo mkdir -p /dev/cpuset", shell=True)
        subprocess.call("mount -t cpuset cpuset /dev/cpuset", shell=True)
    # create CPU Cgroup for this virtual machine:
    subprocess.call("sudo mkdir -p /dev/cpuset/test%s" % pid, shell=True)    
    print "cpuset Cgroup test%s created" % pid
    subprocess.call("echo 0 | sudo tee /dev/cpuset/test%s/cpus" % pid, shell=True)
    subprocess.call("echo 0 | sudo tee /dev/cpuset/test%s/mems" % pid, shell=True)
    subprocess.call("echo %s | sudo tee /dev/cpuset/test%s/tasks" % (pid, pid), shell=True)

    p = Process(target = qemuProcess, name = "qemuProcess")
    p.start()
    q.put(p.pid)
        
def qemuProcess():
    print 'pppp:', os.getppid()
    print 'ppp:', os.getpid()
    strCmd = "/usr/bin/qemu -smp 2 /home/mihas/qemu-miha/debian1.img"
    cmd = strCmd.split()
    #cmd = ["/usr/bin/qemu", "/home/mihas/qemu-miha/debian1.img"]
    os.execl("/usr/bin/qemu", *cmd)
    
def pollVMsLoop(self):
    """Infinite loop that checks for dead VMs"""
    while True:
        try:
            time.sleep(self.POLL_DELAY)
            matchSystemPids(self.controlledVMs)
        except:
            log.exception("Exception in poolVMsLoop")
            
def matchSystemPids():
    pass

if __name__ == "__main__":
    main()
