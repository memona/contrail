- forkAndSubprocessQemu.py: Forks a process, start a new process and start a qemu process inside it. Cgroup was not applied properly on qemu process in this case. 

- subprocessAndSubprocess.py: Similar as forkAndSubprocessQemu, only that python multiprocessing package is used also in the first step (it is easier to pass data from child process to parent). Cgroups work ok in this case.

- forkAndSubprocess.py: Forks a process, start a new process and start a qemu process inside it. Original process children printed out.

- memoryCgroupQemu.py: Similar to subprocessAndSubprocess.py, but also memory Cgroup added.

- waitpidCall.py: testing waitpid calls and "No child processes error"
- cpusetCgroupQemu.py: Similar to subprocessAndSubprocess.py, but also cpuset Cgroup added.

- subprocessAndExeclQemu.py: Finally realized that Cgroups can be implemented without additional process between node manager and VM process.

- send_broadcast.py, receive_broadcast.py: Simple scripts for testing VLANs.

- memstress.py: Simple memory stress test.