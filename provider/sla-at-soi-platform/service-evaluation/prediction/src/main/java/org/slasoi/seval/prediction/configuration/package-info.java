/**
* Package with configuration data objects to
* describe adjustments that should be performed
* on the prediction model.
*
* @author Benjamin Klatt
* @author Franz Brosch
*/
package org.slasoi.seval.prediction.configuration;
