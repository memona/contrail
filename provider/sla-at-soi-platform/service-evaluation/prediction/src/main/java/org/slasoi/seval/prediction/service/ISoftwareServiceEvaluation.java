package org.slasoi.seval.prediction.service;

/**
 * <p>
 * Common interface for model-based design-time evaluation of software services.
 * </p>
 * <p>
 * For a detailed description of the main interface method, see
 * {@link ISoftwareServiceEvaluation#evaluate(Set, SLATemplate)}
 * </p>
 * 
 * @author kuester
 */
public interface ISoftwareServiceEvaluation extends IServiceEvaluation {

    /**
     * Retrieves the endpoint where the evaluation server is expected to run.
     * 
     * @return the evaluation server endpoint.
     */
    String getEvaluationServerEndpoint();

    /**
     * Sets the endpoint of the evaluation web server.
     * 
     * @param evaluationServerEndpoint
     *            the endpoint
     */
    void setEvaluationServerEndpoint(String evaluationServerEndpoint);

    /**
     * Retrieves the evaluation mode of the service evaluator.
     * 
     * @return the evaluation mode
     */
    EvaluationMode getEvaluationMode();

    /**
     * Sets the evaluation mode of the service evaluator.
     * 
     * @param evaluationMode
     *            the evaluation mode
     */
    void setEvaluationMode(EvaluationMode evaluationMode);
}
