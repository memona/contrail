package org.slasoi.seval.prediction.service.settings.impl;

import java.util.LinkedList;
import java.util.List;

import org.slasoi.seval.prediction.service.settings.EvaluationSettings;

/**
 * Settings object for an evaluation run. This data object encapsulates general settings for an evaluation run.
 * 
 * @author Benjamin Klatt
 * 
 */
public class EvaluationSettingsImpl implements EvaluationSettings {

    // ////////////////////////////////
    // ATTRIBUTES
    // ////////////////////////////////

    /** The default setting for the maximum simulation time. */
    private static final int DEFAULT_MAX_SIMULATION_TIME = 1000;

    /** The default setting for the maximum measurement count. */
    private static final int DEFAULT_MAX_MEASUREMENT_COUNT = 15000;

    /** Flag if the evaluation should consider responsibility effects or not. */
    private boolean analyzeResponsibility = false;

    /**
     * Determines the value of the maximum measurements count constraint.
     */
    private long maxMeasurementsCount = DEFAULT_MAX_MEASUREMENT_COUNT;

    /**
     * Determines the value of the maximum simulation time constraint.
     */
    private long maxSimTime = DEFAULT_MAX_SIMULATION_TIME;

    /**
     * Indicates if logging shall be verbose during simulation.
     */
    private boolean verboseLogging = false;

    /** List of sensors to be returned. */
    private List<String> sensorNames = new LinkedList<String>();

    // ////////////////////////////////
    // GETTERS / SETTERS
    // ////////////////////////////////

    /**
     * {@inheritDoc}
     */
    public void setAnalyzeResponsibility(final boolean analyzeResponsibility) {
        this.analyzeResponsibility = analyzeResponsibility;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isAnalyzeResponsibility() {
        return analyzeResponsibility;
    }

    /**
     * Retrieves the maximum measurements count constraint.
     * 
     * @return the maximum measurements count constraint
     */
    public long getMaxMeasurementsCount() {
        return maxMeasurementsCount;
    }

    /**
     * Sets the maximum measurements count constraint.
     * 
     * @param maxMeasurementsCount
     *            the maximum measurements count constraint
     */
    public void setMaxMeasurementsCount(final long maxMeasurementsCount) {
        this.maxMeasurementsCount = maxMeasurementsCount;
    }

    /**
     * Retrieves the maximum simulation time constraint.
     * 
     * @return the maximum simulation time constraint
     */
    public long getMaxSimTime() {
        return maxSimTime;
    }

    /**
     * Sets the maximum simulation time constraint.
     * 
     * @param maxSimTime
     *            the maximum simulation time constraint
     */
    public void setMaxSimTime(final long maxSimTime) {
        this.maxSimTime = maxSimTime;
    }

    /**
     * Retrieves the value of the verboseLogging option.
     * 
     * @return the value of verboseLogging
     */
    public boolean isVerboseLogging() {
        return verboseLogging;
    }

    /**
     * Sets the value of the verboseLogging option.
     * 
     * @param verboseLogging
     *            the value of verboseLogging
     */
    public void setVerboseLogging(final boolean verboseLogging) {
        this.verboseLogging = verboseLogging;
    }

    /**
     * Get the list of sensor names to be returned by the prediction.
     * 
     * Getting the list of sensors and adding new ones to the list [getSensorNames().add("sensorxy")] enables the
     * prediction to return only results for the sensors specified. If no sensors have been defined, predictions results
     * for the external system calls are returned, which are the results of the service calls in the usage scenario.
     * 
     * For specified sensors, the complete sensor name returned by the prediction is also returned to identify the
     * result (serviceId). If the default usage scenario calls are returned, the serviceId represents the name of the
     * service call which is optimized for human readability.
     * 
     * @return The list of sensor names to be returned.
     */
    public List<String> getSensorNames() {
        return sensorNames;
    }
}
