package org.slasoi.seval.prediction.configuration;

import java.util.List;

/**
 * The specification of a usage scenario. This specification includes parameters
 * that should be used in the evaluation such as workloads or usage parameters.
 *
 * @author Benjamin Klatt
 *
 */
public interface UsageScenarioSpecification {

    /**
     * Get the name of the usage scenario to execute.
     *
     * @return the usageScenario
     */
    String getUsageScenarioID();

    /**
     * Set the name of the usage scenario to execute.
     *
     * @param usageScenarioID
     *            the usageScenario to set
     */
    void setUsageScenarioID(String usageScenarioID);

    /**
     * Get the arrival time describing the rate of new incoming requests.
     *
     * @return the interArrivalTime
     */
    String getInterArrivalTime();

    /**
     * Set the arrival time describing the rate of new incoming requests.
     *
     * @param interArrivalTime
     *            the interArrivalTime to set
     */
    void setInterArrivalTime(String interArrivalTime);

    /**
     * Set the system calls to modify.
     *
     * @param systemCalls
     *            The list of system calls.
     */
    void setSystemCalls(List<SystemCall> systemCalls);

    /**
     * Get the list of system calls to modify.
     *
     * @return The list of system calls to be modified.
     */
    List<SystemCall> getSystemCalls();

    /**
     * Add a single system call to the usage scenario specification.
     *
     * @param systemCall
     *            The system call specification to add.
     */
    void addSystemCall(SystemCall systemCall);

}
