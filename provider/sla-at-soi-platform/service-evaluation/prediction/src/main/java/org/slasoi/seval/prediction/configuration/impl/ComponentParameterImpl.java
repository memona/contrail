package org.slasoi.seval.prediction.configuration.impl;

import org.slasoi.seval.prediction.configuration.ComponentParameter;

/**
 * The specification of a component parameter. This identifies the parameter by it's name and specifies the value to be
 * set.
 * 
 * @author Benjamin Klatt
 * 
 */
public class ComponentParameterImpl implements ComponentParameter {

    // ////////////////////////////////
    // ATTRIBUTES
    // ////////////////////////////////

    /** The name for the system call. */
    private String name = null;

    /** The value of the system call. */
    private String value = null;

    // ////////////////////////////////
    // Constructor
    // ////////////////////////////////

    /**
     * The default constructor.
     */
    public ComponentParameterImpl() {
        super();
    }

    /**
     * The enhanced constructor to set the name and value of the parameter.
     * 
     * @param name
     *            The name of the parameter.
     * @param value
     *            The value of the parameter.
     */
    public ComponentParameterImpl(final String name, final String value) {
        this.name = name;
        this.value = value;
    }

    // ////////////////////////////////
    // GETTERS / SETTERS
    // ////////////////////////////////

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.impl.ComponentParameter#getName()
     */
    public final String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.impl.ComponentParameter#setName(java.lang.String)
     */
    public final void setName(final String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.impl.ComponentParameter#getValue()
     */
    public final String getValue() {
        return value;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.configuration.impl.ComponentParameter#setValue(java.lang.String)
     */
    public final void setValue(final String value) {
        this.value = value;
    }

}
