package org.slasoi.seval.prediction.service.impl;

import org.slasoi.seval.prediction.service.ISingleResult.AggregationType;
import org.slasoi.seval.repository.SoftwareModel;
import org.slasoi.slamodel.vocab.common;

/**
 * Mockup-Implementation for offline use of ServiceEvaluation.
 * 
 * @author kuester
 * 
 */
public class EvaluationResultMock extends EvaluationResult {

    /** The median mock up value. */
    private static final double SAMPLE_VALUE_MEDIAN = 1.593405;
    /** The max mock up value. */
    private static final double SAMPLE_VALUE_MAX = 3.11355;
    /** The min mock up value. */
    private static final double SAMPLE_VALUE_MIN = 0.18315;
    /** The mean mock up value. */
    private static final double SAMPLE_VALUE_MEAN = 1.496046316;

    private static final String ORC_DISTRIBUTED = "ORC_Distributed";
    /**
     * The constructor.
     * Initializes a set of sample mock-up results.
     */
    public EvaluationResultMock(SoftwareModel model) 
    {
    	String[] services = {"ORCInventoryService/bookSale", "ORCInventoryService/getProductDetails", "ORCPaymentService/PaymentServiceOperation"};

    	double k = 1.0;
    	if (ORC_DISTRIBUTED.equalsIgnoreCase(model.getModelId()))
    	{
    		k = 0.6;
    	}
    	
    	for (String service: services)
    	{
            addResult(new SingleResult(service, common.completion_time, AggregationType.MEAN, SAMPLE_VALUE_MEAN*k));
            addResult(new SingleResult(service, common.completion_time, AggregationType.MIN, SAMPLE_VALUE_MIN*k));
            addResult(new SingleResult(service, common.completion_time, AggregationType.MAX, SAMPLE_VALUE_MAX*k));
            addResult(new SingleResult(service, common.completion_time, AggregationType.MEDIAN, SAMPLE_VALUE_MEDIAN*k));	
    	}    	
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.slasoi.seval.prediction.service.impl.EvaluationResult#toString()
     */
    public final String toString() {
        return "[*** SERVICE EVALUTION MOCKUP RESULT ***]\n" + super.toString();
    }
}
