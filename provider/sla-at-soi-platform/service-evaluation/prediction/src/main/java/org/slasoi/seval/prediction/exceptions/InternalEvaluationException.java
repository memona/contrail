package org.slasoi.seval.prediction.exceptions;

import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.SimuServiceResultStatus;

/**
 * Indicates an unwanted condition as a result of a service evaluation.
 * 
 * @author brosch
 * 
 */
public class InternalEvaluationException extends RuntimeException {

    /**
     * A serial version UID for the serializable class.
     */
    private static final long serialVersionUID = -7501312972460729583L;

    /**
     * The simulation result status.
     */
    private SimuServiceResultStatus status;

    /**
     * The constructor.
     * 
     * @param status
     *            the simulation results.
     */
    public InternalEvaluationException(final SimuServiceResultStatus status) {
        this.status = status;
    }

    /**
     * Retrieves the simulation result status.
     * 
     * @return the simulation result status
     */
    public final SimuServiceResultStatus getStatus() {
        return status;
    }
}
