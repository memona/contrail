package org.slasoi.seval.prediction.configuration;

/**
 * This class identifies parameters within a system call of a the usage scenario
 * in the usage sub-model of the prediction model to be adjustment.
 *
 * @author Benjamin Klatt
 *
 */
public interface SystemCallVariable {

    /**
     * Get the name of the system call.
     *
     * @return The name of the system call parameter.
     */
    String getName();

    /**
     * Set the name of the system call to set.
     *
     * @param name
     *            The name to set.
     */
    void setName(String name);

    /**
     * Get the value of the system call.
     *
     * @return The value of the system call parameter.
     */
    String getValue();

    /**
     * Set the value of the system call to set.
     *
     * @param value
     *            The value to set.
     */
    void setValue(String value);

}
