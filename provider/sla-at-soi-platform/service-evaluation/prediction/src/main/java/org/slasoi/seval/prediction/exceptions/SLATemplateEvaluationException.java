package org.slasoi.seval.prediction.exceptions;

/**
 * Exception thrown if an SLATemplate referenced by a service builder does not contain all required information.
 * 
 * @author brosch
 */
public class SLATemplateEvaluationException extends Exception {

    /**
     * A serial version UID for the serializable class.
     */
    private static final long serialVersionUID = 3657614625868220304L;

    /**
     * Creates an exception with a message.
     * 
     * @param message
     *            the message
     */
    public SLATemplateEvaluationException(final String message) {
        super(message);
    }

}
