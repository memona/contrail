package org.slasoi.seval.prediction.configuration;

import java.util.List;

/**
 * This system call object identifies the usage scenario model element in the
 * usage model sub-model of the prediction model to be adjustment.
 *
 * @author Benjamin Klatt
 *
 */
public interface SystemCall {

    /**
     * Get the name for the system call.
     *
     * @return The name of the system call.
     */
    String getName();

    /**
     * Set the name for the system call.
     *
     * @param name
     *            The name of the system call.
     */
    void setName(String name);

    /**
     * Get the variables for the system call.
     *
     * @return The variables of the system call.
     */
    List<SystemCallVariable> getVariables();

    /**
     * Set the variables for the system call.
     *
     * @param variables
     *            The variables of the system call.
     */
    void setVariables(List<SystemCallVariable> variables);

    /**
     * Add a single variable to the system call.
     *
     * @param variable
     *            The variable to add.
     */
    void addVariable(SystemCallVariable variable);

}
