/**
 * The exceptions specific for the prediction component.
 *
 * @author Benjamin Klatt
 * @author Franz Brosch
 */
package org.slasoi.seval.prediction.exceptions;

