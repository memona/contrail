package org.slasoi.seval.prediction.prediction.impl;

import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.Entry_type0;

/**
 * A mockup entry type to be able to create such an object without a soap message.
 * 
 * @author Benjamin Klatt
 * 
 */
public class Entry_type0Mock extends Entry_type0 {

    /**
     * The serialization id.
     */
    private static final long serialVersionUID = -1158836649281065665L;

    /**
     * Default constructor to create a mock entry for testing.
     * 
     * @param key
     *            The key of the entry.
     * @param value
     *            The value of the entry.
     */
    public Entry_type0Mock(String key, int value) {
        this.setKey(key);
        this.setValue(value);
    }
}
