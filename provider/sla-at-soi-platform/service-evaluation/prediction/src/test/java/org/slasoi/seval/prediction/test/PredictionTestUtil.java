package org.slasoi.seval.prediction.test;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.Landscape;
import org.slasoi.models.scm.ServiceBinding;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceConstructionModel;
import org.slasoi.models.scm.ServiceConstructionModelFactory;
import org.slasoi.models.scm.extended.ServiceBuilderExtended;
import org.slasoi.slamodel.sla.SLATemplate;

import eu.slaatsoi.slamodel.SLATemplateDocument;

/**
 * Utility class to support test development.
 * 
 * @author Benjamin Klatt
 *
 */
public class PredictionTestUtil {
    
    /** The id of the single resource container. **/
    public static final String RESOURCE_CONTAINER_SINGLE_ID = "AllInOne Infrastructure";


    /**
     * Prepares a list of service builders.
     * 
     * The service builders inform about possible realizations of the target service that is requested through the
     * SLATemplate (which has been created in Step 2). Thereby, a realization includes information about the internal
     * structure of the service, as well as the properties of the required external software services (if any) and
     * infrastructure services. This information stems from the service landscape and is provided to the service
     * evaluation component via its caller, the software POC.
     * 
     * @return A set of initialized service builders.
     */
    public Set<ServiceBuilder> initServiceBuilders() {

        Set<ServiceBuilder> builders = new HashSet<ServiceBuilder>();
        ServiceConstructionModelFactory factory = ServiceConstructionModel.getFactory();
        String landscapePath = System.getenv("SLASOI_HOME") 
                                + File.separator 
                                + "software-servicemanager"
                                + File.separator 
                                + "ORC.scm";
        Landscape landscape = (Landscape) ServiceConstructionModel.loadFromXMI(landscapePath);

        // In a real setting, the binding of dependencies to SLATemplates
        // as shown in the following code would be done by the software
        // service manager (SSM):
        String slaFilePath = System.getenv("SLASOI_HOME") 
                                + File.separator 
                                + "software-servicemanager"
                                + File.separator 
                                + "orc"
                                + File.separator 
                                + "infrastructure-templates"
                                + File.separator 
                                + "infrastructure_SLA_template.xml";
        SLATemplate infrastructureSLATemplate;
        try {
            infrastructureSLATemplate = initSLATemplate(slaFilePath);
        } catch (Exception e) {
            throw new RuntimeException("Failed to load the infrastructure template ",e);
        }
        ServiceBinding binding = factory.createServiceBinding();

        binding.setSlaTemplate(infrastructureSLATemplate);
        Dependency dep = landscape.getImplementations(0).getDependencies().get(0);

        ServiceBuilderExtended serviceBuilder = new ServiceBuilderExtended();

        serviceBuilder.setUuid("ORC_AllInOne");
        serviceBuilder.setImplementation(landscape.getImplementations(0));
        serviceBuilder.addBinding(dep, infrastructureSLATemplate);

        landscape.addBuilder(serviceBuilder);

        builders.add(serviceBuilder);
        
        return builders;
    }
    
    /**
     * Build up the general SLA Template object based on the test infrastructure sla file.
     * @param slaFilePath The sla template file to load.
     * @return The prepared sla template.
     * @throws Exception
     */
    public SLATemplate initSLATemplate(String slaFilePath) throws Exception {
        SLATemplateDocument slaTemplateXml = SLATemplateDocument.Factory.parse(new File(slaFilePath));           
        SLASOITemplateParser slasoiTemplateParser = new SLASOITemplateParser();
        SLATemplate slaTemplate = slasoiTemplateParser.parseTemplate(slaTemplateXml.xmlText());
        return slaTemplate;
    }

}
