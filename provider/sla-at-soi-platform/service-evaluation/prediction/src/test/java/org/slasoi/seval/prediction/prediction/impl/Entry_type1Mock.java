package org.slasoi.seval.prediction.prediction.impl;

import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.Entry_type1;

/**
 * A mockup entry type to be able to create such an object without a soap message.
 * 
 * @author Benjamin Klatt
 * 
 */
public class Entry_type1Mock extends Entry_type1 {

    /**
     * The serialization id.
     */
    private static final long serialVersionUID = -1158836649281065665L;

    /**
     * Default constructor to create a mock entry for testing.
     * 
     * @param percentageKey
     *            The key of the entry.
     * @param value
     *            The value of the entry.
     */
    public Entry_type1Mock(double percentageKey, double value) {
        this.setKey(percentageKey);
        this.setValue(value);
    }
}
