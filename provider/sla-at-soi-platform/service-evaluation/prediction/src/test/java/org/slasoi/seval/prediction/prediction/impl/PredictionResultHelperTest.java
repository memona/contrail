package org.slasoi.seval.prediction.prediction.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.Entry_type0;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.Entry_type1;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.Frequencies_type0;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.Percentiles_type0;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.ReliabilitySimulationResult;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.ResponseTimeSimulationResult;
import org.slasoi.seval.prediction.prediction.impl.generated.SimuServiceServiceStub.SimuServiceResultStatus;
import org.slasoi.seval.prediction.service.IEvaluationResult;

/**
 * Test case for the prediction result helper implementation.
 * 
 * @author Benjamin Klatt
 * 
 */
public class PredictionResultHelperTest {

    @Test
    public void testGetHelper() {
        PredictionResultHelper helper = PredictionResultHelper.getHelper();
        assertNotNull(helper);
    }

    @Test
    public void testBuildPredictionResult() {
        SimuServiceResultStatus status = new SimuServiceResultStatus();
        
        // response time results
        status.addResponseTimeResults(buildResponseTimeResult(0.25, 1));
        status.addResponseTimeResults(buildResponseTimeResult(0.50, 2));
        status.addResponseTimeResults(buildResponseTimeResult(0.75, 3));
        status.addResponseTimeResults(buildResponseTimeResult(0.90, 4));
        status.addResponseTimeResults(buildResponseTimeResult(0.95, 5));
        status.addResponseTimeResults(buildResponseTimeResult(0.99, 6));
        status.addResponseTimeResults(buildResponseTimeResult(0.0, 0));

        // reliability results
        status.addReliabilityResults(buildReliabilityResult("success",10));
        status.addReliabilityResults(buildReliabilityResult("failure",11));
        status.addReliabilityResults(buildReliabilityResult("unknown",99));
        
        PredictionResultHelper helper = PredictionResultHelper.getHelper();
        IEvaluationResult result = helper.buildPredictionResult(status);
        
        assertEquals("wrong number of response time results",6,result.getResponseTimeResults().size());
        assertEquals("wrong number of reliability results",2,result.getReliabilityResults().size());
    }

    /**
     * Helper method create response time results
     * 
     * @param aggregationType
     *            The aggregation type to set.
     * @param value
     *            The value to set.
     * @return The prepared result object.
     */
    private ResponseTimeSimulationResult buildResponseTimeResult(double percentile, double value) {
        
        ResponseTimeSimulationResult result = new ResponseTimeSimulationResult();

        Percentiles_type0 percentiles = new Percentiles_type0();
        percentiles.setEntry(new Entry_type1[]{new Entry_type1Mock(percentile,value)});
        result.setPercentiles(percentiles);

        result.setSensorName("SERVICE_ID");
        
        return result;
    }

    /**
     * Helper method create response time results
     * 
     * @param aggregationType
     *            The aggregation type to set.
     * @param value
     *            The value to set.
     * @return The prepared result object.
     */
    private ReliabilitySimulationResult buildReliabilityResult(String resultType, int occurrences) {
        
        ReliabilitySimulationResult result = new ReliabilitySimulationResult();

        Frequencies_type0 frequencies = new Frequencies_type0();
        frequencies.setEntry(new Entry_type0[]{new Entry_type0Mock(resultType,occurrences)});
        
        result.setFrequencies(frequencies);

        result.setSensorName("SERVICE_ID <modelid>");
        
        return result;
    }
}
