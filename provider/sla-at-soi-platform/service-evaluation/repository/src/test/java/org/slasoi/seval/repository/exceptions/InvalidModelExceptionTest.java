package org.slasoi.seval.repository.exceptions;

import org.junit.Test;
import static org.junit.Assert.assertNotNull;

/**
 * Unit test case for the exception type.
 * @author Benjamin Klatt
 *
 */
public class InvalidModelExceptionTest {

    @Test
    public void testInvalidModelException() {
        InvalidModelException exception = new InvalidModelException("testMessage");
        assertNotNull(exception);
    }
}
