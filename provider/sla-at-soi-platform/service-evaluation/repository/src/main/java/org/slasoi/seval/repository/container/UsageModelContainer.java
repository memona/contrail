package org.slasoi.seval.repository.container;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.emf.ecore.EPackage;
import org.slasoi.seval.repository.EntityInfo;

import de.uka.ipd.sdq.pcm.core.composition.ProvidedDelegationConnector;
import de.uka.ipd.sdq.pcm.core.entity.InterfaceProvidingEntity;
import de.uka.ipd.sdq.pcm.repository.ProvidedRole;
import de.uka.ipd.sdq.pcm.repository.RepositoryComponent;
import de.uka.ipd.sdq.pcm.system.System;
import de.uka.ipd.sdq.pcm.usagemodel.AbstractUserAction;
import de.uka.ipd.sdq.pcm.usagemodel.EntryLevelSystemCall;
import de.uka.ipd.sdq.pcm.usagemodel.UsageModel;
import de.uka.ipd.sdq.pcm.usagemodel.UsageScenario;
import de.uka.ipd.sdq.pcm.usagemodel.UsagemodelPackage;

/**
 * This class represents a model container for a usage model.
 * 
 * @author brosch
 * 
 */
public class UsageModelContainer extends EMFModelContainer<UsageModel> {

    /**
     * Standard constructor.
     */
    public UsageModelContainer() {
        super();
    }

    /**
     * Constructor requiring the file with the persisted usage model.
     * @param usageModelFile The file to read the usage model from.
     */
    public UsageModelContainer(final File usageModelFile) {
        super(usageModelFile);
    }

    /**
     * Constructor requiring to set the usage model in the container.
     * @param usageModel The usage model to set.
     */
    public UsageModelContainer(final UsageModel usageModel) {
        super(usageModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected EPackage getEInstance() {
        return UsagemodelPackage.eINSTANCE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getENsURI() {
        return UsagemodelPackage.eNS_URI;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected EMFModelContainer<UsageModel> newInstance(final File targetFile) {
        return new UsageModelContainer(targetFile);
    }

    /**
     * Retrieves the list of included usage scenarios.
     * 
     * @return the list of usage scenarios
     */
    public EntityInfo[] getAllUsageScenarioInfos() {
        ArrayList<EntityInfo> resultList = new ArrayList<EntityInfo>();
        for (UsageScenario scenario : getModel().getUsageScenario_UsageModel()) {
            resultList.add(new EntityInfo(scenario.getId(), scenario.getEntityName()));
        }
        return resultList.toArray(new EntityInfo[resultList.size()]);
    }

    /**
     * Retrieves a usage scenario by name.
     * 
     * @param scenarioName
     *            the name of the usage scenario
     * @return the usage scenario
     */
    public EntityInfo getUsageScenarioInfoByName(final String scenarioName) {
        for (EntityInfo scenarioInfo : getAllUsageScenarioInfos()) {
            if (scenarioInfo.getName().equals(scenarioName)) {
                return scenarioInfo;
            }
        }
        assert false : "Usage scenario with name \"" + scenarioName + "\" not found!";
        return null;
    }

    /**
     * Retrieves a usage scenario by id.
     * 
     * @param scenarioId
     *            the id of the usage scenario
     * @return the usage scenario
     */
    public EntityInfo getUsageScenarioInfoById(final String scenarioId) {
        for (EntityInfo scenarioInfo : getAllUsageScenarioInfos()) {
            if (scenarioInfo.getId().equals(scenarioId)) {
                return scenarioInfo;
            }
        }
        assert false : "Usage scenario with ID \"" + scenarioId + "\" not found!";
        return null;
    }

    /**
     * Retrieves the usage scenario that corresponds to a certain service operation.
     * 
     * This method assumes that each usage scenario has one EntryLevelSystemCall. The name of the call's signature and
     * the name of the providing service component are compared to the input parameters to identify the demanded usage
     * scenario. In order to search for service components, the corresponding service component model (PCM Repository)
     * has to be queried from the given usage model.
     * 
     * @param serviceName
     *            the name of the service (component)
     * @param operationName
     *            the name of the service operation (signature)
     * @return the usage scenario
     */
    public UsageScenario getUsageScenario(final String serviceName, final String operationName) {
        for (EntityInfo scenarioInfo : getAllUsageScenarioInfos()) {
            if (getServiceComponentInfo(scenarioInfo).getName().equals(serviceName)) {
                if (getSystemCall(scenarioInfo).getOperationSignature__EntryLevelSystemCall().getEntityName()
                        .equals(operationName)) {
                    return getUsageScenario(scenarioInfo);
                }
            }
        }
        assert false : "Usage Scenario for service \"" + serviceName + "." + operationName + "\" not found!";
        return null;
    }

    /**
     * Retrieves the service component that is referred to in the given usage scenario.
     * 
     * This method assumes that each usage scenario has one EntryLevelSystemCall. The service component providing this
     * call is returned as a result. In order to search for service components, the corresponding service component
     * model (PCM Repository) has to be queried from the given usage model.
     * 
     *@param scenarioInfo
     *            the usage scenario
     * @return the service component
     */
    public EntityInfo getServiceComponentInfo(final EntityInfo scenarioInfo) {

        // Find the service component that belongs to the scenario:
        RepositoryComponent component = getServiceComponent(scenarioInfo);

        // Return a new info object for the service component:
        return new EntityInfo(component.getId(), component.getEntityName());
    }

    /**
     * Retrieves the name of the service operation that corresponds to the given usage scenario id.
     * 
     * This method assumes that each usage scenario has one EntryLevelSystemCall as a top-level action within the
     * scenario behaviour. The signature of this call is returned as a result.
     * 
     * @param scenarioInfo
     *            the usage scenario
     * @return the service operation name
     */
    public String getServiceOperationName(final EntityInfo scenarioInfo) {

        // Find the system call belonging to the scenario:
        EntryLevelSystemCall systemCall = getSystemCall(scenarioInfo);

        // Return the service operation name:
        return systemCall.getOperationSignature__EntryLevelSystemCall().getEntityName();
    }

    /**
     * Retrieves the service component that is referred to by the given usage scenario.
     * 
     * This method assumes that each usage scenario has one EntryLevelSystemCall as a top-level action within the
     * scenario behaviour. The service component providing this call is returned as a result.
     * 
     * @param scenarioInfo
     *            the usage scenario
     * @return the service component
     */
    private RepositoryComponent getServiceComponent(final EntityInfo scenarioInfo) {

        // Find the system call belonging to the scenario:
        EntryLevelSystemCall systemCall = getSystemCall(scenarioInfo);

        // Return the service component that provides the corresponding role within the
        // system:
        return getServiceComponent(systemCall.getProvidedRole_EntryLevelSystemCall());
    }

    /**
     * Retrieves the service component that provides the given role at a system's boundary.
     * 
     * @param systemProvidedRole
     *            the provided role at the system's boundary
     * @return the service component
     */
    private RepositoryComponent getServiceComponent(final ProvidedRole systemProvidedRole) {
        InterfaceProvidingEntity entity = systemProvidedRole.getProvidingEntity_ProvidedRole();
        for (ProvidedDelegationConnector connector : ((System) entity)
                .getProvidedDelegationConnectors_ComposedStructure()) {
            if (connector.getOuterProvidedRole_ProvidedDelegationConnector().getId().equals(
                    systemProvidedRole.getId())) {
                return (RepositoryComponent) connector.getInnerProvidedRole_ProvidedDelegationConnector()
                        .getProvidingEntity_ProvidedRole();
            }
        }
        assert false : "Service component for provided role with ID \"" + systemProvidedRole.getId()
                + "\" not found!";
        return null;
    }

    /**
     * Returns the usage scenario model object from the given usage scenario info object.
     * 
     * @param scenarioInfo
     *            the usage scenario info object
     * @return the usage scenario model object
     */
    private UsageScenario getUsageScenario(final EntityInfo scenarioInfo) {

        // Go through all usage scenarios:
        for (UsageScenario scenario : this.getModel().getUsageScenario_UsageModel()) {
            if (scenario.getId().equals(scenarioInfo.getId())) {
                return scenario;
            }
        }

        // Nothing found?
        assert false : "Usage scenario with ID \"" + scenarioInfo.getId() + "\" not found!";
        return null;
    }

    /**
     * Returns the EntrySystemLevelCall model object from the given usage scenario info object.
     * 
     * This method assumes that each usage scenario has one EntryLevelSystemCall as a top-level action within the
     * scenario behavior.
     * 
     * @param scenarioInfo
     *            the usage scenario info object
     * @return the SystemLevelEntryCall model object
     */
    private EntryLevelSystemCall getSystemCall(final EntityInfo scenarioInfo) {

        // Search for the EntrySystemLevelCall:
        for (AbstractUserAction action : getUsageScenario(scenarioInfo).getScenarioBehaviour_UsageScenario()
                .getActions_ScenarioBehaviour()) {
            if (action instanceof EntryLevelSystemCall) {
                return (EntryLevelSystemCall) action;
            }
        }

        // Nothing found?
        assert false : "System call for usage scenario with ID \"" + scenarioInfo.getId() + "\" not found!";
        return null;
    }
}
