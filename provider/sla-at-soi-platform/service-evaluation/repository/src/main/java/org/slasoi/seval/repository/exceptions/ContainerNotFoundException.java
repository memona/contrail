package org.slasoi.seval.repository.exceptions;

import java.util.NoSuchElementException;

/**
 * Indicates that an expected model container was not found in a software model.
 * 
 * This condition indicates that the software model is not complete / valid.
 * 
 * @author patejdl
 * 
 */
public class ContainerNotFoundException extends NoSuchElementException {

    /**
     * Serial version UID for the seralizable class.
     */
    private static final long serialVersionUID = -4581214788598650786L;

    /**
     * Creates the exception with a message.
     * 
     * @param message
     *            the exception message
     */
    public ContainerNotFoundException(final String message) {
        super(message);
    }

}
