package org.slasoi.seval.repository.helper;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

/**
 * This class provides auxiliary functionality for working with EMF models.
 * 
 * @author brosch
 * 
 */
public final class EMFHelper {

    /**
     * Log4j functionality.
     */
    private static final Logger LOGGER = Logger.getLogger(EMFHelper.class.getName());

    /**
     * A singleton pattern ensures the existence of only one EMF helper.
     */
    private static EMFHelper singletonInstance = new EMFHelper();

    /**
     * Retrieves the singleton instance.
     * 
     * @return the singleton instance
     */
    public static EMFHelper getHelper() {
        return singletonInstance;
    }

    /**
     * The constructor is made private according to the singleton pattern.
     */
    private EMFHelper() {
    }

    /**
     * Helper routine to load EMF contents from a XMI model file.
     * 
     * @param fileName
     *            the model file name
     * @param eNsURI
     *            the namespace URI to register
     * @param eINSTANCE
     *            the EPackage to register
     * @return the root model element
     */
    public EObject loadFromXMI(final String fileName, final String eNsURI, final EPackage eINSTANCE) {

        // Create a resource set to hold the resources.
        ResourceSet resourceSet = new ResourceSetImpl();

        // Register the appropriate resource factory to handle all file extensions. The used resource factory class
        // determines the expected deserialization input format!
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
                Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());

        // Register the package to ensure it is available during loading.
        resourceSet.getPackageRegistry().put(eNsURI, eINSTANCE);

        // Construct the URI for the instance file.
        // The argument is treated as a file path only if it denotes an existing
        // file. Otherwise, it's directly treated as a URL.
        File file = new File(fileName);
        URI uri = null;
        if (file.isFile()) {
            uri = URI.createFileURI(file.getAbsolutePath());
        } else {
            uri = URI.createURI(fileName);
        }

        // Demand load resource for this file.
        Resource resource = resourceSet.getResource(uri, true);
        LOGGER.debug("Loaded " + uri);

        EObject eObject = (EObject) resource.getContents().iterator().next();
        return EcoreUtil.getRootContainer(eObject);
    }

    /**
     * Helper routine to save EMF contents to an XMI model file.
     * 
     * @param objectToSave
     *            the EMF contents
     * @param fileName
     *            the model file name
     */
    public void saveToXMI(final EObject objectToSave, final String fileName) {

        // Create a resource set.
        ResourceSet resourceSet = new ResourceSetImpl();

        // Register the default resource factory (only needed for stand-alone) The used resource factory class
        // determines the serialization output format!
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
                Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());

        // Get the URI of the model file.
        URI fileURI = URI.createFileURI(new File(fileName).getAbsolutePath());

        // Create a resource for this file.
        Resource resource = resourceSet.createResource(fileURI);

        // Add the book and writer objects to the contents.
        resource.getContents().add(objectToSave);

        // Save the contents of the resource to the file system.
        try {
            resource.save(Collections.EMPTY_MAP);
            LOGGER.debug("Saved " + fileURI);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
