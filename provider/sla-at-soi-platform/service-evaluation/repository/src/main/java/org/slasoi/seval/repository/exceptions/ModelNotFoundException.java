package org.slasoi.seval.repository.exceptions;

/**
 * Indicates that a certain software model could not be found in the design-time repository.
 * 
 * @author brosch
 */
public class ModelNotFoundException extends Exception {

    /**
     * A UID for serialization.
     */
    private static final long serialVersionUID = -6091416296766254406L;

    /**
     * Instantiates the exception with the given model ID.
     * 
     * @param modelID
     *            the model ID
     */
    public ModelNotFoundException(final String modelID) {
        super("Cannot find SoftwareModel with identifier: " + modelID);
    }
}
