package org.slasoi.seval.repository.container;

import java.io.File;
import java.io.IOException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.slasoi.seval.repository.helper.EMFHelper;
import org.slasoi.seval.repository.helper.IOHelper;

/**
 * Provides container functionality for an EMF model.
 * 
 * @author brosch
 * 
 * @param <T>
 *            the class of the root model element
 */
public abstract class EMFModelContainer<T extends EObject> extends AbstractModelContainer {

    /**
     * References the model contents in memory.
     */
    private T model = null;

    /**
     * References the location of the model in the file system.
     */
    private File modelFile = null;

    /**
     * References the IO helper.
     */
    private IOHelper fileHelper = IOHelper.getHelper();

    /**
     * References the EMF helper.
     */
    private EMFHelper modelHelper = EMFHelper.getHelper();

    /**
     * Retrieves the model package.
     * 
     * Must be implemented by sub classes.
     * 
     * @return the model package
     */
    protected abstract EPackage getEInstance();

    /**
     * Retrieves the model namespace URI.
     * 
     * Must be implemented by sub classes.
     * 
     * @return the model namespace URI
     */
    protected abstract String getENsURI();

    /**
     * Retrieves a new model container instance that is bound to a new model file.
     * 
     * Must be implemented by sub classes.
     * 
     * @param targetFile
     *            the target model file
     * @return the new model container
     */
    protected abstract EMFModelContainer<T> newInstance(File targetFile);

    /**
     * Initializes an empty model container.
     */
    public EMFModelContainer() {
        super();
    }

    /**
     * Initializes a model container for the given file.
     * 
     * @param modelFile
     *            the model file name
     */
    public EMFModelContainer(final File modelFile) {
        // Set modelFile
        this.modelFile = modelFile;
        // load model from file
        load();
    }

    /**
     * Initializes a model container for the given model.
     * 
     * @param model
     *            the model
     */
    public EMFModelContainer(final T model) {
        this.model = model;
    }

    /**
     * Loads the model contents from file to memory, and retrieves the root model element.
     * 
     * @return the root model element
     */
    public T getModel() {
        if (model == null) {
            load();
        }
        return (T) model;
    }

    /**
     * Sets a model file for this container.
     * 
     * @param file
     *            the file object of the model file to set
     */
    public void setModelFile(final File file) {
        modelFile = file;
    }

    /**
     * Retrieves the model file name as a String.
     * 
     * @return the model file name
     */
    public String getModelFileName() {
        return this.modelFile.getName();
    }

    /**
     * Retrieves the model file path as a String.
     * 
     * @return the model file path
     */
    public String getModelFilePath() {
        return this.modelFile.getAbsolutePath();
    }

    /**
     * Loads model contents from file into memory.
     */
    @SuppressWarnings("unchecked")
    public void load() {
        assert (model == null) : "Model has already been loaded!";
        model = (T) modelHelper.loadFromXMI(modelFile.getAbsolutePath(), getENsURI(), getEInstance());
    }

    /**
     * Saves the model contents from memory to file.
     */
    public void store() {
        assert (model != null) : "Model has not been loaded!";
        assert modelFile.exists() : "Path does not exist: " + modelFile.getAbsolutePath();
        modelHelper.saveToXMI(model, modelFile.getAbsolutePath());
    }

    /**
     * Saves the model contents from memory to file to a given destination.
     * 
     * @param targetFile
     *            the target location
     * @throws IOException
     *             Exceotion that the XMI file for the model could not be written.
     */
    public void storeTo(final File targetFile) throws IOException {
        // set the targetFile as new modelFile
        this.modelFile = targetFile;
        // save the model
        modelHelper.saveToXMI(model, targetFile.getAbsolutePath());
    }

    /**
     * Create a copy of a file.
     * 
     * @param targetFile
     *            The file to be copied.
     * @throws IOException
     *             Identifying that the file could not be copied successfully.
     */
    public void copyTo(final File targetFile) throws IOException {
        // hardcopy the file
        fileHelper.copyFile(this.modelFile, targetFile);
        // set copy as new modelFile
        this.modelFile = targetFile;
        // load model (and to do that set it as null first
        this.model = null;
        load();
    }

    /**
     * Checks if this model container equals another container.
     * 
     * The two containers are assumed to be equal if they reference the same model file.
     * 
     * @param object
     *            The object to check for equality.
     * @return true if the object is equal to the current one, false if not.
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(final Object object) {
        if (object instanceof EMFModelContainer) {
            EMFModelContainer<T> mc = (EMFModelContainer<T>) object;
            return this.modelFile.equals(mc.modelFile);
        }
        return false;
    }

    /**
     * The hash code generation.
     * 
     * @return The hash code.
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
