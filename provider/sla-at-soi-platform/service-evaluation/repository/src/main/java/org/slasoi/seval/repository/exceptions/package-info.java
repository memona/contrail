/**
* Repository specific exceptions.
*
* @author Benjamin Klatt
* @author Franz Brosch
*/
package org.slasoi.seval.repository.exceptions;
