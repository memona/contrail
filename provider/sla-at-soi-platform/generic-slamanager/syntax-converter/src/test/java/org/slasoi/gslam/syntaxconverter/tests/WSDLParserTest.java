package org.slasoi.gslam.syntaxconverter.tests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import junit.framework.TestCase;

import org.slasoi.gslam.syntaxconverter.WSDLParser;
import org.slasoi.slamodel.core.Annotated;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.primitives.BOOL;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.service.Interface.Operation.Property;

public class WSDLParserTest extends TestCase {
    public void testWSDLParser() throws Exception {
        // get the WSDL as String
        InputStream wsdlStream = WSDLParserTest.class.getResourceAsStream("/WSDLEXample.wsdl");
        String wsdlString = convertInputStreamToString(wsdlStream);

        // Parse the WSDL-file
        WSDLParser wsdlParser = new WSDLParser();
        Specification[] parsedInterfaceSpec = wsdlParser.parseWSDL(wsdlString);

        assertEquals(1, parsedInterfaceSpec.length);
        Specification specification = parsedInterfaceSpec[0];

        testSpecification(specification);

    }

    private void testSpecification(Specification specification) {
        // Annotation
        testAnnotation(specification);

        // Name
        String name = specification.getName();
        assertEquals("SLASOIInterface", name);

        // Extended
        ID[] extended = specification.getExtended();
        assertEquals(4, extended.length);
        for (int i = 0; i < extended.length; i++) {
            assertEquals("http://www.w3.org/2006/01/wsdl:id", extended[i].getValue());
        }

        // Operation
        Operation[] operations = specification.getOperations();
        for (int i = 0; i < operations.length; i++) {
            testOperation(operations[i]);
        }

    }

    private void testOperation(Operation operation) {
        // Annotation
        testAnnotation(operation);

        // Name
        assertTrue(operation.getName().getValue().startsWith("operation"));

        // Fault
        STND[] faults = operation.getFaults();
        assertEquals(faults.length, 2);
        for (int i = 0; i < faults.length; i++) {
            assertEquals("http://www.example.com/wsdl20sample:fault", faults[i].getValue());
        }

        // Input
        Property[] inputs = operation.getInputs();
        assertEquals(2, inputs.length);
        for (int i = 0; i < inputs.length; i++) {
            testProperty(inputs[i]);
        }

        // Output
        Property[] outputs = operation.getOutputs();
        assertEquals(2, outputs.length);
        for (int i = 0; i < outputs.length; i++) {
            testProperty(outputs[i]);
        }

        // Related
        Property[] relateds = operation.getRelated();
        for (int i = 0; i < relateds.length; i++) {
            testProperty(relateds[i]);
        }
    }

    private void testProperty(Property property) {
        // Annotation
        testAnnotation(property);

        // Name
        assertTrue(property.getName().getValue().startsWith("parameter"));

        // Auxiliary
        assertEquals(false, property.isAuxiliary());

        // Datatype
        STND datatype = property.getDatatype();
        assertEquals("http://www.example.com/wsdl20sample:datatype", datatype.getValue());

        // Domain
        DomainExpr domain = property.getDomain();
        testDomain(domain);
    }

    private void testDomain(DomainExpr domain) {
        assertTrue(domain instanceof SimpleDomainExpr);
        SimpleDomainExpr sDomain = (SimpleDomainExpr) domain;

        // ComparisonOp
        assertEquals("<", sDomain.getComparisonOp().getValue());

        // Value
        assertTrue(sDomain.getValue() instanceof BOOL);
        assertFalse(((BOOL) sDomain.getValue()).getValue());
    }

    private void testAnnotation(Annotated annotated) {
        // Descr
        assertEquals("annotatedtext", annotated.getDescr());

        // Properties
        assertTrue(annotated.getPropertyKeys().length > 0);
        Object propertyValue = annotated.getPropertyValue(new STND("key"));
        assertTrue(propertyValue instanceof String);
        assertEquals("entry", (String) propertyValue);
    }

    protected String convertInputStreamToString(InputStream is) throws IOException {
        if (is != null) {
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            }
            finally {
                is.close();
            }
            return sb.toString();
        }
        else {
            return "";
        }
    }
}
