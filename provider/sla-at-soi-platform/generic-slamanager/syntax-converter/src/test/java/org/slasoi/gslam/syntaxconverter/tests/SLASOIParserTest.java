package org.slasoi.gslam.syntaxconverter.tests;

import java.io.IOException;

import org.apache.xmlbeans.XmlCalendar;
import org.apache.xmlbeans.XmlException;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.slamodel.sla.SLA;

import eu.slaatsoi.slamodel.SLADocument;

public class SLASOIParserTest extends XmlParserTest {

    public void testParseSLASOI() {
        try {
            SLADocument slaXml =
                    SLADocument.Factory.parse(SLASOITemplateParserTest.class.getResourceAsStream("/SLASOI.xml"));

            SLASOIParser slasoiParser = new SLASOIParser();
            SLA sla = slasoiParser.parseSLA(slaXml.xmlText());

            // Annotation
            testAnnotation(sla);

            // UUID
            assertEquals("id", sla.getUuid().getValue());

            // TemplateId
            assertEquals("id", sla.getTemplateId().getValue());

            // ModelVersion
            assertEquals(SLA.$model_version, sla.getModelVersion());

            // AgreedAt
            XmlCalendar agreedAtCal = new XmlCalendar("2002-05-30T09:00:00.000+02:00");
//            assertEquals(agreedAtCal.getTime(), sla.getAgreedAt().getValue().getTime());

            // EffectiveFrom
            XmlCalendar effectiveFrom = new XmlCalendar("2002-05-30T09:00:00.000+02:00");
  //          assertEquals(effectiveFrom.getTime(), sla.getEffectiveFrom().getValue().getTime());

            // EffetiveUntil
            XmlCalendar effectiveUntil = new XmlCalendar("2002-05-30T09:00:00.000+02:00");
    //        assertEquals(effectiveUntil.getTime(), sla.getEffectiveUntil().getValue().getTime());

            // Party
            assertEquals(sla.getParties().length, 2);
            testParties(sla.getParties());

            // InterfaceDeclr
            assertEquals(sla.getInterfaceDeclrs().length, 2);
            testInterfaceDeclrs(sla.getInterfaceDeclrs());

            // VariableDeclr
            assertEquals(sla.getVariableDeclrs().length, 2);
            testVariableDeclrs(sla.getVariableDeclrs());

            // AgreementTerm
            assertEquals(2, sla.getAgreementTerms().length);
            testAgreementTerms(sla.getAgreementTerms());
        }
        catch (XmlException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
}
