package org.slasoi.gslam.syntaxconverter.tests;

import java.io.IOException;

import org.apache.xmlbeans.XmlException;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

import eu.slaatsoi.slamodel.SLATemplateDocument;

public class SLASOITemplateParserTest extends XmlParserTest {

    public void testParseSLASOITemplate() {
        try {
            SLATemplateDocument slaTemplateXml =
                    SLATemplateDocument.Factory.parse(SLASOITemplateParserTest.class
                            .getResourceAsStream("/SLASOITemplate.xml"));

            SLASOITemplateParser slasoiTemplateParser = new SLASOITemplateParser();
            SLATemplate slaTemplate = slasoiTemplateParser.parseTemplate(slaTemplateXml.xmlText());

            // Annotation
            testAnnotation(slaTemplate);

            // UUID
            assertEquals("id", slaTemplate.getUuid().getValue());

            // ModelVersion
            assertEquals(SLA.$model_version, slaTemplate.getModelVersion());

            // Party
            assertEquals(slaTemplate.getParties().length, 2);
            testParties(slaTemplate.getParties());

            // InterfaceDeclr
            assertEquals(slaTemplate.getInterfaceDeclrs().length, 2);
            testInterfaceDeclrs(slaTemplate.getInterfaceDeclrs());

            // VariableDeclr
            assertEquals(slaTemplate.getVariableDeclrs().length, 2);
            testVariableDeclrs(slaTemplate.getVariableDeclrs());

            // AgreementTerm
            assertEquals(2, slaTemplate.getAgreementTerms().length);
            testAgreementTerms(slaTemplate.getAgreementTerms());
        }
        catch (XmlException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
}
