package org.slasoi.gslam.syntaxconverter.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.xml.namespace.QName;

import junit.framework.TestCase;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.slasoi.gslam.syntaxconverter.WSDLParser;
import org.slasoi.gslam.syntaxconverter.WSDLRenderer;
import org.slasoi.slamodel.service.Interface.Specification;
import org.w3.x2006.x01.wsdl.BindingType;
import org.w3.x2006.x01.wsdl.DescriptionDocument;
import org.w3.x2006.x01.wsdl.DocumentationType;
import org.w3.x2006.x01.wsdl.ImportType;
import org.w3.x2006.x01.wsdl.IncludeType;
import org.w3.x2006.x01.wsdl.InterfaceOperationType;
import org.w3.x2006.x01.wsdl.InterfaceType;
import org.w3.x2006.x01.wsdl.MessageRefFaultType;
import org.w3.x2006.x01.wsdl.MessageRefType;
import org.w3.x2006.x01.wsdl.ServiceType;
import org.w3.x2006.x01.wsdl.TypesType;

import eu.slaatsoi.slamodel.DomainExprDocument;
import eu.slaatsoi.slamodel.DomainExprType;
import eu.slaatsoi.slamodel.RelatedDocument;
import eu.slaatsoi.slamodel.SimpleDomainExprType;
import eu.slaatsoi.slamodel.ValueExprType;

public class WSDLRendererTest extends TestCase {
    public void testRenderWSDL() throws Exception {
        // get the WSDL as String
        InputStream wsdlStream = WSDLParserTest.class.getResourceAsStream("/WSDLEXample.wsdl");
        String wsdlString = convertInputStreamToString(wsdlStream);

        // Parse the WSDL-file
        WSDLParser wsdlParser = new WSDLParser();
        Specification[] parsedInterfaceSpec = wsdlParser.parseWSDL(wsdlString);

        assertEquals(1, parsedInterfaceSpec.length);
        Specification specification = parsedInterfaceSpec[0];

        // Render the WSDL
        WSDLRenderer wsdlRenderer = new WSDLRenderer();
        String renderedWSDL = wsdlRenderer.renderWSDL(specification);

        DescriptionDocument renderedWsdlXml = DescriptionDocument.Factory.parse(renderedWSDL);

        // Binding
        BindingType[] bindings = renderedWsdlXml.getDescription().getBindingArray();
        assertEquals(bindings.length, 1);
        for (int i = 0; i < bindings.length; i++) {
            testBinding(bindings[0]);
        }

        // Documentation
        DocumentationType[] docsXml = renderedWsdlXml.getDescription().getDocumentationArray();
        assertEquals(docsXml.length, 0);

        // Import
        ImportType[] imports = renderedWsdlXml.getDescription().getImportArray();
        assertEquals(imports.length, 0);

        // Include
        IncludeType[] includes = renderedWsdlXml.getDescription().getIncludeArray();
        assertEquals(includes.length, 0);

        // Interface
        InterfaceType[] interfaces = renderedWsdlXml.getDescription().getInterfaceArray();
        assertEquals(interfaces.length, 1);
        testInterface(interfaces[0]);

        // Service
        ServiceType[] services = renderedWsdlXml.getDescription().getServiceArray();
        assertEquals(services.length, 0);

        // TargetNamespace
        String targetNamespace = renderedWsdlXml.getDescription().getTargetNamespace();
        assertEquals(targetNamespace, "http://www.example.com/wsdl20sample");

        // Types
        TypesType[] types = renderedWsdlXml.getDescription().getTypesArray();
        assertEquals(types.length, 0);

        renderedWsdlXml.save(new File("WSDLExampleRendered.xml"));
    }

    private void testInterface(InterfaceType interfaceType) throws XmlException {
        // Annotation
        testAnnotation(interfaceType);

        // Extends
        List extendsList = interfaceType.getExtends();
        // TODO test it

        // Operation
        InterfaceOperationType[] operations = interfaceType.getOperationArray();
        for (int i = 0; i < operations.length; i++) {
            testOperation(operations[i]);
        }
    }

    private void testOperation(InterfaceOperationType interfaceOperationType) throws XmlException {
        // Annotation
        testAnnotation(interfaceOperationType);

        // Fault
        MessageRefFaultType[] faults = interfaceOperationType.getOutfaultArray();
        for (int i = 0; i < faults.length; i++) {
            testFault(faults[i]);
        }

        // Input
        MessageRefType[] inputs = interfaceOperationType.getInputArray();
        for (int i = 0; i < inputs.length; i++) {
            testMessageRef(inputs[i]);
        }

        // Output
        MessageRefType[] outputs = interfaceOperationType.getOutputArray();
        for (int i = 0; i < outputs.length; i++) {
            testMessageRef(outputs[i]);
        }

        // Related
        String relatedPath = "*[namespace-uri()='http://www.slaatsoi.eu/slamodel' and local-name()='Related']";
        XmlObject[] relateds = interfaceOperationType.selectPath(relatedPath);
        assertEquals(relateds.length, 2);
        for (int i = 0; i < relateds.length; i++) {
            RelatedDocument relatedXml = RelatedDocument.Factory.parse(relateds[i].getDomNode());
            testRelated(relatedXml);
        }
    }

    private void testRelated(RelatedDocument relatedXml) {
        // Annotation
        testAnnotation(relatedXml.getRelated());

        // Name
        String name = relatedXml.getRelated().getName();
        assertTrue(name.equals("parameter"));

        // Auxiliary
        boolean auxiliary = relatedXml.getRelated().getAuxiliary();
        assertFalse(auxiliary);

        // Datatype
        String datatype = relatedXml.getRelated().getDatatype();
        assertEquals("http://www.example.com/wsdl20sample:datatype", datatype);

        // Domain
        DomainExprType domain = relatedXml.getRelated().getDomain();
        testDomainExpr(domain);

    }

    private void testDomainExpr(DomainExprType domain) {
        assertTrue(domain.isSetSimpleDomainExpr());

        // SimpleDomainExpr
        SimpleDomainExprType simpleDomainExpr = domain.getSimpleDomainExpr();

        // Operator
        String comparisonOp = simpleDomainExpr.getComparisonOp();
        assertEquals("<", comparisonOp);

        // Value
        ValueExprType value = simpleDomainExpr.getValue();
        assertTrue(value.isSetBOOL());
        boolean bool = value.getBOOL();
        assertFalse(bool);
    }

    private void testMessageRef(MessageRefType messageRefType) throws XmlException {
        // Annotation
        testAnnotation(messageRefType);

        // Auxiliary
        String auxiliaryPath = "*[namespace-uri()='http://www.slaatsoi.eu/slamodel' and local-name()='BOOL']";
        XmlObject[] auxs = messageRefType.selectPath(auxiliaryPath);
        assertEquals(1, auxs.length);
        XmlCursor auxCursor = auxs[0].newCursor();
        assertEquals("false", auxCursor.getTextValue());

        // Domain
        String domainPath = "*[namespace-uri()='http://www.slaatsoi.eu/slamodel' and local-name()='DomainExpr']";
        XmlObject[] domainObjects = messageRefType.selectPath(domainPath);
        assertEquals(1, domainObjects.length);
        DomainExprDocument domainExprType = DomainExprDocument.Factory.parse(domainObjects[0].getDomNode());
        testDomainExpr(domainExprType.getDomainExpr());
    }

    private void testFault(MessageRefFaultType messageRefFaultType) {
        QName ref = messageRefFaultType.getRef();
        assertEquals("fault", ref.getLocalPart());
        assertEquals("tns", ref.getNamespaceURI());
    }

    private void testAnnotation(XmlObject annotated) {
        // Desc
        String descPath = "*[namespace-uri()='http://www.slaatsoi.eu/slamodel' and local-name()='Text']";
        XmlObject[] descObjects = annotated.selectPath(descPath);
        assertEquals(descObjects.length, 1);
        XmlCursor descCursor = descObjects[0].newCursor();
        String descString = descCursor.getTextValue();
        assertEquals("annotatedtext", descString);

        // Properties
        String keyPath =
                "*[namespace-uri()='http://www.slaatsoi.eu/slamodel' and local-name()='Properties']"
                        + "//*[namespace-uri()='http://www.slaatsoi.eu/slamodel' and local-name()='Key' and node()='key']";
        XmlObject[] keys = annotated.selectPath(keyPath);
        assertEquals(1, keys.length);

        String entryPath =
                "*[namespace-uri()='http://www.slaatsoi.eu/slamodel' and local-name()='Properties']"
                        + "//*[namespace-uri()='http://www.slaatsoi.eu/slamodel' and local-name()='Entry' and node()='entry']";
        XmlObject[] entryObjects = annotated.selectPath(entryPath);
        assertEquals(1, entryObjects.length);

    }

    private void testBinding(BindingType bindingType) {
        // Name
        String name = bindingType.getName();
        assertEquals("RESTfulInterfaceHttpBinding", name);

        // Interface
        QName interfaceQName = bindingType.getInterface();
        assertEquals("http://www.example.com/wsdl20sample", interfaceQName.getNamespaceURI());
        assertEquals("RESTfulInterface", interfaceQName.getLocalPart());

        // Type
        String type = bindingType.getType();
        assertEquals("http://www.w3.org/2006/01/wsdl/http", type);

        // Operation
        // BindingOperationType[] operations = bindingType.getOperationArray();
        // assertEquals(1, operations.length);
        // BindingOperationType operationXml = operations[0];
        // QName ref = operationXml.getRef();
        // assertEquals("", ref.getNamespaceURI());
        // assertEquals("operation1", ref.getLocalPart());
    }

    protected String convertInputStreamToString(InputStream is) throws IOException {
        if (is != null) {
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            }
            finally {
                is.close();
            }
            return sb.toString();
        }
        else {
            return "";
        }
    }
}
