package org.slasoi.gslam.syntaxconverter.tests;

import java.io.IOException;

import org.apache.xmlbeans.XmlException;
import org.ggf.schemas.graap.x2007.x03.wsAgreement.TemplateDocument;
import org.slasoi.gslam.syntaxconverter.SLATemplateParser;
import org.slasoi.gslam.syntaxconverter.WSAgreementTemplateParser;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;

public class WSAgreementTemplateParserTest extends XmlParserTest {
    public void testParseTemplate() {
        try {
            TemplateDocument wsagSLATemplate =
                    TemplateDocument.Factory.parse(SLATemplateParser.class.getResourceAsStream("/WSAGTemplate.xml"));
            WSAgreementTemplateParser slaTemplateParser = new WSAgreementTemplateParser();
            SLATemplate slaTemplate = slaTemplateParser.parseTemplate(wsagSLATemplate.xmlText());

            // Template UUID
            UUID uuid = slaTemplate.getUuid();
            assertEquals(uuid.getValue(), "templateid");

            // ModelVersion
            String modelVersion = slaTemplate.getModelVersion();
            assertEquals(modelVersion, SLATemplate.$model_version);

            // Party
            Party[] parties = slaTemplate.getParties();
            testParties(parties);

            // InterfaceDeclr
            InterfaceDeclr[] interfaceDeclrs = slaTemplate.getInterfaceDeclrs();
            testInterfaceDeclrs(interfaceDeclrs);

            // VariableDeclr
            VariableDeclr[] variableDeclrs = slaTemplate.getVariableDeclrs();
            testVariableDeclrs(variableDeclrs);

            // AgreementTerm
            AgreementTerm[] agreementTerms = slaTemplate.getAgreementTerms();
            testAgreementTerms(agreementTerms);

        }
        catch (XmlException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
