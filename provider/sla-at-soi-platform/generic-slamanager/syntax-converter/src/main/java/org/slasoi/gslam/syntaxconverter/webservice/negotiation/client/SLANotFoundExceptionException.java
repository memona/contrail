
/**
 * SLANotFoundExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.4  Built on : Dec 19, 2010 (08:18:42 CET)
 */

package org.slasoi.gslam.syntaxconverter.webservice.negotiation.client;

public class SLANotFoundExceptionException extends java.lang.Exception{
    
    private org.slasoi.gslam.syntaxconverter.webservice.negotiation.client.SLASOINegotiationStub.SLANotFoundExceptionE faultMessage;

    
        public SLANotFoundExceptionException() {
            super("SLANotFoundExceptionException");
        }

        public SLANotFoundExceptionException(java.lang.String s) {
           super(s);
        }

        public SLANotFoundExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public SLANotFoundExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(org.slasoi.gslam.syntaxconverter.webservice.negotiation.client.SLASOINegotiationStub.SLANotFoundExceptionE msg){
       faultMessage = msg;
    }
    
    public org.slasoi.gslam.syntaxconverter.webservice.negotiation.client.SLASOINegotiationStub.SLANotFoundExceptionE getFaultMessage(){
       return faultMessage;
    }
}
    