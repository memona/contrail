package org.slasoi.gslam.syntaxconverter;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.slasoi.gslam.syntaxconverter.webservice.NegotiationImpl;

/**
 * Returns a new web service.
 * @author Peter A. Chronz
 *
 */
public class NegotiationFactory implements ServiceFactory {
    
    /**
     * Returns a new web service object for negotiation.
     */
    public Object getService(Bundle arg0, ServiceRegistration arg1) {
        String slamId = (String)arg1.getReference().getProperty("org.apache.axis2.osgi.ws");
        
        return new NegotiationImpl();
    }

    public void ungetService(Bundle arg0, ServiceRegistration arg1, Object arg2) {
        // TODO Auto-generated method stub
    }

}
