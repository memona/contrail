/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/syntax-converter/src/main/java/org/slasoi/gslam/syntaxconverter/TemplateRegistryWSDelegator.java $
 */
package org.slasoi.gslam.syntaxconverter;

import java.util.Vector;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet;
import org.slasoi.gslam.syntaxconverter.broker.Base64Utils;
import org.slasoi.gslam.syntaxconverter.broker.BrokerType;
import org.slasoi.gslam.syntaxconverter.broker.SyntaxConverterCall;
import org.slasoi.gslam.syntaxconverter.broker.SyntaxConverterProxy;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.sla.tools.Validator.Warning;

/**
 * Delegator Class.  It represents a delegator acting on behalf of a template registry through the broker.
 *  
 * @author Miguel Rojas (UDO)
 *
 */
public class TemplateRegistryWSDelegator {
    
    /**
     * Constructor
     */
    public TemplateRegistryWSDelegator() {
    }

    /**
     * Helper method for commands forwarding and their parameters from the SyntaxConverter towards
     * the SLATemplateRegistry 
     */
    protected Object delegate(SyntaxConverterCall call, Object content) throws Exception {
        SyntaxConverterProxy proxy = new SyntaxConverterProxy();
        Vector<String> command = new Vector<String>(3, 2);
        command.add(BrokerType.TEMPLATE_REGISTRY_WS.name());
        command.add(call.name());
        command.add(Base64Utils.encode(content));

        Object result = proxy.command(command);

        if (result instanceof Exception) {
            Exception er = (Exception) result;
            throw new Exception(er.toString());
        }

        return result;
    }

    // --- TemplateRegistry ---
    public String renderMetadata(Metadata metadata) throws Exception {
        return (String) delegate(SyntaxConverterCall.TR_RENDER_METADATA, metadata);
    }

    public Warning parseWarning(String warningString) throws Exception {
        return (Warning) delegate(SyntaxConverterCall.TR_PARSE_WARNING, warningString);
    }

    public ResultSet parseResultSet(String resultString) throws Exception {
        return (ResultSet) delegate(SyntaxConverterCall.TR_PARSE_RESULTSET, resultString);
    }

    public String renderConstraint(ConstraintExpr metadataConstraint) throws Exception {
        return (String) delegate(SyntaxConverterCall.TR_RENDER_CONSTRAINT, metadataConstraint);
    }

    protected SyntaxConverterProxy proxy;
}
