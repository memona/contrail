package org.slasoi.gslam.syntaxconverter;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.syntaxconverter.SyntaxConverter.WSType;

/**
 * Keeps a context of a SLAManager for usage by web service instance.
 * 
 * @author Peter A. Chronz
 */
public final class ContextKeeper {
    private static ContextKeeper instance = null;
    
    private Map<String, SLAManagerContext> contexts;
    
    private ContextKeeper() {
        this.contexts = new HashMap<String, SLAManagerContext>();
    };
/**
 * @return An instance of the context keeper singleton.
 */
    public static ContextKeeper getInstance() {
        if (ContextKeeper.instance == null) {
            instance = new ContextKeeper();
        }
        return instance;
    }
    /**
     * Registers the provided context with a slam id.
     * @param slamId The slam id to be associated with a context.
     * @param context The context that will be needed later on by web services.
     */
    public void registerContext(String slamId, SLAManagerContext context) {
        this.contexts.put(slamId, context);
    }
    
    /**
     * Removes a registered context from the context keeper.
     * @param slamId The slam id for the context to be dropped.
     */
    public void dropContext(String slamId) {
        this.contexts.remove(slamId);
    }
    
    /**
     * Retrieves the context for the respective id.
     * @param slamId The id associated with the required context.
     * @return The required SLAM context.
     */
    public SLAManagerContext getContext(String slamId) {
        SLAManagerContext result = this.contexts.get(slamId);
        LOGGER.info( "requested context :" + result );
        
        return result;
    }
    
    /**
     * Finds the context to a web service by name.
     * @param wsName The web service's name.
     * @return The required SLAM context.
     */
    public SLAManagerContext findContext(String wsName) {
        String slamId = "";
        
        if ( wsName.contains( ""+WSType.Negotiation ) )
            slamId = wsName.replaceAll( ""+WSType.Negotiation, "" );
        
        if ( wsName.contains( ""+WSType.Control ) )
            slamId = wsName.replaceAll( ""+WSType.Control, "" );
        
        if ( wsName.contains( ""+WSType.SLARegistryRegister ) )
            slamId = wsName.replaceAll( ""+WSType.SLARegistryRegister, "" );
        
        if ( wsName.contains( ""+WSType.SLARegistryQuery ) )
            slamId = wsName.replaceAll( ""+WSType.SLARegistryQuery, "" );
        
        if ( wsName.contains( ""+WSType.SLATemplateRegistry ) )
            slamId = wsName.replaceAll( ""+WSType.SLATemplateRegistry, "" );
        
        LOGGER.info( "looking for SLAMContext '" + slamId + "'..." );
        return getContext(slamId);
    }
    
    private static final Logger LOGGER = Logger.getLogger( ContextKeeper.class );
}
