package org.slasoi.gslam.syntaxconverter.webservice.slaregistry.client;

import org.apache.xmlbeans.XmlObject;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAMinimumInfo;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAStateInfo;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.gslam.syntaxconverter.XmlElementParser;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;

import eu.slaatsoi.slamodel.PartyDocument;
import eu.soi.slaAt.gslam.slaRegistry.register.SLAMinimumInfoDocument;
import eu.soi.slaAt.gslam.slaRegistry.register.SLAStateInfoDocument;

public class SLARegistryClientConverter {
    public static SLAMinimumInfo parseSLAMinimumInfo(String minInfoString) throws Exception {
        SLAMinimumInfo minInfo = new SLAMinimumInfo();
        SLAMinimumInfoDocument minInfoXml = SLAMinimumInfoDocument.Factory.parse(minInfoString);

        // agreed at
        minInfo.agreedAt = new TIME(minInfoXml.getSLAMinimumInfo().getAgreedAt());

        // effective from
        minInfo.effectiveFrom = new TIME(minInfoXml.getSLAMinimumInfo().getEffectiveFrom());

        // effective until
        minInfo.effectiveUntil = new TIME(minInfoXml.getSLAMinimumInfo().getEffectiveUntil());

        // parties
        XmlObject[] partyObjects = minInfoXml.getSLAMinimumInfo().getPartyArray();
        Party[] parties = new Party[partyObjects.length];
        for (int j = 0; j < parties.length; j++) {
            XmlElementParser xmlParser = new XmlElementParser();
            PartyDocument partyDoc = PartyDocument.Factory.parse(partyObjects[j].getDomNode());
            parties[j] = xmlParser.parseAgreementParty(partyDoc.getParty());
        }
        minInfo.parties = parties;

        // sla id
        minInfo.SLAID = new UUID(minInfoXml.getSLAMinimumInfo().getSLAID());

        // template id
        minInfo.templateID = new UUID(minInfoXml.getSLAMinimumInfo().getTemplateID());

        return minInfo;
    }

    public static SLAStateInfo parseSLAStateInfo(String stateInfoString) throws Exception {
        SLAStateInfo stateInfo = new SLAStateInfo();
        SLAStateInfoDocument stateInfoDoc;
        stateInfoDoc = SLAStateInfoDocument.Factory.parse(stateInfoString);
        stateInfo.date = stateInfoDoc.getSLAStateInfo().getDate().getTime();
        stateInfo.state = SLAState.valueOf(stateInfoDoc.getSLAStateInfo().getSLAState());
        return stateInfo;
    }

    public static SLA parseSLA(String slaString) throws Exception {
        SLASOIParser slasoiParser = new SLASOIParser();
        return slasoiParser.parseSLA(slaString);
    }
}
