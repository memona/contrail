package org.slasoi.gslam.syntaxconverter.webservice;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ServiceContext;
import org.apache.axis2.service.Lifecycle;
import org.apache.log4j.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.control.IControl;
import org.slasoi.gslam.core.control.Policy;
import org.slasoi.gslam.core.control.PolicyClassType;
import org.slasoi.gslam.syntaxconverter.ContextKeeper;

public class ControlImpl implements IControl, ServiceFactory {

    private SLAManagerContext context;
    private static final Logger LOGGER = Logger.getLogger( ControlImpl.class );

    public ControlImpl() {
    }
    
    public void setContext(SLAManagerContext context) {
    	this.context = context;
    }

    public Policy[] getPolicies(String policyClass) {
        if (policyClass.equals(PolicyClassType.ADJUSTMENT_POLICY)) {
            try {
                return this.context.getProvisioningAdjustment().getPolicies(policyClass);
            }
            catch (SLAManagerContextException e) {
                e.printStackTrace();
                return new Policy[0];
            }
        }
        else if (policyClass.equals(PolicyClassType.NEGOTIATION_POLICY)) {
            try {
                return this.context.getProtocolEngine().getIControl().getPolicies(policyClass);
            }
            catch (SLAManagerContextException e) {
                e.printStackTrace();
                return new Policy[0];
            }
          
            //return new Policy[0];
        }
        else if (policyClass.equals(PolicyClassType.PROVIDER_CONTROL_POLICY)) {
            return new Policy[0];
        }
        else
            return new Policy[0];
    }

    public int setPolicies(String policyClass, Policy[] policies) {
        if (policyClass.equals(PolicyClassType.ADJUSTMENT_POLICY)) {
            try {
                return this.context.getProvisioningAdjustment().setPolicies(policyClass, policies);
            }
            catch (SLAManagerContextException e) {
                e.printStackTrace();
                return -1;
            }
        }
        else if (policyClass.equals(PolicyClassType.NEGOTIATION_POLICY)) {
            try {
                return this.context.getProtocolEngine().getIControl().setPolicies(policyClass, policies);
            }
            catch (SLAManagerContextException e) {
                e.printStackTrace();
                return -1;
            }
            //return -1;
        }
        else if (policyClass.equals(PolicyClassType.PROVIDER_CONTROL_POLICY)) {
            return -1;
        }
        else
            return -1;
    }

    public Object getService(Bundle arg0, ServiceRegistration arg1) {
        String slamId = (String) arg1.getReference().getProperty("org.apache.axis2.osgi.ws");

        this.context = ContextKeeper.getInstance().getContext(slamId);

        return this;
    }

	public void destroy(ServiceContext arg0) {
		// TODO Auto-generated method stub
		
	}

	public void ungetService(Bundle arg0, ServiceRegistration arg1, Object arg2) {
		// TODO Auto-generated method stub
		
	}
}
