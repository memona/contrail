package org.slasoi.gslam.syntaxconverter.webservice;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.ServiceContext;
import org.apache.axis2.service.Lifecycle;
import org.apache.log4j.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.core.negotiation.SLARegistry.RegistrationFailureException;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.core.negotiation.SLARegistry.UpdateFailureException;
import org.slasoi.gslam.syntaxconverter.ContextKeeper;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;

/**
 * Implements the web service for SLA registry's register interaction.
 * @author Peter A. Chronz
 *
 */
public class SLARegistryRegisterImpl implements SLARegistryRegisterAbstraction, ServiceFactory {

    private SLAManagerContext context;
    
    private static final Logger LOGGER = Logger.getLogger( SLARegistryRegisterImpl.class );
    
    public void setContext(SLAManagerContext context) {
    	this.context = context;
    }

    public SLARegistryRegisterImpl() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.gslam.syntaxconverter.webservice.SLARegistryRegisterAbstraction#register(java.lang.String,
     * java.lang.String[], java.lang.String)
     */
    public String register(String agreement, String[] dependencies, String state) throws RegistrationFailureException {
        try {
            // agreement
            ISyntaxConverter synVerter =
                    this.context.getSyntaxConverters().get(SyntaxConverterType.SLASOISyntaxConverter);
            SLA sla = (SLA) synVerter.parseSLA(agreement);

            // dependencies
            UUID[] dependencyUUIDs = new UUID[dependencies.length];
            for (int i = 0; i < dependencyUUIDs.length; i++) {
                dependencyUUIDs[i] = new UUID(dependencies[i]);
            }

            return this.context.getSLARegistry().getIRegister().register(sla, dependencyUUIDs, SLAState.valueOf(state))
                    .getValue();
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.gslam.syntaxconverter.webservice.SLARegistryRegisterAbstraction#update(java.lang.String,
     * java.lang.String, java.lang.String[], java.lang.String)
     */
    public String update(String id, String agreement, String[] dependencies, String state)
            throws UpdateFailureException {
        try {
            // agreement
            ISyntaxConverter synVerter =
                    this.context.getSyntaxConverters().get(SyntaxConverterType.SLASOISyntaxConverter);
            SLA sla = (SLA) synVerter.parseSLA(agreement);

            // dependencies
            UUID[] dependencyUUIDs = new UUID[dependencies.length];
            for (int i = 0; i < dependencyUUIDs.length; i++) {
                dependencyUUIDs[i] = new UUID(dependencies[i]);
            }

            return this.context.getSLARegistry().getIRegister().update(new UUID(id), sla, dependencyUUIDs,
                    SLAState.valueOf(state)).getValue();
        }
        catch (SLAManagerContextException e) {
            e.printStackTrace();
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object getService(Bundle arg0, ServiceRegistration arg1) {
        String slamId = (String) arg1.getReference().getProperty("org.apache.axis2.osgi.ws");

        this.context = ContextKeeper.getInstance().getContext(slamId);

        return this;
    }

	public void destroy(ServiceContext arg0) {
		// TODO Auto-generated method stub
		
	}

	public void ungetService(Bundle arg0, ServiceRegistration arg1, Object arg2) {
		// TODO Auto-generated method stub
		
	}
}
