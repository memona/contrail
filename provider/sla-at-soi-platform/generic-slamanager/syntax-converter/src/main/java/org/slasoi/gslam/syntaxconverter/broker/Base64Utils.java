/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/syntax-converter/src/main/java/org/slasoi/gslam/syntaxconverter/broker/Base64Utils.java $
 */
package org.slasoi.gslam.syntaxconverter.broker;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

import org.apache.commons.codec.binary.Base64;

/**
*
* Helper class for serialization (base64-based) of commands (and their parameters )between 
* the Broker and SyntaxConverter (from OSGi)
* 
* @author Miguel Rojas (UDO)
*
*/
public class Base64Utils {
    
    /**
     * Constructor
     */
    public Base64Utils() {
    }

    /**
     * Returns an encoded object as String
     * 
     * @param obj  Object to be encoded
     * 
     * @return A String representation of the (base64-based) encoded object
     */
    public static String encode(Object obj) {
        String result = null;

        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bytesOutput);
            os.writeObject(obj);
            result = new String(Base64.encodeBase64(bytesOutput.toByteArray()));
            os.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String encode(Vector<?> command) {
        String result = null;

        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bytesOutput);
            os.writeObject(command);
            result = new String(Base64.encodeBase64(bytesOutput.toByteArray()));
            os.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Decoder of a response. This response normally is processed by the SyntaxConverter (under OSGi)
     * for getting results sent by the Broker.
     * 
     * @param response
     * @return
     */
    public static Object decode(String response) {
        Object result = null;

        try {
            ByteArrayInputStream bytesInput = new ByteArrayInputStream(Base64.decodeBase64(response.getBytes()));
            ObjectInputStream is = new ObjectInputStream(bytesInput);
            result = is.readObject();
            is.close();

            return result;
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void main(String[] args) {
        try {
            Vector<String> words = new Vector<String>(3, 2);
            words.add("OSGi");
            words.add("XmlBeans");
            words.add("SyntaxConverter");
            words.add("DukeGmbH");

            String s = Base64Utils.encode(words);
            System.out.println(s);

            System.out.println(Base64Utils.decode(s));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
