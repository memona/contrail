package org.slasoi.gslam.syntaxconverter.webservice;

/**
 * Provides an interface abstraction for SLA template registry to be used as web service. 
 * @author Peter A. Chronz
 *
 */
public interface SLATemplateRegistryAbstraction {

    // Warning[] <-- SLATemplate, Metadata
    public abstract String[] addSLATemplate(String slat, String metadata) throws Exception;

    // UUID[] <-- ConstraintExpr
    public abstract String[] query(String metadataQuery) throws Exception;

    // ResultSet <-- SLATemplate, ConstraintExpr
    public abstract String queryWithTemplate(String query, String metadataConstraint) throws Exception;

    // UUID
    public abstract void removeSLATemplate(String templateId) throws Exception;

}
