/**
 * ExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client;

public class ExceptionException extends java.lang.Exception {

    private org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client.GSLAMSyntaxConverterWSSLATemplateRegistryStub.ExceptionE faultMessage;

    public ExceptionException() {
        super("ExceptionException");
    }

    public ExceptionException(java.lang.String s) {
        super(s);
    }

    public ExceptionException(java.lang.String s, java.lang.Throwable ex) {
        super(s, ex);
    }

    public ExceptionException(java.lang.Throwable cause) {
        super(cause);
    }

    public void setFaultMessage(
            org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client.GSLAMSyntaxConverterWSSLATemplateRegistryStub.ExceptionE msg) {
        faultMessage = msg;
    }

    public org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client.GSLAMSyntaxConverterWSSLATemplateRegistryStub.ExceptionE getFaultMessage() {
        return faultMessage;
    }
}
