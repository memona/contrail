package org.slasoi.gslam.syntaxconverter.webservice.track.client;

import java.rmi.RemoteException;
import java.util.List;

import org.apache.axis2.AxisFault;
import org.slasoi.gslam.syntaxconverter.webservice.track.client.TrackStub.AdjustmentNotificationType;
import org.slasoi.gslam.syntaxconverter.webservice.track.client.TrackStub.GetCustomerPurchaseAuthRequest;
import org.slasoi.gslam.syntaxconverter.webservice.track.client.TrackStub.GetCustomerPurchaseAuthRequestType;
import org.slasoi.gslam.syntaxconverter.webservice.track.client.TrackStub.GetCustomerPurchaseAuthResponse;
import org.slasoi.gslam.syntaxconverter.webservice.track.client.TrackStub.TrackEventRequest;
import org.slasoi.gslam.syntaxconverter.webservice.track.client.TrackStub.TrackEventRequestType;

public class TrackClient {

    private TrackStub stub;

    public TrackClient(String epr) throws AxisFault {
        super();
        this.stub = new TrackStub(epr);
    }

    public int trackEvent(List<AdjustmentNotificationType> violationList) throws RemoteException {
        TrackEventRequest inParam = new TrackEventRequest();
        TrackEventRequestType inParamType = new TrackEventRequestType();
        AdjustmentNotificationType[] violationArray = new AdjustmentNotificationType[violationList.size()];

        for (int i = 0; i < violationList.size(); i++) {
            violationArray[i] = violationList.get(i);
        }

        inParamType.setViolationList(violationArray);
        inParam.setTrackEventRequest(inParamType);

        return this.stub.trackEvent(inParam).getTrackEventResponse().get_return();
    }

    public GetCustomerPurchaseAuthResponse getCustomerPurchaseAuth(long productId, long customerID)
            throws RemoteException {
        GetCustomerPurchaseAuthRequest inParam = new GetCustomerPurchaseAuthRequest();
        inParam.setGetCustomerPurchaseAuthRequest(new GetCustomerPurchaseAuthRequestType());
        inParam.getGetCustomerPurchaseAuthRequest().setCustomerId(customerID);
        inParam.getGetCustomerPurchaseAuthRequest().setProductId(productId);
        return this.stub.getCustomerPurchaseAuth(inParam);
    }
}
