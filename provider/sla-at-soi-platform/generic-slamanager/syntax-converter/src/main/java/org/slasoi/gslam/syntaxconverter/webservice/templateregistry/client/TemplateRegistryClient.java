package org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client;

import org.apache.axis2.AxisFault;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.ISLATemplateRegistryClient;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.ResultSet;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateRenderer;
import org.slasoi.gslam.syntaxconverter.SyntaxConverterDelegator;
import org.slasoi.gslam.syntaxconverter.TemplateRegistryWSDelegator;
import org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client.GSLAMSyntaxConverterWSSLATemplateRegistryStub.AddSLATemplate;
import org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client.GSLAMSyntaxConverterWSSLATemplateRegistryStub.Query;
import org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client.GSLAMSyntaxConverterWSSLATemplateRegistryStub.QueryWithTemplate;
import org.slasoi.gslam.syntaxconverter.webservice.templateregistry.client.GSLAMSyntaxConverterWSSLATemplateRegistryStub.RemoveSLATemplate;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.tools.Validator.Warning;

public class TemplateRegistryClient implements ISLATemplateRegistryClient {

    private GSLAMSyntaxConverterWSSLATemplateRegistryStub stub;
    private TemplateRegistryWSDelegator delegator;
    private SyntaxConverterDelegator synGator;

    public TemplateRegistryClient(String epr) throws AxisFault {
        this.stub = new GSLAMSyntaxConverterWSSLATemplateRegistryStub(epr);
        delegator = new TemplateRegistryWSDelegator();
        this.synGator = new SyntaxConverterDelegator(SyntaxConverterType.SLASOISyntaxConverter);
    }

    public Warning[] addSLATemplate(SLATemplate slat, Metadata metadata) throws Exception {
        AddSLATemplate inParam = new AddSLATemplate();

        if (this.synGator != null) {
            inParam.setSlat(this.synGator.renderSLATemplate(slat));
        }
        else {
            SLASOITemplateRenderer slasoiTemplateRenderer = new SLASOITemplateRenderer();
            inParam.setSlat(slasoiTemplateRenderer.renderSLATemplate(slat));
        }

        if (delegator != null)
            inParam.setMetadata(delegator.renderMetadata(metadata));
        else
            inParam.setMetadata(TemplateRegistryClientConverter.renderMetadata(metadata));

        String[] warningStrings = this.stub.addSLATemplate(inParam).get_return();
        Warning[] warnings = new Warning[warningStrings.length];
        for (int i = 0; i < warnings.length; i++) {
            if (delegator != null)
                warnings[i] = delegator.parseWarning(warningStrings[i]);
            else
                warnings[i] = TemplateRegistryClientConverter.parseWarning(warningStrings[i]);
        }

        return warnings;
    }

    public UUID[] query(ConstraintExpr metadataQuery) throws Exception {
        Query inParam = new Query();

        if (delegator != null) {
            inParam.setMetadataQuery(delegator.renderConstraint(metadataQuery));
        }
        else {
            inParam.setMetadataQuery(new TemplateRegistryClientConverter().renderConstraint(metadataQuery));
        }
        String[] uuidStrings = this.stub.query(inParam).get_return();
        UUID[] uuids = new UUID[uuidStrings.length];
        for (int i = 0; i < uuids.length; i++) {
            uuids[i] = new UUID(uuidStrings[i]);
        }

        return uuids;
    }

    public ResultSet query(SLATemplate query, ConstraintExpr metadataConstraint) throws Exception {
        QueryWithTemplate inParam = new QueryWithTemplate();
        if (this.synGator != null) {
            inParam.setQuery(this.synGator.renderSLATemplate(query));
        }
        else {
            SLASOITemplateRenderer slasoiTemplateRenderer = new SLASOITemplateRenderer();
            inParam.setQuery(slasoiTemplateRenderer.renderSLATemplate(query));
        }

        String metaData = "";
        if (delegator != null) {
            delegator.renderConstraint(metadataConstraint);
        }
        else {
            metaData = new TemplateRegistryClientConverter().renderConstraint(metadataConstraint);
        }
        inParam.setMetadataConstraint(metaData);

        String resultString = this.stub.queryWithTemplate(inParam).get_return();

        if (delegator != null)
            return delegator.parseResultSet(resultString);
        else
            return TemplateRegistryClientConverter.parseResultSet(resultString);
    }

    public void removeSLATemplate(UUID templateId) throws Exception {
        RemoveSLATemplate inParam = new RemoveSLATemplate();

        inParam.setTemplateId(templateId.getValue());

        this.stub.removeSLATemplate(inParam);
    }

}
