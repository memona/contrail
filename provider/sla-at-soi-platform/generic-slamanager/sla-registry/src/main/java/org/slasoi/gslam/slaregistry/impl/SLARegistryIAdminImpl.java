/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (So, 05 Dez 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/sla-registry/src/main/java/org/slasoi/gslam/slaregistry/impl/SLARegistryIQueryImpl.java $
 */

package org.slasoi.gslam.slaregistry.impl;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.slaregistry.impl.db.utils.HibernateUtils;
import org.slasoi.slamodel.primitives.UUID;

/**
 * Helper class of SLARegistry which implements the IAdminUtil services.
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class SLARegistryIAdminImpl implements SLARegistry.IAdminUtils {
    
    public void clearAll(){

        Session s = HibernateUtils.getSession();

        Transaction tx = s.beginTransaction();
        delete( s, "from SlaBasicInfo"  );
        delete( s, "from SlaDependency" );
        delete( s, "from SlaStatus"     );
        delete( s, "from Sla"           );

        tx.commit();
        s.close();
    }
    
    protected void delete( Session s, String clause )
    {
        try
        {
            Query q = s.createQuery( clause );
            List<?> list = q.list();
            if (list.isEmpty()) return;

            Iterator<?> it = list.iterator();
            while (it.hasNext()) {
                s.delete( it.next() );
            }
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }
    
    public void clear( UUID[] slasID ){
        // TODO Auto-generated method stub
        
    }

    public SLAManagerContext getContext() {
        return context;
    }

    public void setContext(SLAManagerContext context) {
        this.context = context;
    }

    private SLAManagerContext context;
    
    public static void main( String[] args )
    {
        new SLARegistryIAdminImpl().clearAll();
    }
}
