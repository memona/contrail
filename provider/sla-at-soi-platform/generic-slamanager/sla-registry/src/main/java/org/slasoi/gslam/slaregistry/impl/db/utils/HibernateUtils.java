/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 1639 $
 * @lastrevision   $Date: 2011-05-10 08:18:29 +0200 (tor, 10 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/main/java/org/slasoi/gslam/slaregistry/impl/db/utils/HibernateUtils.java $
 */

package org.slasoi.gslam.slaregistry.impl.db.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Environment;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.Sla;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaBasicInfo;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaDependency;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatus;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusId;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.SlaStatusRef;

/**
 * Helper class responsible for Hibernate initialization.
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class HibernateUtils {
    private static final Logger LOGGER = Logger.getLogger(HibernateUtils.class);
    private static SessionFactory sessionFactory;
    
    private static void debug(){
        String cp = System.getProperty( "java.class.path" );
        String ps = System.getProperty( "path.separator"  );
        cp = cp.replaceAll( ps, "\n\t" );
        System.out.println( String.format( "HibernateUtils :: CP=\n\t%s", cp ) );
    }

    static {
        debug();
        
        // work around for Hibernate under OSGi
        JavaAssistHelper.linkJavaAssistClassLoader();

        String path = System.getenv("SLASOI_HOME");
        if (path != null) {
            loadResourcesFromEnvironment();
        }
        else {
            loadResourcesFromBundle();
        }
    }

    /**
     * Loads a configuration file from the environment and initiliazes Hibernate environment.
     */
    public static void loadResourcesFromEnvironment() {

        try {
            String path = System.getenv("SLASOI_HOME");
            String hb = path + System.getProperty("file.separator") + "generic-slamanager/sla-registry/db/";
            LOGGER.info("HibernateUtil :: GenericSLAM :: SLARegistry loading configuration from environment...");

            File f = null;
            AnnotationConfiguration ac = new AnnotationConfiguration();
            String session = System.getProperty("gslam.slaregistry.session");
            if (session != null && session.equals("true")) {
                f = new File(hb + "gslam-slaregistry-session.cfg.xml");
                LOGGER.info("\t using : " + f);
                ac.configure(f);
            }
            else {
                f = new File(hb + "gslam-slaregistry.cfg.xml");
                LOGGER.info("\t using : " + f);
                ac.configure(f);
            }

            String dbConfig = getFileAsString(f);

            LOGGER.debug("\t\t ***** " + dbConfig);

            f = new File(hb + "ehcache.xml");
            LOGGER.info("\t using : " + f);
            ac.setProperty(Environment.CACHE_PROVIDER_CONFIG, f.getAbsolutePath());

            ac.addPackage("org.slasoi.gslam.slaregistry.impl.db.hibernate")
                    // the fully qualified package name
                    .addAnnotatedClass(SlaStatusId.class).addAnnotatedClass(Sla.class)
                    .addAnnotatedClass(SlaBasicInfo.class).addAnnotatedClass(SlaDependency.class)
                    .addAnnotatedClass(SlaStatusRef.class).addAnnotatedClass(SlaStatus.class)
                    .addAnnotatedClass(SlaStatusRef.class).setProperty("hibernate.show_sql", "false");

            sessionFactory = (SessionFactory) ac.buildSessionFactory();

            assert (sessionFactory != null) : "Hibernate Session could not be established";

            LOGGER.info("HibernateUtil :: session factory=" + sessionFactory);
            LOGGER.info("HibernateUtil :: GenericSLAM :: SLARegistry loaded!");
        }
        catch (Throwable ex) {
            // Log exception!
            ex.printStackTrace();

            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * Loads a configuration file from the bundle as resource and initiliazes Hibernate environment.
     */
    public static void loadResourcesFromBundle() {

        try {
            LOGGER.info("HibernateUtil :: GenericSLAM :: SLARegistry loading configuration from bundle...");

            AnnotationConfiguration ac = new AnnotationConfiguration();
            String session = System.getProperty("gslam.slaregistry.session");
            if (session != null && session.equals("true"))
                ac.configure("db/gslam-slaregistry-session.cfg.xml");
            else {
                URL resources = HibernateUtils.class.getResource("/db/gslam-slaregistry.cfg.xml");
                ac.configure(resources);
            }

            ac.setProperty(Environment.CACHE_PROVIDER_CONFIG, "db/ehcache.xml");

            sessionFactory =
                    (SessionFactory) ac
                            .addPackage("org.slasoi.gslam.slaregistry.impl.db.hibernate")
                            // the fully qualified package name
                            .addAnnotatedClass(SlaStatusId.class).addAnnotatedClass(Sla.class)
                            .addAnnotatedClass(SlaBasicInfo.class).addAnnotatedClass(SlaDependency.class)
                            .addAnnotatedClass(SlaStatusRef.class).addAnnotatedClass(SlaStatus.class)
                            .addAnnotatedClass(SlaStatusRef.class).setProperty("hibernate.show_sql", "false")
                            .buildSessionFactory();

            assert (sessionFactory != null) : "Hibernate Session could not be established";

            LOGGER.info("HibernateUtil :: GenericSLAM :: SLARegistry loaded!");
        }
        catch (Throwable ex) {
            // Log exception!
            ex.printStackTrace();

            throw new ExceptionInInitializerError(ex);
        }
    }

    protected static String getFileAsString(File f) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            StringBuffer sb = new StringBuffer();
            String line = null;
            while ((line = br.readLine()) != null) {
                if (line.contains("username") || line.contains("password"))
                    sb.append(line);
            }

            return sb.toString();
        }
        catch (Exception e) {
        }

        return "";
    }

    /**
     * Query method for getting a new (hibernate) Session object.
     * 
     * @return the new session
     * 
     * @throws HibernateException
     */
    public static Session getSession() throws HibernateException {
        assert (sessionFactory != null) : "Hibernate Session has not been initialized properly";

        return sessionFactory.openSession();
    }
}
