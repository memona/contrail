/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 1637 $
 * @lastrevision   $Date: 2011-05-10 07:53:20 +0200 (tor, 10 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/sla-registry/src/test/java/org/slasoi/gslam/slaregistry/tests/db/ActivatorTest.java $
 */

package org.slasoi.gslam.slaregistry.tests.db;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import junit.framework.TestCase;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slasoi.gslam.slaregistry.impl.SLARegistryActivator;
import org.slasoi.gslam.slaregistry.impl.db.hibernate.Sla;
import org.slasoi.gslam.slaregistry.impl.db.utils.HibernateUtils;
import org.slasoi.slamodel.sla.SLA;

public class ActivatorTest extends TestCase {

    public void testActivator() {
        try {
            SLARegistryActivator act = new SLARegistryActivator();
            act.start( null );
            act.stop( null );
        }
        catch (Exception e) {
            assertTrue(false);
        }
        
        assertTrue(true);
    }
    
    public void testHibernate()
    {
        try
        {
            HibernateUtils.loadResourcesFromBundle();
        }
        catch (Exception e) {
        }
        try
        {
            System.setProperty( "gslam.slaregistry.session", "true" );
            HibernateUtils.loadResourcesFromEnvironment();
            
            HibernateUtils.getSession();
        }
        catch (Exception e) {
        }
        
        assertTrue(true);
    }
    
    public void testGSR()
    {
        try
        {
            HibernateUtils.loadResourcesFromEnvironment();
            Session s = HibernateUtils.getSession();

            // SLA ------------------------------------------------------------------------
            Query q = s.createQuery("from Sla");
            List<?> list = q.list();
            Vector<SLA> selected = new Vector<SLA>(3, 2);
            Iterator<?> it = list.iterator();
            while (it.hasNext()) {
                Sla curr = (Sla) it.next();
                System.out.println( curr );
            }            
        }
        catch ( Exception e )
        {
        }
        
        assertTrue(true);
    }
}
