Creator	"DbVis"
Whatever	"Stuff"
graph
[
	label	""
	directed	1
	node
	[
		id	0
		graphics
		[
			x	75.0
			y	150.0
			w	158.0
			h	84.67919921875
			type	"rectangle"
			fill	"#FFFFCC"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"sla"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"sla_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	20.0
			y	137.586669921875
		]
		LabelGraphics
		[
			text	"varchar(512)"
			fontSize	10
			fontName	"Helvetica"
			x	87.0
			y	137.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"sla_content"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	20.0
			y	155.837646484375
		]
		LabelGraphics
		[
			text	"longtext"
			fontSize	10
			fontName	"Helvetica"
			x	87.0
			y	155.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"slam_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	20.0
			y	174.088623046875
		]
		LabelGraphics
		[
			text	"varchar(256)"
			fontSize	10
			fontName	"Helvetica"
			x	87.0
			y	174.088623046875
		]
	]
	node
	[
		id	1
		graphics
		[
			x	75.0
			y	300.0
			w	172.0
			h	139.43212890625
			type	"rectangle"
			fill	"#FFFFCC"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"sla_basic_info"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"agreed_at"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	13.0
			y	260.210205078125
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	94.0
			y	260.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"effective_from"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	13.0
			y	278.461181640625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	94.0
			y	278.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"effective_until"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	13.0
			y	296.712158203125
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	94.0
			y	296.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"template_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	13.0
			y	314.963134765625
		]
		LabelGraphics
		[
			text	"varchar(256)"
			fontSize	10
			fontName	"Helvetica"
			x	94.0
			y	314.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"parties"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	13.0
			y	333.214111328125
		]
		LabelGraphics
		[
			text	"longtext"
			fontSize	10
			fontName	"Helvetica"
			x	94.0
			y	333.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"sla_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	13.0
			y	351.465087890625
		]
		LabelGraphics
		[
			text	"varchar(512)"
			fontSize	10
			fontName	"Helvetica"
			x	94.0
			y	351.465087890625
		]
	]
	node
	[
		id	2
		graphics
		[
			x	75.0
			y	4.0
			w	170.0
			h	66.42822265625
			type	"rectangle"
			fill	"#FFFFCC"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"sla_dependency"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"sla_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	14.0
			y	0.712158203125
		]
		LabelGraphics
		[
			text	"varchar(512)"
			fontSize	10
			fontName	"Helvetica"
			x	93.0
			y	0.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"dependencies"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	14.0
			y	18.963134765625
		]
		LabelGraphics
		[
			text	"longtext"
			fontSize	10
			fontName	"Helvetica"
			x	93.0
			y	18.963134765625
		]
	]
	node
	[
		id	3
		graphics
		[
			x	345.0
			y	150.0
			w	146.0
			h	66.42822265625
			type	"rectangle"
			fill	"#FFFFCC"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"sla_status"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"sla_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	296.0
			y	146.712158203125
		]
		LabelGraphics
		[
			text	"varchar(512)"
			fontSize	10
			fontName	"Helvetica"
			x	351.0
			y	146.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"status_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	296.0
			y	164.963134765625
		]
		LabelGraphics
		[
			text	"int(10)"
			fontSize	10
			fontName	"Helvetica"
			x	351.0
			y	164.963134765625
		]
	]
	node
	[
		id	4
		graphics
		[
			x	345.0
			y	282.0
			w	128.0
			h	66.42822265625
			type	"rectangle"
			fill	"#FFFFCC"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"sla_status_ref"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	305.0
			y	278.712158203125
		]
		LabelGraphics
		[
			text	"int(10)"
			fontSize	10
			fontName	"Helvetica"
			x	342.0
			y	278.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"name"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	305.0
			y	296.963134765625
		]
		LabelGraphics
		[
			text	"varchar(256)"
			fontSize	10
			fontName	"Helvetica"
			x	342.0
			y	296.963134765625
		]
	]
	edge
	[
		source	1
		target	0
		label	"FK_sla_basic_info"
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	-1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	2
		target	0
		label	"FK_sla_dependency"
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	1.0
			yTarget	-1.0
		]
	]
	edge
	[
		source	3
		target	4
		label	"FK_status_id"
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	1.0
			yTarget	-1.0
		]
	]
	edge
	[
		source	3
		target	0
		label	"FK_sla_id"
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	1.0
		]
	]
]
