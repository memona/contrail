/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/test/java/org/slasoi/gslam/pac/TaskConfigurationTest.java $
 */

package org.slasoi.gslam.pac;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.slasoi.gslam.pac.config.TaskConfiguration;

import com.thoughtworks.xstream.XStream;

public class TaskConfigurationTest {

    private static final Logger logger = Logger.getLogger(TaskConfigurationTest.class.getName());

    TaskConfiguration config = null;

    @Before
    public void init() {
        config = createConfiguration();
    }

    @Test
    public void testSerialization() {
        // creates an testConfiguration object, serializes it, deserializes the output and checks the new object is
        // equal to the original one

        String str = serialize(config);
        TaskConfiguration configAfter = deserialize(str);

        assertEquals(config.toString(), configAfter.toString());
    }

    public TaskConfiguration createConfiguration() {
        logger.info("Creating new TaskConfiguration...");

        TaskConfiguration config = new TaskConfiguration();
        config.setTaskId("test");
        config.setValue("MAX_NUMBER_CONCURRENT_PLANS", "10");
        config.setValue("RULES_FILE", "/org/slasoi/gslam/pac/manage_T_services.drl");

        return config;
    }

    public String serialize(TaskConfiguration config) {
        logger.info("Serializing...");

        XStream xstream = new XStream();
        xstream.alias(org.slasoi.gslam.pac.config.TaskConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.AgentConfiguration.class);
        String configStr = xstream.toXML(config);

        logger.info(configStr);

        return (configStr);
    }

    public TaskConfiguration deserialize(String configStr) {
        XStream xstream = new XStream();
        xstream.alias(org.slasoi.gslam.pac.config.TaskConfiguration.class.getSimpleName(),
                org.slasoi.gslam.pac.config.TaskConfiguration.class);

        TaskConfiguration taskConfig = new TaskConfiguration();

        try {
            taskConfig = (TaskConfiguration) xstream.fromXML(configStr);
        }

        catch (Exception e) {
            logger.info("Unable to deseserialised Agent Configuration. \n" + e.getMessage());
            e.printStackTrace();
        }

        logger.info(taskConfig);
        return (taskConfig);
    }
}
