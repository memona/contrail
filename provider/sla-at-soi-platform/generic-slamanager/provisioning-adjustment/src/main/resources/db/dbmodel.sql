SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `slasoi_pac` ;
CREATE SCHEMA IF NOT EXISTS `slasoi_pac` DEFAULT CHARACTER SET latin1 ;
USE `slasoi_pac`;

-- -----------------------------------------------------
-- Table `slasoi_pac`.`pac_slaviolation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi_pac`.`pac_slaviolation` ;

CREATE  TABLE IF NOT EXISTS `slasoi_pac`.`pac_slaviolation` (
  `violation_id` VARCHAR(45) NOT NULL ,
  `sla_id` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL ,
  `guaranteeterm_id` VARCHAR(45) NOT NULL ,
  `date_begin` DATETIME NOT NULL ,
  `date_end` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`violation_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi_pac`.`pac_penalty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi_pac`.`pac_penalty` ;

CREATE  TABLE IF NOT EXISTS `slasoi_pac`.`pac_penalty` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `date` DATETIME NOT NULL ,
  `violation_id` VARCHAR(45) NOT NULL ,
  `value` FLOAT NOT NULL ,
  `obligated_party` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `violation_id` (`violation_id` ASC) ,
  CONSTRAINT `violation_id`
    FOREIGN KEY (`violation_id` )
    REFERENCES `slasoi_pac`.`pac_slaviolation` (`violation_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi_pac`.`pac_slawarning`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi_pac`.`pac_slawarning` ;

CREATE  TABLE IF NOT EXISTS `slasoi_pac`.`pac_slawarning` (
  `warning_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `sla_id` VARCHAR(45) NULL DEFAULT NULL ,
  `date_warning` DATETIME NOT NULL ,
  `notification_id` VARCHAR(10) NOT NULL ,
  PRIMARY KEY (`warning_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
