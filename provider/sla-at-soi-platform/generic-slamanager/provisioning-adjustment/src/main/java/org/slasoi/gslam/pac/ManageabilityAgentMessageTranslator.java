/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/ManageabilityAgentMessageTranslator.java $
 */

package org.slasoi.gslam.pac;

import org.apache.log4j.Logger;
import org.slasoi.gslam.pac.events.ManageabilityAgentMessage;
import org.slasoi.gslam.pac.events.Message;

import com.thoughtworks.xstream.XStream;

public class ManageabilityAgentMessageTranslator extends EventTranslator {
    private static final Logger logger = Logger.getLogger(ManageabilityAgentMessageTranslator.class.getName());

    public ManageabilityAgentMessageTranslator() {
        // just for RTTI purposes
    }

    public Message fromXML(String messageStr) {
        logger.debug("ManageabilityAgentMessage from XML");
        logger.debug(messageStr);

        XStream xstream = new XStream();
        xstream.alias(org.slasoi.gslam.pac.events.ManageabilityAgentMessage.class.getSimpleName(),
                org.slasoi.gslam.pac.events.ManageabilityAgentMessage.class);

        Message message = new ManageabilityAgentMessage();

        message = (ManageabilityAgentMessage) xstream.fromXML(messageStr);

        return (message);
    }

    public String toXML(Message message) {
        logger.debug("ManageabilityAgentMessage to XML");
        logger.debug(message);

        XStream xstream = new XStream();
        xstream.alias(org.slasoi.gslam.pac.events.ManageabilityAgentMessage.class.getSimpleName(),
                org.slasoi.gslam.pac.events.ManageabilityAgentMessage.class);
        String messageStr = xstream.toXML(message);
        logger.debug(messageStr);

        return (messageStr);
    }

}
