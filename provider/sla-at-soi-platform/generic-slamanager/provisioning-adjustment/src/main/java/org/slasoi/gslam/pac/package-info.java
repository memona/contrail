/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/package-info.java $
 */

/**
 * SLA@SOI provides a monitoring solution, at software and infrastructure levels, able to detect SLA Violations, 
 * but does not always provide sufficient information for deciding the most appropriate response to a problem. 
 * Such decisions often require additional diagnostic information that explains why a violation has occurred and can, 
 * therefore, indicate what would be an appropriate response action to it. 
 * This is the main objective of the Adjustment Component described in this section, thus helping the service providers 
 * to guarantee their SLA commitments not only by detecting SLA violations once they have occurred but further by anticipating 
 * these violations to trigger the appropriate preventing actions.
 * 
 * The Adjustment functionality aims to minimize the adverse impact on the business level of incidents and problems that are 
 * caused by errors within the lower layers (software and infrastructure), and prevent recurrence of incidents related to 
 * these errors. This module is responsible for investigating the problem, identifying the root cause and resolving the issue. 
 * In case of an SLA violation, the Adjustment module can trigger re-planning, re-configuration and/or alerting to 
 * higher-level SLA. These capabilities are considered to be important in order to guarantee best user perception 
 * preserving underlining resources.
 *
 * The (abstract) ProvisioningAndAdjustment (PAC) component is responsible for executing the plans supplied byPlanningAndOptimisation (<<plan>>).
 *
 * At the SLA Management level, the "execution" of plans is effected by posting "task requests" to the ServiceManager (<<manage_<T>_service>> LINK?), where requests cover:
 *         o provisioning & decommissioning service-instances,
 *          o provisioning & decommissioning of the monitoring infrastructure,
 *          o any adjustments to the above,
 *          o (where relevant) 'pause' and 'resume' of previously requested tasks.
 * The PAC subscribes to the MonitoredEventChannel (<<subscribe_to_event>>) in order to receive notifications of:
 *          o monitored service-instance events (posted by ManageabilityAgents),
 *          o notifications of changes to task status & any other significant events (e.g. the monitoring system going down) posted by the ServiceManager.
 * The PAC can also query the ServiceManager directly for task status information (<<manage_<T>_service>> LINK?).
 * Based on received event notifications, the PAC determines the current status of 'plan execution', and reports this status back to PlanningAndOptimisation (<<plan>>).
 * If required, the PAC also has <<query>> access to all GenericSLAManager registries.
 * The PAC is abstract and must be implemented by domain/layer specific plug-ins.

 * Even when the PAC is domain specific, we have tried to isolate those characteristics that can be shared 
 * for both software and infrastructure layers. With those features, a so called generic PAC has been implemented, 
 * based on an autonomic architecture designed as part of task A3.5 of the project.
 * 
 * The main actors in this architecture are Managed Elements and Agents. Managed Elements are defined as components with direct 
 * access to the resources. Examples of resources are not only software elements from the software landscape 
 * (SOA components, e.g. ORC, the Open Reference Case) but any generic infrastructure resource. 
 * These Managed Elements (MEs) are controlled by an Agent, which provides the ME with autonomic capabilities. 
 * Agent is capable of querying the static and dynamic properties of the Managed Element. An agent can have several tasks assigned to it, 
 * e.g. to receive monitoring events, to analyze incoming information or to execute adjustment actions. 
 * A Task evaluates rules and, when the pre-conditions are met, executes the post-conditions, which in turn are Actions to be 
 * performed on the Managed Elements. 
 * By means of using a generic agent and task implementation, some activities like configuring the agent or the tasks, starting or stopping them all could be implemented once and  only those domain-specific activities (namely the Actions) need to be further worked out.
 * 
 */
package org.slasoi.gslam.pac;

