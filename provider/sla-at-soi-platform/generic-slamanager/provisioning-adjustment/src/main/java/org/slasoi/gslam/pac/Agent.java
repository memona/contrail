/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/provisioning-adjustment/src/main/java/org/slasoi/gslam/pac/Agent.java $
 */

/**
 * 
 */
package org.slasoi.gslam.pac;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.slasoi.gslam.pac.config.AgentConfiguration;
import org.slasoi.gslam.pac.config.Configuration;
import org.slasoi.gslam.pac.config.TaskConfiguration;

/**
 * Actual class implementing an agent. An agent executes a task or set of tasks.
 * 
 * @author Beatriz Fuentes
 * 
 */
public class Agent implements IAgent, Runnable {

    /**
     * Agent identifier
     */
    private String agentId;

    /**
     * List of tasks
     */
    private HashMap<String, Object> tasks = new HashMap<String, Object>();

    /**
     * List of managed elements
     */
    private HashMap<String, ManagedElement> managedElements = new HashMap<String, ManagedElement>();

    /**
     * Logger for this class
     */
    private static Logger logger = Logger.getLogger(Agent.class);

    /**
     * Constructor
     * 
     * @param configFile
     *            filename where the configuration information is stored
     */
    public Agent(String configFile) {
        logger.debug("Creating agent with configuration file: " + configFile);
        Configuration config = new Configuration(configFile);
        configure(config.getAgentConfiguration());
    }

    /**
     * Constructor
     * 
     * @param configuration
     *            object where the configuration information is stored
     */
    public Agent(AgentConfiguration configuration) {
        configure(configuration);
    }

    /**
     * Set the agent identifier
     * 
     * @param id
     *            agent identifier
     */
    public void setAgentId(String id) {
        agentId = id;
    }

    /**
     * Get the agent identifier
     * 
     * @return id agent identifier
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * Gets the tasks attached to this agent
     * 
     * @return the tasks attached to this agent
     */
    public HashMap<String, Object> getTasks() {
        return tasks;
    }

    public HashMap<String, ManagedElement> getManagedElements() {
        return managedElements;
    }

    public void setManagedElements(HashMap<String, ManagedElement> managedElements) {
        this.managedElements = managedElements;
    }

    public void addManagedElement(String managedElementId, Object element) {
        logger.debug("Agent::addManagedElement with id = " + managedElementId);
        ManagedElement managedElement = new ManagedElement(managedElementId, element);
        managedElements.put(managedElementId, managedElement);
    }

    public ManagedElement getManagedElement(String managedElementId) {
        return managedElements.get(managedElementId);
    }

    public boolean canProcessEvent(EventType eventType) {
        logger.debug("Agent " + agentId + " querying event type " + eventType);
        return (findTaskForEvent(eventType) != null);
    }

    public boolean canProcessEvent(Event event) {
        return canProcessEvent(event.getType());
    }

    /**
     * Configure the agent
     * 
     * @param config
     *            object with the agent configuration
     * 
     * @see org.slasoi.gslam.pac.IAgent#configure()
     */
    public void configure(AgentConfiguration config) {
        logger.debug("Agent::configure...");
        agentId = config.getAgentId();
        ArrayList<TaskConfiguration> list = (ArrayList<TaskConfiguration>) config.getTaskConfigurationList();
        for (TaskConfiguration taskConfig : list) {
            String taskName = taskConfig.getClassName();

            try {
                // Create the task instance, using RTTI
                Class<?> taskClass = Class.forName(taskName);
                Object task = taskClass.newInstance();
                ((Task) task).setAgent(this);
                ((Task) task).configure(taskConfig);

                // Add the task instance to the list of tasks
                tasks.put(taskClass.getSimpleName(), task);
                logger.debug("Inserting task " + taskClass.getSimpleName());
            }
            catch (InstantiationException e) {
                logger.info("Error when instantiating the task:  " + e.getMessage());
                e.printStackTrace();
            }
            catch (IllegalAccessException e) {
                logger.info("Cannot access the class: " + e.getMessage());
                e.printStackTrace();
            }
            catch (ClassNotFoundException e) {
                logger.info("Cannot find class " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /**
     * stops all the tasks
     * 
     * @see org.slasoi.gslam.pac.IAgent#stop()
     */
    public void stop() {
        for (Object t : tasks.values())
            ((Task) t).stop();
    }

    /**
     * starts all the tasks
     * 
     * @see org.slasoi.gslam.pac.IAgent#start()
     */
    public void start() {
        run();
    }

    /**
     * run method
     * 
     * @see java.lang.Runnable#run()
     */
    public void run() {
        for (Object t : tasks.values())
            ((Task) t).start();
    }

    /**
     * Receives an event from another agent (or from the PAC itself). Based on the type of the event, decides the task
     * that will handle the event.
     * 
     * @param event
     *            event to be processed
     */
    public void notifyEvent(Event event) {
        logger.debug("Agent::notifyEvent " + event);
        Task task = findTaskForEvent(event.getType());
        if (task != null)
            task.notifyEvent(event);
        else
            logger.debug("ERROR, can not find task for event type " + event.getType());
    }

    private Task findTaskForEvent(EventType eventType) {
        Task task = null;
        boolean found = false;

        Iterator<String> iter = tasks.keySet().iterator();
        while (iter.hasNext() && !found) {
            String taskName = (String) iter.next();
            task = (Task) tasks.get(taskName);
            found = task.canProcessEvent(eventType);
        }

        if (!found)
            task = null;

        return task;
    }

    public boolean managesElement(String id) {
        return managedElements.containsKey(id);
    }

    // All tasks may need translation
    public EventTranslationTask getTranslationTask() {
        EventTranslationTask task = (EventTranslationTask) tasks.get("EventTranslationTask");
        logger.debug("getTranslationTask returns " + task);
        return task;
    }

    public Event query(Event event) {
        Event result = null;
        EventType type = event.getType();
        if (type.equals(EventType.PlanStatusRequest)) {
            Task target = findTaskForEvent(type);
            result = target.query(event);
        }
        return result;
    }

    public String toString() {
        String agent = "Id: " + agentId + "\n";
        agent = agent + "Tasks: \n";
        for (String t : tasks.keySet())
            agent = agent + "   " + t + "\n";

        agent = agent + "Service Managers:  \n";
        for (String t : managedElements.keySet())
            agent = agent + "   " + t + "\n";

        return agent;
    }

}
