/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/test/java/org/slasoi/gslam/monitoring/manager/demos/features/A4SLAMonitoringFeatures.java $
 */

package org.slasoi.gslam.monitoring.manager.demos.features;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import java.util.LinkedList;

import org.slasoi.monitoring.common.features.Basic;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.Event;
import org.slasoi.monitoring.common.features.Function;
import org.slasoi.monitoring.common.features.MonitoringFeature;
import org.slasoi.monitoring.common.features.impl.FeaturesFactoryImpl;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.meta;

/**
 * SLA Component Monitoring Features Test for the A4 example.
 **/
public class A4SLAMonitoringFeatures {
    /** A request event. **/
    private Event requestEvent;
    /** A response event. **/
    private Event responseEvent;
    /** An array of Number type. **/
    private String arrayOfNUMBER = "http://www.slaatsoi.org/types#array_of_NUMBER";
    /** An array of Boolean type. **/
    private String arrayOfBOOLEAN = "http://www.slaatsoi.org/types#array_of_BOOLEAN";

    /**
     * Constructs a set of ComponentMonitoringFeatures.
     * 
     * @return org.slasoi.monitoring.common.features.ComponentMonitoringFeatures[]
     * 
     @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    public final ComponentMonitoringFeatures[] buildTest() {
        FeaturesFactoryImpl ffi = new FeaturesFactoryImpl();
        LinkedList<ComponentMonitoringFeatures> featurestore = new LinkedList<ComponentMonitoringFeatures>();
        featurestore.add(ffi.createComponentMonitoringFeatures());
        featurestore.add(ffi.createComponentMonitoringFeatures());
        featurestore.add(ffi.createComponentMonitoringFeatures());
        ComponentMonitoringFeatures[] cmfeatures = new ComponentMonitoringFeatures[featurestore.size()];

        try {

            // ////////////////////////////////////////////////////////////////////////////////
            // Standard Events
            // ////////////////////////////////////////////////////////////////////////////////
            requestEvent = ffi.createEvent();
            responseEvent = ffi.createEvent();
            requestEvent.setType("REQUEST");
            responseEvent.setType("RESPONSE");

            // ////////////////////////////////////////////////////////////////////////////////
            // Main Components
            // ////////////////////////////////////////////////////////////////////////////////
            cmfeatures[0] = ffi.createComponentMonitoringFeatures();
            cmfeatures[0].setUuid("777e8400-sss2-41d4-a716-406075043333");
            cmfeatures[0].setType("SENSOR");
            cmfeatures[0].setMonitoringFeatures(buildA4Sensor(ffi));

            cmfeatures[1] = ffi.createComponentMonitoringFeatures();
            cmfeatures[1].setUuid("550e8400-e29b-41d4-a716-406075047400");
            cmfeatures[1].setType("REASONER");
            cmfeatures[1].setMonitoringFeatures(buildA4Reasonser1(ffi));

            cmfeatures[2] = ffi.createComponentMonitoringFeatures();
            cmfeatures[2].setUuid("7720e8400-e29b-41d4-a716-406075047222");
            cmfeatures[2].setType("REASONER");
            cmfeatures[2].setMonitoringFeatures(buildA4Reasonser2(ffi));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return cmfeatures;
    }

    /**
     * Build the features for A4 Sensor testing.
     * 
     * @param ffi
     *        a FeaturesFactoryImpl instance
     * @return org.slasoi.monitoring.common.features.MonitoringFeature[]
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeatures
     * 
     **/
    private MonitoringFeature[] buildA4Sensor(final FeaturesFactoryImpl ffi) {
        // ////////////////////////////////////////////////////////////////////////////////
        // SENSOR Features
        // ////////////////////////////////////////////////////////////////////////////////
        // Arrival_Rate Sensor
        Basic arrivalRateSensor = ffi.createPrimitive();
        // BasicMonitoringFeature arrival_rate_sensor = new
        // BasicMonitoringFeature();
        arrivalRateSensor.setName(common.$arrival_rate);
        arrivalRateSensor.setDescription("dummy");
        // add REQUEST events as input to arrival_rate sensor
        Basic[] arrivalRateInputs = new Basic[2];
        arrivalRateInputs[0] = requestEvent;

        // Completion_Time Sensor
        Basic completionTimeSensor = ffi.createPrimitive();
        // BasicMonitoringFeature completion_time_sensor = new
        // BasicMonitoringFeature();
        completionTimeSensor.setName(common.$completion_time);
        completionTimeSensor.setDescription("dummy");
        Basic[] completionTimeSensorInputs = new Basic[2];
        completionTimeSensorInputs[0] = requestEvent;
        completionTimeSensorInputs[1] = responseEvent;

        // VM_Image Sensor
        Basic vmimageSensor = ffi.createPrimitive();
        vmimageSensor.setName("http://www.slaatsoi.org/commonTerms#vm_image");
        vmimageSensor.setDescription("dummy");

        Basic requestSensor = ffi.createPrimitive();
        requestSensor.setName("REQUEST");
        requestSensor.setDescription("Sends Request events for the service and/or operation");

        Basic responseSensor = ffi.createPrimitive();
        responseSensor.setName("RESPONSE");
        responseSensor.setDescription("Sends response events for the service and/or operation");

        LinkedList<MonitoringFeature> cmf1features = new LinkedList<MonitoringFeature>();
        cmf1features.add(arrivalRateSensor);
        cmf1features.add(completionTimeSensor);
        cmf1features.add(vmimageSensor);
        cmf1features.add(requestSensor);
        cmf1features.add(responseSensor);

        MonitoringFeature[] mfs = cmf1features.toArray(new MonitoringFeature[cmf1features.size()]);
        return mfs;
    }

    /**
     * Build the features for A4 Reasoner for testing.
     * 
     * @param ffi
     *        a FeaturesFactoryImpl instance
     * @return org.slasoi.monitoring.common.features.MonitoringFeature[]
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private MonitoringFeature[] buildA4Reasonser1(final FeaturesFactoryImpl ffi)
    {
        // ////////////////////////////////////////////////////////////////////////////////
        // REASONER Features
        // ////////////////////////////////////////////////////////////////////////////////
        // ////////////////////////////////////////////////////////////////////////////////
        // LESS_THAN Reasoner
        // FunctionMonitoringFeature loe_impl = new
        // FunctionMonitoringFeature();
        Function loeImpl = ffi.createFunction();
        loeImpl.setName(core.$less_than);
        loeImpl.setDescription("none");
        Basic[] inputs = new Basic[2];
        Basic input1 = ffi.createPrimitive();
        input1.setName("input1");
        input1.setType(meta.$NUMBER);
        Basic input2 = ffi.createPrimitive();
        input2.setName("input2");
        input2.setType(meta.$NUMBER);
        inputs[0] = input1;
        inputs[1] = input2;
        loeImpl.setInput(inputs);
        Basic output = ffi.createPrimitive();
        output.setName("output1");
        output.setType(meta.$BOOLEAN);
        loeImpl.setOutput(output);
        // ////////////////////////////////////////////////////////////////////////////////
        // GREATER_THAN_OR_EQUALS Reasoner
        Function greImpl = ffi.createFunction();
        // FunctionMonitoringFeature gre_impl = new
        // FunctionMonitoringFeature();
        greImpl.setName(core.$greater_than);
        greImpl.setDescription("none");
        Basic[] greImplInputs = new Basic[2];
        Basic greImplInputs1 = ffi.createPrimitive();
        greImplInputs1.setName("param1");
        greImplInputs1.setType(meta.$NUMBER);
        Basic greImplInputs2 = ffi.createPrimitive();
        greImplInputs1.setName("param2");
        greImplInputs1.setType(meta.$NUMBER);
        greImplInputs[0] = greImplInputs1;
        greImplInputs[1] = greImplInputs2;
        greImpl.setInput(greImplInputs);
        Basic greImplOutput = ffi.createPrimitive();
        greImplOutput.setName(meta.$BOOLEAN);
        greImpl.setOutput(greImplOutput);
        // ////////////////////////////////////////////////////////////////////////////////
        // Availability Reasoner
        Function availImpl = ffi.createFunction();
        availImpl.setName(common.$availability);
        availImpl.setDescription("none");
        Basic[] availImplInput = new Basic[1];
        Basic availImplInput1 = ffi.createPrimitive();
        availImplInput1.setName("param1");
        availImplInput1.setType(arrayOfNUMBER);
        availImplInput[0] = availImplInput1;
        availImpl.setInput(availImplInput);
        Basic availImplOutput = ffi.createPrimitive();
        availImplOutput.setName(meta.$NUMBER);
        availImpl.setOutput(output);
        // ////////////////////////////////////////////////////////////////////////////////
        // meta.NUMBER Equals Reasoner
        Function equalsImpl = ffi.createFunction();
        equalsImpl.setName(core.$equals);
        equalsImpl.setDescription("none");
        Basic[] equalsImplInputs = new Basic[2];
        Basic equalsImplInput1 = ffi.createPrimitive();
        equalsImplInput1.setName("param1");
        equalsImplInput1.setType(meta.$NUMBER);
        Basic equalsImplInput2 = ffi.createPrimitive();
        equalsImplInput2.setName("param2");
        equalsImplInput2.setType(meta.$NUMBER);
        equalsImplInputs[0] = equalsImplInput1;
        equalsImplInputs[1] = equalsImplInput2;
        equalsImpl.setInput(equalsImplInputs);
        Basic equalsImplOutputs = ffi.createPrimitive();
        equalsImplOutputs.setName(meta.$BOOLEAN);
        equalsImpl.setOutput(equalsImplOutputs);

        // ////////////////////////////////////////////////////////////////////////////////
        // meta.TEXT Equals Reasoner
        Function textEqualsImpl = ffi.createFunction();
        // FunctionMonitoringFeature text_equals_impl = new
        // FunctionMonitoringFeature();
        textEqualsImpl.setName(core.$equals);
        textEqualsImpl.setDescription("none");
        Basic[] textEqualsImplInputs = new Basic[2];
        Basic textEqualsImplInput1 = ffi.createPrimitive();
        textEqualsImplInput1.setName("param1");
        textEqualsImplInput1.setType(meta.$TEXT);
        Basic textEqualsImplInput2 = ffi.createPrimitive();
        textEqualsImplInput2.setName("param2");
        textEqualsImplInput2.setType(meta.$TEXT);
        textEqualsImplInputs[0] = textEqualsImplInput1;
        textEqualsImplInputs[1] = textEqualsImplInput2;
        textEqualsImpl.setInput(textEqualsImplInputs);
        Basic textEqualsImplOutput = ffi.createPrimitive();
        textEqualsImplOutput.setName(meta.$BOOLEAN);
        textEqualsImpl.setOutput(textEqualsImplOutput);

        LinkedList<MonitoringFeature> cmf1features = new LinkedList<MonitoringFeature>();
        cmf1features.add(greImpl);
        cmf1features.add(equalsImpl);
        cmf1features.add(availImpl);
        cmf1features.add(textEqualsImpl);

        MonitoringFeature[] mfs = cmf1features.toArray(new MonitoringFeature[cmf1features.size()]);
        return mfs;
     }

    /**
     * Build the features for A4 Reasoner for testing.
     * 
     * @param ffi
     *        a FeaturesFactoryImpl instance
     * @return org.slasoi.monitoring.common.features.MonitoringFeature[]
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private MonitoringFeature[] buildA4Reasonser2(final FeaturesFactoryImpl ffi)
    {
        // ////////////////////////////////////////////////////////////////////////////////
        // MEAN Reasoner
        Function meanImpl = ffi.createFunction();
        // FunctionMonitoringFeature mean_impl = new
        // FunctionMonitoringFeature();
        meanImpl.setName(core.$mean);
        meanImpl.setDescription("none");
        Basic[] meanImplInput = new Basic[1];
        Basic meanImplInput1 = ffi.createPrimitive();
        meanImplInput1.setName("param1");
        meanImplInput1.setType(arrayOfNUMBER);
        meanImplInput[0] = meanImplInput1;
        meanImpl.setInput(meanImplInput);
        Basic meanImplOutput = ffi.createPrimitive();
        meanImplOutput.setName("output1");
        meanImplOutput.setType(meta.$NUMBER);
        meanImpl.setOutput(meanImplOutput);
        // ////////////////////////////////////////////////////////////////////////////////
        // COUNT Reasoner
        Function countImpl = ffi.createFunction();
        countImpl.setName(core.$count);
        countImpl.setDescription("counts a series of any");
        Basic[] countImplInput = new Basic[1];
        Basic countImplInput1 = ffi.createPrimitive();
        countImplInput1.setName("input1");
        countImplInput1.setType(arrayOfNUMBER);
        countImplInput[0] = countImplInput1;
        countImpl.setInput(countImplInput);
        Basic countImplOutput = ffi.createPrimitive();
        countImplOutput.setName("output1");
        countImplOutput.setType(meta.$NUMBER);
        countImpl.setOutput(countImplOutput);
        // ////////////////////////////////////////////////////////////////////////////////
        // SERIES Number Reasoner
        Function seriesImpl = ffi.createFunction();
        // FunctionMonitoringFeature series_impl = new
        // FunctionMonitoringFeature();
        seriesImpl.setName(core.$series);
        seriesImpl.setDescription("none");
        Basic[] seriesImplInputs = new Basic[2];
        Basic seriesImplInputs1 = ffi.createPrimitive();
        seriesImplInputs1.setName("param1");
        seriesImplInputs1.setType(meta.$NUMBER);
        seriesImplInputs[0] = seriesImplInputs1;
        seriesImplInputs[1] = responseEvent;
        seriesImpl.setInput(seriesImplInputs);
        Basic seriesImplOutput = ffi.createPrimitive();
        seriesImplOutput.setName("output1");
        seriesImplOutput.setType(arrayOfNUMBER);
        seriesImpl.setOutput(seriesImplOutput);

        // ////////////////////////////////////////////////////////////////////////////////
        // SERIES Number Reasoner
        Function booleanSeriesImpl = ffi.createFunction();
        // FunctionMonitoringFeature series_impl = new
        // FunctionMonitoringFeature();
        booleanSeriesImpl.setName(core.$series);
        booleanSeriesImpl.setDescription("boolean series reasoner");
        Basic[] booleanSeriesImplInputs = new Basic[1];
        Basic booleanSeriesImplInputs1 = ffi.createPrimitive();
        booleanSeriesImplInputs1.setName("boolean series input");
        booleanSeriesImplInputs1.setType(arrayOfBOOLEAN);
        booleanSeriesImplInputs[0] = booleanSeriesImplInputs1;
        booleanSeriesImpl.setInput(booleanSeriesImplInputs);
        Basic booleanSeriesImplOutput = ffi.createPrimitive();
        booleanSeriesImplOutput.setName("boolean output");
        booleanSeriesImplOutput.setType(meta.$BOOLEAN);
        booleanSeriesImpl.setOutput(booleanSeriesImplOutput);

        LinkedList<MonitoringFeature> cmf1features = new LinkedList<MonitoringFeature>();
        cmf1features.add(meanImpl);
        cmf1features.add(seriesImpl);
        cmf1features.add(countImpl);
        cmf1features.add(booleanSeriesImpl);

        MonitoringFeature[] mfs = cmf1features.toArray(new MonitoringFeature[cmf1features.size()]);
        return mfs;
     }
}
