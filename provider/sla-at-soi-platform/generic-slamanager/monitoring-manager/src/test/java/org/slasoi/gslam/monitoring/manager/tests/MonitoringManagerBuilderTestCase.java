/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/test/java/org/slasoi/gslam/monitoring/manager/tests/MonitoringManagerBuilderTestCase.java $
 */

package org.slasoi.gslam.monitoring.manager.tests;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import junit.framework.TestCase;

import org.slasoi.gslam.core.monitoring.IMonitoringManager;
import org.slasoi.gslam.monitoring.manager.MonitoringManagerBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Test Case for starting the MonitoringManagerBuilder service.
 **/
public class MonitoringManagerBuilderTestCase extends TestCase {
    /** An Application Context. **/
    private ApplicationContext ac;

    /**
     * Configures the XML application context.
     **/
    public final void setUp() {
        ac =
                new FileSystemXmlApplicationContext(
                        "src/main/resources/META-INF/spring/gslam-monitoring-manager-context.xml");
    }

    /**
     * Runs the test case.
     **/
    public final void testMonitoringManagerBuilder() {
        MonitoringManagerBuilder service = (MonitoringManagerBuilder) ac.getBean("monitoringManagerService");
        assertNotNull(service);
        IMonitoringManager mm = service.create();
    }
}
