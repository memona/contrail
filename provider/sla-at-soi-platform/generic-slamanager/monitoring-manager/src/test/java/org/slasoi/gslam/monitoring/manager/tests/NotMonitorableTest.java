/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/test/java/org/slasoi/gslam/monitoring/manager/tests/NotMonitorableTest.java $
 */

package org.slasoi.gslam.monitoring.manager.tests;

import java.io.File;

import junit.framework.TestCase;

import org.slasoi.gslam.monitoring.manager.demos.features.B4MonitoringFeatures;

/**
 * B4 SLA example for the MonitoringManager.
 * 
 * @see org.slasoi.gslam.monitoring.manager.tests.GenericMMTest
 **/
public class NotMonitorableTest extends TestCase {

    /**
     * Start the test case.
     * 
     * @param name
     *            name for test case
     **/
    public NotMonitorableTest(final String name) {
        super(name);
    }

    /**
     * Runs the test case.
     **/
    public final void testMM() {
        String filepath =
                System.getenv("SLASOI_HOME") + File.separator
                        + "..\\..\\generic-slamanager\\monitoring-manager\\src\\test\\java\\org\\slasoi\\gslam"
                        + "\\monitoring\\manager\\demos\\xml\\B6_SLA.xml"; // String

        GenericRun tester = new GenericRun();
        tester.setSLAPath(filepath);
        B4MonitoringFeatures features = new B4MonitoringFeatures();
        tester.setFeatures(features.buildTest());
        tester.setParentName("org.slasoi.gslam.monitoring.manager.tests.NotMonitorable");
        tester.run();
    }

    /**
     * @param args
     *            arguments
     */
    public static void main(final String[] args) {
        NotMonitorableTest tester = new NotMonitorableTest("B4SLATest");
        try {
            tester.testMM();
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
