/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 2264 $
 * @lastrevision   $Date: 2011-06-17 14:16:18 +0200 (pet, 17 jun 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/configuration/MonitoringConfigurationBuilder.java $
 */

package org.slasoi.gslam.monitoring.configuration;

/**
 * @author SLA@SOI (City)

 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 *        JavaDoc: YES
 */

import java.util.Arrays;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.slasoi.gslam.monitoring.manager.impl.MonitoringLogger;
import org.slasoi.gslam.monitoring.reporting.MonitorabilityReport;
import org.slasoi.gslam.monitoring.reporting.ReportEntry;
import org.slasoi.gslam.monitoring.reporting.ReportEntry.Result;
import org.slasoi.gslam.monitoring.selection.Selection;
import org.slasoi.gslam.monitoring.spec.slasoi.UniqueID;
import org.slasoi.monitoring.common.configuration.Component;
import org.slasoi.monitoring.common.configuration.ComponentConfiguration;
import org.slasoi.monitoring.common.configuration.EffectorConfiguration;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.configuration.OutputReceiver;
import org.slasoi.monitoring.common.configuration.ReasonerConfiguration;
import org.slasoi.monitoring.common.configuration.SensorConfiguration;
import org.slasoi.monitoring.common.configuration.impl.ComponentImpl;
import org.slasoi.monitoring.common.configuration.impl.ConfigurationFactoryImpl;
import org.slasoi.monitoring.common.configuration.impl.MonitoringSystemConfigurationImpl;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.SLA;

/**
 * The MonitoringConfigurationBuilder supports building monitoring system configurations.
 * <p>
 * It extends the MonitoringgetLogger() class to support logging of progress
 **/
public class MonitoringConfigurationBuilder extends MonitoringLogger {

    /***
     * Local list of component configurations.
     **/
    private List<ComponentConfiguration> componentconfigs = new LinkedList<ComponentConfiguration>();
    /***
     * Map of component configurations.
     **/
    private HashMap<String, List> componentconfigsmap = new HashMap<String, List>();
    /***
     * List of feature components.
     **/
    private List<ComponentMonitoringFeatures> componentMonitoringFeatures;
    /***
     * Feature component types.
     **/
    private HashMap<String, String> componentTypes = new HashMap<String, String>();
    /** Reporting. **/
    private MonitorabilityReport reporter = new MonitorabilityReport();
    /** SLA ID. **/
    private UUID slaid;

    /***
     * MonitoringConfigurationBuilder constructor.
     * 
     * <p>
     * Creates a new instance of a MonitoringConfigurationBuilder class, initialises logging and configuration maps
     * 
     * 
     @see org.slasoi.gslam.monitoring.manager.impl.MonitoringgetLogger()
     **/
    public MonitoringConfigurationBuilder() {
        initLogging(this.getClass().getName());
        componentconfigsmap.clear();
        componentTypes.clear();
    }

    /**
     * Get the report from building the monitoring system configuration.
     * 
     @return MonitorabilityReport
     **/
    public final MonitorabilityReport getReporter() {
        return reporter;
    }

    /**
     * Adds a component configuration to the Monitoring Configuration list.
     * 
     * <p>
     * Since a component may have one or more configurations, this function checks whether the component already has
     * configurations and if so, will add the passed config as a new configuration to the list
     * 
     * @param uUID
     *            the unique identifier for the component
     * 
     @param config
     *            the component configuration
     * 
     @see org.slasoi.monitoring.common.configuration.ComponentConfiguration
     **/
    @SuppressWarnings("unchecked")
    private void addComponentConfiguration(final String uUID, final ComponentConfiguration config) {
        if (!componentconfigsmap.containsKey(uUID)) {
            // create a new config list
            LinkedList<ComponentConfiguration> configlist = new LinkedList<ComponentConfiguration>();
            componentconfigsmap.put(uUID, configlist);
        }
        // get the existing configurations list
        LinkedList<ComponentConfiguration> configlist =
                (LinkedList<ComponentConfiguration>) componentconfigsmap.get(uUID);
        if (configlist != null) {
            configlist.add(config);
        }
    }

    /**
     * For each selection in a List of Selections.
     * 
     * <p>
     * Creates a new instance of a MonitoringConfigurationBuilder class, initialises logging and configuration maps
     * 
     * @param selections
     *            a list of component selections
     * @param features
     *            an array of component monitoring features
     * @param slaUUID
     *            the id of the SLA
     * 
     @see org.slasoi.gslam.monitoring.selection.Selection
     **/
     public final void processSelections(final List<Selection> selections, final ComponentMonitoringFeatures[] features,
        final UUID slaUUID) {
        logLine();
        getLogger().debug("Processing selections for MonitoringSystemConfiguration (MSC)");
        logLine();

        slaid = slaUUID;

        componentconfigsmap.clear();
        componentMonitoringFeatures = Arrays.asList(features);
        // process the features to get each component type
        // get the type of the component from the feature components
        if (componentMonitoringFeatures != null) {
            //iterate through the components to find UUID
            Iterator<ComponentMonitoringFeatures> featureIT = componentMonitoringFeatures.iterator();
            while (featureIT.hasNext()) {
                ComponentMonitoringFeatures feature = featureIT.next();
                componentTypes.put(feature.getUuid(), feature.getType());
            }
        }
        // add all the selected features to the configuration builder
        // Set<String> selectedids = selectedFeatures.keySet();
        Iterator<Selection> selectedidsit = selections.iterator();
        while (selectedidsit.hasNext()) {
            Selection aselection = selectedidsit.next();
            addSpecificationComponentMonitoringFeature(aselection, null);
        }
        logLine();
    }

    /**
     * Adds a monitoring component feature for a component selection to a component configuration.
     * 
     * @param selection
     *            a component selection
     * 
     @param parent
     *            a component configuration
     * 
     @see org.slasoi.gslam.monitoring.selection.Selection
     * @see org.slasoi.monitoring.common.configuration.ComponentConfiguration
     **/
    public final void addSpecificationComponentMonitoringFeature(final Selection selection,
            final ComponentConfiguration parent) {
        if (selection.getComponentMonitoringFeatures().size() > 0) {
            for (ComponentMonitoringFeatures component : selection.getComponentMonitoringFeatures()) {
                getLogger().debug(
                        "Processing configuration for " + component.getUuid() + " (" + component.getType() + ")");

                ComponentConfiguration config = null;

                // get the type of CMF and select appropriate builder
                if (component.getType().equals("REASONER")) {
                    config = (ComponentConfiguration) buildReasoningComponentConfiguration(selection, parent);
                    if (selection.getLHSSelection() != null) {
                        addSpecificationComponentMonitoringFeature(selection.getLHSSelection(), config);
                    }
                    if (selection.getRHSSelection() != null) {
                        addSpecificationComponentMonitoringFeature(selection.getRHSSelection(), config);
                    }
                    // check if there are any events required
                    if (selection.getEvents().size() > 0) {
                        Iterator<Selection> eventit = selection.getEvents().iterator();
                        while (eventit.hasNext()) {
                            addSpecificationComponentMonitoringFeature(eventit.next(), config);
                        }
                    }
                } else if (component.getType().equals("EFFECTOR")) {
                    config = buildEffectorComponentConfiguration(component);
                } else if (component.getType().equals("SENSOR")) {
                    config = buildSensorComponentConfiguration(selection, component, parent);
                }

                // add configuration to list
                if (config != null) {
                    componentconfigs.add(config);
                }

                // add to component configs list
                addComponentConfiguration(component.getUuid(), config);
            }
        }
    }

    /**
     * Builds a new Effector monitoring component feature configuration.
     * 
     * @param cmf
     *            a set of ComponentMonitoringFeatures
     * @return EffectorConfiguration
     * 
     @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * @see org.slasoi.monitoring.common.configuration.EffectorConfiguration
     **/
    private EffectorConfiguration buildEffectorComponentConfiguration(final ComponentMonitoringFeatures cmf) {
        getLogger().debug("Building new EffectorComponentConfiguration");
        ConfigurationFactoryImpl cfi = new ConfigurationFactoryImpl();
        EffectorConfiguration effector = cfi.createEffectorConfiguration();
        effector.setConfigurationId(cmf.getUuid());
        return effector;
    }

    /**
     * Builds a new Reasoner monitoring component feature configuration.
     * 
     * @param selection
     *            a selection
     @param parent
     *            a parent component configuration
     * 
     @return ReasonerConfiguration
     * 
     @see org.slasoi.gslam.monitoring.selection.Selection
     * @see org.slasoi.monitoring.common.configuration.ReasonerConfiguration
     **/
    public final ReasonerConfiguration buildReasoningComponentConfiguration(final Selection selection,
            final ComponentConfiguration parent) {
        int receivers = 0;
        String parentid = "";
        if (parent != null) {
            parentid = parent.getConfigurationId();
        }
        getLogger().debug("Building new ReasoningConfiguration.");
        getLogger().debug("Description: " + selection.getDescription());
        ConfigurationFactoryImpl cfi = new ConfigurationFactoryImpl();
        ReasonerConfiguration reasoner = cfi.createReasonerConfiguration();
        UniqueID uid = new UniqueID();
        reasoner.setConfigurationId(uid.getUniqueID("RC"));
        getLogger().debug("ReasonerConfiguration generated ID: " + reasoner.getConfigurationId());

        if (selection.getParent() != null) {
            OutputReceiver[] outputreceivers = new OutputReceiver[1];
            OutputReceiver outreceiver1 = cfi.createOutputReceiver();
            outreceiver1.setUuid(selection.getParent().getComponentID());
            outreceiver1.setEventType("COMPUTATION");
            outputreceivers[0] = outreceiver1;
            receivers++;
            reasoner.setOutputReceivers(outputreceivers);
            getLogger().debug("ReasonerConfiguration output receiver set to: " + outreceiver1.getUuid());
        }

        // do something with the specification
        reasoner.setSpecification(selection.getSpecification());

        getLogger().debug("New ReasonserConfiguration built with " + receivers + " receivers.");
        // build a report data entry
        reporter.addReport(slaid.toString(), reporter.buildEntry(parentid, reasoner.getConfigurationId(),
                ReportEntry.Type.CONFIG, ReportEntry.Step.CONFIG_REASONER,
                selection.getDescription(), "COMPUTATION", Result.DEFAULT));

        //reporter.addReport(slaid.toString(),reporter.buildEntry(parent.getConfigurationId(),
        //        reasoner.getConfigurationId(), ReportEntry.Type.CONFIG,ReportEntry.Step.CONFIG_SENSOR,
        //       "Reasoner Output Parent", selection.getDescription(), Result.DEFAULT));

        return reasoner;
    }

    /**
     * Builds a new Sensor monitoring component feature configuration.
     * 
     * @param selection
     *            a selection
     * 
     @param cmf
     *            a set of ComponentMonitoringFeatures
     * 
     @param parent
     *            a component configuration
     * 
     @return SensorConfiguration
     * 
     @see org.slasoi.gslam.monitoring.selection.Selection
     * @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * @see org.slasoi.monitoring.common.configuration.ComponentConfiguration
     * @see org.slasoi.monitoring.common.configuration.SensorConfiguration
     **/
    public final SensorConfiguration buildSensorComponentConfiguration(final Selection selection,
            final ComponentMonitoringFeatures cmf, final ComponentConfiguration parent) {
        ConfigurationFactoryImpl cfi = new ConfigurationFactoryImpl();
        SensorConfiguration sensor = cfi.createSensorConfiguration();
        UniqueID uid = new UniqueID();
        sensor.setConfigurationId(uid.getUniqueID("SC"));
        getLogger().debug("SensorConfiguration generated ID: " + sensor.getConfigurationId());
        getLogger().debug("Description: " + selection.getDescription());
        getLogger().debug("Sensor Event Type: " + selection.getName());
        if (selection.getComponentMonitoringFeatures().size() > 0) {
            sensor.setConfiguredFeature(selection.getComponentMonitoringFeatures().get(0).getMonitoringFeatures(0));
        }

        if (selection.getParent() != null) {
            OutputReceiver[] outputreceivers = new OutputReceiver[1];
            OutputReceiver outreceiver1 = cfi.createOutputReceiver();
            outreceiver1.setUuid(selection.getParent().getComponentID());
            outreceiver1.setEventType(selection.getName());
            outputreceivers[0] = outreceiver1;
            sensor.setOutputReceivers(outputreceivers);
            getLogger().debug("SensorConfiguration output receiver set to: " + outreceiver1.getUuid());
        }

        int receivers = 0;
        if (sensor.getOutputReceivers() != null) {
            receivers = sensor.getOutputReceiversLength();
        }

        getLogger().debug(sensor.getConfigurationId() + " built with " + receivers + " receivers.");

        reporter.addReport(slaid.toString(), reporter.buildEntry(parent.getConfigurationId(),
                sensor.getConfigurationId(), ReportEntry.Type.CONFIG, ReportEntry.Step.CONFIG_SENSOR,
                selection.getDescription(), selection.getName(), Result.DEFAULT));

        return sensor;
    }

    /**
     * Get the configuration for an SLA by UDDI.
     * 
     * @param slaUddi
     *            the UUID of the SLA to retrieve configuration
     * 
     @return org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration
     **/
    @SuppressWarnings("unchecked")
    public final MonitoringSystemConfiguration getConfiguration(final UUID slaUddi) {
        getLogger().debug("Generating MSC for " + slaUddi);
        ConfigurationFactoryImpl cfi = new ConfigurationFactoryImpl();
        MonitoringSystemConfigurationImpl monitorconfig =
                (MonitoringSystemConfigurationImpl) cfi.createMonitoringSystemConfiguration();
        slaid = slaUddi;
        monitorconfig.setUuid(slaUddi.toString());
        int noofcomps = componentconfigsmap.keySet().size();
        int compno = 0;
        ComponentImpl[] comparray = new ComponentImpl[noofcomps];
        // loop through the component list and generate a single MSC
        Iterator<String> uuidit = componentconfigsmap.keySet().iterator();
        while (uuidit.hasNext()) {
            String uuidkey = (String) uuidit.next();
            getLogger().debug(uuidkey);
            ComponentImpl newcomp = (ComponentImpl) cfi.createComponent();
            newcomp.setUuid(uuidkey);
            // get the type for this component from the features map
            if (componentTypes.containsKey(uuidkey)) {
                newcomp.setType(componentTypes.get(uuidkey));
            }
            LinkedList<ComponentConfiguration> cclist =
                    (LinkedList<ComponentConfiguration>) componentconfigsmap.get(uuidkey);
            ComponentConfiguration[] ccconfigs =
                    (ComponentConfiguration[]) cclist.toArray(new ComponentConfiguration[cclist.size()]);
            newcomp.setConfigurations(ccconfigs);
            comparray[compno++] = newcomp;
        }
        monitorconfig.setComponents(comparray);
        getLogger().debug("MSC Generated with " + comparray.length + " components.");

        return monitorconfig;
    }

    /**
     * Generates a trace through a MonitoringSystemConfiguration instance.
     * 
     * @param config
     *            an instance of a MonitoringSystemConfiguration
     * 
     @see org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration
     **/
    public final void printMonitoringSystemConfiguration(final MonitoringSystemConfiguration config) {
        this.logLine();
        getLogger().debug("START Trace of MonitoringSystemConfiguration: " + config.getUuid());
        this.logLine();

        getLogger().debug("Components: " + config.getComponentsLength());

        List<Component> listofComponents = Arrays.asList(config.getComponents());
        Iterator<Component> componentit = listofComponents.iterator();
        while (componentit.hasNext()) {
            Component component = (Component) componentit.next();
            logAsteriskLine();
            getLogger().debug(
                    "Component UUID: " + component.getUuid() + " has " + component.getConfigurations().length
                            + " configurations.");
            logAsteriskLine();
            List<ComponentConfiguration> listOfConfigs = Arrays.asList(component.getConfigurations());
            Iterator<ComponentConfiguration> cconfigs = listOfConfigs.iterator();
            while (cconfigs.hasNext()) {
                Object configelem = (Object) cconfigs.next();
                if (configelem instanceof ReasonerConfiguration) {
                    ReasonerConfiguration rcc = (ReasonerConfiguration) configelem;
                    getLogger().debug("ReasoningConfiguration ID: " + rcc.getConfigurationId());
                    boolean hasSpec = false;
                    if (rcc.getSpecification() != null) {
                        hasSpec = true;
                    }
                    getLogger().debug(rcc.getConfigurationId() + " : hasSpecification : " + hasSpec);
                    logReceivers(rcc.getConfigurationId().toString(), rcc.getOutputReceiversList());
                    previewSpecification(rcc);
                } else if (configelem instanceof SensorConfiguration) {
                    SensorConfiguration sc = (SensorConfiguration) configelem;
                    getLogger().debug("SensorConfiguration ID: " + sc.getConfigurationId());
                    logReceivers(sc.getConfigurationId().toString(), sc.getOutputReceiversList());
                }
            }
        }

        this.logLine();
        getLogger().debug("END Trace of MonitoringSystemConfiguration: " + config.getUuid());
        this.logLine();
    }

    /**
     * Outputs a trace of receivers for a configuration id and a list of receivers.
     * 
     * @param configid
     *            the id of the configuration component
     * 
     @param orlist
     *            a list of OutputReceiver
     * 
     @see org.slasoi.monitoring.common.configuration.OutputReceiver
     **/
    private void logReceivers(final String configid, final List<OutputReceiver> orlist) {
        Iterator<OutputReceiver> orit = orlist.iterator();
        while (orit.hasNext()) {
            OutputReceiver or = orit.next();
            getLogger().debug(configid + ", OutputReceiverId : " + or.getUuid() + ", Type : " + or.getEventType());
        }

        if (orlist.size() == 0) {
            getLogger().debug(configid + " has no output receivers.");
        }
    }

    /**
     * Generates a preview of the specification for a Reasoner configuration component.
     * 
     * @param rcc
     *            a ReasonserConfiguration
     * 
     @see org.slasoi.monitoring.common.configuration.ReasonerConfiguration
     **/
    private void previewSpecification(final ReasonerConfiguration rcc) {
        if (rcc.getSpecification() != null) {
            SLA specmodel = (SLA) rcc.getSpecification();
            AgreementTerm term = null;
            if (specmodel.getAgreementTerms() != null && specmodel.getAgreementTerms().length > 0) {
                for (AgreementTerm aterm : specmodel.getAgreementTerms()) {
                    if (aterm != null) {
                        term = aterm;
                    }
                }

                if (term != null) {
                    int noofgterms = 0;
                    if (term.getGuarantees() != null) {
                        noofgterms = term.getGuarantees().length;
                    }
                    getLogger().debug("-Specification: AgreementTermID: " + term.getId() + ", GTerms: " + noofgterms);
                } else {
                    getLogger().error("-Specification: No AgreementTerms found.");
                }
            }
        } else {
            getLogger().error("-Specification: No specification found.");
        }
    }
}
