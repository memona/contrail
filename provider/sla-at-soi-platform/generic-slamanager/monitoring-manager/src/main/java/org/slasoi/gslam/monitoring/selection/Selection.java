/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 853 $
 * @lastrevision   $Date: 2011-02-28 20:49:33 +0100 (pon, 28 feb 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/selection/Selection.java $
 */

package org.slasoi.gslam.monitoring.selection;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import java.util.LinkedList;
import java.util.List;

/**
 * A Selection represents a candidate component and configuration for a MonitoringComponent.
 * 
 **/
public class Selection {

    /** Name of selection. **/
    private String name = "";
    /** SLA model element specification for selection. **/
    private Object specification = null;
    /** List of monitoring features. **/
    private List<ComponentMonitoringFeatures> cmfs = null;
    /** Left-hand side of selection. **/
    private Selection lhs = null;
    /** Right-hand side of selection. **/
    private Selection rhs = null;
    /** Events associated with this selection. **/
    private List<Selection> events = null;
    /** Indicator whether this selection is a parent. **/
    private boolean bIsRoot = false;
    /** Parent of this selection. **/
    private Selection parent = null;
    /** The selection component ID. **/
    private String componentID = null;
    /** Description of selection. **/
    private String desc = "";
    /** Type of selection. **/
    private String type = "";
    /** ID selection term. **/
    private String selID = "";

    /**
     * Constructor.
     **/
    public Selection() {
        events = new LinkedList<Selection>();
        cmfs = new LinkedList<ComponentMonitoringFeatures>();
        bIsRoot = false;
    }

    /**
     * Sets the id of selection.
     * 
     * @param sID
     *            the id of selection
     * 
     **/
    public final void setID(final String sID) {
        selID = sID;
    }

    /**
     * Sets the type of selection.
     * 
     * <p>
     * The type of selection can be either SENSOR, REASONER or EFFECTOR
     * 
     * @param stype
     *            the type of selection
     * 
     **/
    public final void setType(final String stype) {
        type = stype;
    }

    /**
     * Returns the ID of selection.
     * 
     * @return String - the ID of selection
     * 
     **/
    public final String getID() {
        return selID;
    }

    /**
     * Retrieve the type of selection.
     * 
     * <p>
     * The type of selection can be either SENSOR, REASONER or EFFECTOR
     * 
     * @return String - the type of selection
     * 
     **/
    public final String getType() {
        return type;
    }

    /**
     * Sets the description of the selection.
     * 
     * @param description
     *            the description of selection
     * 
     **/
    public final void setDescription(final String description) {
        desc = description;
    }

    /**
     * Gets the description of the selection.
     * 
     * @return String - the description of selection
     * 
     **/
    public final String getDescription() {
        return desc;
    }

    /**
     * Sets the component id of the selection.
     * 
     * @param id
     *            the id of component
     * 
     **/
    public final void setComponentID(final String id) {
        componentID = id;
    }

    /**
     * Retrieve the ID of the selection component.
     * 
     * @return String - the ID of selection component
     * 
     **/
    public final String getComponentID() {
        return componentID;
    }

    /**
     * Sets a parent selection of this selection.
     * 
     * @param aparent
     *            the parent selection
     * 
     **/
    public final void setParent(final Selection aparent) {
        parent = aparent;
    }

    /**
     * Retrieve the parent selection of this selection.
     * 
     * @return Selection - the parent selection
     * 
     **/
    public final Selection getParent() {
        return parent;
    }

    /**
     * Sets a name for this selection.
     * 
     * @param aname
     *            the name of this selection
     * 
     **/
    public final void setName(final String aname) {
        name = aname;
    }

    /**
     * Get the name of this selection.
     * 
     * @return String - the name of selection
     * 
     **/
    public final String getName() {
        return name;
    }

    /**
     * Sets a specification for this selection.
     * 
     * @param aspec
     *            an object of the specification
     * 
     **/
    public final void setSpecification(final Object aspec) {
        specification = aspec;
    }

    /**
     * Get the specification of this selection.
     * 
     * @return Object - the specification assigned to this specification
     * 
     **/
    public final Object getSpecification() {
        return specification;
    }

    /**
     * Sets a set of component monitoring features for this specification.
     * 
     * @param acmf
     *            a list of component monitoring features
     * 
     **/
    public final void setComponentMonitoringFeatures(final List<ComponentMonitoringFeatures> acmf) {
        cmfs = acmf;
    }

    /**
     * Get the list of Component Monitoring Features for this selection.
     * 
     * @return List<org.slasoi.monitoring.common.features.ComponentMonitoringFeatures >
     * 
     @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    public final List<ComponentMonitoringFeatures> getComponentMonitoringFeatures() {
        return cmfs;
    }

    /**
     * Adds a component monitoring feature to the list of cmfs.
     * 
     * @param cmf
     *            a component monitoring feature
     * 
     **/
    public final void addComponentMonitoringFeature(final ComponentMonitoringFeatures cmf) {
        cmfs.add(cmf);
    }

    /**
     * Get the Left-hand side selection of this selection.
     * 
     * @return Selection
     * 
     **/
    public final Selection getLHSSelection() {
        return lhs;
    }

    /**
     * Set the selection for the LHS of this selection.
     * 
     * @param aselection
     *            a selection
     * 
     **/
    public final void setLHSSelection(final Selection aselection) {
        lhs = aselection;
    }

    /**
     * Set the selection for the RHS of this selection.
     * 
     * @param aselection
     *            a selection
     * 
     **/
    public final void setRHSSelection(final Selection aselection) {
        rhs = aselection;
    }

    /**
     * Get the Right-hand side selection of this selection.
     * 
     * @return Selection
     * 
     **/
    public final Selection getRHSSelection() {
        return rhs;
    }

    /**
     * Get a list of selected events associated with this selection.
     * 
     * @return List<Selection>
     * 
     **/
    public final List<Selection> getEvents() {
        return events;
    }

    /**
     * Set the events (selections) for this selection.
     * 
     * @param list
     *            a list of selection (events)
     * 
     **/

    public final void setEvents(final List<Selection> list) {
        events = list;
    }

    /**
     * Add an event (selection) for this selection.
     * 
     * @param eventsel
     *            an event selection
     * 
     **/
    public final void addEventSelection(final Selection eventsel) {
        events.add(eventsel);
    }

    /**
     * Determines whether this is a root (parent) selection.
     * 
     * Returns: True - it is a root, False - it is not a root
     * 
     * @return boolean
     * 
     **/
    public final boolean getIsRootSelection() {
        return bIsRoot;
    }

    /**
     * Set whether this a root selection.
     * 
     * @param isRoot
     *            boolean of root status (true-is root, false-is not root)
     * 
     **/
    public final void setIsRootSelection(final boolean isRoot) {
        bIsRoot = isRoot;
    }
}
