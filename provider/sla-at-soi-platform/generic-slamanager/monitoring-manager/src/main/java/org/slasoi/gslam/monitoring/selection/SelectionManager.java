/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 2264 $
 * @lastrevision   $Date: 2011-06-17 14:16:18 +0200 (pet, 17 jun 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/
 * generic-slamanager/monitoring-manager/src/main/java/org/slasoi/gslam/monitoring/selection/SelectionManager.java $
 */

package org.slasoi.gslam.monitoring.selection;

/**
 * @author SLA@SOI (City)
 * @date Mar 23, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

import org.slasoi.gslam.monitoring.manager.impl.MonitoringLogger;
import org.slasoi.gslam.monitoring.parser.ASTTermMap;
import org.slasoi.gslam.monitoring.parser.core.ASTAgreement;
import org.slasoi.gslam.monitoring.parser.core.ASTComparator;
import org.slasoi.gslam.monitoring.parser.core.ASTFunctionParameter;
import org.slasoi.gslam.monitoring.parser.core.ASTFunctionParameters;
import org.slasoi.gslam.monitoring.parser.core.ASTGuaranteeTerm;
import org.slasoi.gslam.monitoring.parser.core.ASTTerm;
import org.slasoi.gslam.monitoring.parser.core.ASTTermFunction;
import org.slasoi.gslam.monitoring.parser.core.ASTTermType;
import org.slasoi.gslam.monitoring.parser.core.Node;
import org.slasoi.gslam.monitoring.parser.core.Token;
import org.slasoi.gslam.monitoring.reporting.MonitorabilityReport;
import org.slasoi.gslam.monitoring.reporting.ReportEntry;
import org.slasoi.gslam.monitoring.reporting.ReportEntry.Result;
import org.slasoi.gslam.monitoring.spec.maps.EventTypesMap;
import org.slasoi.gslam.monitoring.spec.maps.OperationMap;
import org.slasoi.gslam.monitoring.spec.maps.ResultTypeMapping;
import org.slasoi.gslam.monitoring.spec.maps.TermEvents;
import org.slasoi.gslam.monitoring.spec.slasoi.SLAModelUtils;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.Event;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.units;

/**
 * Manages the building of a set of component selections and configurations.
 **/
public class SelectionManager extends MonitoringLogger {

    /** A mapping of AST Agreement Terms. **/
    private ASTTermMap terms = null;
    /** Selector for monitoring components. **/
    private ComponentSelector selector = null;
    /** Operation Mapping. **/
    private OperationMap opmap = new OperationMap();
    /** List of Selected components. **/
    private List<Selection> selected = new LinkedList<Selection>();
    /** Current SLA model. **/
    private SLA currModel = null;
    /** Event Map. **/
    private EventTypesMap eventMap = new EventTypesMap();
    /** SLA Model Utilities. **/
    private SLAModelUtils sLAutils = new SLAModelUtils();
    /** Monitorable Flag. **/
    private boolean monitorable = false;
    /** Reporting. **/
    private MonitorabilityReport selreporter = new MonitorabilityReport();

    /**
     * Constructor - initialises logging, configures map and assigns available features.
     * 
     * @param map
     *            the AST map of Agreement Term nodes
     * @param componentMonitoringFeatures
     *            an array of componentMonitoringFeatures
     * @param slaModel
     *            the instance of the SLA
     * 
     **/
    public SelectionManager(final ASTTermMap map, final ComponentMonitoringFeatures[] componentMonitoringFeatures,
            final SLA slaModel) {
        // initialise logging
        initLogging(this.getClass().getName());

        terms = map;
        currModel = slaModel;
        selector = new ComponentSelector(componentMonitoringFeatures);
    }

    /**
     * Get the report from matching terms.
     * 
     * @see org.slasoi.slamodel.sla.SLA
     * 
     @return MonitorabilityReport
     **/
    public final MonitorabilityReport getReporter() {
        return selreporter;
    }

    /**
     * Matches features with components for each part of an Agreement or Guarantee Term.
     * 
     * @return List<Selection> - a list of feature selections for configurations
     * 
     @see org.slasoi.gslam.monitoring.selection.Selection
     * 
     **/
    public final List<Selection> matchFeatureComponents() {
        monitorable = false;

        String agreementTermOp = core.$series;

        // HashMap<String,ComponentMonitoringFeatures> selected = new
        // HashMap<String,ComponentMonitoringFeatures>();
        selected.clear();

        // for each agreement term find a component that offers the expression
        // (i.e. has features for the term, cond and
        // value types)
        // String term = common.$base + "completion_time";

        Iterator<ID> ids = terms.keySet().iterator();
        while (ids.hasNext()) {
            monitorable = true;
            ID id = ids.next();
            logLine();
            getLogger().debug("Processing CMFs for AgreementTerm: " + id);
            logLine();

            getLogger().debug("Selecting CMFs for AgreementTerm ReasoningConfiguration.");
            String arrayOfBOOLEAN = "http://www.slaatsoi.org/types#array_of_BOOLEAN";
            String[] paramTypes = new String[1];
            paramTypes[0] = arrayOfBOOLEAN;

            // locate a suitable Reasoner component for the AgreementTerm
            ComponentMonitoringFeatures rcgcomp = selector.selectComponentReasoners(agreementTermOp, paramTypes);

            Selection aselection = new Selection();
            if (rcgcomp != null) {
                getLogger().debug("Added a Reasoner (" + rcgcomp.getUuid() + ") for op: " + agreementTermOp);

                // aselection.setSpecification(astutils.ASTexpressionToStr(gterm));
                aselection.setSpecification(currModel);
                aselection.setName(id.toString());
                aselection.setID(aselection.getName());
                aselection.addComponentMonitoringFeature(rcgcomp);
                aselection.setIsRootSelection(true);
                aselection.setComponentID(rcgcomp.getUuid());
                aselection.setDescription(rcgcomp.getType() + " for AgreementTerm ID: " + id);
                aselection.setType(rcgcomp.getType());
                selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(id.toString(), rcgcomp
                        .getUuid().toString(), ReportEntry.Type.SELECTION, ReportEntry.Step.SELECTED_REASONER,
                        "Reasoner for Agreement", id.toString(), Result.DEFAULT));

                // now process any preconditions for the Agreement Term
                if (terms.getATPrecondition(id) != null) {
                    Node condnode = terms.getATPrecondition(id);
                    // check it is an agreement
                    if (condnode.jjtGetChild(0) instanceof ASTAgreement) {
                        ASTAgreement agreement = (ASTAgreement) condnode.jjtGetChild(0);
                        if (agreement.jjtGetChild(0) instanceof ASTGuaranteeTerm) {
                            ASTGuaranteeTerm gterm = (ASTGuaranteeTerm) agreement.jjtGetChild(0);
                            processGTerm(gterm, aselection, id);
                        }
                    }
                }

                Iterator<Node> gterms = terms.get(id).iterator();
                while (gterms.hasNext() && monitorable) {
                    Node node = gterms.next();
                    if (node.jjtGetNumChildren() > 0) {
                        // check it is an agreement
                        if (node.jjtGetChild(0) instanceof ASTAgreement) {
                            ASTAgreement agreement = (ASTAgreement) node.jjtGetChild(0);
                            if (agreement.jjtGetChild(0) instanceof ASTGuaranteeTerm) {
                                ASTGuaranteeTerm gterm = (ASTGuaranteeTerm) agreement.jjtGetChild(0);
                                processGTerm(gterm, aselection, id);
                            }
                        }
                    }
                }

                // now filter the specification model for the whole agreement
                SLA newmodel = sLAutils.removeAllButAgreementTerm(currModel, id);
                aselection.setSpecification(newmodel);
            } else {
                monitorable = false;
                getLogger().warn("No CMF found for AgreementTerm reasoner.");
                selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(id.toString(), id
                        .toString(), ReportEntry.Type.SELECTION, ReportEntry.Step.SELECTED_REASONER,
                        "No CMF found for Agreement", id.toString(), Result.ERROR));
            }
            if (!monitorable) {
                getLogger().warn("The AgreementTerm (" + id + ") * IS NOT * monitorable.  Check warnings.");
                // remove any selections for this AgreementTerm
                removeAllRelatedSelections(aselection);
                selected.remove(aselection);
                selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(id.toString(), id
                        .toString(), ReportEntry.Type.AGREEMENT, ReportEntry.Step.MATCHING_TERMS,
                        "Agreement not monitorable.", id.toString(), Result.NOTMONITORABLE));
            } else {
                getLogger().debug("The AgreementTerm (" + id + ") IS monitorable and will be added to the MSC.");
                selected.add(aselection);
                selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(id.toString(), rcgcomp
                        .getUuid().toString(), ReportEntry.Type.AGREEMENT, ReportEntry.Step.MATCHING_TERMS,
                        "Agreement monitorable.", id.toString(), Result.MONITORABLE));
            }
        }
        return selected;
    }

    /**
     * Checks to see whether the reasoner can support a child selection from a parent selection.
     * 
     * @param parent
     *            the parent selection
     * @param child
     *            the child selection
     *            <p>
     *            Note: This is part of the YR2 constraints posed upon the MM.
     * @return boolean. true - is atomic, false - is not atomic
     * 
     @see org.slasoi.gslam.monitoring.selection.Selection
     * 
     **/
    private boolean checkReasonerAtomicElement(final Selection parent, final Selection child) {
        boolean result = false;
        if (!(parent.getIsRootSelection() && child.getComponentID().equals(child.getParent().getComponentID()))
                || child.getType().equals("SENSOR")) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    /**
     * Processes a Guarantee Term Node for given Agreement Term.
     * 
     * @param gterm
     *            a Guarantee Term AST Node
     * @param parent
     *            a parent selection
     * @param agreementID
     *            the Agreement Term ID
     * 
     @return boolean. true - is monitorable, false - is not monitorable
     * 
     @see org.slasoi.gslam.monitoring.selection.Selection
     * 
     **/
    public final boolean processGTerm(final ASTGuaranteeTerm gterm, final Selection parent, final ID agreementID) {
        boolean result = false;
        String gtermid = gterm.jjtGetFirstToken().toString();
        gtermid = gtermid.replace("\"", "");
        ASTTerm lhsTerm = null, rhsTerm = null;
        ASTComparator termComparator = null;
        // Process the children of the Expression: ASTTerm, ASTComparator, ASTTerm
        if (gterm.jjtGetNumChildren() > 0) {
            if (gterm.jjtGetChild(0) instanceof ASTTerm) {
                lhsTerm = (ASTTerm) gterm.jjtGetChild(0);
            }
            if (gterm.jjtGetChild(1) instanceof ASTComparator) {
                termComparator = (ASTComparator) gterm.jjtGetChild(1);
            }
            if (gterm.jjtGetChild(2) instanceof ASTTerm) {
                rhsTerm = (ASTTerm) gterm.jjtGetChild(2);
            }
            String termstr = common.$base + lhsTerm.jjtGetFirstToken().toString();
            String condstr = termComparator.jjtGetFirstToken().toString();
            String valuestr = rhsTerm.jjtGetLastToken().toString();
            if (condstr.length() > 0) {
                getLogger().debug("Processing AST node: " + gtermid);
                selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(agreementID + "/"
                        + gtermid, gtermid, ReportEntry.Type.MATCHING, ReportEntry.Step.MATCHING_TERMS,
                        "Processing Term", gtermid, Result.DEFAULT));
                getLogger().debug("Selecting CMFs for GTerm Op: " + condstr);
                // locate a CMF function feature for condition operator
                String slaop = opmap.getSLASOIFromCond(condstr);
                // get types for reasoner matching
                String lhstype = "";
                String rhstype = "";
                if (lhsTerm != null) {
                    lhstype = getResultDataType(lhsTerm);
                    getLogger().debug("Data type for LHS of expression: " + lhstype);
                }
                if (rhsTerm != null) {
                    rhstype = getResultDataType(rhsTerm);
                    getLogger().debug("Data type for RHS of expression: " + rhstype);
                }
                String[] paramTypes = new String[2];
                paramTypes[0] = lhstype;
                paramTypes[1] = rhstype;
                // locate a suitable Reasoner component for the Guarantee Term main op
                // and Parameter types
                ComponentMonitoringFeatures rcgcomp = selector.selectComponentReasoners(slaop, paramTypes);
                if (rcgcomp != null) {
                    // selected.put(slaop,rcgcomp);
                    Selection aselection = new Selection();

                    // remove all other Gterms except this one from model
                    SLA newmodel = sLAutils.removeAllButGuaranteedState(currModel, agreementID, new ID(gtermid));
                    aselection.setSpecification(newmodel);
                    aselection.setName(gtermid);
                    aselection.setID(gtermid);
                    aselection.addComponentMonitoringFeature(rcgcomp);
                    aselection.setType(rcgcomp.getType());
                    aselection.setComponentID(rcgcomp.getUuid());
                    if (parent != null) {
                        aselection.setParent(parent);
                    }
                    aselection.setDescription("Reasoner for Term: " + gtermid + ", Op: " + slaop);
                    getLogger().debug("Added a " + rcgcomp.getType() + " (" + rcgcomp.getUuid() + ") for op: " + slaop);
                    selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(agreementID + "/"
                            + gtermid, gtermid, ReportEntry.Type.SELECTION, ReportEntry.Step.SELECTED_REASONER,
                            "Appropriate Reasoner Selected", rcgcomp.getUuid(), Result.DEFAULT));
                    // now check if child terms can be monitored by same
                    // (parent) component
                    if (lhsTerm.jjtGetNumChildren() > 0 && lhsTerm.jjtGetChild(0) instanceof ASTTermFunction) {
                        ASTTermFunction func = (ASTTermFunction) lhsTerm.jjtGetChild(0);
                        Selection lhsselection = getFunctionSelection(func, aselection, newmodel, agreementID);
                        // check if component is same as component parent
                        if (lhsselection != null) {
                            if (checkReasonerAtomicElement(aselection, lhsselection)) {
                                aselection.setLHSSelection(lhsselection);
                                result = true;
                            } else {
                                getLogger().warn(
                                        "Could not add component (" + lhsselection.getComponentID()
                                                + ") as Parent ID (" + lhsselection.getParent().getComponentID()
                                                + ") does not match.");
                                result = false;
                            }
                        } else {
                            getLogger().warn("Could not locate component for: " + func.toString());
                            result = false;
                        }
                    } else if (lhsTerm.jjtGetFirstToken() instanceof Token) {
                        Selection lhsselection = getTokenSelection(termstr, aselection, newmodel, agreementID);
                        // check if component is same as component parent
                        if (lhsselection != null) {
                            if (checkReasonerAtomicElement(aselection, lhsselection)) {
                                aselection.setLHSSelection(lhsselection);
                                result = true;
                            } else {
                                getLogger().warn(
                                        "Could not add component (" + lhsselection.getComponentID()
                                                + ") as Parent ID (" + lhsselection.getParent().getComponentID()
                                                + ") does not match.");
                                result = false;
                            }
                        } else {
                            result = false;
                            getLogger().warn("Could not locate component for: " + termstr);
                        }
                        // aselection.setLHSSelection(lhsselection);
                    }
                    if (rhsTerm.jjtGetLastToken() instanceof Token) {
                        IsNumber testtoken = new IsNumber(valuestr);
                        if (!testtoken.getNumber()) {
                            Selection rhsselection = getTokenSelection(valuestr, aselection, newmodel, agreementID);
                            // check if component is same as component parent
                            if (rhsselection != null) {
                                if (checkReasonerAtomicElement(aselection, rhsselection)) {
                                    aselection.setRHSSelection(rhsselection);
                                    result = true;
                                } else {
                                    getLogger().warn(
                                            "Could not add component (" + rhsselection.getComponentID()
                                                    + ") as Parent ID (" + rhsselection.getParent().getComponentID()
                                                    + ") does not match.");
                                    result = false;
                                }
                            }
                        }
                    }

                    selected.add(aselection);
                } else {
                    String params = " params:";
                    for (String param : paramTypes) {
                        params = params + " " + param;
                    }
                    getLogger().warn("No RCG found for selection of op: " + slaop + params);
                    selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(agreementID + "/"
                            + gtermid, gtermid, ReportEntry.Type.MATCHING, ReportEntry.Step.MATCHING_TERMS,
                            "No reasoner matched.", gtermid, Result.WARN));
                    result = false;
                    monitorable = false;
                }
            }
        }
        return result;
    }

    /**
     * Selects an appropriate function feature for a given Term Function node.
     * 
     * @param func
     *            a TermFunction AST Node
     * @param parent
     *            a parent selection
     * @param termmodel
     *            SLA model for functional selection
     * @param agreementID
     *            The ID of the agreement
     * 
     @return Selection. A function feature selection
     * 
     @see org.slasoi.gslam.monitoring.selection.Selection
     * 
     **/
    private Selection getFunctionSelection(final ASTTermFunction func, final Selection parent, final SLA termmodel,
            final ID agreementID) {
        Selection aselection = null;
        String parentPath = buildParentPath(parent);
        String termstr = common.$base + func.jjtGetFirstToken().toString();
        String origterm = termstr.replace(common.$base, "");
        // check if operation mapping
        String opmapstr = opmap.getSLASOIFromCond(termstr);
        if (opmapstr.length() > 0) {
            getLogger().debug("The term: " + termstr + " was replaced with: " + opmapstr);
            termstr = opmapstr;
        }
        ComponentMonitoringFeatures termcomp = null;
        // function/parameter has a type defined
        if (func.jjtGetNumChildren() > 1 && func.jjtGetChild(1) instanceof ASTTermType) {
            if (func.jjtGetChild(1) instanceof ASTTermType) {
                ASTTermType atttype = (ASTTermType) func.jjtGetChild(1);
                String type = atttype.jjtGetFirstToken().toString();
                if (sLAutils.isCoreUnit(type)) {
                    type = units.$base + type;
                }
                String[] paramTypes = new String[1];
                paramTypes[0] = type;
                // locate a suitable Reasoner component for the AgreementTerm
                termcomp = selector.selectComponentReasoners(termstr, paramTypes);
                if (termcomp == null || termcomp.getMonitoringFeaturesLength() < 1) {
                    String tempterm = "";
                    tempterm = core.$base + func.jjtGetFirstToken().toString();
                    termcomp = selector.selectComponentReasoners(tempterm, paramTypes);
                    termstr = tempterm;
                }
            }
        } else {
            termcomp = selector.selectComponentTerm(termstr);
            if (termcomp == null || termcomp.getMonitoringFeaturesLength() < 1) {
                // try with commonterms base
                String tempterm = "";
                tempterm = core.$base + func.jjtGetFirstToken().toString();
                termcomp = selector.selectComponentTerm(tempterm);
                termstr = tempterm;
            }
        }
        getLogger().debug("Matched " + selector.getMatchedCount() + " CMFs for term (func): " + termstr);
        if (termcomp != null) {
            getLogger().debug("Matched a " + termcomp.getType() + " for term: " + termstr);
            selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(parentPath + "/" + origterm,
                    termstr, ReportEntry.Type.MATCHING, ReportEntry.Step.MATCHING_TERMS, "Matched", termcomp.getUuid(),
                    Result.DEFAULT));
            aselection = new Selection();
            aselection.setName(termstr);
            aselection.setID(origterm);
            aselection.setSpecification(termmodel);
            aselection.addComponentMonitoringFeature(termcomp);
            aselection.setDescription(termstr);
            aselection.setComponentID(termcomp.getUuid());
            aselection.setType(termcomp.getType());
            selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(parentPath + "/" + origterm,
                    termstr, ReportEntry.Type.SELECTION, ReportEntry.Step.SELECTED_REASONER, "Reasoner", termcomp
                            .getUuid(), Result.DEFAULT));
            if (parent != null) {
                aselection.setParent(parent);
            }
            // now process any function parameters
            if (func.jjtGetNumChildren() > 0) {
                if (func.jjtGetChild(0) instanceof ASTFunctionParameters) {
                    // loop through parameters
                    ASTFunctionParameters funcparams = (ASTFunctionParameters) func.jjtGetChild(0);
                    if (funcparams.jjtGetNumChildren() > 0) {
                        if (funcparams.jjtGetChild(0) instanceof ASTTermFunction) {
                            ASTTermFunction childfunc = (ASTTermFunction) funcparams.jjtGetChild(0);
                            aselection.setLHSSelection(getFunctionSelection(childfunc, aselection, termmodel,
                                    agreementID));
                        } else {
                            if (funcparams.jjtGetChild(0) instanceof ASTFunctionParameter) {
                                ASTFunctionParameter param1 = (ASTFunctionParameter) funcparams.jjtGetChild(0);
                                String param1termstr = param1.jjtGetFirstToken().toString();
                                if (!param1termstr.contains("/")) {
                                    param1termstr = common.$base + param1.jjtGetFirstToken().toString();
                                }
                                aselection.setLHSSelection(getTokenSelection(param1termstr, aselection, termmodel,
                                        agreementID));
                            }
                        }
                    }

                    if (func.jjtGetNumChildren() > 1) {
                        if (func.jjtGetChild(1) instanceof ASTFunctionParameters) {
                            // loop through parameters
                            funcparams = (ASTFunctionParameters) func.jjtGetChild(1);
                            if (funcparams.jjtGetNumChildren() > 0) {
                                if (funcparams.jjtGetChild(0) instanceof ASTTermFunction) {
                                    ASTTermFunction childfunc = (ASTTermFunction) funcparams.jjtGetChild(0);
                                    aselection.setRHSSelection(getFunctionSelection(childfunc, aselection, termmodel,
                                            agreementID));
                                } else {
                                    if (funcparams.jjtGetChild(0) instanceof ASTFunctionParameter) {
                                        ASTFunctionParameter param1 = (ASTFunctionParameter) funcparams.jjtGetChild(0);
                                        String param1termstr = param1.jjtGetFirstToken().toString();
                                        if (!param1termstr.contains("/")) {
                                            param1termstr = common.$base + param1.jjtGetFirstToken().toString();
                                        }
                                        aselection.setRHSSelection(getTokenSelection(param1termstr, aselection,
                                                termmodel, agreementID));
                                    }
                                }
                            }
                        }
                    }

                    if (func.jjtGetNumChildren() > 2) {
                        if (func.jjtGetChild(2) instanceof ASTFunctionParameters) {
                            // loop through parameters
                            funcparams = (ASTFunctionParameters) func.jjtGetChild(2);
                            if (funcparams.jjtGetNumChildren() > 0) {
                                if (funcparams.jjtGetChild(0) instanceof ASTTermFunction) {
                                    ASTTermFunction childfunc = (ASTTermFunction) funcparams.jjtGetChild(0);
                                    aselection.setRHSSelection(getFunctionSelection(childfunc, aselection, termmodel,
                                            agreementID));
                                } else {
                                    if (funcparams.jjtGetChild(0) instanceof ASTFunctionParameter) {
                                        ASTFunctionParameter param1 = (ASTFunctionParameter) funcparams.jjtGetChild(0);
                                        String param1termstr = param1.jjtGetFirstToken().toString();
                                        if (!param1termstr.contains("/")) {
                                            param1termstr = common.$base + param1.jjtGetFirstToken().toString();
                                        }
                                        aselection.setRHSSelection(getTokenSelection(param1termstr, aselection,
                                                termmodel, agreementID));
                                    }
                                }
                            }
                        }
                    }

                }
                /*
                 * if (func.jjtGetNumChildren() > 1) { if (func.jjtGetChild(1) instanceof ASTFunctionParameters) { //
                 * loop through parameters ASTFunctionParameters funcparams = (ASTFunctionParameters) func
                 * .jjtGetChild(1); processFunctionParameters(funcparams,aselection); } }
                 */
            }
        } else {
            getLogger().error("No CMF found for selection of term: " + termstr);
            monitorable = false;
            selreporter.addReport(termmodel.getUuid().toString(), selreporter.buildEntry(parentPath + "/"
                    + origterm, termstr, ReportEntry.Type.MATCHING,
                    ReportEntry.Step.MATCHING_TERMS, "No CMF matched for term.", termstr,
                    Result.ERROR));
        }
        return aselection;
    }

    /**
     * Builds the tree based upon some Reasoner Function Parameters.
     * 
     * @param funcparams
     *            ASTFunctionParameters
     * @param aselection
     *            Selection
     * @param termmodel
     *            SLA
     * @param agreementID
     *            the agreement id
     * 
     @see org.slasoi.gslam.monitoring.selection.Selection
     * 
     **/
    @SuppressWarnings("unused")
    private void processFunctionParameters(final ASTFunctionParameters funcparams, final Selection aselection,
            final SLA termmodel, final ID agreementID) {
        // just handle 1 or 2 parameters for now
        if (funcparams.jjtGetNumChildren() > 0) {
            // check if it is another function
            if (funcparams.jjtGetChild(0) instanceof ASTTermFunction) {
                ASTTermFunction childfunc = (ASTTermFunction) funcparams.jjtGetChild(0);
                aselection.setLHSSelection(getFunctionSelection(childfunc, aselection, termmodel, agreementID));
                if (funcparams.jjtGetNumChildren() > 1 && funcparams.jjtGetChild(1) instanceof ASTTermFunction) {
                    childfunc = (ASTTermFunction) funcparams.jjtGetChild(1);
                    aselection.setRHSSelection(getFunctionSelection(childfunc, aselection, termmodel, agreementID));
                } else {
                    String test = "";
                }
            } else {
                if (funcparams.jjtGetChild(0) instanceof ASTFunctionParameter) {
                    ASTFunctionParameter param1 = (ASTFunctionParameter) funcparams.jjtGetChild(0);
                    String param1termstr = param1.jjtGetFirstToken().toString();
                    if (!param1termstr.contains("/")) {
                        param1termstr = common.$base + param1.jjtGetFirstToken().toString();
                    }
                    aselection.setLHSSelection(getTokenSelection(param1termstr, aselection, termmodel, agreementID));
                }
            }

            /*
             * if (funcparams.jjtGetChild(0) instanceof ASTTermFunction) { ASTTermFunction childfunc = (ASTTermFunction)
             * funcparams.jjtGetChild(0); aselection.setLHSSelection(getFunctionSelection( childfunc, aselection)); if
             * (childfunc.jjtGetNumChildren()>1 && childfunc.jjtGetChild(1) instanceof ASTFunctionParameters) {
             * ASTFunctionParameters funcparams2 = (ASTFunctionParameters) childfunc.jjtGetChild(1); if
             * (funcparams2.jjtGetChild(0) instanceof ASTTermFunction) { ASTTermFunction childrfunction =
             * (ASTTermFunction)funcparams2.jjtGetChild(0);
             * aselection.setRHSSelection(getFunctionSelection(childrfunction, aselection)); } } } else if
             * (funcparams.jjtGetChild(0) instanceof ASTFunctionParameter) { ASTFunctionParameter param1 =
             * (ASTFunctionParameter) funcparams .jjtGetChild(0); String param1termstr = common.$base +
             * param1.jjtGetFirstToken().toString(); aselection.setLHSSelection(getTokenSelection( param1termstr,
             * aselection)); }
             */

            /*
             * if (funcparams.jjtGetNumChildren() > 1) { if (funcparams.jjtGetChild(1) instanceof ASTTermFunction) {
             * ASTTermFunction childfunc = (ASTTermFunction) funcparams .jjtGetChild(1);
             * aselection.setRHSSelection(getFunctionSelection( childfunc, aselection)); } else if
             * (funcparams.jjtGetChild(1) instanceof ASTFunctionParameter) { ASTFunctionParameter param2 =
             * (ASTFunctionParameter) funcparams .jjtGetChild(1); String param2termstr = common.$base +
             * param2.jjtGetFirstToken().toString(); aselection.setRHSSelection(getTokenSelection( param2termstr,
             * aselection)); } } else if (func.jjtGetNumChildren()>1) {
             * 
             * }
             */
            /*
             * else if (funcparams.jjtGetFirstToken().next!=null) { Token firsttoken =
             * funcparams.jjtGetFirstToken().next; if (firsttoken.next!=null) { Token token = firsttoken.next; String
             * param2termstr = common.$base + token.toString(); aselection.setRHSSelection(getTokenSelection(
             * param2termstr, aselection)); } }
             */
        }
    }

    /**
     * Selects an appropriate feature for a Token node.
     * 
     * @param otermstr
     *            a token (term) string
     * @param parent
     *            a parent selection
     * @param termmodel
     *            SLA model for TokenSelection
     * @param agreementID
     *            the agreement id
     * 
     @return Selection. A feature selection
     * 
     @see org.slasoi.gslam.monitoring.selection.Selection
     * 
     **/
    private Selection getTokenSelection(final String otermstr, final Selection parent, final SLA termmodel,
            final ID agreementID) {
        Selection aselection = null;

        String parentPath = buildParentPath(parent);

        String origterm = otermstr;
        String termstr = otermstr;

        // check if it is a string (literal), if so just return no selection
        if (termstr.startsWith("\"")) {
            return null;
        }

        String opmapstr = opmap.getSLASOIFromCond(termstr);
        if (opmapstr.length() > 0) {
            getLogger().debug("The term: " + termstr + " was replaced with: " + opmapstr);
            termstr = opmapstr;
        }

        ComponentMonitoringFeatures termcomp = null;

        getLogger().debug("Assessing any CMFs for term (token): " + termstr);
        // check if it is a literal value
        String termlit = termstr.replace(common.$base, "");
        termlit = termlit.replace(core.$base, "");
        if ((termlit.startsWith("\"") && termlit.endsWith("\"")) || termlit.endsWith("text")
                || termlit.indexOf(":") == 2) {
            getLogger().debug("The term (token): " + termlit + " is a literal and will be ignored.");
        } else {
            termcomp = selector.selectComponentTerm(termstr);
            if (termcomp == null) {
                // try again with core model reference
                termstr = termstr.replace("http://www.slaatsoi.org/commonTerms#", "http://www.slaatsoi.org/coremodel#");
                termcomp = selector.selectComponentTerm(termstr);
            }

            if (termcomp == null) {
                // try without term base
                termcomp = selector.selectComponentTerm(origterm.replace(common.$base, ""));
            }

            if (termcomp != null) {
                String type = termcomp.getType();
                getLogger().debug("Matched a " + type + " (" + termcomp.getUuid() + " for: " + termstr);
                selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(
                        parentPath + "/" + termstr, termstr, ReportEntry.Type.MATCHING,
                        ReportEntry.Step.MATCHING_TERMS, type.toUpperCase() + " Added", termstr, Result.DEFAULT));
                aselection = new Selection();
                aselection.setName(termstr);
                aselection.setID(termstr);
                if (type.equals("RCG") || type.equals("REASONER")) {
                    aselection.setSpecification(termmodel);
                }
                aselection.addComponentMonitoringFeature(termcomp);
                aselection.setDescription(termcomp.getType() + " for op: " + termstr);
                aselection.setComponentID(termcomp.getUuid());
                aselection.setType(type);
                if (parent != null) {
                    aselection.setParent(parent);
                }

                // add Sensors for events for this term - this should change in YR3
                // to look up events in SLA model core
                TermEvents termevents = eventMap.getEventsForTerm(termstr);
                if (termevents != null && termevents.getEvents().size() > 0) {
                    getLogger().debug("Adding " + termevents.getEvents().size() + " event types for term: " + termstr);
                    if (termevents.getEvents().size() > 0) {
                        LinkedList<Selection> eventsselectionlist = new LinkedList<Selection>();
                        int termeventcount = termevents.getEvents().size();
                        int selectedevents = 0;
                        // select events
                        for (Event event : termevents.getEvents()) {
                            getLogger().debug("Term: " + termstr + " requires Event: " + event.getType());
                            selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(parentPath
                                    + "/" + termstr, termstr, ReportEntry.Type.MATCHING,
                                    ReportEntry.Step.MATCHING_EVENTS, "Matching events.", termstr, Result.DEFAULT));
                            ComponentMonitoringFeatures eventcomp = selector.selectEventComponents(event.getType());
                            if (eventcomp != null) {
                                Selection eventselection = new Selection();
                                eventselection.setName(event.getType());
                                eventselection.addComponentMonitoringFeature(eventcomp);
                                eventselection.setParent(aselection);
                                eventselection.setDescription(eventcomp.getType() + " for event: " + event.getType());
                                eventsselectionlist.add(eventselection);
                                eventselection.setType("SENSOR");
                                // aselection.addComponentMonitoringFeature(eventcomp);
                                getLogger().debug(
                                        "Selected SENSOR (" + eventcomp.getUuid() + ") for event: " + event.getType());
                                selectedevents++;
                                selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(parentPath
                                        + "/" + termstr, termstr, ReportEntry.Type.SELECTION,
                                        ReportEntry.Step.SELECTED_SENSOR, "Sensor selected.", termstr, Result.DEFAULT));
                            }
                        }
                        aselection.setEvents(eventsselectionlist);

                        if (!(termeventcount == selectedevents)) {
                            getLogger().warn("Not all required events were selected for term: " + termstr);
                        }
                    }
                } else {
                    getLogger().warn("No Sensor event types found for term: " + termstr);
                    selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(parentPath + "/"
                            + termstr, termstr, ReportEntry.Type.MATCHING, ReportEntry.Step.MATCHING_EVENTS,
                            "No Events matched.", termstr, Result.WARN));
                }
            } else {
                // no term value so assume a literal number or string
                // check if it is a number
                termstr = termstr.replace("http://www.slaatsoi.org/commonTerms#", "http://www.slaatsoi.org/coremodel#");
                termstr = termstr.replace("http://www.slaatsoi.org/coremodel#", "");

                IsNumber testtoken = new IsNumber(termstr);
                if (!testtoken.getNumber()) {
                    if (isBoolean(termstr)) {
                        getLogger().debug("Term: " + termstr + " is a boolean value and will be ignored.");
                    } else {
                        if (isPath(termstr)) {
                            // if it was a variable report appropriately
                            getLogger().error("No CMF found for path: " + termstr);
                            selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(parentPath
                                    + "/" + termstr, termstr, ReportEntry.Type.MATCHING,
                                    ReportEntry.Step.MATCHING_TERMS, "No CMF matched.", termstr, Result.ERROR));
                            monitorable = false;
                        } else {
                            getLogger().error("No CMF found for selection of term: " + termstr);
                            selreporter.addReport(currModel.getUuid().toString(), selreporter.buildEntry(parentPath
                                    + "/" + termstr, termstr, ReportEntry.Type.MATCHING,
                                    ReportEntry.Step.MATCHING_TERMS, "No CMF matched.", termstr, Result.ERROR));
                            monitorable = false;
                        }
                    }
                } else {
                    getLogger().debug("Term: " + termstr + " is a number value and will be ignored.");
                }
            }
        }

        return aselection;
    }

    /**
     * Get the result data type for a term.
     * 
     * @param term
     *            a token (term) node
     * 
     @return String. If empty string, no data type found.
     * 
     @see org.slasoi.gslam.monitoring.selection.Selection
     * 
     **/
    public final String getResultDataType(final ASTTerm term) {
        ResultTypeMapping map = new ResultTypeMapping();
        String result = "unknown";

        if (term.jjtGetNumChildren() > 0 && term.jjtGetChild(0) instanceof ASTTermFunction) {
            ASTTermFunction func = (ASTTermFunction) term.jjtGetChild(0);
            String termstr = common.$base + func.jjtGetFirstToken().toString();
            result = map.getResultTypeFromTerm(termstr);

            // Selection lhsselection = getFunctionSelection(func,aselection);
            // aselection.setLHSSelection(lhsselection);
        } else if (term.jjtGetFirstToken() instanceof Token) {
            String termstr = common.$base + term.jjtGetFirstToken().toString();
            result = map.getResultTypeFromTerm(termstr);
            // Selection lhsselection = getTokenSelection(termstr,aselection);
            // aselection.setLHSSelection(lhsselection);
        }
        return (result);
    }

    /**
     * Removes all selections associated (child) with a parent selection.
     * 
     * @param aselection
     *            the parent selection
     * 
     @see Selection
     **/
    private void removeAllRelatedSelections(final Selection aselection) {
        if (aselection != null) {
            List<Selection> candidates = new LinkedList<Selection>();
            // search for all selections with parent of this selection
            Iterator<Selection> selectit = selected.iterator();
            while (selectit.hasNext()) {
                Selection nextselection = selectit.next();
                if (nextselection.getParent() == aselection) {
                    candidates.add(nextselection);
                    removeAllRelatedSelections(nextselection);
                }
            }
            // now remove all the candidates from the selected list
            selected.removeAll(candidates);
        }
    }

    /**
     * Check if the string is a boolean.
     * 
     * @param boolstr
     *            the string to boolean check
     * 
     * @return boolean
     * 
     **/
    private boolean isBoolean(final String boolstr) {
        boolean result = false;
        if (boolstr.toLowerCase().equals("false") || boolstr.toLowerCase().equals("true")) {
            result = true;
        }
        return result;
    }

    /**
     * Check if the string is a path.
     * 
     * @param str
     *            the string to path check
     * 
     * @return boolean
     * 
     **/
    private boolean isPath(final String str) {
        boolean result = false;
        if (str.contains("/") && !str.contains("#")) {
            result = true;
        }
        return result;
    }

    /**
     * Build a path including the parent path.
     * 
     * @param parent
     *            the parent selection
     * 
     * @return String
     * 
     @see Selection
     **/
    private String buildParentPath(final Selection parent) {
        String path = "";
        if (parent != null) {
            if (parent.getParent() != null) {
                path = path + buildParentPath(parent.getParent());
                path = path + "/" + parent.getID();
            } else {
                path = path + parent.getID();
            }
        }
        return path;
    }
}
