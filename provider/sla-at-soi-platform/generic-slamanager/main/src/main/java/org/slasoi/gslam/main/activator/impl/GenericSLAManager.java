/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 1559 $
 * @lastrevision   $Date: 2011-05-03 17:16:57 +0200 (tor, 03 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/main/src/main/java/org/slasoi/gslam/main/activator/impl/GenericSLAManager.java $
 */

package org.slasoi.gslam.main.activator.impl;

import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slasoi.gslam.core.authorization.BasicAuthorization;
import org.slasoi.gslam.core.builder.IMonitoringManagerBuilder;
import org.slasoi.gslam.core.builder.ISyntaxConverterBuilder;
import org.slasoi.gslam.core.builder.ProtocolEngineBuilder;
import org.slasoi.gslam.core.builder.SLARegistryBuilder;
import org.slasoi.gslam.core.builder.SLATemplateRegistryBuilder;
import org.slasoi.gslam.core.context.GenericSLAManagerServices;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils;
import org.slasoi.gslam.core.context.SLAMContextAware;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.monitoring.IMonitoringManager;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.core.negotiation.ProtocolEngine;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.sa.pss4slam.ServiceAdvertisementBroker;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.springframework.osgi.context.BundleContextAware;

/**
 * Generic SLA-Manager. This bundle is responsible for the initialization and integration of generic components into a
 * new SLAManagerContext. This SLAManagerContext will be handled by the domain-specific SLA-manager.<br>
 * <br>
 * 
 * The generic components are created from their Builders that have been exposed as OSGi services by themselves. The
 * required builders are:<br>
 * <br>
 * <p>
 * <blockquote>
 * 
 * <pre>
 *      *  SLARegistryBuilder
 *      *  SLATemplateRegistryBuilder
 *      *  ISyntaxConverterBuilder
 *      *  ProtocolEngineBuilder
 *      *  IMonitoringManagerBuilder
 * </pre>
 * 
 * </blockquote>
 * <p>
 * 
 * This bundle exposes an OSGi-service via the method
 * {@link GenericSLAManager#createContext(BundleContext, String, String, String, String)} and shall be invoked by any
 * SLAManager for initialization of its SLAManagerContext. Once the generic components have been initialized, they will
 * have a reference to the owner SLAManagerContext. This reference will allow the access in two ways: SLAManagerContext
 * --> Generic-Components and viceversa<br>
 * <br>
 * 
 * Note that each SLAManager instance will have its own generic-components and they do not have any relationship with
 * other generic-component instances.
 * 
 * @see org.slasoi.gslam.core.context.SLAManagerContext
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class GenericSLAManager implements GenericSLAManagerServices, BundleContextAware {
    /**
     * Default constructor
     */
    public GenericSLAManager() {
    }

    /**
     * Creates a new SLAManagerContext object. This method requires following bundles:<br>
     * <br>
     * <p>
     * <blockquote>
     * 
     * <pre>
     *      SLARegistry          :  gslam-slaregistry         >>  SLARegistryBuilder 
     *      SLATemplateRegistry  :  gslam-slatemplateregistry >>  SLATemplateRegistryBuilder 
     *      ISyntaxConverter     :  gslam-syntaxconverter     >>  ISyntaxConverterBuilder 
     *      ProtocolEngine       :  gslam-protocol-engine     >>  ProtocolEngineBuilder
     *      MonitorEngine        :  gslam-monitor-engine      >>  IMonitoringManagerBuilder
     *      ServiceAdvertisement :  sa-pss4slam               >>  ServiceAdvertisement (by lookup)
     * </pre>
     * 
     * </blockquote>
     * <p>
     * 
     * All Required-Bundles will be resolved in runtime and will be used for creating of generic-components and
     * collected into the just created SLAManagerContext.
     * <p>
     * <p>
     * 
     * As special case, the SyntaxConverter requires the creation of WebServices as follows: SLARegistry,
     * SLATemplateRegistry, INegotiation and IControl.<br>
     * <br>
     * 
     * Additionally, this method will load sla-templates defined under $SLASOI_HOME/generic-slam/templates.conf.
     * 
     * @see org.slasoi.gslam.core.context.SLAManagerContext
     * 
     */
    public SLAManagerContext createContext(BundleContext osgiContextOwner, String SLAManagerID, String EPR,
            String groupID, String wsID) throws CreateContextGenericSLAManagerException {
        if (SLAManagerID == null || SLAManagerID.equals(""))
            throw new CreateContextGenericSLAManagerException("The SLAManagerID cannot be empty.");
        if (osgiContext == null)
            throw new CreateContextGenericSLAManagerException(
                    "OSGi Context not initialized.  Check the gslam-main [bundle] configuration.");

        GenericSLAManagerContextImpl result = new GenericSLAManagerContextImpl(SLAManagerID, EPR, wsID);
        result.setGroupID(groupID);
        result.setSlamUtils(slaManagerUtils);

        try {
            slaManagerUtils.register(result);
            LOGGER.info("###--- SLAManagerID '" + SLAManagerID + "' attached to group '" + groupID + "'");
        }
        catch (Exception e) {
            throw new CreateContextGenericSLAManagerException("SLAManagerContext could not be attached to any group");
        }

        /*
         * - looking for every builder: . SLARegistry . SLATemplateRegistry . ProtocolEngine . ISyntaxConverter .
         * MonitoringManager
         * 
         * - initialize new context object
         */

        ServiceReference service = null;

        // SLATemplateRegistry Builder
        service = getReference(SLATemplateRegistryBuilder.class.getName());
        if (service == null)
            throw new CreateContextGenericSLAManagerException("no SLATemplateRegistryBuilder found.");
        SLATemplateRegistryBuilder slatRegistryBuilder = (SLATemplateRegistryBuilder) osgiContext.getService(service);

        LOGGER.info("###--------------------------------------------------------" + slatRegistryBuilder);

        SLATemplateRegistry slatRegistry = slatRegistryBuilder.create();
        injectIntoContext(slatRegistry, result);
        result.setSLATemplateRegistry(slatRegistry);

        // SLARegistry Builder
        service = getReference(SLARegistryBuilder.class.getName());
        if (service == null)
            throw new CreateContextGenericSLAManagerException("no SLARegistryBuilder found.");
        SLARegistryBuilder slaRegistryBuilder = (SLARegistryBuilder) osgiContext.getService(service);

        LOGGER.info("###--------------------------------------------------------" + slaRegistryBuilder);

        SLARegistry slaRegistry = slaRegistryBuilder.create();
        injectIntoContext(slaRegistry, result);
        result.setSLARegistry(slaRegistry);

        // Protocol Builder
        service = getReference(ProtocolEngineBuilder.class.getName());
        if (service == null)
            throw new CreateContextGenericSLAManagerException("no ProtocolEngineBuilder found.");
        ProtocolEngineBuilder protocolEngineBuilder = (ProtocolEngineBuilder) osgiContext.getService(service);
        ;

        LOGGER.info("###--------------------------------------------------------" + protocolEngineBuilder);

        ProtocolEngine protocolEngine = protocolEngineBuilder.create();
        injectIntoContext(protocolEngine, result);
        result.setProtocolEngine(protocolEngine);

        // SyntaxConverter
        service = getReference(ISyntaxConverterBuilder.class.getName());
        if (service == null)
            throw new CreateContextGenericSLAManagerException("no ISyntaxConverterBuilder found.");
        ISyntaxConverterBuilder syntaxConverterBuilder = (ISyntaxConverterBuilder) osgiContext.getService(service);
        ;

        LOGGER.info("###--------------------------------------------------------" + syntaxConverterBuilder);

        Hashtable<ISyntaxConverter.SyntaxConverterType, ISyntaxConverter> syntaxconverters =
                syntaxConverterBuilder.create();

        Enumeration<SyntaxConverterType> keys = syntaxconverters.keys();

        while (keys.hasMoreElements()) {
            SyntaxConverterType type = keys.nextElement();
            ISyntaxConverter converter = syntaxconverters.get(type);

            injectIntoContext(converter, result);
            linkSyntaxConverterWSToContext(converter, osgiContextOwner);
            createWS(converter);
        }
        result.setSyntaxConverters(syntaxconverters);

        // ServiceAdvertisement
        /*
        ServiceAdvertisementBroker saBroker = new ServiceAdvertisementBroker();
        injectIntoContext(saBroker, result);
        result.setServiceAdvertisement(saBroker);
        LOGGER.info("###--------------------------------------------------------" + saBroker);
        */

        // Authorization
        BasicAuthorization auth = new BasicAuthorization();
        result.setAuthorization(auth);

        // Monitoring
        service = getReference(IMonitoringManagerBuilder.class.getName());
        if (service == null)
            throw new CreateContextGenericSLAManagerException("no IMonitoringManagerBuilder found.");
        IMonitoringManagerBuilder monitoringBuilder = (IMonitoringManagerBuilder) osgiContext.getService(service);
        ;

        LOGGER.info("###--------------------------------------------------------" + monitoringBuilder);

        IMonitoringManager monitoringManager = monitoringBuilder.create();
        injectIntoContext(monitoringManager, result);
        result.setMonitorManager(monitoringManager);

        // Loading of Templates
        String templatesFN = "templates.conf";
        GenericTemplatesLoader templatesLoader = new GenericTemplatesLoader(result);
        SLATemplate[] slaTemplates = templatesLoader.getSLATemplates(templatesFN, SLAManagerID);
        if (slaTemplates != null && slaTemplates.length != 0) {
            try {
                // TODO: load templates into the DB and publish across P/S System

                SLATemplateRegistry tmplRegistry = result.getSLATemplateRegistry();

                for (SLATemplate slaT : slaTemplates) {
                    Metadata slaTMD = new Metadata();

                    slaTMD.setPropertyValue(Metadata.registrar_id, "md:auto_registered_from_service_ads");
                    slaTMD.setPropertyValue(Metadata.provider_uuid, SLAManagerID + getProviderUUID(slaT));
                    slaTMD.setPropertyValue(Metadata.template_uuid, slaT.getUuid().getValue());

                    LOGGER.info("###------------ " + SLAManagerID + "-TemplateRegistry:: template "
                            + slaT.getUuid().getValue() + " will be inserted...");

                    tmplRegistry.addSLATemplate(slaT, slaTMD);
                    LOGGER.info("###------------ done");
                }
            }
            catch (Exception e) {
                LOGGER.debug(e);
            }

            templatesLoader.reset(templatesFN, SLAManagerID);
        }

        DefaultGslamSubscriber pss4slamSubscriber = new DefaultGslamSubscriber(result);
        pss4slamSubscriber.start();

        return result;
    }

    /**
     * Helper method
     * 
     * @param tmpl
     *            template source for extracting the provider id
     */
    protected String getProviderUUID(SLATemplate tmpl) {
        String result = "";
        Party[] parties = tmpl.getParties();
        for (Party p : parties) {
            if (p.getAgreementRole().getValue().equalsIgnoreCase("provider")) {
                result = p.getId().getValue();
                break;
            }
        }
        return result;
    }

    /**
     * Helper method
     */
    protected ServiceReference getReference(String classname) throws CreateContextGenericSLAManagerException {
        try {
            ServiceReference[] refs = osgiContext.getServiceReferences(classname, null);
            if (refs != null && refs.length != 0) {
                return refs[0];
            }
        }
        catch (Exception e) {
            throw new CreateContextGenericSLAManagerException(e.toString());
        }

        return null;
    }

    /**
     * Helper method
     */
    protected void injectIntoContext(Object obj, SLAManagerContext context) {
        if (obj instanceof SLAMContextAware) {
            ((SLAMContextAware) obj).setSLAManagerContext(context);
        }
    }

    /**
     * Helper method
     */
    protected void linkSyntaxConverterWSToContext(Object obj, BundleContext osgiContextOwner) {
        if (obj instanceof BundleContextAware) {
            ((BundleContextAware) obj).setBundleContext(osgiContextOwner);
        }
    }

    /**
     * Helper method
     */
    protected void createWS(Object obj) {
        if (obj instanceof ISyntaxConverterBuilder) {
            ((ISyntaxConverterBuilder) obj).createWS();
        }
    }

    /**
     * Helper method
     */
    public void setBundleContext(BundleContext osgiContext) {
        this.osgiContext = osgiContext;
    }

    /**
     * Helper method which return the available GenericSLAManagerUtils OSGi-service
     */
    public GenericSLAManagerUtils getSlaManagerUtils() {
        return slaManagerUtils;
    }

    /**
     * Injection method for GenericSLAManagerUtils
     */
    public void setSlaManagerUtils(GenericSLAManagerUtils slaManagerUtils) {
        this.slaManagerUtils = slaManagerUtils;
    }

    /**
     * Helper method for loading of SLAM-configuration
     */
    public SLAMConfiguration loadConfigurationFrom(String config) throws CreateContextGenericSLAManagerException {
        try {
            String path = System.getenv("SLASOI_HOME");
            String resource = path + System.getProperty("file.separator") + "slams/" + config;
            LOGGER.info("GenericSLAManager :: loadConfigurationFrom :: '" + resource + "'...");

            Properties p = new Properties();
            p.load(new FileInputStream(resource));

            SLAMConfiguration result = new SLAMConfiguration();

            result.name     = p.getProperty( "slam.name"            );
            result.epr      = p.getProperty( "slam.epr.negotiation" );
            result.wsPrefix = p.getProperty( "slam.ws.prefix"       );
            result.group    = p.getProperty( "slam.group"           );
            
            result.properties = new Hashtable<String, String>();
            Set<Object> keySet = p.keySet();
            for ( Object key : keySet )
            {
                String k = (String)key;
                result.properties.put( k, p.getProperty( k ) );
            }

            return result;

        }
        catch (Exception e) {
            LOGGER.error(e);
        }

        return null;
    }

    protected GenericSLAManagerUtils slaManagerUtils;
    protected BundleContext osgiContext;

    private static final Logger LOGGER = Logger.getLogger(GenericSLAManager.class);
}
