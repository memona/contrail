package org.slasoi.gslam.protocolengine.persistence.profiles;

public enum SocioEconomicLevel {
    
    LOW_INCOME,
    MEDIUM_INCOME,
    HIGH_INCOME

}
