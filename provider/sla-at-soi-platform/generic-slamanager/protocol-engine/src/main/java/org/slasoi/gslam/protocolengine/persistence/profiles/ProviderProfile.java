package org.slasoi.gslam.protocolengine.persistence.profiles;
import javax.persistence.Entity;

@Entity
public class ProviderProfile extends Profile{
    
    private static final long serialVersionUID = 6557720317925192837L;
    private int salesObjective;
    private int maxPenaltyCosts;
    private int maxViolationsCount;
    
    public ProviderProfile(){
        
    }

    public int getSalesObjective() {
        return salesObjective;
    }

    public void setSalesObjective(int salesObjective) {
        this.salesObjective = salesObjective;
    }

    public int getMaxPenaltyCosts() {
        return maxPenaltyCosts;
    }

    public void setMaxPenaltyCosts(int maxPenaltyCosts) {
        this.maxPenaltyCosts = maxPenaltyCosts;
    }

    public int getMaxViolationsCount() {
        return maxViolationsCount;
    }

    public void setMaxViolationsCount(int maxViolationsCount) {
        this.maxViolationsCount = maxViolationsCount;
    }

}
