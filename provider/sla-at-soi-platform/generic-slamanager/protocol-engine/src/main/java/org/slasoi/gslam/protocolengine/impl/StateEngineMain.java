/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Edwin Yaqub - edwin.yaqub@tu-dortmund.de
 * @version        $Rev: 943 $
 * @lastrevision   $Date: 2011-03-14 17:52:08 +0100 (Mon, 14 Mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/protocol-engine/src/main/java/org/slasoi/gslam/protocolengine/impl/StateEngine.java $
 */

package org.slasoi.gslam.protocolengine.impl;

import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.protocolengine.IStateEngine;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseConfiguration;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.command.Command;
import org.drools.command.CommandFactory;
import org.drools.conf.MaintainTMSOption;
import org.drools.definition.KnowledgePackage;
import org.drools.definition.rule.Rule;
import org.drools.io.ResourceFactory;
import org.drools.runtime.ExecutionResults;
import org.drools.runtime.StatefulKnowledgeSession;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.core.*;

/**
 * The <code>StateEngine</code> class is a thin wrapper over a rule engine. It is meant to provide a layer of
 * independence over a rule engine of choice (currently Drools). It converts Events from ProtocolEngine into Drools
 * commands.
 * 
 * @author Edwin
 * @version 1.0-SNAPSHOT
 */
public class StateEngineMain implements IStateEngine {

    private KnowledgeBuilder knowledgeBuilder;
    private KnowledgeBase knowledgeBase;
    private StatefulKnowledgeSession session;
    private SLAManagerContext context;
    private static final Logger LOGGER = Logger.getLogger(StateEngine.class);
    private String pathToDRL;
    private ClassLoader classLoader;

    public StateEngineMain() {
        LOGGER.info("*** INFO *** StateEngine - constructor ***");
        initializeLogger();
    }


    private void initializeLogger()
    {
      Properties logProperties = new Properties();
      try
      {
        logProperties.load(new FileReader(System.getenv("SLASOI_HOME") + System.getProperty("file.separator")
                + "generic-slamanager" + System.getProperty("file.separator") + "protocol-engine"
                + System.getProperty("file.separator") + "protocol-engine-config" + System.getProperty("file.separator") + "log4j.properties") );
        PropertyConfigurator.configure(logProperties);
        LOGGER.info("Logging initialized.");
      }
      catch(IOException e)
      {
        throw new RuntimeException("Unable to load logging property ");
      }
    }
    public void setContext(SLAManagerContext context) {
        this.context = context;
        LOGGER.info("*** INFO *** StateEngine - CONTEXT HAS BEEN SET ***");
        init();
    }

    public void init() {
        LOGGER.info("inside init");
        knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        String pathToDRL = "";
        FileInputStream fstream = null;
        // 2- File not found at SLA-specific location, that means we are using the one at default location.
        try {
            pathToDRL =
                    System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "generic-slamanager"
                            + System.getProperty("file.separator") + "protocol-engine"
                            + System.getProperty("file.separator") + "protocol-engine-config"
                            + System.getProperty("file.separator") + "protocol"
                            + System.getProperty("file.separator") + "BilateralNegotiationProtocol.drl";
            LOGGER.info("3- Now trying to open this path = \n" + pathToDRL);
            fstream = new FileInputStream(pathToDRL);
        }
        catch (FileNotFoundException fnfe2) {
            LOGGER.info("4- Could not find file at above path as well.");
            // fnfe2.printStackTrace();
        }
        try {
            if (fstream != null) {
                fstream.close();
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }

        assert (knowledgeBuilder != null) : "KnowledgeBuilder is null";
        // Reader source = new InputStreamReader( DroolsTest.class.getResourceAsStream("/Sample.drl"));
        knowledgeBuilder.add(ResourceFactory.newFileResource(pathToDRL), ResourceType.DRL);

        assert (!knowledgeBuilder.hasErrors()) : "KnowledgeBuilder has errors. Check syntax of Protocol file!";
        if (knowledgeBuilder.hasErrors()) {
            LOGGER.info("KnowledgeBuilder has errors: " + knowledgeBuilder.getErrors());
            return;
        }

        knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
        knowledgeBase.addKnowledgePackages(knowledgeBuilder.getKnowledgePackages());
        session = knowledgeBase.newStatefulKnowledgeSession();

        KnowledgePackage kp = knowledgeBase.getKnowledgePackage("protocol");
        assert (kp != null) : "KnowledgePackage not found under name protocol.";
        Collection<Rule> rules = kp.getRules();
        for (Rule rule : rules) {
            LOGGER.info("*** INFO *** StateEngine Rule -> " + rule.getName());
        }
        

        // session.fireAllRules();//Just a test instruction.
        
        List<Event> listOfEvents = new ArrayList<Event>(); 
          Event startNegEvent = new Event(EventName.StartNegotiationEvent); 
          //Step #1 : Start Negotiation 
          listOfEvents.add(startNegEvent); 
          listOfEvents = (List<Event>) process(listOfEvents);
          Event returnedStartNegEvent = listOfEvents.get(0); 
          LOGGER.info("**** Event: " +
                  returnedStartNegEvent.getEventName()+ "*** "+returnedStartNegEvent.isProcessedSuccessfully());

//          //Step #2: Customization (Customer side , first call)
//          Event sendCustomizationEvent = new Event(EventName.SendCustomizationEvent); 
//          listOfEvents.removeAll(listOfEvents);
//          listOfEvents.add(sendCustomizationEvent); 
//          listOfEvents = (List<Event>) process(listOfEvents); 
//          Event returnedSendCustomizationEvent = listOfEvents.get(0); 
//          LOGGER.info("**** Event: "+ returnedSendCustomizationEvent.getEventName()+
//                  "*** "+returnedSendCustomizationEvent.isProcessedSuccessfully());
//          if(returnedSendCustomizationEvent != null){
//              LOGGER.info("*** C1 Protocol returned Customization values as: "+
//                      returnedSendCustomizationEvent.getSimplifiedNegotiableParameters().toString());
//          }
//          
//          //Step #2: Customization (Customer side , second call)
//          sendCustomizationEvent = new Event(EventName.SendCustomizationEvent); 
//          listOfEvents.removeAll(listOfEvents);
//          listOfEvents.add(sendCustomizationEvent); 
//          listOfEvents = (List<Event>) process(listOfEvents); 
//          returnedSendCustomizationEvent = listOfEvents.get(0); 
//          LOGGER.info("**** Event: "+ returnedSendCustomizationEvent.getEventName()+
//                  "*** "+returnedSendCustomizationEvent.isProcessedSuccessfully());
//          if(returnedSendCustomizationEvent != null && returnedSendCustomizationEvent.getSimplifiedNegotiableParameters() != null){
//              LOGGER.info("*** C2 Protocol returned Customization values as: "+
//                      returnedSendCustomizationEvent.getSimplifiedNegotiableParameters().toString());
//          }
//          
//          //Step #2: Customization (Customer side , third call)
//          sendCustomizationEvent = new Event(EventName.SendCustomizationEvent); 
//          listOfEvents.removeAll(listOfEvents);
//          listOfEvents.add(sendCustomizationEvent); 
//          listOfEvents = (List<Event>) process(listOfEvents); 
//          returnedSendCustomizationEvent = listOfEvents.get(0); 
//          LOGGER.info("**** Event: "+ returnedSendCustomizationEvent.getEventName()+
//                  "*** "+returnedSendCustomizationEvent.isProcessedSuccessfully());
//          if(returnedSendCustomizationEvent != null && returnedSendCustomizationEvent.getSimplifiedNegotiableParameters() != null){
//              LOGGER.info("*** C3 Protocol returned Customization values as: "+
//                      returnedSendCustomizationEvent.getSimplifiedNegotiableParameters().toString());
//          }

          
          //Step #2: Customization (Provider side , first call)
          Event customizationArrivedEvent = new Event(EventName.CustomizationArrivedEvent);
          
          SimplifiedNegotiableParameters simplifiedNegotiableParameters = new SimplifiedNegotiableParameters();
          simplifiedNegotiableParameters.setNumberOfCustomizationRounds(2);
          simplifiedNegotiableParameters.setNumberOfNegotiationRounds(5);
          simplifiedNegotiableParameters.setOptionalCritiqueOnQoS(true);
          simplifiedNegotiableParameters.setMaxCounterOffersAllowed(5);
          simplifiedNegotiableParameters.setProcessTimeout(590000);
          
          LOGGER.info("*** Providing Partner Params = "+simplifiedNegotiableParameters.toString());
          customizationArrivedEvent.setSimplifiedNegotiableParameters(simplifiedNegotiableParameters);
          LOGGER.info("*** Providing Above Params to Provider");
          //simplifiedNegotiableParameters.setCurrentCustomizationRound(6);
          //customizationArrivedEvent.setSimplifiedNegotiableParameters(simplifiedNegotiableParameters);
          
          listOfEvents.removeAll(listOfEvents);
          listOfEvents.add(customizationArrivedEvent); 
          listOfEvents = (List<Event>) process(listOfEvents); 
          Event returnedCustomizationArrivedEvent = listOfEvents.get(0); 
          LOGGER.info("**** Event: "+ returnedCustomizationArrivedEvent.getEventName()+
                  "*** "+returnedCustomizationArrivedEvent.isProcessedSuccessfully());
          if(returnedCustomizationArrivedEvent != null && returnedCustomizationArrivedEvent.getSimplifiedNegotiableParameters() != null){
              LOGGER.info("*** C1 Protocol returned Customization values as: "+
                      returnedCustomizationArrivedEvent.getSimplifiedNegotiableParameters().toString());
          }
          
          //Step #2: Customization (Provider side, second call)
          simplifiedNegotiableParameters = returnedCustomizationArrivedEvent.getSimplifiedNegotiableParameters();
          customizationArrivedEvent = new Event(EventName.CustomizationArrivedEvent);
          //following line is a must to be set by PE
          customizationArrivedEvent.setSimplifiedNegotiableParameters(simplifiedNegotiableParameters);
          
          listOfEvents.removeAll(listOfEvents);
          listOfEvents.add(customizationArrivedEvent); 
          listOfEvents = (List<Event>) process(listOfEvents); 
          returnedCustomizationArrivedEvent = listOfEvents.get(0); 
          LOGGER.info("**** Event: "+ returnedCustomizationArrivedEvent.getEventName()+
                  "*** "+returnedCustomizationArrivedEvent.isProcessedSuccessfully());
          if(returnedCustomizationArrivedEvent != null && returnedCustomizationArrivedEvent.getSimplifiedNegotiableParameters() != null){
              LOGGER.info("*** C2 Protocol returned Customization values as: "+
                      returnedCustomizationArrivedEvent.getSimplifiedNegotiableParameters().toString());
          }
          
          //Step #2: Customization (Provider side, third call)
          simplifiedNegotiableParameters = returnedCustomizationArrivedEvent.getSimplifiedNegotiableParameters();
          customizationArrivedEvent = new Event(EventName.CustomizationArrivedEvent);
          //following line is a must to be set by PE
          customizationArrivedEvent.setSimplifiedNegotiableParameters(simplifiedNegotiableParameters);
          
          listOfEvents.removeAll(listOfEvents);
          listOfEvents.add(customizationArrivedEvent); 
          listOfEvents = (List<Event>) process(listOfEvents); 
          returnedCustomizationArrivedEvent = listOfEvents.get(0); 
          LOGGER.info("**** Event: "+ returnedCustomizationArrivedEvent.getEventName()+
                  "*** "+returnedCustomizationArrivedEvent.isProcessedSuccessfully());
          if(returnedCustomizationArrivedEvent != null && returnedCustomizationArrivedEvent.getSimplifiedNegotiableParameters() != null){
              LOGGER.info("*** C3 Protocol returned Customization values as: "+
                      returnedCustomizationArrivedEvent.getSimplifiedNegotiableParameters().toString());
          }

          try {
              Thread.currentThread().sleep(1000*60);
          }
          catch (InterruptedException e) {
                  e.printStackTrace();
          }
          
          //Step #3: Negotiation 1 (call 1)
          Event sendProposalEvent = new Event(EventName.SendProposalEvent); 
          listOfEvents.removeAll(listOfEvents);
          listOfEvents.add(sendProposalEvent); 
          listOfEvents = (List<Event>) process(listOfEvents); 
          Event returnedSendProposalEvent = listOfEvents.get(0); 
          LOGGER.info("**** N1 Event: "+ returnedSendProposalEvent.getEventName()+
                  "*** "+returnedSendProposalEvent.isProcessedSuccessfully());
          if(returnedSendProposalEvent != null && returnedSendProposalEvent.getSimplifiedNegotiableParameters() != null){
            LOGGER.info("*** N1 Protocol returned Default Parameter values as: "+
                    returnedSendProposalEvent.getSimplifiedNegotiableParameters().toString());
          }
          
          //Step #3: Negotiation 1 (call 2)
          sendProposalEvent = new Event(EventName.SendProposalEvent); 
          listOfEvents.removeAll(listOfEvents);
          listOfEvents.add(sendProposalEvent); 
          listOfEvents = (List<Event>) process(listOfEvents); 
          returnedSendProposalEvent = listOfEvents.get(0); 
          LOGGER.info("**** N2 Event: "+ returnedSendProposalEvent.getEventName()+
                  "*** "+returnedSendProposalEvent.isProcessedSuccessfully());
          if(returnedSendProposalEvent != null && returnedSendProposalEvent.getSimplifiedNegotiableParameters() != null){
            LOGGER.info("*** N2 Protocol returned Default Parameter values as: "+
                    returnedSendProposalEvent.getSimplifiedNegotiableParameters().toString());
          }
          
          //Step #3: Negotiation 1 (call 3)
          sendProposalEvent = new Event(EventName.SendProposalEvent); 
          listOfEvents.removeAll(listOfEvents);
          listOfEvents.add(sendProposalEvent); 
          listOfEvents = (List<Event>) process(listOfEvents); 
          returnedSendProposalEvent = listOfEvents.get(0); 
          LOGGER.info("**** N3 Event: "+ returnedSendProposalEvent.getEventName()+
                  "*** "+returnedSendProposalEvent.isProcessedSuccessfully());
          if(returnedSendProposalEvent != null ){
            LOGGER.info("*** N3 Protocol returned : "+
                    returnedSendProposalEvent.getProcessingAfterMath());
          }
          

          
          //Step #4: Create Agreement 1 (call 1)
          Event requestAgreementEvent = new Event(EventName.RequestAgreementEvent); 
          listOfEvents.removeAll(listOfEvents);
          listOfEvents.add(requestAgreementEvent); 
          listOfEvents = (List<Event>) process(listOfEvents); 
          Event returnedRequestAgreementEvent = listOfEvents.get(0); 
          LOGGER.info("**** CA1 Event: "+ returnedRequestAgreementEvent.getEventName()+
                  "*** "+returnedRequestAgreementEvent.isProcessedSuccessfully());
          if(returnedRequestAgreementEvent != null && returnedRequestAgreementEvent.getProcessingAfterMath() != null){
            LOGGER.info("*** CA1 Protocol returned : "+
                    returnedRequestAgreementEvent.getProcessingAfterMath());
          }
          
          //Step #4: Create Agreement 1 (call 2)
          requestAgreementEvent = new Event(EventName.SendProposalEvent); 
          listOfEvents.removeAll(listOfEvents);
          listOfEvents.add(requestAgreementEvent); 
          listOfEvents = (List<Event>) process(listOfEvents); 
          returnedRequestAgreementEvent = listOfEvents.get(0); 
          LOGGER.info("**** CA2 Event: "+ returnedRequestAgreementEvent.getEventName()+
                  "*** "+returnedRequestAgreementEvent.isProcessedSuccessfully());
          if(returnedRequestAgreementEvent != null && returnedRequestAgreementEvent.getProcessingAfterMath() != null){
            LOGGER.info("*** CA2 Protocol returned : "+
                    returnedRequestAgreementEvent.getProcessingAfterMath());
          }
          
    }

    public Object process(Object listOfEvents) {
        List<Event> eventsList = (List<Event>) listOfEvents;
        // Convert the listOfEvents received into Drools Commands and batch execute as follows:
        List<Command> commands = convertEventsToCommands(eventsList);
        ExecutionResults results = session.execute(CommandFactory.newBatchExecution(commands));
        int i = session.fireAllRules();// StatelessSession does this implicitly, use will override implicit call.
        // Now, convert all the received result objects back to Events, put them in a List and return.
        eventsList = convertResultsToEvents(results, eventsList.size());
        // The rules should remove them from the session as well so they dont go on firing once their purpose is done!
        return eventsList;
    }

    public List<Command> convertEventsToCommands(List<Event> listOfEvents) {
        List<Command> commands = new ArrayList<Command>();
        if (listOfEvents != null && listOfEvents.size() > 0) {
            int counter = 0;
            for (Event e : listOfEvents) {
                // cmds.add( CommandFactory.newInsert( new Person("edwin", "yaqub","father", 29, false), "p1" ) );
                commands.add(CommandFactory.newInsert(e, counter + ""));
                counter++;
            }
        }
        return commands;
    }

    public List<Event> convertResultsToEvents(ExecutionResults results, int length) {
        List<Event> listOfEvents = new ArrayList<Event>();
        for (int i = 0; i < length; i++) {
            listOfEvents.add((Event) results.getValue(i + ""));
        }
        return listOfEvents;
    }

    // This method to be called at the end of createAgreement() and cancelNegotiation().
    public void disposeWorkingMemory() {
        // Call dispose on session
        session.dispose();
    }

    public static void main(String a[]){
        StateEngineMain stateEngineMain = new StateEngineMain(); 
        stateEngineMain.init();
        //Simple Timeout:
//        long allowedTime = System.currentTimeMillis();
//        System.out.println("Start Time   = "+ System.currentTimeMillis());
//        allowedTime += (1000*25);
//        System.out.println("Allowed Time = "+ allowedTime);
//        try {
//            Thread.currentThread().sleep(1000*30);
//        }
//        catch (InterruptedException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        long currentTime = System.currentTimeMillis();
//        if(currentTime >= allowedTime) {
//            System.out.println("Process Timeout");
//            System.out.println("Current Time = "+ currentTime);
//        }        
    }
    
}