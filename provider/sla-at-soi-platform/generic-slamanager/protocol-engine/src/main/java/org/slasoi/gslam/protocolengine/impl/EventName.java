/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Edwin Yaqub - edwin.yaqub@tu-dortmund.de
 * @version        $Rev: 923 $
 * @lastrevision   $Date: 2011-03-11 12:20:17 +0100 (pet, 11 mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/protocol-engine/src/main/java/org/slasoi/gslam/protocolengine/impl/EventName.java $
 */

package org.slasoi.gslam.protocolengine.impl;

/**
 * The <code>EventName</code> class maintains an enum of possible names for Events. 
 * 
 * @author Edwin Yaqub
 * @version 1.0-SNAPSHOT
 */
public enum EventName {
    StartNegotiationEvent,
    UpdateNegotiationEvent,
    
    SendCustomizationEvent,
    CustomizationArrivedEvent,

    SendProposalEvent,
    ProposalArrivedEvent,

    RequestAgreementEvent,
    AgreementRequestedEvent,

    StartRenegotiationEvent,
    UpdateRenegotiationEvent,

    CancelNegotiationEvent,

    TerminateAgreementEvent
}
