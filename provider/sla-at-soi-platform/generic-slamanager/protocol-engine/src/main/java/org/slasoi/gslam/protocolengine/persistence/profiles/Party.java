package org.slasoi.gslam.protocolengine.persistence.profiles;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

@Entity
public class Party implements java.io.Serializable{
    
    private static final long serialVersionUID = 6782599923315112854L;
    private Integer id;
    private String partyId;
    private Location location;
    private MarketSegment marketSegment;
    
    public Party(){
        
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    @MapsId
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "location_id")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public MarketSegment getMarketSegment() {
        return marketSegment;
    }

    public void setMarketSegment(MarketSegment marketSegment) {
        this.marketSegment = marketSegment;
    }
    
}
