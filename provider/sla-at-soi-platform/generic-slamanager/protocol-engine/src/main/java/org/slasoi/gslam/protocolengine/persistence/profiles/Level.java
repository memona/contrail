package org.slasoi.gslam.protocolengine.persistence.profiles;


public enum Level {
    
    HIGH,
    MEDIUM,
    LOW

}
