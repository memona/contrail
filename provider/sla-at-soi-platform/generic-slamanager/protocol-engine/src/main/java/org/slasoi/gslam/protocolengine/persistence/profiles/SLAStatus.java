package org.slasoi.gslam.protocolengine.persistence.profiles;

public enum SLAStatus {
    
    NOT_EFFECTIVE_YET,
    EFFECTIVE,
    EXPIRED

}
