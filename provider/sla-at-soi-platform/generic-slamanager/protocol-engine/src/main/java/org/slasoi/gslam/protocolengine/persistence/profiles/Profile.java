package org.slasoi.gslam.protocolengine.persistence.profiles;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Profile implements java.io.Serializable{
    
    private static final long serialVersionUID = 7146365363785069028L;
    private Integer id;
    private String profileId;
    private ProfileType profileType;
    private Set<NegotiationHistory> negotiationHistory;
    private Set<BusinessHistory> businessHistory;
    private Timestamp creationTime;
    private Timestamp lastModified;
    private Status status;
    private HashMap<Object, Object> annotations;
    
    public Profile(){
        
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getProfileId() {
        return profileId;
    }
    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }
    public ProfileType getProfileType() {
        return profileType;
    }
    public void setProfileType(ProfileType profileType) {
        this.profileType = profileType;
    }
    
    @OneToMany(cascade=CascadeType.ALL)
    @JoinTable(
            name="ProfileNegHistory",
            joinColumns = @JoinColumn( name="profile_id"),
            inverseJoinColumns = @JoinColumn( name="neg_hist_id")
    )
    public Set<NegotiationHistory> getNegotiationHistory() {
        return negotiationHistory;
    }
    public void setNegotiationHistory(Set<NegotiationHistory> negotiationHistory) {
        this.negotiationHistory = negotiationHistory;
    }
    
    @OneToMany(cascade=CascadeType.ALL)
    @JoinTable(
            name="ProfileBizHistory",
            joinColumns = @JoinColumn( name="profile_id"),
            inverseJoinColumns = @JoinColumn( name="biz_hist_id")
    )    
    public Set<BusinessHistory> getBusinessHistory() {
        return businessHistory;
    }
    public void setBusinessHistory(Set<BusinessHistory> businessHistory) {
        this.businessHistory = businessHistory;
    }
    
    public Timestamp getCreationTime() {
        return creationTime;
    }
    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }
    public Timestamp getLastModified() {
        return lastModified;
    }
    public void setLastModified(Timestamp lastModified) {
        this.lastModified = lastModified;
    }
    
    @MapsId
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "status_id")
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
    public HashMap<Object, Object> getAnnotations() {
        return annotations;
    }
    public void setAnnotations(HashMap<Object, Object> annotations) {
        this.annotations = annotations;
    }
    
}
