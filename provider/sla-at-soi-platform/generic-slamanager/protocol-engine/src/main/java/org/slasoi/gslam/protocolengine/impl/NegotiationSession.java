/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Edwin Yaqub - edwin.yaqub@tu-dortmund.de
 * @version        $Rev: 2078 $
 * @lastrevision   $Date: 2011-06-07 17:02:41 +0200 (tor, 07 jun 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/protocol-engine/src/main/java/org/slasoi/gslam/protocolengine/impl/NegotiationSession.java $
 */

package org.slasoi.gslam.protocolengine.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slasoi.gslam.core.negotiation.INegotiation.CancellationReason;
import org.slasoi.gslam.core.negotiation.INegotiation.Customization;
import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * The <code>NegotiationSession</code> class is used by <code>NegotiationManager</code> to store all information
 * exchanged between negotiators as a single session per negotiation.
 * 
 * @author Edwin
 * @version 1.0-SNAPSHOT
 */
public class NegotiationSession {

    private String uuid;// This is the NegotiationSessionID
    private SessionStatus status;
    private Timestamp creationTime; //Time when initiateNegotiation is invoked. 
    private Long preNegotiationOrNegotiationStartTime = null;//Starting time of negotiation process. Set when customize or negotiate is invoked for the first time 
    private List<SLATemplate> receivedProposals;
    private List<SLATemplate> sentProposals;
    private List<Party> involvedParties;
    private List<CancellationReason> cancellationReasons;
    private List<TerminationReason> terminationReasons;
    private SLA agreedSLA;
    private List<String> listOfTemplateUUIDsForSession;
    private boolean customizationUsed = false;
    private boolean consensusReachedOnCustomization = false;
    private Customization customization;

    public NegotiationSession() {
        creationTime = new Timestamp(System.currentTimeMillis());
        receivedProposals = new ArrayList<SLATemplate>();
        sentProposals = new ArrayList<SLATemplate>();
        involvedParties = new ArrayList<Party>();
        cancellationReasons = new ArrayList<CancellationReason>();
        terminationReasons = new ArrayList<TerminationReason>();
        status = SessionStatus.AVAILABLE;
        listOfTemplateUUIDsForSession = new ArrayList<String>();
    }

    public SLA getAgreedSLA() {
        return agreedSLA;
    }

    public void setAgreedSLA(SLA agreedSLA) {
        this.agreedSLA = agreedSLA;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void generateAndSetUuid() {
        this.uuid = java.util.UUID.randomUUID().toString();
    }

    public void addToReceivedProposals(SLATemplate[] slaTemplates) {
        for (SLATemplate slaTemplate : slaTemplates) {
            receivedProposals.add(slaTemplate);
        }
    }

    public void addToSentProposals(SLATemplate[] slaTemplates) {
        for (SLATemplate slaTemplate : slaTemplates) {
            sentProposals.add(slaTemplate);
        }
    }

    public SessionStatus getStatus() {
        return status;
    }

    public void setStatus(SessionStatus status) {
        this.status = status;
    }

    public List<Party> getInvolvedParties() {
        return involvedParties;
    }

    public void setInvolvedParties(List<Party> involvedParties) {
        this.involvedParties = involvedParties;
    }

    public void addToInvolvedParties(SLATemplate slaTemplate) {
        for (Party party : slaTemplate.getParties()) {
            this.involvedParties.add(party);
        }
        System.out.println("Inside NegotiationSession.addToInvolvedParties - Count = " + this.involvedParties.size());
    }

    public List<CancellationReason> getCancellationReasons() {
        return cancellationReasons;
    }

    public void setCancellationReasons(List<CancellationReason> cancellationReasons) {
        this.cancellationReasons = cancellationReasons;
    }

    public List<TerminationReason> getTerminationReasons() {
        return terminationReasons;
    }

    public void setTerminationReasons(List<TerminationReason> terminationReasons) {
        this.terminationReasons = terminationReasons;
    }

    protected void addTemplateUUIDInSession(String templateUUID) {
        this.listOfTemplateUUIDsForSession.add(templateUUID);
    }

    protected boolean isTemplateUUIDInSession(String templateUUID) {
        return this.listOfTemplateUUIDsForSession.contains(templateUUID);
    }

    public boolean isCustomizationUsed() {
        return customizationUsed;
    }

    public void setCustomizationUsed(boolean customizationUsed) {
        this.customizationUsed = customizationUsed;
    }
    
    public boolean isConsensusReachedOnCustomization() {
        return consensusReachedOnCustomization;
    }

    public void setConsensusReachedOnCustomization(boolean consensusReachedOnCustomization) {
        this.consensusReachedOnCustomization = consensusReachedOnCustomization;
    }

    public Customization getCustomization() {
        return customization;
    }

    public void setCustomization(Customization customization) {
        this.customization = customization;
    }

    public Long getPreNegotiationOrNegotiationStartTime() {
        return preNegotiationOrNegotiationStartTime;
    }

    public void setPreNegotiationOrNegotiationStartTime(Long preNegotiationOrNegotiationStartTime) {
        this.preNegotiationOrNegotiationStartTime = preNegotiationOrNegotiationStartTime;
    }

}
