package org.slasoi.gslam.protocolengine.persistence.profiles;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

@Entity
public class NegotiationHistory implements java.io.Serializable{

    private static final long serialVersionUID = 4984261748081976739L;
    private Integer id;
    private String negotiationId;
    private Outcome outcome;
    private int negotiationRounds;
    private Timestamp startTime;
    private Timestamp endTime;
    private String slaId;
    private String slaTemplateId;
    private boolean renegotiation;
    private StrategyRanks rankings;
    
    public NegotiationHistory(){
        
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNegotiationId() {
        return negotiationId;
    }

    public void setNegotiationId(String negotiationId) {
        this.negotiationId = negotiationId;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }

    @Column(nullable=true)
    public int getNegotiationRounds() {
        return negotiationRounds;
    }

    public void setNegotiationRounds(int negotiationRounds) {
        this.negotiationRounds = negotiationRounds;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public String getSlaTemplateId() {
        return slaTemplateId;
    }

    public void setSlaTemplateId(String slaTemplateId) {
        this.slaTemplateId = slaTemplateId;
    }

    @Column(nullable=true)
    public boolean isRenegotiation() {
        return renegotiation;
    }

    public void setRenegotiation(boolean renegotiation) {
        this.renegotiation = renegotiation;
    }

    @MapsId
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "st_ranks_id")
    public StrategyRanks getRankings() {
        return rankings;
    }

    public void setRankings(StrategyRanks rankings) {
        this.rankings = rankings;
    }

}
