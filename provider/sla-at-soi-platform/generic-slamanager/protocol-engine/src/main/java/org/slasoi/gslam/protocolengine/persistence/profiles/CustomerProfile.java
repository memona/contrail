package org.slasoi.gslam.protocolengine.persistence.profiles;

import java.util.Set;

import javax.persistence.Entity;

import org.hibernate.annotations.CollectionOfElements;

@Entity
public class CustomerProfile extends Profile{

    private static final long serialVersionUID = -979539757651379007L;
    private MarketSegment customerSegment;
    private Evolution customerEvolution;
    private int estimatedAge;
    private Education education;
    private SocioEconomicLevel socioEconomicLevel;
    private Set<Hobbies> hobbies;
    private boolean mobility;
    private Location location;
    
    public CustomerProfile(){
        
    }

    public MarketSegment getCustomerSegment() {
        return customerSegment;
    }

    public void setCustomerSegment(MarketSegment customerSegment) {
        this.customerSegment = customerSegment;
    }

    public Evolution getCustomerEvolution() {
        return customerEvolution;
    }

    public void setCustomerEvolution(Evolution customerEvolution) {
        this.customerEvolution = customerEvolution;
    }

    public int getEstimatedAge() {
        return estimatedAge;
    }

    public void setEstimatedAge(int estimatedAge) {
        this.estimatedAge = estimatedAge;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public SocioEconomicLevel getSocioEconomicLevel() {
        return socioEconomicLevel;
    }

    public void setSocioEconomicLevel(SocioEconomicLevel socioEconomicLevel) {
        this.socioEconomicLevel = socioEconomicLevel;
    }

    @CollectionOfElements
    public Set<Hobbies> getHobbies() {
        return hobbies;
    }

    public void setHobbies(Set<Hobbies> hobbies) {
        this.hobbies = hobbies;
    }

    public boolean isMobility() {
        return mobility;
    }

    public void setMobility(boolean mobility) {
        this.mobility = mobility;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
