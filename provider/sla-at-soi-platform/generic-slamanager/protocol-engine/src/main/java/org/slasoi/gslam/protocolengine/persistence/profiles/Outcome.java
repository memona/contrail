package org.slasoi.gslam.protocolengine.persistence.profiles;

public enum Outcome {
    
    NEG_SUCCEEDED,
    NEG_FAILED,
    NEG_ABORTED,
    SLA_TERMINATED
//    RENEG_FAILED,
//    RENEG_SUCCEEDED,
//    RENEG_ABORTED

}
