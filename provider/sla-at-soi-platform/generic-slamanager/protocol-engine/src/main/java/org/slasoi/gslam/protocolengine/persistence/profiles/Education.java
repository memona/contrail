package org.slasoi.gslam.protocolengine.persistence.profiles;


public enum Education {
    
    NA,
    ELEMENTAL,
    SECONDARY,
    VOCATIONAL,
    SUPERIOR

}
