/* 
SVN FILE: $Id: _m_wrapper.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/_m_wrapper.java $

*/

package org.slasoi.gslam.templateregistry.plog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class _m_wrapper{ 
	
	private Engine eng = null;
	Module module = null;
	public _m_parser parser = null;
	//private List<Clause> init_clauses = new ArrayList<Clause>();
	private List<String> imported = Collections.synchronizedList(new ArrayList<String>());
	
	_m_wrapper(Engine eng, Module m) throws Error{
		this.eng = eng;
		this.module = m;
		parser = new _m_parser(this);
		//____INIT_FLAGS();
	}
	
	public List<String> imported(){
		List<String> list = new ArrayList<String>();
		list.addAll(imported);
		return list;
	}
	
	public Clause.Iterator controlledCandidates(final PI pi, boolean includeImportedModules){
		final List<Clause.Iterator> its = new ArrayList<Clause.Iterator>();
		if (!module.name().equals(Engine.SYSTEM_MODULE_NAME) && pi.equals(PI.current_op_3)){
			Clause.Iterator x = eng.system.module.candidates(pi);
			if (x != null) its.add(x);
		}
		Clause.Iterator x = this.module.candidates(pi);
		if (x != null) its.add(x);
		if (includeImportedModules){
			for (String $m : imported) try{
				_m_wrapper im = eng.__GET_MODULE($m);
				x = im.controlledCandidates(pi, true);
				if (x != null) its.add(x);
			}catch(Exception e){
				// TODO should this throw a prolog error ??
			}
		}
		if (!its.isEmpty()) return new Clause.Iterator(){
			Clause.Iterator it = null;
			public Clause next() throws Error{
				if (it == null && !its.isEmpty()) it = its.remove(0);
				if (it != null){
					Clause x = it.next();
					if (x != null) return x;
					it = null;
					return next();
				}
				return null;
			}
		};
		return null;
	}
	
	/* -------------------------------------------------------------
	 * CLAUSE LISTING
	 * ------------------------------------------------------------- */
	
	Clause.Iterator listClauses() throws Error{
		final PI.Iterator pis = module.predicates();
		return new Clause.Iterator(){
			Clause.Iterator cit = null;
			public Clause next() throws Error {
				if (cit == null){
					PI pi = pis.next();
					if (pi != null) cit = module.candidates(pi);
					else return null;
				}
				Clause c = cit.next();
				if (c != null) return c;
				else{
					cit = null;
					return next();
				}
			}
			
		};
	}
	
//	void exportToFile(String filename) throws Error{
//		try{
//			List<Clause> clauses = eng.mfac.programs().get(filename);
//			File f = new File(filename);
//			FileOutputStream fos = new FileOutputStream(f);
//			for (Clause c : clauses){
//				String $c = c.toString() + ".\n";
//				fos.write($c.getBytes());
//			}
//			fos.close();
//		}catch(Exception e){
//			throw Error.system(e);
//		}
//	}
	
	/* -------------------------------------------------------------
	 * PARSER
	 * ------------------------------------------------------------- */
	
	Clause parse(String s) throws Error{
		return parser.parse(s);
	}
	
	String write(Term t, boolean quoted, boolean numbervars) throws Error{
		return parser.write(t, quoted, numbervars);
	}
		
	/* -------------------------------------------------------------
	 * STATIC CONSULT
	 * ------------------------------------------------------------- */
	/*
	static List<Clause> consultFile(Engine eng, Clause filename) throws Error{
		List<_m_wrapper> modules = new ArrayList<_m_wrapper>();
		List<Clause> clauses = ____CONSULT_FILE(eng, filename, null, modules);
		for (_m_wrapper mw : modules){
			while (!mw.init_clauses.isEmpty()){
				Clause x = mw.init_clauses.remove(0);
				mw.execute(x);
			}
		}
		Clause c = new Clause(PI.consult_1);
		c.add(filename);
		eng.__NOTIFY(c);
		return clauses;
	}*/
	/*
	private static List<Clause> ____CONSULT_FILE(Engine eng, Clause filename, _m_wrapper m, List<_m_wrapper> modules) throws Error{
		List<Term> terms = Collections.synchronizedList(new ArrayList<Term>());
		boolean saved = false;
		try{
			// HAS THIS FILE ALREADY BEEN CONSULTED ???
			try{
				if (eng.session(
					new Clause(
						PI.colon_2,
						Engine.PROGRAMS_MODULE_NAME,
						new Clause(filename.quoted(), new Var("_"))
					)
				).solveNext()){
					unconsultFile(eng, filename);
				}
			}catch(Exception e){}
			
			if (m == null) m = eng.user;
			if (filename != null && !filename.isType(PI.atom_0)){
				throw Error.type(PI.atom_0,filename);
			}
			
			// OPEN THE STREAM ..
			Clause mode = new Clause(PI.read_0);
			List<Term> args = new ArrayList<Term>();
				args.add(filename);
			List<Term> options = new ArrayList<Term>();
			_io_stream stream = eng.streams.open_new_stream("java.io.File", args, mode, options);
			
			// READ & CONSULT ..
			List<Clause> clauses = Collections.synchronizedList(new ArrayList<Clause>());
			List<String> predicates = stream.read_all_terms();
			for (String s : predicates){
				Clause c = m.parser.parse(s);
				m = consultClause(eng, m, c, modules);
				terms.add(c);
				clauses.add(c);
				if (!modules.contains(m)) modules.add(m);
			}
			
			// CLOSE THE STREAM ..
			Clause force_close = new Clause(PI.force_1);
			force_close.add(new Clause(PI.true_0));
				options.add(force_close);
			stream.close(options);
			
			// FINALLY SAVE THE PROGRAM
			Clause clist = Term.toPrologList(terms);
			eng.consult(
				Engine.PROGRAMS_MODULE_NAME, 
				new Clause(filename.quoted(), clist)
			);
			saved = true;
			return clauses;
		}catch(Error error){
			if (!terms.isEmpty()){
				if (saved) unconsultFile(eng, filename); 
				else __UNCONSULT_TERM_LIST(eng, terms);
			}
			throw error;
		}catch(Exception e){
			if (!terms.isEmpty()){
				if (saved) unconsultFile(eng, filename); 
				else __UNCONSULT_TERM_LIST(eng, terms);
			}
			throw Error.system(e);
		}
	}
	*/
	
	static _m_wrapper consultClause(Engine eng, _m_wrapper m, Clause c, List<_m_wrapper> modules) throws Error{
		//if (c.is(PI.directive_1)){
		//	m = ____PROCESS_DIRECTIVE(eng, m, c, modules);
		//}else{
			m.add(c, false, true);
			//}
		return m;
	}
		
	/* -------------------------------------------------------------
	 * STATIC UNCONSULT
	 * ------------------------------------------------------------- */
	/*
	static void unconsultFile(Engine eng, Clause filename) throws Error{
		//is this wrong ?? ===> assumes program will not change (i.e. no assertions etc)
		if (filename != null && !filename.isType(PI.atom_0)){
			throw Error.type(PI.atom_0,filename);
		}
		// RETIREVE THE STORED PROGRAM CLAUSES
		Clause program = new Clause(
			PI.colon_2,
			Engine.PROGRAMS_MODULE_NAME,
			new Clause(filename.quoted(), new Var("CLIST"))
		);  
		Session ss = eng.session(program);
		List<Term> terms = null;
		if (ss.solveNext()){
			Clause clist = ss.var("CLIST").asClause();
			terms = Term.fromPrologList(clist);
		}else{
			throw Error.resource(filename);
		}
		__UNCONSULT_TERM_LIST(eng, terms);
		eng.programs.module.abolish(new PI(filename.quoted(),1));
		eng.unconsult(Engine.PROGRAMS_MODULE_NAME, program);
		Clause c = new Clause(PI.unconsult_1);
		c.add(filename);
		eng.__NOTIFY(c);
	}*/
	/*
	private static void __UNCONSULT_TERM_LIST(Engine eng, List<Term> terms) throws Error{
		List<_m_wrapper> dead = new ArrayList<_m_wrapper>();
		_m_wrapper m = eng.user;
		for (Term t : terms){
			m = unconsultClause(eng, m, (Clause)t);
			if (m != eng.user) dead.add(m);
		}
		for (_m_wrapper mx : dead){
			eng.__KILL_MODULE(mx);
		}
	}*/
	
	static _m_wrapper unconsultClause(Engine eng, _m_wrapper m, Clause c) throws Error{
		/*if (c.is(PI.directive_1)){
			m = ____PROCESS_DIRECTIVE(eng, m, c, null);
		}else*/ if (m == eng.user){
			eng.user.module.retract(c);
			Clause head = c.is(PI.rule_n)? c.arg(0).asClause(): c;
			Clause q = new Clause(head.quoted);
			for (int i=0; i<head.arity(); i++) q.add(new Var("_"));
			try{
				Session.create(eng, m, q).solveNext();
			}catch(Error pe){
				if (pe.error().is(PI.existence_error_2)){
					eng.user.module.abolish(q.pi());
				}else{
					throw pe;
				}
			}
		}
		return m;
	}
		
	/* -------------------------------------------------------------
	 * STATIC PROCESS DIRECTIVES
	 * ------------------------------------------------------------- */
	
	// modules == null ---> UNconsulting !!
	/*
	private static _m_wrapper ____PROCESS_DIRECTIVE(Engine eng, _m_wrapper m, Clause c, List<_m_wrapper> modules) throws Error{
		c = c.arg(0).asClause();
		if (c.is(PI.module_1) || c.is(PI.module_2)){
			Clause name = c.arg(0).asClause();
			if (!name.isType(PI.atom_0)){
				throw Error.type(PI.atom_0, name);
			}
			String $name = name.quoted;
			String type = Engine.DEFAULT_MODULE_TYPE;
			if (c.is(PI.module_2)){
				Clause ctype = c.arg(1).asClause();
				if (!ctype.isType(PI.atom_0)){
					throw Error.type(PI.atom_0, ctype);
				}
				type = ctype.name;
			}
			if (modules != null){ // ie. ==> consulting
				return eng.__CREATE_MODULE($name, type, null);
			}else{ // ie. ==> unconsulting
				return eng.__GET_MODULE($name);
			}
		}else if (c.is(PI.end_module_0)){
			return eng.user;
		}else if (c.is(PI.import_module_1)){
			String $imported = c.arg(0).asClause().name;
			if (!m.imported.contains($imported)) m.imported.add($imported);
		}else if (c.is(PI.dynamic_1)){
			m.declareDirective(c.arg(0).asClause(), PI.dynamic_1, modules != null);
		}else if (c.is(PI.multifile_1)){
			m.declareDirective(c.arg(0).asClause(), PI.multifile_1, modules != null);
		}else if (c.is(PI.discontiguous_1)){
			m.declareDirective(c.arg(0).asClause(), PI.discontiguous_1, modules != null);
		}else if (c.is(PI.op_3)){
			if (modules == null){
				// priority 0 --> remove op
				c = new Clause(PI.op_3, 0, c.arg(1), c.arg(2));
			}
			m.execute(c);
		}else if (c.is(PI.char_conversion_2)){
			if (modules == null){
				Term in = c.arg(0);
				c = new Clause(PI.char_conversion_2);
				c.add(in);
				c.add(in); // ---> in-out mapping is removed !!
			}
			m.execute(c);
		}else if (c.is(PI.initialization_1)){
			if (modules != null){
				Clause goal = c.arg(0).asClause();
				if (!goal.is(PI.directive_1)) m.init_clauses.add(goal);
			}
		}else if (c.is(PI.include_1)){
			if (modules != null){
				____CONSULT_FILE(eng, c.arg(0).asClause(), m, modules);
			}else{
				unconsultFile(eng, c.arg(0).asClause());
			}
		}else if (c.is(PI.ensure_loaded_1)){
			// TODO implement me !!!
			eng.__WARN("Ignoring directive: " + c.toString());
		}else if (c.is(PI.set_prolog_flag_2)){
			if (modules != null) m.execute(c);
		}else{
			eng.__WARN("Ignoring directive: " + c.toString());
		}
		return m;
	}*/
	
	/* -------------------------------------------------------------
	 * ASSERT
	 * ------------------------------------------------------------- */
	
	Clause add(Clause c, boolean assertion, boolean append) throws Error{
		/*if (c.is(PI.parse_n)){
			c = ___CONVERT_TO_DCG(c);
			assertion = false; // TODO not sure !! (if true -> need to declare dynamics)
		}*/
		___CHECK_OK_TO_CONSULT(c);
		//if (assertion) ___CHECK_OK_TO_ASSERT(c);
		module.assertAZ(c, append);
		c.context = this;
		return c;
	}
	
	private void ___CHECK_OK_TO_CONSULT(Clause c) throws Error{
		if (c == null) throw new IllegalArgumentException("No Clause specified");
		//if (c.is(PI.directive_1)) return;
		Clause head = c.is(PI.rule_n)? c.arg(0).asClause(): c;
		head = head.asCallable();
		if (c.is(PI.rule_n)){
			List<Var> singletons = new Var.Accessor(c).singletons();
			for (int i=1; i<c.arity(); i++){
				Term subgoal = c.arg(i);
				if (subgoal.isType(PI.var_0)){
					if (singletons.contains((Var)subgoal)){
						throw Error.type(PI.callable_0, subgoal);
					}
				}else{
					subgoal.asCallable();
				}
			}
		}
	}
	/*
	private void ___CHECK_OK_TO_ASSERT(Clause c) throws Error{
		if (c == null) throw new IllegalArgumentException("No Clause specified");
		PI pi = c.pi();
		if(!isDynamic(pi)){
			throw Error.permission(PI.modify_0, PI.static_procedure_0, pi.asClause());
		}
	}*/
	/*
	boolean isDynamic(PI pi){
		try{
			return this.____GET_DECLARATION(pi, PI.dynamic_1) != null;
		}catch(Exception e){
			return false;
		}
	}*/
		
	/* -------------------------------------------------------------
	 * DIRECTIVES
	 * ------------------------------------------------------------- */
	/*
	void declareDirective(Clause c, PI type, boolean consulting) throws Error{
		// c is arg in dynamic(arg) | multifile(arg) | discontiguous(arg)
		if (c.ispi()){
			List<Term> tlist = new ArrayList<Term>();
				tlist.add(c);
			____DECLARE_DIRECTIVE(tlist, type, consulting);
		}else if (c.is(PI.conjunction_2)){
			while (c.is(PI.conjunction_2)){
				declareDirective(c.arg(0).asClause(), type, consulting);
				c = c.arg(1).asClause();
			}
			declareDirective(c, type, consulting);
		}else if (Term.isPrologList(c)){
			____DECLARE_DIRECTIVE(Term.fromPrologList(c), type, consulting);
		}else{
			throw Error.type(PI.predicate_indicator_0, c);
		}
	}
	
	private void ____DECLARE_DIRECTIVE(List<Term> terms, PI type, boolean consulting) throws Error{
		for (Term t : terms){
			Clause c = t.asClause();
			if (!c.ispi()){
				throw Error.type(PI.predicate_indicator_0, c);
			}
			String name = c.arg(0).asClause().name;
			int arity = c.arg(1).asClause().asInteger();
			PI pi = new PI(name, arity);
			if (Engine.__IS_BI(pi)){
				throw Error.permission(PI.modify_0, PI.private_procedure_0, c);
			}else{ 
				Clause decl = ____GET_DECLARATION(pi, type);
				if (consulting){
					if (decl == null){
						Clause z = new Clause(type, pi.asClause());
						module.assertAZ(z, true);
					}
				}else{
					if (decl != null) module.retract(decl);
				}
			}
		}
	}
	
	private Clause ____GET_DECLARATION(PI pi, PI type) throws Error{
		Clause.Iterator it = controlledCandidates(type, true);
		if (it != null){
			Clause c = it.next();
			while (c != null){
				if (c.arg(0).equals(pi.asClause())) return c;
				c = it.next();
			}
		}
		return null;
	}
	
	void execute(Clause c) throws Error{
		Clause query = new Clause(PI.query_n);
		query.add(c);
		if (!Session.create(eng, this, query).solveNext()){
			eng.__WARN("goal " + c.toString() + " failed");
		}
	}*/
	
	/* -------------------------------------------------------------
	 * DEFINITE CLAUSE GRAMMAR TRANSLATION
	 * ------------------------------------------------------------- */
	/*
	private Clause ___CONVERT_TO_DCG(Clause c) throws Error{
		Clause rule = new Clause(PI.rule_n);
			Clause lhs = c.arg(0).asClause();
			Clause head = new Clause(lhs.quoted);
			for (int i=0; i<lhs.arity(); i++) head.add(lhs.arg(i));
			List<Var> vars = new ArrayList<Var>();
			Var List = Var.autogen();
			vars.add(List);
			head.add(List);
			Var Result = Var.autogen();
			head.add(Result);
		rule.add(head);
		int arity = c.arity();
		for (int i=1; i<arity; i++){
			Clause rhs = c.arg(i).asClause();
			if (rhs.is(PI.list_2)){
				Clause unify = new Clause(PI.unify_2);
				unify.add(vars.get(vars.size()-1));
				List<Term> terminals = Term.fromPrologList(rhs);
				Clause list = new Clause(PI.list_2);
				while (!terminals.isEmpty()){
					Term t = terminals.remove(0);
					list.add(t);
					if (!terminals.isEmpty()){
						Clause rest = new Clause(PI.list_2);
						list.add(rest);
						list = rest;
					}
				}
				if (i == arity-1){
					list.add(Result);
				}else{
					Var v = Var.autogen();
					vars.add(v);
					list.add(v);
				}
				unify.add(list);
				rule.add(unify);
			}else if (rhs.is(PI.curly_brace_n)){
				for (int j=0; j<rhs.arity(); j++) rule.add(rhs.arg(j));
				if (i == arity-1){
					Clause unify = new Clause(PI.unify_2);
					unify.add(vars.get(vars.size()-1));
					unify.add(Result);
					rule.add(unify);
				}
			}else{
				Clause subgoal = new Clause(rhs.quoted);
				for (int j=0; j<rhs.arity(); j++) subgoal.add(rhs.arg(j));
				subgoal.add(vars.get(vars.size()-1));
				if (i == arity-1){
					subgoal.add(Result);
				}else{
					Var v = Var.autogen();
					vars.add(v);
					subgoal.add(v);
				}
				rule.add(subgoal);
			}
		}
		return rule;
	}*/
	
	/* -------------------------------------------------------------
	 * FLAGS
	 * ------------------------------------------------------------- */
	/*
	private void ____INIT_FLAGS() throws Error{
		module.assertAZ(____DEFAULT_FLAG_VALUE(new Clause(PI.bounded_0)), true);
		module.assertAZ(____DEFAULT_FLAG_VALUE(new Clause(PI.max_integer_0)), true);
		module.assertAZ(____DEFAULT_FLAG_VALUE(new Clause(PI.min_integer_0)), true);
		module.assertAZ(____DEFAULT_FLAG_VALUE(new Clause(PI.integer_rounding_function_0)), true);
		module.assertAZ(____DEFAULT_FLAG_VALUE(new Clause(PI.char_conversion_0)), true);
		module.assertAZ(____DEFAULT_FLAG_VALUE(new Clause(PI.debug_0)), true);
		module.assertAZ(____DEFAULT_FLAG_VALUE(new Clause(PI.max_arity_0)), true);
		module.assertAZ(____DEFAULT_FLAG_VALUE(new Clause(PI.unknown_0)), true);
		module.assertAZ(____DEFAULT_FLAG_VALUE(new Clause(PI.double_quotes_0)), true);
	}*/
	/*
	void setFlag(Term flag, Term value) throws Error{
		Clause cflag = flag.asClause();
		if (cflag.is(PI.bounded_0) ||
			cflag.is(PI.integer_rounding_function_0) ||
			cflag.is(PI.max_arity_0) ||
			cflag.is(PI.max_integer_0) ||
			cflag.is(PI.min_integer_0)){
			throw Error.permission(PI.modify_0, PI.flag_0, cflag);
		}
		Clause cvalue = value.asClause();
		if (!cflag.isType(PI.atom_0)){
			throw Error.type(PI.atom_0, cflag);
		}
		if (!cvalue.isType(PI.atom_0)){
			throw Error.type(PI.atom_0, cvalue);
		}
		PI f = null;
		PI v = null;
		if (cflag.is(PI.char_conversion_0)){
			f = PI.char_conversion_0;
			v = cvalue.is(PI.on_0)? PI.on_0:
				cvalue.is(PI.off_0)? PI.off_0:
				null;
		}else if (cflag.is(PI.debug_0)){
			f = PI.debug_0;
			v = cvalue.is(PI.on_0)? PI.on_0:
				cvalue.is(PI.off_0)? PI.off_0:
				null;
		}else if (cflag.is(PI.double_quotes_0)){
			f = PI.double_quotes_0;
			v = cvalue.is(PI.chars_0)? PI.chars_0:
				cvalue.is(PI.codes_0)? PI.codes_0:
				cvalue.is(PI.atom_0)? PI.atom_0:
				null;
		}else if (cflag.is(PI.unknown_0)){
			f = PI.unknown_0;
			v = cvalue.is(PI.error_0)? PI.error_0:
				cvalue.is(PI.fail_0)? PI.fail_0:
				cvalue.is(PI.warning_0)? PI.warning_0:
				null;
		}else{
			throw Error.domain(PI.prolog_flag_0, cflag);
		}
		if (v != null){
			module.retract(____FLAG_CLAUSE(cflag));
			module.assertAZ(new Clause(PI.current_prolog_flag_2, f, v), true);
		}else{
			throw Error.domain(PI.flag_value_0, new Clause(PI.add_2, cflag, cvalue));
		}
	}
	
	Clause getFlag(Clause flag) throws Error{
		return ____FLAG_CLAUSE(flag).arg(1).asClause();
	}
	
	private Clause ____FLAG_CLAUSE(Clause flag) throws Error{
		if (!flag.isType(PI.atom_0)){
			throw Error.type(PI.atom_0, flag);
		}
		Clause.Iterator it = controlledCandidates(PI.current_prolog_flag_2, true);
		if (it != null){
			Clause c = it.next();
			while(c != null){
				if (c.arg(0).equals(flag)) return c;
				c = it.next();
			}
		}
		throw Error.domain(PI.prolog_flag_0, flag);
	}
	
	private Clause ____DEFAULT_FLAG_VALUE(Clause flag) throws Error{
		Object value = 
			flag.is(PI.bounded_0)? PI.true_0:
			flag.is(PI.max_integer_0)? Integer.MAX_VALUE:
			flag.is(PI.min_integer_0)? Integer.MIN_VALUE:
			flag.is(PI.integer_rounding_function_0)? PI.down_0:
			flag.is(PI.char_conversion_0)? PI.on_0:
			flag.is(PI.debug_0)? PI.off_0:
			flag.is(PI.max_arity_0)? Engine.MAX_ARITY:
			flag.is(PI.unknown_0)? PI.error_0:
			flag.is(PI.double_quotes_0)? PI.codes_0:
			null;
		if (value == null){
			throw Error.domain(PI.prolog_flag_0, flag.asClause());
		}
		return new Clause(PI.current_prolog_flag_2, flag, value);
	}*/
	
	/* -------------------------------------------------------------
	 * CHAR CONVERSION
	 * ------------------------------------------------------------- */
	/*
	void setCharConversion(Term a, Term b) throws Error{
		Clause cin = a.asClause();
		String in = cin.name;
		if (in.length() != 1){
			throw Error.type(PI.character_0, cin);
		}
		Clause cout = b.asClause();
		String out = cout.name;
		if (out.length() != 1){
			throw Error.type(PI.character_0, cout);
		}
		Map<Character, Clause> cmap = ____GET_CHAR_CONVERSION_MAP();
		Clause c = cmap.get(in.charAt(0));
		if (c != null){
			module.retract(c);
		}
		if (!in.equals(out)){
			module.assertAZ(new Clause(PI.current_char_conversion_2, cin, cout), true);
		}
	}*/
	/*
	private Map<Character, Clause> ____GET_CHAR_CONVERSION_MAP() throws Error{
		Clause.Iterator it = controlledCandidates(PI.current_char_conversion_2, true);
		Map<Character, Clause> cmap = new Hashtable<Character, Clause>();
		if (it != null){
			Clause c = it.next();
			while (c != null){
				char in = c.arg(0).asClause().asChar();
				cmap.put(in, c);
				c = it.next();
			}
		}
		return cmap;
	}
	
	String convert(String s) throws Error{
		if (module.name().equals(Engine.SYSTEM_MODULE_NAME)) return s;
		Map<Character, Clause> cmap = ____GET_CHAR_CONVERSION_MAP();
		StringBuffer b = new StringBuffer();
		for (int i=0; i<s.length(); i++){
			char in = s.charAt(i);
			Clause map = cmap.get(in);
			if (map != null){
				b.append(map.arg(1).asClause().asChar());
			} else{
				b.append(in);
			}
		} 
		return b.toString();
	}*/
	
	/* -------------------------------------------------------------
	 * OPERATORS
	 * ------------------------------------------------------------- */
	
	void declareOperator(Clause c) throws Error{
		Clause[] ops = ____EXTRACT_OPS(c);
		for (Clause x : ops){
			declareOperator(x, true);
		}
	}
	
	void declareOperator(Clause cop, boolean controlled) throws Error{
		Clause operator = cop.arg(2).asClause();
		if (cop.arg(0).asClause().asInteger() == 0){
			module.retract(cop);
		}else{
			if (controlled){
				if (operator.quoted.equals(PI.conjunction_2.name)){
					throw Error.permission(PI.modify_0, PI.operator_0, operator);
				}else if (____IS_PROHIBITED(cop)){
					throw Error.permission(
						PI.create_0, 
						PI.operator_0, 
						new Clause(PI.op_3, cop.arg(0), cop.arg(1), cop.arg(2))
					);
				}
			}
			module.assertAZ(cop, true);
		}
	}
	
	private _op.Type ____TYPE(Term specifier){
		return specifier.is(PI.xf_0) || specifier.is(PI.yf_0)? _op.Type.POSTFIX:
			specifier.is(PI.fx_0) || specifier.is(PI.fy_0)? _op.Type.PREFIX:
			_op.Type.INFIX;
	}
	
	private boolean ____IS_PROHIBITED(Clause cop) throws Error{
		Clause operator = cop.arg(2).asClause();
		_op.Type type = ____TYPE(cop.arg(1));
		_op op = getOperator(operator.quoted, type);
		return (op != null && op.cop.arg(1).equals(cop.arg(1))) ||
		((type == _op.Type.INFIX) && getOperator(operator.quoted, _op.Type.POSTFIX) != null) ||
		((type == _op.Type.POSTFIX) && getOperator(operator.quoted, _op.Type.INFIX) != null);
	}
	
	_op getOperator(String operator, _op.Type type) throws Error{
		Clause.Iterator it = controlledCandidates(PI.current_op_3, true);
		if (it != null){
			Clause c = it.next();
			while (c != null){
				Clause op = c.arg(2).asClause();
				if (____TYPE(c.arg(1)) == type && 
					(op.name.equals(operator) || op.quoted.equals(operator))
					){
					return new _op(c);
				}
				c = it.next();
			}
		}
		return null;
	}
	
	private Clause[] ____EXTRACT_OPS(Clause c) throws Error{
		Clause priority = c.arg(0).asClause();
		if (!priority.isType(PI.integer_0)){
			throw Error.type(PI.integer_0, priority);
		}
		int p = priority.asInteger();
		if (p < 0 || p > _op.MAX_PRIORITY){
			throw Error.domain(PI.operator_priority_0, priority);
		}
		Clause specifier = c.arg(1).asClause();
		if (!specifier.isType(PI.atom_0)){
			throw Error.type(PI.atom_0, specifier);
		}
		if (specifier.is(PI.fx_0) || 
			specifier.is(PI.fy_0) ||
			specifier.is(PI.xf_0) || 
			specifier.is(PI.yf_0) ||
			specifier.is(PI.xfx_0) || 
			specifier.is(PI.xfy_0) ||
			specifier.is(PI.yfx_0)){
		}else{
			throw Error.domain(PI.operator_specifier_0, specifier);
		}
		Clause operator = c.arg(2).asClause();
		List<Clause> list = new ArrayList<Clause>();
		List<Term> operators = null;
		if (Term.isPrologList(operator)){
			operators = Term.fromPrologList(operator); // --> type error
		}else{
			operators = new ArrayList<Term>();
			operators.add(operator);
		}
		for (Term t : operators){
			operator = t.asClause();
			if (!operator.isType(PI.atom_0)){
				throw Error.type(PI.atom_0, operator);
			}
			list.add(new Clause(PI.current_op_3, priority, specifier, operator));
		}
		return list.toArray(new Clause[0]);
	}
	
}
