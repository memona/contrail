/* 
SVN FILE: $Id: ____plog_query_parser.java 1944 2011-05-31 14:36:04Z kevenkt $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: kevenkt $
@version        $Rev: 1944 $
@lastrevision   $Date: 2011-05-31 16:36:04 +0200 (tor, 31 maj 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/____plog_query_parser.java $

*/

package org.slasoi.gslam.templateregistry;

import org.slasoi.gslam.templateregistry.plog.Clause;
import org.slasoi.gslam.templateregistry.plog.Engine;
import org.slasoi.gslam.templateregistry.plog.Error;
import org.slasoi.gslam.templateregistry.plog.Var;
import org.slasoi.slamodel.sla.SLATemplate;

class ____plog_query_parser {
	
	static String p_PARSE(Engine engine, SLATemplate query) throws Error{ 
		
		// USE "MOD____ID" AS MODULE ID
		
		// THIS IS A B6 SPECIFIC IMPLEMENTATION !!!!
		/*
		 * gets price from current module 'M'
		  M:guaranteed_action(offerPrice,'http://www.slaatsoi.org/slamodel#provider',price(PRICE,UNITS)).
		 */
		// System.out.println("Parsing SLAT Query --> Prolog");
		String $TARIFF = "TARIFF";
		Var TARIFF = new Var("TARIFF");
		
		Clause tariff_usage = ____plog_common.p_EXTRACT_MONTHLY_TARIFF_USAGE_PRICE(query, TARIFF);
        if (tariff_usage != null){
        	StringBuffer buf = new StringBuffer();
        	
        	// M:monthly_tariff_usage(TARIFF,'http://www.slaatsoi.org/coremodel#equals'),
        	
        	buf.append(____plog.MOD____ID);
        	buf.append(":");
        	buf.append(____plog_common.$_monthly_tariff_usage);
        	buf.append("(");
        	buf.append($TARIFF);
        	buf.append(")");
        	
        	// TARIFF USAGE DOMAIN
        	buf.append(", ");
        	buf.append(tariff_usage.toString());
    		
    		return buf.toString();
        }
		
		return null;
	}
	
}
