/* 
SVN FILE: $Id: PI.java 1944 2011-05-31 14:36:04Z kevenkt $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: kevenkt $
@version        $Rev: 1944 $
@lastrevision   $Date: 2011-05-31 16:36:04 +0200 (tor, 31 maj 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/PI.java $

*/

package org.slasoi.gslam.templateregistry.plog;

import java.io.Serializable;

/**
 * Utility constants for Zen Built-In terms/predicates.
 * @author Keven T. Kearney
 */
public final class PI implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String name = null;
	int arity = 0;
	
	public PI(String name, int arity){
		this.name = name;
		this.arity = arity;
	}
	
	public PI(int name, int arity){
		this.name = "" + name;
		this.arity = arity;
	}
	
	public String name(){
		return name;
	}
	
	public int arity(){
		return arity;
	}
	
	public Clause asClause() throws Error{
		Clause pi = new Clause(PI.divide_2);
		pi.add(new Clause(name));
		pi.add(new Clause(arity));
		return pi;
	}
	
	public int hashCode(){
		return (name + arity).hashCode();
	}
	
	public boolean equals(Object o){
		return o instanceof PI && ((PI)o).name.equals(name) && ((PI)o).arity == arity;
	}
	
	public String toString(){
		return name + "/" + arity;
	}
	
	public interface Iterator{
		
		public PI next();
		
	}

	public static final PI abs_1 = new PI("abs",1);
	public static final PI add_2 = new PI("+",2);
	public static final PI add_3 = new PI("add",3);
	public static final PI and_n = new PI("and",-1);
	public static final PI arg_3 = new PI("arg",3);
	public static final PI asserta_1 = new PI("asserta",1);
	public static final PI assertz_1 = new PI("assertz",1);
	public static final PI atan_1 = new PI("atan",1);
	public static final PI atomic_1 = new PI("atomic",1);
	public static final PI bagof_3 = new PI("bagof",3);
	public static final PI bitwise_and_2 = new PI("/\\",2);
	public static final PI bitwise_complement_1 = new PI("\\",1);
	public static final PI bitwise_left_shift_2 = new PI("<<",2);
	public static final PI bitwise_or_2 = new PI("\\/",2);
	public static final PI bitwise_right_shift_2 = new PI(">>",2);
	public static final PI call_1 = new PI("call",1);
	public static final PI ceiling_1 = new PI("ceiling",1);
	public static final PI clause_2 = new PI("clause",2);
	public static final PI compound_1 = new PI("compound",1);
	public static final PI conjunction_2 = new PI("','",2);
	public static final PI constructor_2 = new PI("=..",2);
	public static final PI copy_term_2 = new PI("copy_term",2);
	public static final PI cos_1 = new PI("cos",1);
	public static final PI current_op_3 = new PI("current_op",3);
	public static final PI cut_0 = new PI("!",0);
	public static final PI disjunction_2 = new PI("';'",2);
	public static final PI divide_2 = new PI("/",2);
	public static final PI emptylist_0 = new PI("[]",0);
	public static final PI equal_2 = new PI("=:=",2);
	public static final PI exp_1 = new PI("exp",1);
	public static final PI exists_1 = new PI("exists",1);
	public static final PI fail_0 = new PI("fail",0);
	public static final PI false_0 = new PI("false", 0);
	public static final PI findall_3 = new PI("findall",3);
	public static final PI float_1 = new PI("float",1);
	public static final PI float_fractional_part_1 = new PI("float_fractional_part",1);
	public static final PI float_integer_part_1 = new PI("float_integer_part",1);
	public static final PI floor_1 = new PI("floor",1);
	public static final PI functor_3 = new PI("functor",3);
	public static final PI gt_2 = new PI(">",2);
	public static final PI gte_2 = new PI(">=",2);
	public static final PI halt_0 = new PI("halt",0);
	public static final PI halt_1 = new PI("halt",1);
	public static final PI hat_2 = new PI("^",2);
	public static final PI if_2 = new PI("->",2);
	public static final PI int_divide_2 = new PI("//",2);
	public static final PI integer_1 = new PI("integer",1);
	public static final PI is_2 = new PI("is",2);
	public static final PI list_2 = new PI(".",2);
	public static final PI log_1 = new PI("log",1);
	public static final PI lt_2 = new PI("<",2);
	public static final PI lte_2 = new PI("=<",2);
	public static final PI max_arity_0 = new PI("max_arity",0);
	public static final PI max_integer_0 = new PI("max_integer",0);
	public static final PI min_integer_0 = new PI("min_integer",0);
	public static final PI mod_2 = new PI("mod",2);
	public static final PI multiply_2 = new PI("*",2);
	public static final PI negate_1 = new PI("-",1);
	public static final PI nonvar_1 = new PI("nonvar",1);
	public static final PI not_1 = new PI("not",1);
	public static final PI not_equal_2 = new PI("=\\=",2);
	public static final PI not_provable_1 = new PI("\\+",1);
	public static final PI not_unify_2 = new PI("\\=",2);
	public static final PI number_1 = new PI("number",1);
	public static final PI once_1 = new PI("once",1);
	public static final PI op_3 = new PI("op",3);
	public static final PI or_n = new PI("or",-1);
	public static final PI power_2 = new PI("**",2);
	public static final PI query_n = new PI("?-",-1); // -1 --> 2 or more args
	public static final PI reg_expr_match_2 = new PI("reg_expr_match",2);
	public static final PI rem_2 = new PI("rem",2);
	public static final PI repeat_0 = new PI("repeat",0);
	public static final PI retract_1 = new PI("retract",1);
	public static final PI round_1 = new PI("round",1);
	public static final PI rule_n = new PI(":-",-2); // -2 --> 2 or more args
	public static final PI sign_1 = new PI("sign",1);
	public static final PI sin_1 = new PI("sin",1);
	public static final PI setof_3 = new PI("setof",3);
	public static final PI sqrt_1 = new PI("sqrt",1);
	public static final PI stnd_equal_2 = new PI("eq",2);
	public static final PI stnd_not_equal_2 = new PI("not_eq",2);
	public static final PI stnd_gt_2 = new PI("gt",2);
	public static final PI stnd_gte_2 = new PI("gte",2);
	public static final PI stnd_lt_2 = new PI("lt",2);
	public static final PI stnd_lte_2 = new PI("lte",2);
	public static final PI subtract_2 = new PI("-",2);
	public static final PI true_0 = new PI("true",0);
	public static final PI truncate_1 = new PI("truncate",1);
	public static final PI unify_2 = new PI("=",2);
	public static final PI var_1 = new PI("var",1);
	// ERROR
	public static final PI type_error_2 = new PI("type_error",2);
	public static final PI system_error_0 = new PI("system_error",0);
	public static final PI unknown_0 = new PI("unknown",0);
	public static final PI syntax_error_1 = new PI("syntax_error",1);
	public static final PI resource_error_1 = new PI("resource_error",1);
	public static final PI representation_error_1 = new PI("representation_error",1);
	public static final PI permission_error_3 = new PI("permission_error",3);
	public static final PI instantiation_error_0 = new PI("instantiation_error",0);
	public static final PI existence_error_2 = new PI("existence_error",2);
	public static final PI evaluation_error_1 = new PI("evaluation_error",1);
	public static final PI domain_error_2 = new PI("domain_error",2);
	// ERROR FLAGS
	public static final PI predicate_indicator_0 = new PI("predicate_indicator",0);
	public static final PI not_less_than_zero_0 = new PI("not_less_than_zero",0);
	public static final PI operator_priority_0 = new PI("operator_priority",0);
	public static final PI operator_specifier_0 = new PI("operator_specifier",0);
	public static final PI undefined_0 = new PI("undefined",0);
	public static final PI zero_divisor_0 = new PI("zero_divisor",0);
	public static final PI access_0 = new PI("access",0);
	public static final PI create_0 = new PI("create",0);
	public static final PI modify_0 = new PI("modify",0);
	public static final PI operator_0 = new PI("operator",0);
	public static final PI private_procedure_0 = new PI("private_procedure",0);
	// OP-CODES
	public static final PI fx_0 = new PI("fx",0);
	public static final PI fy_0 = new PI("fy",0);
	public static final PI xf_0 = new PI("xf",0);
	public static final PI xfx_0 = new PI("xfx",0);
	public static final PI xfy_0 = new PI("xfy",0);
	public static final PI yf_0 = new PI("yf",0);
	public static final PI yfx_0 = new PI("yfx",0);
	// TYPE TESTS
	public static final PI atom_0 = new PI("atom",0);
	public static final PI atomic_0 = new PI("atomic",0);
	public static final PI callable_0 = new PI("callable",0);
	public static final PI character_0 = new PI("character",0);
	public static final PI evaluable_0 = new PI("evaluable",0);
	public static final PI integer_0 = new PI("integer",0);
	public static final PI list_0 = new PI("list",0);
	public static final PI number_0 = new PI("number",0);
	public static final PI compound_0 = new PI("compound",0);
	public static final PI nonvar_0 = new PI("nonvar",0);
	public static final PI float_0 = new PI("float",0);
	public static final PI var_0 = new PI("var",0);
	// LISTS
	public static final PI conc_3 = new PI("conc",3);
	public static final PI member_2 = new PI("member",2);
	// MODULES
	public static final PI colon_2 = new PI(":",2);
	public static final PI module_0 = new PI("module",0);
	public static final PI calling_context_1 = new PI("calling_context",1);
	public static final PI current_module_1 = new PI("current_module",1);
	public static final PI current_module_2 = new PI("current_module",2);
	// CODE LISTING
	public static final PI listing_0 = new PI("listing",0);

}
