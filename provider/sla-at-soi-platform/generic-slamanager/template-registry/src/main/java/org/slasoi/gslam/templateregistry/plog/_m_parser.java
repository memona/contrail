/* 
SVN FILE: $Id: _m_parser.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/_m_parser.java $

*/

package org.slasoi.gslam.templateregistry.plog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


final class _m_parser {
	
	private boolean WRITE_LIST_SYNTAX = true;
	
	public static void main(String[] args){
		try{
			String s = "ns:obsv#1 < 4.3^^ns:float.";
			Engine eng = new Engine();
			Clause c = eng.parse(s);
			System.out.println(">> " + c.toString());
			System.out.println(">> " + eng.write(c,true));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private _m_wrapper mw = null;
	
	_m_parser(_m_wrapper mw){
		this.mw = mw;
	}
	
	/* -------------------------------------------------------------
	 * PARSING
	 * ------------------------------------------------------------- */
	
	Clause parse(String s) throws Error{
		__tokenlist tokens = tokenize(s);
		String last = tokens.last();
		if (last == null || !last.equals(".")){
			tokens.add(".");
			throw Error.syntax("Prolog string must end with '.'");
		}
		Term t = parse(tokens, _op.MAX_PRIORITY);
		if (t != null && tokens.finished()){
			Clause c = Term.instantiate((Clause)t, false);
			if (c.is(PI.rule_n) || c.is(PI.query_n)){
				Clause x = new Clause(c.name);
				int start = 0;
				if (c.is(PI.rule_n)){
					x.add(c.arg(0));
					start = 1;
				}
				for (int i=start; i<c.arity(); i++){
					Term arg = c.arg(i);
					if (arg.is(PI.conjunction_2)){
						List<Term> args = Term.fromPrologConjunction(c.arg(i));
						for (Term a : args) x.add(a);
					}else{
						x.add(arg);
					}
				}
				c = x;
			}
			return c;
		}
		throw Error.syntax("Unable to parse: " + s);
	}
	
	private Term parse(__tokenlist tokens, int priority) throws Error{
		if (!tokens.more()) return null;
		int mark = tokens.index();
		_op op = mw.getOperator(tokens.next(), _op.Type.PREFIX);
		if (op != null && !tokens.peek().equals("(") && op.priority <= priority){
			Term t = parse(tokens, op.post_priority);
			if (t != null){
				String op_name = op.name;
				Clause c = new Clause(op_name);
				if (op_name.equals(PI.negate_1.name()) && t.isType(PI.number_0)){
					c = new Clause("-" + t.toString());
				}else{
					c.add(t);
				}
				t = parse_subsequent(c, op.priority, tokens, priority);
				if (t != null) return t;
				else return c;
			}
		}
		tokens.reset(mark);
		Term t = parse(tokens);
		if (t != null){
			Term t1 = parse_subsequent(t, 0, tokens, priority); 
			if (t1 != null) return t1;
			else return t;
		}
		tokens.reset(mark);
		return null;
	}
	
	private Term parse(__tokenlist tokens) throws Error{
		if (!tokens.more()) return null;
		int mark = tokens.index();
		String s = tokens.next();
		try{
			if (isVar(s)) return new Var(s);
			else if (s.equals("[")) return parse_list(tokens);
			else if (s.equals(" (")){
				Term t = parse_conjunction(tokens);
				if (t != null) return t;
				return parse_brackets(tokens);
			}else if (s.equals("(")){
				return parse_brackets(tokens);
			}else{
				Clause c = null;
				if (s.startsWith("\"") && s.endsWith("\"")){
					s = "'" + s.substring(1, s.length()-1) + "'";
					c = new Clause(s);
				}else{
					c = new Clause(s);
				}
				if (tokens.peek().equals("(")){
					tokens.next();
					while(true){
						Term a = parse(tokens, 999);
						if (a == null) return null;
						c.add(a);
						String next = tokens.next();
						if (next.equals(")")) return c;
						else if (!next.equals(",")) return null;
					}
				}
				return c;
			}
		}catch(Exception e){
			// do nothing
		}
		tokens.reset(mark);
		return null;
	}
	
	private Term parse_brackets(__tokenlist tokens) throws Error{
		List<Term> list = new ArrayList<Term>();
		while(true){
			Term a = parse(tokens, _op.MAX_PRIORITY);
			if (a == null) return null;
			list.add(a);
			String next = tokens.next();
			if (next.equals(")")){
				if (list.size() == 1) return list.get(0);
				else return Term.toPrologConjunction(list);
			}
		}
	}
	
	private Term parse_subsequent(Term prev, int prev_priority, __tokenlist tokens, int top_priority) throws Error{
		if (!tokens.more()) return null;
		int mark = tokens.index();
		_op op = mw.getOperator(tokens.next(), _op.Type.POSTFIX);
		if (op != null && op.priority <= top_priority && prev_priority < op.pre_priority){
			Clause x = new Clause(op.name);
				x.add(prev);
			Term t = parse_subsequent(x, op.priority, tokens, top_priority);
			if (t != null) return t;
		}
		tokens.reset(mark);
		String s = tokens.next();
		op = mw.getOperator(s, _op.Type.INFIX); 
		if (op != null && op.priority <= top_priority && prev_priority < op.pre_priority){
			Term t = parse(tokens, op.post_priority);
			if (t != null){
				t = parse_infix(prev, op, t, tokens, top_priority);
				if (t != null) return t;
			}
		}
		tokens.reset(mark);
		return null;
	}
	
	private Term parse_infix(Term t1, _op op1, Term t2, __tokenlist tokens, int top_priority) throws Error{
		Clause _default = new Clause(op1.quoted);
			_default.add(t1);
			_default.add(t2);
		if (!tokens.more()) return _default;
		int mark = tokens.index();
		_op op2 = mw.getOperator(tokens.next(), _op.Type.INFIX);
		if (op2 != null && op2.priority <= top_priority){
			Term t3 = parse(tokens, op2.post_priority);
			if (t3 != null){
				int u = under(op1, op2);
				if (u == 1){
					Term t = parse_infix(_default, op2, t3, tokens, top_priority);
					if (t != null) return t;
				}else if (u == 2){
					Clause x = new Clause(op2.quoted);
					x.add(t2);
					x.add(t3);
					Clause c = new Clause(op1.quoted);
					c.add(t1);
					c.add(x);
					return c;
				}
			}
		}else{
			tokens.reset(mark);
			return _default;
		}
		tokens.reset(mark);
		return null;
	}
	
	private int under(_op op1, _op op2){
		int p1 = op1.priority;
		int p2 = op2.priority;
		if (p1 < p2) return 1;
		if (p1 > p2) return 2;
		if (!op2.prex) return 1;
		if (!op1.postx) return 2;
		return 0;
	}
	
	private Clause parse_list(__tokenlist tokens) throws Error{
		int mark = tokens.index();
		Term head = parse(tokens, 999);
		if (head != null){
			String next = tokens.next();
			if (next.equals("]")){
				return make_list(head, new Clause(PI.emptylist_0), false);
			}else if (next.equals(",")){
				Term tail = parse_list(tokens);
				if (tail != null){
					return make_list(head, tail, false);
				}
			}else if (next.equals("|")){
				Term tail = parse(tokens, 999);
				if (tail != null && tokens.next().equals("]")){
					return make_list(head, tail, true);
				}
			}
		}
		tokens.reset(mark);
		return null;
	}
	
	private Clause make_list(Term head, Term tail, boolean bar) throws Error{
		Clause c = new Clause(PI.list_2);
		c.add(head);
		if (bar && !(tail.isType(PI.var_0) || Term.isPrologList(tail))){
			throw Error.syntax("List element following '|' must be a list");
		}
		c.add(tail);
		return c;
	}
	
	private Term parse_conjunction(__tokenlist tokens) throws Error{
		int mark = tokens.index();
		Term first = parse(tokens, 999);
		if (first != null){
			String next = tokens.next();
			if (next.equals(")")){
				return first;
			}else if (next.equals(",")){
				Term rest = parse_conjunction(tokens);
				if (rest != null){
					Clause c = new Clause(PI.conjunction_2);
					c.add(first);
					c.add(rest);
					return c;
				}
			}
		}
		tokens.reset(mark);
		return null;
	}
	
	/* -------------------------------------------------------------
	 * TOKENISER
	 * ------------------------------------------------------------- */
	
	private __tokenlist tokenize(String s) throws Error{
		__tokenlist tokens = new __tokenlist();
    	if (s == null || s.length() == 0) return tokens;
		String[] toke = { "", s + " " };
		while(toke != null) try{
			toke = nextToken(toke[1]);
			if (toke != null) tokens.add(toke[0]);
		}catch(Exception e){
			//e.printStackTrace();
			throw Error.syntax("Unable to tokenise: " + s);
		}
		return tokens;
	}
	
	private int strip_whitespace(int x, String s){
		boolean whitespace = false;
    	for (; x<s.length(); x++){
    		char c = s.charAt(x);
    		if (c <= 32) whitespace = true;
    		else{
    			if (c == '(' && whitespace){
    				return x-1;
    			}else{
    				break;
    			}
    		}
    	}
    	return x;
	}
	
	private String[] nextToken(String s){
    	if (s == null || s.equals("")) return null;
    	if (s.startsWith(". ")) return new String[]{ ".", "" };
    	int x = 0; // character index
    	x = strip_whitespace(x, s);
    	if (x >= s.length()) return null;
    	if (s.substring(x,x+2).equals(" (")) return new String[]{ " (", s.substring(x+2) };
    	if (s.substring(x,x+2).equals("[]")) return new String[]{ "[]", s.substring(x+2) };
    	if (s.substring(x,x+2).equals("{}")) return new String[]{ "{}", s.substring(x+2) };
    	if (s.substring(x,x+2).equals("()")){
    		x += 2;
        	x = strip_whitespace(x, s);
    	}
    	StringBuffer buf = new StringBuffer();
    	char c = s.charAt(x++);
    	buf.append(c);
		if (c == 34 || c == 39){ // quotes (34:double, 39:single)
	    	boolean done = false;
	    	String internal = new String(new char[]{c,c});
	    	while (!done && x<s.length()){
	    		String dq = null;
	    		if (x+2<s.length()){
	    			dq = s.substring(x,x+2);
	    		}
	    		if (dq != null && dq.equals(internal)){
		    		buf.append(c);
		    		buf.append(c);
		    		x += 2;
	    		}else{
	    			char d = s.charAt(x++);
	    			buf.append(d);
		   			done = d == c;
	    		}
    		}
	    	if (!done){
	    		throw new IllegalArgumentException("Unclosed quoted character sequence");
	    	}
		}else if (_m_parser.isDelimiter(c)){
			return new String[]{ new String(new char[]{c}), s.substring(x) };
		}else if (_m_parser.isDecimalDigit(c) && x<s.length()){
			if (c == '0'){
				char d = s.charAt(x);
				if ("oxb'".indexOf(d) >= 0){
					x++;
					buf.append(d);
					if (d == 'o') x = strip(x, s, buf, Restriction.OCTAL);
					else if (d == 'x') x = strip(x, s, buf, Restriction.HEX);
					else if (d == 'b') x = strip(x, s, buf, Restriction.BINARY);
					else if (d == '\'' && x<s.length()) buf.append(s.charAt(x++));
				}
			}else{
				x = strip(x, s, buf, Restriction.DECIMAL);
		   	}
			// floating point
			if (x<s.length()-1){
				char d = s.charAt(x);
				if (d == '.' && s.charAt(x+1) != ' '){
					x++;
					buf.append(d);
					x = strip(x, s, buf, Restriction.DECIMAL);
				}
			}
			// exponential
			if (x<s.length()-2){
				char d = s.charAt(x);
				if (d == 'E'){
					x++;
					buf.append(d);
					d = s.charAt(x);
					if (d == '+' || d == '-'){
						x++;
						buf.append(d);
					}
					x = strip(x, s, buf, Restriction.DECIMAL);
				}
			}
		}else if (_m_parser.isGraphic(c)){
			x = strip(x, s, buf, Restriction.GRAPHIC);
		}else if (_m_parser.isAlphaNumeric(c)){
			x = strip(x, s, buf, Restriction.ALPHANUM);
		}
		return new String[]{ buf.toString(), s.substring(x) };
	}
	
	private enum Restriction{ DECIMAL, OCTAL, HEX, BINARY, GRAPHIC, ALPHANUM }
	
	private int strip(int x, String s, StringBuffer buf, Restriction type){
		boolean done = false;
		while (!done && x<s.length()){
			char n = s.charAt(x);
			switch(type){
				case DECIMAL: done = !_m_parser.isDecimalDigit(n); break;
				case OCTAL: done = !_m_parser.isOctalDigit(n); break;
				case HEX: done = !_m_parser.isHexadecimalDigit(n); break;
				case BINARY: done = !_m_parser.isBinaryDigit(n); break;
				case GRAPHIC: done = !_m_parser.isGraphic(n); break;
				case ALPHANUM: done = !_m_parser.isAlphaNumeric(n); break;
			}
			if (!done){
				x++;
				buf.append(n);
			}
		}
		return x;
	}
	
	/* -------------------------------------------------------------
	 * WRITE_TERM
	 * ------------------------------------------------------------- */
	
	/*
	 * op_handler can be null !! --> uniform syntax
	 */
	String write(Term t, boolean quoted, boolean numbervars) throws Error{
		if (t.isType(PI.var_0)){
			return t.uniformSyntax(true, false, null);
		}else{
			Clause c = t.asClause();
			if (WRITE_LIST_SYNTAX && (c.is(PI.emptylist_0) || c.is(PI.list_2))){
				List<Term> elements = Term.fromPrologList(c);
				String s = "[";
				int len = elements.size();
				for (int i=0; i<len; i++){
					s += write(elements.get(i), quoted, numbervars);
					if (i < len-1){
						if (i == len-2 && elements.get(i+1).isType(PI.var_0)) s += "|";
						else s += ",";
					}
				}
				s += "]";
				return s;
			}/*else if (c.is(PI.dollar_var_1) && numbervars){
				return numbervar(c.arg(0).asClause());
			}*/else{
				String name = quoted? c.quoted: c.name;
				int arity = c.arity();
				if (arity == 1){
					_op op = mw.getOperator(name, _op.Type.PREFIX);
					if (op != null){
						return name + write(c.arg(0), quoted, numbervars);
					}
					op = mw.getOperator(name, _op.Type.POSTFIX);
					if (op != null){
						return write(c.arg(0), quoted, numbervars) + name;
					}
				}else if (arity == 2){
					_op op = mw.getOperator(name, _op.Type.INFIX);
					if (op != null){
						return write(c.arg(0), quoted, numbervars) 
							+ name
							+ write(c.arg(1), quoted, numbervars);
					}
				}
				return t.uniformSyntax(quoted, numbervars, this);
			}
		}
	}
	
	/* -------------------------------------------------------------
	 * SYNTAX
	 * ------------------------------------------------------------- */
	
	static boolean isAtomic(String s){
		return isAtom(s) | isInteger(s) | isFloatingPoint(s);
	}
	
	private static boolean isAtom(String s){
		return isUnquotedAtom(s) || isQuotedAtom(s);
	}
	
	static boolean isUnquotedAtom(String s){
		return isStandardAtom(s) || isGraphicAtom(s) || isSpecialAtom(s);
	}
	
	private static boolean isStandardAtom(String s){
		return s.length() > 0 && (isLowercase(s.charAt(0)) && isAlphaNumeric(s));
	}
	
	private static boolean isGraphicAtom(String s){
		for (int i=0; i<s.length(); i++){
			if (!isGraphic(s.charAt(i))) return false;
		}
		return s.length() > 0;
	}
	
	static boolean isQuotedAtom(String s){
		return s.length() >= 2 && s.startsWith("'") && s.endsWith("'");
	}
	
	private static boolean isSpecialAtom(String s){
		return s.equals("[]") || s.equals("{}") || s.equals("!") || s.equals("()");
	}
	
	static boolean isVar(String s){
		if (s.length() > 0){
			char c = s.charAt(0);
			return (c == '_' || isUppercase(c)) && isAlphaNumeric(s);
		}
		return false;
	}
	
	private static boolean isInteger(String s){
		try{
			convertToInteger(s);
			return true;
		}catch(NumberFormatException e){
			return false;
		}
	}
	
	private static boolean isFloatingPoint(String s){
		try{
			convertToFloat(s);
			return true;
		}catch(NumberFormatException e){
			return false;
		}
	}
	
	// SINGLE CHARACTER TESTS
	
	private static boolean isAlphaNumeric(char c){
		return c=='_' || isUppercase(c) || isLowercase(c) || isDecimalDigit(c);
	}
	
	private static boolean isGraphic(char c){
		return "#$&*+-./:<=>?@^~\\".indexOf(c) >= 0;
	}
	
	private static boolean isDelimiter(char c){
		return "()[]|,{}".indexOf(c) >= 0;
	}
	
	private static boolean isLowercase(char c){
		return "abcdefghijklmnopqrstuvwxyz".indexOf(c) >= 0;
	}
	
	private static boolean isUppercase(char c){
		return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(c) >= 0;
	}
		
	private static boolean isDecimalDigit(char c){
		return "0123456789".indexOf(c) >= 0;
	}
	
	private static boolean isBinaryDigit(char c){
		return "01".indexOf(c) >= 0;
	}
	
	private static boolean isOctalDigit(char c){
		return "01234567".indexOf(c) >= 0;
	}
	
	private static boolean isHexadecimalDigit(char c){
		return "0123456789ABCDEF".indexOf(c) >= 0;
	}
	
	// PACKAGE RESTRICTED
	
	static int convertToInteger(String s){
		boolean n = s.startsWith("-");
		int i = 0;
		if (s.startsWith(n? "-0o": "0o")){
			i = Integer.parseInt(s.substring(n? 3:2), 8);
		}else if (s.startsWith(n? "-0x": "0x")){
			i = Integer.parseInt(s.substring(n? 3:2), 16);
		}else if (s.startsWith(n? "-0b": "0b")){
			i = Integer.parseInt(s.substring(n? 3:2), 2);
		}else if (s.startsWith(n? "-0'": "0'")){
			i = new Integer(0 + s.charAt(n? 3:2));
		}else{
			i = Integer.parseInt(s.substring(n? 1:0));
		}
		return i * (n? -1:1);
	}
	
	static float convertToFloat(String s) throws NumberFormatException{
		return new Float(s);
	}
	
	private static boolean isAlphaNumeric(String s){
		for (int i=0; i<s.length(); i++){
			if (!isAlphaNumeric(s.charAt(i))) return false;
		}
		return true;
	}
	
	/* -------------------------------------------------------------
	 * TOKEN-LIST-CLASS
	 * ------------------------------------------------------------- */
	
	private final class __tokenlist {
		
		private List<String> list = Collections.synchronizedList(new ArrayList<String>());
		private int index = 0;
		
		void add(String token){
			list.add(token);
		}
		
		boolean finished(){
			return index == list.size()-1;
		}
		
		boolean more(){
			return index < list.size();
		}
		
		String last(){
			if (!list.isEmpty()) return list.get(list.size()-1);
			return null;
		}
		/*
		void replaceLast(String s){
			if (!list.isEmpty()) list.set(list.size()-1, s);
		}
		*/
	    
	    String peek() throws Error{ 
	    	if (index >= list.size()){
	    		throw Error.syntax("Unexpected end of tokens");
	    	}
	    	return list.get(index); 
	    }
	    
	    String next() throws Error{ 
	    	if (index >= list.size()){
	    		throw Error.syntax("Unexpected end of tokens");
	    	}
	    	return list.get(index++);
	    }
	    
	    int index(){
	    	return index;
	    }
	    
	    void reset(int mark){
	    	index = mark;
	    }
	    
	    public String toString(){
	    	String s = "";
	    	for (int i=0; i<list.size(); i++){
	    		s += "\"" + list.get(i).toString() + "\"";
	    		if (i < list.size()-1) s += ", ";
	    	}
	    	return s;
	    }
		
	}
	
}
