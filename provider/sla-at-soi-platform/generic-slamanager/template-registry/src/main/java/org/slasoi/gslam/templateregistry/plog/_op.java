/* 
SVN FILE: $Id: _op.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/_op.java $

*/

package org.slasoi.gslam.templateregistry.plog;

class _op {
	
	enum Type{ PREFIX, INFIX, POSTFIX }

	public static final int MAX_PRIORITY = 1200;
	
	Clause cop = null;
	int pre_priority, priority, post_priority;
	boolean prex, postx;
	String name;
	String quoted;
	
	_op(Clause cop) throws Error{
		this.cop = cop;
		priority = cop.arg(0).asClause().asInteger();
		Clause specifier = cop.arg(1).asClause();
		Clause op = cop.arg(2).asClause();
		name = op.name;
		quoted = op.quoted;
		prex = specifier.is(PI.xf_0) || 
			   specifier.is(PI.xfx_0) || 
			   specifier.is(PI.xfy_0);
		postx = specifier.is(PI.fx_0) || 
				specifier.is(PI.xfx_0) || 
				specifier.is(PI.yfx_0);
		pre_priority = prex? this.priority-1: this.priority; 
		post_priority = postx? this.priority-1: this.priority;
	}
	
	/* -------------------------------------------------------------
	 * ISO-DEFINED OPERATORS
	 * ------------------------------------------------------------- */
	
	static Clause[] builtInOps() throws Error{
		PI op = PI.current_op_3;
		return new Clause[]{
			new Clause(op, 1200, PI.xfx_0, PI.rule_n),
			//new Clause(op, 1200, PI.xfx_0, PI.parse_n),
			new Clause(op, 1200, PI.fx_0, PI.rule_n),
			new Clause(op, 1200, PI.fx_0, PI.query_n),
			new Clause(op, 1100, PI.xfy_0, PI.disjunction_2),
			//new Clause(op, 1050, PI.xfy_0, PI.if_2),
			new Clause(op, 1000, PI.xfy_0, PI.conjunction_2),
			new Clause(op, 900, PI.fy_0, PI.not_provable_1),
			new Clause(op, 700, PI.xfx_0, PI.unify_2), 
			new Clause(op, 700, PI.xfx_0, PI.not_unify_2),
			//new Clause(op, 700, PI.xfx_0, PI.term_identical_2),
			//new Clause(op, 700, PI.xfx_0, PI.term_not_identical_2),
			//new Clause(op, 700, PI.xfx_0, PI.term_gte_2),
			//new Clause(op, 700, PI.xfx_0, PI.term_gt_2),
			//new Clause(op, 700, PI.xfx_0, PI.term_lt_2),
			//new Clause(op, 700, PI.xfx_0, PI.term_lte_2),
			new Clause(op, 700, PI.xfx_0, PI.constructor_2),
			new Clause(op, 700, PI.xfx_0, PI.is_2),
			new Clause(op, 700, PI.xfx_0, PI.equal_2),
			new Clause(op, 700, PI.xfx_0, PI.not_equal_2),
			new Clause(op, 700, PI.xfx_0, PI.gte_2),
			new Clause(op, 700, PI.xfx_0, PI.gt_2),
			new Clause(op, 700, PI.xfx_0, PI.lt_2),
			new Clause(op, 700, PI.xfx_0, PI.lte_2),
			new Clause(op, 500, PI.yfx_0, PI.add_2),
			new Clause(op, 500, PI.yfx_0, PI.subtract_2),
			//new Clause(op, 500, PI.yfx_0, PI.bitwise_and_2),
			//new Clause(op, 500, PI.yfx_0, PI.bitwise_or_2),
			new Clause(op, 400, PI.yfx_0, PI.multiply_2),
			new Clause(op, 400, PI.yfx_0, PI.divide_2),
			new Clause(op, 400, PI.yfx_0, PI.int_divide_2),
			//new Clause(op, 400, PI.yfx_0, PI.rem_2),
			new Clause(op, 400, PI.yfx_0, PI.mod_2),
			//new Clause(op, 400, PI.yfx_0, PI.bitwise_left_shift_2),
			//new Clause(op, 400, PI.yfx_0, PI.bitwise_right_shift_2),
			//new Clause(op, 200, PI.xfx_0, PI.power_2),
			//new Clause(op, 200, PI.xfy_0, PI.hat_2),
			new Clause(op, 200, PI.fy_0, PI.negate_1),
			//new Clause(op, 200, PI.fy_0, PI.bitwise_complement_1),
			// non-ISO defined
			new Clause(op, 100, PI.xfy_0, PI.colon_2),
			//new Clause(op, 101, PI.xfy_0, PI.rdf_language_2), // @
			//new Clause(op, 102, PI.xfy_0, PI.rdf_datatype_2), // ^^
			new Clause(op, 700, PI.xfx_0, PI.stnd_equal_2), // ==
			new Clause(op, 700, PI.xfx_0, PI.stnd_not_equal_2), // !=
		};
	}

}
