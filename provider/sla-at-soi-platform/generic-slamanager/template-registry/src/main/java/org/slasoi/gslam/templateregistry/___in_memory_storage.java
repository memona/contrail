/* 
SVN FILE: $Id: ___in_memory_storage.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/___in_memory_storage.java $

*/

package org.slasoi.gslam.templateregistry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.SLATemplate;

import org.slasoi.gslam.templateregistry.plog.Clause;

class ___in_memory_storage implements ___storage{

	private Map<UUID, String> templateUuidToModuleIdMap 
		= Collections.synchronizedMap(new HashMap<UUID, String>());
	private Map<UUID, SLATemplate> storedTemplates 
		= Collections.synchronizedMap(new HashMap<UUID, SLATemplate>());
	private Map<String, Specification> storedISpecs 
		= Collections.synchronizedMap(new HashMap<String, Specification>());
	private Map<String,Map<String,Clause[]>> storedPredicates
		= Collections.synchronizedMap(new HashMap<String,Map<String,Clause[]>>());
	
	___in_memory_storage(){
		
	}
    
    // Connections ...
	
	public void p_CHECK_CONNECTION(){}

	public void p_DISCONNECT() throws Exception{}
	
	// Predicates ...

    public Clause[] p_GET_PREDICATES(String moduleId, String predicateSig) throws Exception{
    	Map<String,Clause[]> map = storedPredicates.get(moduleId);
    	if (map != null){
    		Clause[] preds = map.get(predicateSig);
    		if (preds != null) return preds;
    	}
    	return new Clause[0];
    }

    public void p_ADD_PREDICATE(Clause predicate, String moduleId, String predicateSig, boolean append) throws Exception{
    	Map<String,Clause[]> map = storedPredicates.get(moduleId);
    	if (map == null){
    		map = new HashMap<String,Clause[]>();
    		storedPredicates.put(moduleId, map);
    	}
    	Clause[] preds = map.get(predicateSig);
    	if (preds == null) preds = new Clause[0];
        List<Clause> list = new ArrayList<Clause>();
        if (!append) list.add(predicate);
        for (Clause c : preds) list.add(c);
        if (append) list.add(predicate);
        map.put(predicateSig,list.toArray(new Clause[0]));
    }
    
    public boolean p_REMOVE_PREDICATE(Clause clause, String moduleId, String predicateSig) throws Exception{
    	Map<String,Clause[]> map = storedPredicates.get(moduleId);
    	if (map != null){
        	Clause[] preds = map.get(predicateSig);
        	if (preds != null && preds.length > 0){
                List<Clause> list = new ArrayList<Clause>();
                for (Clause c : preds){
                	if (!c.equals(clause)) list.add(c);
                }
                map.put(predicateSig,list.toArray(new Clause[0]));
                return list.size() < preds.length;
        	}
    	}
    	return false;
    }
    
    public String[] p_GET_PREDICATE_SIGS(String moduleId) throws Exception{
    	Map<String,Clause[]> map = storedPredicates.get(moduleId);
    	if (map != null) return map.keySet().toArray(new String[0]);
        return new String[0];
    }
    
    // SLATemplates ...
    
    public String[] p_GET_ALL_SLAT_MODULE_IDS() throws Exception{
        return templateUuidToModuleIdMap.values().toArray(new String[0]);
    }
    
    public UUID[] p_GET_ALL_SLAT_UUIDS() throws Exception{
    	List<UUID> uuids = new ArrayList<UUID>();
    	uuids.addAll(templateUuidToModuleIdMap.keySet());
    	return uuids.toArray(new UUID[0]);
    }
	
	public String p_GET_SLAT_MODULE_ID(UUID templateUuid) throws Exception{
		return templateUuidToModuleIdMap.get(templateUuid);
	}

	public UUID p_GET_SLAT_UUID(String moduleId) throws Exception{
		for (UUID uuid : templateUuidToModuleIdMap.keySet()){
			String s = templateUuidToModuleIdMap.get(uuid);
			if (s.equals(moduleId)) return uuid;
		}
		return null;
	}

    public SLATemplate p_GET_SLAT(UUID templateUuid) throws Exception{
    	return storedTemplates.get(templateUuid);
    }
    
    public void p_ADD_SLAT(SLATemplate slat, String moduleId) throws Exception{
    	UUID uuid = slat.getUuid();
    	storedTemplates.put(uuid, slat);
    	templateUuidToModuleIdMap.put(uuid, moduleId);
    }
    
    // Specifications ...
	
	public boolean p_EXISTS_SPEC(String name) throws Exception{
		return storedISpecs.get(name) != null;
	}
    
    public Specification p_GET_SPEC(String name) throws Exception{
    	return storedISpecs.get(name);
    }
    
    public void p_ADD_SPEC(Specification spec) throws Exception{
    	storedISpecs.put(spec.getName(), spec);
    }

}
