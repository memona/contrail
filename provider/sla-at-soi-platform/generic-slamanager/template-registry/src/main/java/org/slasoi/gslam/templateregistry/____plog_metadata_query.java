/* 
SVN FILE: $Id: ____plog_metadata_query.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/____plog_metadata_query.java $

*/

package org.slasoi.gslam.templateregistry;

import java.util.Calendar;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.slamodel.core.CompoundConstraintExpr;
import org.slasoi.slamodel.core.CompoundDomainExpr;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.BOOL;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.LIST;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.vocab.core;

import org.slasoi.gslam.templateregistry.plog.PI;

class ____plog_metadata_query {
	
	static final String $not = PI.not_1.name();
	static final String $and = PI.and_n.name();
	static final String $or = PI.or_n.name();
    
    static String p_CONSTRAINT_TO_PLOG_STRING(ConstraintExpr expr, String mod, int[] counter) throws SLATemplateRegistry.Exception{
    	if (expr instanceof TypeConstraintExpr){
            return p_TYPE_CONSTRAINT_TO_PLOG_STRING((TypeConstraintExpr)expr, mod, counter);
        }else if (expr instanceof CompoundConstraintExpr){
            return p_COMPOUND_CONSTRAINT_TO_PLOG_STRING((CompoundConstraintExpr)expr, mod, counter);
        }
        return null;
    }
    
    static String p_TYPE_CONSTRAINT_TO_PLOG_STRING(TypeConstraintExpr tce, String mod, int[] counter) throws SLATemplateRegistry.Exception{
        ValueExpr ve = tce.getValue();
        String $value = x_VALUE_EXPR(ve);
        if ($value != null){
            DomainExpr de = tce.getDomain();
            return p_DOMAIN_TO_PLOG_STRING("'"+$value+"'", de, mod, counter);
        }
        throw new SLATemplateRegistry.Exception("UTIL: Invalid Value", tce);
    }
    
    static String p_COMPOUND_CONSTRAINT_TO_PLOG_STRING(CompoundConstraintExpr cce, String mod, int[] counter) throws SLATemplateRegistry.Exception{
        STND op = cce.getLogicalOp();
        ConstraintExpr[] ces = cce.getSubExpressions();
        int _op = op.equals(core.not)? 1: op.equals(core.and)? 2: op.equals(core.or)? 3: 0;
        switch (_op){
        case 1:
            if (ces.length==1){
                StringBuffer buf = new StringBuffer();
                buf.append(mod);
                buf.append(":");
                buf.append($not);
                buf.append("(");
                buf.append(p_CONSTRAINT_TO_PLOG_STRING(ces[0], mod, counter));
                buf.append(")");
                return buf.toString();
            }
            break;
        case 2:
        case 3:
            if (ces.length>=2){
                StringBuffer buf = new StringBuffer();
                buf.append(mod);
                buf.append(":");
                if (_op == 2) buf.append($and);
                else buf.append($or);
                buf.append("(");
                for (int i=0; i<ces.length; i++){
                    if (i>0) buf.append(",");
                	if (_op == 3) buf.append("!,");
                    buf.append(p_CONSTRAINT_TO_PLOG_STRING(ces[i], mod, counter));
                }
                buf.append(")");
                return buf.toString();
            }
            break;
        }
        throw new IllegalStateException("UTIL: Invalid CompoundConstraintExpr");
    }
    
    static String p_DOMAIN_TO_PLOG_STRING(String value, DomainExpr expr, String mod, int[] counter) throws SLATemplateRegistry.Exception{
        if (expr instanceof SimpleDomainExpr){
            return p_SIMPLE_DOMAIN_TO_PLOG_STRING(value, (SimpleDomainExpr)expr, mod, counter);
        }else if (expr instanceof CompoundDomainExpr){
            return p_COMPOUND_DOMAIN_TO_PLOG_STRING(value, (CompoundDomainExpr)expr, mod, counter);
        }
        return null;
    }
    
    // EXPRESSION WITH NO ENCLOSING BRACKETS
    static String p_SIMPLE_DOMAIN_TO_PLOG_STRING(String value, SimpleDomainExpr sde, String mod, int[] counter) throws SLATemplateRegistry.Exception{
        ValueExpr ve = sde.getValue();
        STND op = sde.getComparisonOp();
        StringBuffer buf = new StringBuffer();
        buf.append(mod);
        buf.append(":and(");
        buf.append(value);
        buf.append("(V");
        buf.append(counter[0]);
        buf.append("),");
        if (op.equals(core.$equals)) buf.append(PI.stnd_equal_2.name());
        else if (op.equals(core.$not_equals)) buf.append(PI.stnd_not_equal_2.name());
        else if (op.equals(core.$less_than)) buf.append(PI.stnd_lt_2.name());
        else if (op.equals(core.$less_than_or_equals)) buf.append(PI.stnd_lte_2.name());
        else if (op.equals(core.$greater_than)) buf.append(PI.stnd_gt_2.name());
        else if (op.equals(core.$greater_than_or_equals)) buf.append(PI.stnd_gte_2.name());
        else if (op.equals(core.$matches)) buf.append(PI.reg_expr_match_2.name());
        else throw new IllegalStateException("UTIL: Invalid Logical-Op in DomainExpr (must be one of =,!=,<,<=,>,>= or matches)");
        buf.append("(V");
        buf.append(counter[0]++);
        buf.append(",'");
        buf.append(x_VALUE_EXPR(ve));
        buf.append("'))");
        return buf.toString();
    }
    
    static String p_COMPOUND_DOMAIN_TO_PLOG_STRING(String value, CompoundDomainExpr cde, String mod, int[] counter) throws SLATemplateRegistry.Exception{
        STND op = cde.getLogicalOp();
        DomainExpr[] des = cde.getSubExpressions();
        int _op = op.equals(core.not)? 1: op.equals(core.and)? 2: op.equals(core.or)? 3: 0;
        switch (_op){
        case 1:
            if (des.length==1){
                StringBuffer buf = new StringBuffer();
                buf.append(mod);
                buf.append(":");
                buf.append($not);
                buf.append("(");
                buf.append(p_DOMAIN_TO_PLOG_STRING(value, des[0], mod, counter));
                buf.append(")");
                return buf.toString();
            }
            break;
        case 2:
        case 3:
            if (des.length>=2){
                StringBuffer buf = new StringBuffer();
                buf.append(mod);
                buf.append(":");
                if (_op == 2) buf.append($and);
                else buf.append($or);
                buf.append("(");
                for (int i=0; i<des.length; i++){
                	if (_op == 3) buf.append("!,");
                    if (i>0) buf.append(",");
                    buf.append(p_DOMAIN_TO_PLOG_STRING(value, des[i], mod, counter));
                }
                buf.append(")");
                return buf.toString();
            }
            break;
        }
        throw new IllegalStateException("UTIL: Invalid CompoundDomainExpr");
    }
    
    static String x_VALUE_EXPR(ValueExpr ve){
    	if (ve instanceof ID){
            ID value = (ID)ve;
            return value.getValue(); 
        }else if (ve instanceof BOOL){
        	return ((BOOL)ve).toString();
        }else if (ve instanceof CONST){
        	// TODO
        }else if (ve instanceof LIST){
        	// TODO
        }else if (ve instanceof TIME){
        	Calendar cal = ((TIME)ve).getValue();
        	return ____plog_common.x_TIME(cal);
        }
        return null;
    }

}
