/* 
SVN FILE: $Id: _goal.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/_goal.java $

*/

package org.slasoi.gslam.templateregistry.plog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


final class _goal {
	
	private Term context = new Clause(Engine.USER_MODULE_NAME);
	private Term term = null; 
	int bk = 0; //backtrack to ...
	int cp = 0; //continuation point
	boolean cap = false; //true if 1st goal in sequence
	private boolean finished = false;
	private Clause.Iterator candidates = null;
	private List<Integer> bkpts = Collections.synchronizedList(new ArrayList<Integer>()); //stack of backtrack points
	private List<Var> bound = Collections.synchronizedList(new ArrayList<Var>());
	
	_goal(_m_wrapper context, Engine eng, Term term) throws Error{
		if (context != null) this.context = new Clause(context.module.name());
		if (term.is(PI.colon_2)){
			Clause c = term.asClause();
			this.context = c.arg(0);
			term = c.arg(1);
		}
		this.term = term instanceof Var? ((Var)term).binding(): term;
	}
	
	_m_wrapper context(Engine eng) throws Error{
		Clause mname = context.asClause();
		if (!mname.isType(PI.atom_0)){
			throw Error.type(PI.atom_0, mname);
		}
		return eng.__GET_MODULE(mname.quoted);
	}
	
	boolean transparentToCuts(){
		return term.is(PI.conjunction_2) ||
			term.is(PI.disjunction_2) ||
			term.is(PI.if_2);
	}
	
	Term term(){
		return term;
	}
	
	boolean is(PI iso){
		return term.is(iso);
	}
	
	Clause callable() throws Error{
		return term.asCallable();
	}
	
	void pushbk(int b){
		bkpts.add(bk);
		bk = b;
	}
	
	void popbk(){ 
		if (bkpts.size()>0) bk = bkpts.remove(bkpts.size()-1);
	}

	void bind(Var v, Term value){
		//if (v.isAnonymous()) return;
		if (!v.bindto(value)) throw new IllegalStateException("goal -> can't bind var"); 
		bound.add(v);
	}
	
	Clause next() throws Error{
		if (finished) return null;
		purge();
		try{
			Clause c = (candidates != null)? candidates.next(): null;
			if (c == null) finished = true;
			return c;
		}catch(Error pe){
			finished = true;
			throw pe;
		}
	}
	
	boolean isAllocated(){
		return candidates != null;
	}
	
	void allocate(Clause.Iterator candidates){
		purge();
		finished = false;
		this.candidates = candidates;
	}
	
	void deallocate(){
		purge();
		candidates = null;
		finished = false;
	}
	
	void purge(){
		while (bound != null && !bound.isEmpty()){
			bound.remove(0).unbind();
		}
	}
	
	public String toString(){
		return term.toString();
	}
	
}

