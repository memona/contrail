/* 
SVN FILE: $Id: __ref_resolver.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/__ref_resolver.java $

*/

package org.slasoi.gslam.templateregistry;

import java.io.IOException;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface.Specification;

class __ref_resolver implements SLATemplateRegistry.ReferenceResolver{
    
    private SLAManagerContext context = null;
    
    __ref_resolver(SLAManagerContext context) throws Exception{
        this.context = context;
    }

    public Specification getInterfaceSpecification(UUID location) {
        if (context != null) try{
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(location.getValue());
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String wsdlString = EntityUtils.toString(entity);
                Map<ISyntaxConverter.SyntaxConverterType, ISyntaxConverter> scs = context.getSyntaxConverters();
                ISyntaxConverter syntaxConverter = scs.get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter);
                return syntaxConverter.parseWSDL(wsdlString)[0];
            }
        }catch (ClientProtocolException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
