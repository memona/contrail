/* 
SVN FILE: $Id: ____plog_slat_parser.java 1944 2011-05-31 14:36:04Z kevenkt $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: kevenkt $
@version        $Rev: 1944 $
@lastrevision   $Date: 2011-05-31 16:36:04 +0200 (tor, 31 maj 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/____plog_slat_parser.java $

*/

package org.slasoi.gslam.templateregistry;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.service.ResourceType;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.Party.Operative;
import org.slasoi.slamodel.sla.tools.Validator.Report;

import org.slasoi.gslam.templateregistry.plog.Clause;
import org.slasoi.gslam.templateregistry.plog.Engine;
import org.slasoi.gslam.templateregistry.plog.Error;

class ____plog_slat_parser {
	
	static String[] _code = new String[]{
		
	};
	
	static List<Clause> p_PARSE(Engine engine, SLATemplate slat, Metadata metadata, Report rep) throws Error{
		List<Clause> clauses = new ArrayList<Clause>();
        
        // TOP-LEVEL
        clauses.add(engine.parse(____plog_common.p_TEMPLATE_UUID(slat.getUuid())+"."));
        
        // PARTIES
        Party[] parties = slat.getParties();
        if (parties != null) for (Party party : parties){
            clauses.add(engine.parse(____plog_common.p_PARTY(party)+"."));
            Operative[] operatives = party.getOperatives();
            if (operatives != null) for (Operative operative : operatives){
                clauses.add(engine.parse(____plog_common.p_PARTY_OPERATIVE(party, operative)+"."));
            }
        }
		
		// ISPECS
		for (String id : rep.ispecs.keySet()){
			Map<String,Specification> map = rep.ispecs.get(id);
			if (map != null) for (String s : map.keySet()){
				STND role = rep.ispec_providers.get(s);
				clauses.add(engine.parse(____plog_common.p_IFACE(s, role, false)+"."));
			}
		}
		
		// RESOURCE TYPES
        for (String id : rep.res_types.keySet()){
            Map<String,ResourceType> map = rep.res_types.get(id);
            if (map != null) for (String s : map.keySet()){
                STND role = rep.res_type_providers.get(s);
                clauses.add(engine.parse(____plog_common.p_IFACE(s, role, true)+"."));
            }
        }
        
        // B6 SPECIFIC TEST CASE
        /*
         ONLY ENCODE PRICE INFO 
         contained in Guaranteed_action ...
         */
        Clause tariff_usage = ____plog_common.p_EXTRACT_MONTHLY_TARIFF_USAGE_PRICE(slat, null);
        if (tariff_usage != null){
    		Clause c = new Clause(____plog_common.$_monthly_tariff_usage, tariff_usage);
    		clauses.add(c);
        }
		
		// PROGRAM CODE
		for (String s : _code) clauses.add(engine.parse(s));
		
		// METADATA
        Calendar cal = GregorianCalendar.getInstance();
        String time = ____plog_common.x_TIME(cal);
        metadata.setPropertyValue(Metadata.registered_at_datetime, time);
        List<String> md = new ArrayList<String>();
        ____plog_common.p_PARSE_METADATA(metadata, md);
        for (String s : md) clauses.add(engine.parse(s+"."));
		
        // for (Clause c : clauses) System.out.println("[+] "+c.toString());
        
		return clauses;
	}

}
