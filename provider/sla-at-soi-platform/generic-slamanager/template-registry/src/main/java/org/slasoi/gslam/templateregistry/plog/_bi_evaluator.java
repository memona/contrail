/* 
SVN FILE: $Id: _bi_evaluator.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/_bi_evaluator.java $

*/

package org.slasoi.gslam.templateregistry.plog;

class _bi_evaluator {
	
	static Clause.Iterator is(final Clause c){
		return new Clause.OneShot(c){
			protected Clause once() throws Error{
				Term expr = c.arg(1);
				Clause result = evaluate(expr);
				Clause res = new Clause(PI.is_2);
					res.add(result);
					res.add(expr);
				return res;
			}
		};
	}
	
	static Clause.Iterator compare(final Clause c){
		return new Clause.OneShot(c){
			protected Clause once() throws Error{
				try{
					float a = evaluate(c.arg(0)).asFloat();
					float b = evaluate(c.arg(1)).asFloat();
					if( 
						(c.is(PI.equal_2) && a == b) || (c.is(PI.stnd_equal_2) && a == b) ||
						(c.is(PI.not_equal_2) && a != b) || (c.is(PI.stnd_not_equal_2) && a != b) ||
						(c.is(PI.lt_2) && a < b) || (c.is(PI.stnd_lt_2) && a < b) ||
						(c.is(PI.lte_2) && a <= b) || (c.is(PI.stnd_lte_2) && a <= b) ||
						(c.is(PI.gt_2) && a > b) || (c.is(PI.stnd_gt_2) && a > b) ||
						(c.is(PI.gte_2) && a >= b) || (c.is(PI.stnd_gte_2) && a >= b)
					){
						return c;
					}
				}catch(Error e){
					String a = c.arg(0).asClause().name();
					String b = c.arg(1).asClause().name();
					// System.out.println("testing: "+a+" "+c.name()+" "+b);
					if( 
						(c.is(PI.equal_2) && a.compareTo(b)==0) || (c.is(PI.stnd_equal_2) && a.compareTo(b)==0) ||
						(c.is(PI.not_equal_2) && a.compareTo(b)!=0) || (c.is(PI.stnd_not_equal_2) && a.compareTo(b)!=0) ||
						(c.is(PI.lt_2) && a.compareTo(b)<0) || (c.is(PI.stnd_lt_2) && a.compareTo(b)<0) ||
						(c.is(PI.lte_2) && a.compareTo(b)<=0) || (c.is(PI.stnd_lte_2) && a.compareTo(b)<=0) ||
						(c.is(PI.gt_2) && a.compareTo(b)>0) || (c.is(PI.stnd_gt_2) && a.compareTo(b)>0) ||
						(c.is(PI.gte_2) && a.compareTo(b)>=0) || (c.is(PI.stnd_gte_2) && a.compareTo(b)>=0) ||
						(c.is(PI.reg_expr_match_2) && a.matches(b))
					){
						return c;
					}
				}
				return null;
			}
		};
	}
	
	private enum Func{
		ADD,SUB,MULT,INT_DIV,DIV,MOD,ABS,SIGN,
		FLOAT, FLOAT_INT,FLOAT_FRACT,
		FLOOR,TRUNCATE,ROUND,CEILING,
		SIN,COS,ATAN,EXP,LOG,SQRT,POWER,
		BITR,BITL,BIT_AND,BIT_OR,BIT_C
	}
	
	private static Clause evaluate(Term t) throws Error{
		if (t instanceof Var){
			return evaluate(t.asClause());
		}else{
			Clause c = (Clause)t;
			if (c.isType(PI.number_0)) return c;
			else if (c.is(PI.add_2)) return process2arg(c, Func.ADD);
			else if (c.is(PI.subtract_2)) return process2arg(c, Func.SUB);
			else if (c.is(PI.multiply_2)) return process2arg(c, Func.MULT);
			else if (c.is(PI.int_divide_2)) return process2arg(c, Func.INT_DIV);
			else if (c.is(PI.divide_2)) return process2arg(c, Func.DIV);
			else if (c.is(PI.rem_2) || c.is(PI.mod_2)) return process2arg(c, Func.MOD);
			else if (c.is(PI.abs_1)) return process1arg(c, Func.ABS);
			else if (c.is(PI.sign_1)) return process1arg(c, Func.SIGN);
			else if (c.is(PI.float_1)) return process1arg(c, Func.FLOAT);
			else if (c.is(PI.float_integer_part_1)) return process1arg(c, Func.FLOAT_INT);
			else if (c.is(PI.float_fractional_part_1)) return process1arg(c, Func.FLOAT_FRACT);
			else if (c.is(PI.floor_1)) return process1arg(c, Func.FLOOR);
			else if (c.is(PI.truncate_1)) return process1arg(c, Func.TRUNCATE);
			else if (c.is(PI.round_1)) return process1arg(c, Func.ROUND);
			else if (c.is(PI.ceiling_1)) return process1arg(c, Func.CEILING);
			else if (c.is(PI.sin_1)) return process1arg(c, Func.SIN);
			else if (c.is(PI.cos_1)) return process1arg(c, Func.COS);
			else if (c.is(PI.atan_1)) return process1arg(c, Func.ATAN);
			else if (c.is(PI.exp_1)) return process1arg(c, Func.EXP);
			else if (c.is(PI.log_1)) return process1arg(c, Func.LOG);
			else if (c.is(PI.sqrt_1)) return process1arg(c, Func.SQRT);
			else if (c.is(PI.bitwise_right_shift_2)) return process2arg(c, Func.BITR);
			else if (c.is(PI.bitwise_left_shift_2)) return process2arg(c, Func.BITL);
			else if (c.is(PI.bitwise_and_2)) return process2arg(c, Func.BIT_AND);
			else if (c.is(PI.bitwise_or_2)) return process2arg(c, Func.BIT_OR);
			else if (c.is(PI.bitwise_complement_1)) return process1arg(c, Func.BIT_C);
			else if (c.is(PI.power_2)) return process2arg(c, Func.POWER);
			else throw Error.type(PI.evaluable_0, c.pi().asClause());
		}
	}
	
	private static Clause process1arg(Clause c, Func op) throws Error{
		Clause a = evaluate(c.arg(0));
		if (op == Func.FLOAT || op == Func.FLOAT_INT || op == Func.FLOAT_FRACT || 
			op == Func.FLOOR || op == Func.TRUNCATE || 
			op == Func.ROUND || op == Func.CEILING){
			a = new Clause(a.asFloat());
		}
		if (a.isType(PI.integer_0)){
			return processInt(a.asInteger(), op);
		}else if (a.isType(PI.float_0)){
			if (op == Func.BIT_C){
				throw Error.type(PI.integer_0, a);
			}
			return processFloat(a.asFloat(), op);
		}else{
			throw Error.type(PI.number_0, a);
		}
	}
	
	private static Clause process2arg(Clause c, Func op) throws Error{
		Clause a = evaluate(c.arg(0));
		Clause b = evaluate(c.arg(1));
		if (a.isType(PI.integer_0)){
			if (b.isType(PI.integer_0)){
				return processInt(a.asInteger(), b.asInteger(), op);
			}else if (b.isType(PI.float_0)){
				if (op == Func.INT_DIV || op == Func.MOD ||
					op == Func.BITR || op == Func.BITL || op == Func.BIT_AND || op == Func.BIT_OR){
					throw Error.type(PI.integer_0, b);
				}
				return processFloat(a.asInteger(), b.asFloat(), op);
			}else{
				throw Error.type(PI.number_0, b);
			}
		}else if (a.isType(PI.float_0)){
			if (op == Func.INT_DIV || op == Func.MOD ||
				op == Func.BITR || op == Func.BITL || op == Func.BIT_AND || op == Func.BIT_OR){
				throw Error.type(PI.integer_0, a);
			}
			if (b.isType(PI.integer_0)){
				return processFloat(a.asFloat(), b.asInteger(), op);
			}else if (b.isType(PI.float_0)){
				return processFloat(a.asFloat(), b.asFloat(), op); 
			}else{
				throw Error.type(PI.number_0, b);
			}
		}else{
			throw Error.type(PI.number_0, a);
		}
	}
	
	private static Clause processInt(int a, Func op) throws Error{
		int x = 0;
		boolean ok = true;
		switch(op){
			case ABS: x = Math.abs(a); break;
			case SIGN: x = a<0?-1:a>0?1:0; break;
			case BIT_C: x = ~a; break;
			case SIN: 
			case COS: 
			case ATAN: 
			case EXP: 
			case LOG: 
			case SQRT: return processFloat(a, op);
			default: ok = false;
		}
		if (ok) return new Clause("" + x);
		throw new IllegalStateException("This shouldn't happen :)");
	}
	
	private static Clause processFloat(float a, Func op) throws Error{
		double d = 0;
		boolean ok = true;
		switch(op){
			case ABS: d = Math.abs(a); break;
			case SIGN: d = a<0?-1:a>0?1:0; break;
			case FLOAT: d = a; break; 
			case TRUNCATE:
			case FLOAT_INT:
				boolean neg = a < 0;
				if (neg) a *= -1;
				int x = (int)a;
				if (neg) x *= -1;
				return new Clause("" + x);
			case FLOAT_FRACT:
				neg = a < 0;
				if (neg) a *= -1;
				a = a - (int)a;
				if (neg) a *= -1;
				d = a;
				break;
			case FLOOR: return new Clause("" + (int)Math.floor(a));
			case ROUND: return new Clause("" + (int)Math.round(a));
			case CEILING: return new Clause("" + (int)Math.ceil(a));
			case SIN: d = Math.sin(a); break;
			case COS: d = Math.cos(a); break;
			case ATAN: d = Math.atan(a); break;
			case EXP: d = Math.exp(a); break;
			case LOG: d = Math.log(a); break;
			case SQRT: d = Math.sqrt(a); break;
			default: ok = false;
		}
		if (ok) return toFloat(new Float(d)); 
		throw new IllegalStateException("This shouldn't happen :)");
	} 
	
	private static Clause processInt(int a, int b, Func op) throws Error{
		if (b == 0 && (op == Func.INT_DIV || op == Func.DIV || op == Func.MOD)){
			throw Error.evaluation(PI.zero_divisor_0);
		}
		int x = -1;
		boolean ok = true;
		switch(op){
			case ADD: x = a + b; break;
			case SUB: x = a - b; break;
			case MULT: x = a * b; break;
			case INT_DIV: x = a / b; break;
			case MOD: x = a % b; break;
			case BITR: x = a >> b; break;
			case BITL: x = a << b; break;
			case BIT_AND: x = a & b; break;
			case BIT_OR: x = a | b; break;
			case POWER:
			case DIV: return processFloat(a, b, op);
			default: ok = false;
		}
		if (ok) return new Clause("" + x);
		throw new IllegalStateException("This shouldn't happen :)");
	}
	
	private static Clause processFloat(float a, float b, Func op) throws Error{
		if (b == 0 && op == Func.DIV){
			throw Error.evaluation(PI.zero_divisor_0);
		}
		double d = 0;
		boolean ok = true;
		switch(op){
			case ADD: d = a + b; break;
			case SUB: d = a - b; break;
			case MULT: d = a * b; break;
			case DIV: d = a / b; break;
			case POWER: d = Math.pow(a,b); break;
			default: ok = false;
		}
		if (ok) return toFloat(d); 
		throw new IllegalStateException("This shouldn't happen :)");
	}
	
	private static Clause toFloat(double f) throws Error{
		Float F = new Float(f); 
		if (F.isNaN() || F.isInfinite()){
			throw Error.evaluation(PI.undefined_0);
		}
		return new Clause("" + F);
	}
	
}
