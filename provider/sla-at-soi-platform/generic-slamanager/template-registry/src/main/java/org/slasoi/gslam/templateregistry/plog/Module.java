/* 
SVN FILE: $Id: Module.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/Module.java $

*/

package org.slasoi.gslam.templateregistry.plog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;
import java.util.List;

/**
 * Interface defining a persistent Zen Prolog 'backend' 
 * store for Prolog modules.
 * @author Keven T. Kearney
 */
public interface Module {
	
	public String name();
	
	public String moduleType();
	
	public boolean isPublic(PI pi);
	
	public Clause.Iterator candidates(PI pi);
	
	public void assertAZ(Clause c, boolean append) throws Error;
	
	public PI.Iterator predicates();
	
	public boolean retract(Clause c);
	
	public boolean abolish(PI pi);
	
	public static class Impl implements Module{
		
		private String name = null;
		private String type = null;
		private Map<PI, List<Clause>> clauses = null;
		
		public Impl(String name, String type){
			this.name = name;
			this.type = type;
			clauses = Collections.synchronizedMap(new Hashtable<PI, List<Clause>>());
		}

		public String name() {
			return name;
		}

		public String moduleType() {
			return type;
		}
		
		public Clause.Iterator candidates(final PI pi){
			if (pi != null){
				final List<Clause> list = clauses.get(pi);
				if (list == null || list.size() == 0){
					return null;
				}else{
					return new Clause.Iterator(){
						java.util.Iterator<Clause> it = null;
						public Clause next() throws Error{
							if (it == null) it = list.iterator();
							if (it.hasNext()) return it.next();
							return null;
						}
					};
				} 
			}else{
				return null;
			}
		}
		
		public void assertAZ(Clause c, boolean append) throws Error{
			PI pi = c.pi();
			List<Clause> list = clauses.get(pi);
			if (list == null){
				list = Collections.synchronizedList(new ArrayList<Clause>());
				clauses.put(pi, list);
			}
			if (append) list.add(c);
			else{
				List<Clause> a = Collections.synchronizedList(new ArrayList<Clause>());
				list.clear();
				list.add(c);
				for (int i=0; i<a.size(); i++) list.add(a.get(i));
			}
		}

		public boolean isPublic(PI pi){
			return true;
		}	
		
		public PI.Iterator predicates(){
			return new PI.Iterator(){
				java.util.Iterator<PI> pis = null; 
				public PI next(){
					if (pis == null){
						pis = clauses.keySet().iterator();
					}
					if (pis.hasNext()) return pis.next();
					return null;
				}
			};
		}
		
		public boolean retract(Clause c){
			List<Clause> list = clauses.get(c.pi());
			if (list != null){
				return list.remove(c);
			}
			return false;
		}
		
		public boolean abolish(PI pi){
			return clauses.remove(pi) != null;
		}
	
	}
	/*
	public interface Factory{

		public Module create(String name, String type);

		public void destroy(Module m);

		public Module[] modules();
		
		public static class Impl implements Module.Factory{

			protected List<Module> modules = Collections.synchronizedList(new ArrayList<Module>());
			protected Map<String, List<Clause>> programs = Collections.synchronizedMap(new Hashtable<String, List<Clause>>());
			
			public Module create(String name, String type){
				Module m = new Module.Impl(name, type);
				modules.add(m);
				return m;
			}

			public void destroy(Module m){
				// do nothing
			}

			public Module[] modules(){
				return modules.toArray(new Module[0]);
			}
			
		}
		
	}*/
	
}
