/* 
SVN FILE: $Id: Error.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/main/java/org/slasoi/gslam/templateregistry/plog/Error.java $

*/

package org.slasoi.gslam.templateregistry.plog;

/**
 * Encapsulates all exceptions throw by Zen Prolog
 * (including all ISO defined Prolog Errors).
 * @author Keven T. Kearney
 */
public final class Error extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	private Clause error = null;
	
	Error(Clause error){
		super(error.toString());
		this.error = error;
	}
	
	Error(Clause error, String msg){
		super(msg);
		this.error = error;
	}
	
	static boolean isStandard(Clause error){
		return 	error.is(PI.domain_error_2) ||
				error.is(PI.evaluation_error_1) ||
				error.is(PI.existence_error_2) ||
				error.is(PI.instantiation_error_0) ||
				error.is(PI.permission_error_3) ||
				error.is(PI.representation_error_1) ||
				error.is(PI.resource_error_1) ||
				error.is(PI.syntax_error_1) ||
				error.is(PI.system_error_0) ||
				error.is(PI.type_error_2);
	}
	
	/**
	 * Retrieve the {@link Clause Clause} representing the Prolog Error
	 * which caused this exception.
	 */
	public Clause error(){
		return error;
	}
	
	/**
	 * Create a Prolog domain_error.
	 */
	public static Error domain(PI valid, Term culprit){
		Clause c = new Clause(PI.domain_error_2);
		c.add(new Clause(valid));
		c.add(culprit);
		return new Error(c);
	}
	
	/**
	 * Create a Prolog evaluation_error.
	 */
	public static Error evaluation(PI error){
		Clause c = new Clause(PI.evaluation_error_1);
		c.add(new Clause(error));
		return new Error(c);
	}
	
	/**
	 * Create a Prolog existence_error.
	 */
	public static Error existence(PI type, Term culprit){
		Clause c = new Clause(PI.existence_error_2);
		c.add(new Clause(type));
		c.add(culprit);
		return new Error(c);
	}
	
	/**
	 * Create a Prolog instantiation_error.
	 */
	public static Error instantiation(){
		return new Error(
			new Clause(PI.instantiation_error_0)
		);
	}
	
	/**
	 * Create a Prolog permission_error.
	 */
	public static Error permission(PI op, PI type, Term culprit){
		Clause c = new Clause(PI.permission_error_3);
		c.add(new Clause(op));
		c.add(new Clause(type));
		c.add(culprit);
		return new Error(c);
	}
	
	/**
	 * Create a Prolog representation_error.
	 */
	public static Error representation(PI flag){
		Clause c = new Clause(PI.representation_error_1);
		c.add(new Clause(flag));
		return new Error(c);
	}
	
	/**
	 * Create a Prolog resource_error.
	 */
	public static Error resource(Clause resource){
		Clause c = new Clause(PI.resource_error_1);
		c.add(resource);
		return new Error(c);
	}
	
	/**
	 * Create a Prolog syntax_error.
	 */
	public static Error syntax(String msg){
		Clause c = new Clause(PI.syntax_error_1);
		try{
			c.add(new Clause("'" + msg + "'"));
		}catch(Exception e){
			c.add(new Clause(PI.unknown_0));
		}
		return new Error(c);
	}
	
	/**
	 * Create a Prolog system_error.
	 */
	public static Error system(Exception e){
		// e.printStackTrace();
		return new Error(
			new Clause(PI.system_error_0), 
			e.getMessage()
		);
	}
	
	/**
	 * Create a Prolog type_error.
	 */
	public static Error type(PI valid, Term culprit){
		Clause c = new Clause(PI.type_error_2);
		c.add(new Clause(valid));
		c.add(culprit);
		return new Error(c);
	}
	
}