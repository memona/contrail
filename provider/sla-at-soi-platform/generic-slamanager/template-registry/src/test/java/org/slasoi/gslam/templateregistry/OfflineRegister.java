/* 
SVN FILE: $Id: OfflineRegister.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/test/java/org/slasoi/gslam/templateregistry/OfflineRegister.java $

*/

package org.slasoi.gslam.templateregistry;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl;
import org.slasoi.gslam.templateregistry.SLATemplateRegistryImpl.Mode;
import org.slasoi.slamodel.core.CID;
import org.slasoi.slamodel.core.CompoundConstraintExpr;
import org.slasoi.slamodel.core.CompoundDomainExpr;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.LIST;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.ServiceRef;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.service.Interface.Operation.Property;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.CustomAction;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.Invocation;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.meta;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;
import org.slasoi.slamodel.vocab.xsd;
import org.slasoi.slamodel.vocab.ext.Expression;

import junit.framework.TestCase;

public class OfflineRegister extends TestCase{
	
	static Metadata metadata = null;

	private void x_REGISTER(SLATemplateRegistry reg, SLATemplate slat, String msg, boolean verbose){
    	try{
    		if (metadata == null) metadata = _examples.p_METADATA_1();
    		reg.addSLATemplate(slat, metadata); // note "reg" uses a fake _REF_RESOLVER so can't use static validation !!
    		if (msg != null){
                if (verbose) System.out.println("EXPECTED ---->> "+msg);
                fail("Expected exception: " + msg);
    		}
    	}catch(java.lang.Exception e){
    		if (msg == null) e.printStackTrace();
    		else{
        		if (verbose) System.out.println("<< "+e.getMessage());
    			assertEquals(msg, e.getMessage());
        		//if (verbose) System.out.println("---->> "+msg);
    		}
    	}
	}
	
	public void testBadContent_TOP_LEVEL(){
		try{
			boolean verbose = false;
			
        	SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
        	reg.setReferenceResolver(new _TEST_REF_RESOLVER());
        	
        	// SLATemplateRegistryImpl.x_VALIDATE_SLAT_UUID ..
        	
        	x_REGISTER(reg, null, "REG: No SLATemplate specified", verbose);
        	x_REGISTER(reg, new SLA(), "REG: SLAs can not be stored in the SLATemplateRegistry :)", verbose);
        	
        	SLATemplate slat = new SLATemplate();
        	x_REGISTER(reg, slat, "REG: No template UUID specified", verbose);
        	
        	// missing (not online) "REG: SLATemplate already exists (duplicate template UUID)"
        	
		}catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
	}
	
	public void testBadContent_PARTIES(){
		try{
			boolean verbose = false;

        	SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
        	reg.setReferenceResolver(new _TEST_REF_RESOLVER());
        	SLATemplate slat = new SLATemplate();
        	slat.setUuid(new UUID("template1"));

        	// PARTIES
        	
        	x_REGISTER(reg, slat, "FPV: No agreement parties specifed", verbose);
        	
        	slat.setParties(new Party[]{});
        	x_REGISTER(reg, slat, "FPV: No agreement parties specifed", verbose);
        	
        	Party p1 = new Party(new ID("p1"), sla.provider);
        	slat.setParties(new Party[]{ p1, p1 });
        	x_REGISTER(reg, slat, "ACC: Duplicate ID: p1", verbose);
			
        	p1.setAgreementRole(new STND("xyz"));
        	slat.setParties(new Party[]{ p1 });
        	x_REGISTER(reg, slat, "FPV: Invalid Agreement Role: xyz", verbose);
       	   	p1.setAgreementRole(sla.provider);
			
        	Party c1 = new Party(new ID("p1"), sla.customer);
        	slat.setParties(new Party[]{ c1 });
        	x_REGISTER(reg, slat, "FPV: No provider party specifed", verbose);
        	
        	Party.Operative op = new Party.Operative(new ID("op"));
        	p1.setOperatives(new Party.Operative[]{ op, op });
        	slat.setParties(new Party[]{ p1 });
        	x_REGISTER(reg, slat, "ACC: Duplicate ID: (p1/)op", verbose);
        	
		}catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
	}
	
	public void testBadContent_IFACE_SPECS(){
		try{
			boolean verbose = false;
			
        	SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
        	reg.setReferenceResolver(new _TEST_REF_RESOLVER());
        	SLATemplate slat = new SLATemplate();
        	slat.setUuid(new UUID("template1"));
        	Party p1 = new Party(new ID("p1"), sla.provider);
        	slat.setParties(new Party[]{ p1 });
        	
        	// INTERFACE DECLRS
        	
        	Specification spec = new Specification(_TEST_REF_RESOLVER.$A);
            spec.setExtended(new ID[]{ new ID(_TEST_REF_RESOLVER.$A) });
        	InterfaceDeclr idec = new InterfaceDeclr(new ID("idec1"), sla.provider, spec);
        	slat.setInterfaceDeclrs(new InterfaceDeclr[]{ idec });
        	x_REGISTER(reg, slat, "IFS (0): circular extension in interface 'A'", verbose);
        	
        	spec.setExtended(new ID[]{ new ID(_TEST_REF_RESOLVER.$B_exts_A) });
        	x_REGISTER(reg, slat, "IFS (2): circular extension in interface 'B_exts_A'", verbose);
        	
        	spec.setExtended(new ID[]{ new ID(_TEST_REF_RESOLVER.$B_no_ops) });
        	x_REGISTER(reg, slat, "FPV: InterfaceDeclr 'idec1' has no operations & no endpoints", verbose);
        	
        	// adds an operation with same name as operation in extended list ...
        	spec.setExtended(new ID[]{ new ID(_TEST_REF_RESOLVER.$B_opX) });
        	Operation opX = new Operation(new ID(_TEST_REF_RESOLVER.$OPX));
        	spec.setOperations(new Operation[]{ opX });
        	x_REGISTER(reg, slat, "ACC: Duplicate ID: (idec1/)opX", verbose);
        	
        	spec.setExtended(new ID[]{ new ID("inexistent") });
        	x_REGISTER(reg, slat, "IFS (1): unable to locate interface: 'inexistent'", verbose);
			
        	spec.setOperations(new Operation[]{ });
        	spec.setExtended(new ID[]{});
        	x_REGISTER(reg, slat, "FPV: InterfaceDeclr 'idec1' has no operations & no endpoints", verbose);
        	
        	spec.setOperations(new Operation[]{ opX, opX });
        	x_REGISTER(reg, slat, "ACC: Duplicate ID: (idec1/)opX", verbose);
			
        	spec.setOperations(new Operation[]{ opX });
        	Property q1 = new Property(new ID("q1"));
        	opX.setInputs(new Property[]{ q1, q1 });
        	x_REGISTER(reg, slat, "ACC: Duplicate ID: (idec1/opX/)q1", verbose);
        	opX.setInputs(new Property[]{ });
        	
        	opX.setOutputs(new Property[]{ q1, q1 });
        	x_REGISTER(reg, slat, "ACC: Duplicate ID: (idec1/opX/)q1", verbose);
        	opX.setOutputs(new Property[]{ });
        	
        	opX.setRelated(new Property[]{ q1, q1 });
        	x_REGISTER(reg, slat, "ACC: Duplicate ID: (idec1/opX/)q1", verbose);
        	opX.setRelated(new Property[]{ });
        	
		}catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
	}
	
	public void testBadContent_IFACE_DECLRS(){
		try{
			boolean verbose = false;
			
        	SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
        	reg.setReferenceResolver(new _TEST_REF_RESOLVER());
        	SLATemplate slat = new SLATemplate();
        	slat.setUuid(new UUID("template1"));
        	Party p1 = new Party(new ID("p1"), sla.provider);
        	Party c1 = new Party(new ID("c1"), sla.customer);
        	slat.setParties(new Party[]{ p1, c1 });
        	
        	// INTERFACE DECLRS

        	// reset ...
        	slat.setInterfaceDeclrs(new InterfaceDeclr[]{});
        	x_REGISTER(reg, slat, "FPV: no provider interface declared", verbose);
        	
        	Specification spec = x_ISPEC();
        	InterfaceDeclr idec = new InterfaceDeclr(new ID("idec1"), sla.customer, spec);
        	slat.setInterfaceDeclrs(new InterfaceDeclr[]{ idec });
        	x_REGISTER(reg, slat, "FPV: no provider interface declared", verbose);
        	
        	c1.setId(new ID("c1"));
        	slat.setParties(new Party[]{ p1, c1 });
        	idec.setProviderRef(sla.customer);
        	slat.setInterfaceDeclrs(new InterfaceDeclr[]{ idec });
        	x_REGISTER(reg, slat, "FPV: no provider interface declared", verbose);
        	
        	slat.setParties(new Party[]{ p1 });
        	x_REGISTER(reg, slat, "ACC: Invalid ProviderRef in InterfaceDeclr (no party defined for role: customer)", verbose);
        	
        	idec.setProviderRef(sla.provider);
        	slat.setParties(new Party[]{ p1, new Party(new ID("p2"), sla.provider) });
        	x_REGISTER(reg, slat, "ACC: Ambiguous ProviderRef in InterfaceDeclr (multiple parties defined for role: provider)", verbose);
			
        	slat.setParties(new Party[]{ p1 });
        	idec.setProviderRef(new ID("x"));
        	x_REGISTER(reg, slat, "ACC: invalid party reference: x", verbose);
        	
        	idec.setProviderRef(new ID("x/y/z"));
        	x_REGISTER(reg, slat, "ACC: invalid party reference: x/y/z", verbose);
        	
        	idec.setProviderRef(new ID("x/y"));
        	x_REGISTER(reg, slat, "ACC: invalid party reference: x/y", verbose);
        	
        	idec.setProviderRef(new ID("p1/xxx"));
        	x_REGISTER(reg, slat, "ACC: invalid party reference: p1/xxx", verbose);
        	idec.setProviderRef(sla.provider);
        	
        	idec.setProviderRef(new ID("p1")); // should be ok !!
        	x_REGISTER(reg, slat, null, verbose);
        	
        	slat.setUuid(new UUID(slat.getUuid().getValue()+ "XXXX"));
        	idec.setProviderRef(sla.provider);
            
            idec.setProviderRef(new ID("idec1/xxx"));
            x_REGISTER(reg, slat, "ACC: invalid party reference: idec1/xxx", verbose);
            idec.setProviderRef(sla.provider);
        	
        	Endpoint ep = new Endpoint(new ID("ep1"), new STND("xyz"));
        	idec.setEndpoints(new Endpoint[]{ ep, ep });
        	x_REGISTER(reg, slat, "FPV: Invalid Endpoint Protocol: xyz", verbose);
        	
        	ep.setProtocol(sla.SOAP);
        	x_REGISTER(reg, slat, "ACC: Duplicate ID: (idec1/)ep1", verbose);
        	
		}catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
	}
	
	public void testBadContent_VAR_DECLRS_AND_EXPRS(){
		try{
			boolean verbose = false;
			
        	SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
        	reg.setReferenceResolver(new _TEST_REF_RESOLVER());
        	SLATemplate slat = x_PARTY_AND_IDECL();
        	
        	// VAR DECLRS
        	
        	VariableDeclr vdec = new VariableDeclr(new ID("p1"), new ID("xyz"));
        	slat.setVariableDeclrs(new VariableDeclr[]{ vdec });
        	x_REGISTER(reg, slat, "ACC: Duplicate ID: p1", verbose);
        	
        	vdec.setVar(new ID("v1"));
        	slat.setVariableDeclrs(new VariableDeclr[]{ vdec, vdec });
        	x_REGISTER(reg, slat, "ACC: Duplicate ID: v1", verbose);
        	
        	// 1. ValueExprs ------------------------------------------------------------
        	
        	// CONST
        	
        	CONST constant = new CONST("xxx", new STND("?"));
        	vdec.setExpr(constant);
        	slat.setVariableDeclrs(new VariableDeclr[]{ vdec });
        	x_REGISTER(reg, slat, "EXPR: unrecognised CONST datatype: \"xxx\" ?", verbose);
        	
        	constant.setDatatype(units.Byte);
        	x_REGISTER(reg, slat, "EXPR: can't parse numeric CONST: \"xxx\" Byte", verbose);
        	
        	// LIST
        	LIST list = new LIST();
        	vdec.setExpr(list);
        	x_REGISTER(reg, slat, "EXPR: list is empty: []", verbose);
        	
        	list.add(new CONST("10", units.b_per_s));
        	list.add(new CONST("1", units.mm));
        	x_REGISTER(reg, slat, "EXPR: list contains conflicting types: [\"10\" b_per_s, \"1\" mm]", verbose);
        	
        	// ID
        	// create: "v1 is 10 b_per_s"
        	ID var_id = new ID("v2");
        	VariableDeclr vdec1 = new VariableDeclr(var_id, new CONST("10", units.b_per_s));
        	// create list: "[ v2, 1 mm ]"
        	list.set(0, var_id);
        	slat.setVariableDeclrs(new VariableDeclr[]{ vdec1, vdec }); // vdec still contains list !
        	// should give same conflicting types message - i.e. resolves ID in list to CONST
        	x_REGISTER(reg, slat, "EXPR: list contains conflicting types: [v2, \"1\" mm]", verbose);
        	
        	// STND
        	
        	// ServiceRef
        	ServiceRef sref = new ServiceRef();
        	sref.setInterfaceDeclrIds(new ID[]{ var_id }); // var_id denotes a CONST
        	vdec.setExpr(sref);
        	x_REGISTER(reg, slat, "EXPR: invalid interface ID 'v2' in ServiceRef:service_ref{ v2 }", verbose);

        	sref.setInterfaceDeclrIds(new ID[]{ });
        	sref.setOperationIds(new ID[]{ var_id }); // var_id denotes a CONST
        	x_REGISTER(reg, slat, "EXPR: invalid operation ID 'v2' in ServiceRef:service_ref{ v2 }", verbose);

        	sref.setOperationIds(new ID[]{ });
        	sref.setEndpointIds(new ID[]{ var_id }); // var_id denotes a CONST
        	x_REGISTER(reg, slat, "EXPR: invalid endpoint ID 'v2' in ServiceRef:service_ref{ v2 }", verbose);
        	
        	sref.setInterfaceDeclrIds(new ID[]{ new ID("idec1") }); // gets tested first, so if it's ok, we should get the same endpoint ref error
        	x_REGISTER(reg, slat, "EXPR: invalid endpoint ID 'v2' in ServiceRef:service_ref{ idec1 v2 }", verbose);
        	
        	// FunctionalExpr
        	FunctionalExpr fe = new FunctionalExpr(new STND("xyz"), new ValueExpr[]{});
        	vdec = new VariableDeclr(new ID("V"), fe);
        	slat.setVariableDeclrs(new VariableDeclr[]{ vdec });
        	x_REGISTER(reg, slat, "EXPR: unrecognised function: xyz()", verbose);
        	
        	fe.setOperator(core.add);
        	fe.setParameters(new ValueExpr[]{ one_kg }); // wrong number of params
        	x_REGISTER(reg, slat, "EXPR: unrecognised function: http://www.slaatsoi.org/coremodel#add(WEIGHT)", verbose);

        	fe.setParameters(new ValueExpr[]{ abc_str }); // wrong datatype, but checks arity first
        	x_REGISTER(reg, slat, "EXPR: unrecognised function: http://www.slaatsoi.org/coremodel#add(TEXT)", verbose);
        	
        	fe.setParameters(new ValueExpr[]{ one_kg, abc_str }); // correct number of params, wrong datatype
        	x_REGISTER(reg, slat, "EXPR: unrecognised function: http://www.slaatsoi.org/coremodel#add(WEIGHT,TEXT)", verbose);
        	
        	fe.setOperator(core.round);
        	fe.setParameters(new ValueExpr[]{ one_kg, one_kg }); // 1st arg should be type COUNT
        	x_REGISTER(reg, slat, "EXPR: unrecognised function: http://www.slaatsoi.org/coremodel#round(WEIGHT,WEIGHT)", verbose);

        	fe.setParameters(new ValueExpr[]{ one_point_five, one_point_five }); // 2nd arg should be type COUNT (not a decimal)
        	x_REGISTER(reg, slat, "EXPR: unrecognised function: http://www.slaatsoi.org/coremodel#round(QUANTITY,QUANTITY)", verbose);
        	
        	// nested function
        	FunctionalExpr fe2 = new FunctionalExpr(core.add, new ValueExpr[]{ one, one_point_five }); // --> NUMBER
        	fe.setParameters(new ValueExpr[]{ one_point_five, fe2 }); // 2nd arg should be type COUNT 
        	x_REGISTER(reg, slat, "EXPR: unrecognised function: http://www.slaatsoi.org/coremodel#round(QUANTITY,NUMBER)", verbose);

            // SLAT attributes ------------------------------------------------------------
            
            // first check that "invalid ID is working"
            vdec.setExpr(new ID("idec1/op1/invalid-id")); // default 'related' property
            x_REGISTER(reg, slat, "EXPR: invalid ID: idec1/op1/invalid-id", verbose);
        	
        	vdec.setExpr(sla.sla_effectiveFrom);
        	VariableDeclr vdec2 = new VariableDeclr(new ID("BAD"), new ID("not-a-property"));
            slat.setVariableDeclrs(new VariableDeclr[]{ vdec, vdec2 });
            x_REGISTER(reg, slat, "EXPR: invalid ID: not-a-property", verbose);
            
            vdec.setExpr(new ID("idec1/non-existent-endpoint-id"));
            x_REGISTER(reg, slat, "EXPR: invalid ID: idec1/non-existent-endpoint-id", verbose);
            
            vdec.setExpr(new ID("idec1/ep1")); // this time the endpoint exists
            vdec2 = new VariableDeclr(new ID("OK"), new ID("idec1/ep1/location")); // so we should also be able to refer to its location
            VariableDeclr vdec3 = new VariableDeclr(new ID("BAD"), new ID("idec1/ep1/XXXX")); // but not its XXXX
            slat.setVariableDeclrs(new VariableDeclr[]{ vdec, vdec2, vdec3 });
            x_REGISTER(reg, slat, "EXPR: invalid ID: idec1/ep1/XXXX", verbose);
            
        	// default related properties ------------------------------------------------------------

            slat.setVariableDeclrs(new VariableDeclr[]{ vdec });
            
            // Provider & Interface set by x_PARTY_AND_IDECL() ..
        	
        	// now repeat for a default 'related' property : should not throw an 'invalid' id exception
        	vdec.setExpr(new ID("idec1/op1/endpoint")); // default 'related' property
        	// the "endpoint" property should be recognised, so ..
        	VariableDeclr this_will_fail = new VariableDeclr(new ID("FAIL"), new CONST("YEAH", new STND("?")));
        	slat.setVariableDeclrs(new VariableDeclr[]{ vdec, this_will_fail });
        	x_REGISTER(reg, slat, "EXPR: unrecognised CONST datatype: \"YEAH\" ?", verbose);
        	
        	// repeat for others
        	vdec.setExpr(new ID("idec1/op1/consumer"));
        	x_REGISTER(reg, slat, "EXPR: unrecognised CONST datatype: \"YEAH\" ?", verbose);
        	vdec.setExpr(new ID("idec1/op1/request_time"));
        	x_REGISTER(reg, slat, "EXPR: unrecognised CONST datatype: \"YEAH\" ?", verbose);
        	vdec.setExpr(new ID("idec1/op1/reply_time"));
        	x_REGISTER(reg, slat, "EXPR: unrecognised CONST datatype: \"YEAH\" ?", verbose);
        	// try the input property 'in1'
        	vdec.setExpr(new ID("idec1/op1/in1"));
        	x_REGISTER(reg, slat, "EXPR: unrecognised CONST datatype: \"YEAH\" ?", verbose);
        	
        	// EventExpr
        	
        	// 2. DomainExprs ------------------------------------------------------------
        	
        	SimpleDomainExpr sde1 = new SimpleDomainExpr(one, new STND("invalid-comp-op"));
        	vdec.setExpr(sde1);
        	x_REGISTER(reg, slat, "EXPR: invalid comparison operator in domain expression 'invalid-comp-op \"1\"'", verbose);
        	
        	sde1.setComparisonOp(core.equals);
        	CompoundDomainExpr cde = new CompoundDomainExpr(new STND("invalid-logical-op"), new DomainExpr[]{ sde1 });
        	vdec.setExpr(cde);
        	x_REGISTER(reg, slat, "EXPR: invalid logical operator in 'invalid-logical-op'", verbose);
        	
        	cde.setLogicalOp(core.and);
        	x_REGISTER(reg, slat, "EXPR: to few arguments to 'and'/'or' operator (min = 2)", verbose);

        	cde.setLogicalOp(core.not);
        	cde.setSubExpressions(new DomainExpr[]{ sde1, sde1 });
        	x_REGISTER(reg, slat, "EXPR: too many arguments to 'not' operator (max = 1)", verbose);
        	
        	cde.setLogicalOp(core.and);
        	SimpleDomainExpr sde2 = new SimpleDomainExpr(one_kg, core.less_than);
        	cde.setSubExpressions(new DomainExpr[]{ sde1, sde2 });
        	x_REGISTER(reg, slat, "EXPR: conflicting Types in compound domain expr: (= \"1\" and < \"1\" kg)", verbose);
        	
        	vdec.setExpr(new DomainExpr(){
        		private static final long serialVersionUID = 1L;
        		public Expression expression(){ return meta.DOMAIN; }
        	});
            x_REGISTER(reg, slat, "EXPR: unknown DomainExpr class: org.slasoi.gslam.templateregistry.OfflineRegister$1", verbose);

            // 3. ConstraintExprs ------------------------------------------------------------
            
            sde1.setValue(one);
            sde1.setComparisonOp(new STND("invalid-comp-op"));
            TypeConstraintExpr tce1 = new TypeConstraintExpr(one_kg, sde1);
            vdec.setExpr(tce1);
            x_REGISTER(reg, slat, "EXPR: invalid comparison operator in domain expression 'invalid-comp-op \"1\"'", verbose);

            sde1.setComparisonOp(core.equals);
            x_REGISTER(reg, slat, "EXPR: conflicting types in expression: \"1\" kg = \"1\"", verbose);
            
            tce1.setValue(one);
            sde2 = new SimpleDomainExpr(one_kg, core.less_than);
            TypeConstraintExpr tce2 = new TypeConstraintExpr(one_kg, sde2);
            CompoundConstraintExpr cce = new CompoundConstraintExpr(new STND("invalid_logical-op"), new ConstraintExpr[]{ tce1, tce2 });
            vdec.setExpr(cce);
            x_REGISTER(reg, slat, "EXPR: invalid logical operator in 'invalid_logical-op'", verbose);
            
            cce.setLogicalOp(core.not);
            x_REGISTER(reg, slat, "EXPR: too many arguments to 'not' operator (max = 1)", verbose);

            cce.setLogicalOp(core.and);
            cce.setSubExpressions(new ConstraintExpr[]{ tce1 });
            x_REGISTER(reg, slat, "EXPR: to few arguments to 'and'/'or' operator (min = 2)", verbose);
        	
		}catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
	}
 	
    public void testBadContent_G_STATES(){
        try{
            boolean verbose = false;
            
            SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
            SLATemplate slat = x_PARTY_AND_IDECL();
            
            SimpleDomainExpr sde = new SimpleDomainExpr(one, core.equals);
            TypeConstraintExpr state = new TypeConstraintExpr(one_kg, sde);
            Guaranteed.State gs1 = new Guaranteed.State(new ID("GS1"), state);
            AgreementTerm at1 = new AgreementTerm(new ID("AT1"), null, null, new Guaranteed[]{ gs1 });
            slat.setAgreementTerms(new AgreementTerm[]{ at1 });
            x_REGISTER(reg, slat, "EXPR: conflicting types in expression: \"1\" kg = \"1\"", verbose);
            
            CID cid = new CID("STATE1");
            VariableDeclr vdec1 = new VariableDeclr(cid, state);
            slat.setVariableDeclrs(new VariableDeclr[]{ vdec1 });
            gs1.setState(cid);
            // should get exaclty same error - i.e. semantically no different ..
            x_REGISTER(reg, slat, "EXPR: conflicting types in expression: \"1\" kg = \"1\"", verbose);
            
            // try to reference SLAT properties from inside the agreement term ...
            
            state.setValue(one);
            VariableDeclr vdec = new VariableDeclr(new ID("VX"), sla.sla_effectiveFrom);
            VariableDeclr vdec2 = new VariableDeclr(new ID("BAD"), new ID("not-a-property"));
            at1.setVariableDeclrs(new VariableDeclr[]{ vdec, vdec2 });
            x_REGISTER(reg, slat, "EXPR: invalid ID: not-a-property", verbose);
            
            vdec.setExpr(new ID("idec1/non-existent-endpoint-id"));
            x_REGISTER(reg, slat, "EXPR: invalid ID: idec1/non-existent-endpoint-id", verbose);
            
            vdec.setExpr(new ID("idec1/ep1")); // this time the endpoint exists
            vdec2 = new VariableDeclr(new ID("OK"), new ID("idec1/ep1/location")); // so we should also be able to refer to its location
            VariableDeclr vdec3 = new VariableDeclr(new ID("BAD"), new ID("idec1/ep1/XXXX")); // but not its XXXX
            at1.setVariableDeclrs(new VariableDeclr[]{ vdec, vdec2, vdec3 });
            x_REGISTER(reg, slat, "EXPR: invalid ID: idec1/ep1/XXXX", verbose);
            
        }catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
    }
    
    public void testBadContent_G_ACTIONS(){
        try{
            boolean verbose = false;
            
            SLATemplateRegistryImpl reg = new SLATemplateRegistryImpl(Mode.OFFLINE_TEST);
            SLATemplate slat = x_PARTY_AND_IDECL();
            
            CustomAction custom_action = new CustomAction(){	
            	private static final long serialVersionUID = 1L;
            };
            custom_action.setDescr("anything what-so-ever");
            EventExpr event = new EventExpr(new STND("invalid-op"), new Expr[]{ new ID("XXX") });
            Guaranteed.Action ga1 = new Guaranteed.Action(
                new ID("GA1"), 
                new ID("invalid-actor"), 
                new STND("invalid-policy"), 
                event, 
                custom_action
            );
            AgreementTerm at1 = new AgreementTerm(new ID("AT1"), null, null, new Guaranteed[]{ ga1 });
            slat.setAgreementTerms(new AgreementTerm[]{ at1 });
            x_REGISTER(reg, slat, "ACC: invalid party reference: invalid-actor", verbose);
            
            ga1.setActorRef(sla.customer);
            x_REGISTER(reg, slat, "ACC: Invalid ActorRef in Guaranteed.Action (no party defined for role: customer)", verbose);

            // this tests the case that the provider ref is an ID (as allowed by the model) instead of a STND (as preferred) ...
            ga1.setActorRef(new ID("http://www.slaatsoi.org/slamodel#provider"));
            x_REGISTER(reg, slat, "FPV: Invalid Acion Policy: invalid-policy", verbose);
            
            ga1.setActorRef(sla.provider);
            x_REGISTER(reg, slat, "FPV: Invalid Acion Policy: invalid-policy", verbose);
            
            ga1.setPolicy(sla.mandatory);
            x_REGISTER(reg, slat, "EXPR: invalid ID: XXX", verbose);
            
            event.setOperator(core.fault); // event op OK but wrng number of params
            x_REGISTER(reg, slat, "EXPR: invalid ID: XXX", verbose);
            
            event.setParameters(new Expr[]{ new ID("XXX"), new ID("YYY") }); // invalid param types
            x_REGISTER(reg, slat, "EXPR: invalid ID: XXX", verbose);
            
            event.setParameters(new Expr[]{ new ID("idec1"), new ID("YYY") });
            x_REGISTER(reg, slat, "EXPR: invalid ID: YYY", verbose);
            
            event.setParameters(new Expr[]{ new ID("idec1"), new ID("idec1/ep1") });
            x_REGISTER(reg, slat, "EXPR: unrecognised event: http://www.slaatsoi.org/coremodel#fault(INTERFACE,ENDPOINT)", verbose);
            
            // fix event ..
            LIST faults = new LIST();
            faults.add(new STND("my-fault"));
            event.setParameters(new Expr[]{ new ID("idec1"), faults });
            
            // change action (defn) to Invocation
            
            ID endpoint_id = new ID("idec1/ep_doesnt_exist");
            ID operation_id = new ID("op_doesnt_exist");
            Invocation invoc = new Invocation(operation_id);
            invoc.setEndpointId(endpoint_id);
            ga1.setPostcondition(invoc);
            x_REGISTER(reg, slat, "EXPR: invalid ID: idec1/ep_doesnt_exist", verbose);
            
            endpoint_id.setValue("idec1/op1"); // = an operation, not an endpoint
            x_REGISTER(reg, slat, "INVOC: invalid enpoint ref: idec1/op1", verbose);
            
            endpoint_id.setValue("idec1/ep1"); // fixed
            x_REGISTER(reg, slat, "EXPR: invalid ID: op_doesnt_exist", verbose);
            
            operation_id.setValue("idec1/ep1"); // doesn't point to an operation
            x_REGISTER(reg, slat, "INVOC: invalid operation ref: idec1/ep1", verbose);
            
            operation_id.setValue("idec1/op1"); // fixed
            invoc.setParameterValue(new ID("xyz"), new ID("abc"));
            x_REGISTER(reg, slat, "EXPR: invalid ID: xyz", verbose);
            
        }catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
    }
	
	private SLATemplate x_PARTY_AND_IDECL(){
    	SLATemplate slat = new SLATemplate();
    	slat.setUuid(new UUID("template1"));
    	Party p1 = new Party(new ID("p1"), sla.provider);
    	slat.setParties(new Party[]{ p1 });
    	Specification spec = x_ISPEC();
    	InterfaceDeclr idec = new InterfaceDeclr(new ID("idec1"), sla.provider, spec);
    	slat.setInterfaceDeclrs(new InterfaceDeclr[]{ idec });
    	Endpoint ep1 = new Endpoint(new ID("ep1"), sla.REST);
    	idec.setEndpoints(new Endpoint[]{ ep1 });
		return slat;
	}
	
	private Interface.Specification x_ISPEC(){
		Interface.Specification spec = new Interface.Specification("iface_spec1");
		Operation op1 = new Operation(new ID("op1"));
		Property in1 = new Property(new ID("in1"));
		op1.setInputs(new Property[]{ in1 });
		spec.setOperations(new Operation[]{ op1 });
		return spec;
	}
    
    static CONST one_kg = new CONST("1", units.kg);
    static CONST one = new CONST("1", null);
    static CONST one_point_five = new CONST("1.5", null);
    static CONST abc_str = new CONST("abc", xsd.string);

}
