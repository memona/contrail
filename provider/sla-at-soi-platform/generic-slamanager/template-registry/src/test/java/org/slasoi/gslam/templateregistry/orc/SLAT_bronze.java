package org.slasoi.gslam.templateregistry.orc;

import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.LIST;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.service.ResourceType;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.resources;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;
import org.slasoi.slamodel.vocab.xsd;

public class SLAT_bronze extends SLATemplate{
    
    public SLAT_bronze(){
        
        // COMMON  ..

        ID VM_Access_Point = new ID("VM_Access_Point");
        CONST ORC_1 = new CONST("http://www.intel.ie/ORC_images/ORC.ovf", xsd.anyURI);
        CONST ORC_2 = new CONST("http://www.intel.ie/ORC_images/ORC2.ovf", xsd.anyURI);
        ID VM_IMAGE_VAR = new ID("VM_IMAGE_VAR");
        
        // THE SLAT ...
        
        this.setUuid(new UUID("ORC_InfrastructureSLATBronze"));
        this.setParties(new Party[]{
            new Party(
                new ID("INTEL.IE"), 
                sla.provider
            )
        });
        this.setInterfaceDeclrs(new InterfaceDeclr[]{
            new InterfaceDeclr(
                VM_Access_Point,
                sla.provider,
                new Endpoint[]{
                    new Endpoint(
                        new ID("Endpoint"),
                        new UUID("http://www.vm1.slaatsoi.eu"),
                        sla.SSH
                    )
                },
                new ResourceType("VirtualMachine")
            )    
        });
        this.setAgreementTerms(new AgreementTerm[]{
            new AgreementTerm(
                new ID("Reliability"),
                null,     
                null,
                new Guaranteed[]{
                    new Guaranteed.State(
                        new ID("MTTRState"),
                        new TypeConstraintExpr(
                            new FunctionalExpr(
                                common.mttr, 
                                new ValueExpr[]{ VM_Access_Point }
                            ), 
                            new SimpleDomainExpr(
                                new CONST("8", units.hrs), 
                                core.less_than
                            )
                        )
                    ),
                    new Guaranteed.State(
                        new ID("MTTFState"),
                        new TypeConstraintExpr(
                            new FunctionalExpr(
                                common.mttf, 
                                new ValueExpr[]{ VM_Access_Point }
                            ), 
                            new SimpleDomainExpr(
                                new CONST("175000", units.hrs), 
                                core.greater_than
                            )
                        )
                    )
                }
            ),
            new AgreementTerm(
                new ID("Performance"),
                null,     
                new VariableDeclr[]{
                    new Customisable(
                        VM_IMAGE_VAR, 
                        new SimpleDomainExpr(
                            new LIST(new ValueExpr[]{ ORC_1, ORC_2 }), 
                            core.member_of
                        ),
                        ORC_1
                    )
                },
                new Guaranteed[]{
                    new Guaranteed.State(
                        new ID("VM_CORES"),
                        new TypeConstraintExpr(
                            new FunctionalExpr(
                                resources.vm_cores, 
                                new ValueExpr[]{ VM_Access_Point }
                            ), 
                            new SimpleDomainExpr(
                                new CONST("1", null), 
                                core.equals
                            )
                        )
                    ),
                    new Guaranteed.State(
                        new ID("VM_CPU_SPEED"),
                        new TypeConstraintExpr(
                            new FunctionalExpr(
                                resources.cpu_speed, 
                                new ValueExpr[]{ VM_Access_Point }
                            ), 
                            new SimpleDomainExpr(
                                new CONST("1", units.GHz), 
                                core.equals
                            )
                        )
                    ),
                    new Guaranteed.State(
                        new ID("VM_MEMORY_SIZE"),
                        new TypeConstraintExpr(
                            new FunctionalExpr(
                                resources.memory, 
                                new ValueExpr[]{ VM_Access_Point }
                            ), 
                            new SimpleDomainExpr(
                                new CONST("1024", units.MB), 
                                core.equals
                            )
                        )
                    ),
                    new Guaranteed.State(
                        new ID("VM_IMAGE"),
                        new TypeConstraintExpr(
                            new FunctionalExpr(
                                resources.vm_image, 
                                new ValueExpr[]{ VM_Access_Point }
                            ), 
                            new SimpleDomainExpr(
                                VM_IMAGE_VAR, 
                                core.equals
                            )
                        )
                    )
                }
            )
        });
    }

}
