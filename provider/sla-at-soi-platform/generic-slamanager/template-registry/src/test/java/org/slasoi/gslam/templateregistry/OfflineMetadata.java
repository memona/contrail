/* 
SVN FILE: $Id: OfflineMetadata.java 304 2010-12-05 13:45:45Z andy-edmonds $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: andy-edmonds $
@version        $Rev: 304 $
@lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/template-registry/src/test/java/org/slasoi/gslam/templateregistry/OfflineMetadata.java $

*/

package org.slasoi.gslam.templateregistry;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.slamodel.core.CompoundConstraintExpr;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.core;

import junit.framework.TestCase;

public class OfflineMetadata extends TestCase{
	
    public void test(){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	SLATemplate slat1 = _examples.p_SLAT_all();
        	UUID template1Id = slat1.getUuid();
        	SLATemplate slat2 = _examples.p_SLAT_simple1();
        	UUID template2Id = slat2.getUuid();
        	SLATemplateRegistryImpl reg = _examples.p_SLATREG(null, false);
        	Metadata metadata1 = _examples.p_METADATA_1();
        	reg.addSLATemplate(slat1, metadata1);
        	Metadata metadata2 = _examples.p_METADATA_2();
        	reg.addSLATemplate(slat2, metadata2);
        	
        	Metadata m = reg.getMetadata(slat1.getUuid());
        	if (!m.equals(metadata1)){
                fail("getMetadata --> doesn't match original metadata");
        	}
        	
        	String new_provider_uuid = "XYZ";
        	reg.setMetadataProperty(template1Id, Metadata.provider_uuid, new_provider_uuid);
        	String v = reg.getMetadataProperty(template1Id, Metadata.provider_uuid);
        	if (!new_provider_uuid.equals(v)){
        		m = reg.getMetadata(slat1.getUuid());
        		x_DUMP(m);
                fail("(1)");
        	}
        	reg.setMetadataProperty(template1Id, Metadata.provider_uuid, _examples.$slam_id);

    		Calendar cal1 =GregorianCalendar.getInstance();
        	String time1 = sdf.format(cal1.getTime());
        	reg.setMetadataProperty(template1Id, Metadata.activated_at_datetime, time1);
        	v = reg.getMetadataProperty(template1Id, Metadata.activated_at_datetime);
        	if (!time1.equals(v)){
        		m = reg.getMetadata(slat1.getUuid());
        		x_DUMP(m);
                fail("(2)");
        	}
        	
        	// QUERIES
        	
        	// ? all slats (metadata = null)
        	UUID[] uuids = reg.query(null);
        	if (!x_TEST_Q_RESULTS(uuids, new UUID[]{_examples.template_all_id,_examples.template_simple1_id})) fail("(all)");
        	
        	// ? slats by provider 1
            SimpleDomainExpr SD1 = new SimpleDomainExpr(new ID(_examples.$slam_id), core.equals);
            TypeConstraintExpr TC1 = new TypeConstraintExpr(Metadata.provider_uuid, SD1);
            uuids = reg.query(TC1);
        	if (!x_TEST_Q_RESULTS(uuids, new UUID[]{_examples.template_all_id})) fail("(p1)");
            
            // ? slats by provider 2
            SimpleDomainExpr SD2 = new SimpleDomainExpr(new ID(_examples.$another_provider_id), core.equals);
            TypeConstraintExpr TC2 = new TypeConstraintExpr(Metadata.provider_uuid, SD2);
            uuids = reg.query(TC2);
        	if (!x_TEST_Q_RESULTS(uuids, new UUID[]{_examples.template_simple1_id})) fail("(p2)");
            
            // ? OR : slats provider by either 1 or 2
            CompoundConstraintExpr CCE1 = new CompoundConstraintExpr(core.or, new ConstraintExpr[]{ TC1, TC2 });
            uuids = reg.query(CCE1);
        	if (!x_TEST_Q_RESULTS(uuids, new UUID[]{_examples.template_all_id,_examples.template_simple1_id})) fail("(either)");
        	
        	// ? AND : provider & registrar
            SimpleDomainExpr SD3 = new SimpleDomainExpr(new ID(_examples.$registrar_id), core.equals);
            TypeConstraintExpr TC3 = new TypeConstraintExpr(Metadata.registrar_id, SD3);
            CCE1 = new CompoundConstraintExpr(core.and, new ConstraintExpr[]{ TC1, TC3 });
            uuids = reg.query(CCE1);
        	if (!x_TEST_Q_RESULTS(uuids, new UUID[]{_examples.template_all_id})) fail("(provider+registrar)");
        	
        	// ? OR : provider & registrar
            CCE1 = new CompoundConstraintExpr(core.or, new ConstraintExpr[]{ TC1, TC3 });
            uuids = reg.query(CCE1);
        	if (!x_TEST_Q_RESULTS(uuids, new UUID[]{_examples.template_all_id,_examples.template_simple1_id})) fail("(provider+registrar)");
        	
        	// ? time >
        	Calendar cal2 =GregorianCalendar.getInstance();
        	cal2.add(Calendar.YEAR, 1);
        	String time2 = sdf.format(cal2.getTime());
        	reg.setMetadataProperty(template2Id, Metadata.activated_at_datetime, time2);
            SimpleDomainExpr SD4 = new SimpleDomainExpr(new TIME(cal1), core.greater_than);
            TypeConstraintExpr TC4 = new TypeConstraintExpr(Metadata.activated_at_datetime, SD4);
            uuids = reg.query(TC4);
        	if (!x_TEST_Q_RESULTS(uuids, new UUID[]{_examples.template_simple1_id})) fail("(time>)");        	

            SD4.setComparisonOp(core.less_than);
            uuids = reg.query(TC4);
        	if (!x_TEST_Q_RESULTS(uuids, new UUID[]{_examples.template_all_id})) fail("(time>)");       
        	
        }catch(Exception e){
            e.printStackTrace();
            fail("Unexpected exception: " + e.getMessage());
        }
    }
    
    private boolean x_TEST_Q_RESULTS(UUID[] uuids, UUID[] expected){
    	if (uuids.length != expected.length) return false;
    	List<UUID> list = new ArrayList<UUID>();
    	for (UUID uuid : uuids) list.add(uuid);
    	for (UUID x : expected){
    		if (list.contains(x)) list.remove(x);
    		else return false;
    	}
    	return true;
    }
    
    private void x_DUMP(Metadata m){
    	STND[] keys = m.getPropertyKeys();
    	for (STND key : keys){
    		String v = m.getPropertyValue(key);
    		System.out.println(key.getValue()+" ==> "+v);
    	}
    }

}
