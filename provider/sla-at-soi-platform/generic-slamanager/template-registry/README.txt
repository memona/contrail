CONFIGURATION OF THE SLA-TEMPLATE-REGISTRY

The SLATemplateRegistry requires JDBC access to a persistent backend store.
Database properties (driver, user, password & url) are specified in "db.properties"