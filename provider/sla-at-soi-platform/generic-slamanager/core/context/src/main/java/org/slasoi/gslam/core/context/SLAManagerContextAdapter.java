/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 1559 $
 * @lastrevision   $Date: 2011-05-03 17:16:57 +0200 (tor, 03 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/core/context/src/main/java/org/slasoi/gslam/core/context/SLAManagerContextAdapter.java $
 */

package org.slasoi.gslam.core.context;

import java.util.Hashtable;

import org.slasoi.gslam.core.authorization.Authorization;
import org.slasoi.gslam.core.monitoring.IMonitoringManager;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ProtocolEngine;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.gslam.core.negotiation.ServiceManagerRegistry;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter.SyntaxConverterType;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.slasoi.sa.pss4slam.core.ServiceAdvertisement;

/**
 * Default implementation of a SLAManagerContext
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class SLAManagerContextAdapter implements SLAManagerContext {
    /**
     * Default constructor
     */
    public SLAManagerContextAdapter() {
    }

    public Authorization getAuthorization() throws SLAManagerContextException {
        return null;
    }

    public Hashtable<String, String> getProperties() throws SLAManagerContextException {
        return null;
    }

    public String getEPR() throws SLAManagerContextException {
        return null;
    }
    
    public String getWSPrefix() throws SLAManagerContextException {
        return null;
    }

    public String getGroupID() throws SLAManagerContextException {
        return null;
    }

    public IMonitoringManager getMonitorManager() throws SLAManagerContextException {
        return null;
    }

    public PlanningOptimization getPlanningOptimization() throws SLAManagerContextException {
        return null;
    }

    public ProtocolEngine getProtocolEngine() throws SLAManagerContextException {
        return null;
    }

    public ProvisioningAdjustment getProvisioningAdjustment() throws SLAManagerContextException {
        return null;
    }

    public String getSLAManagerID() throws SLAManagerContextException {
        return null;
    }

    public SLARegistry getSLARegistry() throws SLAManagerContextException {
        return null;
    }

    public SLATemplateRegistry getSLATemplateRegistry() throws SLAManagerContextException {
        return null;
    }

    public ServiceAdvertisement getServiceAdvertisement() throws SLAManagerContextException {
        return null;
    }

    public ServiceManagerRegistry getServiceManagerRegistry() throws SLAManagerContextException {
        return null;
    }

    public GenericSLAManagerUtils getSlamUtils() {
        return null;
    }

    public Hashtable<SyntaxConverterType, ISyntaxConverter> getSyntaxConverters() throws SLAManagerContextException {
        return null;
    }

    public void setProperties( Hashtable<String, String> props ) throws SLAManagerContextException {
    }

    public void setGroupID(String groupID) {
    }

    public void setPlanningOptimization(PlanningOptimization customizedPOC) {
    }

    public void setProvisioningAdjustment(ProvisioningAdjustment customizedPAC) {
    }

    public void setSlamUtils(GenericSLAManagerUtils slamUtils) {
    }

    public void setAuthorization(Authorization auth) {
    }
}
