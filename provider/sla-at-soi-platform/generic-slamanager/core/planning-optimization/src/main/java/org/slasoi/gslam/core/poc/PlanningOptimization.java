/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Kuan Lu - kuan.lu@uni-dortmund.de Edwin Yaqub - edwin.yaqub@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/core/planning-optimization/src/main/java/org/slasoi/gslam/core/poc/PlanningOptimization.java $
 */

package org.slasoi.gslam.core.poc;

import java.util.List;

import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment.Status;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * This interface defines all the methods and inner-interfaces for planning and optimization in all domain specific
 * scenarios.
 * 
 * @author Miguel Rojas
 * @author Kuan Lu
 * @author Edwin Yaqub
 */
public interface PlanningOptimization {
    /**
     * Gets the instance of inner class of IAssessmentAndCustomize for negotiation.
     */
    public IAssessmentAndCustomize getIAssessmentAndCustomize();

    /**
     * Gets the instance of inner class of INotification for provision.
     */
    public INotification getINotification();

    /**
     * Gets the instance of inner class of IPlanStatus for plan status updating.
     */
    public IPlanStatus getIPlanStatus();

    /**
     * Gets the instance of inner class of IReplan for re-planing.
     */
    public IReplan getIReplan();

    /**
     * This interface defines all the methods and inner-interfaces for planning and optimization in all domain specific
     * scenarios.
     * 
     * @author Miguel Rojas
     * @author Kuan Lu
     * @author Edwin Yaqub
     */
    public interface IAssessmentAndCustomize {
        /**
         * Starts the negotiation.
         * 
         * @param negotiationID
         * @param slaTemplate
         * @return SLATemplate[]
         */
        public SLATemplate[] negotiate(String negotiationID, SLATemplate slaTemplate) throws Exception;

        /**
         * Creates an agreement.
         * 
         * @param negotiationID
         * @param slaTemplate
         * @return org.slasoi.slamodel.sla.SLA
         */
        public SLA createAgreement(String negotiationID, SLATemplate slaTemplate);

        /**
         * Terminates the negotiation.
         * 
         * @param slaID
         * @param terminationReason
         * @return boolean
         */
        public boolean terminate(UUID slaID, List<TerminationReason> terminationReason);

        /**
         * Provision.
         * 
         * @param slaID
         * @return org.slasoi.slamodel.sla.SLA
         */
        public SLA provision(UUID slaID);
    }

    /**
     * This interface defines the method for starting to provision.
     * 
     * @author Miguel Rojas
     * @author Kuan Lu
     * @author Edwin Yaqub
     */
    public interface INotification {
        /**
         * Starts to provision.
         * 
         * @param org
         *            .slasoi.slamodel.sla.SLA
         */
        public void activate(SLA sla);
    }

    /**
     * This interface defines the method for passing the status of a specific plan.
     * 
     * @author Miguel Rojas
     * @author Kuan Lu
     * @author Edwin Yaqub
     */
    public interface IPlanStatus {
        /**
         * Passes the status of a specific plan.
         * 
         * @param planId
         * @param status
         */
        public void planStatus(String planId, Status status);
    }

    /**
     * This interface defines the method for re-plan.
     * 
     * @author Miguel Rojas
     * @author Kuan Lu
     * @author Edwin Yaqub
     */
    public interface IReplan {
        /**
         * Re-plan.
         * 
         * @param planId
         * @param analysis
         */
        public void rePlan(String planId, String analysis);
    }
}
