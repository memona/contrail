/* 
SVN FILE: $Id: SLATemplateRegistry.java 1943 2011-05-31 14:34:42Z kevenkt $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: kevenkt $
@version        $Rev: 1943 $
@lastrevision   $Date: 2011-05-31 16:34:42 +0200 (tor, 31 maj 2011) $
@filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/core/negotiation/src/main/java/org/slasoi/gslam/core/negotiation/SLATemplateRegistry.java $

*/

package org.slasoi.gslam.core.negotiation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.slasoi.slamodel.core.Annotated;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.tools.Validator.Warning;
import org.slasoi.slamodel.vocab.ext.Extensions;

public interface SLATemplateRegistry{
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Logger
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void addListener(Listener listener);
    
	public void removeListener(Listener listener);
    
    public interface Listener{
    	
    	public void message(String msg, Calendar timestamp);
    	
    	public void templateRegistered(UUID uuid, Calendar timestamp);
    	
    	public void registerTemplateFailed(SLATemplate slat, java.lang.Exception e, Calendar timestamp);
    	
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Queries
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * [TO DESCRIBE]    
     * @param metadata_query
     * @return
     * @throws Exception
     */
    public UUID[] query(ConstraintExpr metadata_query) 
    throws Exception;

    /**
     * [TO DESCRIBE]
     * @param slat
     * @param metadata_constraint
     * @return
     * @throws Exception
     */
    public ResultSet query(SLATemplate query, ConstraintExpr metadata_constraint) 
    throws Exception;
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Query Results
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public static class ResultSet{
    	
    	public List<Warning> warnings = Collections.synchronizedList(new ArrayList<Warning>());
    	public List<java.lang.Exception> errors = Collections.synchronizedList(new ArrayList<java.lang.Exception>());
    	public List<Result> results = Collections.synchronizedList(new ArrayList<Result>());
    	
    	public static class Result implements Comparable<Result>{
    		
    		private UUID uuid = null;
    		private float confidence = 0.0f;
    		
    		public Result(UUID uuid, float confidence){
    			if (uuid == null) throw new IllegalArgumentException("No UUID specified");
    			this.uuid = uuid;
    			this.confidence = confidence;
    		}
    		
    		public UUID getUuid(){ return uuid; }
    		
    		public float getConfidence(){ return confidence; }
    		
    		public String toString(){
    			StringBuffer buf = new StringBuffer();
    			buf.append(uuid.getValue());
    			buf.append(" [");
    			buf.append(confidence);
    			buf.append("]");
    			return buf.toString();
    		}

			public int compareTo(Result r){
				return new Float(confidence).compareTo(new Float(r.confidence));
			}
    		
    	}
    	
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ADD / REMOVE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * [TO DESCRIBE]
     * @param templateId
     * @return
     * @throws Exception
     */
    public SLATemplate getSLATemplate(UUID templateId) 
    throws Exception;
    
    
    
    /**
     * [TO DESCRIBE]
     * @return
     * @throws Exception
     */
    public SLATemplate[] getSLATemplates() 
    throws Exception;
    
    
    
    
    /**
     * [TO DESCRIBE]
     * @param interfaceName
     * @return
     * @throws Exception
     */
    public Specification getInterfaceSpecification(String interfaceName)
    throws Exception;
    
    /**
     * [TO DESCRIBE]
     * @param templateId
     * @param key
     * @param value
     * @throws Exception
     */
    public void setMetadataProperty(UUID templateId, STND key, String value) 
    throws Exception;
    
    /**
     * [TO DESCRIBE]
     * @param templateId
     * @param key
     * @return
     * @throws Exception
     */
    public String getMetadataProperty(UUID templateId, STND key) 
    throws Exception;
    
    /**
     * [TO DESCRIBE]
     * @param templateId
     * @return
     * @throws Exception
     */
    public Metadata getMetadata(UUID templateId) 
    throws Exception;
    
    /**
     * [TO DESCRIBE]
     * @param slat
     * @param metadata
     * @return
     * @throws Exception
     */
    public Warning[] addSLATemplate(SLATemplate slat, SLATemplateRegistry.Metadata metadata) 
    throws Exception;
     
    /**
     * [TO DESCRIBE]
     * @param templateId
     * @throws Exception
     */
    public void removeSLATemplate(UUID templateId) 
    throws Exception;
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // VALIDATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Performs a complete validation of SLATemplate content. The template is valid if no exception is thrown.
     * @param slat the SLATemplate to validate
     * @throws Exception if any illegal content is encountered
     */
    public Warning[]  validateSLATemplate(SLATemplate slat) 
    throws Exception;
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // DOMAIN SPECIFIC CUSTOMISATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * [TO DESCRIBE]
     */
    public interface ReferenceResolver{
        
        /**
         * [TO DESCRIBE]
         * @param location
         * @return
         */
        public Interface.Specification getInterfaceSpecification(UUID location);

    }
    
    /**
     * [TO DESCRIBE]
     * @param resolver
     */
    public void setReferenceResolver(ReferenceResolver resolver) throws SLATemplateRegistry.Exception;
    
    /**
     * [TO DESCRIBE]
     * @param extensions
     */
    public void addExtensions(Extensions extensions) throws SLATemplateRegistry.Exception;
    
    /**
     * [TO DESCRIBE]
     * @param extensions
     */
    public void removeExtensions(Extensions extensions) throws SLATemplateRegistry.Exception;
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Metadata
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * <p>This class is an empty concrete extension of the Annotated class (defined by the SLAModel), 
     * and encapsulates a simple dictionary of property key/value pairs.
     */
    public static class Metadata extends Annotated{
        
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		/**
         * [TO DESCRIBE]
         */
        public static final String $base = "http://www.slaatsoi.org/slat/metadata#"; 
        /**
         * String constant for the metadata property key 'provider_uuid'. 
         * value: a UUID identifying the provider of the described SLATemplate.
         */
        public static final String $provider_uuid = $base + "provider_uuid";
        /**
         * String constant for the metadata property key 'template_uuid'. 
         * value: the UUID  of the described SLATemplate as defined within the template itself.
         */
        public static final String $template_uuid = $base + "template_uuid";
        /**
         * String constant for the metadata property key 'creator_id'. 
         * value: a UUID  identifying the designer/creator of the described SLATemplate.
         */
        public static final String $creator_id = $base + "creator_id";
        /**
         * String constant for the metadata property key 'registrar_id'. 
         * value: a UUID  identifying the registrar responsible for registering the described SLATemplate with the SLATemplateRegistry..
         */
        public static final String $registrar_id = $base + "registrar_id";
        /**
         * String constant for the metadata property key 'registered_at_datetime'. 
         * value: a TIME  value giving the date/time at which the SLATemplate was registered in the SLATemplateRegistry..
         */
        public static final String $registered_at_datetime = $base + "registered_at_datetime";
        /**
         * String constant for the metadata property key 'activated_at_datetime'. 
         * value: a TIME value giving the date/time at which the SLATemplate was 'activated', i.e. made available as a 'service offer'.
         */
        public static final String $activated_at_datetime = $base + "activated_at_datetime";
        /**
         * String constant for the metadata property key 'disactived_at_datetime'. 
         * value: a TIME value giving the date/time at which the SLATemplate was 'disactivated', i.e. withdrawn as a 'service offer' (must be later than #activated_at_datetime).
         */
        public static final String $disactived_at_datetime = $base + "disactived_at_datetime";
        /**
         * String constant for the metadata property key 'previous_version'. 
         * value: the UUID  of the old-version of the template.
         */
        public static final String $previous_version = $base + "previous_version";
        /**
         * String constant for the metadata property key 'replaced_by_version'. 
         * value: the UUID of the new-version of the template.
         */
        public static final String $replaced_by_version = $base + "replaced_by_version";
        /**
         * String constant for the metadata property key 'service_type'. 
         * value: the UUID of the type of service provided by the template (corresponds to the ID of the ServiceType defined in the Service Construction Model).
         */
        public static final String $service_type = $base + "service_type";
        /*
        public static final String[] __strings = {
            $provider_uuid,
            $template_uuid,
            $creator_id,
            $registrar_id,
            $registered_at_datetime,
            $activated_at_datetime,
            $disactived_at_datetime,
            $previous_version,
            $replaced_by_version,
            $service_type
        };
        */
        // ... STND VERSIONS
        /**
         * STND constant for the metadata property key 'provider_uuid'. 
         * value: a UUID identifying the provider of the described SLATemplate.
         */
        public static final STND provider_uuid = new STND($provider_uuid);
        /**
         * STND constant for the metadata property key 'template_uuid'. 
         * value: the UUID  of the described SLATemplate as defined within the template itself.
         */
        public static final STND template_uuid = new STND($template_uuid);
        /**
         * STND constant for the metadata property key 'creator_id'. 
         * value: a UUID  identifying the designer/creator of the described SLATemplate.
         */
        public static final STND creator_id = new STND($creator_id);
        /**
         * STND constant for the metadata property key 'registrar_id'. 
         * value: a UUID  identifying the registrar responsible for registering the described SLATemplate with the SLATemplateRegistry..
         */
        public static final STND registrar_id = new STND($registrar_id);
        /**
         * STND constant for the metadata property key 'registered_at_datetime'. 
         * value: a TIME  value giving the date/time at which the SLATemplate was registered in the SLATemplateRegistry..
         */
        public static final STND registered_at_datetime = new STND($registered_at_datetime);
        /**
         * STND constant for the metadata property key 'activated_at_datetime'. 
         * value: a TIME value giving the date/time at which the SLATemplate was 'activated', i.e. made available as a 'service offer'.
         */
        public static final STND activated_at_datetime = new STND($activated_at_datetime);
        /**
         * STND constant for the metadata property key 'disactived_at_datetime'. 
         * value: a TIME value giving the date/time at which the SLATemplate was 'disactivated', i.e. withdrawn as a 'service offer' (must be later than #activated_at_datetime).
         */
        public static final STND disactived_at_datetime = new STND($disactived_at_datetime);
        /**
         * STND constant for the metadata property key 'previous_version'. 
         * value: the UUID  of the old-version of the template.
         */
        public static final STND previous_version = new STND($previous_version);
        /**
         * STND constant for the metadata property key 'replaced_by_version'. 
         * value: the UUID of the new-version of the template.
         */
        public static final STND replaced_by_version = new STND($replaced_by_version);
        /**
         * STND constant for the metadata property key 'service_type'. 
         * value: the UUID of the type of service provided by the template (corresponds to the ID of the ServiceType defined in the Service Construction Model).
         */
        public static final STND service_type = new STND($service_type);
        
        public static final STND[] __stnds = {
            provider_uuid,
            template_uuid,
            creator_id,
            registrar_id,
            registered_at_datetime,
            activated_at_datetime,
            disactived_at_datetime,
            previous_version,
            replaced_by_version
        };
        
        public boolean equals(Object obj){
        	if (this == obj) return true;
        	if (obj instanceof Metadata){
        		Metadata m = (Metadata)obj;
        		STND[] keys1 = this.getPropertyKeys();
        		STND[] keys2 = m.getPropertyKeys();
        		if (keys1.length == keys2.length){
        			for (STND key : keys1){
        				String v1 = this.getPropertyValue(key);
        				String v2 = m.getPropertyValue(key);
        				if (v2 == null || !v1.equals(v2)) return false;
        			}
        			return true;
        		}
        	}
        	return false;
        }
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Exceptions
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Class of exceptions thrown by SLATemplateRegistry methods.
     */
    public static class Exception extends java.lang.Exception{
    	
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Object source_entity = null;
        
        public Exception(java.lang.Exception e){
            super(e);
        }
        
        public Exception(String msg, Object source_entity){
            super(msg);
            this.source_entity = source_entity;
        }
        
        /**
         * @return the specific SLA(T) entity that was the source of the error<br>
         * For SYSTEM_ERROR, the source is always <code>null</code>,<br>
         * otherwise it can be any SLA(T) Model object<br>
         * Note that ID, UUID &amp; STND objects may be sourced here as Strings.
         */
        public Object sourceEntity(){
            return source_entity;
        }
        
    }
    
}
