/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/commons/pss4slam-how2use/src/main/java/org/slasoi/gslam/commons/how2use/pss4slam/PSS4SLAMBean.java $
 */

package org.slasoi.gslam.commons.how2use.pss4slam;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.slasoi.sa.pss4slam.core.ServiceAdvertisement;
import org.slasoi.sa.pss4slam.core.IPublishable;
import org.slasoi.sa.pss4slam.core.ISubscribable;

import org.springframework.osgi.extensions.annotation.ServiceReference;

public class PSS4SLAMBean
{
    public void start()
    {
        publisher  = new Publisher ( services );
        
        subscriber0 = new Subscriber( "subscriber#0", services );
        subscriber1 = new Subscriber( "subscriber#1", services );
        subscriber2 = new Subscriber( "subscriber#2", services );
        
        Thread t = new Thread( new Runnable()
        {
            public void run()
            {
                try
                {
                    System.out.println( "PSS4SLAMBean will start to send templates to the 'pss4slam' in 5 secs..." );
                    Thread.sleep( 5000 );
                }
                catch ( Exception e )
                {
                }
                
                publisher.run();
            }
        } );
        
        t.start();
    }
    
    public void stop()
    {
    }
    
    @ServiceReference
    public void setServiceAdvertisement( ServiceAdvertisement services ) // work-around:  services should be ServiceAdvertisement.  
    {
        LOGGER.info( "PSS4SLAMBean getting ServiceAdvertisement reference :  " + services + " from OSGi container/Spring DM" );
        this.services = services;
    }

    protected ServiceAdvertisement services;
    
    protected Publisher  publisher ;
    
    protected Subscriber subscriber0;
    protected Subscriber subscriber1;
    protected Subscriber subscriber2;

    private static final Log LOGGER = LogFactory.getLog( PSS4SLAMBean.class );
}
