/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/commons/plan/src/main/java/org/slasoi/gslam/commons/plan/Task.java $
 */

/**
 * 
 */
package org.slasoi.gslam.commons.plan;

import java.util.Date;

import org.slasoi.gslam.core.pac.ProvisioningAdjustment;

/**
 * @author Beatriz
 * 
 */
public class Task implements ProvisioningAdjustment.Task {

    /**
     * task identifier.
     */
    private String taskId;

    /**
     * SLA id.
     */
    private String slaId;

    /**
     * name of the action to be executed.
     */
    private String actionName;

    /**
     * identifier of the Service Manager to which the action will be sent.
     */
    private String serviceManagerId;

    /**
     * time when the service should be deployed. If not specified, the execution will start asap.
     */
    private Date deploymentTime = null;

    /**
     * maximum allowed time for the action to be executed.
     */
    private long timeout = 0;

    /**
     * property for internal purposes (for example, could be useful to indicate if the node of the plan has already been
     * visited.
     */
    private String status;

    /**
     * Constructor.
     * 
     * @param taskId
     *            the identifier for this task
     * @param slaId
     *            the sla identifier
     * @param actionName
     *            the name of the action to be executed
     * @param serviceManagerId
     *            the identifier of the service manager
     */
    public Task(String taskId, String slaId, String actionName, String serviceManagerId) {
        this.taskId = taskId;
        this.slaId = slaId;
        this.actionName = actionName;
        this.serviceManagerId = serviceManagerId;
        this.status = "CREATED"; // use enum
    }

    /**
     * sets the task id.
     * 
     * @param id
     *            identifier for this task
     */
    public void setTaskId(String id) {
        taskId = id;
    }

    /**
     * gets the task identifier.
     * 
     * @return the task identifier
     */
    public String getTaskId() {
        return (taskId);
    }

    /**
     * sets the SLA id.
     * 
     * @param id
     *            the SLA identifier
     */
    public void setSlaId(String id) {
        slaId = id;
    }

    /**
     * gets the SLA id.
     * 
     * @return the SLA identifier
     */
    public String getSlaId() {
        return (slaId);
    }

    /**
     * sets the name of the action.
     * 
     * @param actionName
     *            the name of the action to be executed
     */
    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    /**
     * gets the name of the action to be executed.
     * 
     * @return the name of the action to be executed
     */
    public String getActionName() {
        return (actionName);
    }

    /**
     * sets the ServiceManager id.
     * 
     * @param id
     *            the ServiceManager identifier
     */
    public void setServiceManagerId(String id) {
        serviceManagerId = id;
    }

    /**
     * gets the ServiceManager id.
     * 
     * @return the ServiceManager identifier
     */
    public String getServiceManagerId() {
        return (serviceManagerId);
    }

    /**
     * sets the deployment time.
     * 
     * @param deploymentTime
     *            the deployment time
     */
    public void setDeploymentTime(Date deploymentTime) {
        this.deploymentTime = deploymentTime;
    }

    /**
     * gets the deployment time.
     * 
     * @return the deployment time
     */
    public Date getDeploymentTime() {
        return (deploymentTime);
    }

    /**
     * sets the timeout.
     * 
     * @param timeout
     *            maximum time for the action to get executed
     */
    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    /**
     * gets the timeout.
     * 
     * @return the timeout
     */
    public long getTimeout() {
        return (timeout);
    }

    /**
     * set the status of the task.
     * 
     * @param status
     *            the status of the task
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * gets the status of the task.
     * 
     * @return the status of the task
     */
    public String getStatus() {
        return (status);
    }

    /**
     * toString method.
     * 
     * @return a String representation of a Plan
     */
    public String toString() {
        return taskId;
    }
}
