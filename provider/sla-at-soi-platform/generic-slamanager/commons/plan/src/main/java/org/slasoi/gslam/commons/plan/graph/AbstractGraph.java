/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/commons/plan/src/main/java/org/slasoi/gslam/commons/plan/graph/AbstractGraph.java $
 */

package org.slasoi.gslam.commons.plan.graph;

import java.util.Collection;
import java.util.Set;

/**
 * Implementation of an abstract graph, with generic methods that are valid for both directed and undirected graphs.
 * 
 * @param <V>
 *            vertex
 * @param <E>
 *            edge
 * 
 * @author Beatriz Fuentes
 * 
 */

public abstract class AbstractGraph<V, E> implements Graph<V, E> {

    /**
     * Checks if the graph contains an edge going from vertex v1 to vertex v2.
     * 
     * @param v1
     *            head vertex of the edge.
     * @param v2
     *            tail vertex of the edge.
     * @return <tt>true</tt> if the graph contains the specified edge.
     * @throws VertexNotFoundException
     *             if one of the vertices is not contained in the graph
     */

    public boolean containsEdge(final V v1, final V v2) throws VertexNotFoundException {
        return getEdges(v1, v2) != null;
    }

    /**
     * Removes the specified edges.
     * 
     * @param edges
     *            edges to be removed from this graph.
     * 
     */
    public void removeEdges(final Collection<E> edges) {
        for (E e : edges) {
            try {
                removeEdge(e);
            }
            catch (Exception exception) {
                // Just ignore the missing edge/vertex and continue with the removal
                continue;
            }
        }
    }

    /**
     * Removes all the edges going from vertex v1 to vertex v2. Returns a set of all removed edges. If both vertices
     * exist but no edge is found, returns an empty set.
     * 
     * @param v1
     *            head vertex of the edge.
     * @param v2
     *            tail vertex of the edge.
     * 
     * @return the removed edges
     * @throws VertexNotFoundException
     *             if one of the specified vertices does not exist in the graph
     */
    public Set<E> removeEdges(final V v1, final V v2) throws VertexNotFoundException {
        Set<E> edges = getEdges(v1, v2);
        removeEdges(edges);

        return edges;
    }

    /**
     * Removes the specified vertices.
     * 
     * @param vertices
     *            vertices to be removed from this graph.
     * 
     */
    public void removeVertices(final Collection<V> vertices) {
        for (V v : vertices) {
            try {
                removeVertex(v);
            }
            catch (VertexNotFoundException exception) {
                // Just ignore the missing edge and continue with the removal
                continue;
            }
        }
    }
}
