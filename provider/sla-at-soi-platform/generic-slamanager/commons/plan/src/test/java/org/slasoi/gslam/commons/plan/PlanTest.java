/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Beatriz Fuentes - fuentes@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (ned, 05 dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/generic-slamanager/commons/plan/src/test/java/org/slasoi/gslam/commons/plan/PlanTest.java $
 */

/**
 *
 */
package org.slasoi.gslam.commons.plan;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Beatriz Fuentes
 * 
 */
public class PlanTest {
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(PlanTest.class.getName());

    /**
     * Maximum number of tasks.
     */
    private static final int MAX_TASKS = 11;

    /**
     * First step when filling the plan.
     */
    private static final int STEP1 = 4;

    /**
     * Second step when filling the plan.
     */
    private static final int STEP2 = 6;

    /**
     * Third step when filling the plan.
     */
    private static final int STEP3 = 8;

    /**
     * Fourth step when filling the plan.
     */
    private static final int STEP4 = 10;

    /**
     * This will be a parent of other nodes.
     */
    private static final int PARENT1 = 0;

    /**
     * This will be a parent of other nodes.
     */
    private static final int PARENT2 = 1;

    /**
     * This will be a parent of other nodes.
     */
    private static final int PARENT3 = 4;

    /**
     * This will be a parent of other nodes.
     */
    private static final int PARENT4 = 8;

    /**
     * This node will be the vertex of an edge.
     */
    private static final int EDGE1 = 3;

    /**
     * This node will be the vertex of an edge.
     */
    private static final int EDGE2 = 7;

    /**
     * Timeout.
     */
    private static final int TIMEOUT = 10;

    /**
     * Created as status of task
     */
    private static final String CREATED = "Created";

    /**
     * array of tasks to be inserted into the plan.
     */
    Task[] tasks = new Task[MAX_TASKS];

    /**
     * Initialize the array of tasks.
     */
    @Before
    public final void init() {
        for (int i = 0; i < MAX_TASKS; i++) {
            tasks[i] = new Task(new Integer(i).toString(), "SLA_23456", "Action_" + i, "SM_" + i);
            tasks[i].setDeploymentTime(new Date());
            tasks[i].setTimeout(TIMEOUT);
            tasks[i].setStatus(CREATED);
        }
    }

    /**
     * Test that fills a plan and then tries to insert again a root node.
     * 
     * @throws RootFoundException
     *             if the root has already been set
     * @throws TaskFoundException
     *             if the task has already been inserted
     */
    @Test(expected = RootFoundException.class)
    public final void rootFoundException() throws TaskFoundException, RootFoundException {

        // Fills a simple plan with the root node
        Plan plan = new Plan("MyPlan");

        logger.info("Filling plan ");

        plan.setRoot(tasks[0]);

        // add another task as root
        plan.setRoot(tasks[1]);

    }

    /**
     * Test that fills a plan and then tries to insert again a node.
     * 
     * @throws TaskFoundException
     *             if the task has already been inserted
     * @throws RootFoundException
     */
    @Test(expected = TaskFoundException.class)
    public final void vertexFoundException() throws TaskFoundException, RootFoundException {

        // Fills a simple plan with the root node
        Plan plan = new Plan("MyPlan");

        logger.info("Filling plan ");

        plan.setRoot(tasks[0]);

        // add another task
        plan.addNode(tasks[1]);

        // add the same task again
        plan.addNode(tasks[1]);
    }

    /**
     * Test that fills a plan and then tries to access a not-existing node.
     * 
     * @throws TaskFoundException
     *             if the task has already been inserted
     * @throws RootFoundException
     *             if the root node had already been inserted
     * @throws TaskNotFoundException
     *             when trying to access a not-existing node.
     */
    @Test(expected = TaskNotFoundException.class)
    public final void vertexNotFoundException() throws TaskFoundException, RootFoundException, TaskNotFoundException {

        // Fills a simple plan with the root node
        Plan plan = new Plan("MyPlan");

        logger.info("Filling plan ");

        plan.setRoot(tasks[0]);

        // add another task
        plan.getChildren(tasks[1]);

    }

    /**
     * Method that iterates on a given plan, printing its content.
     * 
     * @param plan
     *            the plan to be visited
     */
    @Test
    public final void iterateOnPlan() {
        Plan plan = new Plan("MyPlan");

        fillPlan(plan);
        logger.info("iterateOnPlan " + plan.getPlanId());

        boolean[] visited = new boolean[MAX_TASKS];
        for (int i = 0; i < MAX_TASKS; i++) {
            visited[i] = false;
        }

        // First, take the root
        Task root = plan.getRootTask();

        // List to store the not-yet-expanded nodes
        ArrayList<Task> visitedTasks = new ArrayList<Task>();
        visitedTasks.add(root);

        Task current = null;
        while (visitedTasks.size() > 0) {
            // Take the first task and deletes it from the list
            current = visitedTasks.get(0);

            logger.info("Node " + current.getTaskId() + " visited");
            visited[new Integer(current.getTaskId()).intValue()] = true;

            // Checking parameters
            Task task = tasks[new Integer(current.getTaskId()).intValue()];
            assertEquals(current.getActionName(), task.getActionName());
            assertEquals(current.getServiceManagerId(), task.getServiceManagerId());
            assertEquals(current.getSlaId(), task.getSlaId());
            assertEquals(current.getTaskId(), task.getTaskId());
            assertEquals(current.getDeploymentTime(), task.getDeploymentTime());
            assertEquals(current.getTimeout(), task.getTimeout());
            assertEquals(current.getStatus(), task.getStatus());

            // Expands the node and add the children to the list if it is not already there
            try {
                Set<Task> children = plan.getChildren(current);
                for (Task t : children) {
                    if (!visitedTasks.contains(t)) {
                        visitedTasks.add(t);
                    }
                }
            }
            catch (TaskNotFoundException e) {
                e.printStackTrace();
            }

            visitedTasks.remove(current);
        }

        for (int i = 0; i < MAX_TASKS; i++) {
            assert (visited[i]);
        }

    }

    /**
     * Test that fills a plan, and then iterates on it, printing the content.
     */
    public final void fillPlan(final Plan plan) {

        // Fills a simple plan with several tasks

        logger.info("Filling plan ");

        try {
            plan.setRoot(tasks[0]);

            // add 1, 2, 3, 4 as children
            List<Task> children = new ArrayList<Task>();
            for (int i = 1; i <= STEP1; i++) {
                children.add(tasks[i]);
                logger.debug("Adding task " + i);
            }
            plan.addChildren(tasks[PARENT1], children);
            logger.debug("... as children of node " + PARENT1);

            // add 5,6 as 1's children
            List<Task> children1 = new ArrayList<Task>();
            for (int i = STEP1 + 1; i <= STEP2; i++) {
                children1.add(tasks[i]);
                logger.debug("Adding task " + i);
            }
            plan.addChildren(tasks[PARENT2], children1);
            logger.debug("... as children of node " + PARENT2);

            // add 7,8 as 4's children
            List<Task> children4 = new ArrayList<Task>();
            for (int i = STEP2 + 1; i <= STEP3; i++) {
                children4.add(tasks[i]);
                logger.debug("Adding task " + i);
            }
            plan.addChildren(tasks[PARENT3], children4);
            logger.debug("... as children of node " + PARENT3);

            // add an edge between 3 and 7
            plan.addEdge(tasks[EDGE1], tasks[EDGE2]);
            logger.debug("Adding edge between " + EDGE1 + " and " + EDGE2);

            // add 9,10 as 8's children
            List<Task> children8 = new ArrayList<Task>();
            for (int i = STEP3 + 1; i <= STEP4; i++) {
                children8.add(tasks[i]);
                logger.debug("Adding task " + i);
            }
            plan.addChildren(tasks[PARENT4], children8);
            logger.debug("... as children of node " + PARENT4);

            logger.info(plan.toString());

            logger.info("Number of nodes in plan = " + plan.getNumberNodes());

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // iterateOnPlan(plan);
    }

}
