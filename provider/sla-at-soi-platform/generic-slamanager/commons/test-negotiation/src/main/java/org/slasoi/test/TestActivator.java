package org.slasoi.test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils.GenericSLAManagerUtilsException;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.INegotiation;
import org.slasoi.gslam.core.negotiation.INegotiation.CRITIQUE;
import org.slasoi.gslam.core.negotiation.INegotiation.Customization;
import org.slasoi.gslam.core.negotiation.INegotiation.InvalidNegotiationIDException;
import org.slasoi.gslam.core.negotiation.INegotiation.NegotiableParameters;
import org.slasoi.gslam.core.negotiation.INegotiation.NegotiationGlossary;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationInProgressException;
import org.slasoi.gslam.core.negotiation.INegotiation.OperationNotPossibleException;
import org.slasoi.gslam.core.negotiation.INegotiation.RESULT;
import org.slasoi.gslam.core.negotiation.INegotiation.SLACreationException;
import org.slasoi.gslam.core.negotiation.INegotiation.SLANotFoundException;
import org.slasoi.gslam.core.negotiation.INegotiation.TerminationReason;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.core.negotiation.ProtocolEngine;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;


public class TestActivator implements BundleActivator {
    private BundleContext context;
    private ServiceTracker tracker;
    private GenericSLAManagerUtils genericSLAManagerUtils;

    private static final String COMMENT = "#";
    private static final String ORC_HOME = "{SLASOI_ORC_HOME}";
    private static final String SLASOI_HOME = System.getenv("SLASOI_HOME");
    private static final String SLASOI_ORC_HOME = System.getenv("SLASOI_ORC_HOME");
    private INegotiation iNegotiation;
    private INegotiation bizINegotiation;
    private SLATemplate softwareSLATemplate;
    private SLATemplate businessSLATemplate;
    private SLATemplate infrSLATemplate;
    private String softwareSLATemplateXml;
    private String infrSLATemplateXml;
    private ISyntaxConverter sc;
	private SLATemplateRegistry slatr;
    private static Logger LOGGER = Logger.getLogger(TestActivator.class);

    public void start(BundleContext context) throws java.lang.Exception {
        this.context = context;
        this.tracker = new ServiceTracker(context, GenericSLAManagerUtils.class.getName(), null);
        tracker.open();
        try {
            genericSLAManagerUtils = (GenericSLAManagerUtils) tracker.waitForService(5000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Commander cmd = new Commander();
        context.registerService(cmd.getClass().getName(), cmd, null);
    }

    public void stop(BundleContext context) throws java.lang.Exception {
        LOGGER.info("**************** TestActivator BUNDLE STOPPED ******************");
        tracker.close();
    }
    
    public void tempSetup1(){
        try {
            if (genericSLAManagerUtils != null) {

                SLAManagerContext[] slaManagerContexts = genericSLAManagerUtils.getContextSet("GLOBAL");
                sc =
                    ((Hashtable<ISyntaxConverter.SyntaxConverterType, ISyntaxConverter>) slaManagerContexts[0]
                            .getSyntaxConverters())
                            .get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter);
                
                //Default 2 SLAM Scenario : SWSLAM is to be in slaManagerContexts[0] and IFSLAM is to be in slaManagerContexts[1]
                if (slaManagerContexts != null && slaManagerContexts.length <= 2) {
                    LOGGER.info("***  TestActivator: slaManagerContexts[0]\n" + slaManagerContexts[0].getSLAManagerID());
                    
                    //ProtocolEngine pe = slaManagerContexts[0].getProtocolEngine();

                    // Main Negotiation Test
                    String filePath =
                            SLASOI_HOME + System.getProperty("file.separator") + "generic-slamanager"
                                    + System.getProperty("file.separator")
                                    + "commons" + System.getProperty("file.separator") + "test-templates"
                                    + System.getProperty("file.separator") + "templates.conf";
                    LineIterator pathLines = null;
                    pathLines = FileUtils.lineIterator(new File(filePath));

                    if (pathLines != null) {
                        while (pathLines.hasNext()) {
                            String pathLine = pathLines.nextLine();
                            LOGGER.info("Reader : path-1 = " + pathLine);
                            if (pathLine == null || pathLine.equals("") || pathLine.startsWith(COMMENT)) {
                                continue;
                            }
                            if (pathLine.contains(ORC_HOME)) {
                                pathLine = pathLine.replace(ORC_HOME, SLASOI_ORC_HOME);
                            }
                            LOGGER.info("Reader : path-2 = " + pathLine);

                            File file = new File(pathLine);
                            String fileName = file.getName().substring(0, file.getName().lastIndexOf(".xml"));
                            softwareSLATemplateXml = FileUtils.readFileToString(file);
                            LOGGER.info("************** CONTACTING SYNTAX CONVERTER *****************");

                            softwareSLATemplate = (SLATemplate) sc.parseSLATemplate(softwareSLATemplateXml);
                            LOGGER.info("************** RECEIVED FROM SYNTAX CONVERTER *****************"
                                    + softwareSLATemplate);
                            // Set Annotations:
                            softwareSLATemplate.setPropertyValue(org.slasoi.slamodel.vocab.sla.service_type,
                                    "e19cd2cb-07b1-432b-bd82-ffa33f3fb0fc");
                            Party[] parties = softwareSLATemplate.getParties();
                            Party[] modifiedParties = new Party[parties.length + 1];
                            int index = 0;
                            for (Party party : parties) {
                                modifiedParties[index] = party;
                                index++;
                                if (party.getAgreementRole() != null && party.getAgreementRole().equals(sla.provider)) {
                                    if (party.getPropertyValue(sla.gslam_epr) != null) {
                                        LOGGER.info("********** TestActivator: Provide party : "
                                                + party.getPropertyValue(org.slasoi.slamodel.vocab.sla.gslam_epr));
                                    }
                                    else {
                                        System.out
                                                .println("********** TestActivator: Provide party was NOT set! Setting gslam_epr = "
                                                        + "http://localhost:8080/services/SWNegotiation?wsdl");
                                        party.setPropertyValue(sla.gslam_epr,
                                                "http://localhost:8080/services/SWNegotiation?wsdl");
                                    }
                                }
                                else {
                                    continue;
                                }
                            }
                            Party customerParty = new Party(new ID("ORC_SW_Service-user"), sla.customer);
                            customerParty.setPropertyValue(sla.gslam_epr, "http://customer.client.bundle");
                            modifiedParties[index] = customerParty;
                            softwareSLATemplate.setParties(modifiedParties);
                            
                            
                        }                        
                    }
                    
                    if(sc != null){
                        System.out.println("***  TestActivator: SC is not null - About to invoke initiateNegotiation");
                        INegotiation negotiationClient = sc.getNegotiationClient("http://localhost:8080/services/SWNegotiation?wsdl");
                        String negotiationId = negotiationClient.initiateNegotiation(softwareSLATemplate);
                        System.out.println("***  TestActivator: Got NegotiationId = "+negotiationId);
                        
                        Customization customization = new Customization();
                        NegotiableParameters negotiableParameter1 = new NegotiableParameters(NegotiationGlossary.NEGOTIATION_ROUNDS);
                        negotiableParameter1.setValue(10 + "");
                        negotiableParameter1.setCritique(CRITIQUE.ACCEPTED);

                        NegotiableParameters negotiableParameter2 = new NegotiableParameters(NegotiationGlossary.PROCESS_TIMEOUT);
                        negotiableParameter2.setValue(900000 + "");
                        negotiableParameter2.setCritique(CRITIQUE.ACCEPTED);

                        NegotiableParameters negotiableParameter3 = new NegotiableParameters(NegotiationGlossary.CUSTOMIZATION_ROUNDS);
                        negotiableParameter3.setValue(6 + "");
                        negotiableParameter3.setCritique(CRITIQUE.ACCEPTED);

                        NegotiableParameters negotiableParameter4 = new NegotiableParameters(NegotiationGlossary.MAX_COUNTER_OFFERS);
                        negotiableParameter4.setValue(4 + "");
                        negotiableParameter4.setCritique(CRITIQUE.ACCEPTED);

                        NegotiableParameters negotiableParameter5 =
                                new NegotiableParameters(NegotiationGlossary.OPTIONAL_CRITIQUE_ON_QOS);
                        negotiableParameter5.setValue(true+"");
                        negotiableParameter5.setCritique(CRITIQUE.ACCEPTED);

                        List<NegotiableParameters> listOfNegotiableParameters = new ArrayList<NegotiableParameters>();
                        listOfNegotiableParameters.add(negotiableParameter1);
                        listOfNegotiableParameters.add(negotiableParameter2);
                        listOfNegotiableParameters.add(negotiableParameter3);
                        listOfNegotiableParameters.add(negotiableParameter4);
                        listOfNegotiableParameters.add(negotiableParameter5);

                        customization.setListOfNegotiableParameters(listOfNegotiableParameters);
                        customization = negotiationClient.customize(negotiationId, customization);
                        System.out.println("***  TestActivator: Got customization = "+customization.toString());
                    }
                }
                else {
                    
                }
            }
        }
            catch (GenericSLAManagerUtilsException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            catch (SLAManagerContextException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (java.lang.Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

    }
    
    public void tempSetup2(){
        try {
            if (genericSLAManagerUtils != null) {

                SLAManagerContext[] slaManagerContexts = genericSLAManagerUtils.getContextSet("GLOBAL");
                sc =
                    ((Hashtable<ISyntaxConverter.SyntaxConverterType, ISyntaxConverter>) slaManagerContexts[0]
                            .getSyntaxConverters())
                            .get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter);
                
                //Default 2 SLAM Scenario : SWSLAM is to be in slaManagerContexts[0] and IFSLAM is to be in slaManagerContexts[1]
                if (slaManagerContexts != null && slaManagerContexts.length <= 2) {
                    LOGGER.info("***  TestActivator: slaManagerContexts[0]\n" + slaManagerContexts[0].getSLAManagerID());
                    
                    //ProtocolEngine pe = slaManagerContexts[0].getProtocolEngine();

                    // Main Negotiation Test
                    String filePath =
                            SLASOI_HOME + System.getProperty("file.separator") + "generic-slamanager"
                                    + System.getProperty("file.separator")
                                    + "commons" + System.getProperty("file.separator") + "test-templates"
                                    + System.getProperty("file.separator") + "templates.conf";
                    LineIterator pathLines = null;
                    pathLines = FileUtils.lineIterator(new File(filePath));

                    if (pathLines != null) {
                        while (pathLines.hasNext()) {
                            String pathLine = pathLines.nextLine();
                            LOGGER.info("Reader : path-1 = " + pathLine);
                            if (pathLine == null || pathLine.equals("") || pathLine.startsWith(COMMENT)) {
                                continue;
                            }
                            if (pathLine.contains(ORC_HOME)) {
                                pathLine = pathLine.replace(ORC_HOME, SLASOI_ORC_HOME);
                            }
                            LOGGER.info("Reader : path-2 = " + pathLine);

                            File file = new File(pathLine);
                            String fileName = file.getName().substring(0, file.getName().lastIndexOf(".xml"));
                            infrSLATemplateXml = FileUtils.readFileToString(file);
                            LOGGER.info("************** CONTACTING SYNTAX CONVERTER *****************");

                            infrSLATemplate = (SLATemplate) sc.parseSLATemplate(infrSLATemplateXml);
                            LOGGER.info("************** RECEIVED FROM SYNTAX CONVERTER *****************"
                                    + infrSLATemplate);
                            // Set Annotations:

                            Party[] parties = infrSLATemplate.getParties();
                            Party[] modifiedParties = new Party[parties.length + 1];
                            int index = 0;
                            for (Party party : parties) {
                                modifiedParties[index] = party;
                                index++;
                                if (party.getAgreementRole() != null && party.getAgreementRole().equals(sla.provider)) {
                                    if (party.getPropertyValue(sla.gslam_epr) != null) {
                                        LOGGER.info("********** TestActivator: Provide party : "
                                                + party.getPropertyValue(org.slasoi.slamodel.vocab.sla.gslam_epr));
                                    }
                                    else {
                                        System.out
                                                .println("********** TestActivator: Provide party was NOT set! Setting gslam_epr = "
                                                        + "http://localhost:8080/services/ISNegotiation?wsdl");
                                        party.setPropertyValue(sla.gslam_epr,
                                                "http://localhost:8080/services/ISNegotiation?wsdl");
                                    }
                                }
                                else {
                                    continue;
                                }
                            }
                            Party customerParty = new Party(new ID("ORC_ISLAM_client"), sla.customer);
                            customerParty.setPropertyValue(sla.gslam_epr, "http://localhost:8080/services/SWNegotiation?wsdl");
                            modifiedParties[index] = customerParty;
                            infrSLATemplate.setParties(modifiedParties);
                            
                            
                        }                        
                    }
                    
                    if(sc != null){
                        
                        //Initiate a Possible Negotiation:
                        System.out.println("***  TestActivator: SC is not null - About to invoke initiateNegotiation");
                        INegotiation negotiationClient = sc.getNegotiationClient("http://localhost:8080/services/SWNegotiation?wsdl");
                        String negotiationId = negotiationClient.initiateNegotiation(infrSLATemplate);
                        System.out.println("***  TestActivator: Got NegotiationId = "+negotiationId);
                        
                        //Perform Pre-Negotiation:
                        Customization customization = new Customization();
                        NegotiableParameters negotiableParameter1 = new NegotiableParameters(NegotiationGlossary.NEGOTIATION_ROUNDS);
                        negotiableParameter1.setValue(10 + "");
                        negotiableParameter1.setCritique(CRITIQUE.ACCEPTED);

                        NegotiableParameters negotiableParameter2 = new NegotiableParameters(NegotiationGlossary.PROCESS_TIMEOUT);
                        negotiableParameter2.setValue(900000 + "");
                        negotiableParameter2.setCritique(CRITIQUE.ACCEPTED);

                        NegotiableParameters negotiableParameter3 = new NegotiableParameters(NegotiationGlossary.CUSTOMIZATION_ROUNDS);
                        negotiableParameter3.setValue(6 + "");
                        negotiableParameter3.setCritique(CRITIQUE.ACCEPTED);

                        NegotiableParameters negotiableParameter4 = new NegotiableParameters(NegotiationGlossary.MAX_COUNTER_OFFERS);
                        negotiableParameter4.setValue(4 + "");
                        negotiableParameter4.setCritique(CRITIQUE.ACCEPTED);

                        NegotiableParameters negotiableParameter5 =
                                new NegotiableParameters(NegotiationGlossary.OPTIONAL_CRITIQUE_ON_QOS);
                        negotiableParameter5.setValue(true+"");
                        negotiableParameter5.setCritique(CRITIQUE.ACCEPTED);

                        List<NegotiableParameters> listOfNegotiableParameters = new ArrayList<NegotiableParameters>();
                        listOfNegotiableParameters.add(negotiableParameter1);
                        listOfNegotiableParameters.add(negotiableParameter2);
                        listOfNegotiableParameters.add(negotiableParameter3);
                        listOfNegotiableParameters.add(negotiableParameter4);
                        listOfNegotiableParameters.add(negotiableParameter5);

                        customization.setListOfNegotiableParameters(listOfNegotiableParameters);
                        customization = negotiationClient.customize(negotiationId, customization);
                        System.out.println("***  TestActivator: 1- Got consolidated customization result = "+customization.toString());
                        
                        System.out.println("***  TestActivator: Invoking customization again ");
                        customization = negotiationClient.customize(negotiationId, customization);
                        System.out.println("***  TestActivator: 2- Got consolidated customization result = "+customization.toString());
                        
                        //Starting Negotiation IFF PreNegotiation has succeeded in reaching consensus through Customization mechanism:
                        if(customization.getResult().equals(RESULT.AGREED)){
                            try {
                                
                                LOGGER.info("********************  TestActivator: Before calling negotiate-1");
                                SLATemplate infrSLATemplates[] = negotiationClient.negotiate(negotiationId, infrSLATemplate);
                                LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                                        + infrSLATemplates.length);
                                
                                infrSLATemplates = negotiationClient.negotiate(negotiationId, infrSLATemplates[0]);
                                LOGGER.info("********************  TestActivator: After calling negotiate-2. Counter-Offers returned = "
                                        + infrSLATemplates.length);
                                
//                                infrSLATemplates = negotiationClient.negotiate(negotiationId, infrSLATemplates[0]);
//                                LOGGER.info("********************  TestActivator: After calling negotiate-3. Counter-Offers returned = "
//                                        + infrSLATemplates.length);
                                
                                LOGGER.info("********************  TestActivator: Before calling createAgreement");
                                SLA sla = negotiationClient.createAgreement(negotiationId, infrSLATemplates[0]);
                                LOGGER.info("********************  TestActivator: After calling createAgreement. SLA.AgreedAt = "
                                        + sla.getAgreedAt() + " SLA.EffectiveFrom = " + sla.getEffectiveFrom() + "SLA.EffectiveUntil = "
                                        + sla.getEffectiveUntil());
                                
                                LOGGER.info("********************  TestActivator: END OF NEGOTIATION PROCESS ");
                            }

                            catch (OperationNotPossibleException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            catch (OperationInProgressException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            catch (InvalidNegotiationIDException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            catch (SLACreationException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        
                    }
                }
                else {
                    
                }
            }
        }
            catch (GenericSLAManagerUtilsException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            catch (SLAManagerContextException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (java.lang.Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    }
    
    public void setup() {
        try {
            if (genericSLAManagerUtils != null) {

                SLAManagerContext[] slaManagerContexts = genericSLAManagerUtils.getContextSet("GLOBAL");
                
                //Default 2 SLAM Scenario : SWSLAM is to be in slaManagerContexts[0] and IFSLAM is to be in slaManagerContexts[1]
                if (slaManagerContexts != null && slaManagerContexts.length <= 2) {
                    LOGGER.info("***  TestActivator: slaManagerContexts[0]\n" + slaManagerContexts[0].getSLAManagerID());
                    sc =
                            ((Hashtable<ISyntaxConverter.SyntaxConverterType, ISyntaxConverter>) slaManagerContexts[0]
                                    .getSyntaxConverters())
                                    .get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter);

                    slatr = slaManagerContexts[0].getSLATemplateRegistry();
                    iNegotiation = sc.getNegotiationClient("http://localhost:8080/services/SWNegotiation?wsdl");
                    
                    // Main Negotiation Test
                    String filePath =
                            SLASOI_HOME + System.getProperty("file.separator") + "generic-slamanager"
                                    + System.getProperty("file.separator") + System.getProperty("file.separator")
                                    + "commons" + System.getProperty("file.separator") + "test-templates"
                                    + System.getProperty("file.separator") + "templates.conf";
                    LineIterator pathLines = null;
                    pathLines = FileUtils.lineIterator(new File(filePath));

                    if (pathLines != null) {
                        while (pathLines.hasNext()) {
                            String pathLine = pathLines.nextLine();
                            LOGGER.info("Reader : path-1 = " + pathLine);
                            if (pathLine == null || pathLine.equals("") || pathLine.startsWith(COMMENT)) {
                                continue;
                            }
                            if (pathLine.contains(ORC_HOME)) {
                                pathLine = pathLine.replace(ORC_HOME, SLASOI_ORC_HOME);
                            }
                            LOGGER.info("Reader : path-2 = " + pathLine);

                            File file = new File(pathLine);
                            String fileName = file.getName().substring(0, file.getName().lastIndexOf(".xml"));
                            softwareSLATemplateXml = FileUtils.readFileToString(file);
                            LOGGER.info("************** CONTACTING SYNTAX CONVERTER *****************");
                            softwareSLATemplate = (SLATemplate) sc.parseSLATemplate(softwareSLATemplateXml);
                            LOGGER.info("************** RECEIVED FROM SYNTAX CONVERTER *****************"
                                    + softwareSLATemplate);
                            // Set Annotations:
                            softwareSLATemplate.setPropertyValue(org.slasoi.slamodel.vocab.sla.service_type,
                                    "e19cd2cb-07b1-432b-bd82-ffa33f3fb0fc");
                            Party[] parties = softwareSLATemplate.getParties();
                            Party[] modifiedParties = new Party[parties.length + 1];
                            int index = 0;
                            for (Party party : parties) {
                                modifiedParties[index] = party;
                                index++;
                                if (party.getAgreementRole() != null && party.getAgreementRole().equals(sla.provider)) {
                                    if (party.getPropertyValue(sla.gslam_epr) != null) {
                                        LOGGER.info("********** TestActivator: Provide party : "
                                                + party.getPropertyValue(org.slasoi.slamodel.vocab.sla.gslam_epr));
                                    }
                                    else {
                                        System.out
                                                .println("********** TestActivator: Provide party was NOT set! Setting gslam_epr = "
                                                        + "http://localhost:8080/services/SWNegotiation?wsdl");
                                        party.setPropertyValue(sla.gslam_epr,
                                                "http://localhost:8080/services/SWNegotiation?wsdl");
                                    }
                                }
                                else {
                                    continue;
                                }
                            }
                            Party customerParty = new Party(new ID("ORC_SW_Service-user"), sla.customer);
                            customerParty.setPropertyValue(sla.gslam_epr, "http://customer.client.bundle");
                            modifiedParties[index] = customerParty;
                            softwareSLATemplate.setParties(modifiedParties);
                        }
                    }
                }
                //3 SLAM Scenario : BZSLAM is to be in slaManagerContexts[2]. SWSLAM and IFSLAM could be in other index locations.
                else if (slaManagerContexts != null && slaManagerContexts.length == 3) {
                    LOGGER.info("***  TestActivator: slaManagerContexts[2]\n" + slaManagerContexts[2].getSLAManagerID());
                    sc =
                            ((Hashtable<ISyntaxConverter.SyntaxConverterType, ISyntaxConverter>) slaManagerContexts[2]
                                    .getSyntaxConverters())
                                    .get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter);

                    slatr = slaManagerContexts[2].getSLATemplateRegistry();
                    bizINegotiation = sc.getNegotiationClient("http://localhost:8080/services/BZNegotiation?wsdl");
                }
            }
        }
        catch (GenericSLAManagerUtilsException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        catch (SLAManagerContextException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLATemplateRegistry.Exception e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (java.lang.Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private SLATemplate getInfSLATemplate(String filePath) {
        SLATemplate infSLATemplate = null;
        String infSLATemplateXml = null;
        File file = new File(filePath);
        String fileName = file.getName().substring(0, file.getName().lastIndexOf(".xml"));
        try {
            infSLATemplateXml = FileUtils.readFileToString(file);
            LOGGER.info("************** CONTACTING SYNTAX CONVERTER *****************");
            infSLATemplate = (SLATemplate) sc.parseSLATemplate(infSLATemplateXml);
            LOGGER.info("************** OBTAINED INF-SLATemplate = " + infSLATemplate);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (SLATemplateRegistry.Exception e) {
            e.printStackTrace();
        }
        catch (java.lang.Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return infSLATemplate;
    }

	public void insertTempTesting() {
		try {
            String filePath =
                    SLASOI_ORC_HOME + System.getProperty("file.separator") + "I_SLATs"
                            + System.getProperty("file.separator") + "ORCInfrastructure_SLA_template.xml";//"ORCInfrastructure_SLATemplateGold.xml";
            LOGGER.info("************ filePath = " + filePath);
            SLATemplate infSLATemplate = getInfSLATemplate(filePath);
			Metadata metaData = new Metadata();
            metaData.setPropertyValue(Metadata.provider_uuid, infSLATemplate.getUuid().getValue());
            metaData.setPropertyValue(Metadata.registrar_id, "MockRegistrar123");
            metaData.setPropertyValue(Metadata.service_type, "b2c3f591-f2ca-42b3-92a0-955ba2b36035");
            slatr.addSLATemplate(infSLATemplate, metaData);
        }
		catch (SLATemplateRegistry.Exception e) {
            e.printStackTrace();
        }
	}
	
    public void deleteTempTesting() {
        UUID uuid = new UUID("ORC_InfrastructureSLATGold");
        try {
            LOGGER.info("\n****** Removing Template From SLATR with UUID = "+uuid.getValue());
            slatr.removeSLATemplate(uuid);
        }
        catch (SLATemplateRegistry.Exception e) {
            e.printStackTrace();
        }
    }
    
    public void retrieveBizTemplate() {
        LOGGER.info("\n****** Inside retrieveBizTemplate() ****** ");
        UUID uuid = new UUID("ORCBUSINESS_SW_SLAT1");
        try {
            this.businessSLATemplate = slatr.getSLATemplate(uuid);
            
            if(this.businessSLATemplate != null){
                LOGGER.info("\n****** Template Available In SLATR ****** \n"+this.businessSLATemplate.toString());
                LOGGER.info("\n****** Adding Client Party information ******");
                  Party[] parties = this.businessSLATemplate.getParties();
                  Party[] modifiedParties = new Party[parties.length + 1];
                  int index = 0;
                  
                  for (Party party : parties) {
                      modifiedParties[index] = party;
                      index++;
                  }
                  //Setting BSLAM as customer of SWSLAM.
                  Party customerParty = new Party(new ID("ORC_SW_Service-user"), sla.customer);
                  customerParty.setPropertyValue(sla.gslam_epr,"http://localhost:8080/services/BZNegotiation?wsdl");
                  modifiedParties[index] = customerParty;
                  this.businessSLATemplate.setParties(modifiedParties);
            }
            else {
                LOGGER.info("\n****** Template Was Not Available In SLATR Anymore ****** ");
            }
        }
        catch (SLATemplateRegistry.Exception e) {
            e.printStackTrace();
        }
    }
	
    public void incTesting() {
        try {
            LOGGER.info("********************  TestActivator: Before calling initiateNegotiation");
            String negotiationID = iNegotiation.initiateNegotiation(softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling initiateNegotiation. NegID = "
                    + negotiationID);
            LOGGER.info("********************  TestActivator: Before calling negotiate-1");
            SLATemplate slaTemplates[] = iNegotiation.negotiate(negotiationID, softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                    + slaTemplates.length);
            LOGGER.info("********************  TestActivator: Before calling createAgreement");
            SLA sla = iNegotiation.createAgreement(negotiationID, slaTemplates[0]);
            LOGGER.info("********************  TestActivator: After calling createAgreement. SLA.AgreedAt = "
                    + sla.getAgreedAt() + " SLA.EffectiveFrom = " + sla.getEffectiveFrom() + "SLA.EffectiveUntil = "
                    + sla.getEffectiveUntil());
        }

        catch (OperationNotPossibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (OperationInProgressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvalidNegotiationIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLACreationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void bincTesting() {
        LOGGER.info("******  TestActivator: Inside bincTesting() *********");
        retrieveBizTemplate();
        LOGGER.info("******  Template retrieved from BSLAM.SLATR = *********\n"+this.businessSLATemplate.toString());
        try {
            if(bizINegotiation != null){
                LOGGER.info("********************  TestActivator: Before calling initiateNegotiation");
                String negotiationID = bizINegotiation.initiateNegotiation(this.businessSLATemplate);
                LOGGER.info("********************  TestActivator: After calling initiateNegotiation. NegID = "
                        + negotiationID);
                LOGGER.info("********************  TestActivator: Before calling negotiate-1");
                SLATemplate slaTemplates[] = bizINegotiation.negotiate(negotiationID, this.businessSLATemplate);
                LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                        + slaTemplates.length);
                LOGGER.info("********************  TestActivator: Before calling createAgreement");
                SLA sla = bizINegotiation.createAgreement(negotiationID, slaTemplates[0]);
                LOGGER.info("********************  TestActivator: After calling createAgreement. SLA.AgreedAt = "
                        + sla.getAgreedAt() + " SLA.EffectiveFrom = " + sla.getEffectiveFrom() + "SLA.EffectiveUntil = "
                        + sla.getEffectiveUntil());  
            }
            else {
                LOGGER.info("**** TestActivator: BizINegotiation interface is not initialized ****");
            }
        }
        catch (OperationNotPossibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (OperationInProgressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvalidNegotiationIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLACreationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public void violateMaxHopTesting() {
        try {
            LOGGER.info("********************  TestActivator: Before calling initiateNegotiation");
            String negotiationID = iNegotiation.initiateNegotiation(softwareSLATemplate);
            System.out.println("********************  TestActivator: After calling initiateNegotiation. NegID = "
                    + negotiationID);
            // 1
            LOGGER.info("********************  TestActivator: Before calling negotiate-1");
            SLATemplate slaTemplates[] = iNegotiation.negotiate(negotiationID, softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                    + slaTemplates.length);

            // 2
            slaTemplates = iNegotiation.negotiate(negotiationID, softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-2. Counter-Offers returned = "
                    + slaTemplates.length);
            // 3
            slaTemplates = iNegotiation.negotiate(negotiationID, softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-3. Counter-Offers returned = "
                    + slaTemplates.length);
            // 4 : Should break
            slaTemplates = iNegotiation.negotiate(negotiationID, softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-4. Counter-Offers returned = "
                    + slaTemplates.length);

            LOGGER.info("********************  TestActivator: Before calling createAgreement");
            SLA sla = iNegotiation.createAgreement(negotiationID, slaTemplates[0]);
            LOGGER.info("********************  TestActivator: After calling createAgreement. SLA.AgreedAt = "
                    + sla.getAgreedAt() + " SLA.EffectiveFrom = " + sla.getEffectiveFrom() + "SLA.EffectiveUntil = "
                    + sla.getEffectiveUntil());
        }
        catch (OperationNotPossibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (OperationInProgressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvalidNegotiationIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLACreationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void wrongNegIdTesting() {
        try {
            LOGGER.info("********************  TestActivator: Before calling initiateNegotiation");
            String negotiationID = iNegotiation.initiateNegotiation(softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling initiateNegotiation. NegID = "
                    + negotiationID);
            LOGGER.info("********************  TestActivator: Before calling negotiate-1 with manipulated NegID");
            SLATemplate slaTemplates[] =
                    iNegotiation.negotiate(negotiationID + "kjgdf865df-342343", softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                    + slaTemplates.length);
            LOGGER.info("********************  TestActivator: Before calling createAgreement");
            SLA sla = iNegotiation.createAgreement(negotiationID, slaTemplates[0]);
            LOGGER.info("********************  TestActivator: After calling createAgreement. SLA.AgreedAt = "
                    + sla.getAgreedAt() + " SLA.EffectiveFrom = " + sla.getEffectiveFrom() + "SLA.EffectiveUntil = "
                    + sla.getEffectiveUntil());
        }
        catch (OperationNotPossibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (OperationInProgressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvalidNegotiationIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLACreationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void wrongSLATemplateINCTesting() {
        try {
            LOGGER.info("********************  TestActivator: Before calling initiateNegotiation");
            String negotiationID = iNegotiation.initiateNegotiation(softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling initiateNegotiation. NegID = "
                    + negotiationID);

            String filePath =
                    SLASOI_ORC_HOME + System.getProperty("file.separator") + "I_SLATs"
                            + System.getProperty("file.separator") + "ORCInfrastructure_SLATemplateGold.xml";
            LOGGER.info("************ filePath = " + filePath);
            SLATemplate infSLATemplate = getInfSLATemplate(filePath);

            LOGGER.info("********************  TestActivator: Before calling negotiate-1");
            SLATemplate slaTemplates[] = iNegotiation.negotiate(negotiationID, infSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                    + slaTemplates.length);
            LOGGER.info("********************  TestActivator: Before calling createAgreement");
            SLA sla = iNegotiation.createAgreement(negotiationID, slaTemplates[0]);
            LOGGER.info("********************  TestActivator: After calling createAgreement. SLA.AgreedAt = "
                    + sla.getAgreedAt() + " SLA.EffectiveFrom = " + sla.getEffectiveFrom() + "SLA.EffectiveUntil = "
                    + sla.getEffectiveUntil());
        }
        catch (OperationNotPossibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (OperationInProgressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvalidNegotiationIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLACreationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void wrongSLATemplateRNCTesting() {
        try {
            LOGGER.info("********************  TestActivator: Before calling initiateNegotiation");
            String negotiationID = iNegotiation.initiateNegotiation(softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling initiateNegotiation. NegID = "
                    + negotiationID);
            LOGGER.info("********************  TestActivator: Before calling negotiate-1");
            SLATemplate slaTemplates[] = iNegotiation.negotiate(negotiationID, softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                    + slaTemplates.length);
            LOGGER.info("********************  TestActivator: Before calling createAgreement");
            SLA sla = iNegotiation.createAgreement(negotiationID, slaTemplates[0]);
            LOGGER.info("********************  TestActivator: After calling createAgreement. SLA.UUID = "
                    + sla.getUuid() + "SLA.AgreedAt = " + sla.getAgreedAt() + " SLA.EffectiveFrom = "
                    + sla.getEffectiveFrom() + "SLA.EffectiveUntil = " + sla.getEffectiveUntil() + "\n\n\n\n");

            // Renegotiate
            LOGGER.info("********************  TestActivator: Before calling renegotiate");
            String reNegotiationID = iNegotiation.renegotiate(sla.getUuid());
            LOGGER.info("********************  TestActivator: After calling renegotiate. ReNegID = " + reNegotiationID);

            // Loading wrong SLATemplate:
            String filePath =
                    SLASOI_ORC_HOME + System.getProperty("file.separator") + "I_SLATs"
                            + System.getProperty("file.separator") + "ORCInfrastructure_SLATemplateGold.xml";
            LOGGER.info("************ filePath = " + filePath);
            SLATemplate infSLATemplate = getInfSLATemplate(filePath);

            // Passing wrong SLATemplate while renegotiating:
            LOGGER.info("********************  TestActivator: Before calling negotiate-1");
            slaTemplates = iNegotiation.negotiate(reNegotiationID, infSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                    + slaTemplates.length);
            LOGGER.info("********************  TestActivator: Before calling createAgreement");
            SLA renegotiatedSLA = iNegotiation.createAgreement(reNegotiationID, slaTemplates[0]);
            LOGGER
                    .info("********************  TestActivator: After calling createAgreement. RenegotiatedSLA.AgreedAt = "
                            + renegotiatedSLA.getAgreedAt()
                            + " RenegotiatedSLA.EffectiveFrom = "
                            + renegotiatedSLA.getEffectiveFrom()
                            + "RenegotiatedSLA.EffectiveUntil = "
                            + renegotiatedSLA.getEffectiveUntil());
        }
        catch (OperationNotPossibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (OperationInProgressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvalidNegotiationIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLACreationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLANotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void firstAndFinalOfferTesting() {
        try {
            LOGGER.info("********************  TestActivator: Before calling initiateNegotiation");
            String negotiationID = iNegotiation.initiateNegotiation(softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling initiateNegotiation. NegID = "
                    + negotiationID);

            // No negotiate() called
            LOGGER.info("********************  TestActivator: Before calling createAgreement");
            SLA sla = iNegotiation.createAgreement(negotiationID, softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling createAgreement. SLA.AgreedAt = "
                    + sla.getAgreedAt() + " SLA.EffectiveFrom = " + sla.getEffectiveFrom() + "SLA.EffectiveUntil = "
                    + sla.getEffectiveUntil());
        }
        catch (OperationNotPossibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (OperationInProgressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLACreationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvalidNegotiationIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void missingEPRTesting() {
        try {
            softwareSLATemplate = (SLATemplate) sc.parseSLATemplate(softwareSLATemplateXml); // Has no EPRs set in it
            LOGGER.info("********************  TestActivator: Before calling initiateNegotiation");
            String negotiationID = iNegotiation.initiateNegotiation(softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling initiateNegotiation. NegID = "
                    + negotiationID);
        }
        catch (SLATemplateRegistry.Exception e) {
            e.printStackTrace();
        }
        catch (java.lang.Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void controlProtocolTesting() {

    }

    public void cancelNegotiationTesting() {
        try {
            LOGGER.info("********************  TestActivator: Before calling initiateNegotiation");
            String negotiationID = iNegotiation.initiateNegotiation(softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling initiateNegotiation. NegID = "
                    + negotiationID);
            LOGGER.info("********************  TestActivator: Before calling negotiate-1");
            SLATemplate slaTemplates[] = iNegotiation.negotiate(negotiationID, softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                    + slaTemplates.length);

            List<INegotiation.CancellationReason> cancellationReasons =
                    new ArrayList<INegotiation.CancellationReason>();
            cancellationReasons.add(INegotiation.CancellationReason.STATUS_COMPROMISED);
            cancellationReasons.add(INegotiation.CancellationReason.WILL_RETURN_LATER);

            LOGGER.info("********************  TestActivator: Before calling cancelNegotiation");
            boolean result = iNegotiation.cancelNegotiation(negotiationID, cancellationReasons);
            LOGGER.info("********************  TestActivator: After calling cancelNegotiation. Result = " + result);
        }
        catch (OperationNotPossibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (OperationInProgressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvalidNegotiationIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void terminateAgreementTesting() {
        try {
            // INC:
            LOGGER.info("********************  TestActivator: Before calling initiateNegotiation");
            String negotiationID = iNegotiation.initiateNegotiation(softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling initiateNegotiation. NegID = "
                    + negotiationID);
            LOGGER.info("********************  TestActivator: Before calling negotiate-1");
            SLATemplate slaTemplates[] = iNegotiation.negotiate(negotiationID, softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                    + slaTemplates.length);
            LOGGER.info("********************  TestActivator: Before calling createAgreement");
            SLA sla = iNegotiation.createAgreement(negotiationID, slaTemplates[0]);
            LOGGER.info("********************  TestActivator: After calling createAgreement. SLA.AgreedAt = "
                    + sla.getAgreedAt() + " SLA.EffectiveFrom = " + sla.getEffectiveFrom() + "SLA.EffectiveUntil = "
                    + sla.getEffectiveUntil());

            List<TerminationReason> terminationReasons = new ArrayList<TerminationReason>();
            terminationReasons.add(TerminationReason.CUSTOMER_INITIATED); // mandatory to set!
            terminationReasons.add(TerminationReason.UNSATISFACTORY_QUALITY);

            LOGGER.info("********************  TestActivator: Before calling terminate");
            boolean result = iNegotiation.terminate(sla.getUuid(), terminationReasons);
            LOGGER.info("********************  TestActivator: After calling terminate - Result = " + result);
        }
        catch (OperationNotPossibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (OperationInProgressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvalidNegotiationIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLACreationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLANotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void renegotiateTesting() {
        try {
            LOGGER.info("********************  TestActivator: Before calling initiateNegotiation");
            String negotiationID = iNegotiation.initiateNegotiation(softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling initiateNegotiation. NegID = "
                    + negotiationID);
            LOGGER.info("********************  TestActivator: Before calling negotiate-1");
            SLATemplate slaTemplates[] = iNegotiation.negotiate(negotiationID, softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                    + slaTemplates.length);
            LOGGER.info("********************  TestActivator: Before calling createAgreement");
            SLA sla = iNegotiation.createAgreement(negotiationID, slaTemplates[0]);
            LOGGER.info("********************  TestActivator: After calling createAgreement. SLA.UUID = "
                    + sla.getUuid() + "SLA.AgreedAt = " + sla.getAgreedAt() + " SLA.EffectiveFrom = "
                    + sla.getEffectiveFrom() + "SLA.EffectiveUntil = " + sla.getEffectiveUntil() + "\n\n\n\n");

            // Renegotiate
            LOGGER.info("********************  TestActivator: Before calling renegotiate");
            String reNegotiationID = iNegotiation.renegotiate(sla.getUuid());
            LOGGER.info("********************  TestActivator: After calling renegotiate. ReNegID = " + reNegotiationID);

            LOGGER.info("********************  TestActivator: Before calling negotiate-1");
            slaTemplates = iNegotiation.negotiate(reNegotiationID, softwareSLATemplate);
            LOGGER.info("********************  TestActivator: After calling negotiate-1. Counter-Offers returned = "
                    + slaTemplates.length);
            LOGGER.info("********************  TestActivator: Before calling createAgreement");
            SLA renegotiatedSLA = iNegotiation.createAgreement(reNegotiationID, slaTemplates[0]);
            LOGGER
                    .info("********************  TestActivator: After calling createAgreement. RenegotiatedSLA.AgreedAt = "
                            + renegotiatedSLA.getAgreedAt()
                            + " RenegotiatedSLA.EffectiveFrom = "
                            + renegotiatedSLA.getEffectiveFrom()
                            + "RenegotiatedSLA.EffectiveUntil = "
                            + renegotiatedSLA.getEffectiveUntil());
        }
        catch (OperationNotPossibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (OperationInProgressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InvalidNegotiationIDException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLACreationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SLANotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public class Commander {

        public void context() {
            LOGGER.info("context");
            SLAManagerContext[] slaManagerContexts;
            try {
                slaManagerContexts = genericSLAManagerUtils.getContextSet("GLOBAL");
                int i = 0;
                for (SLAManagerContext ctx : slaManagerContexts) {
                    LOGGER.info("\nidx" + i++ + "--------------------------------------------");
                    LOGGER.info(ctx);
                    }
            }
            catch (GenericSLAManagerUtilsException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        public void peTest1() {
            LOGGER.info("Initiate - peTest1()");
            tempSetup1();
        }

        public void peTest2() {
            LOGGER.info("Initiate - peTest2()");
            tempSetup2();
        }
        /**
         * Usage: echo {bundle-Id} incTest
         */
        public void incTest() {
            LOGGER.info("Initiate->Negotiate->CreateAgreement - incTest()");
            setup();
            incTesting();
        }
        
        public void bincTest() {
            LOGGER.info("Initiate->Negotiate->CreateAgreement - bincTest()");
            setup();
            bincTesting();
        }
        
        public void renegotiateTest() {
            LOGGER.info("INC->Renegotiate->Negotiate->CreateAgreement - renegotiateTest()");
            setup();
            renegotiateTesting();
        }

        public void wrongNegIdTest() {
            LOGGER.info("Initiate->Negotiate{WrongNegID}->CreateAgreement - wrongNegIdTest()");
            setup();
            wrongNegIdTesting();
            /*
             * Gives: org.slasoi.gslam.core.negotiation.INegotiation$InvalidNegotiationIDException: Negotiation ID does
             * not exist. at org.slasoi.gslam.protocolengine.impl.MessageHandler.negotiate(MessageHandler.java:292)
             */
        }

        public void missingEPRTest() {
            LOGGER.info("Initiate->Negotiate->CreateAgreement - missingEPRTest()");
            setup();
            missingEPRTesting();
            /*
             * Gives: Caused by: java.lang.AssertionError: Provider GSLAM_EPR not specified at
             * org.slasoi.gslam.protocolengine.impl.NegotiationManager.initiateNegotiation(NegotiationManager.java:88)
             */
        }

        public void wrongSLATemplateINCTest() {
            LOGGER.info("Initiate->Negotiate{WrongSLAT}->CreateAgreement - wrongSLATemplateINCTest()");
            setup();
            wrongSLATemplateINCTesting();
            /*
             * org.slasoi.gslam.core.negotiation.INegotiation$OperationNotPossibleException: SLATemplate not recognized
             * by current negotiation session. at
             * org.slasoi.gslam.protocolengine.impl.NegotiationManager.negotiate(NegotiationManager.java:346)
             */
        }

        public void wrongSLATemplateRNCTest() {
            LOGGER.info("INC->Renegotiate->Negotiate{WrongSLAT}->CreateAgreement - wrongSLATemplateRNCTest()");
            setup();
            wrongSLATemplateRNCTesting();
            /*
             * org.slasoi.gslam.core.negotiation.INegotiation$OperationNotPossibleException: SLATemplate not recognized
             * by current negotiation session. at
             * org.slasoi.gslam.protocolengine.impl.NegotiationManager.negotiate(NegotiationManager.java:346)
             */
        }

        public void cancelNegotiationTest() {
            LOGGER.info("Initiate->Negotiate->CancelNegotiation{TwoReasons} - cancelNegotiationTest()");
            setup();
            cancelNegotiationTesting();
        }

        public void violateMaxHopTest() {
            LOGGER.info("Initiate->Negotiate*4->CreateAgreement - violateMaxHopTest()");
            setup();
            violateMaxHopTesting();
            /*
             * Gives: Caused by: java.lang.AssertionError: The Operation was not allowed by the Negotiation Protocol at
             * Provider side. at
             * org.slasoi.gslam.protocolengine.impl.NegotiationManager.negotiate(NegotiationManager.java:277)
             */
        }

        public void firstAndFinalOfferTest() {
            LOGGER.info("Initiate->CreateAgreement - firstAndFinalOfferTest()");
            setup();
            firstAndFinalOfferTesting();
            /*
             * Gives: Caused by: java.lang.AssertionError: The Operation was not allowed by the Negotiation Protocol at
             * Provider side. at
             * org.slasoi.gslam.protocolengine.impl.NegotiationManager.createAgreement(NegotiationManager.java:417)
             */
        }

        public void terminateAgreementTest() {
            LOGGER.info("Initiate->Negotiate->CreateAgreement->TerminateAgreement - terminateAgreementTest()");
            setup();
            terminateAgreementTesting();
        }

        public void controlProtocolTest() {
            LOGGER.info("Retrieve and Configure Protocol Policies - controlProtocolTest()");
            setup();
            controlProtocolTesting();
        }
		
		public void insertTest() {
            LOGGER.info("Insert Inf Template in SLATR - insertTest()");
            setup();
            insertTempTesting();
        }
        public void deleteTest() {
            LOGGER.info("Delete Inf Template from SLATR - deleteTest()");
            setup();
            deleteTempTesting();
        }		
        public void retrieveBizTempTest() {
            LOGGER.info("Retrieve Biz Template from SLATR - retrieveBizTempTest()");
            setup();
            retrieveBizTemplate();
        }
    }
}
