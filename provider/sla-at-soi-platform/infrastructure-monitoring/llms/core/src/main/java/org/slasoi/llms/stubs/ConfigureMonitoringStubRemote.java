/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/stubs/ConfigureMonitoringStubRemote.java $
 */

package org.slasoi.llms.stubs;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.llms.interfaces.IConfigureMonitoring;
import org.slasoi.llms.manager.MonitoringProvisioningException;
import org.slasoi.llms.manager.MonitoringRequest;
import org.slasoi.llms.rpc.PubSubRPC;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ConfigureMonitoringStubRemote implements IConfigureMonitoring {
    private static Logger log = Logger.getLogger(ConfigureMonitoringStubRemote.class);

    public static String channelName = "USERCMP_TO_LLMS";

    PubSubRPC pubSubRPC;

    public ConfigureMonitoringStubRemote(PubSubRPC pubSubRPC) throws MessagingException, FileNotFoundException, IOException {
        super();
        this.pubSubRPC = pubSubRPC;
        System.out.println("---------------------------- ConfigureMonitoringStubRemote initialized -------------------------------");
    }


    /**
     * Provisioned monitoring mechanisms/capabilites. It TODO
     *
     * @param request The monitoring request.
     * @return requestID
     */
    public Object provision(MonitoringRequest request) throws MonitoringProvisioningException {
        try {
            JSONObject args = new JSONObject();
            args.put("command", "provision"); // obligatory for each command
            args.put("request", request.toJSON());

            JSONObject retObj = pubSubRPC.sendCommand(args, true);
            assert (retObj.has("retval"));
            return retObj.getInt("retval");

        }
        catch (Exception e) {
            log.error(e);
            return null;
        }
//		return Manager.getInstance().provision(request);
    }

    /**
     * Provisioned monitoring mechanisms/capabilites from XML request.
     *
     * @param requestXML The monitoring request in XML format (see MonitoringProvisioning.xml example).
     * @return requestID
     */
    public Object provision(String requestXML) throws MonitoringProvisioningException {
        return provision(MonitoringRequest.createFromXML(requestXML));
    }

    public Object reprovision(Object requestID, MonitoringRequest newRequest) throws MonitoringProvisioningException {
        return null; // TODO implement
//		return Manager.getInstance().reprovision(requestID, newRequest);
    }

    public void free(Object requestID) throws MonitoringProvisioningException {
        return; // TODO implement
//		Manager.getInstance().free(requestID);
	}
}
