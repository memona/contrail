/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/manager/Manager.java $
 */

package org.slasoi.llms.manager;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.llms.common.*;
import org.slasoi.llms.db.DBManager;
import org.slasoi.llms.interfaces.IConfigureMonitoring;
import org.slasoi.llms.interfaces.IMonitoringDataAndHistory;
import org.slasoi.llms.interfaces.IServiceInstance2Monitoring;
import org.slasoi.llms.interfaces.MetricNotificationListener.MetricNotificationType;
import org.slasoi.llms.manager.MetricRegistry.RegistrationStatus;

import javax.sql.RowSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

public class Manager implements IConfigureMonitoring, IMonitoringDataAndHistory, IServiceInstance2Monitoring {
    private static final Logger log = Logger.getLogger(Manager.class);

    DBManager db;        // persistency support
    PubSubManager pubSubManager;

    MetricRegistry registry = new MetricRegistry();
    MonitoringStore store = new MonitoringStore(this, registry);
//	DroolsGateway 		reasoningEngine = new DroolsGateway();		// disabled in Y2

    // RPC tables
    Hashtable<Integer, MonitoringRequest> requests = new Hashtable<Integer, MonitoringRequest>();
    int requestsSize = 0;

    Hashtable<String, ArrayList<MetricRequest>> subscribers = new Hashtable<String, ArrayList<MetricRequest>>();


    public Manager(DBManager db, PubSubManager pubSubManager) {
        super();
        this.db = db;
        this.pubSubManager = pubSubManager;

        if (db != null) {
            // load metrics
            ArrayList<MetricEx> metrics = db.loadMetrics();
            if (metrics == null)
                throw new RuntimeException("Error while loading LLMS data from DB.");
            for (MetricEx m : metrics)
                registry.registerMetric(m);

            // load metric observations/values
            ArrayList<Observation> values = db.loadObservations();
            if (values == null)
                throw new RuntimeException("Error while loading LLMS data from DB.");
            for (Observation o : values)
                store.storeMetricObservation(o);
            log.debug("Using DB " + db.getConnStr());
        }
        else
            log.info("Running without DB support.");

        log.debug("Manager set up.");
    }

    public DBManager getDb() {
        return db;
    }

    public PubSubManager getPubSubManager() {
        return pubSubManager;
    }


    // PROVISIONING METHODS   IConfigureMonitoring

    public synchronized Object provision(MonitoringRequest request) throws MonitoringProvisioningException {

        // invoke configuration with requested parameters for all metric sensors
        for (MetricRequest mr : request.getMetrics()) {
            String metricURL = mr.getMetricURL();

            // add subscriber for metric notification
            ArrayList<MetricRequest> s = subscribers.get(metricURL);
            if (s == null) {
                s = new ArrayList<MetricRequest>();
                subscribers.put(metricURL, s);
            }
            s.add(mr);

            MetricEx m = registry.getMetricEx(metricURL);
            if (m == null)
                throw new MonitoringProvisioningException(String.format("metricURL(%s) not yet registered.", metricURL));

            /* ConfigureMetric disabled ***
               ConfigureMetric effector = registry.getMetricEx(metricURL).getEffector();
               if (effector != null)
                   if (!effector.configMetric(metricURL, mr.getParams()))	 // synchronous call
                       throw new MonitoringProvisioningException(String.format("configMetric(%s) failed.", metricURL));
               */
        }

//		// deploy rules		// disabled in Y2
//		for (RulePackage r : request.getRulePackages())
//			if (reasoningEngine.addPackage(r))
//				reasoningEngine.setManager(this); // set global reference to Manager object from rule packages
//			else
//				throw new MonitoringProvisioningException(String.format("Problems in rule package: addPackage(%s,%s) failed.", r.getResource(), r.getType()));

        // store request
        requests.put(Integer.valueOf(requestsSize), request);
        return requestsSize++;
    }

    public synchronized Object reprovision(Object requestID, MonitoringRequest newRequest) throws MonitoringProvisioningException {
        return null; // TODO implement
    }


    public synchronized void free(Object requestID) throws MonitoringProvisioningException {
        MonitoringRequest request = requests.get(requestID);
        if (request == null)
            throw new MonitoringProvisioningException(String.format("requestID %s does not exist", requestID));

//		// remove rules			// disabled in Y2
//		for (RulePackage r : request.getRulePackages())
//			if (r.getId() != null)
//				reasoningEngine.removePackage(r.getId());

        //TODO implement free MIHA
    }

    public synchronized ArrayList<MetricRequest> getSubscribers(String metricURL) {
        return subscribers.get(metricURL);
    }


    //IMonitoringDataAndHistory

    public Metric getMetric(String metricURL) {
        return registry.getMetric(metricURL);
    }

    public ArrayList<Metric> getMatchingURLs(String pattern) {
        return registry.getMatchingURLs(pattern);
    }

    public MetricValue getMetricValue(String metricURL) {
        return store.getMetricValue(metricURL);
    }

    public MetricHistory getMetricValueHistory(String metricURL, Period period) {
        return store.getMetricValueHistory(metricURL, period);
    }

    public ArrayList<RowSet> queryHistory(String query) {
        return store.queryHistory(query);
    }


    // IServiceInstance2Monitoring

    public MetricRegistry.RegistrationStatus registerMetric(MetricEx metric) {
        MetricRegistry.RegistrationStatus ret = registry.registerMetric(metric);
        if (ret == RegistrationStatus.SUCCESS) {
            // save metric to DB
            if (db != null)
                db.saveMetric(metric);    //ignore errors
        }

        return ret;
    }

    public void unregisterMetric(String metricURL) {
        registry.unregisterMetric(metricURL);
    }

    public boolean isRegisteredMetric(String metricURL) {
        return registry.containsMetric(metricURL);
    }

    public void storeMetricObservation(String metricURL, Date time, String value) {
        Observation o = store.storeMetricObservation(metricURL, time, value);

        // save metric observation to DB
        if (db != null)
            db.saveObservation(o);    //ignore errors
    }

    // NOTIFICATIONS
    void sendNotification(String channelName, String metricURL, MetricNotificationType type, Object value) {
        if (pubSubManager != null) {
            String payload = null;
            // Create new message and publish it.
            JSONObject o = new JSONObject();
            try {
                o.put("type", type);
                o.put("metricURL", metricURL);

                if (type == MetricNotificationType.GENERIC)
                    o.put("value", (String) value);
                else
                    o.put("value", ((Observation) value).toJSON());

                payload = o.toString();
                PubSubMessage message = new PubSubMessage(channelName, payload);

                pubSubManager.publish(message);
                log.debug(String.format("Notification (channel %s) sent: %s", channelName, payload));

            }
            catch (JSONException e) {
                log.error(e);
            }
            catch (MessagingException e) {
                log.error(channelName + " Error when publishing msg: " + payload);
            }
        }
    }

    public void sendMetricNotification(String channelName, String metricURL, MetricNotificationType type, Observation value) {
        sendNotification(channelName, metricURL, type, value);
    }

    public void sendGenericNotification(String channelName, String eventMetricURL, Object... args) {
        JSONArray arr = new JSONArray();
        for (Object arg : args)
            arr.put(arg);

        sendNotification(channelName, eventMetricURL, MetricNotificationType.GENERIC, arr.toString());
    }

    // OSGi test function from IMonitoringDataAndHistory
    public String test() {
        // TODO Auto-generated method stub
        return null;
    }

}
