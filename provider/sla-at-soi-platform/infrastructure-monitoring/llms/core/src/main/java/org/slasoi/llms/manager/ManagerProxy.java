/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/manager/ManagerProxy.java $
 */

package org.slasoi.llms.manager;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.llms.common.*;
import org.slasoi.llms.rpc.CommandHandler;
import org.slasoi.llms.rpc.PubSubRPC;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;


public class ManagerProxy {
    private static Logger log = Logger.getLogger(ManagerProxy.class);

    final String sensorChannelName = "SENSORS_TO_LLMS";
    final String UserComponentChannelName = "USERCMP_TO_LLMS";

    Manager manager;


    public ManagerProxy(Manager manager) throws MessagingException, FileNotFoundException, IOException {
        super();
        this.manager = manager;

        setupSensorChannel(manager.getPubSubManager());
        setupUserComponentChannel(manager.getPubSubManager());
        log.debug("ManagerProxy set up.");
    }

    public Manager getManager() {
        return manager;
    }


    void setupSensorChannel(PubSubManager pubSubManager) throws FileNotFoundException, MessagingException, IOException {
        // Create PubSubRPC instances. Make sure properties files are set.
        PubSubRPC pubSubRPC = new PubSubRPC(pubSubManager, sensorChannelName, true);

        // REGISTER COMMAND HANDLERS
        addIServiceInstance2Monitoring(pubSubRPC);
    }

    void setupUserComponentChannel(PubSubManager pubSubManager) throws FileNotFoundException, MessagingException, IOException {
        // Create PubSubRPC instances. Make sure properties files are set.
        PubSubRPC pubSubRPC = new PubSubRPC(pubSubManager, UserComponentChannelName, true);

        // REGISTER COMMAND HANDLERS
        addIServiceInstance2Monitoring(pubSubRPC);
        addIMonitoringDataAndHistory(pubSubRPC);
        addIConfigureMonitoring(pubSubRPC);
    }

    void addIServiceInstance2Monitoring(PubSubRPC pubSubRPC) {
        pubSubRPC.addCommandHandler("registerMetric", new CommandHandler() {
            public Object processCommand(String command, JSONObject o) throws Exception {
                Metric metric = new Metric(o.getJSONObject("metric"));
                MetricRegistry.RegistrationStatus retval = manager.registerMetric(new MetricEx(metric));

                return retval;     //send return message
            }
        });

        pubSubRPC.addCommandHandler("unregisterMetric", new CommandHandler() {
            public Object processCommand(String command, JSONObject o) throws Exception {
                String metricURL = o.getString("metricURL");
                manager.unregisterMetric(metricURL);
                return null;    // no return
            }
        });

        pubSubRPC.addCommandHandler("isRegisteredMetric", new CommandHandler() {
            public Object processCommand(String command, JSONObject o) throws Exception {
                String metricURL = o.getString("metricURL");
                boolean ret = manager.isRegisteredMetric(metricURL);
                return ret;        // return bool
            }
        });

        pubSubRPC.addCommandHandler("sendObservation", new CommandHandler() {
            public Object processCommand(String command, JSONObject o) throws Exception {
                String metricURL = o.getString("metricURL");
                Date time = new Date(o.getLong("time"));
                String value = o.getString("value");
                manager.storeMetricObservation(metricURL, time, value);
                return null;    // no return
            }
        });
    }

    void addIMonitoringDataAndHistory(PubSubRPC pubSubRPC) {
        pubSubRPC.addCommandHandler("getMetric", new CommandHandler() {
            public Object processCommand(String command, JSONObject o) throws Exception {
                String metricURL = o.getString("metricURL");

                Metric retval = manager.getMetric(metricURL);
                return ((retval != null) ? retval.toJSON() : null);        // JSON return
            }
        });

        pubSubRPC.addCommandHandler("getMatchingURLs", new CommandHandler() {
            public Object processCommand(String command, JSONObject o) throws Exception {
                String pattern = o.getString("pattern");

                ArrayList<Metric> retArr = manager.getMatchingURLs(pattern);
                assert (retArr != null);
                JSONArray retval = new JSONArray();
                for (Metric m : retArr)
                    retval.put(m.toJSON());
                return retval;        // JSON return
            }
        });

        pubSubRPC.addCommandHandler("getMetricValue", new CommandHandler() {
            public Object processCommand(String command, JSONObject o) throws Exception {
                String metricURL = o.getString("metricURL");

                MetricValue retval = manager.getMetricValue(metricURL);
                return ((retval != null) ? retval.toJSON() : null);        // JSON return
            }
        });

        pubSubRPC.addCommandHandler("getMetricValueHistory", new CommandHandler() {
            public Object processCommand(String command, JSONObject o) throws Exception {
                String metricURL = o.getString("metricURL");
                Period period = new Period(o.getJSONObject("period"));

                MetricHistory retval = manager.getMetricValueHistory(metricURL, period);
                return ((retval != null) ? retval.toJSON() : null);        // JSON return
            }
        });
    }

    void addIConfigureMonitoring(PubSubRPC pubSubRPC) {
        pubSubRPC.addCommandHandler("provision", new CommandHandler() {
            public Object processCommand(String command, JSONObject o) throws Exception {
                MonitoringRequest request = new MonitoringRequest(o.getJSONObject("request"));

                return manager.provision(request);    // return Object (int)
            }
        });
    }

}
