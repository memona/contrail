/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/common/Metric.java $
 */

package org.slasoi.llms.common;

import org.json.JSONException;
import org.json.JSONObject;

public class Metric {

    String metricURL;
    MetricType metricType;

    public Metric(String metricURL, MetricType metricType) {
        super();
        this.metricURL = metricURL;
        this.metricType = metricType;
    }

    public Metric(JSONObject o) throws JSONException, ClassNotFoundException {
        super();
        this.metricURL = o.getString("metricURL");
        this.metricType = new MetricType(o.getJSONObject("metricType"));
    }

    public JSONObject toJSON() {
        try {
            JSONObject o = new JSONObject();
            o.put("metricURL", metricURL);
            o.put("metricType", metricType.toJSON());

            return o;

        }
        catch (JSONException e) {
            return null;
        }
    }

    public String getMetricURL() {
        return metricURL;
    }

    public void setMetricURL(String metricURL) {
        this.metricURL = metricURL;
    }

    public MetricType getMetricType() {
        return metricType;
    }

    public void setMetricType(MetricType metricType) {
        this.metricType = metricType;
    }


    /**
     * Static helper method for Metric URL creation.
     */
    public static String createMetricURL(String serviceInstanceURL, MetricType.MetricClass metricClass, String metricName) {
        switch (metricClass) {
            case FLUENT:
                return serviceInstanceURL + "/fluent/" + metricName;
            case EVENT:
                return serviceInstanceURL + "/event/" + metricName;
            case ALARM:
                return serviceInstanceURL + "/alarm/" + metricName;
            default:
                return null;
        }

    }
}