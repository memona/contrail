/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/main/java/org/slasoi/llms/common/RulePackage.java $
 */

package org.slasoi.llms.common;

import org.apache.log4j.Logger;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Reader;

public class RulePackage {
    private static Logger log = Logger.getLogger(RulePackage.class);

    Resource resource;    // from file, String, byte[], etc.
    ResourceType type;    // DRL, PKG, BRL,
    private String id = null;

    RulePackage(Resource resource, ResourceType type) {
        super();
        this.resource = resource;
        this.type = type;
    }

    public RulePackage(JSONObject o) throws JSONException, ClassNotFoundException {
        super();
        this.resource = ResourceFactory.newByteArrayResource(o.getString("resource").getBytes());
        this.type = ResourceType.DRL;
    }

    public JSONObject toJSON() throws JSONException, IOException {
        JSONObject o = new JSONObject();
        o.put("resource", resourcetoString());
        assert (type == ResourceType.DRL);    // now only DRL is supported in remote API
        o.put("type", "DRL");

        return o;
    }

    public static RulePackage createRulePackageFromString(String content) {
        return createRulePackageFromBytes(content.getBytes());
    }

    public static RulePackage createRulePackageFromString(String content, ResourceType type) {
        return createRulePackageFromBytes(content.getBytes(), type);
    }

    public static RulePackage createRulePackageFromBytes(byte[] content) {
        return createRulePackageFromBytes(content, ResourceType.DRL);
    }

    public static RulePackage createRulePackageFromBytes(byte[] content, ResourceType type) {
        return new RulePackage(ResourceFactory.newByteArrayResource(content), type);
    }

    public static RulePackage createRulePackageFromFile(String filePath) {
        return createRulePackageFromFile(filePath, ResourceType.DRL);
    }

    public static RulePackage createRulePackageFromFile(String filePath, ResourceType type) {
        return new RulePackage(ResourceFactory.newFileResource(filePath), type);
    }

    public static RulePackage createRulePackageFromURL(String url) {
        return createRulePackageFromURL(url, ResourceType.DRL);
    }

    public static RulePackage createRulePackageFromURL(String url, ResourceType type) {
        return new RulePackage(ResourceFactory.newUrlResource(url), type);
    }


    public Resource getResource() {
        return resource;
    }

    public ResourceType getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    // to be set by the reasoning engine
    public void setId(String id) {
        this.id = id;
    }

    String resourcetoString() throws IOException {
        try {
            String ret = "";
            char[] buf = new char[1024 * 1024];
            int len;

            Reader reader = resource.getReader();
            while ((len = reader.read(buf)) > 0) {
                ret += new String(buf, 0, len);
            }

            return ret;
        }
        catch (IOException e) {
            log.error(e);
            throw e;
        }
    }

}
