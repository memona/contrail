/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miha Vuk - miha.vuk@xlab.si
 * @version        $Rev: 565 $
 * @lastrevision   $Date: 2011-01-31 16:30:01 +0100 (pon, 31 jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/llms/core/src/test/java/org/slasoi/llms/common/ObservationTest.java $
 */

package org.slasoi.llms.common;

import junit.framework.TestCase;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.Date;

public class ObservationTest extends TestCase {

    @Test
    public void testCreateIntegerObs() throws MetricValueException {
        MetricType cpuMetricType = new MetricType("CPU", "%", MetricType.MetricClass.FLUENT, Integer.class);
        String cpu1MetricURL = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1",
                MetricType.MetricClass.FLUENT, cpuMetricType.getName());
        Metric cpu1 = new Metric(cpu1MetricURL, cpuMetricType);

        Integer observationValue = 75;
        Observation o = new Observation(cpu1, new Date(), observationValue.toString());
        assertEquals(o.getMetric().getMetricURL(), cpu1.getMetricURL());
        assertNotNull(o.getTime());
        assertEquals(o.getIntValue(), observationValue.intValue());

        Date d = new Date();
        o.setMetric(cpu1);
        o.setTime(d);
        o.setValue("80");
        assertEquals(o.getIntValue(), 80);
        assertEquals(o.getTime(), d);
        assertEquals(o.getMetric().getMetricURL(), cpu1.getMetricURL());
    }

    @Test
    public void testCreateDoubleObs() throws MetricValueException {
        MetricType cpuMetricType = new MetricType("CPU", "%", MetricType.MetricClass.FLUENT, Double.class);
        String cpu1MetricURL = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1",
                MetricType.MetricClass.FLUENT, cpuMetricType.getName());
        Metric cpu1 = new Metric(cpu1MetricURL, cpuMetricType);

        Double observationValue = 75.2;
        Observation o = new Observation(cpu1, new Date(), observationValue.toString());
        assertEquals(o.getMetric().getMetricURL(), cpu1.getMetricURL());
        assertNotNull(o.getTime());
        assertEquals(o.getDoubleValue(), observationValue);
    }

    @Test
    public void testCreateBooleanObs() throws MetricValueException {
        MetricType powerStateMetricType = new MetricType("powerState", "", MetricType.MetricClass.EVENT, Boolean.class);
        String powerStateMetricUrl = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1",
                MetricType.MetricClass.EVENT, powerStateMetricType.getName());
        Metric cpu1 = new Metric(powerStateMetricUrl, powerStateMetricType);

        Boolean observationValue = true;
        Observation o = new Observation(cpu1, new Date(), observationValue.toString());
        assertEquals(o.getMetric().getMetricURL(), cpu1.getMetricURL());
        assertNotNull(o.getTime());
        assertEquals(o.getBoolValue(), observationValue.booleanValue());
    }

    @Test
    public void testCreateStringObs() throws MetricValueException {
        MetricType stringMetricType = new MetricType("stringMetric", "", MetricType.MetricClass.EVENT, String.class);
        String stringMetricUrl = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1",
                MetricType.MetricClass.EVENT, stringMetricType.getName());
        Metric stringMetric = new Metric(stringMetricUrl, stringMetricType);
        String observationValue = "value1";
        Observation stringObservation = new Observation(stringMetric, new Date(), observationValue);
        assertEquals(stringObservation.getMetric().getMetricURL(), stringMetric.getMetricURL());
        assertNotNull(stringObservation.getTime());
        assertEquals(stringObservation.getStringValue(), observationValue);
    }

    @Test
    public void testCreateIntegerObsJson() throws MetricValueException, JSONException, ClassNotFoundException {
        MetricType cpuMetricType = new MetricType("CPU", "%", MetricType.MetricClass.FLUENT, Integer.class);
        String cpu1MetricURL = Metric.createMetricURL("slasoi://myManagedObject.company.com/TravelService/VMs/VM1",
                MetricType.MetricClass.FLUENT, cpuMetricType.getName());
        Metric cpu1 = new Metric(cpu1MetricURL, cpuMetricType);

        Integer observationValue = 75;

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("metric", cpu1.toJSON());
        jsonObj.put("time", System.currentTimeMillis());
        jsonObj.put("value", observationValue);

        Observation o = new Observation(jsonObj);
        assertEquals(o.getMetric().getMetricURL(), cpu1.getMetricURL());
        assertNotNull(o.getTime());
        assertEquals(o.getIntValue(), observationValue.intValue());
    }
}
