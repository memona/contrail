/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3050 $
 * @lastrevision $Date: 2011-09-09 14:11:04 +0200 (pet, 09 sep 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/ClusterCpuSpeedUsage. $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue("CLUSTER_CPU_SPEED_USAGE")
public class ClusterCpuSpeedUsage extends Metric {
    private static final Logger log = Logger.getLogger(ClusterCpuSpeedUsage.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        log.trace(String.format("Computing metric CLUSTER_CPU_SPEED_USAGE."));
        String cpuSpeedUsage;
        try {
            double cpuSpeedTotalCapacity = 0;
            double cpuSpeedUsed = 0;
            for (String hostFqdn : monitoringEngine.getAllHostMachines()) {
                // get host CPU speed
                double hostCpuSpeed = (Double) monitoringEngine.getHostMetricData(hostFqdn,
                        RawMetric.CPU_SPEED).getValue();
                int hostNumOfCores = (Integer) monitoringEngine.getHostMetricData(hostFqdn,
                        RawMetric.CPU_CORES_COUNT).getValue();
                cpuSpeedTotalCapacity += hostCpuSpeed * hostNumOfCores;

                // compute used CPU speed by VMs running on the host
                for (String vmFqdn : monitoringEngine.getGuestMachines(hostFqdn)) {
                    double vmCpuSpeedReserved = (Double) monitoringEngine.getVmMetricData(vmFqdn,
                            RawMetric.CPU_SPEED).getValue();
                    int vmNumOfCores = (Integer) monitoringEngine.getVmMetricData(vmFqdn,
                        RawMetric.CPU_CORES_COUNT).getValue();
                    cpuSpeedUsed += vmCpuSpeedReserved * vmNumOfCores;
                }
            }
            cpuSpeedUsage = String.format("%.2f/%.2f", cpuSpeedUsed, cpuSpeedTotalCapacity);
        }
        catch (Exception e) {
            throw new Exception(String.format("Failed to compute metric CLUSTER_CPU_SPEED_USAGE: %s", e.getMessage()));
        }

        log.trace(String.format("CLUSTER_CPU_SPEED_USAGE = %s", cpuSpeedUsage));
        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = storeMetricValue(cpuSpeedUsage, metricsTimestamp);
        storeMetricHistoryValue(cpuSpeedUsage, metricsTimestamp);
    }

    @Override
    public String parseValue(String value) throws InvalidMetricValueException {
        return value;
    }

    @Override
    public String formatValue(Object value) {
        return value.toString();
    }

    @Override
    public Object getViolationThresholdValue() throws InvalidMetricValueException {
        return null;
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        return null;
    }

    @Override
    public Object getWarningThresholdValue() {
        return null;
    }

    @Override
    public String getWarningCondition() {
        return null;
    }

    @Override
    public Object estimateWarningThreshold(Object violationThreshold) {
        return null;
    }
}
