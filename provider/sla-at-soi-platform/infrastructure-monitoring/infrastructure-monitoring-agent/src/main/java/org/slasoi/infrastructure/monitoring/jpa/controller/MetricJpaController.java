/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/controller/MetricJpaController.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.controller;

import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.IllegalOrphanException;
import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.NonexistentEntityException;
import org.slasoi.infrastructure.monitoring.jpa.entities.*;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class MetricJpaController {

    public MetricJpaController() {
        emf = Persistence.createEntityManagerFactory("InfrastructureMonitoringPU");
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Metric metric) {
        if (metric.getViolationList() == null) {
            metric.setViolationList(new ArrayList<Violation>());
        }
        if (metric.getMetricValueHistoryList() == null) {
            metric.setMetricValueHistoryList(new ArrayList<MetricValueHistory>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Vm vm = metric.getVm();
            if (vm != null) {
                vm = em.getReference(vm.getClass(), vm.getVmId());
                metric.setVm(vm);
            }
            Service service = metric.getService();
            if (service != null) {
                service = em.getReference(service.getClass(), service.getServiceId());
                metric.setService(service);
            }
            MetricType metricType = metric.getMetricType();
            if (metricType != null) {
                metricType = em.getReference(metricType.getClass(), metricType.getMetricTypeId());
                metric.setMetricType(metricType);
            }
            MetricValue metricValue = metric.getMetricValue();
            if (metricValue != null) {
                metricValue = em.getReference(metricValue.getClass(), metricValue.getMetricId());
                metric.setMetricValue(metricValue);
            }
            List<Violation> attachedViolationList = new ArrayList<Violation>();
            for (Violation violationListViolationToAttach : metric.getViolationList()) {
                violationListViolationToAttach = em.getReference(violationListViolationToAttach.getClass(), violationListViolationToAttach.getViolationId());
                attachedViolationList.add(violationListViolationToAttach);
            }
            metric.setViolationList(attachedViolationList);
            List<MetricValueHistory> attachedMetricValueHistoryList = new ArrayList<MetricValueHistory>();
            for (MetricValueHistory metricValueHistoryListMetricValueHistoryToAttach : metric.getMetricValueHistoryList()) {
                metricValueHistoryListMetricValueHistoryToAttach = em.getReference(metricValueHistoryListMetricValueHistoryToAttach.getClass(), metricValueHistoryListMetricValueHistoryToAttach.getMetricValueHistoryPK());
                attachedMetricValueHistoryList.add(metricValueHistoryListMetricValueHistoryToAttach);
            }
            metric.setMetricValueHistoryList(attachedMetricValueHistoryList);
            em.persist(metric);
            if (vm != null) {
                vm.getMetricList().add(metric);
                vm = em.merge(vm);
            }
            if (service != null) {
                service.getMetricList().add(metric);
                service = em.merge(service);
            }
            if (metricType != null) {
                metricType.getMetricList().add(metric);
                metricType = em.merge(metricType);
            }
            if (metricValue != null) {
                Metric oldMetricOfMetricValue = metricValue.getMetric();
                if (oldMetricOfMetricValue != null) {
                    oldMetricOfMetricValue.setMetricValue(null);
                    oldMetricOfMetricValue = em.merge(oldMetricOfMetricValue);
                }
                metricValue.setMetric(metric);
                metricValue = em.merge(metricValue);
            }
            for (Violation violationListViolation : metric.getViolationList()) {
                Metric oldMetricOfViolationListViolation = violationListViolation.getMetric();
                violationListViolation.setMetric(metric);
                violationListViolation = em.merge(violationListViolation);
                if (oldMetricOfViolationListViolation != null) {
                    oldMetricOfViolationListViolation.getViolationList().remove(violationListViolation);
                    oldMetricOfViolationListViolation = em.merge(oldMetricOfViolationListViolation);
                }
            }
            for (MetricValueHistory metricValueHistoryListMetricValueHistory : metric.getMetricValueHistoryList()) {
                Metric oldMetricOfMetricValueHistoryListMetricValueHistory = metricValueHistoryListMetricValueHistory.getMetric();
                metricValueHistoryListMetricValueHistory.setMetric(metric);
                metricValueHistoryListMetricValueHistory = em.merge(metricValueHistoryListMetricValueHistory);
                if (oldMetricOfMetricValueHistoryListMetricValueHistory != null) {
                    oldMetricOfMetricValueHistoryListMetricValueHistory.getMetricValueHistoryList().remove(metricValueHistoryListMetricValueHistory);
                    oldMetricOfMetricValueHistoryListMetricValueHistory = em.merge(oldMetricOfMetricValueHistoryListMetricValueHistory);
                }
            }
            em.getTransaction().commit();
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Metric metric) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Metric persistentMetric = em.find(Metric.class, metric.getMetricId());
            Vm vmOld = persistentMetric.getVm();
            Vm vmNew = metric.getVm();
            Service serviceOld = persistentMetric.getService();
            Service serviceNew = metric.getService();
            MetricType metricTypeOld = persistentMetric.getMetricType();
            MetricType metricTypeNew = metric.getMetricType();
            MetricValue metricValueOld = persistentMetric.getMetricValue();
            MetricValue metricValueNew = metric.getMetricValue();
            List<Violation> violationListOld = persistentMetric.getViolationList();
            List<Violation> violationListNew = metric.getViolationList();
            List<MetricValueHistory> metricValueHistoryListOld = persistentMetric.getMetricValueHistoryList();
            List<MetricValueHistory> metricValueHistoryListNew = metric.getMetricValueHistoryList();
            List<String> illegalOrphanMessages = null;
            if (metricValueOld != null && !metricValueOld.equals(metricValueNew)) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain MetricValue " + metricValueOld + " since its metric field is not nullable.");
            }
            for (Violation violationListOldViolation : violationListOld) {
                if (!violationListNew.contains(violationListOldViolation)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Violation " + violationListOldViolation + " since its metric field is not nullable.");
                }
            }
            for (MetricValueHistory metricValueHistoryListOldMetricValueHistory : metricValueHistoryListOld) {
                if (!metricValueHistoryListNew.contains(metricValueHistoryListOldMetricValueHistory)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain MetricValueHistory " + metricValueHistoryListOldMetricValueHistory + " since its metric field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (vmNew != null) {
                vmNew = em.getReference(vmNew.getClass(), vmNew.getVmId());
                metric.setVm(vmNew);
            }
            if (serviceNew != null) {
                serviceNew = em.getReference(serviceNew.getClass(), serviceNew.getServiceId());
                metric.setService(serviceNew);
            }
            if (metricTypeNew != null) {
                metricTypeNew = em.getReference(metricTypeNew.getClass(), metricTypeNew.getMetricTypeId());
                metric.setMetricType(metricTypeNew);
            }
            if (metricValueNew != null) {
                metricValueNew = em.getReference(metricValueNew.getClass(), metricValueNew.getMetricId());
                metric.setMetricValue(metricValueNew);
            }
            List<Violation> attachedViolationListNew = new ArrayList<Violation>();
            for (Violation violationListNewViolationToAttach : violationListNew) {
                violationListNewViolationToAttach = em.getReference(violationListNewViolationToAttach.getClass(), violationListNewViolationToAttach.getViolationId());
                attachedViolationListNew.add(violationListNewViolationToAttach);
            }
            violationListNew = attachedViolationListNew;
            metric.setViolationList(violationListNew);
            List<MetricValueHistory> attachedMetricValueHistoryListNew = new ArrayList<MetricValueHistory>();
            for (MetricValueHistory metricValueHistoryListNewMetricValueHistoryToAttach : metricValueHistoryListNew) {
                metricValueHistoryListNewMetricValueHistoryToAttach = em.getReference(metricValueHistoryListNewMetricValueHistoryToAttach.getClass(), metricValueHistoryListNewMetricValueHistoryToAttach.getMetricValueHistoryPK());
                attachedMetricValueHistoryListNew.add(metricValueHistoryListNewMetricValueHistoryToAttach);
            }
            metricValueHistoryListNew = attachedMetricValueHistoryListNew;
            metric.setMetricValueHistoryList(metricValueHistoryListNew);
            metric = em.merge(metric);
            if (vmOld != null && !vmOld.equals(vmNew)) {
                vmOld.getMetricList().remove(metric);
                vmOld = em.merge(vmOld);
            }
            if (vmNew != null && !vmNew.equals(vmOld)) {
                vmNew.getMetricList().add(metric);
                vmNew = em.merge(vmNew);
            }
            if (serviceOld != null && !serviceOld.equals(serviceNew)) {
                serviceOld.getMetricList().remove(metric);
                serviceOld = em.merge(serviceOld);
            }
            if (serviceNew != null && !serviceNew.equals(serviceOld)) {
                serviceNew.getMetricList().add(metric);
                serviceNew = em.merge(serviceNew);
            }
            if (metricTypeOld != null && !metricTypeOld.equals(metricTypeNew)) {
                metricTypeOld.getMetricList().remove(metric);
                metricTypeOld = em.merge(metricTypeOld);
            }
            if (metricTypeNew != null && !metricTypeNew.equals(metricTypeOld)) {
                metricTypeNew.getMetricList().add(metric);
                metricTypeNew = em.merge(metricTypeNew);
            }
            if (metricValueNew != null && !metricValueNew.equals(metricValueOld)) {
                Metric oldMetricOfMetricValue = metricValueNew.getMetric();
                if (oldMetricOfMetricValue != null) {
                    oldMetricOfMetricValue.setMetricValue(null);
                    oldMetricOfMetricValue = em.merge(oldMetricOfMetricValue);
                }
                metricValueNew.setMetric(metric);
                metricValueNew = em.merge(metricValueNew);
            }
            for (Violation violationListNewViolation : violationListNew) {
                if (!violationListOld.contains(violationListNewViolation)) {
                    Metric oldMetricOfViolationListNewViolation = violationListNewViolation.getMetric();
                    violationListNewViolation.setMetric(metric);
                    violationListNewViolation = em.merge(violationListNewViolation);
                    if (oldMetricOfViolationListNewViolation != null && !oldMetricOfViolationListNewViolation.equals(metric)) {
                        oldMetricOfViolationListNewViolation.getViolationList().remove(violationListNewViolation);
                        oldMetricOfViolationListNewViolation = em.merge(oldMetricOfViolationListNewViolation);
                    }
                }
            }
            for (MetricValueHistory metricValueHistoryListNewMetricValueHistory : metricValueHistoryListNew) {
                if (!metricValueHistoryListOld.contains(metricValueHistoryListNewMetricValueHistory)) {
                    Metric oldMetricOfMetricValueHistoryListNewMetricValueHistory = metricValueHistoryListNewMetricValueHistory.getMetric();
                    metricValueHistoryListNewMetricValueHistory.setMetric(metric);
                    metricValueHistoryListNewMetricValueHistory = em.merge(metricValueHistoryListNewMetricValueHistory);
                    if (oldMetricOfMetricValueHistoryListNewMetricValueHistory != null && !oldMetricOfMetricValueHistoryListNewMetricValueHistory.equals(metric)) {
                        oldMetricOfMetricValueHistoryListNewMetricValueHistory.getMetricValueHistoryList().remove(metricValueHistoryListNewMetricValueHistory);
                        oldMetricOfMetricValueHistoryListNewMetricValueHistory = em.merge(oldMetricOfMetricValueHistoryListNewMetricValueHistory);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = metric.getMetricId();
                if (findMetric(id) == null) {
                    throw new NonexistentEntityException("The metric with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Metric metric;
            try {
                metric = em.getReference(Metric.class, id);
                metric.getMetricId();
            }
            catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The metric with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            MetricValue metricValueOrphanCheck = metric.getMetricValue();
            if (metricValueOrphanCheck != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Metric (" + metric + ") cannot be destroyed since the MetricValue " + metricValueOrphanCheck + " in its metricValue field has a non-nullable metric field.");
            }
            List<Violation> violationListOrphanCheck = metric.getViolationList();
            for (Violation violationListOrphanCheckViolation : violationListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Metric (" + metric + ") cannot be destroyed since the Violation " + violationListOrphanCheckViolation + " in its violationList field has a non-nullable metric field.");
            }
            List<MetricValueHistory> metricValueHistoryListOrphanCheck = metric.getMetricValueHistoryList();
            for (MetricValueHistory metricValueHistoryListOrphanCheckMetricValueHistory : metricValueHistoryListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Metric (" + metric + ") cannot be destroyed since the MetricValueHistory " + metricValueHistoryListOrphanCheckMetricValueHistory + " in its metricValueHistoryList field has a non-nullable metric field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Vm vm = metric.getVm();
            if (vm != null) {
                vm.getMetricList().remove(metric);
                vm = em.merge(vm);
            }
            Service service = metric.getService();
            if (service != null) {
                service.getMetricList().remove(metric);
                service = em.merge(service);
            }
            MetricType metricType = metric.getMetricType();
            if (metricType != null) {
                metricType.getMetricList().remove(metric);
                metricType = em.merge(metricType);
            }
            em.remove(metric);
            em.getTransaction().commit();
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Metric> findMetricEntities() {
        return findMetricEntities(true, -1, -1);
    }

    public List<Metric> findMetricEntities(int maxResults, int firstResult) {
        return findMetricEntities(false, maxResults, firstResult);
    }

    private List<Metric> findMetricEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Metric.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally {
            em.close();
        }
    }

    public Metric findMetric(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Metric.class, id);
        }
        finally {
            em.close();
        }
    }

    public int getMetricCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Metric> rt = cq.from(Metric.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally {
            em.close();
        }
    }

}
