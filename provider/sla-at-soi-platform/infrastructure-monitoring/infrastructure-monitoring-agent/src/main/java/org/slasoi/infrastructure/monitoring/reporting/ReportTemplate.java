/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2898 $
 * @lastrevision $Date: 2011-07-29 16:40:24 +0200 (pet, 29 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/reporting/ReportTemplate.java $
 */

package org.slasoi.infrastructure.monitoring.reporting;

import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;

import java.awt.*;
import java.io.ByteArrayOutputStream;

abstract class ReportTemplate {
    static Font titlePageTitleFont = new Font(Font.HELVETICA, 26, Font.BOLD);
    static Font titlePageSubtitleFont = new Font(Font.HELVETICA, 16, Font.BOLD);
    static Font chapterFont = new Font(Font.HELVETICA, 16, Font.BOLD);
    static Font sectionFont = new Font(Font.HELVETICA, 14, Font.BOLD);
    static Font subsectionFont = new Font(Font.HELVETICA, 12, Font.BOLD);
    static Font tableFont = new Font(Font.HELVETICA, 10, Font.NORMAL);
    static Font normalFont = new Font(Font.HELVETICA, 10, Font.NORMAL);
    static Font normalBoldFont = new Font(Font.HELVETICA, 10, Font.BOLD);
    static Color tableHeaderColor = Color.CYAN;
    static GradientPaint chartBackgroundPaint = new GradientPaint(0F, 0F, new Color(173, 220, 255), 500F, 400F, new Color(217, 229, 243));
    static Color chartBarsColor = new Color(17, 96, 178);
    static int chartWidth = 400;
    static int chartHeight = 300;

    public abstract ByteArrayOutputStream generate() throws Exception;

    void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
