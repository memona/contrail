/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/qos/MonitoringFeaturesBuilder.java $
 */

package org.slasoi.infrastructure.monitoring.qos;

import org.apache.log4j.Logger;
import org.slasoi.monitoring.common.features.*;
import org.slasoi.monitoring.common.features.impl.FeaturesFactoryImpl;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.meta;
import org.slasoi.slamodel.vocab.units;

import java.util.Iterator;
import java.util.LinkedList;

public class MonitoringFeaturesBuilder {
    private static Logger log = Logger.getLogger(MonitoringFeaturesBuilder.class);

    /**
     * A local instance of a FeaturesFactoryImpl.
     */
    private static FeaturesFactoryImpl ffi = new FeaturesFactoryImpl();

    /**
     * Standards Events. *
     */
    private static Event evtRequest = null;
    /**
     * Standards Events. *
     */
    private static Event evtResponse = null;

    /**
     * Extension mapping for arrayOfNUMBER.
     */
    private static String strArrayOfNumber = "http://www.slaatsoi.org/types#array_of_NUMBER";
    /**
     * Extension mapping for arrayOfBOOLEAN.
     */
    private static String strArrayOfBOOLEAN = "http://www.slaatsoi.org/types#array_of_BOOLEAN";
    /**
     * Extension mapping for arrayOfTEXT.
     */
    private static String strArrayOfTEXT = "http://www.slaatsoi.org/types#array_of_TEXT";

    /**
     * Constructor.
     */
    public MonitoringFeaturesBuilder() {
        evtRequest = ffi.createEvent();
        evtResponse = ffi.createEvent();
        evtRequest.setType("REQUEST");
        evtRequest.setName("REQUEST");
        evtResponse.setType("RESPONSE");
        evtResponse.setName("RESPONSE");
    }

    /**
     * Provides an arrayOfNUMBER SLA datatype.
     *
     * @return String
     */
    public final String arrayOfNUMBER() {
        return strArrayOfNumber;
    }

    /**
     * Constructs a set of ComponentMonitoringFeatures.
     *
     * @return org.slasoi.monitoring.common.features.ComponentMonitoringFeatures[]
     * @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     */
    public final ComponentMonitoringFeatures[] getMonitoringFeatures() {
        log.trace("getMonitoringFeatures() started.");
        LinkedList<ComponentMonitoringFeatures> featurestore = new LinkedList<ComponentMonitoringFeatures>();
        featurestore.add(ffi.createComponentMonitoringFeatures());
        featurestore.add(ffi.createComponentMonitoringFeatures());
        featurestore.add(ffi.createComponentMonitoringFeatures());
        ComponentMonitoringFeatures[] cmfeatures = new ComponentMonitoringFeatures[featurestore.size()];

        // ////////////////////////////////////////////////////////////////////////////////
        // Core Component for Everest Reasoner Features
        // ////////////////////////////////////////////////////////////////////////////////
        cmfeatures[0] = ffi.createComponentMonitoringFeatures();
        cmfeatures[0].setUuid("1801114f-7663-4b50-9bd7-3bcd2c394d84");
        cmfeatures[0].setType("REASONER");

        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        // Model Features
        mflist.addAll(buildModelFeatures());

        // Terms Features
        mflist.addAll(buildLogicalOpFeatures());
        mflist.addAll(buildComparisonOpFeatures());
        mflist.addAll(buildArithmeticFunctionFeatures());
        mflist.addAll(buildAggregateFunctionFeatures());
        mflist.addAll(buildTimeSeriesFeatures());
        mflist.addAll(buildContextFeatures());
        mflist.addAll(buildEventFunctionFeatures());
        mflist.addAll(buildQoSTermFeatures());
        cmfeatures[0].setMonitoringFeatures(mfListToArray(mflist));

        // ////////////////////////////////////////////////////////////////////////////////
        // Core Component for Everest Sensor Features
        // ////////////////////////////////////////////////////////////////////////////////
        mflist.clear();
        mflist.addAll(buildStandardServiceFeatures());
        cmfeatures[1] = ffi.createComponentMonitoringFeatures();
        cmfeatures[1].setUuid("62d2a312-d414-419b-8320-c1cad31319dc");
        cmfeatures[1].setType("SENSOR");
        cmfeatures[1].setMonitoringFeatures(mfListToArray(mflist));

        // B4 extension features
        cmfeatures[2] = ffi.createComponentMonitoringFeatures();
        cmfeatures[2].setUuid("87ecb14c-c12e-4ec7-908d-7545199c3c4b");
        cmfeatures[2].setType("SENSOR");

        mflist.clear();

        String type = "";
        type = "http://www.slaatsoi.org/commonTerms#persistence";
        mflist.add(buildSensor(type, type, "noevent"));
        type = "http://www.slaatsoi.org/commonTerms#memory";
        mflist.add(buildSensor(type, type, "noevent"));
        type = "http://www.slaatsoi.org/commonTerms#cpu_speed";
        mflist.add(buildSensor(type, type, "noevent"));
        type = "http://www.slaatsoi.org/commonTerms#vm_cores";
        mflist.add(buildSensor(type, type, "noevent"));
        /*
         * type = common.$location; mflist.add(EverestTestMonitoringFeatures.buildSensor(type, type, "noevent"));
         * //disabled in order to compile, because $location was removed from common
         */
        type = common.$isolation;
        mflist.add(buildSensor(type, type, "noevent"));
        type = "http://www.slaatsoi.org/commonTerms#vm_image";
        mflist.add(buildSensor(type, type, "noevent"));
        type = "http://www.slaatsoi.org/commonTerms#VM_Access_Point";
        mflist.add(buildSensor(type, type, "RESPONSE"));
        type = "autogen/VM_X";
        mflist.add(buildSensor(type, type, "RESPONSE"));
        type = "REQUEST";
        mflist.add(buildSensor(type, type, "noevent"));
        type = "RESPONSE";
        mflist.add(buildSensor(type, type, "noevent"));

        // greater_than reasoners and primitives
        mflist.add(buildFunction(core.$greater_than,
                "greater_than xsd:integer Reasoner", "xsd:integer", "xsd:integer", meta.$BOOLEAN));
        // isa reasoner and primitives
        mflist.add(buildFunction(core.$isa, "isa xsd:boolean Reasoner",
                "xsd:boolean", "xsd:boolean", meta.$BOOLEAN));
        // member_of reasoner and primitives
        mflist.add(buildFunction(core.$member_of, "member_of xsd:string Reasoner",
                "xsd:string", "xsd:string", meta.$BOOLEAN));
        // member_of reasoner and primitives
        mflist.add(buildFunction(core.$member_of, "member_of units#GHz Reasoner",
                units.$GHz, units.$GHz, meta.$BOOLEAN));
        // member_of reasoner and primitives
        mflist.add(buildFunction(core.$member_of, "member_of units#GHz Reasoner",
                "xsd:anyURI", "xsd:anyURI", meta.$BOOLEAN));

        cmfeatures[2].setMonitoringFeatures(mfListToArray(mflist));

        log.trace("getMonitoringFeatures() finished successfully.");
        return cmfeatures;
    }

    /**
     * Returns a standard Request Event.
     *
     * @return Event
     * @see org.slasoi.monitoring.common.features.Event
     */
    public static Event getRequestEvent() {
        return evtRequest;
    }

    /**
     * Returns a standard Response Event.
     *
     * @return Event
     * @see org.slasoi.monitoring.common.features.Event
     */
    public static Event getResponseEvent() {
        return evtResponse;
    }

    /**
     * Builds a set of SLA Model Features.
     *
     * @return LinkedList<MonitoringFeatures>
     * @see org.slasoi.monitoring.common.features.MonitoringFeature
     */
    private static LinkedList<MonitoringFeature> buildModelFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        // ////////////////////////////////////////////////////////////////////////////
        // AgreementTerm Reasoner (core.$series,arrayofBOOLEAN,core.$BOOLEAN
        // ////////////////////////////////////////////////////////////////////////////
        mflist.add(buildFunction(core.$series, "Reasoner for Series of BOOLEAN", strArrayOfBOOLEAN, "", meta.$BOOLEAN));

        return mflist;
    }

    /**
     * Build a set of Standard Service Sensors.
     *
     * @return LinkedList<MonitoringFeatures>
     * @see org.slasoi.monitoring.common.features.MonitoringFeature
     */
    private static LinkedList<MonitoringFeature> buildStandardServiceFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildSensor(evtRequest));
        mflist.add(buildSensor(evtResponse));

        return mflist;

    }

    /**
     * Build a set of Logical Operator Features.
     *
     * @return LinkedList<MonitoringFeatures>
     * @see org.slasoi.monitoring.common.features.MonitoringFeature
     */
    private static LinkedList<MonitoringFeature> buildLogicalOpFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(core.$and, "AND Reasoner", meta.$BOOLEAN, meta.$BOOLEAN, meta.$BOOLEAN));

        // special features for monitoring agreement terms
        mflist.add(buildFunction(core.$and, "AND arrayOfBOOLEAN Reasoner", strArrayOfBOOLEAN, "", meta.$BOOLEAN));

        mflist.add(buildFunction(core.$or, "OR Reasoner", meta.$BOOLEAN, meta.$BOOLEAN, meta.$BOOLEAN));
        mflist.add(buildFunction(core.$not, "NOT Reasoner", meta.$BOOLEAN, "", meta.$BOOLEAN));

        // special features for service path terms

        return mflist;

    }

    /**
     * Build a set of Comparison Operator Features.
     *
     * @return LinkedList<MonitoringFeatures>
     * @see org.slasoi.monitoring.common.features.MonitoringFeature
     */
    private static LinkedList<MonitoringFeature> buildComparisonOpFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        // equals reasoners and primitives
        mflist.add(buildFunction(core.$equals, "equals NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER, meta.$BOOLEAN));
        mflist.add(buildFunction(core.$equals, "equals TEXT Reasoner", meta.$TEXT, meta.$TEXT, meta.$BOOLEAN));
        mflist.add(buildFunction(core.$equals, "equals TIME Reasoner", meta.$TIME_STAMP, meta.$TIME_STAMP,
                meta.$BOOLEAN));

        // not_equals reasoners and primitives
        mflist
                .add(buildFunction(core.$not_equals, "not_equals NUMBER Reasoner", meta.$STND, meta.$STND,
                        meta.$BOOLEAN));
        mflist.add(buildFunction(core.$not_equals, "not_equals TEXT Reasoner", meta.$TEXT, meta.$TEXT, meta.$BOOLEAN));
        mflist.add(buildFunction(core.$not_equals, "equals TIME Reasoner", meta.$TIME_STAMP, meta.$TIME_STAMP,
                meta.$BOOLEAN));

        // less_than reasoners and primitives
        mflist.add(buildFunction(core.$less_than, "less_than NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER,
                meta.$BOOLEAN));

        // less_than_or_equals reasoners and primitives
        mflist.add(buildFunction(core.$less_than_or_equals, "less_than_or_equals NUMBER Reasoner", meta.$NUMBER,
                meta.$NUMBER, meta.$BOOLEAN));

        // greater_than reasoners and primitives
        mflist.add(buildFunction(core.$greater_than, "greater_than NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER,
                meta.$BOOLEAN));
        mflist.add(buildFunction(core.$greater_than, "greater_than percentage Reasoner", units.$percentage,
                units.$percentage, meta.$BOOLEAN));

        // greater_than_or_equals reasoners and primitives
        mflist.add(buildFunction(core.$greater_than_or_equals, "greater_than_or_equals Reasoner", meta.$NUMBER,
                meta.$NUMBER, meta.$BOOLEAN));

        return mflist;

    }

    /**
     * Build a set of Arithmetic Operator Features.
     *
     * @return LinkedList<MonitoringFeatures>
     * @see org.slasoi.monitoring.common.features.MonitoringFeature
     */
    private static LinkedList<MonitoringFeature> buildArithmeticFunctionFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(core.$add, "add NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER, meta.$NUMBER));
        mflist.add(buildFunction(core.$subtract, "subtract NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER, meta.$NUMBER));
        mflist.add(buildFunction(core.$multiply, "multiply NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER, meta.$NUMBER));
        mflist.add(buildFunction(core.$divide, "divide NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER, meta.$NUMBER));

        return mflist;

    }

    /**
     * Build a set of Aggregate Operator Features.
     *
     * @return LinkedList<MonitoringFeatures>
     * @see org.slasoi.monitoring.common.features.MonitoringFeature
     */
    private static LinkedList<MonitoringFeature> buildAggregateFunctionFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(core.$sum, "sum Reasoner", strArrayOfNumber, "", meta.$NUMBER));
        mflist.add(buildFunction(core.$mean, "mean Reasoner", strArrayOfNumber, "", meta.$NUMBER));
        mflist.add(buildFunction(core.$max, "sum Reasoner", strArrayOfNumber, "", meta.$NUMBER));
        mflist.add(buildFunction(core.$min, "min Reasoner", strArrayOfNumber, "", meta.$NUMBER));
        mflist.add(buildFunction(core.$count, "count NUMBER Reasoner", strArrayOfNumber, "", meta.$NUMBER));
        mflist.add(buildFunction(core.$count, "count TEXT Reasoner", strArrayOfTEXT, "", meta.$NUMBER));

        return mflist;

    }

    /**
     * Build a set of Time Series Operator Features.
     *
     * @return LinkedList<MonitoringFeatures>
     * @see org.slasoi.monitoring.common.features.MonitoringFeature
     */
    private static LinkedList<MonitoringFeature> buildTimeSeriesFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(core.$series, "series with Event Reasoner", strArrayOfNumber, meta.$EVENT,
                meta.$NUMBER));

        return mflist;

    }

    /**
     * Build a set of Context Operator Features.
     *
     * @return LinkedList<MonitoringFeatures>
     * @see org.slasoi.monitoring.common.features.MonitoringFeature
     */
    private static LinkedList<MonitoringFeature> buildContextFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(core.$time_is, "time_is reasoner", "", "", core.$time));
        mflist.add(buildFunction(core.$day_is, "day_is reasoner", "", "", meta.$DAY));

        return mflist;
    }

    /**
     * Build a set of Event Function Operator Features.
     *
     * @return LinkedList<MonitoringFeatures>
     * @see org.slasoi.monitoring.common.features.MonitoringFeature
     */
    private static LinkedList<MonitoringFeature> buildEventFunctionFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic seconds", units.$s, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic ms", units.$ms, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic mins", units.$min, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic hours", units.$hrs, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic days", units.$day, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic month", units.$month, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic year", units.$year, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic year", units.$year, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$time, "time sensor", "", core.$time));

        return mflist;
    }

    /**
     * Build a set of QoS Term Features.
     *
     * @return LinkedList<MonitoringFeatures>
     * @see org.slasoi.monitoring.common.features.MonitoringFeature
     */
    private static LinkedList<MonitoringFeature> buildQoSTermFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(common.$availability, "availability reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$arrival_rate, "arrival_rate reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$throughput, "throughput reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$completion_time, "completion_time reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$mttr, "mttr reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$mttf, "mttf reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$reliability, "reliability reasoner", "", "", meta.$NUMBER));

        return mflist;
    }

    /**
     * Converts a MonitoringFeature List to an Array.
     *
     * @param mflist A LinkedList of MonitoringFeatures
     * @return MonitoringFeatures[]
     * @see org.slasoi.monitoring.common.features.MonitoringFeature
     */
    public static MonitoringFeature[] mfListToArray(final LinkedList<MonitoringFeature> mflist) {
        MonitoringFeature[] modelfeatures = new MonitoringFeature[mflist.size()];
        Iterator<MonitoringFeature> mflit = mflist.iterator();
        int counter = 0;
        while (mflit.hasNext()) {
            modelfeatures[counter++] = mflit.next();
        }
        return modelfeatures;
    }

    /**
     * Builds a generic feature for a Reasoner Function.
     *
     * @param type       Feature type
     * @param desc       Feature description
     * @param param1Type Feature input 1
     * @param param2Type Feature input 2
     * @param resultType Feature result type
     * @return Function
     */
    public static Function buildFunction(final String type, final String desc, final String param1Type,
                                         final String param2Type, final String resultType) {
        int noofparams = 1;
        if (param2Type.length() > 0) {
            noofparams = 2;
        }

        Function fimpl = ffi.createFunction();
        fimpl.setName(type);
        fimpl.setDescription(desc);

        if (param1Type.length() > 0) {
            Basic[] fInput = new Basic[noofparams];

            Basic fInput1 = ffi.createPrimitive();
            fInput1.setName("param1");
            fInput1.setType(param1Type);
            fInput[0] = fInput1;

            if (param2Type.length() > 0) {
                Basic fInput2 = ffi.createPrimitive();
                fInput2.setName("param2");
                fInput2.setType(param2Type);
                fInput[1] = fInput2;
            }

            fimpl.setInput(fInput);
        }

        if (resultType.length() > 0) {
            Basic fOutput = ffi.createPrimitive();
            fOutput.setName("output1");
            fOutput.setType(resultType);
            fimpl.setOutput(fOutput);
        }

        return fimpl;
    }

    /**
     * Builds a generic feature for a Reasoner Function with an output event.
     *
     * @param type       Feature type
     * @param desc       Feature description
     * @param unit1Type  Unit input 1
     * @param resultType Feature result type
     * @return Function
     */
    public static Function buildFunctionUnitWithEvent(final String type, final String desc, final String unit1Type,
                                                      final String resultType) {
        Function fimpl = ffi.createFunction();
        fimpl = buildFunction(type, desc, "", "", "");

        // add unit as first parameter
        if (unit1Type.length() > 0) {
            Basic[] fimplInputs = new Basic[1];
            Primitive periodicInput1 = ffi.createPrimitive();
            periodicInput1.setName(desc + " input");
            periodicInput1.setUnit(unit1Type);
            fimplInputs[0] = periodicInput1;
            fimpl.setInput(fimplInputs);
        }

        if (resultType.length() > 0) {
            // now add event as output
            Event eventImpl = ffi.createEvent();
            eventImpl.setName(resultType);
            eventImpl.setType(resultType);
            fimpl.setOutput(eventImpl);
        }
        return fimpl;
    }

    /**
     * Builds a generic feature with name and description for a Sensor with a given output event.
     *
     * @param name  Feature name
     * @param desc  Feature description
     * @param event Event type provided by the Sensor
     * @return Function
     */
    public static Function buildSensor(final String name, final String desc, final String event) {
        Function fimpl = ffi.createFunction();
        fimpl = buildFunction(name, desc, "", "", "");

        if (event.length() > 0) {
            // now add event as output
            Event eventImpl = ffi.createEvent();
            eventImpl.setName(event);
            eventImpl.setType(event);
            fimpl.setOutput(eventImpl);
        }
        return fimpl;
    }

    /**
     * Builds a generic feature for a Sensor with a given output event.
     *
     * @param event Event type provided by the Sensor
     * @return Function
     */
    public static Function buildSensor(final Event event) {
        Function fimpl = null;

        if (event != null) {
            fimpl = buildFunctionUnitWithEvent(event.getName(), event.getDescription(), "", event.getName());
        }
        return fimpl;
    }
}
