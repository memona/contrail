/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1420 $
 * @lastrevision $Date: 2011-04-18 13:05:54 +0200 (pon, 18 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/managers/MetricValueManager.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.managers;

import org.slasoi.infrastructure.monitoring.jpa.controller.MetricValueJpaController;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MetricValueManager extends MetricValueJpaController {
    private static MetricValueManager instance;

    private MetricValueManager() {

    }

    public static MetricValueManager getInstance() {
        if (instance == null) {
            instance = new MetricValueManager();
        }
        return instance;
    }

    public void updateOrCreate(MetricValue metricValue) throws Exception {
        MetricValue metricValueOld = findMetricValue(metricValue.getMetricId());
        if (metricValueOld != null) {
            edit(metricValue);
        }
        else {
            create(metricValue);
        }
    }

    public MetricValue storeMetricValue(Metric metric, Object valueObj, Date timestamp) throws Exception {
        String value = (valueObj != null) ? valueObj.toString() : null;
        MetricValue metricValue = new MetricValue(metric.getMetricId(), timestamp);
        metricValue.setMetric(metric);
        metricValue.setValue(value);
        metricValue.setSlaCompliance(null);
        MetricValueManager.getInstance().updateOrCreate(metricValue);
        metric.setMetricValue(metricValue);
        return metricValue;
    }

    public List<MetricValue> getServiceMetricValues(Service service) {
        // get service metrics ids
        List<Integer> metricIdList = new ArrayList<Integer>();
        for (Metric metric : service.getMetricList()) {
            metricIdList.add(metric.getMetricId());
        }

        return getMetricValues(metricIdList);
    }

    public List<MetricValue> getVmMetricValues(Vm vm) {
        // get vm metrics ids
        List<Integer> metricIdList = new ArrayList<Integer>();
        for (Metric metric : vm.getMetricList()) {
            metricIdList.add(metric.getMetricId());
        }

        return getMetricValues(metricIdList);
    }

    private List<MetricValue> getMetricValues(List<Integer> metricIdList) {
        if (metricIdList.size() == 0) {
            return null;
        }

        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT mv FROM MetricValue mv WHERE mv.metric.metricId IN :metricIdList");
            query.setParameter("metricIdList", metricIdList);

            try {
                return (List<MetricValue>) query.getResultList();
            }
            catch (NoResultException e) {
                return null;
            }
        }
        finally {
            em.close();
        }
    }
}
