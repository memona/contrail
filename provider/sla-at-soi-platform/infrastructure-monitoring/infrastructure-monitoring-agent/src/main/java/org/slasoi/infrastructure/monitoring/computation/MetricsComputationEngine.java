/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2987 $
 * @lastrevision $Date: 2011-08-18 11:11:29 +0200 (čet, 18 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/computation/MetricsComputationEngine.java $
 */

package org.slasoi.infrastructure.monitoring.computation;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricType;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class MetricsComputationEngine {
    private static Logger log = Logger.getLogger(MetricsComputationEngine.class);
    private IMonitoringEngine monitoringEngine;

    public MetricsComputationEngine(IMonitoringEngine monitoringEngine) {
        this.monitoringEngine = monitoringEngine;
    }

    public void computeValidateMetrics(List<Service> services) throws Exception {
        log.trace("Metrics computation started.");

        // compute infrastructure metrics
        List infrastructureMetrics = MetricManager.getInstance().findInfrastructureMetrics();
        if (infrastructureMetrics != null) {
            computeMetrics(infrastructureMetrics);
        }

        if (services.size() == 0) {
            log.trace("No services are being monitored. Metrics computation and validation skipped.");
            return;
        }

        for (Service service : services) {
            try {
                if (!monitoringEngine.isMonitoringDataAvailable(service)) {
                    log.info(String.format("Monitoring data is not available for the service '%s'. Metrics " +
                            "computation and validation skipped.", service.getServiceName()));
                    continue;
                }

                // compute VM metrics
                for (Vm vm : service.getVmList()) {
                    if (vm.getMetricList().size() > 0) {
                        computeMetrics(vm.getMetricList());
                    }
                    else {
                        log.trace(String.format("No metrics defined for VM %s.", vm.getName()));
                    }
                }

                if (service.getMetricList().size() > 0) {
                    computeMetrics(service.getMetricList());
                }
                else {
                    log.trace(String.format("No metrics defined for service %s.", service.getServiceName()));
                }
            }
            catch (Exception e) {
                log.error(String.format("Failed to compute&validate metrics of the service %s.", service.getServiceName()), e);
            }
        }
        log.trace("Metrics computation finished successfully.");
    }

    private void computeMetrics(List<Metric> metrics) throws Exception {
        if (metrics.size() == 0) {
            return;
        }

        Comparator<Metric> comparator = new Comparator<Metric>() {
            public int compare(Metric metric1, Metric metric2) {
                return metric1.getMetricType().getComputationOrder() - metric2.getMetricType().getComputationOrder();
            }
        };
        PriorityQueue<Metric> priorityQueue = new PriorityQueue<Metric>(metrics.size(), comparator);
        for (Metric metric : metrics) {
            if (metric.getMetricType().getType() != MetricType.Type.CONFIG) {
                priorityQueue.add(metric);
            }
        }

        for (Metric metric : priorityQueue) {
            try {
                metric.compute(monitoringEngine);
            }
            catch (Exception e) {
                MetricType metricType = metric.getMetricType();
                if (metricType.getTarget() == MetricType.Target.SERVICE) {
                    log.error(String.format("Failed to compute/validate metric %s of the service '%s': %s",
                            metric.getMetricType().getName(), metric.getService().getServiceName(), e.getMessage()), e);
                }
                else if (metricType.getTarget() == MetricType.Target.VM) {
                    Vm vm = metric.getVm();
                    log.error(String.format("Failed to compute/validate metric %s of the service '%s', resource '%s': %s",
                            metric.getMetricTypeEnum(), vm.getService().getServiceName(), vm.getFqdn(), e.getMessage()), e);
                }
                else {
                    log.error(String.format("Failed to compute/validate metric %s with id=%d: %s",
                            metric.getMetricTypeEnum(), metric.getMetricId(), e.getMessage()), e);
                }
            }
        }
    }
}
