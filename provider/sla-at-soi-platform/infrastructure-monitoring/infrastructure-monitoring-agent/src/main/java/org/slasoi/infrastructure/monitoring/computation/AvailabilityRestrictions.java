/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1662 $
 * @lastrevision $Date: 2011-05-11 14:33:23 +0200 (sre, 11 maj 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/computation/AvailabilityRestrictions.java $
 */

package org.slasoi.infrastructure.monitoring.computation;

import java.util.Calendar;
import java.util.Date;

public class AvailabilityRestrictions {
    Period workingHours;

    public AvailabilityRestrictions(Period workingHours) {
        this.workingHours = workingHours;
    }

    public Period getWorkingHours() {
        return workingHours;
    }

    public boolean isWorkingTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        if (workingHours == Period.NON_STOP) {
            return true;
        }
        else if (workingHours == Period.WORKING_DAYS) {
            int weekday = calendar.get(Calendar.DAY_OF_WEEK);
            return (weekday >= Calendar.MONDAY && weekday <= Calendar.FRIDAY);
        }
        else if (workingHours == Period.BUSINESS_HOURS) {
            int weekday = calendar.get(Calendar.DAY_OF_WEEK);
            int hours = calendar.get(Calendar.HOUR_OF_DAY);
            return (weekday >= Calendar.MONDAY && weekday <= Calendar.FRIDAY &&
                    hours >= 8 && hours <= 16);
        }
        else if (workingHours == Period.NEVER) {
            return false;
        }
        else {
            throw new RuntimeException("Invalid Period.");
        }
    }

    public enum Period {
        NON_STOP,
        WORKING_DAYS,
        BUSINESS_HOURS,
        NEVER;

        public static Period fromString(String text) throws Exception {
            try {
                return Period.valueOf(text.toUpperCase());
            }
            catch (Exception e) {
                throw new Exception("Invalid Period value: " + text);
            }
        }
    }
}
