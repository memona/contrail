/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2818 $
 * @lastrevision $Date: 2011-07-26 08:32:55 +0200 (tor, 26 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/Vm.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities;

import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "vm", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"fqdn"}),
        @UniqueConstraint(columnNames = {"service_id", "resource_id"})})
@NamedQueries({
        @NamedQuery(name = "Vm.findAll", query = "SELECT v FROM Vm v"),
        @NamedQuery(name = "Vm.findByVmId", query = "SELECT v FROM Vm v WHERE v.vmId = :vmId"),
        @NamedQuery(name = "Vm.findByResourceId", query = "SELECT v FROM Vm v WHERE v.resourceId = :resourceId"),
        @NamedQuery(name = "Vm.findByName", query = "SELECT v FROM Vm v WHERE v.name = :name"),
        @NamedQuery(name = "Vm.findByFqdn", query = "SELECT v FROM Vm v WHERE v.fqdn = :fqdn"),
        @NamedQuery(name = "Vm.findByClusterName", query = "SELECT v FROM Vm v WHERE v.clusterName = :clusterName")})
public class Vm implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "vm_id", nullable = false)
    private Integer vmId;
    @Basic(optional = false)
    @Column(name = "resource_id", nullable = false)
    private int resourceId;
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 50)
    private String name;
    @Column(name = "fqdn", length = 100)
    private String fqdn;
    @Basic(optional = false)
    @Column(name = "cluster_name", nullable = false, length = 50)
    private String clusterName;
    @JoinColumn(name = "service_id", referencedColumnName = "service_id", nullable = false)
    @ManyToOne(optional = false)
    private Service service;
    @OneToMany(mappedBy = "vm")
    private List<Metric> metricList;

    public Vm() {
    }

    public Vm(Integer vmId) {
        this.vmId = vmId;
    }

    public Vm(Integer vmId, int resourceId, String name, String clusterName) {
        this.vmId = vmId;
        this.resourceId = resourceId;
        this.name = name;
        this.clusterName = clusterName;
    }

    public Integer getVmId() {
        return vmId;
    }

    public void setVmId(Integer vmId) {
        this.vmId = vmId;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFqdn() {
        return fqdn;
    }

    public void setFqdn(String fqdn) {
        this.fqdn = fqdn;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public List<Metric> getMetricList() {
        return metricList;
    }

    public void setMetricList(List<Metric> metricList) {
        this.metricList = metricList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vmId != null ? vmId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vm)) {
            return false;
        }
        Vm other = (Vm) object;
        if ((this.vmId == null && other.vmId != null) || (this.vmId != null && !this.vmId.equals(other.vmId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.slasoi.infrastructure.monitoring.jpa.entities.Vm[vmId=" + vmId + "]";
    }

    public Metric getMetric(MetricTypeEnum metricTypeEnum) {
        return MetricManager.getInstance().findVmMetric(metricTypeEnum, this);
    }
}
