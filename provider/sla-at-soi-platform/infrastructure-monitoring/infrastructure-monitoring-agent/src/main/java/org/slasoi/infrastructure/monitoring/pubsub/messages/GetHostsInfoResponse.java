/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3048 $
 * @lastrevision $Date: 2011-09-05 10:26:14 +0200 (pon, 05 sep 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/messages/GetHostsInfoResponse.java $
 */

package org.slasoi.infrastructure.monitoring.pubsub.messages;

import com.google.gson.Gson;
import org.slasoi.infrastructure.monitoring.jpa.enums.VmStateEnum;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.utils.JsonUtils;

import java.util.ArrayList;
import java.util.List;

public class GetHostsInfoResponse extends PubSubResponse {
    private List<Host> hosts;

    public GetHostsInfoResponse() {
        super.setResponseType("GetHostsInfoResponse");
        hosts = new ArrayList<Host>();
    }

    public static GetHostsInfoResponse fromJson(String jsonString) {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.fromJson(jsonString, GetHostsInfoResponse.class);
    }

    public String toJson() {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.toJson(this);
    }

    public List<Host> getHosts() {
        return hosts;
    }

    public void putHost(Host host) {
        hosts.add(host);
    }

    public static class Host {
        private String name;
        private String fqdn;
        private String location;
        private String cpuSpeed;
        private String numOfCores;
        private String cpuUtilization;
        private String memorySize;
        private String memFree;
        private String diskTotal;
        private String diskFree;
        private boolean poweredOn;
        private List<Vm> guestMachines;

        public Host() {
            guestMachines = new ArrayList<Vm>();
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFqdn() {
            return fqdn;
        }

        public void setFqdn(String fqdn) {
            this.fqdn = fqdn;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getCpuSpeed() {
            return cpuSpeed;
        }

        public void setCpuSpeed(String cpuSpeed) {
            this.cpuSpeed = cpuSpeed;
        }

        public String getNumOfCores() {
            return numOfCores;
        }

        public void setNumOfCores(String numOfCores) {
            this.numOfCores = numOfCores;
        }

        public String getCpuUtilization() {
            return cpuUtilization;
        }

        public void setCpuUtilization(String cpuUtilization) {
            this.cpuUtilization = cpuUtilization;
        }

        public String getMemorySize() {
            return memorySize;
        }

        public void setMemorySize(String memorySize) {
            this.memorySize = memorySize;
        }

        public String getMemFree() {
            return memFree;
        }

        public void setMemFree(String memFree) {
            this.memFree = memFree;
        }

        public String getDiskTotal() {
            return diskTotal;
        }

        public void setDiskTotal(String diskTotal) {
            this.diskTotal = diskTotal;
        }

        public String getDiskFree() {
            return diskFree;
        }

        public void setDiskFree(String diskFree) {
            this.diskFree = diskFree;
        }

        public boolean isPoweredOn() {
            return poweredOn;
        }

        public void setPoweredOn(boolean poweredOn) {
            this.poweredOn = poweredOn;
        }

        public List<Vm> getGuestMachines() {
            return guestMachines;
        }

        public void putGuestMachine(Vm vm) {
            guestMachines.add(vm);
        }
    }

    public static class Vm {
        private String name;
        private String fqdn;
        private VmStateEnum state;
        private String cpuSpeed;
        private String numOfCores;
        private String cpuLoad;
        private String memorySize;
        private String memUsage;
        private String persistent;
        private String imageTemplateName;
        private String serviceName;
        private String serviceUri;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFqdn() {
            return fqdn;
        }

        public void setFqdn(String fqdn) {
            this.fqdn = fqdn;
        }

        public VmStateEnum getState() {
            return state;
        }

        public void setState(VmStateEnum state) {
            this.state = state;
        }

        public String getCpuSpeed() {
            return cpuSpeed;
        }

        public void setCpuSpeed(String cpuSpeed) {
            this.cpuSpeed = cpuSpeed;
        }

        public String getNumOfCores() {
            return numOfCores;
        }

        public void setNumOfCores(String numOfCores) {
            this.numOfCores = numOfCores;
        }

        public String getCpuLoad() {
            return cpuLoad;
        }

        public void setCpuLoad(String cpuLoad) {
            this.cpuLoad = cpuLoad;
        }

        public String getMemorySize() {
            return memorySize;
        }

        public void setMemorySize(String memorySize) {
            this.memorySize = memorySize;
        }

        public String getMemUsage() {
            return memUsage;
        }

        public void setMemUsage(String memUsage) {
            this.memUsage = memUsage;
        }

        public String getPersistent() {
            return persistent;
        }

        public void setPersistent(String persistent) {
            this.persistent = persistent;
        }

        public String getImageTemplateName() {
            return imageTemplateName;
        }

        public void setImageTemplateName(String imageTemplateName) {
            this.imageTemplateName = imageTemplateName;
        }

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        public String getServiceUri() {
            return serviceUri;
        }

        public void setServiceUri(String serviceUri) {
            this.serviceUri = serviceUri;
        }
    }
}
