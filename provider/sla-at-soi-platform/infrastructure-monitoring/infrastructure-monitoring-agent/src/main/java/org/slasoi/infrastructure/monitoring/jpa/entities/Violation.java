/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/Violation.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities;

import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "violation")
@NamedQueries({
        @NamedQuery(name = "Violation.findAll", query = "SELECT v FROM Violation v"),
        @NamedQuery(name = "Violation.findByViolationId", query = "SELECT v FROM Violation v WHERE v.violationId = :violationId"),
        @NamedQuery(name = "Violation.findByMetricValue", query = "SELECT v FROM Violation v WHERE v.metricValue = :metricValue"),
        @NamedQuery(name = "Violation.findByThreshold", query = "SELECT v FROM Violation v WHERE v.threshold = :threshold"),
        @NamedQuery(name = "Violation.findByStartTime", query = "SELECT v FROM Violation v WHERE v.startTime = :startTime"),
        @NamedQuery(name = "Violation.findByEndTime", query = "SELECT v FROM Violation v WHERE v.endTime = :endTime"),
        @NamedQuery(name = "Violation.findBySeverity", query = "SELECT v FROM Violation v WHERE v.severity = :severity"),
        @NamedQuery(name = "Violation.findByType", query = "SELECT v FROM Violation v WHERE v.type = :type")})
public class Violation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "violation_id", nullable = false)
    private Integer violationId;
    @Basic(optional = false)
    @Column(name = "metric_value", nullable = false, length = 32)
    private String metricValue;
    @Column(name = "threshold", length = 32)
    private String threshold;
    @Basic(optional = false)
    @Column(name = "start_time", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;
    @Column(name = "end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;
    @Column(name = "severity", nullable = false)
    @Enumerated(EnumType.STRING)
    private ViolationSeverity severity;
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private ViolationType type;
    @JoinColumn(name = "metric_id", referencedColumnName = "metric_id", nullable = false)
    @ManyToOne(optional = false)
    private Metric metric;

    public Violation() {
    }

    public Violation(Integer violationId) {
        this.violationId = violationId;
    }

    public Violation(Integer violationId, String metricValue, Date startTime, ViolationSeverity severity, ViolationType type) {
        this.violationId = violationId;
        this.metricValue = metricValue;
        this.startTime = startTime;
        this.severity = severity;
        this.type = type;
    }

    public Integer getViolationId() {
        return violationId;
    }

    public void setViolationId(Integer violationId) {
        this.violationId = violationId;
    }

    public String getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(String metricValue) {
        this.metricValue = metricValue;
    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public ViolationSeverity getSeverity() {
        return severity;
    }

    public void setSeverity(ViolationSeverity severity) {
        this.severity = severity;
    }

    public ViolationType getType() {
        return type;
    }

    public void setType(ViolationType type) {
        this.type = type;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (violationId != null ? violationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Violation)) {
            return false;
        }
        Violation other = (Violation) object;
        if ((this.violationId == null && other.violationId != null) || (this.violationId != null && !this.violationId.equals(other.violationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.slasoi.infrastructure.monitoring.jpa.entities.Violation[violationId=" + violationId + "]";
    }

}
