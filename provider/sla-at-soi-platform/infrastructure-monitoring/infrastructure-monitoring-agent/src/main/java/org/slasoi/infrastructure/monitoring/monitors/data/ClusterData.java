/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2106 $
 * @lastrevision $Date: 2011-06-09 10:38:47 +0200 (čet, 09 jun 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/monitors/data/ClusterData.java $
 */

package org.slasoi.infrastructure.monitoring.monitors.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ClusterData {
    private String name;
    private String fqdn;
    private Map<String, HostData> hosts;
    private Map<String, VmData> virtualMachines;

    public ClusterData() {
        hosts = new HashMap<String, HostData>();
        virtualMachines = new HashMap<String, VmData>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFqdn() {
        return fqdn;
    }

    public void setFqdn(String fqdn) {
        this.fqdn = fqdn;
    }

    public HostData getHostData(String machineName) {
        return hosts.get(machineName);
    }

    public void putHostData(HostData hostData) {
        hosts.put(hostData.getFqdn(), hostData);
    }

    public VmData getVmData(String machineName) {
        return virtualMachines.get(machineName);
    }

    public boolean containsVm(String machineName) {
        return virtualMachines.containsKey(machineName);
    }

    public void putVmData(VmData vmData) {
        virtualMachines.put(vmData.getFqdn(), vmData);
    }

    public Collection<HostData> getHostList() {
        return hosts.values();
    }

    public Collection<VmData> getVmList() {
        return virtualMachines.values();
    }
}
