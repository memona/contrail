/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3049 $
 * @lastrevision $Date: 2011-09-05 10:29:54 +0200 (pon, 05 sep 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/computation/MetricsComputationHelper.java $
 */

package org.slasoi.infrastructure.monitoring.computation;

import org.eclipse.persistence.queries.ScrollableCursor;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValueHistory;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricValueHistoryManager;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class MetricsComputationHelper {
    public static Double computeAvailability(Metric availMetric, ReportingPeriod reportingPeriod) {
        EntityManager em = MetricValueHistoryManager.getInstance().getEntityManager();
        try {
            Query query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric AND " +
                    "mvh.endTime >= :startTime AND mvh.metricValueHistoryPK.startTime < :endTime");
            query.setParameter("metric", availMetric);
            query.setParameter("startTime", reportingPeriod.getStartDate());
            query.setParameter("endTime", reportingPeriod.getEndDate());
            List results = query.getResultList();
            if (results.size() == 0) {
                // no availability history data available
                return null;
            }

            long upTime = 0;
            long downTime = 0;
            Iterator iterator = results.iterator();
            while (iterator.hasNext()) {
                MetricValueHistory mvh = (MetricValueHistory) iterator.next();
                long startTime = mvh.getStartTime().getTime();
                long endTime = mvh.getEndTime().getTime();
                // check if mvh start and end are outside reporting period
                if (startTime < reportingPeriod.getStartDate().getTime()) {
                    startTime = reportingPeriod.getStartDate().getTime();
                }
                if (endTime > reportingPeriod.getEndDate().getTime()) {
                    endTime = reportingPeriod.getEndDate().getTime();
                }

                long interval = endTime - startTime;
                if (Boolean.parseBoolean(mvh.getValue()) == true) {
                    upTime += interval;
                }
                else {
                    downTime += interval;
                }
            }
            long totalTime = upTime + downTime;
            if (totalTime > 0) {
                return 100 * (double) upTime / totalTime;
            }
            else {
                return null;
            }
        }
        finally {
            em.close();
        }
    }

    public static Double computeMttf(Metric availMetric, ReportingPeriod reportingPeriod) {
        EntityManager em = MetricValueHistoryManager.getInstance().getEntityManager();
        Date periodStart = reportingPeriod.getStartDate();
        try {
            Query query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric AND mvh.endTime > :periodStart");
            query.setParameter("metric", availMetric);
            query.setParameter("periodStart", periodStart);
            query.setHint("eclipselink.cursor.scrollable", true);

            ScrollableCursor scrollableCursor;
            try {
                scrollableCursor = (ScrollableCursor) query.getSingleResult();
            }
            catch (NoResultException e) {
                // no availability history data available
                return null;
            }

            List<Long> uptimeIntervals = new ArrayList<Long>();
            List<Long> downtimeIntervals = new ArrayList<Long>();
            Boolean previousState = null;

            while (scrollableCursor.hasNext()) {
                MetricValueHistory mvh = (MetricValueHistory) scrollableCursor.next();

                // calculate interval length
                long interval;
                if (mvh.getStartTime().getTime() < periodStart.getTime()) {
                    interval = mvh.getEndTime().getTime() - periodStart.getTime();
                }
                else {
                    interval = mvh.getEndTime().getTime() - mvh.getStartTime().getTime();
                }

                Boolean state = Boolean.parseBoolean(mvh.getValue());
                if (state == true) {
                    if (state == previousState && uptimeIntervals.size() > 0) {
                        int indexLast = uptimeIntervals.size() - 1;
                        long lastInterval = uptimeIntervals.get(indexLast);
                        lastInterval += interval;
                        uptimeIntervals.set(indexLast, lastInterval);
                    }
                    else {
                        uptimeIntervals.add(interval);
                    }
                }
                else if (state == false) {
                    if (state == previousState && downtimeIntervals.size() > 0) {
                        int indexLast = downtimeIntervals.size() - 1;
                        long lastInterval = downtimeIntervals.get(indexLast);
                        lastInterval += interval;
                        downtimeIntervals.set(indexLast, lastInterval);
                    }
                    else {
                        downtimeIntervals.add(interval);
                    }
                }

                previousState = state;
            }
            Boolean currentAvailState = previousState;

            Double mttf;
            if (downtimeIntervals.size() == 0 && uptimeIntervals.size() == 0) {
                return null;
            }
            else if (downtimeIntervals.size() == 0) {
                mttf = Double.POSITIVE_INFINITY;
            }
            else if (uptimeIntervals.size() == 0) {
                mttf = 0.0;
            }
            else {
                long uptimeIntervalsSum = 0;
                for (Long uptimeInterval : uptimeIntervals) {
                    uptimeIntervalsSum += uptimeInterval;
                }
                // MTTF in hours
                mttf = ((double) uptimeIntervalsSum / uptimeIntervals.size() / 1000 / 3600);
            }

            return mttf;
        }
        finally {
            em.close();
        }
    }

    public static Double computeMttr(Metric availMetric, ReportingPeriod reportingPeriod) {
        EntityManager em = MetricValueHistoryManager.getInstance().getEntityManager();
        Date periodStart = reportingPeriod.getStartDate();
        try {
            Query query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric AND mvh.endTime > :periodStart");
            query.setParameter("metric", availMetric);
            query.setParameter("periodStart", periodStart);
            query.setHint("eclipselink.cursor.scrollable", true);

            ScrollableCursor scrollableCursor;
            try {
                scrollableCursor = (ScrollableCursor) query.getSingleResult();
            }
            catch (NoResultException e) {
                // no availability history data available
                return null;
            }

            List<Long> uptimeIntervals = new ArrayList<Long>();
            List<Long> downtimeIntervals = new ArrayList<Long>();
            Boolean previousState = null;

            while (scrollableCursor.hasNext()) {
                MetricValueHistory mvh = (MetricValueHistory) scrollableCursor.next();

                // calculate interval length
                long interval;
                if (mvh.getStartTime().getTime() < periodStart.getTime()) {
                    interval = mvh.getEndTime().getTime() - periodStart.getTime();
                }
                else {
                    interval = mvh.getEndTime().getTime() - mvh.getStartTime().getTime();
                }

                Boolean state = Boolean.parseBoolean(mvh.getValue());
                if (state == true) {
                    if (state == previousState && uptimeIntervals.size() > 0) {
                        int indexLast = uptimeIntervals.size() - 1;
                        long lastInterval = uptimeIntervals.get(indexLast);
                        lastInterval += interval;
                        uptimeIntervals.set(indexLast, lastInterval);
                    }
                    else {
                        uptimeIntervals.add(interval);
                    }
                }
                else if (state == false) {
                    if (state == previousState && downtimeIntervals.size() > 0) {
                        int indexLast = downtimeIntervals.size() - 1;
                        long lastInterval = downtimeIntervals.get(indexLast);
                        lastInterval += interval;
                        downtimeIntervals.set(indexLast, lastInterval);
                    }
                    else {
                        downtimeIntervals.add(interval);
                    }
                }

                previousState = state;
            }

            // if last state is false (down) then do not use the last downtimeIntervals list element
            // because there was no recovery after that downtime interval
            if (previousState == false) {
                downtimeIntervals.remove(downtimeIntervals.size() - 1);
            }

            Double mttr;
            if (downtimeIntervals.size() == 0) {
                mttr = Double.NaN;
            }
            else {
                long downtimeIntervalsSum = 0;
                for (Long downtimeInterval : downtimeIntervals) {
                    downtimeIntervalsSum += downtimeInterval;
                }
                // MTTR in minutes
                mttr = ((double) downtimeIntervalsSum / downtimeIntervals.size() / 1000 / 60);
            }

            return mttr;
        }
        finally {
            em.close();
        }
    }

    public static Double computeMttv(Metric slaComplianceMetric, ReportingPeriod reportingPeriod) throws Exception {
        EntityManager em = MetricValueHistoryManager.getInstance().getEntityManager();
        Date periodStart = reportingPeriod.getStartDate();
        try {
            Query query = em.createQuery("SELECT mvh FROM MetricValueHistory mvh WHERE mvh.metric = :metric AND mvh.endTime > :periodStart");
            query.setParameter("metric", slaComplianceMetric);
            query.setParameter("periodStart", periodStart);
            query.setHint("eclipselink.cursor.scrollable", true);

            ScrollableCursor scrollableCursor;
            try {
                scrollableCursor = (ScrollableCursor) query.getSingleResult();
            }
            catch (NoResultException e) {
                // no availability history data available
                return null;
            }

            List<Long> okIntervals = new ArrayList<Long>();
            List<Long> violationIntervals = new ArrayList<Long>();
            SLAComplianceEnum previousCompliance = null;

            while (scrollableCursor.hasNext()) {
                MetricValueHistory mvh = (MetricValueHistory) scrollableCursor.next();

                // calculate interval length
                long interval;
                if (mvh.getStartTime().getTime() < periodStart.getTime()) {
                    interval = mvh.getEndTime().getTime() - periodStart.getTime();
                }
                else {
                    interval = mvh.getEndTime().getTime() - mvh.getStartTime().getTime();
                }

                SLAComplianceEnum compliance = SLAComplianceEnum.fromString(mvh.getValue());
                if (compliance.getSeverity() <= SLAComplianceEnum.WARNING.getSeverity()) {
                    if (compliance == previousCompliance && okIntervals.size() > 0) {
                        int indexLast = okIntervals.size() - 1;
                        long lastInterval = okIntervals.get(indexLast);
                        lastInterval += interval;
                        okIntervals.set(indexLast, lastInterval);
                    }
                    else {
                        okIntervals.add(interval);
                    }
                }
                else if (compliance == SLAComplianceEnum.VIOLATION) {
                    if (compliance == previousCompliance && violationIntervals.size() > 0) {
                        int indexLast = violationIntervals.size() - 1;
                        long lastInterval = violationIntervals.get(indexLast);
                        lastInterval += interval;
                        violationIntervals.set(indexLast, lastInterval);
                    }
                    else {
                        violationIntervals.add(interval);
                    }
                }

                previousCompliance = compliance;
            }
            SLAComplianceEnum currentCompliance = previousCompliance;

            Double mttv;
            if (violationIntervals.size() == 0 && okIntervals.size() == 0) {
                mttv = Double.POSITIVE_INFINITY;
            }
            else if (violationIntervals.size() == 0) {
                mttv = Double.POSITIVE_INFINITY;
            }
            else if (okIntervals.size() == 0) {
                mttv = 0.0;
            }
            else {
                long okIntervalsSum = 0;
                for (Long okInterval : okIntervals) {
                    okIntervalsSum += okInterval;
                }
                // MTTF in hours
                mttv = ((double) okIntervalsSum / okIntervals.size() / 1000 / 3600);
            }

            return mttv;
        }
        finally {
            em.close();
        }
    }
}
