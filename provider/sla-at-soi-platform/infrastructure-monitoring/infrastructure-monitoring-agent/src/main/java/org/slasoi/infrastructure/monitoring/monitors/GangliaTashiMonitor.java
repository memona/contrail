/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3048 $
 * @lastrevision $Date: 2011-09-05 10:26:14 +0200 (pon, 05 sep 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/monitors/GangliaTashiMonitor.java $
 */

package org.slasoi.infrastructure.monitoring.monitors;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.exceptions.MetricNotSupportedException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.monitors.data.MetricData;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;
import org.slasoi.infrastructure.monitoring.monitors.ganglia.GangliaMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.tashisensor.TashiSensorMonitor;

import java.util.*;

public class GangliaTashiMonitor implements IMonitoringEngine {
    private static Logger log = Logger.getLogger(GangliaTashiMonitor.class);
    private GangliaMonitoringEngine gangliaMonitor;
    private TashiSensorMonitor tashiSensorMonitor;
    Status status;
    Date metricsCollectedDate;

    private static RawMetric[] gangliaHostMetrics = {RawMetric.MEM_FREE, RawMetric.MEM_TOTAL, RawMetric.CPU_SPEED, RawMetric.CPU_SYSTEM,
            RawMetric.CPU_USER, RawMetric.CPU_IDLE, RawMetric.DISK_FREE, RawMetric.DISK_TOTAL, RawMetric.IP_ADDRESS,
            RawMetric.LOAD_ONE, RawMetric.LOAD_FIVE};
    private static final Set<RawMetric> gangliaHostMetricsSet = new HashSet<RawMetric>(Arrays.asList(gangliaHostMetrics));

    private static RawMetric[] gangliaVmMetrics = {RawMetric.MEM_USAGE, RawMetric.CPU_LOAD};
    private static final Set<RawMetric> gangliaVmMetricsSet = new HashSet<RawMetric>(Arrays.asList(gangliaVmMetrics));

    private static RawMetric[] tashiHostMetrics = {RawMetric.HOSTNAME, RawMetric.AUDITABILITY, RawMetric.LOCATION, RawMetric.SAS70_COMPLIANCE, RawMetric.CCR,
            RawMetric.DATA_CLASSIFICATION, RawMetric.HW_REDUNDANCY_LEVEL, RawMetric.DISK_THROUGHPUT, RawMetric.NET_THROUGHPUT,
            RawMetric.DATA_ENCRYPTION, RawMetric.AVAILABILITY_STATUS, RawMetric.CPU_CORES_COUNT,
            RawMetric.SHARED_IMAGES_DISK_FREE, RawMetric.SHARED_IMAGES_DISK_USED, RawMetric.POWER_STATUS};
    private static final Set<RawMetric> tashiHostMetricsSet = new HashSet<RawMetric>(Arrays.asList(tashiHostMetrics));

    private static RawMetric[] tashiVmMetrics = {RawMetric.HOSTNAME, RawMetric.AVAILABILITY_STATUS, RawMetric.CPU_CORES_COUNT,
            RawMetric.MEM_TOTAL, RawMetric.VM_STATE, RawMetric.CPU_CORES_COUNT, RawMetric.CPU_SPEED, RawMetric.MEM_TOTAL,
            RawMetric.VM_IMAGE_TEMPLATE, RawMetric.VM_PERSISTENCE};
    private static final Set<RawMetric> tashiVmMetricsSet = new HashSet<RawMetric>(Arrays.asList(tashiVmMetrics));

    public GangliaTashiMonitor(GangliaMonitoringEngine gangliaMonitor, TashiSensorMonitor tashiSensorMonitor) {
        this.gangliaMonitor = gangliaMonitor;
        this.tashiSensorMonitor = tashiSensorMonitor;
        this.status = IMonitoringEngine.Status.BUSY;
    }

    @Override
    public void query() throws Exception {
        log.trace("Collecting metrics data...");
        this.status = IMonitoringEngine.Status.BUSY;
        try {
            tashiSensorMonitor.queryTashi();
            gangliaMonitor.queryGanglia();
        }
        catch (Exception e) {
            this.status = IMonitoringEngine.Status.ERROR;
            throw new Exception("Failed to collect monitoring data: " + e.getMessage(), e);
        }
        metricsCollectedDate = new Date();
        this.status = IMonitoringEngine.Status.OK;
        log.trace("Metrics data collected successfully.");
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public Date getMetricsCollectedDate() {
        return metricsCollectedDate;
    }

    @Override
    public MetricData getVmMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException {
        if (tashiVmMetricsSet.contains(rawMetric)) {
            return tashiSensorMonitor.getVmMetricData(fqdn, rawMetric);
        }
        else if (gangliaVmMetricsSet.contains(rawMetric)) {
            return gangliaMonitor.getVmMetricData(fqdn, rawMetric);
        }
        else {
            throw new MetricNotSupportedException("Metric " + rawMetric + " is not supported.");
        }
    }

    @Override
    public MetricData getHostMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException {
        if (tashiHostMetricsSet.contains(rawMetric)) {
            return tashiSensorMonitor.getHostMetricData(fqdn, rawMetric);
        }
        else if (gangliaHostMetricsSet.contains(rawMetric)) {
            return gangliaMonitor.getHostMetricData(fqdn, rawMetric);
        }
        else {
            throw new MetricNotSupportedException("Metric " + rawMetric + " is not supported.");
        }
    }

    @Override
    public boolean checkVmExists(String fqdn) {
        return tashiSensorMonitor.checkVmExists(fqdn);
    }

    @Override
    public boolean checkHostExists(String fqdn) {
        return tashiSensorMonitor.checkHostExists(fqdn);
    }

    @Override
    public String getVmHostMachine(String fqdn) {
        return tashiSensorMonitor.getVmHostMachine(fqdn);
    }

    @Override
    public List<String> getGuestMachines(String hostFqdn) {
        List<String> vmListTashi = tashiSensorMonitor.getGuestMachines(hostFqdn);

        // check if monitoring data is also provided by Ganglia for these VMs
        // return only those VMs which are reported by both Tashi and Ganglia
        List<String> vmList = new ArrayList<String>();
        for (String vmFqdn : vmListTashi) {
            if (gangliaMonitor.isVmMonitoringDataAvailable(vmFqdn)) {
                vmList.add(vmFqdn);
            }
        }
        return vmList;
    }

    @Override
    public List<String> getAllHostMachines() {
        return tashiSensorMonitor.getAllHostMachines();
    }

    @Override
    public List<String> getAllGuestMachines() {
        return tashiSensorMonitor.getAllGuestMachines();
    }

    @Override
    public String getInfrastructureLayoutXml() {
        return tashiSensorMonitor.getInfrastructureLayoutXml();
    }

    @Override
    public boolean isMonitoringDataAvailable(Service service) {
        for (Vm vm : service.getVmList()) {
            if (!tashiSensorMonitor.isVmMonitoringDataAvailable(vm.getFqdn()) ||
                    !gangliaMonitor.isVmMonitoringDataAvailable(vm.getFqdn())) {
                return false;
            }
        }
        return true;
    }
}
