/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3048 $
 * @lastrevision $Date: 2011-09-05 10:26:14 +0200 (pon, 05 sep 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/monitors/tashisensor/TashiSensorMonitor.ja $
 */

package org.slasoi.infrastructure.monitoring.monitors.tashisensor;

import org.apache.log4j.Logger;
import org.slasoi.common.messaging.pointtopoint.Message;
import org.slasoi.common.messaging.pointtopoint.Messaging;
import org.slasoi.infrastructure.monitoring.exceptions.MetricNotSupportedException;
import org.slasoi.infrastructure.monitoring.monitors.data.MetricData;
import org.slasoi.infrastructure.monitoring.monitors.data.MonitoringData;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;
import org.slasoi.infrastructure.monitoring.monitors.data.VmData;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TashiSensorMonitor {
    private static Logger log = Logger.getLogger(TashiSensorMonitor.class);
    private static int MAX_WAIT_TIME = 20;
    private Messaging messaging;
    private String xmppTashiUser;
    private MonitoringData vmLayoutData;
    private MonitoringData clusterConfData;
    private String infrastructureLayoutXml;
    private boolean gotVmLayoutData = false;
    private boolean gotClusterConfData = false;

    private static RawMetric[] clusterConfHostMetrics = {RawMetric.AUDITABILITY, RawMetric.LOCATION, RawMetric.SAS70_COMPLIANCE, RawMetric.CCR,
            RawMetric.DATA_CLASSIFICATION, RawMetric.HW_REDUNDANCY_LEVEL, RawMetric.DISK_THROUGHPUT, RawMetric.NET_THROUGHPUT, RawMetric.DATA_ENCRYPTION};
    private static final Set<RawMetric> clusterConfHostMetricsSet = new HashSet<RawMetric>(Arrays.asList(clusterConfHostMetrics));

    private static RawMetric[] vmLayoutHostMetrics = {RawMetric.HOSTNAME, RawMetric.AVAILABILITY_STATUS,
            RawMetric.CPU_CORES_COUNT, RawMetric.CPU_SPEED, RawMetric.MEM_TOTAL, RawMetric.SHARED_IMAGES_DISK_FREE,
            RawMetric.SHARED_IMAGES_DISK_USED, RawMetric.POWER_STATUS};
    private static final Set<RawMetric> vmLayoutHostMetricsSet = new HashSet<RawMetric>(Arrays.asList(vmLayoutHostMetrics));

    private static RawMetric[] vmLayoutVmMetrics = {RawMetric.HOSTNAME, RawMetric.VM_STATE, RawMetric.AVAILABILITY_STATUS, RawMetric.CPU_CORES_COUNT, RawMetric.CPU_SPEED, RawMetric.MEM_TOTAL,
            RawMetric.VM_IMAGE_TEMPLATE, RawMetric.VM_PERSISTENCE, RawMetric.MEM_TOTAL};
    private static final Set<RawMetric> vmLayoutVmMetricsSet = new HashSet<RawMetric>(Arrays.asList(vmLayoutVmMetrics));

    public TashiSensorMonitor(Messaging messaging, String xmppTashiUser) throws Exception {
        log.trace("TashiSensorMonitor constructor started.");
        this.messaging = messaging;
        this.xmppTashiUser = xmppTashiUser;

        TashiSensorHandler tashiSensorHandler = new TashiSensorHandler(this);
        TashiSensorListener tashiSensorListener = new TashiSensorListener(tashiSensorHandler);
        messaging.addMessageListener(tashiSensorListener);
    }

    public void queryTashi() throws Exception {
        log.trace("queryTashi() started.");

        log.trace("Querying Tashi for VmLayoutData...");
        gotVmLayoutData = false;
        sendCommand("listVmLayout");

        log.trace("Querying Tashi for ClusterConfData...");
        gotClusterConfData = false;
        sendCommand("listClusterConfig");

        log.trace("Waiting for response from Tashi...");
        long startTime = System.currentTimeMillis();
        while ((!gotVmLayoutData || !gotClusterConfData) && System.currentTimeMillis() - startTime < MAX_WAIT_TIME * 1000) {
            Thread.sleep(50);
        }

        if (!gotVmLayoutData && !gotClusterConfData) {
            throw new Exception("Failed to query Tashi: no response received.");
        }
        else if (!gotVmLayoutData || !gotClusterConfData) {
            throw new Exception(String.format("Failed to query Tashi: incomplete response received: VmLayoutData %s, ClusterConfData %s.",
                    (gotVmLayoutData ? "ok" : "missing"),
                    (gotClusterConfData ? "ok" : "missing")));
        }
        else {
            log.trace("Tashi queried successfully. Got VmLayoutData and ClusterConfData.");
        }

        log.trace("queryTashi() finished successfully.");
    }

    private void sendCommand(String command) {
        log.trace(String.format("Sending command '%s' to Tashi sensor.", command));
        Message message = new Message(xmppTashiUser, command);
        messaging.sendMessage(message);
    }

    public MonitoringData getVmLayoutData() {
        return vmLayoutData;
    }

    public void setVmLayoutData(MonitoringData vmLayoutData) {
        this.vmLayoutData = vmLayoutData;
    }

    public String getInfrastructureLayoutXml() {
        return infrastructureLayoutXml;
    }

    public MonitoringData getClusterConfData() {
        return clusterConfData;
    }

    public void setClusterConfData(MonitoringData clusterConfData) {
        this.clusterConfData = clusterConfData;
    }

    public void setInfrastructureLayoutXml(String infrastructureLayoutXml) {
        this.infrastructureLayoutXml = infrastructureLayoutXml;
    }

    public Messaging getMessaging() {
        return messaging;
    }

    public void setMessaging(Messaging messaging) {
        this.messaging = messaging;
    }

    public String getXmppTashiUser() {
        return xmppTashiUser;
    }

    public void setXmppTashiUser(String xmppTashiUser) {
        this.xmppTashiUser = xmppTashiUser;
    }

    public boolean getGotVmLayoutData() {
        return gotVmLayoutData;
    }

    public void setGotVmLayoutData(boolean gotVmLayoutData) {
        this.gotVmLayoutData = gotVmLayoutData;
    }

    public boolean getGotClusterConfData() {
        return gotClusterConfData;
    }

    public void setGotClusterConfData(boolean gotClusterConfData) {
        this.gotClusterConfData = gotClusterConfData;
    }

    public MetricData getVmMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException {
        if (vmLayoutVmMetricsSet.contains(rawMetric)) {
            return vmLayoutData.getVmData(fqdn).getMetricData(rawMetric);
        }
        else {
            throw new MetricNotSupportedException("Metric " + rawMetric + " is not supported by Tashi.");
        }
    }

    public MetricData getHostMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException {
        if (clusterConfHostMetricsSet.contains(rawMetric)) {
            return clusterConfData.getHostData(fqdn).getMetricData(rawMetric);
        }
        else if (vmLayoutHostMetricsSet.contains(rawMetric)) {
            return vmLayoutData.getHostData(fqdn).getMetricData(rawMetric);
        }
        else {
            throw new MetricNotSupportedException("Metric " + rawMetric + " is not supported by Tashi.");
        }
    }

    public String getVmHostMachine(String fqdn) {
        VmData vmData = vmLayoutData.getVmData(fqdn);
        if (vmData != null) {
            return vmLayoutData.getVmData(fqdn).getHostFqdn();
        }
        else {
            return null;
        }
    }

    public boolean checkVmExists(String fqdn) {
        return vmLayoutData.containsVmData(fqdn);
    }

    public boolean checkHostExists(String fqdn) {
        return vmLayoutData.getHostData(fqdn) != null;
    }

    public List<String> getGuestMachines(String hostFqdn) {
        return vmLayoutData.getHostData(hostFqdn).getVmList();
    }

    public List<String> getAllHostMachines() {
        return vmLayoutData.getHostList();
    }

    public List<String> getAllGuestMachines() {
        return vmLayoutData.getVmList();
    }

    public boolean isVmMonitoringDataAvailable(String fqdn) {
        return vmLayoutData.containsVmData(fqdn);
    }
}
