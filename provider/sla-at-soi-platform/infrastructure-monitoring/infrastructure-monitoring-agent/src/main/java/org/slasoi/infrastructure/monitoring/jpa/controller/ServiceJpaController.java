/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1406 $
 * @lastrevision $Date: 2011-04-15 13:48:15 +0200 (pet, 15 apr 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/controller/ServiceJpaController.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.controller;

import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.IllegalOrphanException;
import org.slasoi.infrastructure.monitoring.jpa.controller.exceptions.NonexistentEntityException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class ServiceJpaController {

    public ServiceJpaController() {
        emf = Persistence.createEntityManagerFactory("InfrastructureMonitoringPU");
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Service service) {
        if (service.getVmList() == null) {
            service.setVmList(new ArrayList<Vm>());
        }
        if (service.getMetricList() == null) {
            service.setMetricList(new ArrayList<Metric>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Vm> attachedVmList = new ArrayList<Vm>();
            for (Vm vmListVmToAttach : service.getVmList()) {
                vmListVmToAttach = em.getReference(vmListVmToAttach.getClass(), vmListVmToAttach.getVmId());
                attachedVmList.add(vmListVmToAttach);
            }
            service.setVmList(attachedVmList);
            List<Metric> attachedMetricList = new ArrayList<Metric>();
            for (Metric metricListMetricToAttach : service.getMetricList()) {
                metricListMetricToAttach = em.getReference(metricListMetricToAttach.getClass(), metricListMetricToAttach.getMetricId());
                attachedMetricList.add(metricListMetricToAttach);
            }
            service.setMetricList(attachedMetricList);
            em.persist(service);
            for (Vm vmListVm : service.getVmList()) {
                Service oldServiceOfVmListVm = vmListVm.getService();
                vmListVm.setService(service);
                vmListVm = em.merge(vmListVm);
                if (oldServiceOfVmListVm != null) {
                    oldServiceOfVmListVm.getVmList().remove(vmListVm);
                    oldServiceOfVmListVm = em.merge(oldServiceOfVmListVm);
                }
            }
            for (Metric metricListMetric : service.getMetricList()) {
                Service oldServiceOfMetricListMetric = metricListMetric.getService();
                metricListMetric.setService(service);
                metricListMetric = em.merge(metricListMetric);
                if (oldServiceOfMetricListMetric != null) {
                    oldServiceOfMetricListMetric.getMetricList().remove(metricListMetric);
                    oldServiceOfMetricListMetric = em.merge(oldServiceOfMetricListMetric);
                }
            }
            em.getTransaction().commit();
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Service service) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Service persistentService = em.find(Service.class, service.getServiceId());
            List<Vm> vmListOld = persistentService.getVmList();
            List<Vm> vmListNew = service.getVmList();
            List<Metric> metricListOld = persistentService.getMetricList();
            List<Metric> metricListNew = service.getMetricList();
            List<String> illegalOrphanMessages = null;
            for (Vm vmListOldVm : vmListOld) {
                if (!vmListNew.contains(vmListOldVm)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Vm " + vmListOldVm + " since its service field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Vm> attachedVmListNew = new ArrayList<Vm>();
            for (Vm vmListNewVmToAttach : vmListNew) {
                vmListNewVmToAttach = em.getReference(vmListNewVmToAttach.getClass(), vmListNewVmToAttach.getVmId());
                attachedVmListNew.add(vmListNewVmToAttach);
            }
            vmListNew = attachedVmListNew;
            service.setVmList(vmListNew);
            List<Metric> attachedMetricListNew = new ArrayList<Metric>();
            for (Metric metricListNewMetricToAttach : metricListNew) {
                metricListNewMetricToAttach = em.getReference(metricListNewMetricToAttach.getClass(), metricListNewMetricToAttach.getMetricId());
                attachedMetricListNew.add(metricListNewMetricToAttach);
            }
            metricListNew = attachedMetricListNew;
            service.setMetricList(metricListNew);
            service = em.merge(service);
            for (Vm vmListNewVm : vmListNew) {
                if (!vmListOld.contains(vmListNewVm)) {
                    Service oldServiceOfVmListNewVm = vmListNewVm.getService();
                    vmListNewVm.setService(service);
                    vmListNewVm = em.merge(vmListNewVm);
                    if (oldServiceOfVmListNewVm != null && !oldServiceOfVmListNewVm.equals(service)) {
                        oldServiceOfVmListNewVm.getVmList().remove(vmListNewVm);
                        oldServiceOfVmListNewVm = em.merge(oldServiceOfVmListNewVm);
                    }
                }
            }
            for (Metric metricListOldMetric : metricListOld) {
                if (!metricListNew.contains(metricListOldMetric)) {
                    metricListOldMetric.setService(null);
                    metricListOldMetric = em.merge(metricListOldMetric);
                }
            }
            for (Metric metricListNewMetric : metricListNew) {
                if (!metricListOld.contains(metricListNewMetric)) {
                    Service oldServiceOfMetricListNewMetric = metricListNewMetric.getService();
                    metricListNewMetric.setService(service);
                    metricListNewMetric = em.merge(metricListNewMetric);
                    if (oldServiceOfMetricListNewMetric != null && !oldServiceOfMetricListNewMetric.equals(service)) {
                        oldServiceOfMetricListNewMetric.getMetricList().remove(metricListNewMetric);
                        oldServiceOfMetricListNewMetric = em.merge(oldServiceOfMetricListNewMetric);
                    }
                }
            }
            em.getTransaction().commit();
        }
        catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = service.getServiceId();
                if (findService(id) == null) {
                    throw new NonexistentEntityException("The service with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Service service;
            try {
                service = em.getReference(Service.class, id);
                service.getServiceId();
            }
            catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The service with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Vm> vmListOrphanCheck = service.getVmList();
            for (Vm vmListOrphanCheckVm : vmListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Service (" + service + ") cannot be destroyed since the Vm " + vmListOrphanCheckVm + " in its vmList field has a non-nullable service field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Metric> metricList = service.getMetricList();
            for (Metric metricListMetric : metricList) {
                metricListMetric.setService(null);
                metricListMetric = em.merge(metricListMetric);
            }
            em.remove(service);
            em.getTransaction().commit();
        }
        finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Service> findServiceEntities() {
        return findServiceEntities(true, -1, -1);
    }

    public List<Service> findServiceEntities(int maxResults, int firstResult) {
        return findServiceEntities(false, maxResults, firstResult);
    }

    private List<Service> findServiceEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Service.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        }
        finally {
            em.close();
        }
    }

    public Service findService(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Service.class, id);
        }
        finally {
            em.close();
        }
    }

    public int getServiceCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Service> rt = cq.from(Service.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        }
        finally {
            em.close();
        }
    }

}
