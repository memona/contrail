/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 3034 $
 * @lastrevision $Date: 2011-08-29 14:30:56 +0200 (pon, 29 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/ServiceReportingInter $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.enums.ReportingInterval;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("SERVICE_REPORTING_INTERVAL")
public class ServiceReportingInterval extends Metric {
    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        throw new UnsupportedOperationException("Operation not supported for config QoS terms.");
    }

    @Override
    public ReportingInterval parseValue(String value) throws InvalidMetricValueException {
        return ReportingInterval.fromString(value);
    }

    @Override
    public String formatValue(Object value) {
        return value.toString();
    }

    @Override
    public String displayValue(String value) throws InvalidMetricValueException {
        ReportingInterval enumValue = parseValue(value);
        return enumValue.getTitle();
    }

    @Override
    public ReportingInterval getViolationThresholdValue() throws InvalidMetricValueException {
        return ReportingInterval.fromString(this.getConfigValue());
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getViolationThresholdWithUnit() throws InvalidMetricValueException {
        return getViolationThresholdValue().getTitle();
    }

    @Override
    public Object getWarningThresholdValue() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getWarningCondition() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object estimateWarningThreshold(Object violationThreshold) {
        return null;
    }
}
