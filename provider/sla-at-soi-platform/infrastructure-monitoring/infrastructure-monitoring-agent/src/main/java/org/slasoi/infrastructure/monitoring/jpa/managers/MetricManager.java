/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2958 $
 * @lastrevision $Date: 2011-08-11 11:18:14 +0200 (čet, 11 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/managers/MetricManager.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.managers;

import org.slasoi.infrastructure.monitoring.jpa.controller.MetricJpaController;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricType;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class MetricManager extends MetricJpaController {
    private static MetricManager instance;

    private MetricManager() {

    }

    public static MetricManager getInstance() {
        if (instance == null) {
            instance = new MetricManager();
        }
        return instance;
    }

    public Metric findVmMetric(MetricTypeEnum metricTypeEnum, Vm vm) {
        MetricType metricType = metricTypeEnum.getMetricType();
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT m FROM Metric m WHERE m.metricType = :metricType AND m.service IS NULL AND m.vm = :vm");
            query.setParameter("metricType", metricType);
            query.setParameter("vm", vm);
            return (Metric) query.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            em.close();
        }
    }

    public Metric findServiceMetric(MetricTypeEnum metricTypeEnum, Service service) {
        MetricType metricType = metricTypeEnum.getMetricType();
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT m FROM Metric m WHERE m.metricType = :metricType AND m.service = :service AND m.vm IS NULL");
            query.setParameter("metricType", metricType);
            query.setParameter("service", service);
            return (Metric) query.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            em.close();
        }
    }

    public Metric findInfrastructureMetric(MetricTypeEnum metricTypeEnum) {
        MetricType metricType = metricTypeEnum.getMetricType();
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT m FROM Metric m WHERE m.metricType = :metricType");
            query.setParameter("metricType", metricType);
            return (Metric) query.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            em.close();
        }
    }

    public List findInfrastructureMetrics() {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT m FROM Metric m WHERE m.metricType.target = :target");
            query.setParameter("target", MetricType.Target.INFRASTRUCTURE);
            return query.getResultList();
        }
        catch (NoResultException e) {
            return null;
        }
        finally {
            em.close();
        }
    }

    public void registerInfrastructureMetrics() throws Exception {
        EntityManager em = getEntityManager();
        List<MetricType> infrMetricTypes;
        try {
            Query query = em.createQuery("SELECT mt FROM MetricType mt WHERE mt.target = :target");
            query.setParameter("target", MetricType.Target.INFRASTRUCTURE);
            infrMetricTypes = (List<MetricType>) query.getResultList();
        }
        catch (NoResultException e) {
            return;
        }
        finally {
            em.close();
        }

        for (MetricType metricType : infrMetricTypes) {
            MetricTypeEnum metricTypeEnum = MetricTypeEnum.convert(metricType);

            // check if metric is already registered
            if (findInfrastructureMetric(metricTypeEnum) != null) {
                continue;
            }

            Metric metric = Metric.create(metricTypeEnum);
            MetricManager.getInstance().create(metric);
        }
    }
}
