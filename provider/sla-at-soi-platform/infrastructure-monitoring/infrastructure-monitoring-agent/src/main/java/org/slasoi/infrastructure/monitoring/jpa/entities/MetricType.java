/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2958 $
 * @lastrevision $Date: 2011-08-11 11:18:14 +0200 (čet, 11 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/MetricType.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities;

import org.slasoi.infrastructure.monitoring.jpa.enums.DataType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "metric_type")
@NamedQueries({
        @NamedQuery(name = "MetricType.findAll", query = "SELECT m FROM MetricType m"),
        @NamedQuery(name = "MetricType.findByMetricTypeId", query = "SELECT m FROM MetricType m WHERE m.metricTypeId = :metricTypeId"),
        @NamedQuery(name = "MetricType.findByName", query = "SELECT m FROM MetricType m WHERE m.name = :name"),
        @NamedQuery(name = "MetricType.findByTitle", query = "SELECT m FROM MetricType m WHERE m.title = :title"),
        @NamedQuery(name = "MetricType.findByDescription", query = "SELECT m FROM MetricType m WHERE m.description = :description"),
        @NamedQuery(name = "MetricType.findByQosTermUri", query = "SELECT m FROM MetricType m WHERE m.qosTermUri = :qosTermUri"),
        @NamedQuery(name = "MetricType.findByMetricUnit", query = "SELECT m FROM MetricType m WHERE m.metricUnit = :metricUnit"),
        @NamedQuery(name = "MetricType.findByTarget", query = "SELECT m FROM MetricType m WHERE m.target = :target")})
public class MetricType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "metric_type_id", nullable = false)
    private Integer metricTypeId;
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 50)
    private String name;
    @Column(name = "title", length = 50)
    private String title;
    @Column(name = "description", length = 100)
    private String description;
    @Column(name = "qos_term_uri", length = 50)
    private String qosTermUri;
    @Column(name = "metric_unit", length = 50)
    private String metricUnit;
    @Column(name = "target")
    @Enumerated(EnumType.STRING)
    private Target target;
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private Type type;
    @Column(name = "is_critical")
    private Boolean isCritical;
    @Column(name = "data_type")
    @Enumerated(EnumType.STRING)
    private DataType dataType;
    @Column(name = "computation_order", nullable = false)
    private Integer computationOrder;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "metricType")
    private List<Metric> metricList;

    public MetricType() {
    }

    public MetricType(Integer metricTypeId) {
        this.metricTypeId = metricTypeId;
    }

    public MetricType(Integer metricTypeId, String name) {
        this.metricTypeId = metricTypeId;
        this.name = name;
    }

    public Integer getMetricTypeId() {
        return metricTypeId;
    }

    public void setMetricTypeId(Integer metricTypeId) {
        this.metricTypeId = metricTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQosTermUri() {
        return qosTermUri;
    }

    public void setQosTermUri(String qosTermUri) {
        this.qosTermUri = qosTermUri;
    }

    public String getMetricUnit() {
        return metricUnit;
    }

    public void setMetricUnit(String metricUnit) {
        this.metricUnit = metricUnit;
    }

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Boolean isCritical() {
        return isCritical;
    }

    public void setCritical(Boolean critical) {
        this.isCritical = critical;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public Integer getComputationOrder() {
        return computationOrder;
    }

    public void setComputationOrder(Integer computationOrder) {
        this.computationOrder = computationOrder;
    }

    public List<Metric> getMetricList() {
        return metricList;
    }

    public void setMetricList(List<Metric> metricList) {
        this.metricList = metricList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (metricTypeId != null ? metricTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MetricType)) {
            return false;
        }
        MetricType other = (MetricType) object;
        if ((this.metricTypeId == null && other.metricTypeId != null) || (this.metricTypeId != null && !this.metricTypeId.equals(other.metricTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.slasoi.infrastructure.monitoring.jpa.entities.MetricType[metricTypeId=" + metricTypeId + "]";
    }

    public static enum Target {
        VM,
        SERVICE,
        INFRASTRUCTURE;
    }

    public static enum Type {
        METRIC,
        QOS_TERM,
        CONFIG;
    }
}
