/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2818 $
 * @lastrevision $Date: 2011-07-26 08:32:55 +0200 (tor, 26 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/monitors/tashisensor/TashiSensorMockup.jav $
 */

package org.slasoi.infrastructure.monitoring.monitors.tashisensor;

import org.apache.log4j.Logger;
import org.slasoi.common.messaging.pointtopoint.Message;
import org.slasoi.common.messaging.pointtopoint.MessageEvent;
import org.slasoi.common.messaging.pointtopoint.MessageListener;
import org.slasoi.common.messaging.pointtopoint.Messaging;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

public class TashiSensorMockup implements Runnable {
    private static Logger log = Logger.getLogger(TashiSensorMockup.class);
    private static TashiSensorMockup instance;
    private Messaging messaging;
    private String imaXmppUser;
    private String mockupVmLayoutXmlFilePath;
    private String mockupClusterConfMessageFilePath;
    List<String> mockupAuditRecordMessages;
    private Thread thread;

    public TashiSensorMockup(Messaging tashiMockupSensorMessaging, String imaXmppUser, String mockupVmLayoutXmlFilePath,
                             String mockupClusterConfMessageFilePath, List<String> mockupAuditRecordMessages) {
        log.trace("Starting mockup Tashi sensor...");
        if (instance != null) {
            throw new RuntimeException("Only one instance of TashiSensorMockup is allowed.");
        }
        messaging = tashiMockupSensorMessaging;
        this.imaXmppUser = imaXmppUser;
        this.mockupVmLayoutXmlFilePath = mockupVmLayoutXmlFilePath;
        this.mockupClusterConfMessageFilePath = mockupClusterConfMessageFilePath;
        this.mockupAuditRecordMessages = mockupAuditRecordMessages;

        log.trace("Adding message listener TashiSensorMockupListener.");
        TashiSensorMockupListener listener = new TashiSensorMockupListener(messaging, imaXmppUser, mockupVmLayoutXmlFilePath, mockupClusterConfMessageFilePath);
        tashiMockupSensorMessaging.addMessageListener(listener);
        instance = this;
        log.trace("Mockup Tashi sensor started successfully.");
    }

    public static TashiSensorMockup getInstance() {
        return instance;
    }

    public void start() {
        thread = new Thread(this);
        thread.start();
    }

    public void run() {
        try {
            Thread.sleep(2000);
            for (String filePath : mockupAuditRecordMessages) {
                sendMockMessage(filePath, imaXmppUser);
                Thread.sleep(1000);
            }
        }
        catch (InterruptedException e) {
            log.trace("TashiSensorMockup thread interrupted.");
        }
        catch (Exception e) {
            log.error(e);
        }
    }

    public void stop() {
        log.trace("Stopping mockup Tashi sensor...");
        if (thread != null) {
            thread.interrupt();
            try {
                thread.join();
            }
            catch (InterruptedException e) {
                log.error(e);
            }
        }
        log.trace("Tashi mockup sensor stopped successfully.");
    }

    private void sendMockMessage(String fileName, String sendToUser) throws Exception {
        log.trace("Sending mock message from file " + fileName);
        File file = new File(fileName);
        byte[] fileBArray = new byte[(int) file.length()];
        FileInputStream fis = new FileInputStream(file);
        fis.read(fileBArray);
        String content = new String(fileBArray);

        log.trace(String.format("Sending message to %s:\n%s", sendToUser, content));
        messaging.sendMessage(new Message(sendToUser, content));
        log.trace("Message sent successfully.");
    }
}

class TashiSensorMockupListener implements MessageListener {
    private static Logger log = Logger.getLogger(TashiSensorMockup.class);
    private Messaging messaging;
    private String sendToUser;
    private String mockupVmLayoutMessageFilePath;
    private String mockupClusterConfMessageFilePath;

    TashiSensorMockupListener(Messaging messaging, String imaXmppUser, String mockupVmLayoutMessageFilePath,
                              String mockupClusterConfMessageFilePath) {
        log.trace("Creating TashiSensorMockupListener.");
        this.messaging = messaging;
        this.sendToUser = imaXmppUser;
        this.mockupVmLayoutMessageFilePath = mockupVmLayoutMessageFilePath;
        this.mockupClusterConfMessageFilePath = mockupClusterConfMessageFilePath;
    }

    public void processMessage(MessageEvent messageEvent) {
        try {
            Message message = messageEvent.getMessage();
            String payload = message.getPayload();
            log.trace(String.format("New message arrived from %s: %s", message.getFrom(), payload));
            if (payload == null) {
                return;
            }
            else if (payload.equals("listVmLayout")) {
                log.trace("Message command is listVmLayout.");
                File file = new File(mockupVmLayoutMessageFilePath);
                byte[] fileBArray = new byte[(int) file.length()];
                FileInputStream fis = new FileInputStream(file);
                fis.read(fileBArray);
                String content = new String(fileBArray);

                log.trace(String.format("Sending message to %s:\n%s", sendToUser, content));
                messaging.sendMessage(new Message(sendToUser, content));
            }
            else if (payload.equals("listClusterConfig")) {
                log.trace("Message command is listClusterConfig.");
                File file = new File(mockupClusterConfMessageFilePath);
                byte[] fileBArray = new byte[(int) file.length()];
                FileInputStream fis = new FileInputStream(file);
                fis.read(fileBArray);
                String content = new String(fileBArray);

                log.trace(String.format("Sending message to %s:\n%s", sendToUser, content));
                messaging.sendMessage(new Message(sendToUser, content));
            }
        }
        catch (Exception e) {
            log.error("processMessage() failed.", e);
        }
    }
}