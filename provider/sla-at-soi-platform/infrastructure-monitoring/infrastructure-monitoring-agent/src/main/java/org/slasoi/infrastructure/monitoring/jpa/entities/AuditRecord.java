/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 1948 $
 * @lastrevision $Date: 2011-05-31 17:21:17 +0200 (tor, 31 maj 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/AuditRecord.java $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "audit_record", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"audit_record_id"})})
@NamedQueries({
        @NamedQuery(name = "AuditRecord.findAll", query = "SELECT a FROM AuditRecord a"),
        @NamedQuery(name = "AuditRecord.findByAuditRecordId", query = "SELECT a FROM AuditRecord a WHERE a.auditRecordId = :auditRecordId"),
        @NamedQuery(name = "AuditRecord.findBySource", query = "SELECT a FROM AuditRecord a WHERE a.source = :source"),
        @NamedQuery(name = "AuditRecord.findByUserId", query = "SELECT a FROM AuditRecord a WHERE a.userId = :userId"),
        @NamedQuery(name = "AuditRecord.findByDescription", query = "SELECT a FROM AuditRecord a WHERE a.description = :description"),
        @NamedQuery(name = "AuditRecord.findByTimestamp", query = "SELECT a FROM AuditRecord a WHERE a.timestamp = :timestamp")})
public class AuditRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "audit_record_id", nullable = false)
    private Integer auditRecordId;
    @Column(name = "source", length = 20)
    private String source;
    @Column(name = "user_id", length = 20)
    private String userId;
    @Column(name = "fqdn", length = 50)
    private String fqdn;
    @Column(name = "description", length = 100)
    private String description;
    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    public AuditRecord() {
    }

    public AuditRecord(Integer auditRecordId) {
        this.auditRecordId = auditRecordId;
    }

    public Integer getAuditRecordId() {
        return auditRecordId;
    }

    public void setAuditRecordId(Integer auditRecordId) {
        this.auditRecordId = auditRecordId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFqdn() {
        return fqdn;
    }

    public void setFqdn(String fqdn) {
        this.fqdn = fqdn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (auditRecordId != null ? auditRecordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuditRecord)) {
            return false;
        }
        AuditRecord other = (AuditRecord) object;
        if ((this.auditRecordId == null && other.auditRecordId != null) || (this.auditRecordId != null && !this.auditRecordId.equals(other.auditRecordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.slasoi.infrastructure.monitoring.jpa.entities.AuditRecord[auditRecordId=" + auditRecordId + "]";
    }
}
