/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2818 $
 * @lastrevision $Date: 2011-07-26 08:32:55 +0200 (tor, 26 jul 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/pubsub/messages/GetServiceSLASummaryRespon $
 */

package org.slasoi.infrastructure.monitoring.pubsub.messages;

import com.google.gson.Gson;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.utils.JsonUtils;

import java.util.ArrayList;
import java.util.List;

public class GetServiceSLASummaryResponse extends PubSubResponse {
    private String serviceUri;
    private List<QoSTermInfo> qosTerms;
    private List<ResourceInfo> resources;

    public GetServiceSLASummaryResponse() {
        this.setResponseType("GetServiceSLASummaryResponse");
        qosTerms = new ArrayList<QoSTermInfo>();
        resources = new ArrayList<ResourceInfo>();
    }

    public static GetServiceSLASummaryResponse fromJson(String jsonString) {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.fromJson(jsonString, GetServiceSLASummaryResponse.class);
    }

    public String toJson() {
        Gson gson = JsonUtils.getInstance().getGson();
        return gson.toJson(this);
    }

    public String getServiceUri() {
        return serviceUri;
    }

    public void setServiceUri(String serviceUri) {
        this.serviceUri = serviceUri;
    }

    public List<QoSTermInfo> getQosTerms() {
        return qosTerms;
    }

    public void setQosTerms(List<QoSTermInfo> qosTerms) {
        this.qosTerms = qosTerms;
    }

    public void putQosTerm(QoSTermInfo qosTerm) {
        this.qosTerms.add(qosTerm);
    }

    public List<ResourceInfo> getResources() {
        return resources;
    }

    public void setResources(List<ResourceInfo> resources) {
        this.resources = resources;
    }

    public void putResourceInfo(ResourceInfo resourceInfo) {
        this.resources.add(resourceInfo);
    }

    public static class QoSTermInfo {
        private MetricTypeEnum guaranteedTerm;
        private String metricTitle;
        private String metricDescription;
        private String value;
        private String threshold;
        private String unit;
        private QoSTermState compliance;

        public MetricTypeEnum getGuaranteedTerm() {
            return guaranteedTerm;
        }

        public void setGuaranteedTerm(MetricTypeEnum guaranteedTerm) {
            this.guaranteedTerm = guaranteedTerm;
        }

        public String getMetricTitle() {
            return metricTitle;
        }

        public void setMetricTitle(String metricTitle) {
            this.metricTitle = metricTitle;
        }

        public String getMetricDescription() {
            return metricDescription;
        }

        public void setMetricDescription(String metricDescription) {
            this.metricDescription = metricDescription;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getThreshold() {
            return threshold;
        }

        public void setThreshold(String threshold) {
            this.threshold = threshold;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public QoSTermState getCompliance() {
            return compliance;
        }

        public void setCompliance(QoSTermState compliance) {
            this.compliance = compliance;
        }
    }

    public static class ResourceInfo {
        private String resourceName;
        private String resourceFqdn;
        private List<QoSTermInfo> qosTerms;

        public ResourceInfo() {
            qosTerms = new ArrayList<QoSTermInfo>();
        }

        public String getResourceName() {
            return resourceName;
        }

        public void setResourceName(String resourceName) {
            this.resourceName = resourceName;
        }

        public String getResourceFqdn() {
            return resourceFqdn;
        }

        public void setResourceFqdn(String resourceFqdn) {
            this.resourceFqdn = resourceFqdn;
        }

        public List<QoSTermInfo> getQosTerms() {
            return qosTerms;
        }

        public void setQosTerms(List<QoSTermInfo> qosTerms) {
            this.qosTerms = qosTerms;
        }

        public void putQosTerm(QoSTermInfo qosTerm) {
            this.qosTerms.add(qosTerm);
        }
    }

    public enum QoSTermState {
        COMPLIANT,
        PUNISHABLE_WARNING,
        ACCEPTABLE_WARNING,
        PUNISHABLE_VIOLATION,
        ACCEPTABLE_VIOLATION;

        public static QoSTermState fromString(String text) throws Exception {
            try {
                return QoSTermState.valueOf(text.toUpperCase());
            }
            catch (Exception e) {
                throw new Exception("Invalid QoSTermState value: " + text);
            }
        }

        public static QoSTermState getQoSTermState(SLAComplianceEnum slaCompliance, ViolationSeverity violationSeverity) throws Exception {
            if (slaCompliance == null && violationSeverity == null) {
                return null;
            }
            else if (slaCompliance == SLAComplianceEnum.COMPLIANT) {
                return COMPLIANT;
            }
            else {
                return QoSTermState.fromString(violationSeverity.toString() + "_" + slaCompliance.toString());
            }
        }
    }
}
