/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2978 $
 * @lastrevision $Date: 2011-08-12 15:39:20 +0200 (pet, 12 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/jpa/entities/metrics/ClusterDiskSpaceUsage $
 */

package org.slasoi.infrastructure.monitoring.jpa.entities.metrics;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.monitoring.exceptions.InvalidMetricValueException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.MetricValue;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

@Entity
@DiscriminatorValue("CLUSTER_DISK_SPACE_USAGE")
public class ClusterDiskSpaceUsage extends Metric {
    private static final Logger log = Logger.getLogger(ClusterDiskSpaceUsage.class);

    @Override
    public void compute(IMonitoringEngine monitoringEngine) throws Exception {
        log.trace(String.format("Computing metric CLUSTER_DISK_SPACE_USAGE."));
        String diskSpaceUsage;
        try {
            List<String> hostMachines = monitoringEngine.getAllHostMachines();
            if (hostMachines.size() > 0) {
                // all hosts use (the same) shared disk to access VM image files so we take into account
                // only disk space usage data from one host (doesn't matter which)
                String hostFqdn = hostMachines.get(0);
                double diskFree = (Double) monitoringEngine.getHostMetricData(hostFqdn,
                        RawMetric.SHARED_IMAGES_DISK_FREE).getValue();
                double diskUsed = (Double) monitoringEngine.getHostMetricData(hostFqdn,
                        RawMetric.SHARED_IMAGES_DISK_USED).getValue();
                double diskSize = diskFree + diskUsed;
                diskSpaceUsage = String.format("%.3f/%.3f", diskUsed, diskSize);
            }
            else {
                diskSpaceUsage = "0/0";
            }
        }
        catch (Exception e) {
            throw new Exception(String.format("Failed to compute metric CLUSTER_DISK_SPACE_USAGE: %s", e.getMessage()));
        }

        log.trace(String.format("CLUSTER_DISK_SPACE_USAGE = %s", diskSpaceUsage));
        Date metricsTimestamp = monitoringEngine.getMetricsCollectedDate();
        MetricValue metricValue = storeMetricValue(diskSpaceUsage, metricsTimestamp);
        storeMetricHistoryValue(diskSpaceUsage, metricsTimestamp);
    }

    @Override
    public String parseValue(String value) throws InvalidMetricValueException {
        return value;
    }

    @Override
    public String formatValue(Object value) {
        return value.toString();
    }

    @Override
    public Object getViolationThresholdValue() throws InvalidMetricValueException {
        return null;
    }

    @Override
    public String getViolationCondition() throws InvalidMetricValueException {
        return null;
    }

    @Override
    public Object getWarningThresholdValue() {
        return null;
    }

    @Override
    public String getWarningCondition() {
        return null;
    }

    @Override
    public Object estimateWarningThreshold(Object violationThreshold) {
        return null;
    }
}
