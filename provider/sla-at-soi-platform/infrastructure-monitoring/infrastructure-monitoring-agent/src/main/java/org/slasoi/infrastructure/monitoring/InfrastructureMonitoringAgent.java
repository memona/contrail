/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2987 $
 * @lastrevision $Date: 2011-08-18 11:11:29 +0200 (čet, 18 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/InfrastructureMonitoringAgent.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.pubsub.listeners.ConfigurationChannelListener;
import org.slasoi.infrastructure.monitoring.pubsub.listeners.MonitoringDataRequestListener;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

public class InfrastructureMonitoringAgent {
    private static Logger log = Logger.getLogger(InfrastructureMonitoringAgent.class);
    public static final String APPLICATION_NAME = "InfrastructureMonitoringAgent";
    private static InfrastructureMonitoringAgent instance;
    private PubSubManager pubSubManager;
    private IMonitoringEngine monitoringEngine;
    private Channel configurationChannel;
    private Channel eventChannel;
    private Channel monitoringDataRequestChannel;
    private int metricsCollectionInterval;
    private int metricsValidationInterval;
    private List<Thread> workerThreads = new ArrayList<Thread>();
    private MonitoringThread monitoringThread;
    List<MessageListener> messageListeners = new ArrayList<MessageListener>();
    boolean testMode;
    private Object lock = new Object();

    public InfrastructureMonitoringAgent(PubSubManager pubSubManager, IMonitoringEngine monitoringEngine, Properties settings) throws MessagingException, IOException {
        log.trace("InfrastructureMonitoringAgent() constructor started.");
        this.pubSubManager = pubSubManager;
        this.monitoringEngine = monitoringEngine;

        this.configurationChannel = new Channel(settings.getProperty("ConfigurationChannel"));
        this.eventChannel = new Channel(settings.getProperty("EventChannel"));
        this.monitoringDataRequestChannel = new Channel(settings.getProperty("MonitoringDataRequestChannel"));
        this.metricsCollectionInterval = Integer.parseInt(settings.getProperty("MetricsCollectionInterval"));
        this.metricsValidationInterval = Integer.parseInt(settings.getProperty("MetricsValidationInterval"));
        Locale.setDefault(Locale.ENGLISH);

        log.trace("Creating configuration channel.");
        pubSubManager.createChannel(configurationChannel);
        log.trace("Creating event channel.");
        pubSubManager.createChannel(eventChannel);
        log.trace("Creating monitoring data request channel.");
        pubSubManager.createChannel(monitoringDataRequestChannel);

        instance = this;
        log.trace("InfrastructureMonitoringAgent() constructor finished successfully.");
    }

    public static InfrastructureMonitoringAgent getInstance() {
        return instance;
    }

    public void start() throws Exception {
        log.trace("Starting InfrastructureMonitoringAgent...");

        // add message listeners
        ConfigurationChannelListener confChannelListener = new ConfigurationChannelListener();
        MonitoringDataRequestListener monDataReqListener = new MonitoringDataRequestListener();
        messageListeners.add(confChannelListener);
        messageListeners.add(monDataReqListener);
        pubSubManager.addMessageListener(confChannelListener, new String[]{configurationChannel.getName()});
        pubSubManager.addMessageListener(monDataReqListener, new String[]{monitoringDataRequestChannel.getName()});

        log.trace("Subscribing to configuration channel.");
        pubSubManager.subscribe(configurationChannel.getName());
        log.trace("Subscribing to event channel.");
        pubSubManager.subscribe(eventChannel.getName());
        log.trace("Subscribing to monitoring data request channel.");
        pubSubManager.subscribe(monitoringDataRequestChannel.getName());

        // register infrastructure metrics which are monitored from the IMA start and are not
        // related to specific service
        MetricManager.getInstance().registerInfrastructureMetrics();

        // start monitoring thread
        monitoringThread = new MonitoringThread(monitoringEngine, pubSubManager, eventChannel);
        Thread monitoringThreadThread = new Thread(monitoringThread);
        if (!testMode) {
            monitoringThreadThread.start();
            workerThreads.add(monitoringThreadThread);
        }
        else {
            log.trace("Running in test mode. Metrics collecting and validating has to be initiated manually.");
        }

        log.info("InfrastructureMonitoringAgent started successfully.");
    }

    public void start(boolean testMode) throws Exception {
        this.testMode = testMode;
        start();
    }

    public void stop() {
        log.trace("Stopping InfrastructureMonitoringAgent...");

        log.trace("Unregistering pub/sub listeners.");
        for (MessageListener listener : messageListeners) {
            pubSubManager.removeMessageListener(listener);
        }

        try {
            pubSubManager.unsubscribe(configurationChannel.getName());
            pubSubManager.close();
        }
        catch (MessagingException e) {
            log.warn("Failed to unsubscribe from configuration channel: " + e.getMessage());
        }
        try {
            pubSubManager.unsubscribe(eventChannel.getName());
        }
        catch (MessagingException e) {
            log.warn("Failed to unsubscribe from event channel: " + e.getMessage());
        }

        for (Thread t : workerThreads) {
            try {
                t.interrupt();
            }
            catch (Exception e) {
            }
        }
        for (Thread t : workerThreads) {
            try {
                t.join();
            }
            catch (InterruptedException e) {
            }
        }
        log.info("InfrastructureMonitoringAgent stopped successfully.");
    }

    public PubSubManager getPubSubManager() {
        return pubSubManager;
    }

    public void setPubSubManager(PubSubManager pubSubManager) {
        this.pubSubManager = pubSubManager;
    }

    public IMonitoringEngine getMonitoringEngine() {
        return monitoringEngine;
    }

    public void setMonitoringEngine(IMonitoringEngine monitoringEngine) {
        this.monitoringEngine = monitoringEngine;
    }

    public Channel getConfigurationChannel() {
        return configurationChannel;
    }

    public void setConfigurationChannel(Channel configurationChannel) {
        this.configurationChannel = configurationChannel;
    }

    public Channel getEventChannel() {
        return eventChannel;
    }

    public void setEventChannel(Channel eventChannel) {
        this.eventChannel = eventChannel;
    }

    public Channel getMonitoringDataRequestChannel() {
        return monitoringDataRequestChannel;
    }

    public void setMonitoringDataRequestChannel(Channel monitoringDataRequestChannel) {
        this.monitoringDataRequestChannel = monitoringDataRequestChannel;
    }

    public int getMetricsCollectionInterval() {
        return metricsCollectionInterval;
    }

    public void setMetricsCollectionInterval(int metricsCollectionInterval) {
        this.metricsCollectionInterval = metricsCollectionInterval;
    }

    public int getMetricsValidationInterval() {
        return metricsValidationInterval;
    }

    public void setMetricsValidationInterval(int metricsValidationInterval) {
        this.metricsValidationInterval = metricsValidationInterval;
    }

    public Object getLock() {
        return lock;
    }

    protected void initiateMonitoringCycle() throws Exception {
        monitoringThread.monitorManually();
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("infrastructure-monitoring-agent-context.xml");
        InfrastructureMonitoringAgent agent = (InfrastructureMonitoringAgent) context.getBean("agent");
        agent.start();
    }
}
