/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2463 $
 * @lastrevision $Date: 2011-06-30 17:03:37 +0200 (čet, 30 jun 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/main/java/org/slasoi/infrastructure/monitoring/monitors/IMonitoringEngine.java $
 */

package org.slasoi.infrastructure.monitoring.monitors;

import org.slasoi.infrastructure.monitoring.exceptions.MetricNotSupportedException;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.monitors.data.MetricData;
import org.slasoi.infrastructure.monitoring.monitors.data.RawMetric;

import java.util.Date;
import java.util.List;

public interface IMonitoringEngine {

    public enum Status {
        OK,
        ERROR,
        BUSY;
    }

    public void query() throws Exception;

    public Status getStatus();

    public Date getMetricsCollectedDate();

    public MetricData getVmMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException;

    public MetricData getHostMetricData(String fqdn, RawMetric rawMetric) throws MetricNotSupportedException;

    public boolean checkVmExists(String fqdn);

    public boolean checkHostExists(String fqdn);

    public String getVmHostMachine(String fqdn);

    public List<String> getGuestMachines(String hostFqdn);

    public List<String> getAllHostMachines();

    public List<String> getAllGuestMachines();

    public String getInfrastructureLayoutXml();

    public boolean isMonitoringDataAvailable(Service service);
}
