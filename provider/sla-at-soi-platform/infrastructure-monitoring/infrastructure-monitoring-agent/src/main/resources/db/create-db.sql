/*
SQLyog Community v8.32 
MySQL - 5.1.52-community : Database - infrastructure_monitoring_agent
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`infrastructure_monitoring_agent` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_bin */;

USE `infrastructure_monitoring_agent`;

/*Table structure for table `audit_record` */

DROP TABLE IF EXISTS `audit_record`;

CREATE TABLE `audit_record` (
  `audit_record_id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(20) COLLATE latin1_bin DEFAULT NULL,
  `user_id` varchar(20) COLLATE latin1_bin DEFAULT NULL,
  `fqdn` varchar(50) COLLATE latin1_bin DEFAULT NULL,
  `description` varchar(256) COLLATE latin1_bin DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`audit_record_id`),
  UNIQUE KEY `audit_record_id` (`audit_record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

/*Table structure for table `metric` */

DROP TABLE IF EXISTS `metric`;

CREATE TABLE `metric` (
  `metric_id` int(11) NOT NULL AUTO_INCREMENT,
  `metric_type_id` int(11) NOT NULL,
  `metric_type` varchar(50) COLLATE latin1_bin DEFAULT NULL,
  `vm_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `violationThreshold` varchar(32) COLLATE latin1_bin DEFAULT NULL,
  `warningThreshold` varchar(32) COLLATE latin1_bin DEFAULT NULL,
  `config_value` varchar(32) COLLATE latin1_bin DEFAULT 'NULL',
  PRIMARY KEY (`metric_id`),
  UNIQUE KEY `metrictype_service_vm` (`metric_type_id`,`vm_id`),
  KEY `FK_metric_service` (`service_id`),
  KEY `FK_metric_vm` (`vm_id`),
  CONSTRAINT `FK_metric_metrictype` FOREIGN KEY (`metric_type_id`) REFERENCES `metric_type` (`metric_type_id`),
  CONSTRAINT `FK_metric_service` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_metric_vm` FOREIGN KEY (`vm_id`) REFERENCES `vm` (`vm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

/*Table structure for table `metric_type` */

DROP TABLE IF EXISTS `metric_type`;

CREATE TABLE `metric_type` (
  `metric_type_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE latin1_bin NOT NULL,
  `title` varchar(50) COLLATE latin1_bin DEFAULT NULL,
  `description` varchar(200) COLLATE latin1_bin DEFAULT NULL,
  `qos_term_uri` varchar(50) COLLATE latin1_bin DEFAULT NULL,
  `metric_unit` varchar(50) COLLATE latin1_bin DEFAULT NULL,
  `target` enum('VM','SERVICE','INFRASTRUCTURE') COLLATE latin1_bin DEFAULT NULL,
  `type` enum('QOS_TERM','METRIC','CONFIG') COLLATE latin1_bin DEFAULT NULL,
  `is_critical` tinyint(1) DEFAULT NULL,
  `data_type` enum('STRING','INTEGER','DOUBLE','BOOLEAN','ENUM','DATE') COLLATE latin1_bin DEFAULT NULL,
  `computation_order` int(2) NOT NULL DEFAULT '5',
  PRIMARY KEY (`metric_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

/*Table structure for table `metric_value` */

DROP TABLE IF EXISTS `metric_value`;

CREATE TABLE `metric_value` (
  `metric_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `value` varchar(32) COLLATE latin1_bin DEFAULT NULL,
  `sla_compliance` enum('COMPLIANT','WARNING','VIOLATION') COLLATE latin1_bin DEFAULT NULL,
  `violation_severity` enum('PUNISHABLE','ACCEPTABLE') COLLATE latin1_bin DEFAULT NULL,
  PRIMARY KEY (`metric_id`),
  CONSTRAINT `mv_metric_FK` FOREIGN KEY (`metric_id`) REFERENCES `metric` (`metric_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

/*Table structure for table `metric_value_history` */

DROP TABLE IF EXISTS `metric_value_history`;

CREATE TABLE `metric_value_history` (
  `metric_id` int(11) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `value` varchar(32) COLLATE latin1_bin DEFAULT NULL,
  PRIMARY KEY (`metric_id`,`start_time`),
  CONSTRAINT `mvh_metric_FK` FOREIGN KEY (`metric_id`) REFERENCES `metric` (`metric_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

/*Table structure for table `service` */

DROP TABLE IF EXISTS `service`;

CREATE TABLE `service` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(64) COLLATE latin1_bin NOT NULL,
  `service_url` varchar(128) COLLATE latin1_bin DEFAULT NULL,
  `startDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`service_id`),
  UNIQUE KEY `service_unique1` (`service_url`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

/*Table structure for table `violation` */

DROP TABLE IF EXISTS `violation`;

CREATE TABLE `violation` (
  `violation_id` int(11) NOT NULL AUTO_INCREMENT,
  `metric_id` int(11) NOT NULL,
  `metric_value` varchar(32) COLLATE latin1_bin NOT NULL,
  `threshold` varchar(32) COLLATE latin1_bin DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `severity` enum('PUNISHABLE','ACCEPTABLE') COLLATE latin1_bin NOT NULL,
  `type` enum('WARNING','VIOLATION') COLLATE latin1_bin NOT NULL,
  PRIMARY KEY (`violation_id`),
  KEY `violation_FK_metric` (`metric_id`),
  CONSTRAINT `violation_FK_metric` FOREIGN KEY (`metric_id`) REFERENCES `metric` (`metric_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

/*Table structure for table `vm` */

DROP TABLE IF EXISTS `vm`;

CREATE TABLE `vm` (
  `vm_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `resource_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE latin1_bin NOT NULL,
  `fqdn` varchar(100) COLLATE latin1_bin DEFAULT NULL,
  `cluster_name` varchar(50) COLLATE latin1_bin NOT NULL,
  PRIMARY KEY (`vm_id`),
  UNIQUE KEY `vm_unique2` (`fqdn`),
  UNIQUE KEY `service_resource_ui` (`service_id`,`fqdn`),
  CONSTRAINT `FK_service` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
