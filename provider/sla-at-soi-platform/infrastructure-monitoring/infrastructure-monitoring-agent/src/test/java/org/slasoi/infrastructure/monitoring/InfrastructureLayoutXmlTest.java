/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/InfrastructureLayoutXmlTest.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pointtopoint.Messaging;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.monitors.IMonitoringEngine;
import org.slasoi.infrastructure.monitoring.pubsub.InfrastructureInfoRequest;
import org.slasoi.infrastructure.monitoring.pubsub.InfrastructureInfoResponse;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class InfrastructureLayoutXmlTest {
    private static Logger log = Logger.getLogger(InfrastructureLayoutXmlTest.class);
    private List<InfrastructureInfoResponse> responses = new ArrayList<InfrastructureInfoResponse>();

    @Autowired
    private InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("tashiMockupSensorMessaging")
    private Messaging tashiMockupSensorMessaging;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Test
    public void testInfrastructureLayoutXml() throws Exception {
        log.trace("Test InfrastructureLayoutXmlTest started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // initiate one monitoring cycle
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();

        IMonitoringEngine monitoringEngine = InfrastructureMonitoringAgent.getInstance().getMonitoringEngine();

        log.trace("Checking if InfrastructureLayoutXml was created.");
        String layoutXml = monitoringEngine.getInfrastructureLayoutXml();
        assertNotNull(layoutXml);

        log.trace("Testing XMPP service for serving InfrastructureLayoutXml.");
        MessageListener messageListener = registerMessagesListener(pubSubManager2);
        InfrastructureInfoRequest request = new InfrastructureInfoRequest();
        request.setMessageId(UUID.randomUUID().toString());
        String payload = request.toJson();
        PubSubMessage pubSubMessage = new PubSubMessage(agent.getMonitoringDataRequestChannel().getName(), payload);
        pubSubManager2.publish(pubSubMessage);

        log.trace("Waiting for response.");
        long startTime = System.currentTimeMillis();
        while (responses.size() < 1 && System.currentTimeMillis() - startTime < 30000) {
            Thread.sleep(50);
        }
        assertEquals(responses.size(), 1);
        InfrastructureInfoResponse response = responses.get(0);
        assertEquals(response.getInReplyTo(), request.getMessageId());
        assertEquals(response.getResponseType(), "InfrastructureInfoResponse");
        assertNotNull(response.getData());

        TestingUtils.unregisterMessagesListener(pubSubManager2, agent.getMonitoringDataRequestChannel().getName(),
                messageListener);
        agent.stop();
        log.trace("Test testSendSysInfoRequest() finished.");
    }

    private MessageListener registerMessagesListener(PubSubManager pubSubManager) throws MessagingException,
            InterruptedException {
        pubSubManager.subscribe(agent.getMonitoringDataRequestChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived:\n" + payload);
                try {
                    PubSubResponse pubSubResponse = PubSubResponse.fromJson(payload);
                    if (pubSubResponse.getResponseType().equals("InfrastructureInfoResponse")) {
                        InfrastructureInfoResponse response = InfrastructureInfoResponse.fromJson(payload);
                        responses.add(response);
                    }
                }
                catch (Exception e) {
                    assert false : "Invalid InfrastructureInfoResponse response message.";
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }
}
