/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/AcceptableServiceViolationsTest.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.ServiceManager;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.events.EventMessage;
import org.slasoi.infrastructure.monitoring.pubsub.messages.events.EventType;
import org.slasoi.infrastructure.monitoring.qos.events.ViolationEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class AcceptableServiceViolationsTest {
    private static Logger log = Logger.getLogger(AcceptableServiceViolationsTest.class);
    private List<ViolationEvent> violationEvents = new ArrayList<ViolationEvent>();

    @Autowired
    public InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Before
    @After
    public void cleanupDatabase() {
        TestingUtils.cleanupDatabase();
    }

    @Test
    @DirtiesContext
    public void testAcceptableServiceViolations() throws Exception {
        log.trace("Test testAcceptableServiceViolations() started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // send monitoring request to configuration pub/sub channel
        ServiceRegistrationHelper serviceRegistrationHelper = new ServiceRegistrationHelper(agent, pubSubManager2);
        RegisterServiceRequest registerServiceRequest = serviceRegistrationHelper.createRegisterServiceRequestForASVTest();

        serviceRegistrationHelper.sendRegisterServiceRequest(registerServiceRequest);

        //  subscribe to event channel and register violation messages listener
        MessageListener messageListener = registerViolationMessagesListener(pubSubManager2);

        List<Service> services = ServiceManager.getInstance().findServiceEntities();
        Service service = services.get(0);
        List<Vm> vmList = service.getVmList();
        Vm vm1 = vmList.get(0);
        Vm vm2 = vmList.get(1);

        // initiate two monitoring cycles
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();
        Thread.sleep(1000);
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();
        pubSubManager2.unsubscribe(agent.getEventChannel().getName());

        // check registration of ACCEPTABLE_SERVICE_VIOLATIONS metric
        Metric asvMetric = null;
        asvMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.ACCEPTABLE_SERVICE_VIOLATIONS, service);
        assertNotNull(asvMetric);
        assertEquals(asvMetric.getMetricValue().getValue(), "3");
        assertEquals(asvMetric.getViolationThreshold(), "5");
        assertEquals(asvMetric.getWarningThreshold(), "4");

        // VM_SLA_COMPLIANCE
        Metric vmSlaCompliance1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_SLA_COMPLIANCE, vm1);
        Metric vmSlaCompliance2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_SLA_COMPLIANCE, vm2);
        assertEquals(SLAComplianceEnum.fromString(vmSlaCompliance1.getMetricValue().getValue()), SLAComplianceEnum.VIOLATION);
        assertEquals(SLAComplianceEnum.fromString(vmSlaCompliance2.getMetricValue().getValue()), SLAComplianceEnum.COMPLIANT);

        // SERVICE_SLA_COMPLIANCE
        Metric serviceSlaCompliance = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_SLA_COMPLIANCE, service);
        assertEquals(SLAComplianceEnum.fromString(serviceSlaCompliance.getMetricValue().getValue()), SLAComplianceEnum.COMPLIANT);

        unregisterMessagesListener(pubSubManager2, messageListener);
        agent.stop();
        log.trace("Test testAcceptableServiceViolations() finished successfully.");
    }

    private MessageListener registerViolationMessagesListener(PubSubManager pubSubManager) throws MessagingException, InterruptedException {
        pubSubManager.subscribe(agent.getEventChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived: " + payload);
                try {
                    JSONObject o = new JSONObject(payload);
                    EventMessage eventMessage = new EventMessage(o);
                    assertEquals(eventMessage.getOriginator(), InfrastructureMonitoringAgent.APPLICATION_NAME);
                    if (eventMessage.getEventType() == EventType.SLA_VIOLATION) {
                        ViolationEvent violationEvent = new ViolationEvent(eventMessage.getEventData());
                        violationEvents.add(violationEvent);
                    }
                }
                catch (Exception e) {
                    fail("Invalid violation event message.");
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }

    private ViolationEvent findVmViolationEvent(Vm vm, String guaranteedTerm) {
        for (ViolationEvent v : violationEvents) {
            if (v.getGuaranteedTerm().equals(guaranteedTerm) && v.getResourceName().equals(vm.getName())) {
                return v;
            }
        }
        return null;
    }

    private ViolationEvent findServiceViolationEvent(Service service, String guaranteedTerm) {
        for (ViolationEvent v : violationEvents) {
            if (v.getGuaranteedTerm().equals(guaranteedTerm) && v.getServiceUri().equals(service.getServiceUrl())) {
                return v;
            }
        }
        return null;
    }

    private void unregisterMessagesListener(PubSubManager pubSubManager, MessageListener messageListener) throws
            MessagingException {
        log.trace("Unregistering message listener.");
        pubSubManager.removeMessageListener(messageListener);
        pubSubManager.unsubscribe(agent.getEventChannel().getName());
    }
}
