/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/AvailabilityRestrictionsTest.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.infrastructure.monitoring.computation.AvailabilityRestrictions;
import org.slasoi.infrastructure.monitoring.jpa.entities.Metric;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.entities.Vm;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.SLAComplianceEnum;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.jpa.managers.MetricManager;
import org.slasoi.infrastructure.monitoring.jpa.managers.ServiceManager;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.events.EventMessage;
import org.slasoi.infrastructure.monitoring.pubsub.messages.events.EventType;
import org.slasoi.infrastructure.monitoring.qos.events.ViolationEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class AvailabilityRestrictionsTest {
    private static Logger log = Logger.getLogger(AvailabilityRestrictionsTest.class);
    private List<ViolationEvent> violationEvents = new ArrayList<ViolationEvent>();

    @Autowired
    public InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Before
    @After
    public void cleanupDatabase() {
        TestingUtils.cleanupDatabase();
    }

    @Test
    @DirtiesContext
    public void testAcceptableViolations() throws Exception {
        log.trace("Test testAcceptableViolations() started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // send monitoring request to configuration pub/sub channel
        ServiceRegistrationHelper serviceRegistrationHelper = new ServiceRegistrationHelper(agent, pubSubManager2);
        RegisterServiceRequest registerServiceRequest = serviceRegistrationHelper.createRegisterServiceRequest(AvailabilityRestrictions.Period.NEVER, "0");
        serviceRegistrationHelper.sendRegisterServiceRequest(registerServiceRequest);

        //  subscribe to event channel and register violation messages listener
        MessageListener messageListener = registerViolationMessagesListener(pubSubManager2);

        List<Service> services = ServiceManager.getInstance().findServiceEntities();
        Service service = services.get(0);
        List<Vm> vmList = service.getVmList();
        Vm vm1 = vmList.get(0);
        Vm vm2 = vmList.get(1);

        // initiate two monitoring cycles
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();
        Thread.sleep(1000);
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();

        // check registration of SERVICE_AVAILABILITY_RESTRICTIONS metric
        Metric availRestrMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_AVAILABILITY_RESTRICTIONS, service);
        assertNotNull(availRestrMetric);
        AvailabilityRestrictions.Period period = AvailabilityRestrictions.Period.fromString(availRestrMetric.getConfigValue());
        assertEquals(period, AvailabilityRestrictions.Period.NEVER);


        // SERVICE_AVAILABILITY: violation should be acceptable
        Metric serviceAvailTotalMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_AVAILABILITY, service);
        assertEquals(serviceAvailTotalMetric.getMetricValue().getSlaCompliance(), SLAComplianceEnum.VIOLATION);
        assertEquals(serviceAvailTotalMetric.getMetricValue().getViolationSeverity(), ViolationSeverity.ACCEPTABLE);

        // VM_CORES: violation should be acceptable
        Metric vmCoresMetric1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_CORES, vm1);
        Metric vmCoresMetric2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_CORES, vm2);
        assertEquals(vmCoresMetric1.getMetricValue().getSlaCompliance(), SLAComplianceEnum.VIOLATION);
        assertEquals(vmCoresMetric2.getMetricValue().getSlaCompliance(), SLAComplianceEnum.COMPLIANT);
        assertEquals(vmCoresMetric1.getMetricValue().getViolationSeverity(), ViolationSeverity.ACCEPTABLE);
        assertEquals(vmCoresMetric2.getMetricValue().getViolationSeverity(), null);

        // SERVICE_DATA_CLASSIFICATION: violation should be punishable
        Metric serviceDataClassMetric = MetricManager.getInstance().findServiceMetric(MetricTypeEnum.SERVICE_DATA_CLASSIFICATION, service);
        assertEquals(serviceDataClassMetric.getMetricValue().getSlaCompliance(), SLAComplianceEnum.VIOLATION);
        assertEquals(serviceDataClassMetric.getMetricValue().getViolationSeverity(), ViolationSeverity.PUNISHABLE);

        // VM_DATA_ENCRYPTION: violation should be punishable
        Metric vmDataEncryption1 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_DATA_ENCRYPTION, vm1);
        Metric vmDataEncryption2 = MetricManager.getInstance().findVmMetric(MetricTypeEnum.VM_DATA_ENCRYPTION, vm2);
        assertEquals(vmDataEncryption1.getMetricValue().getSlaCompliance(), SLAComplianceEnum.VIOLATION);
        assertEquals(vmDataEncryption2.getMetricValue().getSlaCompliance(), SLAComplianceEnum.COMPLIANT);
        assertEquals(vmDataEncryption1.getMetricValue().getViolationSeverity(), ViolationSeverity.PUNISHABLE);
        assertEquals(vmDataEncryption2.getMetricValue().getViolationSeverity(), null);

        // wait for violation event messages
        long startTime = new Date().getTime();
        while (violationEvents.size() < 9 && new Date().getTime() - startTime < 30000) {
            Thread.sleep(200);
        }
        assertEquals(violationEvents.size(), 8);

        // check if violation events arrived / didn't arrive
        // if violation is punishable violation event should be published to the pubsub, otherwise not
        ViolationEvent violationEvent;
        log.trace("Checking violation event messages...");

        // SERVICE_AVAILABILITY:
        violationEvent = findServiceViolationEvent(service, "SERVICE_AVAILABILITY");
        assertNull(violationEvent);

        // VM_CORES
        violationEvent = findVmViolationEvent(vm1, "VM_CORES");
        assertNull(violationEvent);

        violationEvent = findVmViolationEvent(vm2, "VM_CORES");
        assertNull(violationEvent);

        // SERVICE_DATA_CLASSIFICATION:
        violationEvent = findServiceViolationEvent(service, "SERVICE_DATA_CLASSIFICATION");
        assertNotNull(violationEvent);

        // VM_DATA_ENCRYPTION
        violationEvent = findVmViolationEvent(vm1, "VM_DATA_ENCRYPTION");
        assertNotNull(violationEvent);

        violationEvent = findVmViolationEvent(vm2, "VM_DATA_ENCRYPTION");
        assertNull(violationEvent);

        unregisterMessagesListener(pubSubManager2, messageListener);
        agent.stop();
        log.trace("Test testAcceptableViolations() finished successfully.");
    }

    private MessageListener registerViolationMessagesListener(PubSubManager pubSubManager) throws MessagingException,
            InterruptedException {
        pubSubManager.subscribe(agent.getEventChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived: " + payload);
                try {
                    JSONObject o = new JSONObject(payload);
                    EventMessage eventMessage = new EventMessage(o);
                    assertEquals(eventMessage.getOriginator(), InfrastructureMonitoringAgent.APPLICATION_NAME);
                    if (eventMessage.getEventType() == EventType.SLA_VIOLATION) {
                        ViolationEvent violationEvent = new ViolationEvent(eventMessage.getEventData());
                        violationEvents.add(violationEvent);
                    }
                }
                catch (Exception e) {
                    fail("Invalid violation event message.");
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }

    private ViolationEvent findVmViolationEvent(Vm vm, String guaranteedTerm) {
        for (ViolationEvent v : violationEvents) {
            if (v.getGuaranteedTerm().equals(guaranteedTerm) && v.getResourceName().equals(vm.getName())) {
                return v;
            }
        }
        return null;
    }

    private ViolationEvent findServiceViolationEvent(Service service, String guaranteedTerm) {
        for (ViolationEvent v : violationEvents) {
            if (v.getGuaranteedTerm().equals(guaranteedTerm) && v.getServiceUri().equals(service.getServiceUrl())) {
                return v;
            }
        }
        return null;
    }

    private void unregisterMessagesListener(PubSubManager pubSubManager, MessageListener messageListener) throws
            MessagingException {
        log.trace("Unregistering message listener.");
        pubSubManager.removeMessageListener(messageListener);
        pubSubManager.unsubscribe(agent.getEventChannel().getName());
        pubSubManager.close();
    }
}
