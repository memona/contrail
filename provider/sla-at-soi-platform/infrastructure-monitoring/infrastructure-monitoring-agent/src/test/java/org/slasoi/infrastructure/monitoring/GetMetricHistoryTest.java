/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/GetMetricHistoryTest.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.GetMetricHistoryRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.GetMetricHistoryResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class GetMetricHistoryTest {
    private static Logger log = Logger.getLogger(GetMetricHistoryTest.class);
    private List<GetMetricHistoryResponse> responses = new ArrayList<GetMetricHistoryResponse>();

    @Autowired
    private InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Before
    @After
    public void cleanupDatabase() {
        TestingUtils.cleanupDatabase();
    }

    @Test
    public void testGetMetricHistoryXmppService() throws Exception {
        log.trace("Test testGetMetricHistoryXmppService started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // send monitoring request to configuration pub/sub channel
        ServiceRegistrationHelper serviceRegistrationHelper = new ServiceRegistrationHelper(agent, pubSubManager2);
        RegisterServiceRequest registerServiceRequest = serviceRegistrationHelper.createRegisterServiceRequest();
        serviceRegistrationHelper.sendRegisterServiceRequest(registerServiceRequest);

        // initiate two monitoring cycles
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();
        Thread.sleep(1000);
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();

        MessageListener messageListener = registerMessagesListener(pubSubManager2);

        log.trace("Sending GetMetricHistoryRequest messages to pubsub.");
        GetMetricHistoryRequest request1 = new GetMetricHistoryRequest();
        request1.setMessageId(UUID.randomUUID().toString());
        request1.setMetricType(MetricTypeEnum.SERVICE_AVAILABILITY_STATUS);
        request1.setServiceUri("slasoi://myManagedObject.company.com/Service/TravelService");
        request1.setMaxNumberOfValues(50);
        PubSubMessage pubSubMessage1 = new PubSubMessage(agent.getMonitoringDataRequestChannel().getName(), request1.toJson());
        pubSubManager2.publish(pubSubMessage1);

        GetMetricHistoryRequest request2 = new GetMetricHistoryRequest();
        request2.setMessageId(UUID.randomUUID().toString());
        request2.setMetricType(MetricTypeEnum.VM_MEMORY_SIZE_USED);
        request2.setServiceUri("slasoi://myManagedObject.company.com/Service/TravelService");
        request2.setResourceFqdn("vm1.openlab.com");
        request2.setMaxNumberOfValues(50);
        PubSubMessage pubSubMessage2 = new PubSubMessage(agent.getMonitoringDataRequestChannel().getName(), request2.toJson());
        pubSubManager2.publish(pubSubMessage2);

        log.trace("Waiting for response.");
        long startTime = System.currentTimeMillis();
        while (responses.size() < 2 && System.currentTimeMillis() - startTime < 30000) {
            Thread.sleep(50);
        }
        assertEquals(responses.size(), 2);

        log.trace("Checking GetMetricHistoryResponse 1 ...");
        GetMetricHistoryResponse response1 = findResponse(request1.getMessageId());
        assertEquals(response1.getInReplyTo(), request1.getMessageId());
        assertEquals(response1.getResponseType(), "GetMetricHistoryResponse");
        assertNotNull(response1.getTimestamp());
        assertEquals(response1.getOriginator(), InfrastructureMonitoringAgent.APPLICATION_NAME);
        assertEquals(response1.getServiceUri(), request1.getServiceUri());
        List<GetMetricHistoryResponse.MetricValue> metricValues1 = response1.getMetricValues();
        assertTrue(metricValues1.size() > 1);
        assertEquals(metricValues1.get(0).getValue(), "false");
        assertNotNull(metricValues1.get(0).getTimestamp());
        assertEquals(metricValues1.get(1).getValue(), "false");
        assertNotNull(metricValues1.get(1).getTimestamp());

        log.trace("Checking GetMetricHistoryResponse 2 ...");
        GetMetricHistoryResponse response2 = findResponse(request2.getMessageId());
        assertEquals(response2.getInReplyTo(), request2.getMessageId());
        assertEquals(response2.getResponseType(), "GetMetricHistoryResponse");
        assertNotNull(response2.getTimestamp());
        assertEquals(response2.getOriginator(), InfrastructureMonitoringAgent.APPLICATION_NAME);
        assertEquals(response2.getServiceUri(), request2.getServiceUri());
        assertEquals(response2.getResourceName(), request2.getResourceFqdn());
        List<GetMetricHistoryResponse.MetricValue> metricValues2 = response2.getMetricValues();
        assertTrue(metricValues2.size() > 1);
        assertEquals(metricValues2.get(0).getValue(), "112.391");
        assertNotNull(metricValues2.get(0).getTimestamp());
        assertEquals(metricValues2.get(1).getValue(), "112.391");
        assertNotNull(metricValues2.get(1).getTimestamp());

        unregisterMessagesListener(pubSubManager2, messageListener);
        agent.stop();
        log.trace("Test testGetMetricHistoryXmppService() finished.");
    }

    private MessageListener registerMessagesListener(PubSubManager pubSubManager) throws MessagingException,
            InterruptedException {
        log.trace("Registering message listener.");
        pubSubManager.subscribe(agent.getMonitoringDataRequestChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived:\n" + payload);
                try {
                    PubSubResponse pubSubResponse = PubSubResponse.fromJson(payload);
                    if (pubSubResponse.getResponseType().equals("GetMetricHistoryResponse")) {
                        GetMetricHistoryResponse metricHistoryResponse = GetMetricHistoryResponse.fromJson(payload);
                        responses.add(metricHistoryResponse);
                    }
                }
                catch (Exception e) {
                    fail("Invalid GetMetricHistoryResponse message.");
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }

    private GetMetricHistoryResponse findResponse(String messageId) {
        for (GetMetricHistoryResponse response : responses) {
            if (response.getInReplyTo().equals(messageId)) {
                return response;
            }
        }
        return null;
    }

    private void unregisterMessagesListener(PubSubManager pubSubManager, MessageListener messageListener) throws
            MessagingException {
        log.trace("Unregistering message listener.");
        pubSubManager.removeMessageListener(messageListener);
        pubSubManager.unsubscribe(agent.getMonitoringDataRequestChannel().getName());
    }
}