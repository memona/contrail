/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/ReportingTest.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.commons.math.stat.regression.SimpleRegression;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.jpa.entities.Service;
import org.slasoi.infrastructure.monitoring.jpa.enums.MetricTypeEnum;
import org.slasoi.infrastructure.monitoring.jpa.managers.ServiceManager;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.*;
import org.slasoi.infrastructure.monitoring.reporting.InvestmentGovernanceReport;
import org.slasoi.infrastructure.monitoring.reporting.ServiceSummaryReport;
import org.slasoi.infrastructure.monitoring.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class ReportingTest {
    private static Logger log = Logger.getLogger(IMATest.class);
    List<GenerateReportResponse> responses = new ArrayList<GenerateReportResponse>();

    @Autowired
    public InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Before
    @After
    public void cleanupDatabase() {
        TestingUtils.cleanupDatabase();
    }

    @Test
    public void testReporting() throws Exception {
        log.trace("Test testReporting() started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // send monitoring request to configuration pub/sub channel
        ServiceRegistrationHelper serviceRegistrationHelper = new ServiceRegistrationHelper(agent, pubSubManager2);
        RegisterServiceRequest registerServiceRequest = serviceRegistrationHelper.createRegisterServiceRequest();

        serviceRegistrationHelper.sendRegisterServiceRequest(registerServiceRequest);

        // initiate two monitoring cycles
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();
        Thread.sleep(1000);
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();

        List<Service> services = ServiceManager.getInstance().findServiceEntities();
        Service service = services.get(0);
        log.trace("Generating ServiceSummaryReport.");
        ServiceSummaryReport srvSummaryReport = new ServiceSummaryReport(service);
        assertNotNull(srvSummaryReport);

        // Service Summary Report
        ByteArrayOutputStream baos = srvSummaryReport.generate();
        log.trace("ServiceSummaryReport1 generated successfully.");
        assertTrue(baos.size() > 0);
        FileOutputStream fos = new FileOutputStream("/temp/ServiceSummaryReport-test.pdf");
        baos.writeTo(fos);
        fos.close();
        baos.close();

        // Service Summary Report with fromDate and toDate
        Date now = new Date();
        Date fromDate = new Date(now.getTime() - 2*3600*1000);
        Date toDate = new Date(now.getTime() - 3600*1000);
        srvSummaryReport = new ServiceSummaryReport(service, fromDate, toDate);
        baos = srvSummaryReport.generate();
        log.trace("ServiceSummaryReport2 generated successfully.");
        assertTrue(baos.size() > 0);
        //FileOutputStream fos = new FileOutputStream("/temp/ServiceSummaryReport-test.pdf");
        //baos.writeTo(fos);
        //fos.close();
        baos.close();

        // Investment Governance Report
        InvestmentGovernanceReport report = new InvestmentGovernanceReport();
        baos = report.generate();
        log.trace("InvestmentGovernanceReport generated successfully.");
        assertTrue(baos.size() > 0);
        //FileOutputStream fos = new FileOutputStream("/temp/InvestmentGovernanceReport.pdf");
        //baos.writeTo(fos);
        //fos.close();
        baos.close();


        // test generate report request through monitoring data pub/sub channel
        MessageListener messageListener = registerMessagesListener(pubSubManager2);
        log.trace("Sending ServiceSummaryReport request message to pubsub.");
        now = new Date();
        fromDate = new Date(now.getTime() - 24*3600*1000);
        GenerateReportRequest.ServiceSummary request1 = new GenerateReportRequest.ServiceSummary();
        request1.setMessageId(UUID.randomUUID().toString());
        request1.setServiceUri(service.getServiceUrl());
        request1.setFromDate(fromDate);
        request1.setToDate(now);
        PubSubMessage pubSubMessage1 = new PubSubMessage(agent.getMonitoringDataRequestChannel().getName(),
                request1.toJson());
        pubSubManager2.publish(pubSubMessage1);

        log.trace("Sending InvestmentGovernanceReport request request message to pubsub.");
        GenerateReportRequest.InvestmentGovernance request2 = new GenerateReportRequest.InvestmentGovernance();
        request2.setMessageId(UUID.randomUUID().toString());
        PubSubMessage pubSubMessage2 = new PubSubMessage(agent.getMonitoringDataRequestChannel().getName(),
                request2.toJson());
        pubSubManager2.publish(pubSubMessage2);

        log.trace("Waiting for response.");
        long startTime = System.currentTimeMillis();
        while (responses.size() < 2 && System.currentTimeMillis() - startTime < 30000) {
            Thread.sleep(50);
        }
        assertEquals(responses.size(), 2);

        log.trace("Checking GenerateReportResponse messages ...");
        GenerateReportResponse response1 = findResponse(request1.getMessageId());
        GenerateReportResponse response2 = findResponse(request2.getMessageId());

        assertTrue(response1.getPdfDocument().length > 0);
        assertTrue(response2.getPdfDocument().length > 0);
        assertTrue(response1.getPdfDocument().length != response2.getPdfDocument().length);

        /*
        FileOutputStream fos = new FileOutputStream("/temp/report.pdf");
        fos.write(response1.getPdfDocument());
        fos.close();
        */

        unregisterMessagesListener(pubSubManager2, messageListener);
        agent.stop();
        log.trace("Test testReporting() finished successfully.");
    }

    private MessageListener registerMessagesListener(PubSubManager pubSubManager) throws MessagingException, InterruptedException {
        log.trace("Registering message listener.");
        pubSubManager.subscribe(agent.getMonitoringDataRequestChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived:\n" + Utils.shortenString(payload, 200));
                try {
                    PubSubResponse pubSubResponse = PubSubResponse.fromJson(payload);
                    if (pubSubResponse.getResponseType().equals("GenerateReportResponse")) {
                        GenerateReportResponse reportResponse = GenerateReportResponse.fromJson(payload);
                        responses.add(reportResponse);
                    }
                }
                catch (Exception e) {
                    fail("Invalid GenerateReportResponse message.");
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }

    private GenerateReportResponse findResponse(String messageId) {
        for (GenerateReportResponse response : responses) {
            if (response.getInReplyTo().equals(messageId)) {
                return response;
            }
        }
        return null;
    }

    private void unregisterMessagesListener(PubSubManager pubSubManager, MessageListener messageListener) throws
            MessagingException {
        log.trace("Unregistering message listener.");
        pubSubManager.removeMessageListener(messageListener);
        pubSubManager.unsubscribe(agent.getMonitoringDataRequestChannel().getName());
    }

    @Test
    public void testCommonsMathRegression() throws Exception {
        log.trace("testCommonsMathRegression() started.");
        SimpleRegression regression = new SimpleRegression();
        regression.addData(1, 2);
        regression.addData(2, 3);
        regression.addData(5, 6);
        regression.addData(10, 8);
        assertEquals(regression.predict(1), 2.4286, 0.001);
        assertEquals(regression.predict(15), 11.7143, 0.001);
        log.trace("testCommonsMathRegression() finished successfully.");
    }
}
