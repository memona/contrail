/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/GetServicesInfoTest.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pointtopoint.Messaging;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.GetServicesInfoRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.GetServicesInfoResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class GetServicesInfoTest {
    private static Logger log = Logger.getLogger(GetServicesInfoTest.class);
    private List<GetServicesInfoResponse> responses = new ArrayList<GetServicesInfoResponse>();

    @Autowired
    private InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("tashiMockupSensorMessaging")
    private Messaging tashiMockupSensorMessaging;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Before
    @After
    public void cleanupDatabase() {
        TestingUtils.cleanupDatabase();
    }

    @Test
    public void testGetServicesInfoXmppService() throws Exception {
        log.trace("Test testGetServicesInfoXmppService started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // send monitoring request to configuration pub/sub channel
        ServiceRegistrationHelper serviceRegistrationHelper = new ServiceRegistrationHelper(agent, pubSubManager2);
        RegisterServiceRequest registerServiceRequest = serviceRegistrationHelper.createRegisterServiceRequest();
        serviceRegistrationHelper.sendRegisterServiceRequest(registerServiceRequest);

        MessageListener messageListener = registerMessagesListener(pubSubManager2);

        // initiate two monitoring cycles
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();
        Thread.sleep(1000);
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();

        log.trace("Sending GetServicesInfoRequest message to pubsub.");
        GetServicesInfoRequest request = new GetServicesInfoRequest();
        request.setMessageId(UUID.randomUUID().toString());
        String payload = request.toJson();
        PubSubMessage pubSubMessage = new PubSubMessage(agent.getMonitoringDataRequestChannel().getName(), payload);
        pubSubManager2.publish(pubSubMessage);

        log.trace("Waiting for response.");
        long startTime = System.currentTimeMillis();
        while (responses.size() < 1 && System.currentTimeMillis() - startTime < 30000) {
            Thread.sleep(50);
        }
        assertEquals(responses.size(), 1);

        GetServicesInfoResponse response = responses.get(0);
        assertEquals(response.getInReplyTo(), request.getMessageId());
        assertEquals(response.getResponseType(), "GetServicesInfoResponse");
        assertNotNull(response.getTimestamp());
        assertEquals(response.getOriginator(), InfrastructureMonitoringAgent.APPLICATION_NAME);

        assertEquals(response.getServices().size(), 1);
        GetServicesInfoResponse.ServiceInfo serviceInfo = response.getServices().get(0);
        assertEquals(serviceInfo.getName(), "TravelService");
        assertEquals(serviceInfo.getServiceUri(), "slasoi://myManagedObject.company.com/Service/TravelService");
        assertEquals(serviceInfo.getResources().size(), 2);
        assertEquals(serviceInfo.getResourceCount(), 2);
        assertTrue(serviceInfo.getCreatedDate().getTime() - new Date().getTime() < 300000);
        assertEquals(serviceInfo.getTotalUptime(), "0");
        assertTrue(serviceInfo.getTotalDowntime().equals("0") ||
                serviceInfo.getTotalDowntime().matches("\\d min"));

        GetServicesInfoResponse.ResourceInfo resourceInfo1 = serviceInfo.getResources().get(0);
        GetServicesInfoResponse.ResourceInfo resourceInfo2 = serviceInfo.getResources().get(1);

        assertEquals(resourceInfo1.getName(), "vm1");
        assertEquals(resourceInfo1.getFqdn(), "vm1.openlab.com");
        assertEquals(resourceInfo1.getState().toString(), "RUNNING");
        assertEquals(resourceInfo1.getCpuCores(), "1");
        assertEquals(resourceInfo1.getCpuSpeed(), "1.0 GHz");
        assertEquals(resourceInfo1.getMemory(), "128.0 MB");
        assertEquals(resourceInfo1.isPersistent(), false);
        assertEquals(resourceInfo1.getImageTemplateName(), "tashi.img");

        assertEquals(resourceInfo2.getName(), "vm2");
        assertEquals(resourceInfo2.getFqdn(), "vm2.openlab.com");
        assertEquals(resourceInfo2.getState().toString(), "PAUSED");
        assertEquals(resourceInfo2.getCpuCores(), "2");
        assertEquals(resourceInfo2.getCpuSpeed(), "2.0 GHz");
        assertEquals(resourceInfo2.getMemory(), "256.0 MB");
        assertEquals(resourceInfo2.isPersistent(), false);
        assertEquals(resourceInfo2.getImageTemplateName(), "tashi.img");

        unregisterMessagesListener(pubSubManager2, messageListener);
        agent.stop();

        log.trace("Test testGetServicesInfoXmppService() finished.");
    }

    private MessageListener registerMessagesListener(PubSubManager pubSubManager) throws MessagingException,
            InterruptedException {
        log.trace("Registering message listener.");
        pubSubManager.subscribe(agent.getMonitoringDataRequestChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived:\n" + payload);
                try {
                    PubSubResponse response = PubSubResponse.fromJson(payload);
                    if (response.getResponseType().equals("GetServicesInfoResponse")) {
                        GetServicesInfoResponse servicesInfoResponse = GetServicesInfoResponse.fromJson(payload);
                        responses.add(servicesInfoResponse);
                    }
                }
                catch (Exception e) {
                    fail("Invalid GetServicesInfoResponse message.");
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }

    private void unregisterMessagesListener(PubSubManager pubSubManager, MessageListener messageListener) throws
            MessagingException {
        log.trace("Unregistering message listener.");
        pubSubManager.removeMessageListener(messageListener);
        pubSubManager.unsubscribe(agent.getMonitoringDataRequestChannel().getName());
    }
}