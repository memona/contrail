/**
 * Copyright (c) 2008-2011, XLAB d.o.o.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of XLAB d.o.o. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL XLAB d.o.o. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Damjan Murn - damjan.murn@xlab.si
 * @version $Rev: 2984 $
 * @lastrevision $Date: 2011-08-17 09:01:18 +0200 (sre, 17 avg 2011) $
 * @filesource $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/infrastructure-monitoring/infrastructure-monitoring-agent/src/test/java/org/slasoi/infrastructure/monitoring/GetServiceViolationsTest.java $
 */

package org.slasoi.infrastructure.monitoring;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pointtopoint.Messaging;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationSeverity;
import org.slasoi.infrastructure.monitoring.jpa.enums.ViolationType;
import org.slasoi.infrastructure.monitoring.pubsub.PubSubResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.GetServiceViolationsRequest;
import org.slasoi.infrastructure.monitoring.pubsub.messages.GetServiceViolationsResponse;
import org.slasoi.infrastructure.monitoring.pubsub.messages.RegisterServiceRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context-IMATest.xml"})
public class GetServiceViolationsTest {
    private static Logger log = Logger.getLogger(GetServiceViolationsTest.class);
    private List<GetServiceViolationsResponse> responses = new ArrayList<GetServiceViolationsResponse>();

    @Autowired
    private InfrastructureMonitoringAgent agent;

    @Autowired
    @Qualifier("tashiMockupSensorMessaging")
    private Messaging tashiMockupSensorMessaging;

    @Autowired
    @Qualifier("pubSubManager2")
    public PubSubManager pubSubManager2;

    @Before
    @After
    public void cleanupDatabase() {
        TestingUtils.cleanupDatabase();
    }

    @Test
    public void testGetServiceViolations() throws Exception {
        log.trace("testGetServiceViolations() started.");

        // start Infrastructure Monitoring Agent in test mode
        agent.start(true);

        // send RegisterService request to configuration pub/sub channel
        ServiceRegistrationHelper serviceRegistrationHelper = new ServiceRegistrationHelper(agent, pubSubManager2);
        RegisterServiceRequest registerServiceRequest = serviceRegistrationHelper.createRegisterServiceRequest();
        serviceRegistrationHelper.sendRegisterServiceRequest(registerServiceRequest);

        MessageListener messageListener = registerMessagesListener(pubSubManager2);

        // initiate two monitoring cycles
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();
        Thread.sleep(1000);
        InfrastructureMonitoringAgent.getInstance().initiateMonitoringCycle();

        log.trace("Sending GetServiceViolationsRequest message to pubsub.");
        GetServiceViolationsRequest request = new GetServiceViolationsRequest();
        request.setMessageId(UUID.randomUUID().toString());
        request.setServiceUri("slasoi://myManagedObject.company.com/Service/TravelService");
        String payload = request.toJson();
        PubSubMessage pubSubMessage = new PubSubMessage(agent.getMonitoringDataRequestChannel().getName(), payload);
        pubSubManager2.publish(pubSubMessage);

        log.trace("Waiting for response...");
        long startTime = System.currentTimeMillis();
        while (responses.size() < 1 && System.currentTimeMillis() - startTime < 30000) {
            Thread.sleep(50);
        }
        assertEquals(responses.size(), 1);

        log.trace("Checking GetServiceViolationsRequest...");
        GetServiceViolationsResponse response = responses.get(0);
        assertEquals(response.getInReplyTo(), request.getMessageId());
        assertEquals(response.getResponseType(), "GetServiceViolationsResponse");
        assertNotNull(response.getTimestamp());
        assertEquals(response.getOriginator(), InfrastructureMonitoringAgent.APPLICATION_NAME);
        assertEquals(response.getServiceUri(), request.getServiceUri());

        GetServiceViolationsResponse.ServiceEvents serviceEvents = response.getServiceEvents();
        assertTrue(serviceEvents.getViolations().size() > 0);
        // find SERVICE_AVAILABILITY violation
        GetServiceViolationsResponse.Violation availViolation = null;
        for (GetServiceViolationsResponse.Violation violationData : serviceEvents.getViolations()) {
            if (violationData.getGuaranteedTerm().equals("SERVICE_AVAILABILITY")) {
                availViolation = violationData;
            }
        }
        assertNotNull(availViolation);

        // check SERVICE_AVAILABILITY violation data
        assertEquals(Double.parseDouble(availViolation.getMetricValue()), 0, 0.0001);
        assertEquals(Double.parseDouble(availViolation.getThresholdValue()), 98.0, 0.0001);
        assertEquals(availViolation.getMetricUnit(), "%");
        assertNotNull(availViolation.getBeginDate());
        assertNull(availViolation.getEndDate());
        assertEquals(availViolation.getSeverity(), ViolationSeverity.PUNISHABLE);
        assertEquals(availViolation.getType(), ViolationType.VIOLATION);

        // check resource events data
        assertEquals(response.getResourceEvents().size(), 2);

        // find vm1
        GetServiceViolationsResponse.ResourceEvents vm1ResourceEvents = null;
        for (GetServiceViolationsResponse.ResourceEvents resourceEvents : response.getResourceEvents()) {
            if (resourceEvents.getResourceFqdn().equals("vm1.openlab.com")) {
                vm1ResourceEvents = resourceEvents;
            }
        }
        assertNotNull(vm1ResourceEvents);

        // check vm1 resource data
        assertEquals(vm1ResourceEvents.getResourceName(), "vm1");

        // find VM_MEMORY_SIZE_AVAILABLE violation
        assertTrue(vm1ResourceEvents.getViolations().size() > 0);
        GetServiceViolationsResponse.Violation memAvailViolation = null;
        for (GetServiceViolationsResponse.Violation violationData : vm1ResourceEvents.getViolations()) {
            if (violationData.getGuaranteedTerm().equals("VM_MEMORY_SIZE_AVAILABLE")) {
                memAvailViolation = violationData;
            }
        }
        assertNotNull(memAvailViolation);
        assertEquals(Double.parseDouble(memAvailViolation.getMetricValue()), 197.391, 0.0001);
        assertEquals(Double.parseDouble(memAvailViolation.getThresholdValue()), 256.0, 0.0001);
        assertEquals(memAvailViolation.getMetricUnit(), "MB");
        assertNotNull(memAvailViolation.getBeginDate());
        assertNull(memAvailViolation.getEndDate());
        assertEquals(memAvailViolation.getSeverity(), ViolationSeverity.PUNISHABLE);
        assertEquals(memAvailViolation.getType(), ViolationType.VIOLATION);

        log.trace("GetServiceViolationsResponse is ok.");

        unregisterMessagesListener(pubSubManager2, messageListener);
        agent.stop();
        log.trace("testGetServiceViolations() finished.");
    }

    private MessageListener registerMessagesListener(PubSubManager pubSubManager) throws MessagingException,
            InterruptedException {
        log.trace("Registering message listener.");
        pubSubManager.subscribe(agent.getMonitoringDataRequestChannel().getName());

        MessageListener messageListener = new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                String payload = messageEvent.getMessage().getPayload();
                log.trace("New message arrived:\n" + payload);
                try {
                    PubSubResponse pubSubResponse = PubSubResponse.fromJson(payload);
                    if (pubSubResponse.getResponseType().equals("GetServiceViolationsResponse")) {
                        GetServiceViolationsResponse violationsResponse = GetServiceViolationsResponse.fromJson(payload);
                        responses.add(violationsResponse);
                    }
                }
                catch (Exception e) {
                    fail("Invalid GetServiceViolationsResponse message.");
                }
            }
        };
        pubSubManager.addMessageListener(messageListener);
        return messageListener;
    }

    private void unregisterMessagesListener(PubSubManager pubSubManager, MessageListener messageListener) throws
            MessagingException {
        log.trace("Unregistering message listener.");
        pubSubManager.removeMessageListener(messageListener);
        pubSubManager.unsubscribe(agent.getMonitoringDataRequestChannel().getName());
    }
}
