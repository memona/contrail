package skeletonslam.main;

import java.io.File;

import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.filtering.MavenFileFilter;
import org.apache.maven.shared.filtering.MavenResourcesFiltering;
import org.codehaus.plexus.archiver.manager.ArchiverManager;

/**
 * Contains common jobs for SKELETON mojos.
 * 
 * @version $Id: AbstractWarMojo.java 985625 2011-01-15 08:15:16Z mrojas $
 */
public abstract class AbstractSkeletonMojo extends AbstractMojo
{
    /**
     * The Maven project.
     * 
     * @parameter default-value="${project}"
     * @required
     * @readonly
     */
    private MavenProject project;

    /**
     * The directory containing compiled classes.
     * 
     * @parameter default-value="${project.build.outputDirectory}"
     * @required
     * @readonly
     */
    private File classesDirectory;

    /**
     * Skeleton Maven Name.
     * 
     * @parameter 
     *            default-value="${project.groupId}/${project.artifactId}/${project.version}/"
     * @required
     */
    private String skeletonPluginNamespace;
    
    /**
     * The directory where the maven repository is.
     * 
     * @parameter 
     *            default-value="${settings.localRepository}"
     * @required
     */
    private File mavenRepoDirectory;
    
    /**
     * @parameter default-value="${basedir}/src/main/java"
     * @required
     */
    private File javaSourceDirectory;

    /**
     * The path to the web.xml file to use.
     * 
     * @parameter expression="${maven.war.webxml}"
     */
    private File webXml;

    /**
     * @component role="org.apache.maven.artifact.factory.ArtifactFactory"
     * @required
     * @readonly
     */
    private ArtifactFactory artifactFactory;

    /**
     * To look up Archiver/UnArchiver implementations.
     * 
     * @component role="org.codehaus.plexus.archiver.manager.ArchiverManager"
     * @required
     */
    private ArchiverManager archiverManager;

    /**
     * @component role="org.apache.maven.shared.filtering.MavenFileFilter"
     *            role-hint="default"
     * @required
     */
    protected MavenFileFilter mavenFileFilter;

    /**
     * @component 
     *            role="org.apache.maven.shared.filtering.MavenResourcesFiltering"
     *            role-hint="default"
     * @required
     */
    protected MavenResourcesFiltering mavenResourcesFiltering;

    /**
     * @parameter default-value="${session}"
     * @readonly
     * @required
     * @since 2.1-alpha-2
     */
    protected MavenSession session;

    /**
     * To filter deployment descriptors. <b>Disabled by default.</b>
     * 
     * @parameter expression="${maven.war.filteringDeploymentDescriptors}"
     *            default-value="false"
     * @since 2.1-alpha-2
     */
    protected boolean filteringDeploymentDescriptors = false;

    public MavenProject getProject()
    {
        return project;
    }

    public void setProject( MavenProject project )
    {
        this.project = project;
    }

    public File getClassesDirectory()
    {
        return classesDirectory;
    }

    public void setClassesDirectory( File classesDirectory )
    {
        this.classesDirectory = classesDirectory;
    }

    public File getJavaSourceDirectory()
    {
        return javaSourceDirectory;
    }

    public void setJavaSourceDirectory( File javaSourceDirectory )
    {
        this.javaSourceDirectory = javaSourceDirectory;
    }

    public File getWebXml()
    {
        return webXml;
    }

    public void setWebXml( File webXml )
    {
        this.webXml = webXml;
    }

    public ArtifactFactory getArtifactFactory()
    {
        return this.artifactFactory;
    }

    public void setArtifactFactory( ArtifactFactory artifactFactory )
    {
        this.artifactFactory = artifactFactory;
    }
    
    public File getMavenRepoDirectory()
    {
        return mavenRepoDirectory;
    }

    public void setMavenRepoDirectory( File mavenRepoDirectory )
    {
        this.mavenRepoDirectory = mavenRepoDirectory;
    }

    public String getSkeletonPluginNamespace()
    {
        return skeletonPluginNamespace;
    }

    public void setSkeletonPluginNamespace( String skeletonPluginNamespace )
    {
        this.skeletonPluginNamespace = skeletonPluginNamespace;
    }

    public ArchiverManager getArchiverManager()
    {
        return archiverManager;
    }

    public void setArchiverManager( ArchiverManager archiverManager )
    {
        this.archiverManager = archiverManager;
    }
}
