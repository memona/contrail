package org.slasoi.ss.v11.core;

import org.slasoi.ism.occi.IsmOcciService;

/**
 * This interface is for injecting instance of ISMOCCI into the SLA manager context.
 * 
 */
public interface OCCIAware {
    /**
     * Sets the instance of ISMOCCI into the SLA manager context.
     * 
     * @param iServiceManager
     */
    
    public void setOCCI( IsmOcciService iServiceManager );
}
