package org.slasoi.ss.v11.pac.impl;

import org.apache.log4j.Logger;
import org.slasoi.gslam.core.control.Policy;
import org.slasoi.gslam.pac.ProvisioningAndAdjustment;
import org.slasoi.ism.occi.IsmOcciService;
import org.slasoi.ss.v11.core.OCCIAware;

/**
 * DOMAIN SPECIFIC PAC.
 * 
 * @author Miguel Rojas (UDO)
 * 
 */
public class ProvisioningAdjustmentImpl extends ProvisioningAndAdjustment implements OCCIAware {
    /**
     * logger.
     */
    private static Logger logger = Logger.getLogger(ProvisioningAdjustmentImpl.class.getName());

    /**
     * Constructor.
     * 
     * @param configFile
     *            configuration file to be used by the PAC
     */
    public ProvisioningAdjustmentImpl(final String configFile) {
        super(configFile);
        logger.info("Creating Skeleton PAC...");
    }

    /**
     * Default constructor.
     */
    public ProvisioningAdjustmentImpl() {
        logger.info("Creating Skeleton PAC...");
    }

    /**
     * Triggers the execution of a provisioning plan.
     * 
     * @param plan
     *            the plan to be executed
     * @throws PlanFoundException
     *             if the plan has been already sent for execution
     * @throws PlanFormatException
     *             if the plan is not corrected built
     */
    public void executePlan(Plan plan) throws PlanFoundException, PlanFormatException {
        logger.info("Execute plan " + plan.getPlanId());
    }

    /**
     * Cancels the execution of a plan.
     * 
     * @param planId
     *            the identifier of the plan to be cancelled
     * @throws PlanNotFoundException
     *             if the plan is not being executed
     */
    public void cancelExecution(String planId) throws PlanNotFoundException {
        logger.info("Cancel plan " + planId);
    }

    /**
     * POC informs the PAC that a given action is being executed.
     * 
     * @param planId
     *            identifier of the plan affected by the action
     * @param action
     *            action being executed
     * @param estimatedTime
     *            estimation of time for the action to finish
     * @throws PlanNotFoundException
     *             if the plan is not under PAC's control
     */
    public void ongoingAction(String planId, Task action, long estimatedTime) throws PlanNotFoundException {
        logger.info("Ongoing action " + action.getActionName() + " affecting plan " + planId);
    }

    /**
     * Method to inform about the status of a given plan.
     * 
     * @param planId
     *            the identifier of the plan
     * @return the plan status.
     * @throws PlanNotFoundException
     */
    public Status getPlanStatus(String planId) throws PlanNotFoundException {
        logger.info("get status of plan " + planId);
        return Status.PROVISIONING;
    }

    /**
     * Method to query the LLMS database.
     * 
     * @param ServiceManagerId
     *            ID of the ServiceManager to forward the query
     * @param query
     *            database query
     * @return the result of the query
     */
    // To be changed once the ServiceManagers define the query interface
    public String queryMonitoringDatabase(String ServiceManagerId, String query) {
        logger.info("Executing query " + query + " in Service Manager " + ServiceManagerId);
        return "hello";
    }

    /**
     * Set policies to the PAC. To be used from the business layer.
     * 
     * @param policyClassType
     *            type of policy (Adjustment/Negotiation)
     * @param policies
     *            new policies
     * @return result of the action
     */
    public int setPolicies(String policyClassType, Policy[] policies) {
        logger.info("Setting policies ");
        for (Policy policy : policies) {
            logger.info(policy.toString());
        }

        return 0;
    }

    /**
     * Get the policies used by the PAC. To be used from the business layer.
     * 
     * @param policyClassType
     *            type of policy (Adjustment/Negotiation)
     * @return policies the PAC policies
     */
    public Policy[] getPolicies(String policyClassType) {
        logger.info("Getting policies");
        return null;
    }
    
    public void setOCCI( IsmOcciService ism )
    {
        _ISM_ = ism;
    }
    
    protected IsmOcciService _ISM_;
}
