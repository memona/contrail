package org.slasoi.ss.v11.pac;

import org.slasoi.gslam.core.builder.ProvisioningAdjustmentBuilder;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;

import org.slasoi.ss.v11.pac.impl.ProvisioningAdjustmentImpl;

public class ProvisioningAdjustmentServices implements ProvisioningAdjustmentBuilder
{
    public ProvisioningAdjustment create()
    {
        return new ProvisioningAdjustmentImpl();
    }
    
    public void setConfigurationFile( String file )
    {
        filename = file;
    }
    
    protected String filename;
}
