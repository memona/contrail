package org.slasoi.ss.v11.poc.impl.datastructure;

import org.slasoi.ss.v11.poc.impl.utils.Constant;

/**
 * <code>Harddisk</code> represents Harddisk in resource request.
 */
public class Harddisk extends Resource
{

    public Harddisk()
    {
        this.setResourceName( Constant.Harddisk );
    }
}
