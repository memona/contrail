/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.translator;

/**
 * @author SLA@SOI (City)
 *
 * @date June 22, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 *        JavaDoc: YES
 */
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.monitoring.city.utils.RCGConstants;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.service.Interface.Operation;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.units;
import org.slasoi.slamodel.vocab.xsd;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;

import uk.ac.city.soi.everest.bse.Formula;
import uk.ac.city.soi.everest.bse.FormulaReader;
import uk.ac.city.soi.everest.bse.FormulaWriter;
import uk.ac.city.soi.everest.bse.XMLConverter;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Fluent;
import uk.ac.city.soi.everest.core.Function;
import uk.ac.city.soi.everest.core.Variable;
import uk.ac.city.soi.everest.monitor.Predicate;
import uk.ac.city.soi.everest.util.FormulaConverter;
import eu.slaatsoi.slamodel.impl.InterfaceResourceTypeTypeImpl;

/**
 * This class is used for initiating parametric templates.
 */
public class Instantiator<ASTNode> {
    // initialise logging
    Logger logger = Logger.getLogger(ASTTranslator.class.getName());

    private SLA inputSLA;
    private LinkedHashMap<String, String> parametricTemplates;
    private LinkedHashMap<ASTNode, String> qosTermCases;
    private String assignedExpressionCaseId;
    private FormulaReader formulaReader;
    private FormulaConverter formulaConverter;
    private LinkedHashMap<String, LinkedHashMap<String, Formula>> qosTermsFormulas;
    private LinkedHashMap<String, ArrayList<ASTNode>> nodeOperatorsSignatures;
    private LinkedHashMap<ASTNode, Object[]> qosTermsPaths;

    /**
     * Constructor with values for class fields.
     * 
     * @param inputSLA -
     *            the input SLA
     * @param parametricTemplates -
     *            a set of parametric templates
     * @param qosTermCases -
     *            the map containing the case ids for any visited operator (and qos term) i.e.qosTermCases
     * @param jqosTermsPaths -
     *            the path construct for any visited operator(and qos term)i.e. qosTermsPaths
     * @param nodeOperatorsSignatures -
     *            a map that contains the AST nodes including the same operator per operator signature i.e.
     *            nodeOperatorsSignatures
     * @param jaTHashCode -
     *            the agreement term hash code i.e. aTHashCode
     */
    public Instantiator(SLA inputSLA, LinkedHashMap<String, String> parametricTemplates,
            LinkedHashMap<ASTNode, String> qosTermCases, LinkedHashMap<ASTNode, Object[]> qosTermsPaths,
            LinkedHashMap<String, ArrayList<ASTNode>> nodeOperatorsSignatures, String aTHashCode) {
        this.inputSLA = inputSLA;
        this.parametricTemplates = parametricTemplates;
        this.assignedExpressionCaseId = aTHashCode;
        this.qosTermCases = qosTermCases;
        this.nodeOperatorsSignatures = nodeOperatorsSignatures;
        this.formulaReader = new FormulaReader();
        this.formulaConverter = new FormulaConverter();
        this.qosTermsFormulas = new LinkedHashMap<String, LinkedHashMap<String, Formula>>();
        this.qosTermsPaths = qosTermsPaths;
    }

    /**
     * Retrieves the map containing the formulas per visited operator (and qos term), i.e. qosTermsFormulas.
     * 
     * @return
     */
    public LinkedHashMap<String, LinkedHashMap<String, Formula>> getQosTermsFormulas() {
        return qosTermsFormulas;
    }

    /**
     * Utility method for checking whether the parametric number of operation parameters is contained in the input
     * parametric template.
     * 
     * @param parametricTemplate -
     *            the input parametric template
     * @return
     */
    /*
    private boolean containsParametricNumberOfOperationArguments(String parametricTemplate) {
        boolean contains = false;

        if (parametricTemplate.indexOf(RCGConstants.PARAMETRIC_NUMBER_OF_OPERATION_PARAMETERS) > -1) {
            contains = true;
        }

        return contains;
    }
	*/
    /**
     * Resolves the operation name, partner name and any occurrence of variable with parametric arguments number in the input formulas by substituting the
     * parametric variables with variables generated to represent the real arguments of the corresponding operator
     * (and/or qos term) taken from the input path.
     * 
     * @param formulas -
     *            the input formulas
     * @param path -
     *            the input path
     * @return
     */
    private LinkedHashMap<String, Formula> resolveOperationPartnerVariables(LinkedHashMap<String, Formula> formulas, Object[] path) {

    	ArrayList<LinkedHashMap<String, Object>> ifDecls = (ArrayList<LinkedHashMap<String, Object>>) path[5];
    	
        if(ifDecls != null){        
            for (Formula formula : formulas.values()) {
            	this.allignPredicatePrefixWithStatus(formula);
            	ArrayList<Object> allPreds = new ArrayList<Object>();
            	allPreds.addAll(formula.getBody());
            	allPreds.addAll(formula.getHead());
                for (Object bodyItem : allPreds) {
                    if (bodyItem instanceof Predicate) {
                        Predicate predicate = (Predicate) bodyItem;
                       
                        boolean completion2 = false;
                        boolean completion1 = false;
                        boolean idleTime2 = false;
                        LinkedHashMap<String, Object> interfaceInfo = new LinkedHashMap<String, Object>();
                        
                        interfaceInfo = ifDecls.get(0);
                        
                        if((formula.getFormulaId().contains(common.completion_time.toString()) || formula.getFormulaId().contains("http://www.slaatsoi.org/extension#completion_time"))
                        		    && ifDecls.size() > 1 && predicate.getPrefix().equals(Constants.PREFIX_IR)){
                        	interfaceInfo = ifDecls.get(1);
                        	completion2 = true;
                        	//predicate.setPrefix(Constants.PREFIX_IC);
                        }
                        if((formula.getFormulaId().contains(common.completion_time.toString()) || formula.getFormulaId().contains("http://www.slaatsoi.org/extension#completion_time"))
                    		    && ifDecls.size() == 1){
                    	completion1 = true;
                        }                        
                        else if((formula.getFormulaId().contains("http://www.slaatsoi.org/extension#idle_time") || formula.getFormulaId().contains("idle_time"))
                    		    && ifDecls.size() > 1 && predicate.getPrefix().equals(Constants.PREFIX_IC)){
                        	interfaceInfo = ifDecls.get(1);                        	
                        	//idleTime2 = true; 
                        	//predicate.setPrefix(Constants.PREFIX_IC);
                        }
                        else if((formula.getFormulaId().contains("http://www.slaatsoi.org/extension#idle_time") || formula.getFormulaId().contains("idle_time"))
                    		    && ifDecls.size() > 1 && predicate.getPrefix().equals(Constants.PREFIX_IR)){
                        	//interfaceInfo = ifDecls.get(1);                        	
                        	idleTime2 = true; 
                        	predicate.setPrefix(Constants.PREFIX_IC);
                        }
                        
                        InterfaceDeclr interfaceDecl = (InterfaceDeclr) (interfaceInfo.get(RCGConstants.INTERFACE_DECLARATION));
                        
            			String partnerName = "serviceId";
            			
                        Object interfaceObject = interfaceDecl.getInterface();
                        if (interfaceObject instanceof Specification) {
                            partnerName = ((Specification) interfaceObject).getName();
                        }
                        else if (interfaceObject instanceof InterfaceResourceTypeTypeImpl) {
                            partnerName = ((InterfaceResourceTypeTypeImpl) interfaceObject).getName();
                        }
                        if (partnerName == null)
                            partnerName = interfaceDecl.getId().toString();
                        
                        Operation operation = null;
                                                
                        operation =
                            ((Operation) (interfaceInfo
                                    .get(RCGConstants.INTERFACE_SPECIFICATION_OPERATION)));
                        
                    	if(predicate.getEcName().equals(Constants.PRED_HAPPENS) || predicate.getEcName().equals(Constants.PRED_INITIATES) 
                        		|| predicate.getEcName().equals(Constants.PRED_TERMINATES)){
                                                      		                                

                                predicate.setPartnerId(partnerName);
                                
                                if(operation != null) predicate.setOperationName(operation.getName().toString());
                                
                                if(completion2 || idleTime2){
                                    ArrayList<Variable> originalVars = predicate.getAllVariablesOnly();
                                    for(int i = 0; i < originalVars.size(); i++){
                                    	Variable originalVar = originalVars.get(i);
                                    	if(originalVar.getConstName().equals("receiver1") || originalVar.getConstName().equals("sender1") || 
                                    	   originalVar.getConstName().equals("source1")|| originalVar.getConstName().equals("serviceId1")){
                                    		Variable newVar = new Variable(originalVar);
                                    		newVar.setConstName(originalVar.getConstName().substring(0, originalVar.getConstName().length() - 1) + "2");
                                    		newVar.setName(originalVar.getName().substring(0, originalVar.getName().length() - 1) + "2");
                                    		predicate.getVariables().add(i, newVar);
                                    		predicate.getVariables().remove(originalVar);
                                    		//predicate.addVariable(newVar);
                                    		
                                    	}
                                    	/*
                                    	else if(originalVar.getConstName().contains("status")){
                                    		Variable newVar = new Variable(originalVar);
                                    		newVar.setValue("REQ-B");
                                    		predicate.getVariables().add(i, newVar);
                                    		predicate.getVariables().remove(originalVar);                                    		
                                    	}
                                    	*/
                                    }                                	
                                }   
                                else if(completion1){
                                    ArrayList<Variable> originalVars = predicate.getAllVariablesOnly();
                                    for(int i = 0; i < originalVars.size(); i++){
                                    	Variable originalVar = originalVars.get(i);
                                    	if(originalVar.getConstName().equals("operationId1")){
                                    		predicate.getVariables().remove(originalVar);
                                    		//predicate.addVariable(newVar);
                                    		
                                    	}
                                    }                                	
                                	
                                }
                        }
                    	                	                	
                        for (int i = 0; i < predicate.getVariables().size(); i++) {
                            Object object = predicate.getVariable(i);
                            if (object instanceof Variable) {
                                Variable variable = (Variable) object;
                                if (variable.getConstName()
                                        .contains(RCGConstants.PARAMETRIC_NUMBER_OF_OPERATION_PARAMETERS)
                                        || variable.getName().contains(
                                                RCGConstants.PARAMETRIC_NUMBER_OF_OPERATION_PARAMETERS)) {
                                	                               
                                    if (operation != null) {
                                        if (interfaceInfo
                                                .containsKey(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_OUTPUT) 
                                                || predicate.getPrefix().equals(Constants.PREFIX_IR)) {
                                            // generate and append variables for each output parameter
                                            for (int j = 0; j < operation.getOutputs().length; j++) {

                                                Operation.Property output = operation.getOutputs()[j];

                                                // create a new variable
                                                Variable outputVariable = new Variable(variable);

                                                // set output variable name
                                                outputVariable.setConstName(output.getName().toString());
                                                outputVariable.setName("v" + output.getName().toString());

                                                // set output variable type
                                                STND outputType = ((STND) output.getDatatype());
                                                String outputEVERESTType = null;
                                                if (outputType.equals(xsd.$integer)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_INT;
                                                }
                                                else if (outputType.equals(xsd.$long)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_LONG;
                                                }
                                                else if (outputType.equals(xsd.$double)
                                                        || outputType.equals(xsd.$float)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_DOUBLE;
                                                }
                                                else if (outputType.equals(xsd.$string)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_STRING;
                                                }
                                                else if (outputType.equals(xsd.$boolean)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_BOOL;
                                                }                                                
                                                else if (outputType.equals(xsd.$dateTime)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_STRING;
                                                }                                                
                                                
                                                
                                                outputVariable.setType(outputEVERESTType);

                                                // add new variable
                                                predicate.getVariables().add(i + j, outputVariable);
                                            }
                                            
                                            for (int j = 0; j < operation.getRelated().length; j++) {

                                                Operation.Property output = operation.getRelated()[j];

                                                // create a new variable
                                                Variable outputVariable = new Variable(variable);

                                                // set output variable name
                                                outputVariable.setConstName(output.getName().toString());
                                                outputVariable.setName("v" + output.getName().toString());

                                                // set output variable type
                                                STND outputType = ((STND) output.getDatatype());
                                                String outputEVERESTType = null;
                                                if (outputType.equals(xsd.$integer)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_INT;
                                                }
                                                else if (outputType.equals(xsd.$long)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_LONG;
                                                }
                                                else if (outputType.equals(xsd.$double)
                                                        || outputType.equals(xsd.$float)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_DOUBLE;
                                                }
                                                else if (outputType.equals(xsd.$string)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_STRING;
                                                }
                                                else if (outputType.equals(xsd.$boolean)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_BOOL;
                                                }                                                
                                                else if (outputType.equals(xsd.$dateTime)) {
                                                    outputEVERESTType = Constants.DATA_TYPE_STRING;
                                                }                                                
                                                
                                                
                                                outputVariable.setType(outputEVERESTType);

                                                // add new variable
                                                predicate.getVariables().add(i + j, outputVariable);
                                            }                                            
                                            // remove PARAMETRIC_NUMBER_OF_OPERATION_PARAMETERS variable
                                            predicate.getVariables().remove(variable);
                                        }
                                        else if (interfaceInfo
                                                .containsKey(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_INPUT)
                                                || predicate.getPrefix().equals(Constants.PREFIX_IC)) {
                                            // generate and append variables for each input parameter
                                            for (Operation.Property input : operation.getInputs()) {
                                                // create a new variable
                                                Variable inputVariable = new Variable(variable);

                                                // set input variable name
                                                inputVariable.setConstName(input.getName().toString());
                                                inputVariable.setName("v" + input.getName().toString());

                                                // set input variable type
                                                STND inputType = ((STND) input.getDatatype());
                                                String inputEVERESTType = null;
                                                if (inputType.equals(xsd.$integer)) {
                                                    inputEVERESTType = Constants.DATA_TYPE_INT;
                                                }
                                                else if (inputType.equals(xsd.$long)) {
                                                    inputEVERESTType = Constants.DATA_TYPE_LONG;
                                                }
                                                else if (inputType.equals(xsd.$double) || inputType.equals(xsd.$float)) {
                                                    inputEVERESTType = Constants.DATA_TYPE_DOUBLE;
                                                }
                                                else if (inputType.equals(xsd.$string)) {
                                                    inputEVERESTType = Constants.DATA_TYPE_STRING;
                                                }
                                                
                                                else if (inputType.equals(xsd.$boolean)) {
                                                    inputEVERESTType = Constants.DATA_TYPE_BOOL;
                                                }                                                
                                                else if (inputType.equals(xsd.$dateTime)) {
                                                    inputEVERESTType = Constants.DATA_TYPE_STRING;
                                                }                                                                                                
                                                inputVariable.setType(inputEVERESTType);

                                                // add new variable
                                                predicate.getVariables().add(i, inputVariable);
                                            }
                                            // remove PARAMETRIC_NUMBER_OF_OPERATION_PARAMETERS variable
                                            predicate.getVariables().remove(variable);
                                        }
                                    }
                                }
                            }
                        }
                        
                        Fluent fluent = predicate.getFluent();
                        
                        if(fluent != null){                        	
                        	ArrayList<Object> arguments = fluent.getArguments();
                        	for(Object obj : arguments){
                        		if(obj instanceof Variable){
                        			Variable variable = (Variable)obj;                        			
                                    if(variable.getObjValue().contains(RCGConstants.PARAMETRIC_SERVICE_ID)){                                    	
                                    	variable.setValue(partnerName);                                    	
                                    }                        			
                        		}
                        		else if(obj instanceof Function){
                        			// if needed code here to replace [serviceId] in parameter of function 
                        		}
                        	}
                        }
                    }
                }
            }
        }        
        return formulas;
    }

    /**
     * Generates instantiated formulas from parametric templates.
     */
    public void generateInstantiatedFormulasFromParametricTemplates() {

        if (parametricTemplates.size() > 0) {
            // process the string parametricTemplates
            Set set = parametricTemplates.entrySet();
            Iterator it = set.iterator();
            while (it.hasNext()) {
                Map.Entry me = (Map.Entry) it.next();
                String operatorNodeSignature = (String) me.getKey();
                String parametricTemplate = (String) me.getValue();

                // retrieve the operator's node from nodeOperatorsSignatures
                ASTNode operatorNode = null;
                for (String key : this.nodeOperatorsSignatures.keySet()) {
                    if (key != null) {
                        if (key.equals(operatorNodeSignature)) {
                            operatorNode =
                                    this.nodeOperatorsSignatures.get(key).get(
                                            (this.nodeOperatorsSignatures.get(key).size()) - 1);
                            break;
                        }
                    }
                }

                // retrieve the operator's node path from qosTermsPaths
                Object[] nodeOperatorPath = this.qosTermsPaths.get(operatorNode);
                //System.err.println("===========> " + operatorNodeSignature + ":" + operatorNode.hashCode() + "   : " + nodeOperatorPath[5]);

                // check whether any ParametricNumberOfOperationArguments are specified
                /*
                boolean containsParametricNumberOfOperationArguments = false;
                if (this.containsParametricNumberOfOperationArguments(parametricTemplate)) {
                    // this.resolveParametricNumberOfOperationArguments(parametricTemplate, nodeOperatorPath);
                    containsParametricNumberOfOperationArguments = true;
                }
				*/
                // parse the XML string
                Document document = this.formulaConverter.readFormulas((String) parametricTemplate, Constants.PKG);
                // traverse the parsed document
                try {
                    DocumentTraversal traversal = (DocumentTraversal) document;

                    NodeIterator iterator =
                            traversal.createNodeIterator(document.getDocumentElement(), NodeFilter.SHOW_ELEMENT, null,
                                    true);

                    // for each element, check whether its value and/or its attributes are parametric
                    for (Node n = iterator.nextNode(); n != null; n = iterator.nextNode()) {
                        Element nElement = ((Element) n);
                        //logger.debug("Element: " + nElement.getTagName());
                        // case that element is parametric
                        if (nElement.getFirstChild() != null) {
                            if (!nElement.getFirstChild().getNodeValue().equals("")) {
                                if (nElement.getFirstChild().getNodeValue().contains(RCGConstants.PARAMETRIC_CHAR)) {
                                    logger.debug("found paramteric element value : " + nElement.getFirstChild().getNodeValue());
                                    // substitute parametric string
                                    substituteParametricString(operatorNodeSignature, nodeOperatorPath, nElement
                                            .getFirstChild(), document, n, iterator);
                                    logger.debug("Substituted paramteric element value : " + nElement.getFirstChild().getNodeValue());
                                }
                            }
                        }
                        // check whether element's attributes are parametric
                        if (nElement.getAttributes().getLength() > 0) {
                            for (int i = 0; i < nElement.getAttributes().getLength(); i++) {
                                Node attribute = nElement.getAttributes().item(i);
                                // case that element's attribute is parametric
                                if (!attribute.getNodeValue().equals("")) {
                                    if (attribute.getNodeValue().contains(RCGConstants.PARAMETRIC_CHAR)) {
                                        logger.debug("found paramteric attribute value : " + attribute.getNodeValue());
                                        // substitute parametric string
                                        substituteParametricString(operatorNodeSignature, nodeOperatorPath, attribute,
                                                document, n, iterator);
                                    }
                                }
                            }
                        }
                    }

                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }

                // transform the document object into a String object
                //String instantiatedXMLQoSTermTheory = XMLConverter.toString(document.getDocumentElement());
                //logger.debug("Instantiated XML: " + instantiatedXMLQoSTermTheory);

                // generate instantiated EVEREST formula objects from document
                LinkedHashMap<String, Formula> qosTermFormulas = formulaReader.getFormula(document);

                // resolve any ParametricNumberOfOperationArguments

               this.resolveOperationPartnerVariables(qosTermFormulas, nodeOperatorPath);


                FormulaWriter writer = new FormulaWriter();                
                logger.debug("Instantiated XML: \n\n" + writer.generateXML(qosTermFormulas));                
                
                // store qosTerm Formulas in formulas
                this.qosTermsFormulas.put(operatorNodeSignature, qosTermFormulas);
            }
        }
        else {
            logger.debug("NOTIFICATION: No parametric templates found due to unsupported functional expression operators");
        }
    }

    /**
     * Makes substitutions of parametric strings within the XML representation of a parametric template.
     * 
     * @param operatorNodeSignature -
     *            the operator Node Signature
     * @param path -
     *            the input path
     * @param documentNode -
     *            the input document Node
     * @param document -
     *            the input document
     * @param currentNode -
     *            the input current Node
     * @param iterator -
     *            the current iterator
     */
    public void substituteParametricString(String operatorNodeSignature, Object[] path, Node documentNode,
            Document document, Node currentNode, NodeIterator iterator) {

        // retrieve the operator's node from nodeOperatorsSignatures
        ASTNode operatorNode = null;
        String nodeKey = "";
        for (String key : this.nodeOperatorsSignatures.keySet()) {
            if (key != null) {
                if (key.equals(operatorNodeSignature)) {
                    operatorNode =
                            this.nodeOperatorsSignatures.get(key).get(
                                    (this.nodeOperatorsSignatures.get(key).size()) - 1);
                    nodeKey = key;
                    break;
                }
            }
        }

        LinkedHashMap<String, Object> interfaceInfo = null;
        // retrieve the operator's interface information
        ArrayList<LinkedHashMap<String, Object>> ifDecls = (ArrayList<LinkedHashMap<String, Object>>) path[5];
        if(ifDecls != null){
            for(LinkedHashMap<String, Object> ifo : ifDecls){
            	if(ifo.containsKey(nodeKey)) interfaceInfo = ifo;
            }
        }
        
        

        if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_QOS_TERM_CASE_ID)) {

            // retrieve the qos term case id from qosTermsCases
            String qosTermCaseId = this.qosTermCases.get(operatorNode);
            // substitute the parametric string
            String subString =
                    documentNode.getNodeValue().replace(RCGConstants.PARAMETRIC_QOS_TERM_CASE_ID, qosTermCaseId);
            documentNode.setNodeValue(subString);
        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_ASSIGNED_SLA_EXPRESSION_CASE_ID)) {
            // substitute the parametric string
            String subString =
                    documentNode.getNodeValue().replace(RCGConstants.PARAMETRIC_ASSIGNED_SLA_EXPRESSION_CASE_ID,
                            this.assignedExpressionCaseId);
            documentNode.setNodeValue(subString);
        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_SERVICE_ID)) {
            /*
             * ATTENTION: The following code is based on the assumption that all interface declarations that are
             * included in the input SLA refer to the SAME interface!!! // retrieve the service interface reference from
             * the first interface declaration of input SLA InterfaceRef iFaceRef = null; for(InterfaceDeclr iFaceDecl :
             * this.inputSLA.getInterfaceDeclrs()){ //this.inputSLA.get Endpoint[] endpoints = iFaceDecl.getEndpoints();
             * Endpoint endpoint = endpoints[0]; break; } //String serviceId = iFaceRef.get
             * 
             */

            /*
             * MOCK-UP code: Assumption: For sake of testing
             */
            //String subString = documentNode.getNodeValue().replace(RCGConstants.PARAMETRIC_SERVICE_ID, "ServiceName");
            //documentNode.setNodeValue(subString);
        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_SENSOR_ID)) {
            /*
             * MOCK-UP code
             */
            String subString = documentNode.getNodeValue().replace(RCGConstants.PARAMETRIC_SENSOR_ID, "ServiceName");
            documentNode.setNodeValue(subString);
        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_ASSIGNED_SLA_EXPRESSION_ID)) {
            /*
             * MOCK-UP code
             */
            String subString =
                    documentNode.getNodeValue().replace(RCGConstants.PARAMETRIC_ASSIGNED_SLA_EXPRESSION_ID, "86400000");
            documentNode.setNodeValue(subString);
        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_PARTNER_NAME)) {
        	/*
            String partnerIP = null;

            if (interfaceInfo != null) {

                // retrieve interface declaration from interface info
                InterfaceDeclr interfaceDecl = (InterfaceDeclr) (interfaceInfo.get(RCGConstants.INTERFACE_DECLARATION));

                // retrieve notifier/sensor IP from software service manager
                // String partnerIP = getPartnerIp(interfaceDecl);

                
                 // MOCK-UP code for testing: For sake of testing partner name is set equal to interface name
                 
                Object interfaceObject = interfaceDecl.getInterface();
                if (interfaceObject instanceof Specification) {
                    partnerIP = ((Specification) interfaceObject).getName();
                }
                else if (interfaceObject instanceof InterfaceResourceTypeTypeImpl) {
                    partnerIP = ((InterfaceResourceTypeTypeImpl) interfaceObject).getName();
                }

            }
            logger.debug("partnerIP :" + partnerIP);

            if (partnerIP == null) {
                partnerIP = "ServiceIp";
            }

            String subString = documentNode.getNodeValue().replace(RCGConstants.PARAMETRIC_PARTNER_NAME, partnerIP);
            documentNode.setNodeValue(subString);
            */
        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_INTERFACE_ID)) {

            // retrieve interface declaration from interface info
            InterfaceDeclr interfaceDecl = (InterfaceDeclr) (interfaceInfo.get(RCGConstants.INTERFACE_DECLARATION));

            Object interfaceObject = interfaceDecl.getInterface();
            if (interfaceObject instanceof Specification) {
                String interfaceId = ((Specification) interfaceObject).getName();
                documentNode.setNodeValue(interfaceId);
            }
        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_INTERFACE_OPERATION)) {

            // retrieve interface operation from interface info
            Operation operation = (Operation) (interfaceInfo.get(RCGConstants.INTERFACE_SPECIFICATION_OPERATION));

            documentNode.setNodeValue(operation.getName().toString());
        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_INTERFACE_OPERATION_PARAMETER)) {

            // check whether the operation parameter refers to input or output and retrieve it
            Operation.Property parameter = null;
            if (interfaceInfo.containsValue(RCGConstants.STATUS_REQUEST_AFTER)
                    || interfaceInfo.containsValue(RCGConstants.STATUS_REQUEST_BEFORE)) {
                parameter =
                        (Operation.Property) (interfaceInfo.get(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_INPUT));
            }
            else {
                parameter =
                        (Operation.Property) (interfaceInfo.get(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_OUTPUT));
            }

            String subString =
                documentNode.getNodeValue().replace(
                        RCGConstants.PARAMETRIC_INTERFACE_OPERATION_PARAMETER, parameter.getName().toString());
            
            documentNode.setNodeValue(subString);
        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_INTERFACE_OPERATION_PARAMETER_DATA_TYPE)) {

            // check whether the operation parameter refers to input or output and retrieve it
            Operation.Property parameter = null;
            if (interfaceInfo.containsValue(RCGConstants.STATUS_REQUEST_AFTER)
                    || interfaceInfo.containsValue(RCGConstants.STATUS_REQUEST_BEFORE)) {
                parameter =
                        (Operation.Property) (interfaceInfo.get(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_INPUT));
            }
            else {
                parameter =
                        (Operation.Property) (interfaceInfo.get(RCGConstants.INTERFACE_SPECIFICATION_OPERATION_OUTPUT));
            }

            STND parameterType = ((STND) parameter.getDatatype());
            String parameterEVERESTType = null;
            if (parameterType.equals(xsd.$integer)) {
                parameterEVERESTType = Constants.DATA_TYPE_INT;
            }
            else if (parameterType.equals(xsd.$long)) {
                parameterEVERESTType = Constants.DATA_TYPE_LONG;
            }
            else if (parameterType.equals(xsd.$double) || parameterType.equals(xsd.$float)) {
                parameterEVERESTType = Constants.DATA_TYPE_DOUBLE;
            }
            else if (parameterType.equals(xsd.$string)) {
                parameterEVERESTType = Constants.DATA_TYPE_STRING;
            }
            else if(parameterType.equals(xsd.$boolean)){
            	parameterEVERESTType = Constants.DATA_TYPE_BOOL;
            }
            else if(parameterType.equals(xsd.$dateTime)){
            	parameterEVERESTType = Constants.DATA_TYPE_STRING;
            }            
            
            String subString =
                    documentNode.getNodeValue().replace(
                            RCGConstants.PARAMETRIC_INTERFACE_OPERATION_PARAMETER_DATA_TYPE, parameterEVERESTType);
            documentNode.setNodeValue(subString);
        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_STATUS)) {

            // check whether the status refers to request or response and retrieve it
            String subString = null;
            if (interfaceInfo.containsValue(RCGConstants.STATUS_REQUEST_AFTER)) {
                subString = RCGConstants.STATUS_REQUEST_AFTER;
            }
            else if (interfaceInfo.containsValue(RCGConstants.STATUS_REQUEST_BEFORE)) {
                subString = RCGConstants.STATUS_REQUEST_BEFORE;
            }
            else if (interfaceInfo.containsValue(RCGConstants.STATUS_RESPONSE_AFTER)) {
            	subString = RCGConstants.STATUS_RESPONSE_AFTER;                	
            }
            else if (interfaceInfo.containsValue(RCGConstants.STATUS_RESPONSE_BEFORE)) {
                subString = RCGConstants.STATUS_RESPONSE_BEFORE;
            }

            documentNode.setNodeValue(subString);

        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_PERIODIC_EVENT_PERIOD)) {
        	/*
			//Sereies has been implemented as internal function so no need for this
            Long period = 0l;
            if (path[9] != null) {
                if (path[9] instanceof STND) {
                    STND funcExprOperator = (STND) path[9];
                    // series case
                    if (funcExprOperator.equals(core.series)) {
                        // check series parameters for retrieving periodic event specs
                        if (path[8] != null) {
                            if (path[8] instanceof ValueExpr[]) {
                                ValueExpr[] seriesParameters = (ValueExpr[]) path[8];
                                for (ValueExpr seriesParameter : seriesParameters) {
                                    if (seriesParameter instanceof EventExpr) {
                                        EventExpr seriesPeriodicEvent = (EventExpr) seriesParameter;
                                        for (Expr seriesPeriodicEventParameter : seriesPeriodicEvent.getParameters()) {
                                            if (seriesPeriodicEventParameter instanceof ValueExpr) {
                                                if (((ValueExpr) seriesPeriodicEventParameter) instanceof CONST) {
                                                    CONST periodConstant =
                                                            (CONST) ((ValueExpr) seriesPeriodicEventParameter);
                                                    String constantValue = periodConstant.getValue();
                                                    STND constantSLADataType = periodConstant.getDatatype();

                                                    if (constantSLADataType != null) {
                                                        Double doubleConstantValue = Double.parseDouble(constantValue);
                                                        // case that SLA constant type is measured in ms units
                                                        if (constantSLADataType.equals(units.ms)) {
                                                            period = Long.parseLong(constantValue);
                                                        }
                                                        // case that SLA constant type is measured in second units
                                                        else if (constantSLADataType.equals(units.s)) {
                                                            // convert seconds into msecs
                                                            period =
                                                                    ((Double) (doubleConstantValue * 1000)).longValue();
                                                        }
                                                        // case that SLA constant type is measured in minute units
                                                        if (constantSLADataType.equals(units.min)) {
                                                            // convert minutes into msecs
                                                            period =
                                                                    ((Double) (doubleConstantValue * 60 * 1000))
                                                                            .longValue();
                                                        }
                                                        // case that SLA constant type is measured in hour units
                                                        else if (constantSLADataType.equals(units.hrs)) {
                                                            // convert hours into msecs
                                                            period =
                                                                    ((Double) (doubleConstantValue * 60 * 60 * 1000))
                                                                            .longValue();
                                                        }
                                                        // case that SLA constant type is measured in day units
                                                        else if (constantSLADataType.equals(units.day)) {
                                                            // convert days into msecs
                                                            period =
                                                                    ((Double) (doubleConstantValue * 24 * 60 * 60 * 1000))
                                                                            .longValue();
                                                        }
                                                        // case that SLA constant type is measured in month units
                                                        else if (constantSLADataType.equals(units.month)) {
                                                            // convert months into msecs
                                                            period =
                                                                    ((Double) (doubleConstantValue * 30 * 24 * 60 * 60 * 1000))
                                                                            .longValue();
                                                        }
                                                        // case that SLA constant type is measured in year units
                                                        else if (constantSLADataType.equals(units.year)) {
                                                            // convert years into msecs
                                                            period =
                                                                    ((Double) (doubleConstantValue * 12 * 30 * 24 * 60
                                                                            * 60 * 1000)).longValue();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            String subString =
                    documentNode.getNodeValue().replace(RCGConstants.PARAMETRIC_PERIODIC_EVENT_PERIOD,
                            period.toString());
            documentNode.setNodeValue(subString);
            */
        }
        else if (documentNode.getNodeValue().contains(RCGConstants.PARAMETRIC_PERIODIC_EVENT_PERIOD_START_TIME)) {
            // retrieve periodStartTime
            // periodStartTime : SLAEffectiveFromTime in ms
            Long periodStartTime = ((TIME) this.inputSLA.getEffectiveFrom()).getValue().getTimeInMillis();

            String subString =
                    documentNode.getNodeValue().replace(RCGConstants.PARAMETRIC_PERIODIC_EVENT_PERIOD_START_TIME,
                            periodStartTime.toString());
            documentNode.setNodeValue(subString);
        }        	
    }     
                      

    /*
     * Retrieves the partner (i.e. sensor) IP from the input interface declaration
     * 
     * @param org.slasoi.slamodel.sla.InterfaceDeclr - the input interfaceDecl @return java.lang.String - the partner
     * (i.e. sensor) IP
     * 
     * private String getPartnerIp(InterfaceDeclr interfaceDecl) { String partnerIp = null; // create a software service
     * manager reference // ISoftwareServiceManager softwareServiceManager = new ISoftwareServiceManager(); // query
     * software service manager to get notifier/sensor information regarding the interface // described in given
     * interface declaration
     * 
     * return partnerIp; }
     */

    /**
     * Changes the predicates prefixes included in a formula according to the value of the corresponding Status
     * variables.
     * 
     * @param formula
     */
    private void allignPredicatePrefixWithStatus(Formula formula) {
        ArrayList<Predicate> formulaPredicates = new ArrayList<Predicate>();
        formulaPredicates.addAll(formula.getBody());
        formulaPredicates.addAll(formula.getHead());

        for (Predicate predicate : formulaPredicates) {
            if (!predicate.getEcName().equals(Constants.PRED_HOLDSAT)
                    && !predicate.getEcName().equals(Constants.PRED_INITIALLY)
                    && !predicate.getEcName().equals(Constants.PRED_RELATION)) {
                if (predicate.getPrefix() != null && predicate.getPrefix() != "") {
                    for (Variable variable : predicate.getAllVariablesOnly()) {
                        if (variable.getConstName().contains("status")) {
                            if ((variable.getValue().equals(Constants.STATUS_REQUEST_AFTER) || variable.getValue()
                                    .equals(Constants.STATUS_REQUEST_BEFORE))
                                    && !predicate.getPrefix().equals(Constants.PREFIX_IC)) {
                                predicate.setPrefix(Constants.PREFIX_IC);
                                logger.debug("set prefix to: " + Constants.PREFIX_IC);
                            }
                            else if ((variable.getValue().equals(Constants.STATUS_RESPONSE_AFTER) || variable
                                    .getValue().equals(Constants.STATUS_RESPONSE_BEFORE))
                                    && !predicate.getPrefix().equals(Constants.PREFIX_IR)) {
                                predicate.setPrefix(Constants.PREFIX_IR);
                                logger.debug("set prefix to: " + Constants.PREFIX_IR);
                            }
                        }
                    }
                }
            }
        }
    }    
}
