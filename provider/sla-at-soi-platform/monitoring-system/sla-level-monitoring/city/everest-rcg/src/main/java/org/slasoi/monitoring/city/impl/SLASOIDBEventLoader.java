package org.slasoi.monitoring.city.impl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimerTask;

import org.slasoi.monitoring.city.database.slasoiInteractionEvent.SlasoiInteractionEventEntityManager;

import uk.ac.city.soi.everest.NewDataAnalyzerEC;
import uk.ac.city.soi.everest.SRNTEvent.TypeOfEvent;
import uk.ac.city.soi.everest.monitor.Template;

public class SLASOIDBEventLoader  extends TimerTask{
	
	private NewDataAnalyzerEC ndaec;
	private SlasoiInteractionEventEntityManager slasoiInteractionEventStorer;
	
	public SLASOIDBEventLoader(SlasoiInteractionEventEntityManager slasoiInteractionEventStorer, NewDataAnalyzerEC ndaec){
		this.ndaec = ndaec;
		this.slasoiInteractionEventStorer = slasoiInteractionEventStorer;
	}
	
    public void run() {
    	if(!ndaec.isBusy()){
    		HashMap<String, TypeOfEvent> eventsWithKey =  slasoiInteractionEventStorer.getAllEverestEventsWithPrimaryKey();
    		ArrayList<TypeOfEvent> events = sortEventsByTime(eventsWithKey);
    		for(TypeOfEvent event : events) ndaec.receiveTypeOfEventObject(event);
    		Iterator<String> keys = eventsWithKey.keySet().iterator();
    		while(keys.hasNext()){
    			String key = keys.next();
    			slasoiInteractionEventStorer.deleteByPrimaryKey(key);
    		}
    	}
        //System.out.format("Time's up!%n");
        //timer.cancel(); //Terminate the timer thread
    }
    
    private ArrayList<TypeOfEvent> sortEventsByTime(HashMap<String, TypeOfEvent> eventsWithKey){
    	ArrayList<TypeOfEvent> returnEvents = new ArrayList<TypeOfEvent>();
    	
    	
        String keyArray[] = eventsWithKey.keySet().toArray(new String[0]);

        for (int i = 0; i < keyArray.length - 1; i++) {
            for (int j = i + 1; j < keyArray.length; j++) {
                // if true swap
                if (Long.parseLong(keyArray[i].substring(keyArray[i].indexOf("+") + 1)) > Long.parseLong(keyArray[j].substring(keyArray[j].indexOf("+") + 1))) {
                    String tmp = keyArray[i];
                    keyArray[i] = keyArray[j];
                    keyArray[j] = tmp;
                }
            }
        }
        

        for (int i = 0; i < keyArray.length; i++) 
        	returnEvents.add(eventsWithKey.get(keyArray[i]));        
        
        
    	return returnEvents;
    }

}
