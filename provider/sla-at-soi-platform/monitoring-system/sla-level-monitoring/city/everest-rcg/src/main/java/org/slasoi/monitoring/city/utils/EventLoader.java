/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * 
 */
package org.slasoi.monitoring.city.utils;

/**
 * @author Davide Lorenzoli
 * 
 * @date 13 Jul 2009
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is a utility class for loading SLA@SOI events from file
 * 
 */
public class EventLoader {
    //private static String fromDelimeter;
    //private static String toDelimeter;
	private static Pattern fromPattern;
	private static Pattern toPattern;

    /**
     * Load, from a text file, XML events in EVEREST monitor engine complaint.
     * 
     * @param fileName
     * @return
     * @throws IOException
     */
    public static ArrayList<String> loadEverestEvents(String fileName) throws IOException {
    	/*
        fromDelimeter = "<event>";
        toDelimeter = "</event>";
		*/
    	
    	fromPattern = Pattern.compile("<event>");
    	toPattern = Pattern.compile("</event>");    	
    	
        return loadEvents(fileName);
    }

    /**
     * Load, from a text file, XML events SLA@SOI complaint.
     * 
     * @param fileName
     * @return
     * @throws IOException
     */
    public static ArrayList<String> loadSLASOIEvents(String fileName) throws IOException {
    	/*
        fromDelimeter = "<ns2:Event";
        toDelimeter = "</ns2:Event>";
		*/
    	
    	fromPattern = Pattern.compile("<([a-zA-Z0-9]*:)?Event");
    	toPattern = Pattern.compile("</([a-zA-Z0-9]*:)?Event>");
        return loadEvents(fileName);
    }

    /**
     * @param fileName
     * @return
     * @throws IOException
     */
    private static ArrayList<String> loadEvents(String fileName) throws IOException {
        FileReader fr = new FileReader(fileName);
        BufferedReader br = new BufferedReader(fr);
        ArrayList<String> events = new ArrayList<String>();

        String eachLine = br.readLine();

        boolean insideEvent = false;

        String event = new String();
        //Matcher matcher = null;
        
        while (eachLine != null) {

            // check for the beginning event tag
        	
        	Matcher matcher = fromPattern.matcher(eachLine);
        	
        	
        	if(matcher.find()){
            //if (!insideEvent && eachLine.indexOf(fromDelimeter) != -1) {
                insideEvent = true;
            }
			
        	
        	
            // check for the ending event tag
        	
        	matcher = toPattern.matcher(eachLine);
        	if(matcher.find()){
            //if (insideEvent && eachLine.indexOf(toDelimeter) != -1) {
                // add the closing tag
                event += eachLine + "\n";
                // add the event to the event list
                events.add(event);
                
                //System.out.println(event);

                // reset the event
                event = new String();
                // set inside event to false
                insideEvent = false;
            }

            // add a line belonging to en event
            if (insideEvent) {
                event += eachLine + "\n";
            }

            eachLine = br.readLine();
        }

        return events;
    }
}
