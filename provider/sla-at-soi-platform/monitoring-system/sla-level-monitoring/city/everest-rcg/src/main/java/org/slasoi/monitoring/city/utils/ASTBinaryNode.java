/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.utils;

/**
 * @author SLA@SOI (City)
 *
 * @date June 22, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 *        JavaDoc: YES
 */

/**
 * ASTBinaryNode extends the ASTNode class for modelling AST node that have only two children defined explicitly as left
 * and right child
 * 
 */
public class ASTBinaryNode extends ASTNode {

    private ASTNode leftChild;
    private ASTNode rightChild;

    /**
     * Constructor.
     */
    public ASTBinaryNode() {
        super();
        this.leftChild = null;
        this.rightChild = null;
    }

    /**
     * Retrieves the left child of this AST node.
     * 
     * @return
     */
    public ASTNode getLeftChild() {
        return leftChild;
    }

    /**
     * Sets the left child of this AST node.
     * 
     * @param leftChild -
     *            the left child to set
     */
    public void setLeftChild(ASTNode leftChild) {
        this.leftChild = leftChild;
        this.addChild(leftChild);
    }

    /**
     * Retrieves the right child of this AST node.
     * 
     * @return
     */
    public ASTNode getRightChild() {
        return rightChild;
    }

    /**
     * Sets the right child of this AST node.
     * 
     * @param rightChild -
     *            the right child to set
     */
    public void setRightChild(ASTNode rightChild) {
        this.rightChild = rightChild;
        this.addChild(rightChild);
    }

    /**
     * Returns a booleans indicating whether this AST node has the input node in its left sub-tree.
     * 
     * @param node -
     *            the input node
     * @return
     */
    public boolean hasInLeftSubtree(ASTNode node) {
        boolean inLeftSubtree = false;

        if (this.leftChild != null) {
            if (this.leftChild.equals(node)) {
                inLeftSubtree = true;
            }
            else {
                for (ASTNode leftGChild : this.leftChild.getChildren()) {
                    if (leftGChild.equals(node) || leftGChild.hasDescendent(node)) {
                        inLeftSubtree = true;
                    }
                }
            }
        }

        return inLeftSubtree;
    }

}
