/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * 
 */
package org.slasoi.monitoring.city.impl;

/**
 * @author SLA@SOI (City)
 * 
 * @date 09 July 2010
 * 
 * Flags: SLASOI checkstyle: YES JavaDoc: YES
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




import org.apache.log4j.Logger;
import org.slasoi.common.messaging.pubsub.MessageEvent;
import org.slasoi.common.messaging.pubsub.MessageListener;
import org.slasoi.monitoring.city.conf.RCGProperties;
import org.slasoi.monitoring.city.database.Operator.OperatorEntityManager;
import org.slasoi.monitoring.city.database.Operator.OperatorEntityManagerInterface;
import org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManager;
import org.slasoi.monitoring.city.database.functionalExprOperatorParametricTemplate.FunctionalExprOperatorParametricTemplateEntityManager;
import org.slasoi.monitoring.city.database.functionalExprOperatorParametricTemplate.FunctionalExprOperatorParametricTemplateEntityManagerInterface;
import org.slasoi.monitoring.city.database.monitoringConfiguration.MonitoringConfigurationEntityManager;
import org.slasoi.monitoring.city.database.monitoringConfiguration.MonitoringConfigurationEntityManagerInterface;
import org.slasoi.monitoring.city.database.slasoiInteractionEvent.SlasoiInteractionEventEntityManager;
import org.slasoi.monitoring.city.database.slasoiMonitoringResultEvent.SlasoiMonitoringResultEventEntityManager;
import org.slasoi.monitoring.city.database.slasoiMonitoringResultEvent.SlasoiMonitoringResultEventEntityManagerInterface;
import org.slasoi.monitoring.city.service.CityRCGService;
import org.slasoi.monitoring.city.translator.Translator;
import org.slasoi.monitoring.city.utils.ParametricTemplateHandler;
import org.slasoi.monitoring.city.utils.RCGConstants;
import org.slasoi.monitoring.common.configuration.ReasonerConfiguration;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.common;

import uk.ac.city.soi.database.DatabaseManagerFactory;
import uk.ac.city.soi.database.DatabaseManagerInterface;
import uk.ac.city.soi.database.EntityManagerFactoryInterface;
import uk.ac.city.soi.everest.NewDataAnalyzerEC;
import uk.ac.city.soi.everest.SRNTEvent.TypeOfEvent;
import uk.ac.city.soi.everest.bse.Formula;
import uk.ac.city.soi.everest.database.EntityManagerUtil;

/**
 * This class is the actual implementation of EVEREST Reasoning Component Gateway.
 */

public class EverestRCG implements CityRCGService, MessageListener{

    // initialise logging
    Logger logger = Logger.getLogger(EverestRCG.class);

    // initialise connection to rcg db

    Connection rcgConnection;

    // Core City Monitor (Analyzer)
    NewDataAnalyzerEC ndaec;

    // uk.ac.city.soi.everest rcg uuid
    private String uuid = RCGConstants.EVEREST_RCG_UUID;

    // local collection of received reasoner configurations
    private ArrayList<ReasonerConfiguration> reasonerConfigurations = new ArrayList<ReasonerConfiguration>();
    
    // uk.ac.city.soi.everest monitoring features
    private ComponentMonitoringFeatures componentMonitoringFeatures;

    // uk.ac.city.soi.everest rcg translator
    private Translator translator;

    // entity managers initiation prerequisites
    private DatabaseManagerInterface databaseManager;
    private EntityManagerFactoryInterface entityManagerFactory;

    // entity managers
    private AgreementTermEntityManager agreementTermStorer;
    private FunctionalExprOperatorParametricTemplateEntityManagerInterface functionalExprOperatorParametricTemplateStorer;
    private MonitoringConfigurationEntityManagerInterface monitoringConfigurationStorer;
    private OperatorEntityManagerInterface operatorStorer;
    private SlasoiInteractionEventEntityManager slasoiInteractionEventStorer;
    private SlasoiMonitoringResultEventEntityManagerInterface slasoiMonitoringResultEventStorer;
    
    //Timers 
    private Timer eventTimer;
    private Timer monResultTimer;

    private String slasoiOrcHome = "";
    private MonitoringResultsManager monitoringResultsManager;

    
    /**
     * Constructor.
     */
    public EverestRCG() {
    	slasoiOrcHome = System.getenv().get("SLASOI_HOME");

        if (slasoiOrcHome == null) {
            logger.error("SLASOI_HOME variable not set");
            System.exit(1);
        }

        logger.debug("SLASOI_HOME is set to: " + slasoiOrcHome);

        // Core City Monitor (Analyzer)
        ndaec = new NewDataAnalyzerEC(slasoiOrcHome + "/monitoring-system/sla-level-monitoring/city/everest");


        // initiate db manager
        databaseManager =
                DatabaseManagerFactory.getDatabaseManager(slasoiOrcHome
                        + "/monitoring-system/sla-level-monitoring/city/rcg.database.properties");
        // get entity manager factory
        entityManagerFactory = databaseManager.getEntityManagerFactory();

        // initiate agreementTermStorer
        //agreementTermStorer = new AgreementTermEntityManager(entityManagerFactory.getConnectionManager().getConnection());
        agreementTermStorer =(AgreementTermEntityManager) entityManagerFactory.getEntityManager(AgreementTermEntityManager.class);
        // add agreementTermStorer to rcg properties
        RCGProperties.addProperty(RCGConstants.AGREEMENT_TERM_STORER_KEY, agreementTermStorer);

        // initiate functionalExprOperatorParametricTemplateStorer
        functionalExprOperatorParametricTemplateStorer =
                (FunctionalExprOperatorParametricTemplateEntityManager) entityManagerFactory
                        .getEntityManager(FunctionalExprOperatorParametricTemplateEntityManager.class);
        // add functionalExprOperatorParametricTemplateStorer to rcg properties
        RCGProperties.addProperty(RCGConstants.FUNCTIONAL_EXPR_OPERATOR_PARAMETRIC_TEMPLATE_STORER_KEY,
                functionalExprOperatorParametricTemplateStorer);

        // initiate functionalExprOperatorParametricTemplateStorer
        monitoringConfigurationStorer =
                (MonitoringConfigurationEntityManager) entityManagerFactory
                        .getEntityManager(MonitoringConfigurationEntityManager.class);
        // add monitoringConfigurationStorer to rcg properties
        RCGProperties.addProperty(RCGConstants.MONITORING_CONFIGURATION_STORER_KEY, monitoringConfigurationStorer);

        // initiate operatorStorer
        operatorStorer = (OperatorEntityManager) entityManagerFactory.getEntityManager(OperatorEntityManager.class);
        // add operatorStorer to rcg properties
        RCGProperties.addProperty(RCGConstants.OPERATOR_STORER_KEY, operatorStorer);

        // initiate slasoiInteractionEventStorer
        slasoiInteractionEventStorer =
                (SlasoiInteractionEventEntityManager) entityManagerFactory
                        .getEntityManager(SlasoiInteractionEventEntityManager.class);
        // add slasoiInteractionEventStorer to rcg properties
        RCGProperties.addProperty(RCGConstants.SLASOI_INTERACTION_EVENT_STORER_KEY, slasoiInteractionEventStorer);

        // initiate slasoiMonitoringResultEventStorer
        slasoiMonitoringResultEventStorer =
                (SlasoiMonitoringResultEventEntityManager) entityManagerFactory
                        .getEntityManager(SlasoiMonitoringResultEventEntityManager.class);
        // add slasoiMonitoringResultEventStorer to rcg properties
        RCGProperties.addProperty(RCGConstants.SLASOI_MONITORING_RESULT_EVENT_STORER_KEY,
                slasoiMonitoringResultEventStorer);

        
        LinkedHashMap<String, String> guaranteeTermFormulaIdMap = new LinkedHashMap<String, String>();
        RCGProperties.addProperty(RCGConstants.GUARANTEED_TERM_FORMULA_ID_MAP_KEY, guaranteeTermFormulaIdMap);
        
        // truncate rcg db tables
        logger.debug("Truncate RCG db tables");
        /*
        agreementTermStorer.execute("TRUNCATE rcg.agreement_term");
        agreementTermStorer.execute("TRUNCATE rcg.operator");
        agreementTermStorer.execute("TRUNCATE rcg.monitoring_configuration");
        agreementTermStorer.execute("TRUNCATE rcg.functional_expr_operator_parametric_template");
        agreementTermStorer.execute("TRUNCATE rcg.slasoi_interaction_event");
        agreementTermStorer.execute("TRUNCATE rcg.slasoi_mon_result_event");
        */
        agreementTermStorer.execute("TRUNCATE agreement_term");
        agreementTermStorer.execute("TRUNCATE operator");
        agreementTermStorer.execute("TRUNCATE monitoring_configuration");
        agreementTermStorer.execute("TRUNCATE functional_expr_operator_parametric_template");
        agreementTermStorer.execute("TRUNCATE slasoi_interaction_event");
        agreementTermStorer.execute("TRUNCATE slasoi_mon_result_event");        
        // initialise an entityManagerUtil i.e. serenity db manager
        Connection connection = uk.ac.city.soi.everest.database.DatabaseManagerFactory.getDatabaseManager().getConnectionManager().getConnection();
        //Connection connection = databaseManager.getConnectionManager().getConnection();
        EntityManagerUtil entityManagerUtil = new EntityManagerUtil(connection);
        
        // truncate serenity db tables
        logger.debug("Truncate Serenity db tables");
        try {
            entityManagerUtil.executeQuery("TRUNCATE event");
            entityManagerUtil.executeQuery("TRUNCATE fluent");
            entityManagerUtil.executeQuery("TRUNCATE predicate");
            entityManagerUtil.executeQuery("TRUNCATE statistic");
            entityManagerUtil.executeQuery("TRUNCATE template");
            entityManagerUtil.executeQuery("TRUNCATE templatedefined");        	
        } catch (SQLException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        
        
		ParametricTemplateHandler handler = new ParametricTemplateHandler();
        handler.insertQoSTerm(common.mttr.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricMTTRTheory.xml",
                "A5.MTTR");
        handler.insertQoSTerm(common.availability.toString(),
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricAvailabilityTheory.xml", "A6.Availability");
        
        handler.insertQoSTerm(common.throughput.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricThroughputTheory.xml",
				"A4.Throughput");         
        handler.insertQoSTerm(common.mttf.toString(), slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricMTTFTheory.xml",
        		"A4.MTTF");
        handler.insertQoSTerm(common.reliability.toString(),
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricReliabilityTheory.xml", "R4.Reliability");
        handler.insertQoSTerm(common.completion_time.toString(),
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricCompletionTimeTheory.xml", "A1.completion_time");
        handler.insertQoSTerm("http://www.slaatsoi.org/extension#completion_time",
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricCompletionTimeTheory.xml", "A1.completion_time");
        handler.insertQoSTerm(common.arrival_rate.toString(),
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricArrivalRateTheory.xml", "R4.arrival_rate");
        handler.insertQoSTerm(common.accessibility.toString(),
                slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricAccessibilityTheory.xml", "R5.Accessibility");
        handler.insertQoSTerm(RCGConstants.DEFAULT_PARAMETRIC_TEMPLATE, slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricDefaultTheory.xml",
        "A1.default_template");
        
        handler.insertQoSTerm(RCGConstants.IDLE_TIME_TEMPLATE, slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricIdleTimeTheory.xml",
        "A1.idle_time");
        handler.insertQoSTerm(RCGConstants.TTF_TEMPLATE, slasoiOrcHome+"/monitoring-system/sla-level-monitoring/city/parametricTemplates/parametricTTFTheory.xml",
        "A3.TTF");        
                
        // subscribe to pub sub bus
        subscribe_to_interaction_events();
        
        //this.subscribe_to_interaction_events("A4MonTest");      
        //start timers
        
        monitoringResultsManager = new MonitoringResultsManager("", this.ndaec);
        
        // checks periodically for Monitoring Results generated by EVEREST
        checkPeriodicallyForMonitoringResults();
        
        //eventTimer = new Timer();
        //eventTimer.schedule(new SLASOIDBEventLoader(slasoiInteractionEventStorer, ndaec), 0, 1000);
        
        
        // initialise message listener
        //messageListener = new EverestRCGMessageListener();

    }

    /**
     * Transforms sla soi message event to TypeOfEvent uk.ac.city.soi.everest event object.
     * 
     * @param messageEvent
     * @return
     */
    public void processMessage(MessageEvent messageEvent) {
    	logger.debug("Received Event " + messageEvent);
    	//System.out.println("Received Event " + messageEvent);
        TypeOfEvent event = null;

        // get the payload string of the message
        String xmlEvent = messageEvent.getMessage().getPayload();
        ArrayList<String> newEvents = prePorcessEvents(xmlEvent);
        for(String payload : newEvents){
            final String TIME_TAG = "Timestamp>";
            
            String ts = payload.substring(payload.indexOf(TIME_TAG) + TIME_TAG.length(), payload.lastIndexOf(TIME_TAG));
            String timeStamp = ts.substring(0, ts.indexOf("<")).trim();
            // process the payload
            InteractionEventManager eventManager = new InteractionEventManager();
            event = eventManager.toCoreMonitorFormatObject(payload);
            

            slasoiInteractionEventStorer.addEvent(((Object) (event.hashCode())).toString() + "+" + timeStamp, payload, event);
            
            ndaec.receiveTypeOfEventObject(event);        	
        }

        
        //return this.messageListener.returnedProcessedMessage(messageEvent);
    }

    /**
     * Feeds EVEREST with monitoring specification generated from the input sla.
     * 
     * @param slaUuid
     */
    public void feedEverestWithMonitoringSpecifications(String slaUuid) {

    	//System.err.println("33333Starting monitoring for  " + slaUuid);
    	
        ArrayList<String> aTIds = agreementTermStorer.getAgreementsTermIds(slaUuid);
        
        //System.err.println("33333Size of aTIds " + aTIds.size());
        
        LinkedHashMap<String, Formula> slaFormulas = new LinkedHashMap<String, Formula>();
        HashMap<String, String> formulaIdSLAIdMap = new HashMap<String, String>();
        for (String aTId : aTIds) {

            LinkedHashMap<String, Formula> aTFormulas = agreementTermStorer.getAgreementTermTheory(slaUuid, aTId);
            //System.err.println("44444Size of aTFormulas " + aTFormulas + " for id " + aTId);
            //System.err.println("44444Size of aTFormulas " + aTFormulas.size() + " for id " + aTId);
            
            Set set = aTFormulas.entrySet();
            Iterator i = set.iterator();
             
            while (i.hasNext()) {
                Map.Entry me = (Map.Entry) i.next();
                slaFormulas.put((String) me.getKey(), (Formula) me.getValue());
                formulaIdSLAIdMap.put((String) me.getKey(), slaUuid);
                //System.err.println("6666 Ading formula " + (String) me.getKey());
            }

        }
        //System.err.println("Number of formulas " + slaFormulas.size());
        try {
            ndaec.receiveFormulas(slaFormulas);
        }
        catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }       
        monitoringResultsManager.addFormulaIdSLAIdMap(formulaIdSLAIdMap);
        monitoringResultsManager.addSLAId(slaUuid);
    }

    /**
     * Feeds EVEREST with SLA@SOI message events.
     * 
     * @param messageEvent
     * @return
     */
    public boolean feedEverestWithEvent(MessageEvent messageEvent) {
        TypeOfEvent event = null;

        // get the payload string of the message
        String payload = messageEvent.getMessage().getPayload();

        // process the payload
        InteractionEventManager eventManager = new InteractionEventManager();
        event = eventManager.toCoreMonitorFormatObject(payload);
        

        slasoiInteractionEventStorer.addEvent(((Object) (event.hashCode())).toString(), messageEvent.getMessage()
                .getPayload(), event);
        
        return ndaec.receiveTypeOfEventObject(event);
        //return true;
    }

    /**
     * @see org.slasoi.monitoring.core.ReasoningComponentGateway#addConfiguration(org.slasoi.monitoring.common.configuration.ReasonerConfiguration)
     */
    public void addConfiguration(ReasonerConfiguration reasonerConfiguration) {

    	//System.out.println("SSSS Adding configuration " + reasonerConfiguration.getConfigurationId());
        // store to local collection
        reasonerConfigurations.add(reasonerConfiguration);

        Object specification = reasonerConfiguration.getSpecification();               

        //System.out.println("SSSS Specification " + specification.toString());
        
        if (specification instanceof SLA) {
            SLA sla = (SLA) specification;
            //System.out.println("SSSS Specification is SLA " + sla.toString());
            // connect and store to db
            /*
            monitoringConfigurationStorer.addConfiguration(sla.getUuid().toString(), reasonerConfiguration
                    .getConfigurationId(), reasonerConfiguration);
			*/
            // initialise rcg translator
            translator = new Translator(sla, this.ndaec);

            // translate sla agreement terms
            translator.translate();

        }
    }

    /**
     * @see org.slasoi.monitoring.core.ReasoningComponentGateway#getComponentMonitoringFeatures()
     */
    public ComponentMonitoringFeatures getComponentMonitoringFeatures() {
        return componentMonitoringFeatures;
    }

    /**
     * @see org.slasoi.monitoring.core.ReasoningComponentGateway#getConfigurations()
     */
    public ArrayList<ReasonerConfiguration> getConfigurations() {
        return reasonerConfigurations;
    }

    /**
     * @see org.slasoi.monitoring.core.ReasoningComponentGateway#getUuid()
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @see org.slasoi.monitoring.core.ReasoningComponentGateway#removeConfiguration(java.lang.String)
     */
    public void removeConfiguration(String configurationId) {

        // remove from local collection
        ReasonerConfiguration toBeRemoved = null;
        Iterator<ReasonerConfiguration> iterator = this.reasonerConfigurations.iterator();
        while (iterator.hasNext()) {
            ReasonerConfiguration rc = iterator.next();
            if (rc.getConfigurationId().equals(configurationId)) {
                toBeRemoved = rc;
            }
        }
        if (toBeRemoved != null) {
            this.reasonerConfigurations.remove(toBeRemoved);
        }
    }

    /**
     * @see org.slasoi.monitoring.core.ReasoningComponentGateway#setComponentMonitoringFeatures(org.slasoi.monitoring.common.features.ComponentMonitoringFeatures)
     */
    public void setComponentMonitoringFeatures(ComponentMonitoringFeatures componentMonitoringFeatures) {
        this.componentMonitoringFeatures = componentMonitoringFeatures;
    }

    /**
     * @see org.slasoi.monitoring.core.ReasoningComponentGateway#setUuid(java.lang.String)
     */
    public void setUuid(String value) {
        uuid = value;
    }

    /**
     * @see org.slasoi.monitoring.core.ReasoningComponentGateway#startMonitoring(java.lang.String)
     */
    public boolean startMonitoring(String configurationId) {

        // retrieve monitoring specifications generated from the given configuration
        String slaUuid = null;
        for (ReasonerConfiguration conf : reasonerConfigurations) {
            if (conf.getConfigurationId().equals(configurationId)) {
                Object obj = conf.getSpecification();
                if (obj instanceof SLA) {
                    slaUuid = ((SLA) obj).getUuid().toString();
                }
                else if (obj instanceof SLATemplate) {
                    slaUuid = ((SLATemplate) obj).getUuid().toString();
                }
            }
        }

        // feed uk.ac.city.soi.everest with monitoring specifications
        feedEverestWithMonitoringSpecifications(slaUuid);

        return true;
    }

    /**
     * Checks periodically for Monitoring Results generated by EVEREST.
     * 
     * @param slaId
     * @param ndaec
     */
    public void checkPeriodicallyForMonitoringResults() {

        // initialise a new MonitoringResultsManager thread
    	monResultTimer = new Timer();
        //MonitoringResultsManager manager = new MonitoringResultsManager(slaId, ndaec);
        monResultTimer.schedule(monitoringResultsManager, 10000, 20000);
        logger.debug("scheduledExecutionTime: " +  monitoringResultsManager.scheduledExecutionTime());
    }

    /**
     * @see org.slasoi.monitoring.core.ReasoningComponentGateway#stopMonitoring(java.lang.String)
     */
    public boolean stopMonitoring(String configurationId) {
        logger.debug("startMonitoring code to be implemented");
        eventTimer.cancel();
        //throw new RuntimeException("startMonitoring code to be implemented");
        
        return true;
    }

    /**
     * Returns the EVEREST instance.
     * 
     * @return the ndaec
     */
    public NewDataAnalyzerEC getNdaec() {
        return ndaec;
    }

    /*******************************************************************************************************************
     * Protected methods: visible only by inheriting classes
     ******************************************************************************************************************/

    /**
     * @return the translator
     */
    protected Translator getTranslator() {
        return translator;
    }

    /**
     * @param translator
     *            the translator to set
     */
    protected void setTranslator(Translator translator) {
        this.translator = translator;
    }

    /**
     * @param ndaec
     */
    protected void setNdaec(NewDataAnalyzerEC ndaec) {
        this.ndaec = ndaec;
    }

    /**
     * subscribe with the bus to listen the interaction event.
     */
    private void subscribe_to_interaction_events() {
    	Properties props = new Properties();
    	String chName = "EverestEventChannel";
    	 //try retrieve data from file
        try {
        	props.load(new FileInputStream(slasoiOrcHome+ "/monitoring-system/sla-level-monitoring/city/slasoiInteractionEventBus.properties"));
        	chName = props.getProperty("xmpp_channel");
        	//System.out.println(chName);
         }catch(IOException e){
        	 e.printStackTrace();
        }    	
    	//slasoiOrcHome+ "/monitoring-system/sla-level-monitoring/city/slasoiInteractionEventBus.properties"
        // subscribe with the bus to listen the interaction event
        SLASOIInteractionEventSubscriber event_subscriber = new SLASOIInteractionEventSubscriber();
        event_subscriber.subscribe(chName, this);
    }

    /*
     * Following method is only to support B6SLA, in case of extended type of completion time 
     */
	private ArrayList<String> prePorcessEvents(String event){
		ArrayList<String> events = new ArrayList<String>(); 
		//String val = "";
		if(event.contains("phoneCall") && event.contains("REQ")){
			Pattern oidPattern = Pattern.compile("<OperationID>([a-zA-Z0-9]*)?</OperationID>");
			Matcher matcher = oidPattern.matcher(event);
			String matched = "";
	        if(matcher.find())
	            matched = matcher.group();
	        String oId = matched.substring("<OperationID>".length(), matched.length() - "</OperationID>".length());
	        //oId = oId.substring(0, )
	        //System.out.println("+++++++++++++ oId Pattern " + oId);
	        int statusIndex = event.indexOf("</Status>") + "</Status>".length();
	        
	        String firstPart = event.substring(0, statusIndex);
	        String lastPart = event.substring(statusIndex + 1);
	        
	        String midPart = "<Parameters><Argument><Simple><ArgName>operationId</ArgName>" +
                             "<ArgType>http://www.w3.org/2001/XMLSchema#string</ArgType>" +
                             "<Direction>IN</Direction>" +
                             "<Value xsi:type=\"xs:string\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
                             oId + "</Value></Simple></Argument></Parameters>";	        	       
	        events.add(firstPart + midPart + lastPart);			
		}
		else if(event.contains("phoneCall") && event.contains("RES")){
			
			Pattern oidPattern = Pattern.compile("<OperationID>([a-zA-Z0-9]*)?</OperationID>");
			Matcher matcher = oidPattern.matcher(event);
			String matched = "";
	        if(matcher.find())
	            matched = matcher.group();
	        String oId = matched.substring("<OperationID>".length(), matched.length() - "</OperationID>".length());
	        //oId = oId.substring(0, )
	        //System.out.println("+++++++++++++ oId Pattern " + oId);
	        int parameterIndex = event.indexOf("<Parameters>") + "<Parameters>".length();
	        
	        String firstPart = event.substring(0, parameterIndex);
	        String lastPart = event.substring(parameterIndex + 1);
	        
	        String midPart = "<Argument><Simple><ArgName>operationId</ArgName>" +
                             "<ArgType>http://www.w3.org/2001/XMLSchema#string</ArgType>" +
                             "<Direction>OUT</Direction>" +
                             "<Value xsi:type=\"xs:string\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
                             oId + "</Value></Simple></Argument>";	        
	        events.add(firstPart + midPart + lastPart);
		}
		else if(event.contains("operatorAnswer") && event.contains("REQ")){
			Pattern pattern = Pattern.compile("<OperationID>([a-zA-Z0-9]*)?</OperationID>");
			Matcher matcher = pattern.matcher(event);
			String matched = "";
	        if(matcher.find())
	            matched = matcher.group();
	        String oId = matched.substring("<OperationID>".length(), matched.length() - "</OperationID>".length());
	        //oId = oId.substring(0, )
	        //System.out.println("+++++++++++++ oId Pattern " + oId);
	        int index = event.indexOf("</Status>") + "</Status>".length();
	        
	        String firstPart = event.substring(0, index);
	        String lastPart = event.substring(index + 1);
	        
	        String midPart = "<Parameters><Argument><Simple><ArgName>operationId</ArgName>" +
                             "<ArgType>http://www.w3.org/2001/XMLSchema#string</ArgType>" +
                             "<Direction>IN</Direction>" +
                             "<Value xsi:type=\"xs:string\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
                             oId + "</Value></Simple></Argument></Parameters>";
	        
	        String val = firstPart + midPart + lastPart; 	        			
	        events.add(val);
	        
	        val = val.replace("ServiceOperationCallStartEvent", "ServiceOperationCallEndEvent");
	        val = val.replace("<Status>REQ-A</Status>", "<Status>RES-A</Status>");
	        val = val.replace("<Direction>IN</Direction>", "<Direction>OUT</Direction>");
	        
	        index = val.indexOf("<Sender>") + "<Sender>".length() + 1;
	        int index1 = val.indexOf("</Sender>");
	        String sender = val.substring(index, index1);
	        	       	        
	        index = val.indexOf("<Receiver>") + "<Receiver>".length() + 1;
	        index1 = val.indexOf("</Receiver>");	        
	        String receiver = val.substring(index, index1);
	         
	        index = val.indexOf("</Notifier>") + "</Notifier>".length();
	        firstPart = val.substring(0, index);
	        
	        index = val.indexOf("</EventContext>");
	        lastPart = val.substring(index);
	        
	        midPart = "<Source><SwServiceLayerSource><Sender>" + receiver + "</Sender><Receiver>" +
	                  sender + "</Receiver></SwServiceLayerSource></Source>";
	        
	        events.add(firstPart + midPart + lastPart);
		}		
		else events.add(event);
		
		return events;
		
	}    
}
