/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.database.Operator;

/**
 * @author SLA@SOI (City)
 * 
 * @date 28 June 2010
 * 
 * Flags: SLASOI checkstyle: YES JavaDoc: YES
 */
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.slasoi.monitoring.city.utils.RCGConstants;

import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.database.EntityManagerInterface;
import uk.ac.city.soi.database.PersistentEntity;

/**
 * This class provides methods for managing the Operator MySQL database table.
 * 
 */
public class OperatorEntityManager extends EntityManagerCommons implements EntityManagerInterface,
        OperatorEntityManagerInterface {
    // logger
    private static Logger logger = Logger.getLogger(OperatorEntityManager.class);

    /**
     * Constructor.
     * 
     * @param connection
     */
    public OperatorEntityManager(Connection connection) {
        super(connection);
    }

    /*******************************************************************************************************************
     * IMPlEMENTED METHODS FROM functionalExprOperatorParametricTemplateEntityManagerInterface
     ******************************************************************************************************************/

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
     */
    public int delete(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
     */
    public int deleteByPrimaryKey(String arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
     */
    public int insert(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
     */
    public int update(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
     */
    public ArrayList executeQuery(String query) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#select()
     */
    public ArrayList select() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
     */
    public PersistentEntity selectByPrimaryKey(String entityPrimaryKey) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see org.slasoi.monitoring.city.database.Operator.OperatorEntityManagerInterface#insert(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Object[])
     */
    public void insert(String slaUUId, String atId, String operatorCaseId, String preconditionOf, String gsId,
            String vdId, String interfaceId, String parameterOf, String operatorSignature, String operatorType,
            String operator, Object[] operatorPath) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        Blob functionalExprOperatorPathBlob = null;
        try {
            functionalExprOperatorPathBlob = toBlob(operatorPath);
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //System.out.println(getConnection());

        try {
            pstmt =
                    getConnection().prepareStatement(
                            "INSERT INTO operator " + "(agreement_term_monitoring_configuration_sla_uuid, "
                                    + "agreement_term_agreement_term_id, operator_hash_code, "
                                    + "precondition_of, guaranteed_state_id, variable_declaration_id, "
                                    + "interface_declaration_id, parameter_of, operator_signature, "
                                    + "operator_type, operator, operator_path) "
                                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            pstmt.setString(1, slaUUId);
            pstmt.setString(2, atId);
            pstmt.setString(3, operatorCaseId);
            pstmt.setString(4, preconditionOf);
            pstmt.setString(5, gsId);
            pstmt.setString(6, vdId);
            pstmt.setString(7, interfaceId);
            pstmt.setString(8, parameterOf);
            pstmt.setString(9, operatorSignature);
            pstmt.setString(10, operatorType);
            pstmt.setString(11, operator);
            pstmt.setBlob(12, functionalExprOperatorPathBlob);
            //logger.debug(pstmt.toString());
            pstmt.executeUpdate();
        }
        catch (SQLException ex) {
            // handle any errors
//            logger.debug("SQLException: " + ex.getMessage());
//            logger.debug("SQLState: " + ex.getSQLState());
//            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

    }

    /**
     * @see org.slasoi.monitoring.city.database.Operator.OperatorEntityManagerInterface#getGuaranteedStatesIds(java.lang.String,
     *      java.lang.String)
     */
    public ArrayList<String> getGuaranteedStatesIds(String slaUuid, String agreementTermId) {
        ArrayList<String> guaranteedStatesIds = new ArrayList<String>();
        String guaranteedStateId = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt =
                    getConnection()
                            .prepareStatement(
                                    "SELECT guaranteed_state_id FROM operator "
                                            + "WHERE agreement_term_monitoring_configuration_sla_uuid = ? AND agreement_term_agreement_term_id = ?");

            pstmt.setString(1, slaUuid);
            pstmt.setString(2, agreementTermId);

            resultSet = pstmt.executeQuery();

            if (resultSet.first()) {
                do {
                    guaranteedStateId = resultSet.getString("guaranteed_state_id");
                    if (!guaranteedStateId.equals(RCGConstants.NOT_APPLICABLE)) {
                        guaranteedStatesIds.add(guaranteedStateId);
                    }
                }
                while (resultSet.next());
            }

        }
        catch (SQLException ex) {
            // handle any errors
//            logger.debug("SQLException: " + ex.getMessage());
//            logger.debug("SQLState: " + ex.getSQLState());
//            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return guaranteedStatesIds;
    }

    /**
     * @see org.slasoi.monitoring.city.database.Operator.OperatorEntityManagerInterface#getFunctExprOperatorHashCodesAndPaths(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public HashMap<String, Object[]> getFunctExprOperatorHashCodesAndPaths(String slaUuid, String agreementTermId,
            String guaranteed_state_id) {
        HashMap<String, Object[]> hashCodesAndPaths = new HashMap<String, Object[]>();
        String hashCode = null;
        Object[] path = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt =
                    getConnection()
                            .prepareStatement(
                                    "SELECT operator_hash_code, operator_path FROM operator "
                                            + "WHERE agreement_term_monitoring_configuration_sla_uuid = ? AND agreement_term_agreement_term_id = ? AND guaranteed_state_id = ?");

            pstmt.setString(1, slaUuid);
            pstmt.setString(2, agreementTermId);
            pstmt.setString(3, guaranteed_state_id);

            resultSet = pstmt.executeQuery();

            if (resultSet.first()) {
                do {
                    hashCode = resultSet.getString("operator_hash_code");
                    // get the blob
                    Blob blob = resultSet.getBlob("operator_path");
                    try {
                        path = ((Object[]) toObject(blob));
                    }
                    catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }                    
//                    catch (IOException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//                    catch (ClassNotFoundException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
                    hashCodesAndPaths.put(hashCode, path);
                }
                while (resultSet.next());
            }

        }
        catch (SQLException ex) {
            // handle any errors
//            logger.debug("SQLException: " + ex.getMessage());
//            logger.debug("SQLState: " + ex.getSQLState());
//            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return hashCodesAndPaths;
    }

    /**
     * @see org.slasoi.monitoring.city.database.Operator.OperatorEntityManagerInterface#existsRecord(java.lang.String)
     */
    public boolean existsRecord(String caseId) {
        boolean exists = false;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt =
                    getConnection().prepareStatement(
                            "SELECT operator_hash_code FROM operator " + "WHERE operator_hash_code = ?");

            pstmt.setString(1, caseId);

            resultSet = pstmt.executeQuery();

            if (resultSet.first()) {
                exists = true;
            }

        }
        catch (SQLException ex) {
            // handle any errors
//            logger.debug("SQLException: " + ex.getMessage());
//            logger.debug("SQLState: " + ex.getSQLState());
//            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return exists;
    }

    /**
     * @see org.slasoi.monitoring.city.database.Operator.OperatorEntityManagerInterface#update(java.lang.String,
     *      java.lang.Object[])
     */
    public void update(String caseId, Object[] qosTermPath) {

        PreparedStatement pstmt = null;
        int resultSet = 0;

        Blob functionalExprOperatorPathBlob = null;
        try {
            functionalExprOperatorPathBlob = toBlob(qosTermPath);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
//        catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        catch (SQLException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }

        try {
            pstmt =
                    getConnection().prepareStatement(
                            "UPDATE operator SET operator_path = ?" + "WHERE operator_hash_code = ?");

            pstmt.setBlob(1, functionalExprOperatorPathBlob);
            pstmt.setString(2, caseId);

            resultSet = pstmt.executeUpdate();
            //logger.debug(resultSet);

        }
        catch (SQLException ex) {
            // handle any errors
//            logger.debug("SQLException: " + ex.getMessage());
//            logger.debug("SQLState: " + ex.getSQLState());
//            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }

    }

    /*******************************************************************************************************************
     * INHERITED METHODS FROM EntityManagerInterface
     ******************************************************************************************************************/

    /**
     * @see uk.ac.city.soi.database.EntityManagerCommons#count()
     */
    public int count() {
        // TODO Auto-generated method stub
        return 0;
    }

}
