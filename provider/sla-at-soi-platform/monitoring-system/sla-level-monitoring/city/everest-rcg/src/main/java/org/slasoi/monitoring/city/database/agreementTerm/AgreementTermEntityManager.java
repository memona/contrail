/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.database.agreementTerm;

/**
 * @author SLA@SOI (City)
 * 
 * @date 28 June 2010
 * 
 * Flags: SLASOI checkstyle: YES JavaDoc: YES
 */
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.database.EntityManagerInterface;
import uk.ac.city.soi.database.PersistentEntity;
import uk.ac.city.soi.everest.bse.Formula;

/**
 * This class provides methods for managing the AgreementTerm MySQL database table.
 * 
 */
public class AgreementTermEntityManager extends EntityManagerCommons implements EntityManagerInterface,
        AgreementTermEntityManagerInterface {
    // logger
    private static Logger logger = Logger.getLogger(AgreementTermEntityManager.class);

    /**
     * Constructor
     * 
     * @param connection
     */
    public AgreementTermEntityManager(Connection connection) {
        super(connection);
    }

    /**
     * @see org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManagerInterface#addAgreementTerm(java.lang.String,
     *      java.lang.String, java.lang.String, java.util.LinkedHashMap)
     */
    public void addAgreementTerm(String slaUuid, String agreementTermId, String agreementTermHashCode,
            LinkedHashMap<String, Formula> agreementTermTheoryObject) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        Blob agreementTermTheoryBlob = null;
        try {
            agreementTermTheoryBlob = toBlob(agreementTermTheoryObject);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            pstmt =
                    getConnection().prepareStatement(
                            "INSERT INTO agreement_term " + "(monitoring_configuration_sla_uuid, "
                                    + "agreement_term_id, " + "agreement_term_hash_code, "
                                    + "agreement_term_theory_object) " + "VALUES (?, ?, ?, ?)");

            pstmt.setString(1, slaUuid);
            pstmt.setString(2, agreementTermId);
            pstmt.setString(3, agreementTermHashCode);
            pstmt.setBlob(4, agreementTermTheoryBlob);

            pstmt.executeUpdate();
        }
        catch (SQLException ex) {
            // handle any errors
//            logger.debug("SQLException: " + ex.getMessage());
//            logger.debug("SQLState: " + ex.getSQLState());
//            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

    }

    /**
     * @see org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManagerInterface#getAgreementTermHashCode(java.lang.String,
     *      java.lang.String)
     */
    public String getAgreementTermHashCode(String slaUuid, String agreementTermId) {
    	
        String aTHashCode = null;
        /*
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt =
                    getConnection().prepareStatement(
                            "SELECT agreement_term_hash_code FROM agreement_term "
                                    + "WHERE monitoring_configuration_sla_uuid = ? AND agreement_term_id = ?");

            pstmt.setString(1, slaUuid);
            pstmt.setString(2, agreementTermId);

            resultSet = pstmt.executeQuery();

            if (resultSet.first()) {
                aTHashCode = resultSet.getString("agreement_term_hash_code");
            }

        }
        catch (SQLException ex) {
            // handle any errors
//            logger.debug("SQLException: " + ex.getMessage());
//            logger.debug("SQLState: " + ex.getSQLState());
//            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }
	*/
        return aTHashCode;
        
    }

    /**
     * @see org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManagerInterface#getAgreementsTermIds(java.lang.String)
     */
    public ArrayList<String> getAgreementsTermIds(String slaUuid) {
        ArrayList<String> aTIds = new ArrayList<String>();
        String aTId = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt =
                    getConnection().prepareStatement(
                            "SELECT agreement_term_id FROM agreement_term "
                                    + "WHERE monitoring_configuration_sla_uuid = ?");

            pstmt.setString(1, slaUuid);

            resultSet = pstmt.executeQuery();

            if (resultSet.first()) {
                do {
                    aTId = resultSet.getString("agreement_term_id");
                    aTIds.add(aTId);
                }
                while (resultSet.next());
            }

        }
        catch (SQLException ex) {
            // handle any errors
//            logger.debug("SQLException: " + ex.getMessage());
//            logger.debug("SQLState: " + ex.getSQLState());
//            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return aTIds;
    }

    /**
     * @see org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManagerInterface#getAgreementTermsHashCodesAndIds(java.lang.String)
     */
    public HashMap<String, String> getAgreementTermsHashCodesAndIds(String slaUuid) {

        HashMap<String, String> aTsHashCodesAndIds = new HashMap<String, String>();
        String aTHashCode = null;
        String aTId = null;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt =
                    getConnection().prepareStatement(
                            "SELECT agreement_term_hash_code, agreement_term_id FROM agreement_term "
                                    + "WHERE monitoring_configuration_sla_uuid = ?");

            pstmt.setString(1, slaUuid);

            resultSet = pstmt.executeQuery();

            if (resultSet.first()) {
                do {
                    aTHashCode = resultSet.getString("agreement_term_hash_code");
                    aTId = resultSet.getString("agreement_term_id");
                    aTsHashCodesAndIds.put(aTHashCode, aTId);
                }
                while (resultSet.next());
            }

        }
        catch (SQLException ex) {
            // handle any errors
//            logger.debug("SQLException: " + ex.getMessage());
//            logger.debug("SQLState: " + ex.getSQLState());
//            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return aTsHashCodesAndIds;
    }

    /**
     * @see org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManagerInterface#getAgreementTermTheory(java.lang.String,
     *      java.lang.String)
     */
    public LinkedHashMap<String, Formula> getAgreementTermTheory(String slaUuid, String agreementTermId) {
        LinkedHashMap<String, Formula> aTTheory = null;

        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt =
                    getConnection().prepareStatement(
                            "SELECT agreement_term_theory_object FROM agreement_term "
                                    + "WHERE monitoring_configuration_sla_uuid = ? AND agreement_term_id = ?");

            pstmt.setString(1, slaUuid);
            pstmt.setString(2, agreementTermId);

            resultSet = pstmt.executeQuery();
            //System.err.println("55555 Size of result set " + resultSet + " for id " + agreementTermId);
            if (resultSet.first()) {
                // get the blob
                Blob blob = resultSet.getBlob("agreement_term_theory_object");

                try {
                    LinkedHashMap<String, Formula> linkedHashMap = ((LinkedHashMap<String, Formula>) toObject(blob));
                    aTTheory = linkedHashMap;
                }
                catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }                                    
//                catch (IOException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//                catch (ClassNotFoundException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
            }

        }
        catch (SQLException ex) {
            // handle any errors
//            logger.debug("SQLException: " + ex.getMessage());
//            logger.debug("SQLState: " + ex.getSQLState());
//            logger.debug("VendorError: " + ex.getErrorCode());
            ex.printStackTrace();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return aTTheory;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
     */
    public ArrayList executeQuery(String query) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
     */
    public int delete(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
     */
    public int deleteByPrimaryKey(String arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
     */
    public int insert(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#select()
     */
    public ArrayList select() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
     */
    public PersistentEntity selectByPrimaryKey(String entityPrimaryKey) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
     */
    public int update(Object arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

}
