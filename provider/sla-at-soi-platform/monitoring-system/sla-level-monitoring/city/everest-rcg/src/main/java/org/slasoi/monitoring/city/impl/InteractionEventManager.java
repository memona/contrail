/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.impl;

/**
 * @author SLA@SOI (City)
 * 
 * @date 09 July 2010
 * 
 * Flags: SLASOI checkstyle: YES JavaDoc: YES
 */

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.common.eventschema.ArgumentList;
import org.slasoi.common.eventschema.ArgumentType;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.EventMetaDataType;
import org.slasoi.common.eventschema.SimpleArgument;
import org.slasoi.common.eventschema.impl.MonitoringEventServiceImpl;
import org.slasoi.slamodel.vocab.xsd;

import uk.ac.city.soi.everest.SRNTEvent.ArgumentsType;
import uk.ac.city.soi.everest.SRNTEvent.Context;
import uk.ac.city.soi.everest.SRNTEvent.EventType;
import uk.ac.city.soi.everest.SRNTEvent.MessageArgsList;
import uk.ac.city.soi.everest.SRNTEvent.OpMsg;
import uk.ac.city.soi.everest.SRNTEvent.TypeOfEvent;
import uk.ac.city.soi.everest.core.Constants;

/**
 * This class processes and translates SLA@SOI Interaction events
 * 
 */
public class InteractionEventManager {

    private static final Logger logger = Logger.getLogger(InteractionEventManager.class.getName());

    private MonitoringEventServiceImpl monitoringEventServiceImpl;

    /**
     * Constructor.
     */
    public InteractionEventManager() {
        this.monitoringEventServiceImpl = new MonitoringEventServiceImpl();
    }

    /**
     * This method translates an slasoiEvent (string) into the core CITY monitor event format (TypeOfEvent) (At the
     * current stage, this works only for simple parameters).
     * 
     * @param slasoiEvent -
     *            the event to be translated
     * @return
     */
    public TypeOfEvent toCoreMonitorFormatObject(String slasoiEvent) {

        logger.debug("slasoiEvent:"+slasoiEvent);
        
        //String operationId = "";
        // slasoi formatted event (EventInstance)
        EventInstance evInstance = null;
        try {
            evInstance = this.monitoringEventServiceImpl.unmarshall(slasoiEvent);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        logger.debug("evInstance:"+evInstance);
        /*
        List<EventMetaDataType> metaDatas = evInstance.getEventMetadata();
        for(EventMetaDataType metaData : metaDatas){
        	if(metaData.getKey().equals("callId")){
        		operationId = metaData.getValue();
        	}
        }
        */
        // core formatted event for translation (initialise all elements)
        TypeOfEvent coreInstance = new TypeOfEvent();
        // ACTUAL TRANSLATION !!!!!
        // -- EventID --
        coreInstance.setEventID(BigInteger.valueOf(evInstance.getEventID().getID()));
        // -- Event Type (Operation name and parameters)

        // Operation ID
        coreInstance.setType(new EventType());
        coreInstance.getType().setOperationMessage(new OpMsg());
        /*
        if(!operationId.equals(""))
        	coreInstance.getType().getOperationMessage().setOperationID(Long.parseLong(operationId));
        else
        */
        coreInstance.getType().getOperationMessage().setOperationID(
                evInstance.getEventPayload().getInteractionEvent().getOperationID());
        // (!!! Set equal to the Event ID !!!)
        // coreInstance.getType().getOperationMessage().setOperationID(eventId);

        // Operation Name
        
         coreInstance.getType().getOperationMessage().setOperationName(
         evInstance.getEventPayload().getInteractionEvent().getOperationName());
         
        // Basic Assumption: Operation name = service name
        // if req event, operation name = receiver name
        // else if res event, operation name = sender name
         /*
        if (evInstance.getEventPayload().getInteractionEvent().getStatus().contains("REQ")) {
            coreInstance.getType().getOperationMessage().setOperationName(
                    evInstance.getEventContext().getSource().getSwServiceLayerSource().getReceiver().getName());
        }
        else if (evInstance.getEventPayload().getInteractionEvent().getStatus().contains("RES")) {
            coreInstance.getType().getOperationMessage().setOperationName(
                    evInstance.getEventContext().getSource().getSwServiceLayerSource().getSender().getName());
        }
         */
        // Status
        coreInstance.getType().getOperationMessage().setStatus(
                evInstance.getEventPayload().getInteractionEvent().getStatus());
        // coreInstance.getType().getOperationMessage().setStatus("REQ-A");

        // -- Message Args List (sender, receiver, source, timestamp)

        // Sender
        coreInstance.setMessageArgs(new MessageArgsList());
        coreInstance.getMessageArgs().setSender(new uk.ac.city.soi.everest.SRNTEvent.Entity());
        coreInstance.getMessageArgs()
                .setSender(
                        translateEntityToCore((evInstance.getEventContext().getSource().getSwServiceLayerSource()
                                .getSender())));

        // Receiver
        coreInstance.getMessageArgs().setReceiver(new uk.ac.city.soi.everest.SRNTEvent.Entity());
        coreInstance.getMessageArgs()
                .setReceiver(
                        translateEntityToCore(evInstance.getEventContext().getSource().getSwServiceLayerSource()
                                .getReceiver()));

        // Source (Notifier in slasoi format)
        // [Notifier IS NOT an Entity in slasoi event schema]
        coreInstance.getMessageArgs().setContext(new Context());
        coreInstance.getMessageArgs().getContext().setEventSource(new uk.ac.city.soi.everest.SRNTEvent.Entity());
        coreInstance.getMessageArgs().getContext().getEventSource().setName(
                evInstance.getEventContext().getNotifier().getName());
        coreInstance.getMessageArgs().getContext().getEventSource().setPort(
                evInstance.getEventContext().getNotifier().getPort());
        coreInstance.getMessageArgs().getContext().getEventSource().setIpAddress(
                evInstance.getEventContext().getNotifier().getIP());

        // if it's a request, then use the info from receiver
        if (evInstance.getEventPayload().getInteractionEvent().getStatus().contains("REQ")) {
            coreInstance.getMessageArgs().getContext().getEventSource().setIpAddress(
                    evInstance.getEventContext().getSource().getSwServiceLayerSource().getReceiver().getName());
            
            //Following code is quick fix for B6 events. As operationID is no generated correctly
            /*
            coreInstance.getType().getOperationMessage().setOperationID(
            		Long.parseLong(evInstance.getEventContext().getSource().getSwServiceLayerSource().getSender().getProcessID()));
            */
            
        }
        // if its a response, then use info from sender
        if (evInstance.getEventPayload().getInteractionEvent().getStatus().contains("RES")) {
            coreInstance.getMessageArgs().getContext().getEventSource().setIpAddress(
                    evInstance.getEventContext().getSource().getSwServiceLayerSource().getSender().getName());
            
            //Following code is quick fix for B6 events. As operationID is no generated correctly
            /*
            coreInstance.getType().getOperationMessage().setOperationID(
            		Long.parseLong(evInstance.getEventContext().getSource().getSwServiceLayerSource().getReceiver().getProcessID()));
            */            
        }

        // **********************************************************************
        // Timestamp, collection time, report time
        coreInstance.getMessageArgs().getContext().setCollectionTime(
                evInstance.getEventContext().getTime().getCollectionTime());
        coreInstance.getMessageArgs().getContext()
                .setReportTime(evInstance.getEventContext().getTime().getReportTime());
        coreInstance.getMessageArgs().getContext().setTimestamp(evInstance.getEventContext().getTime().getTimestamp());
        coreInstance.getType().getOperationMessage().setOpArgs(new ArgumentsType());

        // **************************************************************************************************************
        // *********** UNCOMMENT to translate also Operation Parameters from
        // events (not reuqired in Y1 demonstrator)****

        //ArgumentList slasoiParams = evInstance.getEventPayload().getInteractionEvent().getParameters();

        // Parameters
        // !!! WORKS ONLY FOR SIMPLE PARAMETERS ArgumentsType
        ArgumentsType coreArgs = translateArgumentsToCore(evInstance, coreInstance.getMessageArgs().getContext().getEventSource().getIpAddress());
        
        coreInstance.getType().getOperationMessage().getOpArgs();
        coreInstance.getType().getOperationMessage().setOpArgs(coreArgs);

        // **************************************************************************************************************

        return coreInstance;
    }

    /**
     * This method translate an element Entity from the slasoi format into the CITY core monitor format.
     * 
     * @param entity
     * @return
     */
    private static uk.ac.city.soi.everest.SRNTEvent.Entity translateEntityToCore(
            org.slasoi.common.eventschema.Entity entity) {
        uk.ac.city.soi.everest.SRNTEvent.Entity coreEntity = new uk.ac.city.soi.everest.SRNTEvent.Entity();
        // Translation
        coreEntity.setName(entity.getName());
        coreEntity.setIpAddress(entity.getIp());
        coreEntity.setPort(entity.getPort());
        // ProcessID and UserID may not available, a check is first required
        if (coreEntity.getProcessID() != 0) {
            coreEntity.setProcessID(Long.valueOf(entity.getProcessID()));
        }
        if (coreEntity.getUserID() != 0) {
            coreEntity.setUserID(Long.valueOf(entity.getUserID()));
        }
        return coreEntity;
    }

    /**
     * Private method to translate operation parameters (arguments) from slasoi format to CITY Core monitor format.
     * 
     * @return
     */
    private static ArgumentsType translateArgumentsToCore(EventInstance slasoiInstance, String partnerName) {
        // List slasoiArgs

        // List<ArgumentType> slasoiArgs = slasoiArgList.getArgument();
        // Iterator<ArgumentType> slasoiIte = slasoiArgs.iterator();
        // core Args
        ArgumentsType coreArgs = new ArgumentsType();
        HashMap<String, String> dataTypeMap = new HashMap<String, String>();
        
        dataTypeMap.put(xsd.$integer, Constants.DATA_TYPE_INT);
        dataTypeMap.put(xsd.$long, Constants.DATA_TYPE_LONG);
        dataTypeMap.put(xsd.$double, Constants.DATA_TYPE_DOUBLE);
        dataTypeMap.put(xsd.$float, Constants.DATA_TYPE_DOUBLE);
        dataTypeMap.put(xsd.$string, Constants.DATA_TYPE_STRING);
        dataTypeMap.put(xsd.$dateTime, Constants.DATA_TYPE_STRING);
        dataTypeMap.put(xsd.$boolean, Constants.DATA_TYPE_BOOL);
        
        // List coreArgs
        String direction = "IN";

        // *****************************************************************************
        // The following loop translates into the coreFormat (Serenity) event
        // the parameters of
        // the operation call/response. For SLA@SOI Year 1 demonstrator, since
        // rules can be
        // computed only with the time stamps of events, we don't make such a
        // translation
        // (Therefore, the parameters are the only required "by default" by the
        // Serenity event)
        // This translation works only with Simple Parameters
        // ******************************************************************************
        /*
         * while(slasoiIte.hasNext()){ ArgumentType slasoiArg = slasoiIte.next(); code.SRNTEvent.ArgumentType coreArg =
         * new code.SRNTEvent.ArgumentType(); coreArg.setName( slasoiArg.getSimple().getArgName());
         * coreArg.setDirection( slasoiArg.getSimple().getDirection()); if (coreArg.getDirection().equals("OUT")){
         * direction = "OUT"; } coreArg.setType( slasoiArg.getSimple().getArgType()); coreArg.setValue(
         * slasoiArg.getSimple().getValue()); coreList.add(coreArg); coreArgs.getArgument().add(coreArg); }
         */
        // ********************************************************************************
        // Insert required correlation arguments
        // operationName: string
        /*
        uk.ac.city.soi.everest.SRNTEvent.ArgumentType coreArgOperationName =
                new uk.ac.city.soi.everest.SRNTEvent.ArgumentType();
        coreArgOperationName.setName("operationName");
        coreArgOperationName.setType("string");

        String operationName = slasoiInstance.getEventPayload().getInteractionEvent().getOperationName();
        long operationId = slasoiInstance.getEventPayload().getInteractionEvent().getOperationID();
        coreArgOperationName.setValue(operationName + operationId);
        coreArgOperationName.setDirection(direction);
        coreArgs.getArgument().add(coreArgOperationName);
        */
 
        
        // operation parameters
        ArgumentList argumentsList =
                (ArgumentList) slasoiInstance.getEventPayload().getInteractionEvent().getParameters();
        logger.debug("Events operation arguments list: " + argumentsList);
        if (argumentsList != null) {
            for (ArgumentType argument : slasoiInstance.getEventPayload().getInteractionEvent().getParameters()
                    .getArgument()) {
            	uk.ac.city.soi.everest.SRNTEvent.ArgumentType coreArg =
                  new uk.ac.city.soi.everest.SRNTEvent.ArgumentType();
            	SimpleArgument simple = null;
            	
            	if(argument.getSimple() != null) simple = argument.getSimple();
            	else if(argument.getStruct().getArgument() != null) simple = argument.getStruct().getArgument().get(0);
            	String argType = simple.getArgType();
            	if(dataTypeMap.containsKey(simple.getArgType())) argType = dataTypeMap.get(simple.getArgType()); 
                logger.debug("\t - argName: " + simple.getArgName());
                logger.debug("\t - argType: " + argType);
                logger.debug("\t - argValue: " + simple.getValue());
                
                if (simple.getDirection().equals("OUT")) direction = "OUT"; 
                
                coreArg.setName(simple.getArgName());
                coreArg.setType(argType);
                coreArg.setValue(simple.getValue());
                coreArg.setDirection(simple.getDirection());
                
                coreArgs.getArgument().add(coreArg);
            }
        }

        uk.ac.city.soi.everest.SRNTEvent.ArgumentType cArg = new uk.ac.city.soi.everest.SRNTEvent.ArgumentType();
        
	    cArg.setName("serviceId");
	    cArg.setType(Constants.DATA_TYPE_STRING);
	    cArg.setValue(partnerName);
	    cArg.setDirection(direction);
        
	    coreArgs.getArgument().add(0, cArg);
        /*
        // senderID: string
        uk.ac.city.soi.everest.SRNTEvent.ArgumentType coreArgSenderID =
                new uk.ac.city.soi.everest.SRNTEvent.ArgumentType();
        coreArgSenderID.setName("senderID");
        coreArgSenderID.setType("string");

        String senderID = slasoiInstance.getEventContext().getSource().getSwServiceLayerSource().getSender().getName();
        coreArgSenderID.setValue(senderID);
        coreArgSenderID.setDirection(direction);
        coreArgs.getArgument().add(coreArgSenderID);

        // receiverID: string
        uk.ac.city.soi.everest.SRNTEvent.ArgumentType coreArgRecID =
                new uk.ac.city.soi.everest.SRNTEvent.ArgumentType();
        coreArgRecID.setName("receiverID");
        coreArgRecID.setType("string");
        coreArgRecID.setValue(slasoiInstance.getEventContext().getSource().getSwServiceLayerSource().getReceiver()
                .getName());
        coreArgRecID.setDirection(direction);
        coreArgs.getArgument().add(coreArgRecID);

        // evSourceID: string
        // evSource.value = notifier.name+notifier.IP+notifier.port
        uk.ac.city.soi.everest.SRNTEvent.ArgumentType coreArgSourceID =
                new uk.ac.city.soi.everest.SRNTEvent.ArgumentType();
        coreArgSourceID.setName("sourceID");
        coreArgSourceID.setType("string");
        String sourceName = slasoiInstance.getEventContext().getNotifier().getName();
        String sourceIP = slasoiInstance.getEventContext().getNotifier().getIP();
        long sourcePort = slasoiInstance.getEventContext().getNotifier().getPort();
        coreArgSourceID.setValue(sourceName + sourceIP + sourcePort);
        coreArgSourceID.setDirection(direction);
        coreArgs.getArgument().add(coreArgSourceID);
		*/
        return coreArgs;
    }

}
