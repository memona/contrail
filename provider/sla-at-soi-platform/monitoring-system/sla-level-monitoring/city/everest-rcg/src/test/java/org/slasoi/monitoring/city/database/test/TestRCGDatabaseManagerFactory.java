/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Theocharis Tsigkritis - t7t@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.city.database.test;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slasoi.monitoring.city.database.RCGDatabaseManagerFactory;
import org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManager;
import org.slasoi.monitoring.city.database.agreementTerm.AgreementTermEntityManagerInterface;

import uk.ac.city.soi.database.DatabaseManagerInterface;
import uk.ac.city.soi.database.EntityManagerFactoryInterface;
import uk.ac.city.soi.database.EntityManagerInterface;

/**
 * @author Davide Lorenzoli
 * 
 * @date 21 May 2010
 */
public class TestRCGDatabaseManagerFactory extends TestCase {
	/*
	Logger logger = Logger.getLogger(TestRCGDatabaseManagerFactory.class);
	
	protected void setUp() throws Exception {
		PropertyConfigurator.configure("./conf/log4j.properties");
	}

	protected void tearDown() throws Exception {
	}

	public void testDatabase() {
    	String slasoiOrcHome = System.getenv().get("SLASOI_HOME");
    	
    	assertNotNull("SLASOI_HOME must not be null", slasoiOrcHome);
    	
    	String propertiesFile = slasoiOrcHome + "/monitoring-system/sla-level-monitoring/city/rcg.database.properties";
    	
		DatabaseManagerInterface databaseManager = RCGDatabaseManagerFactory.getDatabaseManager(propertiesFile);
		
		EntityManagerFactoryInterface entityManagerFactory = databaseManager.getEntityManagerFactory();
		
		// test connection
		assertNotNull(databaseManager.getConnectionManager().getConnection());
		
		// test class loading
		AgreementTermEntityManagerInterface entityManager = (AgreementTermEntityManager) entityManagerFactory.getEntityManager(AgreementTermEntityManager.class); 
		assertNotNull(entityManager);
		assertEquals(AgreementTermEntityManager.class.getName(), entityManager.getClass().getName());
	}
	*/
}
