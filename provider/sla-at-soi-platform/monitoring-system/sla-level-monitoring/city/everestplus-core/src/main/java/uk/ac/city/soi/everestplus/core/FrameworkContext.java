/**
 * 
 */
package uk.ac.city.soi.everestplus.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.log4j.Logger;

import uk.ac.city.soi.database.DatabaseManagerFactory;
import uk.ac.city.soi.database.DatabaseManagerInterface;
import uk.ac.city.soi.database.EntityManagerFactoryInterface;
import uk.ac.city.soi.database.EntityManagerInterface;
import uk.ac.city.soi.everestplus.database.DistributionEntityManager;
import uk.ac.city.soi.everestplus.database.EventEntityManager;
import uk.ac.city.soi.everestplus.database.InferredDistributionEntityManager;
import uk.ac.city.soi.everestplus.database.ModelFactoryEntityManager;
import uk.ac.city.soi.everestplus.database.PredictionEntityManager;
import uk.ac.city.soi.everestplus.database.PredictionPolicyEntityManager;
import uk.ac.city.soi.everestplus.database.QoSEntityManager;
import uk.ac.city.soi.everestplus.database.StatisticsEntityManager;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 31, 2011
 */
public class FrameworkContext {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	private static final String QOS_ENTITY_MANAGER_PROPERTY = "everestplus.store.class.qos";
	
	/** enumeration containing names for retrieving framework properties **/
	public enum PropertiesFileNames {
		FRAMEWORK { public String toString() { return "framework.properties"; } },
		
		MYSQL { public String toString() { return "DB" + FILE_SEPARATOR + "db.properties"; } },
		
		XMPP { public String toString() { return "xmpp.properties"; } }
	};
	
	/** enumeration containing names of the database manager classes **/
	public enum DatabaseManagers {
		DISTRIBUTIONS { public String toString() { return DistributionEntityManager.class.getName(); } },
		
		EVENTS { public String toString() { return EventEntityManager.class.getName(); } },
		
		INFERRED_DISTRIBUTIONS { public String toString() { return InferredDistributionEntityManager.class.getName(); } },
		
		MODEL_FACTORIES { public String toString() { return ModelFactoryEntityManager.class.getName(); } },
		
		PREDICTIONS { public String toString() { return PredictionEntityManager.class.getName(); } },
		
		QOSS { public String toString() { return QoSEntityManager.class.getName(); } },
		
		PREDICTION_POLICY { public String toString() { return PredictionPolicyEntityManager.class.getName(); } },
		
		STATISTICS { public String toString() { return StatisticsEntityManager.class.getName(); } }
	};
	
	private Hashtable<PropertiesFileNames, Properties> frameworkProperties;
	private EntityManagerFactoryInterface entityManagerFactory;
	private Hashtable<DatabaseManagers, EntityManagerInterface> databaseManagers;
	private String frameworkConfigurationfolder;
	
	private static final String FILE_SEPARATOR = System.getProperty("file.separator");
	
	// ------------------------------------------------------------------------
	// 								CONSTRUCTORS
	// ------------------------------------------------------------------------
	
	/**
	 * @param frameworkConfigurationfolder The path to the folder DOES NOT terminates
	 * 									   with the 'file.separator' symbol, e.g., '/'
	 */
	public FrameworkContext(String frameworkConfigurationfolder) throws FrameworkContextInitialisationException {
		this.frameworkConfigurationfolder = frameworkConfigurationfolder;
		
		if (frameworkProperties == null) {
			this.frameworkProperties = loadFrameworkProperties(frameworkConfigurationfolder);
		}
		
		if (entityManagerFactory == null) {
			// initiate entity manager factory
			DatabaseManagerInterface databaseManager = DatabaseManagerFactory.getDatabaseManager(frameworkConfigurationfolder + FILE_SEPARATOR + PropertiesFileNames.MYSQL.toString());
			
			// database manager can be null if any error occurred to connect
			// to the database
			if (databaseManager != null) {
				entityManagerFactory = databaseManager.getEntityManagerFactory();
			} else {
				throw new FrameworkContextInitialisationException("Errors initialising database entity manager factory");
			}
		}

		if (databaseManagers == null) {
			this.databaseManagers = loadDatabaseManagers(frameworkConfigurationfolder);
			
			// if a user-specified QoS entity manager exists then the default
			// one is overridden
			String qosEntityManagerProperty = frameworkProperties.get(PropertiesFileNames.FRAMEWORK).getProperty(QOS_ENTITY_MANAGER_PROPERTY);
			if (qosEntityManagerProperty != null) {
				// create an instance of user-specified QoS entity manager
				// remove existing QoS entity manage
				databaseManagers.remove(DatabaseManagers.QOSS);
				// add user-specified QoS entity manager
				databaseManagers.put(DatabaseManagers.QOSS, loadDatabaseManager(qosEntityManagerProperty));
				
				logger.debug("Replaced default QoS entity manager with used-specified: " + databaseManagers.get(DatabaseManagers.QOSS).getClass().getName());
			}
		}
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @return the frameworkConfigurationfolder
	 */
	public String getFrameworkConfigurationfolder() {
		return frameworkConfigurationfolder;
	}
	
	/**
	 * @return the entityManagerFactory
	 */
	public EntityManagerFactoryInterface getEntityManagerFactory() {
		return entityManagerFactory;
	}
	
	/**
	 * @param propertiesFileName
	 * @return <code>null</code> if the property doesn't exist or the
	 *         FrameworkContext was not initialised
	 */
	public Properties getFrameworkProperties(PropertiesFileNames propertiesFileName) {
		return frameworkProperties == null ? null: frameworkProperties.get(propertiesFileName);
	}
	
	/**
	 * @param databaseManagerName
	 * @return <code>null</code> if the database manager doesn't exist or the
	 *         FrameworkContext was not initialised
	 */
	public EntityManagerInterface getDatabaseManager(DatabaseManagers databaseManagerName) {
		return databaseManagers == null ? null : databaseManagers.get(databaseManagerName);
	}

	/**
	 * @param databaseManagerName
	 * @param newInstance if <code>true</code> creates a new instance
	 * @return <code>null</code> if the database manager doesn't exist or the
	 *         FrameworkContext was not initialised
	 */
	public EntityManagerInterface getDatabaseManager(DatabaseManagers databaseManagerName, boolean newInstance) {
		return newInstance ? loadDatabaseManager(databaseManagerName) : getDatabaseManager(databaseManagerName);
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @param frameworkConfigurationFolder
	 * @return
	 */
	private Hashtable<PropertiesFileNames, Properties> loadFrameworkProperties(String frameworkConfigurationFolder) {
		Hashtable<PropertiesFileNames, Properties> properties = new Hashtable<PropertiesFileNames, Properties>();
		
		
		for (PropertiesFileNames propertiesFileName : PropertiesFileNames.values()) {
			String propertiesFile = frameworkConfigurationFolder + FILE_SEPARATOR + propertiesFileName.toString();
			
			logger.debug("Loading properties: " + propertiesFileName.toString());
			
			Properties currentProperties = new Properties();
			try {
				// load properties
				currentProperties.load(new FileInputStream(propertiesFile));
				// add properties to properties hashtable
				properties.put(propertiesFileName, currentProperties);
			} catch (FileNotFoundException e) {
				logger.error(e.getMessage(), e);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		return properties;
	}
	
	/**
	 * @param frameworkConfigurationFolder
	 * @return
	 */
	private Hashtable<DatabaseManagers, EntityManagerInterface> loadDatabaseManagers(String frameworkConfigurationFolder) {
		Hashtable<DatabaseManagers, EntityManagerInterface> databaseManagers = new Hashtable<FrameworkContext.DatabaseManagers, EntityManagerInterface>();
		
		for (DatabaseManagers databaseManager : DatabaseManagers.values()) {
			EntityManagerInterface entityManager = loadDatabaseManager(databaseManager);
			
			if (entityManager != null) {
				// save instance
				logger.debug("Loaded Database Entity Manager: " + entityManager.getClass().getName());
				databaseManagers.put(databaseManager, entityManager);
			}
		}
		
		return databaseManagers;
	}
	
	/**
	 * @param databaseManager
	 * @return
	 */
	private EntityManagerInterface loadDatabaseManager(DatabaseManagers databaseManager) {
		return loadDatabaseManager(databaseManager.toString());
	}
	
	/**
	 * @param databaseManager
	 * @return
	 */
	private EntityManagerInterface loadDatabaseManager(String databaseManager) {
		EntityManagerInterface entityManager = null;
		
		try {
			// load class
			Class databaseManagerClass = getClass().getClassLoader().loadClass(databaseManager);
			// create instance
			entityManager = entityManagerFactory.getEntityManager(databaseManagerClass);
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
		}
		
		return entityManager;	
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
