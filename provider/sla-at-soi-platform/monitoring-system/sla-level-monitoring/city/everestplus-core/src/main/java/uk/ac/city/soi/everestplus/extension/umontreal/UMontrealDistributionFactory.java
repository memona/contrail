/**
 * 
 */
package uk.ac.city.soi.everestplus.extension.umontreal;

import org.apache.log4j.Logger;

import umontreal.iro.lecuyer.probdist.AndersonDarlingDist;
import umontreal.iro.lecuyer.probdist.BetaDist;
import umontreal.iro.lecuyer.probdist.CauchyDist;
import umontreal.iro.lecuyer.probdist.ChiDist;
import umontreal.iro.lecuyer.probdist.ChiSquareDist;
import umontreal.iro.lecuyer.probdist.ChiSquareNoncentralDist;
import umontreal.iro.lecuyer.probdist.ContinuousDistribution;
import umontreal.iro.lecuyer.probdist.CramerVonMisesDist;
import umontreal.iro.lecuyer.probdist.DistributionFactory;
import umontreal.iro.lecuyer.probdist.ExponentialDist;
import umontreal.iro.lecuyer.probdist.ExtremeValueDist;
import umontreal.iro.lecuyer.probdist.FatigueLifeDist;
import umontreal.iro.lecuyer.probdist.FisherFDist;
import umontreal.iro.lecuyer.probdist.FoldedNormalDist;
import umontreal.iro.lecuyer.probdist.FrechetDist;
import umontreal.iro.lecuyer.probdist.GammaDist;
import umontreal.iro.lecuyer.probdist.GumbelDist;
import umontreal.iro.lecuyer.probdist.HalfNormalDist;
import umontreal.iro.lecuyer.probdist.HyperbolicSecantDist;
import umontreal.iro.lecuyer.probdist.InverseDistFromDensity;
import umontreal.iro.lecuyer.probdist.InverseGaussianDist;
import umontreal.iro.lecuyer.probdist.JohnsonSBDist;
import umontreal.iro.lecuyer.probdist.JohnsonSUDist;
import umontreal.iro.lecuyer.probdist.KolmogorovSmirnovDist;
import umontreal.iro.lecuyer.probdist.KolmogorovSmirnovPlusDist;
import umontreal.iro.lecuyer.probdist.LaplaceDist;
import umontreal.iro.lecuyer.probdist.LogisticDist;
import umontreal.iro.lecuyer.probdist.LoglogisticDist;
import umontreal.iro.lecuyer.probdist.LognormalDist;
import umontreal.iro.lecuyer.probdist.NakagamiDist;
import umontreal.iro.lecuyer.probdist.NormalDist;
import umontreal.iro.lecuyer.probdist.NormalInverseGaussianDist;
import umontreal.iro.lecuyer.probdist.ParetoDist;
import umontreal.iro.lecuyer.probdist.Pearson5Dist;
import umontreal.iro.lecuyer.probdist.Pearson6Dist;
import umontreal.iro.lecuyer.probdist.PiecewiseLinearEmpiricalDist;
import umontreal.iro.lecuyer.probdist.PowerDist;
import umontreal.iro.lecuyer.probdist.RayleighDist;
import umontreal.iro.lecuyer.probdist.StudentDist;
import umontreal.iro.lecuyer.probdist.TriangularDist;
import umontreal.iro.lecuyer.probdist.TruncatedDist;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.probdist.WatsonGDist;
import umontreal.iro.lecuyer.probdist.WatsonUDist;
import umontreal.iro.lecuyer.probdist.WeibullDist;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 29, 2010
 */
public class UMontrealDistributionFactory implements uk.ac.city.soi.everestplus.model.distribution.DistributionFactory<UMontrealDistribution> {
	// logger
	private static Logger logger = Logger.getLogger(UMontrealDistributionFactory.class);
	
	/**
	 * 
	 */
	public UMontrealDistributionFactory() {
	}

	public static Class[] getSupportedDistributions() {
		Class[] distributions = new Class[] {
				AndersonDarlingDist.class, BetaDist.class, CauchyDist.class,
				ChiDist.class, ChiSquareDist.class, ChiSquareNoncentralDist.class,
				CramerVonMisesDist.class, ExponentialDist.class, ExtremeValueDist.class,
				FatigueLifeDist.class, FisherFDist.class, FoldedNormalDist.class,
				FrechetDist.class, GammaDist.class, GumbelDist.class, HalfNormalDist.class,
				HyperbolicSecantDist.class, InverseDistFromDensity.class,
				InverseGaussianDist.class, JohnsonSBDist.class, JohnsonSUDist.class,
				KolmogorovSmirnovDist.class, KolmogorovSmirnovPlusDist.class, LaplaceDist.class,
				LogisticDist.class, LoglogisticDist.class, LognormalDist.class,
				NakagamiDist.class, NormalDist.class, NormalInverseGaussianDist.class,
				ParetoDist.class, Pearson5Dist.class, Pearson6Dist.class,
				PiecewiseLinearEmpiricalDist.class, PowerDist.class, RayleighDist.class,
				StudentDist.class, TriangularDist.class, TruncatedDist.class, UniformDist.class,
				WatsonGDist.class, WatsonUDist.class, WeibullDist.class
				};
		
		return distributions;		
	}
	
	/**
	 * @see uk.ac.city.soi.everestplus.model.ModelFactory#isSupported(java.lang.Class)
	 */
	public boolean isSupported(Class modelClass) {
		return modelClass.getSuperclass().equals(ContinuousDistribution.class);
	}
	
	/**
	 * @see uk.ac.city.soi.everestplus.model.distribution.DistributionFactory#createDistribution(java.lang.Class, double[])
	 */
	public UMontrealDistribution createDistribution(Class distributionClass, double[] parameters) {
		return createDistribution(distributionClass.getName(), parameters);
	}
	
	/**
	 * @see uk.ac.city.soi.everestplus.model.distribution.DistributionFactory#createDistribution(java.lang.String, double[])
	 */
	public UMontrealDistribution createDistribution(String className, double[] parameters) {		
		// construct the parameter string
		String parametersString = new String();
		for (int i=0; i<parameters.length; i++) {
			parametersString += Double.toString(parameters[i]);
			
			// if the current parameter isn't the last one add a comma
			if (i<(parameters.length-1)) {
				parametersString += ", ";
			}
		}
		
		// create the executable string
		String executableString = className + "(" + parametersString + ")";
		
		// create a distribution object
		UMontrealDistribution distribution = null;
		
		try {
			ContinuousDistribution continuousDistribution = DistributionFactory.getContinuousDistribution (executableString);
			distribution = new UMontrealDistribution(
					continuousDistribution, // distribution instance
					continuousDistribution.getClass().getName() // distribution id
					);
			
			logger.debug("Executabe string: " + executableString);
			logger.debug("Distribution instance: " + continuousDistribution);
		}
		// If it fails it means the distribution wants as input an array
		// of integers instead of an array of doubles.
		catch (Exception e) {
			try {
				int[] intParameters = new int[parameters.length];
				for (int i=0; i<intParameters.length; i++) {
					intParameters[i] = (int) parameters[i];
				}
				
				// construct the parameter string
				parametersString = new String();
				for (int i=0; i<parameters.length; i++) {
					parametersString += Integer.toString(intParameters[i]);
					
					// if the current parameter isn't the last one add a comma
					if (i<(parameters.length-1)) {
						parametersString += ", ";
					}
				}
				
				// create the executable string
				executableString = className + "(" + parametersString + ")";
				
				//logger.info("executableString:" + executableString);
				
				ContinuousDistribution continuousDistribution = DistributionFactory.getContinuousDistribution (executableString); 
				distribution = new UMontrealDistribution(
						continuousDistribution, // distribution instance
						continuousDistribution.getClass().getName() // distribution id
						);
				
				logger.debug("Executabe string: " + executableString);
				logger.debug("Distribution instance: " + continuousDistribution);
			} catch (Exception e2) {
				logger.error("Cannot create " + executableString + " instance: " + e.getMessage());
			}
		}
		
		return distribution;
	}

}
