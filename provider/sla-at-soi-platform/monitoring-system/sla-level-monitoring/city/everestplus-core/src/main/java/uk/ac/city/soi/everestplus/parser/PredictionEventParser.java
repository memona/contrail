/**
 * 
 */
package uk.ac.city.soi.everestplus.parser;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.log4j.Logger;
import org.slasoi.common.eventschema.AgreementTermType;
import org.slasoi.common.eventschema.AssessmentResultType;
import org.slasoi.common.eventschema.Entity;
import org.slasoi.common.eventschema.EventContextType;
import org.slasoi.common.eventschema.EventIdType;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.EventNotifier;
import org.slasoi.common.eventschema.EventPayloadType;
import org.slasoi.common.eventschema.EventSource;
import org.slasoi.common.eventschema.EventTime;
import org.slasoi.common.eventschema.GuatanteedStateType;
import org.slasoi.common.eventschema.PredictionInfoType;
import org.slasoi.common.eventschema.PredictionResultEventType;
import org.slasoi.common.eventschema.PropertiesType;
import org.slasoi.common.eventschema.SLAType;
import org.slasoi.common.eventschema.ServiceSourceType;

import uk.ac.city.soi.everestplus.core.Prediction;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jul 1, 2011
 */
public class PredictionEventParser {
	// logger
	private static Logger logger = Logger.getLogger(PredictionEventParser.class);
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	public static EventInstance getPredictionEvent(Prediction prediction) throws DatatypeConfigurationException {
   	EventInstance event = new EventInstance();
    	
    	// event context
    	EventContextType eventContext = new EventContextType();
    	
    	// event id
    	EventIdType eventId = new EventIdType();
    	eventId.setEventTypeID("PredictionResultEvent");
    	eventId.setID(System.currentTimeMillis());
    	
    	event.setEventID(eventId);
    	
    	// event time
    	EventTime eventTime = new EventTime();
    	
    	GregorianCalendar collectionTime = (GregorianCalendar) GregorianCalendar.getInstance();
    	collectionTime.setTimeInMillis(prediction.getTimestamp());
    	
    	GregorianCalendar reportTime = (GregorianCalendar) GregorianCalendar.getInstance();
    	reportTime.setTimeInMillis(prediction.getTimestamp());
    	
    	eventTime.setTimestamp(prediction.getTimestamp());
    	eventTime.setCollectionTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(collectionTime));
    	eventTime.setReportTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(reportTime));
    	
    	eventContext.setTime(eventTime);
    	
    	// notifier
    	EventNotifier notifier = new EventNotifier();
    	notifier.setIP(getIPAddress());
    	notifier.setName(getHostName());
    	notifier.setPort(0);
    	
    	eventContext.setNotifier(notifier);
    	
    	// source
    	EventSource source = new EventSource();
    	ServiceSourceType serviceSource = new ServiceSourceType();
    	
    	// sender
    	Entity sender = new Entity();
    	sender.setIp(getIPAddress());
    	sender.setName(getHostName());
    	sender.setPort(0);
    	sender.setServiceEPR("ServiceEpr");
    	
    	serviceSource.setSender(sender);
    	
    	// receiver
    	Entity receiver = new Entity();
    	receiver.setIp(getIPAddress());
    	receiver.setName(getHostName());
    	receiver.setPort(0);
    	receiver.setServiceEPR("ServiceEpr");
    	
    	serviceSource.setReceiver(receiver);
    	
    	source.setSwServiceLayerSource(serviceSource);
    	
    	eventContext.setSource(source);
    	
    	event.setEventContext(eventContext);
    	
    	// event payload
    	EventPayloadType eventPayload = new EventPayloadType();
    	
    	PredictionResultEventType predictionResultEventType = new PredictionResultEventType();
    	
    	predictionResultEventType.setExtraProperties(new PropertiesType());
    	
    	// prediction info
    	PredictionInfoType predictionInfo = new PredictionInfoType();
    	
    	predictionInfo.setStartingTime(prediction.getTimestamp());
    	predictionInfo.setEnddingTime(prediction.getTimestamp()+prediction.getPredictionSpecification().getPredictionSettings().getPredictionHorizon());
    	predictionInfo.setProbabilityOfViolation(prediction.getValue());
    	predictionInfo.setNotificationThreshold(-1);
    	
    	predictionResultEventType.setPredictionInfo(predictionInfo);
    	
    	// Type
    	predictionResultEventType.setType("warning");
    	
    	// SLA info
    	SLAType sla = new SLAType();
    	
    	sla.setSlaURI("Unknown");
    	sla.setSlaUUID(prediction.getPredictionSpecification().getPredictionTarget().getVariable().getSlaId());
    	sla.setAssessmentResult(AssessmentResultType.NOT_ASSESSED);
    	
    	AgreementTermType agreementTerm = new AgreementTermType();
    	agreementTerm.setAgreementTermID(prediction.getPredictionSpecification().getPredictionTarget().getVariable().getAgreementTermId());
    	agreementTerm.setAssessmentResult(AssessmentResultType.NOT_ASSESSED);
    	
    	GuatanteedStateType guaranteedState = new GuatanteedStateType();
    	guaranteedState.setGuaranteedID(prediction.getPredictionSpecification().getPredictionTarget().getVariable().getGuaranteedId());
    	guaranteedState.setAssessmentResult(AssessmentResultType.NOT_ASSESSED);
    	guaranteedState.setQoSName(prediction.getPredictionSpecification().getPredictionTarget().getVariable().getQoSId());
    	guaranteedState.setQoSValue("Unknown");
    	
    	agreementTerm.getGuaranteedStateOrGuaranteedAction().add(guaranteedState);

    	sla.getAgreementTerm().add(agreementTerm);
    	
    	predictionResultEventType.setSLAInfo(sla);
    	
    	eventPayload.setPredictionResultEvent(predictionResultEventType);
    	
    	event.setEventPayload(eventPayload);
    	
    	return event;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

    /**
     * @return The IP address of the machine running EverestPlus
     */
    private static String getIPAddress() {
    	String ipAddress = "127.0.0.1";
    	
    	try {
			InetAddress inetAddress = InetAddress.getLocalHost();
			
			ipAddress = inetAddress.getHostAddress();
		} catch (UnknownHostException e) {
			logger.error(e.getMessage(), e);
		}
		
		return ipAddress;
    	
    }
    
    /**
     * @return The host name of the machine running EverestPlus
     */
    private static String getHostName() {
    	String hostName = "localhost";
    	
    	try {
			InetAddress inetAddress = InetAddress.getLocalHost();
			
			hostName = inetAddress.getHostAddress();
		} catch (UnknownHostException e) {
			logger.error(e.getMessage(), e);
		}
		
		return hostName;
    	
    }
    
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
