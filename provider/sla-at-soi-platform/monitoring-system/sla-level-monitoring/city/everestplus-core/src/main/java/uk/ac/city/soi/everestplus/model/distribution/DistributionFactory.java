/**
 * 
 */
package uk.ac.city.soi.everestplus.model.distribution;

import uk.ac.city.soi.everestplus.model.ModelFactory;

/**
 * @author Davide Lorenzoli
 * @param <E>
 * 
 * @date Jun 29, 2010
 */
public interface DistributionFactory<E> extends ModelFactory<E> {

	/**
	 * Create an instance of a given distribution initialised with the
	 * given parameters
	 * 
	 * @param distributionId The distribution to create an instance of 
	 * @param parameters The parameters to initialise the distribution with 
	 * @return The distribution instance, <code>null</code> if a new instance
	 *         cannot be created 
	 */
	public E createDistribution(String distributionId, double parameters[]);
	
	/**
	 * Create an instance of a given distribution initialised with the
	 * given parameters
	 * 
	 * @param distributionClass The distribution to create an instance of 
	 * @param parameters The parameters to initialise the distribution with 
	 * @return The distribution instance, <code>null</code> if a new instance
	 *         cannot be created 
	 */
	public E createDistribution(Class distributionClass, double parameters[]);
}
