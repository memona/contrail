/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import java.sql.Connection;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.slasoi.monitoring.city.database.qos.QoS;
import org.slasoi.monitoring.city.database.qos.QoSEntityManager;

import uk.ac.city.soi.database.EntityManagerCommons;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 11, 2011
 */
public class QoSEverestRCGEntityManager extends EntityManagerCommons implements QoSEntityManagerInterface {
	// logger
	private static Logger logger = Logger.getLogger(QoSEverestRCGEntityManager.class);
	
	private static final String DATABASE_TABLE_NAME = "qos"; 
	
	// Everest RCG QoS entity manager
	private QoSEntityManager everestRCGQoSEM;
	
	/**
	 * @param connection
	 */
	public QoSEverestRCGEntityManager(Connection connection) {
		this(connection, DATABASE_TABLE_NAME);
	}
	
	/**
	 * @param connection
	 * @param databaseTable
	 */
	public QoSEverestRCGEntityManager(Connection connection, String databaseTable) {
		super(connection, databaseTable);
		everestRCGQoSEM = new QoSEntityManager(connection);
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	public int deleteByPrimaryKey(String paramString) {
		return everestRCGQoSEM.deleteByPrimaryKey(paramString);
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	public Double selectByPrimaryKey(String paramString) {
		QoS qos = everestRCGQoSEM.selectByPrimaryKey(paramString);
		
		return qos == null ? null : (Double) qos.getValue();
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	public ArrayList<Double> select() {
		ArrayList<Double> qoss = new ArrayList<Double>();
		
		for (QoS qos : everestRCGQoSEM.select()) {
			qoss.add((Double) qos.getValue());
		}
		
		return qoss;
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
	 */
	public ArrayList<Double> executeQuery(String paramString) {
		ArrayList<Double> qoss = new ArrayList<Double>();
		
		for (QoS qos : everestRCGQoSEM.executeQuery(paramString)) {
			qoss.add((Double) qos.getValue());
		}
		
		return qoss;
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#selectLastByTimestamp(java.lang.String, java.lang.String, java.lang.String, long)
	 */
	public Double selectLastByTimestamp(String slaId, String guaranteedId, String qosId, long timestamp) {
		QoS qos = everestRCGQoSEM.selectLastByTimestamp(slaId, guaranteedId, qosId, timestamp);
		
		return qos == null ? null : (Double) qos.getValue();
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#selectLastN(java.lang.String, java.lang.String, java.lang.String, long)
	 */
	public ArrayList<Double> selectLastN(String slaId, String guaranteedId, String qosId, long numberOfQoS) {
		ArrayList<Double> qoss = new ArrayList<Double>();
		
		for (QoS qos : everestRCGQoSEM.selectLastN(slaId, guaranteedId, qosId, numberOfQoS)) {
			qoss.add((Double) qos.getValue());
		}
		
		return qoss;
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#selectLast(java.lang.String, java.lang.String, java.lang.String)
	 */
	public Double selectLast(String slaId, String guaranteedId, String qosId) {
		QoS qos = everestRCGQoSEM.selectLast(slaId, guaranteedId, qosId);
		
		return qos == null ? null : (Double) qos.getValue();
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#selectByName(java.lang.String, java.lang.String, java.lang.String, long, int)
	 */
	public ArrayList<Double> selectByName(String slaId, String guaranteedId, String qosId, long timestamp, int limit) {
		ArrayList<Double> qoss = new ArrayList<Double>();
		
		for (QoS qos : everestRCGQoSEM.selectByName(slaId, guaranteedId, qosId, timestamp, limit)) {
			qoss.add((Double) qos.getValue());
		}
		
		return qoss;
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#count(java.lang.String, java.lang.String, java.lang.String, long)
	 */
	public int count(String slaId, String guaranteedId, String qosId, long timestamp) {
		return everestRCGQoSEM.count(slaId, guaranteedId, qosId, timestamp);
	}

	/**
	 * @see uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface#count(java.lang.String, java.lang.String, java.lang.String)
	 */
	public int count(String slaId, String guaranteedId, String qosId) {
		return everestRCGQoSEM.count(slaId, guaranteedId, qosId);
	}
	
	// ------------------------------------------------------------------------
	// 								NOT IMPLEMENTED METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
	 */
	public int insert(Double qos) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	public int update(Double qos) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	public int delete(Double qos) {
		throw new UnsupportedOperationException("TO DO");
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								INNER CLASSES
	// ------------------------------------------------------------------------
}
