/**
 * 
 */
package uk.ac.city.soi.everestplus.util;

/**
 * @author Davide Lorenzoli
 * 
 * @date 3 Mar 2010
 */
public class Random extends java.util.Random {
	
	/**
	 * @param mean
	 * @param standardDeviation
	 * @return
	 */
	public synchronized double nextGaussian(double mean, double standardDeviation) {
		double n = super.nextGaussian();
		
		n = (n*standardDeviation)+mean;
		
		return n; 
	}
	
	public static void main(String[] args) {
		Random random = new Random();
		
		for (int i=0; i<200; i++) {
			System.out.println(random.nextGaussian(400, 20));
		}
	}
}
