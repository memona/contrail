/**
 * 
 */
package uk.ac.city.soi.everestplus.util;

import uk.ac.city.soi.database.DatabaseManagerFactory;
import uk.ac.city.soi.database.DatabaseManagerInterface;
import uk.ac.city.soi.database.EntityManagerFactoryInterface;
import uk.ac.city.soi.everestplus.database.DistributionEntityManager;
import umontreal.iro.lecuyer.probdist.AndersonDarlingDist;
import umontreal.iro.lecuyer.probdist.BetaDist;
import umontreal.iro.lecuyer.probdist.CauchyDist;
import umontreal.iro.lecuyer.probdist.ChiDist;
import umontreal.iro.lecuyer.probdist.ChiSquareDist;
import umontreal.iro.lecuyer.probdist.ChiSquareNoncentralDist;
import umontreal.iro.lecuyer.probdist.CramerVonMisesDist;
import umontreal.iro.lecuyer.probdist.ExponentialDist;
import umontreal.iro.lecuyer.probdist.ExtremeValueDist;
import umontreal.iro.lecuyer.probdist.FatigueLifeDist;
import umontreal.iro.lecuyer.probdist.FisherFDist;
import umontreal.iro.lecuyer.probdist.FoldedNormalDist;
import umontreal.iro.lecuyer.probdist.FrechetDist;
import umontreal.iro.lecuyer.probdist.GammaDist;
import umontreal.iro.lecuyer.probdist.GumbelDist;
import umontreal.iro.lecuyer.probdist.HalfNormalDist;
import umontreal.iro.lecuyer.probdist.HyperbolicSecantDist;
import umontreal.iro.lecuyer.probdist.InverseDistFromDensity;
import umontreal.iro.lecuyer.probdist.InverseGaussianDist;
import umontreal.iro.lecuyer.probdist.JohnsonSBDist;
import umontreal.iro.lecuyer.probdist.JohnsonSUDist;
import umontreal.iro.lecuyer.probdist.KolmogorovSmirnovDist;
import umontreal.iro.lecuyer.probdist.KolmogorovSmirnovPlusDist;
import umontreal.iro.lecuyer.probdist.LaplaceDist;
import umontreal.iro.lecuyer.probdist.LogisticDist;
import umontreal.iro.lecuyer.probdist.LoglogisticDist;
import umontreal.iro.lecuyer.probdist.LognormalDist;
import umontreal.iro.lecuyer.probdist.NakagamiDist;
import umontreal.iro.lecuyer.probdist.NormalDist;
import umontreal.iro.lecuyer.probdist.NormalInverseGaussianDist;
import umontreal.iro.lecuyer.probdist.ParetoDist;
import umontreal.iro.lecuyer.probdist.Pearson5Dist;
import umontreal.iro.lecuyer.probdist.Pearson6Dist;
import umontreal.iro.lecuyer.probdist.PiecewiseLinearEmpiricalDist;
import umontreal.iro.lecuyer.probdist.PowerDist;
import umontreal.iro.lecuyer.probdist.RayleighDist;
import umontreal.iro.lecuyer.probdist.StudentDist;
import umontreal.iro.lecuyer.probdist.TriangularDist;
import umontreal.iro.lecuyer.probdist.TruncatedDist;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.probdist.WatsonGDist;
import umontreal.iro.lecuyer.probdist.WatsonUDist;
import umontreal.iro.lecuyer.probdist.WeibullDist;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 29, 2010
 */
public class PopulateDistribution {	
	DistributionEntityManager distributionEM;
	
	/**
	 * 
	 */
	public PopulateDistribution() {
		String databaseProperties = "./conf/database.properties";
		String mysqlProperties = "./conf/database.mysql.properties";
		DatabaseManagerInterface dm = DatabaseManagerFactory.getDatabaseManager(databaseProperties);
		EntityManagerFactoryInterface emf = dm.getEntityManagerFactory();
		
		distributionEM = (DistributionEntityManager) emf.getEntityManager(DistributionEntityManager.class);
	}

	/**
	 * 
	 */
	public void populate() {
		Class[] distributions = new Class[] {
				AndersonDarlingDist.class, BetaDist.class, CauchyDist.class,
				ChiDist.class, ChiSquareDist.class, ChiSquareNoncentralDist.class,
				CramerVonMisesDist.class, ExponentialDist.class, ExtremeValueDist.class,
				FatigueLifeDist.class, FisherFDist.class, FoldedNormalDist.class,
				FrechetDist.class, GammaDist.class, GumbelDist.class, HalfNormalDist.class,
				HyperbolicSecantDist.class, InverseDistFromDensity.class,
				InverseGaussianDist.class, JohnsonSBDist.class, JohnsonSUDist.class,
				KolmogorovSmirnovDist.class, KolmogorovSmirnovPlusDist.class, LaplaceDist.class,
				LogisticDist.class, LoglogisticDist.class, LognormalDist.class,
				NakagamiDist.class, NormalDist.class, NormalInverseGaussianDist.class,
				ParetoDist.class, Pearson5Dist.class, Pearson6Dist.class,
				PiecewiseLinearEmpiricalDist.class, PowerDist.class, RayleighDist.class,
				StudentDist.class, TriangularDist.class, TruncatedDist.class, UniformDist.class,
				WatsonGDist.class, WatsonUDist.class, WeibullDist.class};
		
		distributionEM.truncate();
		
		for (int i=0; i<distributions.length; i++) {
			String className = distributions[i].getConstructors()[0].getName();
			
			distributionEM.insert(
					className, // distribution id
					className.substring(className.lastIndexOf(".")+1, className.lastIndexOf("Dist")), // distribution name
					"University of Montreal distribution implementation", // descritpion
					className); // class name
			
			System.out.println("Inserted: " + className);
		}
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PopulateDistribution populateDistribution = new PopulateDistribution();
		System.out.println("Populating distribution table...");
		populateDistribution.populate();
		System.out.println("Populating done.");
	}

}
