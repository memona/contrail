/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.database.EntityManagerInterface;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 8, 2010
 */
public class InferredDistributionEntityManager extends EntityManagerCommons implements EntityManagerInterface<InferredDistributionEntity> {
	// logger
	private static Logger logger = Logger.getLogger(InferredDistributionEntityManager.class);
	
	private static final String DATABASE_TABLE_NAME = "inferred_distribution";
	
	/**
	 * @param connection
	 */
	public InferredDistributionEntityManager(Connection connection) {
		this(connection, DATABASE_TABLE_NAME);
	}
	
	/**
	 * @param connection
	 * @param databaseTable
	 */
	public InferredDistributionEntityManager(Connection connection, String databaseTable) {
		super(connection, databaseTable);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param specificationParameterPK
	 * @param distributionPK
	 * @return
	 */
	public int delete(String predictionPolicyId, String qosId, String distributionId) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int deletedRecords = 0;
		
		try {
			String query = "DELETE FROM " + getDatabaseTable() + " " +
			   			   "WHERE prediction_policy_id=? AND qos_id=? AND distribution_id=?";
			
			pstmt = getConnection().prepareStatement(query);
			
			pstmt.setString(1, predictionPolicyId);
			pstmt.setString(2, qosId);
			pstmt.setString(3, distributionId);
			
			deletedRecords = pstmt.executeUpdate();

		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return deletedRecords;		
	}
		
	/**
	 * @param predictionSpecification
	 * @return
	 */
	public InferredDistributionEntity selectBestFit(String predictionPolicyId, String qosId) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		InferredDistributionEntity result = null;
		
		try {
			String query = "SELECT * FROM " + getDatabaseTable() + " " +
						   "WHERE prediction_policy_id=? AND qos_id=? AND goodness_of_fit =  " + 
						   "	(SELECT MIN(goodness_of_fit) FROM " + getDatabaseTable() + " " +
						   "	WHERE prediction_policy_id=? AND qos_id=? AND goodness_of_fit<>0) " +
						   "ORDER BY distribution_id, qos_id, parameter_index ASC";
			
			pstmt = getConnection().prepareStatement(query);
			
			pstmt.setString(1, predictionPolicyId);
			pstmt.setString(2, qosId);
			pstmt.setString(3, predictionPolicyId);
			pstmt.setString(4, qosId);
			
			logger.debug(pstmt.toString());
			
		    ResultSet rs = pstmt.executeQuery();
		    
		    rs.last();
		    double parameterValues[] = new double[0];
		    
		    int i = 0;
			if (rs.first()) {
				// create model entity
				InferredDistributionEntity inferredDistributionEntity = new InferredDistributionEntity(
						predictionPolicyId,
						rs.getString("qos_id"),
						rs.getString("model_factory_id"),
						rs.getString("distribution_id"),
						new double[0], // parameters
						new double[rs.getInt("data_points")],
						rs.getDouble("goodness_of_fit")
						);
				
				// count parameters
				do {
					i++;
				} while (rs.next() && rs.getString("distribution_id").equals(inferredDistributionEntity.getDistributionId()));
				
				rs.first();
				
				parameterValues = new double[i];
				
				i = 0;
				// fetch distribution parameter values
				do {
					parameterValues[i++] = rs.getDouble("parameter_value");
				} while (rs.next() && rs.getString("distribution_id").equals(inferredDistributionEntity.getDistributionId()));
				
				// set parameters
				inferredDistributionEntity.setParameters(parameterValues);
				
				result = inferredDistributionEntity;
			}
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return result;			
	}
	
	/**
	 * @param predictionPolicyId
	 * @param qosId
	 * @return
	 */
	public int count(String predictionPolicyId, String qosId) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int result = 0;		
			
		try {
			String query = "SELECT distribution_id FROM " + getDatabaseTable() + " " +
						   "WHERE prediction_policy_id=? AND qos_id=? " +
						   "GROUP BY distribution_id";						   
			   
		    pstmt = getConnection().prepareStatement(query);
		    
		    pstmt.setString(1, predictionPolicyId);
		    pstmt.setString(2, qosId);
		    
		    resultSet = pstmt.executeQuery();
		    
		    if (resultSet.last()) {
		    	result = resultSet.getRow();
		    }
		}
		catch (SQLException ex){
		    // handle any errors
		    ex.printStackTrace();
		}
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    releaseResources(pstmt, resultSet);
		}		
		
		return result;
	}
	
	/**
	 * @param predictionSpecification
	 * @param distribution
	 * @return
	 */
	public int update(InferredDistributionEntity entity) {
		
		delete(entity);
		
		return insert(entity);
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#insert(java.lang.Object)
	 */
	public int insert(InferredDistributionEntity entity) {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int insertedRecords = 0;
		
		// delete existing model
		delete(entity);
		
		try {
			String query = "INSERT INTO " + getDatabaseTable() + " " +
			   			   "SET prediction_policy_id=?, qos_id=?, distribution_id=?, model_factory_id=?, parameter_index=?, parameter_value=?, goodness_of_fit=?, data_points=?";
			
			int parameterIndex=0;
			for (double parameterValue : entity.getParameters()) {
				pstmt = getConnection().prepareStatement(query);
				
				if (Double.isNaN(parameterValue)) {
					return 0;
				}
				
				if (Double.isInfinite(parameterValue)) {
					parameterValue = Double.MAX_VALUE;
				}
				
				pstmt.setString(1, entity.getPredictionPolicyId());
				pstmt.setString(2, entity.getQosId());
				pstmt.setString(3, entity.getDistributionId());
				pstmt.setString(4, entity.getModelFactoryId());
				pstmt.setInt(5, parameterIndex++);
				pstmt.setDouble(6, parameterValue);
				pstmt.setDouble(7, entity.getGoodnessOfFit());
				pstmt.setInt(8, entity.getDataPoints().length);
				
				insertedRecords = pstmt.executeUpdate();
			}
			
			//System.out.println(getClass().getName() + ": " + pstmt);

		}
		catch (SQLException e){
		    // handle any errors
		    logger.error(e.getMessage(), e);
		} 
		finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

			releaseResources(pstmt, resultSet);
		}
		
		return insertedRecords;
	}

	// ------------------------------------------------------------------------
	// 								NOT IMPLEMENTED METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	public int delete(InferredDistributionEntity entity) {
		return delete(entity.getPredictionPolicyId(), entity.getQosId(), entity.getDistributionId());
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	public int deleteByPrimaryKey(String entityPrimaryKey) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery()
	 */
	public ArrayList<InferredDistributionEntity> executeQuery(String query) {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeUpdate(java.lang.String)
	 */
	public int executeUpdate(String query) {
		throw new UnsupportedOperationException("TO DO");
	}
	
	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	public ArrayList<InferredDistributionEntity> select() {
		throw new UnsupportedOperationException("TO DO");
	}

	/**
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	public InferredDistributionEntity selectByPrimaryKey(String entityPrimaryKey) {
		throw new UnsupportedOperationException("TO DO");
	}
}
