/**
 * 
 */
package uk.ac.city.soi.everestplus.predictor;

import org.apache.log4j.Logger;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 23, 2011
 */
public class PredictorFactory {
	// logger
	private static Logger logger = Logger.getLogger(PredictorFactory.class);
	
	private static final String EXCEPTION_MESSAGE = "The current implementation supports the dynamic creation of " +
													"Predictor objects with default constructor only";
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param className The predictor fully qualified class name
	 * @return An instance of the given predictor class, <code>null</code> if errors occur
	 */
	public static Predictor getPredictor(String className) {
		return getPredictor(className, new Object[0]);
	}
	
	/**
	 * @param className The predictor fully qualified class name
	 * @param arguments Predictor constructor arguments
	 * @return An instance of the given predictor class, <code>null</code> if errors occur
	 */
	public static Predictor getPredictor(String className, Object[] arguments) {
		Predictor predictor = null;
		
		try {
			//Class<Predictor> predictorClass = (Class<Predictor>) ClassLoader.getSystemClassLoader().loadClass(className);
			Class<Predictor> predictorClass = (Class<Predictor>)Class.forName(className);
			
			if (arguments.length == 0) {
				predictor = predictorClass.newInstance();				
			} else {
				// TO DO: implementation of dynamic loading of Predictor object
				//        with no default constructor
				
				throw new UnsupportedOperationException(EXCEPTION_MESSAGE);
			}
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
		} catch (InstantiationException e) {
			logger.error(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			logger.error(e.getMessage(), e);
		}
		
		return predictor;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
