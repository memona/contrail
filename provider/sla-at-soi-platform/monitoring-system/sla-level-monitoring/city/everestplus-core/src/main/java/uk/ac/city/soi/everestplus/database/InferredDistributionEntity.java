/**
 * 
 */
package uk.ac.city.soi.everestplus.database;

import uk.ac.city.soi.database.PersistentEntity;
import uk.ac.city.soi.everestplus.model.distribution.InferredDistribution;


/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 24, 2010
 */
public class InferredDistributionEntity extends InferredDistribution implements PersistentEntity {
	private long entityId;
	
	private String predictionPolicyId;
	private String qosId;
	private String modelFactoryId;
	

	/**
	 * @param predictionPolicyId
	 * @param qosId
	 * @param modelFactoryId
	 * @param inferredDistribution
	 */
	public InferredDistributionEntity(String predictionPolicyId, String qosId, String modelFactoryId, InferredDistribution inferredDistribution) {
		super(inferredDistribution);
		this.predictionPolicyId = predictionPolicyId;
		this.qosId = qosId;
		this.modelFactoryId = modelFactoryId;
	}

	/**
	 * @param predictionPolicyId
	 * @param qosId
	 * @param modelFactoryId
	 * @param distributionId
	 * @param parameters
	 * @param dataPoints
	 * @param goodnessOfFit
	 */
	public InferredDistributionEntity(String predictionPolicyId, String qosId, String modelFactoryId, String distributionId, double[] parameters, double[] dataPoints, double goodnessOfFit) {
		super(distributionId, parameters, dataPoints, goodnessOfFit);
		this.predictionPolicyId = predictionPolicyId;
		this.qosId = qosId;
		this.modelFactoryId = modelFactoryId;
	}



	/**
	 * @return the entityId
	 */
	public long getEntityId() {
		return entityId;
	}
	
	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}
	
	/**
	 * @return the predictionPolicyId
	 */
	public String getPredictionPolicyId() {
		return predictionPolicyId;
	}

	/**
	 * @param predictionPolicyId the predictionPolicyId to set
	 */
	public void setPredictionPolicyId(String predictionPolicyId) {
		this.predictionPolicyId = predictionPolicyId;
	}

	/**
	 * @return the qosId
	 */
	public String getQosId() {
		return qosId;
	}

	/**
	 * @param qosId the qosId to set
	 */
	public void setQosId(String qosId) {
		this.qosId = qosId;
	}

	/**
	 * @return the modelFactoryId
	 */
	public String getModelFactoryId() {
		return modelFactoryId;
	}

	/**
	 * @param modelFactoryId the modelFactoryId to set
	 */
	public void setModelFactoryId(String modelFactoryId) {
		this.modelFactoryId = modelFactoryId;
	}

	/**
	 * @see uk.ac.city.soi.everestplus.model.distribution.DistributionModel#getCDF(double)
	 */
	@Override
	public double getCDF(double x) {
		return Double.NaN;
	}

	/**
	 * @see uk.ac.city.soi.everestplus.model.distribution.DistributionModel#getPDF(double)
	 */
	@Override
	public double getPDF(double x) {
		return Double.NaN;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String parameters = new String();
		String result = new String();
		
		// format parameters
		for (int i=0; i<getParameters().length; i++) {
			parameters += getParameters()[i];
			if (i<getParameters().length-1) {
				parameters += ", ";
			}
		}
		
		result = getDistributionId() + "(" + parameters + ")[GoF=" + getGoodnessOfFit() + "]";
		
		return result;
	}	
}
