/**
 * 
 */
package uk.ac.city.soi.everestplus.util;

import org.slaatsoi.prediction.schema.PredictionPolicyType;

import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.core.Prediction;
import uk.ac.city.soi.everestplus.database.PredictionEntityManager;
import uk.ac.city.soi.everestplus.database.PredictionPolicyEntityManager;
import uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 8, 2010
 */
public class Statistics {
	PredictionEntityManager predictionEM;
	QoSEntityManagerInterface qosEM;
	PredictionPolicyEntityManager predictionPolicyEM;
	
	public Statistics() {
		predictionEM = (PredictionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.PREDICTIONS);
		qosEM = (QoSEntityManagerInterface) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.QOSS);
		predictionPolicyEM = (PredictionPolicyEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.PREDICTION_POLICY);
	}
	
	/**
	 * precision = TP / (TP+FP)
	 */
	public void computePrecision(String predictionPolicyId) {
		int truePositive = 0;
		int trueNegative=0;
		int falsePositive = 0;
		int falseNegative = 0;
	
		PredictionPolicyType predictionPolicy = predictionPolicyEM.selectByPrimaryKey(predictionPolicyId);
		
		// loop over prediction results
		for (int i=400; i<501; i++) {
			Prediction prediction = predictionEM.selectPrediction(i, 1).get(0);
		
			long timestamp = prediction.getTimestamp() + predictionPolicy.getPredictionSettings().getPredictionHorizon();
			
			Double qos = qosEM.selectLastByTimestamp(
					predictionPolicy.getPredictionTarget().getVariable().getSlaId(),
					predictionPolicy.getPredictionTarget().getVariable().getGuaranteedId(),
					predictionPolicy.getPredictionTarget().getVariable().getQoSId(),
					timestamp);
			
			// compute TP, TN, FP, FN
			if (qos != null) {
				String prefix = new String();
				
				// TRUE POSITIVE
				if (qos > predictionPolicy.getPredictionTarget().getValue() && prediction.getValue() > 0.5) {
					prefix = "TP";
					truePositive++;
				}
				
				// TRUE NEGATIVE
				if (qos <= predictionPolicy.getPredictionTarget().getValue() && prediction.getValue() <= 0.5) {
					prefix = "TN";
					trueNegative++;
				}
				
				// FALSE POSITIVE
				if (qos <= predictionPolicy.getPredictionTarget().getValue() && prediction.getValue() > 0.5) {
					prefix = "FP";
					falsePositive++;
				}	
				
				// FALSE NEGATIVE
				if (qos > predictionPolicy.getPredictionTarget().getValue() && prediction.getValue() <= 0.5) {
					prefix = "FN";
					falseNegative++;
				}
				
				System.out.println(prefix + " " + qos + " " + prediction.getValue());
			} else {
				System.out.println("/");
			}
		}
		
		System.out.println("TP\tFP\tTN\tFN");
		System.out.println(truePositive + "\t" + falsePositive + "\t" + trueNegative + "\t" + falseNegative);
		System.out.println();
		
		// compute precision
		double precision = ((double)truePositive/(truePositive+falsePositive));
		System.out.println("Precision: " + precision);
		
		// compute accurancy
		double accurancy = ((double)(truePositive+trueNegative)/(truePositive+falsePositive+trueNegative+falseNegative));
		System.out.println("Accurancy: " + accurancy);
		
		double recall = ((double)(truePositive)/(truePositive+trueNegative));
		System.out.println("Recall:" + recall);
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String specificationId = "SPEC-1";
		
		Statistics statistics = new Statistics();
		statistics.computePrecision(specificationId);
	}

}
