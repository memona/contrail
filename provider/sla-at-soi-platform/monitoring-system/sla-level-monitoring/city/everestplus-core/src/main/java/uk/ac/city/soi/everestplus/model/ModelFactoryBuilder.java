/**
 * 
 */
package uk.ac.city.soi.everestplus.model;

import java.util.Hashtable;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 29, 2010
 */
public class ModelFactoryBuilder extends ClassLoader {
	private static Hashtable<String, ModelFactory> modelFactories = new Hashtable<String, ModelFactory>();
	
	/**
	 * @param className
	 * @return
	 */
	public static ModelFactory createModelFactory(String className) {
		ModelFactory modelFactory = null;
		
		try {
			modelFactory = (ModelFactory) getSystemClassLoader().loadClass(className).newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return modelFactory;
	}
	
	/**
	 * @param modelClass
	 * @return
	 */
	public static ModelFactory createModelFactoryNew(Class modelClass) {
		
		// loop over the model factories
		for (ModelFactory modelFactory : modelFactories.values()) {
			
			// check is a model factory supports the given model
			if (modelFactory.isSupported(modelClass)) {
				return modelFactory;
			}
		}
		
		return null;
	}
	
	/**
	 * Add a model factory to the list of available ones
	 * 
	 * @param modelFactory
	 */
	public static void addModelFactory(ModelFactory modelFactory) {
		modelFactories.put(modelFactory.getClass().getName(), modelFactory);
	}
	
	/**
	 * @param modelFactory
	 */
	public static void removeModelFactory(ModelFactory modelFactory) {
		modelFactories.remove(modelFactory.getClass().getName());
	}
}
