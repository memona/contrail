<?xml version="1.0" encoding="UTF-8"?>

<!-- VERSION 0.5.0 -->

<!--
	(23.06.2011): Renamed OperatorType to ComparisonOperatorType
	
	(22.06.2011): Added support for predictor auxiliary variables

	(21.06.2011): removed QoSId element from Predictor element. Added Parameters
				  element to Predictor element.

	(11.06.2011): corrected misspelled elements and added documentation

	(11.06.2011): updated schema, no longer compatible with the previous one
	
	(28.06.2011): created first schema version 0.0.1
 -->

<xsd:schema 
	xmlns:jaxb="http://java.sun.com/xml/ns/jaxb"
	jaxb:version="2.1"
	
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	targetNamespace="http://www.slaatsoi.org/prediction-policy"
	xmlns="http://www.slaatsoi.org/prediction-policy"
>

	<!-- JAXB compiler options to set the output package -->
	<xsd:annotation>
		<xsd:documentation>JAXB compiler options to set the output package</xsd:documentation>
		<xsd:appinfo>
			<jaxb:schemaBindings>
				<jaxb:package name="org.slaatsoi.prediction.schema"/>
			</jaxb:schemaBindings>
           <jaxb:globalBindings>
               <jaxb:serializable />
           </jaxb:globalBindings>
		</xsd:appinfo>
	</xsd:annotation>
	
	<!-- **********************************************************************
								Root Element 
	*********************************************************************** -->
	
    <xsd:element name="PredictionPolicy" type="PredictionPolicyType">
    	<xsd:annotation>
    		<xsd:documentation>Prediction policy root element</xsd:documentation>
    	</xsd:annotation>
    </xsd:element>
	
	<!-- PredictionPolicyType -->
	
    <xsd:complexType name="PredictionPolicyType">
        <xsd:annotation>
        	<xsd:documentation>This element defines the root element type. A prediction policy structures information in three groups: prediction target, predictor, and prediction settings.</xsd:documentation>
        </xsd:annotation>
        <xsd:sequence minOccurs="1" maxOccurs="1">
			<xsd:element name="PredictionTarget" type="PredictionTargetType" minOccurs="1" maxOccurs="1">
				<xsd:annotation>
					<xsd:documentation>This element contains information about the SLA element to predict about.</xsd:documentation>
				</xsd:annotation></xsd:element>
			<xsd:element name="Predictor" type="PredictorType" minOccurs="1" maxOccurs="1">
				<xsd:annotation>
					<xsd:documentation>This element contains information about the kind of prediction to apply to the chosen SLA element.</xsd:documentation>
				</xsd:annotation></xsd:element>
    		<xsd:element name="PredictionSettings" type="PredictionSettingsType" minOccurs="1" maxOccurs="1" >
    			<xsd:annotation>
    				<xsd:documentation>This element contains information about the chosen predictor configuration.</xsd:documentation>
    			</xsd:annotation></xsd:element>
    	</xsd:sequence>
    	<xsd:attribute name="PredictionPolicyId" type="xsd:string">
    		<xsd:annotation>
    			<xsd:documentation>The prediction policy unique identifier.</xsd:documentation>
    		</xsd:annotation></xsd:attribute>
    </xsd:complexType>

	<!-- PredictionTargetType -->

    <xsd:complexType name="PredictionTargetType">
        <xsd:annotation>
        	<xsd:documentation>This element contains information about the SLA element to predict about. The four elements SlaId, AgreementId, GuaranteedId, and QoSId uniquely identify an element within an SLA. Operator and Value elements apply prediction constraints  to the element.

For instance:
{sla1, at1, g1, qos1} less_than 30, where the prediction target is {sla1, at1, g1, qos1}, the operator is less_than, and the values is 30.</xsd:documentation>
        </xsd:annotation>
        <xsd:sequence minOccurs="1" maxOccurs="1">
            <xsd:element name="Variable" type="VariableType" maxOccurs="1" minOccurs="1">
            	<xsd:annotation>
            		<xsd:documentation>This element contains the information about an SLA prediction target item. A target item is composed by four values {SlaId, AgreementTermId, GuaranteedId, QoSId}.</xsd:documentation>
            	</xsd:annotation></xsd:element>
            <xsd:element name="ComparisonOperator" type="ComparisonOperatorType" minOccurs="1"
        		maxOccurs="1"
        	>
        		<xsd:annotation>
        			<xsd:documentation>
        				This element contains the constraint operator that,
        				together with the element Value, defines the prediction
        				expression. Allowed Operator element values are defined
        				by OperatorType element.
        			</xsd:documentation>
        		</xsd:annotation>
        	</xsd:element>
        	<xsd:element name="Value" type="xsd:double" minOccurs="1"
        		maxOccurs="1"
        	>
        		<xsd:annotation>
        			<xsd:documentation>
        				This element contains the value associated to constraint
        				operator.
        			</xsd:documentation>
        		</xsd:annotation>
        	</xsd:element>
        </xsd:sequence>
    </xsd:complexType>

	<!-- PredictorType -->
    
    <xsd:complexType name="PredictorType">
        <xsd:annotation>
        	<xsd:documentation>This element contains information about the kind of prediction to apply to the chosen SLA element.

For instance, to predict about mean time to repair (MTTR):
- Qos id: MTTR
- Predicto Id: uk.ac.city.soi.everestplus.predictor.MTTRredictor</xsd:documentation>
        </xsd:annotation>
        <xsd:sequence minOccurs="1" maxOccurs="1">
        	<xsd:element name="PredictorId" type="xsd:string">
        		<xsd:annotation>
        			<xsd:documentation>
        				The Id of predictor to compute a QoS violation
        				probability. Everest-Plus uses classes fully-qualified
        				names as predictor Ids.

        				For instance, to compute predictions about mean time to
        				repair (MTTR) the MTTR predictor class name is
        				uk.ac.city.soi.everestplus.predictor.MTTRredictor.
        			</xsd:documentation>
        		</xsd:annotation>
        	</xsd:element>
        	<xsd:element name="ConfigurationParameters" maxOccurs="1" minOccurs="0">
        		<xsd:annotation>
        			<xsd:documentation>The parameters necessary to initialise the predictor. Everest-Plus dynamically load a predictor by its Id, whose value correspond to the predictor class name, and than invoke its constructor selected with respect to the number of provided parameters.</xsd:documentation>
        		</xsd:annotation>
        		<xsd:complexType>
        			<xsd:annotation>
        				<xsd:documentation>The parameters necessary to initialise the predictor. Everest-Plus dynamically load a predictor by its Id, whose value correspond to the predictor class name, and than invoke its constructor selected with respect to the number of provided parameters.</xsd:documentation>
        			</xsd:annotation>
        			<xsd:sequence maxOccurs="1" minOccurs="1">
        				<xsd:element name="Parameter" maxOccurs="unbounded"
        					minOccurs="1"
        				>
        					<xsd:annotation>
        						<xsd:documentation>
        							The parameter to be given as argument to the
        							predictor constructor
        						</xsd:documentation>
        					</xsd:annotation>
        					<xsd:complexType>
        						<xsd:attribute name="key" type="xsd:string">
        							<xsd:annotation>
        								<xsd:documentation>
        									The parameter name
        								</xsd:documentation>
        							</xsd:annotation>
        						</xsd:attribute>
        						<xsd:attribute name="value" type="xsd:string">
        							<xsd:annotation>
        								<xsd:documentation>
        									The parameter value
        								</xsd:documentation>
        							</xsd:annotation>
        						</xsd:attribute>
        					</xsd:complexType>
        				</xsd:element>
        			</xsd:sequence>
        		</xsd:complexType>
        	</xsd:element>
        	<xsd:element name="AuxiliaryVariables" type="AuxiliaryVariablesType" maxOccurs="1" minOccurs="0">
        		<xsd:annotation>
        			<xsd:documentation>This element contains information about the data a predictor needs to operate.</xsd:documentation>
        		</xsd:annotation></xsd:element>
        </xsd:sequence>
    </xsd:complexType>

	<!-- PredictionSettingsType -->

    <xsd:complexType name="PredictionSettingsType">
        <xsd:annotation>
        	<xsd:documentation>This element contains information about the chosen predictor configuration.</xsd:documentation>
        </xsd:annotation>
        <xsd:sequence minOccurs="1" maxOccurs="1">
    		<xsd:element name="PredictionHorizon" type="xsd:long" minOccurs="1" maxOccurs="1">
    			<xsd:annotation>
    				<xsd:documentation>This element contains information about how far in the future the prediction should look at. The prediction horizon is expressed in milliseconds.

For instance, to configure a predictor to compute the probability to observe a violation in 10 minutes time in the future the prediction horizon value will be equal to 600000 (1000*60*10).</xsd:documentation>
    			</xsd:annotation>
    		</xsd:element>
    		<xsd:element name="HistorySize" type="xsd:int">
    			<xsd:annotation>
    				<xsd:documentation>This element  contains the number of historical data points to consider during the statistical model inferring process. Everest-Plus existing set of implemented predictors uses a default value of 100, which was proved a good compromise between model accurancy and model generation overhead.</xsd:documentation>
    			</xsd:annotation>
    		</xsd:element>
    		<xsd:element name="NotificationThresholds" type="NotificationThresholdsType" minOccurs="1" maxOccurs="1" >
    			<xsd:annotation>
    				<xsd:documentation>This element contains the set theresholds definining when the prediction framework should create a warning event.</xsd:documentation>
    			</xsd:annotation></xsd:element>
    	</xsd:sequence>
    </xsd:complexType>
    
    <!-- NotificationTheresholdsType -->
    
    <xsd:complexType name="NotificationThresholdsType">
        <xsd:annotation>
        	<xsd:documentation>This element contains the set theresholds definining when the prediction framework should create a warning event.</xsd:documentation>
        </xsd:annotation>
        <xsd:sequence minOccurs="1" maxOccurs="1">
    		<xsd:element name="NotificationThreshold" type="ThresholdType" minOccurs="1" maxOccurs="unbounded">
    		    <xsd:annotation>
    				<xsd:documentation>
    				Indicates when a warning of violation must be issued, e.g.,
    				when the probability of violating the constraint MTTR > 30
    				is greater than 0.85, that is Pr(MTTR > 30) > 0.85.
    				</xsd:documentation>
    			</xsd:annotation>
    		</xsd:element>
    	</xsd:sequence>
    </xsd:complexType>
    
    <!-- TheresholdType -->
    
    <xsd:complexType name="ThresholdType">
    	<xsd:sequence minOccurs="1" maxOccurs="1">
    		<xsd:element name="Operator" type="ComparisonOperatorType" minOccurs="1" maxOccurs="1">
    			<xsd:annotation>
    				<xsd:documentation>This element contains the constraint operator that, together with the element Value, defines the prediction notification thresholds. Allowed Operator element values are defined by OperatorType element.</xsd:documentation>
    			</xsd:annotation></xsd:element>
    		<xsd:element name="Value" type="xsd:double" minOccurs="1" maxOccurs="1">
    			<xsd:annotation>
    				<xsd:documentation>This element contains the value associated to constraint operator.</xsd:documentation>
    			</xsd:annotation></xsd:element>
    	</xsd:sequence>
    </xsd:complexType>
    
    <!-- **********************************************************************
								Enumerations 
	*********************************************************************** -->
	
	<xsd:simpleType name="ComparisonOperatorType">
    	<xsd:restriction base="xsd:string">
    		<xsd:enumeration value="less_than" />
    		<xsd:enumeration value="less_equal_than" />
    		<xsd:enumeration value="equal" />
    		<xsd:enumeration value="greater_equal_than" />
    		<xsd:enumeration value="greater_than" />
    	</xsd:restriction>
    </xsd:simpleType>
    





    <xsd:complexType name="AuxiliaryVariablesType">
        <xsd:annotation>
        	<xsd:documentation>This element contains information about the data a predictor needs to operate.</xsd:documentation>
        </xsd:annotation>
        <xsd:sequence maxOccurs="1" minOccurs="1">
    		<xsd:element name="Variable" type="VariableType" maxOccurs="unbounded" minOccurs="1">
    			<xsd:annotation>
    				<xsd:documentation>This element contains information about the data a predictor needs to operate.</xsd:documentation>
    			</xsd:annotation></xsd:element>
    	</xsd:sequence>
    </xsd:complexType>

    <xsd:complexType name="VariableType">
        <xsd:annotation>
        	<xsd:documentation>This element contains the information about an SLA prediction target item. A target item is composed by four values {SlaId, AgreementTermId, GuaranteedId, QoSId}.</xsd:documentation>
        </xsd:annotation>
        <xsd:attribute name="SlaId" type="xsd:string"></xsd:attribute>
    	<xsd:attribute name="AgreementTermId" type="xsd:string"></xsd:attribute>
        <xsd:attribute name="GuaranteedId" type="xsd:string"></xsd:attribute>
        <xsd:attribute name="QoSId" type="xsd:string"></xsd:attribute>
    </xsd:complexType>
</xsd:schema>