/**
 * 
 */
package uk.ac.city.soi.everestplus.core;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 31, 2011
 */
public class FrameworkContextManager {
	// logger
	private static Logger logger = Logger.getLogger(FrameworkContextManager.class);
	private static final String CONF_FOLDER_NAME = "conf";
	private static FrameworkContext frameworkContext;

	
	/**
	 * Private constructor, an instance of FrameworkContext is obtained
	 * by invoking <code>getFrameworkContext</code>
	 */
	private FrameworkContextManager() {};
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * This method initialises the framework context. It is mandatory to invoke
	 * this method before any other method.
	 * 
	 * @throws FrameworkContextInitialisationException
	 */
	public static void initialiseFrameworkContext() throws FrameworkContextInitialisationException {
		// if the context is not initialised then do it
		if (frameworkContext == null) {
			frameworkContext = initFrameworkContext();
		}
	}
	
	/**
	 * @return The FrameworkContext object, <code>null</code> if the context
	 *         hasn't been yet initialised.
	 */
	public static FrameworkContext getFrameworkContext() {
		if (frameworkContext == null) throw new RuntimeException("Plese, invoke initialiseFrameworkContext method first.");
		return frameworkContext;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return The FrameworkContext object
	 * @throws FrameworkContextInitialisationException 
	 */
	private static FrameworkContext initFrameworkContext() throws FrameworkContextInitialisationException {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		
		String fileSeparator = System.getProperty("file.separator");
		String slasoiHome = System.getenv("SLASOI_HOME");
		
		// path to configuration folder within SLA@SOI OSGI environment
		if (slasoiHome != null) {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(fileSeparator);
			frameworkConfigurationFolder.append("monitoring-system");
			frameworkConfigurationFolder.append(fileSeparator);
			frameworkConfigurationFolder.append("sla-level-monitoring");
			frameworkConfigurationFolder.append(fileSeparator);
			frameworkConfigurationFolder.append("city-everestplus-core");
			frameworkConfigurationFolder.append(fileSeparator);
			frameworkConfigurationFolder.append(CONF_FOLDER_NAME);
		}
		// path to configuration folder as stand alone framework
		else {
			logger.debug("Environmental variable JAVA_HOME not set.");			
			frameworkConfigurationFolder = new StringBuilder(ClassLoader.getSystemResource(CONF_FOLDER_NAME).getPath());
		}
		
		try {
			logger.debug("Loading context properties from: " + new File(frameworkConfigurationFolder.toString()).getCanonicalPath());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		
		System.out.println(frameworkConfigurationFolder.toString());
		return new FrameworkContext(frameworkConfigurationFolder.toString());
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
