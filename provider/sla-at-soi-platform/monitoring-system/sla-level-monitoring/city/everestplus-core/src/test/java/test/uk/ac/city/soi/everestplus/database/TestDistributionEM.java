/**
 * 
 */
package test.uk.ac.city.soi.everestplus.database;


import junit.framework.Assert;
import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.database.DistributionEntityManager;
import uk.ac.city.soi.everestplus.extension.umontreal.UMontrealDistributionFactory;
import umontreal.iro.lecuyer.probdist.AndersonDarlingDist;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jul 15, 2010
 */
public class TestDistributionEM extends TestCase {
	static {
		try {
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	private static Logger logger = Logger.getLogger(TestDistributionEM.class);
	
	private DistributionEntityManager distributionEM;

	/**
	 * 
	 */
	public TestDistributionEM() {
		distributionEM = (DistributionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.DISTRIBUTIONS);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		distributionEM.truncate();
	}
	
	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	@After
	protected void tearDown() throws Exception {
		distributionEM.truncate();
	}
	
	@Test
	public void testTruncate() {
		if (distributionEM.count() == 0) insertDistributions();
		
		distributionEM.truncate();
		
		assertEquals(0, distributionEM.count());
	}
	
	@Test
	public void testInsert() {
		insertDistributions();
		
		Assert.assertEquals(UMontrealDistributionFactory.getSupportedDistributions().length, distributionEM.count());
	}

	@Test
	public void testSelectById() {
		testInsert();
		
		Assert.assertEquals(AndersonDarlingDist.class.getName(), distributionEM.selectDistributionById(AndersonDarlingDist.class.getName()).getDistributionId());
	}
	
	@Test
	public void testSelectByClassName() {
		testInsert();
		
		Assert.assertEquals(AndersonDarlingDist.class.getName(), distributionEM.selectDistributionByClassName(AndersonDarlingDist.class.getName()).getClassName());
	}	
	
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	private void insertDistributions() {
		Class[] distributions = UMontrealDistributionFactory.getSupportedDistributions();
		
		for (int i=0; i<distributions.length; i++) {
			String className = distributions[i].getConstructors()[0].getName();
			
			distributionEM.insert(
					className, // distribution id
					className.substring(className.lastIndexOf(".")+1, className.lastIndexOf("Dist")), // distribution name
					"University of Montreal distribution implementation", // descritpion
					className); // class name
		}
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
