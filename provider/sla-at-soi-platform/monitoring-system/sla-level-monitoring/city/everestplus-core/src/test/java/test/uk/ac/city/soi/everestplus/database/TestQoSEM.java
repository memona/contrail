/**
 * 
 */
package test.uk.ac.city.soi.everestplus.database;

import java.io.IOException;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.xml.sax.SAXException;

import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.database.QoSEntityManager;
import uk.ac.city.soi.everestplus.parser.PredictionPolicyParser;
import uk.ac.city.soi.everestplus.util.XMLUtils;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 27, 2011
 */
public class TestQoSEM extends TestCase {
	static {
		PropertyConfigurator.configure(ClassLoader.getSystemResource("conf/log4j.properties").getPath());
		try {
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	// logger
	private static Logger logger = Logger.getLogger(TestEverestRCGQoSEM.class);
	private static final String PREDICTION_POLICY_SCHEMA = ClassLoader.getSystemResource("model/prediction-policy.xsd").getPath();
	private static final String PREDICTION_POLICY = ClassLoader.getSystemResource("PredictionPolicy-MTTC-BookTime-Khaled.xml").getPath();
	
	private QoSEntityManager qosEM;
	
	/**
	 * 
	 */
	public TestQoSEM() {
		qosEM = (QoSEntityManager) FrameworkContextManager.getFrameworkContext().getEntityManagerFactory().getEntityManager(QoSEntityManager.class);
		logger.debug(qosEM.getConnection());
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		qosEM.truncate();
		populateQos();
	}
	
	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	@Override
	protected void tearDown() throws Exception {
		qosEM.truncate();
	}
	
	/**
	 * @throws IOException
	 * @throws SAXException
	 */
	@Test
	public void testInsert() throws IOException, SAXException {
		qosEM.truncate();
		
		assertEquals(0, qosEM.count());
		
		populateQos();
		
		assertEquals(500, qosEM.count());
	}
	
	/**
	 * @throws IOException
	 * @throws SAXException
	 */
	public void testSelect() throws IOException, SAXException {
		populateQos();
		
		PredictionPolicyType predictionPolicy = getPredictionPolicy();
		
		Double value = qosEM.selectLast(
				predictionPolicy.getPredictionTarget().getVariable().getSlaId(),
				predictionPolicy.getPredictionTarget().getVariable().getGuaranteedId(),
				predictionPolicy.getPredictionTarget().getVariable().getQoSId());
		
		assertEquals(498d, value);
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	private void populateQos() throws IOException, SAXException {
		PredictionPolicyType predictionPolicy = getPredictionPolicy();
		
		for (int i=0; i<500; i++) {
			qosEM.insert(
					predictionPolicy.getPredictionTarget().getVariable().getSlaId(),
					predictionPolicy.getPredictionTarget().getVariable().getAgreementTermId(),
					predictionPolicy.getPredictionTarget().getVariable().getGuaranteedId(),
					predictionPolicy.getPredictionTarget().getVariable().getQoSId(),
					i,
					System.currentTimeMillis());
		}
	}
	
	/**
	 * @return
	 * @throws IOException
	 * @throws SAXException
	 */
	private PredictionPolicyType getPredictionPolicy() throws IOException, SAXException {
		String predictionPolicyXML = XMLUtils.fileToString(PREDICTION_POLICY);
		String predictionPolicySchemaXML = XMLUtils.fileToString(PREDICTION_POLICY_SCHEMA);
		
		logger.debug(predictionPolicyXML);
		
		PredictionPolicyType predictionPolicy = PredictionPolicyParser.parsePredictionPolicy(predictionPolicyXML, predictionPolicySchemaXML);
		
		return predictionPolicy;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								INNER CLASSES
	// ------------------------------------------------------------------------
}
