/**
 * 
 */
package test.uk.ac.city.soi.everestplus.database;


import java.io.IOException;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.xml.sax.SAXException;

import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.core.Prediction;
import uk.ac.city.soi.everestplus.database.PredictionEntity;
import uk.ac.city.soi.everestplus.database.PredictionEntityManager;
import uk.ac.city.soi.everestplus.database.PredictionPolicyEntityManager;
import uk.ac.city.soi.everestplus.parser.PredictionPolicyParser;
import uk.ac.city.soi.everestplus.util.XMLUtils;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jul 15, 2010
 */
public class TestPredictionEM {
	static {
		try {
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	private static Logger logger = Logger.getLogger(TestPredictionEM.class);
	
	private static final String PREDICTION_POLICY_SCHEMA = ClassLoader.getSystemResource("model/prediction-policy.xsd").getPath();
	private static final String PREDICTION_POLICY = ClassLoader.getSystemResource("PredictionPolicy-MTTC-BookTime-Khaled.xml").getPath();
	
	private PredictionEntityManager predictionEM;
	private PredictionPolicyEntityManager predictionPolicyEM;

	/**
	 * 
	 */
	public TestPredictionEM() {
		predictionEM = (PredictionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.PREDICTIONS);
		predictionPolicyEM = (PredictionPolicyEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.PREDICTION_POLICY);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		predictionEM.truncate();
		predictionPolicyEM.truncate();
		
	}

	/**
	 * 
	 */
	@After
	public void tearDown() {
		predictionEM.truncate();
		predictionPolicyEM.truncate();
	}
	
	@Test
	public void testInsert() throws IOException, SAXException {
		predictionPolicyEM.insert(getPredictionPolicy());
		predictionEM.insert(new PredictionEntity(getPrediction()));
		
		Assert.assertEquals(1, predictionEM.count());
	}
	
	/**
	 * @throws SAXException 
	 * @throws IOException 
	 * 
	 */
	public void testSelectBySpecificationIdAndTimestamp() throws IOException, SAXException {
		predictionPolicyEM.insert(getPredictionPolicy());
		Prediction prediction = getPrediction();
		predictionEM.insert(new PredictionEntity(prediction));
		
		PredictionEntity selectedPrediction = predictionEM.selectBySpecificationIdAndTimestamp(
				getPredictionPolicy().getPredictionPolicyId(),
				System.currentTimeMillis());
		
		Assert.assertEquals(prediction.getPredictionSpecification().getPredictionPolicyId(), selectedPrediction.getPredictionSpecification().getPredictionPolicyId());
		Assert.assertEquals(prediction.getValue(), selectedPrediction.getValue());
		Assert.assertEquals(prediction.getPredictionSpecification().getPredictionSettings().getPredictionHorizon(), selectedPrediction.getPredictionSpecification().getPredictionSettings().getPredictionHorizon());
		Assert.assertEquals(prediction.getTimestamp(), selectedPrediction.getTimestamp());
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @return
	 * @throws IOException
	 * @throws SAXException
	 */
	private PredictionPolicyType getPredictionPolicy() throws IOException, SAXException {
		String predictionPolicyXML = XMLUtils.fileToString(PREDICTION_POLICY);
		String predictionPolicySchemaXML = XMLUtils.fileToString(PREDICTION_POLICY_SCHEMA);
		
		logger.debug(predictionPolicyXML);
		
		PredictionPolicyType predictionPolicy = PredictionPolicyParser.parsePredictionPolicy(predictionPolicyXML, predictionPolicySchemaXML);
		
		return predictionPolicy;
	}
	
	/**
	 * @return
	 * @throws SAXException 
	 * @throws IOException 
	 */
	private Prediction getPrediction() throws IOException, SAXException {
		return new Prediction(getPredictionPolicy(), 0.8, System.currentTimeMillis());
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
