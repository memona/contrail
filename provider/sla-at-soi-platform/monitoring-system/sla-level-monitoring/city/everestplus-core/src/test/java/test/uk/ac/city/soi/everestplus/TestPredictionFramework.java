/**
 * 
 */
package test.uk.ac.city.soi.everestplus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.slaatsoi.prediction.schema.VariableType;
import org.xml.sax.SAXException;

import test.uk.ac.city.soi.everestplus.distribution.TestDistributions;
import uk.ac.city.soi.database.EntityManagerCommons;
import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.core.Prediction;
import uk.ac.city.soi.everestplus.database.QoSEntityManager;
import uk.ac.city.soi.everestplus.database.QoSEntityManagerInterface;
import uk.ac.city.soi.everestplus.impl.EverestPlusImpl;
import uk.ac.city.soi.everestplus.parser.PredictionPolicyParser;
import uk.ac.city.soi.everestplus.util.XMLUtils;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 28, 2010
 */
public class TestPredictionFramework extends TestCase {
	static {
		try {
			//System.out.println("HGDHAGDHASGDHSAG");
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	private static Logger logger = Logger.getLogger(TestDistributions.class);
	
	private static final String PREDICTION_POLICY_SCHEMA = ClassLoader.getSystemResource("model/prediction-policy.xsd").getPath();
	//private static final String PREDICTION_POLICY = ClassLoader.getSystemResource("PredictionPolicy-MTTC-BookTime-Khaled.xml").getPath();
	private static final String PREDICTION_POLICY = ClassLoader.getSystemResource("PredictionPolicy-B6SLA-G3.xml").getPath();
	
	
	private QoSEntityManagerInterface qosEM;
	
	/**
	 * 
	 */
	public TestPredictionFramework() {
		qosEM = (QoSEntityManagerInterface) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.QOSS);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		((EntityManagerCommons) qosEM).truncate();
	}
	
	/**
	 * @throws IOException
	 * @throws SAXException
	 */
	@Test
	public void testPrediction() throws IOException, SAXException {
		EverestPlusImpl everestPlus = new EverestPlusImpl();
		
		long startingTimestamp = System.currentTimeMillis();
		long endingTimestamp = startingTimestamp + 600000; // 10 minutes
		int numberOfQoS = 500;
		
		long predictionTimestamp = startingTimestamp+300000;
		long predictionHorizon = 100000;
		
		logger.info("QoS starting timestamp: " + startingTimestamp);
		logger.info("QoS ending timestamp: " + endingTimestamp);
		
		//populateQoS(startingTimestamp, endingTimestamp, numberOfQoS);
		
		PredictionPolicyType predictionPolicy = getPredictionPolicy();
		//predictionPolicy.getPredictionTarget().setValue(2600);
		//predictionPolicy.getPredictionSettings().setPredictionHorizon(predictionHorizon);
		
		// add prediction
		Prediction prediction = everestPlus.getPrediction(predictionPolicy, predictionTimestamp);
		
		assertNotNull(prediction);
		
		logger.info("Prediction value (t=" + prediction.getTimestamp() + "): " + prediction.getValue());
		
		Double qosValue = qosEM.selectLastByTimestamp(
				predictionPolicy.getPredictionTarget().getVariable().getSlaId(),
				predictionPolicy.getPredictionTarget().getVariable().getGuaranteedId(),
				predictionPolicy.getPredictionTarget().getVariable().getQoSId(),
				predictionTimestamp+predictionHorizon);
		
		logger.info("Future QoS value (t=" + (predictionTimestamp+predictionHorizon) + "): " + qosValue);
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	private void populateQoS(long startingTimestamp, long endingTimestamp, int numberOfQoS) throws IOException, SAXException {
		PredictionPolicyType predictionPolicy = getPredictionPolicy();
		
		ArrayList<VariableType> variables = new ArrayList<VariableType>();
		variables.add(predictionPolicy.getPredictionTarget().getVariable());
		variables.addAll(predictionPolicy.getPredictor().getAuxiliaryVariables().getVariable());
		
		Random random = new Random();
		long offset = (endingTimestamp-startingTimestamp)/numberOfQoS;
		
		for (int i=0; i<numberOfQoS; i++) {
				double value = Math.abs(random.nextGaussian() - 131)/0.05;
			
				((QoSEntityManager) qosEM).insert(
						variables.get(i%2).getSlaId(),
						variables.get(i%2).getAgreementTermId(),
						variables.get(i%2).getGuaranteedId(),
						variables.get(i%2).getQoSId(),
						value,
						startingTimestamp);
				
				startingTimestamp += offset;
		}	
	}
	
	/**
	 * @return
	 * @throws IOException
	 * @throws SAXException
	 */
	private static PredictionPolicyType getPredictionPolicy() throws IOException, SAXException {
		String predictionPolicyXML = XMLUtils.fileToString(PREDICTION_POLICY);
		String predictionPolicySchemaXML = XMLUtils.fileToString(PREDICTION_POLICY_SCHEMA);
		
		logger.debug(predictionPolicyXML);
		
		PredictionPolicyType predictionPolicy = PredictionPolicyParser.parsePredictionPolicy(predictionPolicyXML, predictionPolicySchemaXML);
		
		return predictionPolicy;
	}
	
	public static void main(String[] args){
		TestPredictionFramework tpf = new TestPredictionFramework();
		try{
			/*
			System.out.println("HGDHAGDHASGDHSAG");
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
			*/
			tpf.setUp();
			tpf.testPrediction();
		}catch(Exception exp){
			exp.printStackTrace();
		}
		
		
	}
}
