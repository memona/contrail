/**
 * 
 */
package test.uk.ac.city.soi.everestplus.distribution;

import java.io.IOException;
import java.util.Random;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.slaatsoi.prediction.schema.PredictionPolicyType;
import org.xml.sax.SAXException;

import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;
import uk.ac.city.soi.everestplus.database.DistributionEntityManager;
import uk.ac.city.soi.everestplus.database.InferredDistributionEntity;
import uk.ac.city.soi.everestplus.database.InferredDistributionEntityManager;
import uk.ac.city.soi.everestplus.database.PredictionPolicyEntityManager;
import uk.ac.city.soi.everestplus.extension.umontreal.UMontrealDistributionCalculator;
import uk.ac.city.soi.everestplus.extension.umontreal.UMontrealDistributionFactory;
import uk.ac.city.soi.everestplus.model.ModelCalculator;
import uk.ac.city.soi.everestplus.model.ModelFactoryBuilder;
import uk.ac.city.soi.everestplus.model.distribution.DistributionModel;
import uk.ac.city.soi.everestplus.parser.PredictionPolicyParser;
import uk.ac.city.soi.everestplus.util.XMLUtils;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 25, 2010
 */
public class TestDistributions extends TestCase {
	static {
		try {
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	private static Logger logger = Logger.getLogger(TestDistributions.class);
	
	private static final String PREDICTION_POLICY_SCHEMA = ClassLoader.getSystemResource("model/prediction-policy.xsd").getPath();
	private static final String PREDICTION_POLICY = ClassLoader.getSystemResource("PredictionPolicy-MTTC-BookTime-Khaled.xml").getPath();
	
	private InferredDistributionEntityManager modelEM;
	private DistributionEntityManager distributionEM;
	private PredictionPolicyEntityManager predictionPolicyEM;

	/**
	 * 
	 */
	public TestDistributions() {
		modelEM = (InferredDistributionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.INFERRED_DISTRIBUTIONS);
		distributionEM = (DistributionEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.DISTRIBUTIONS);
		predictionPolicyEM = (PredictionPolicyEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(DatabaseManagers.PREDICTION_POLICY);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		modelEM.truncate();
		distributionEM.truncate();
		predictionPolicyEM.truncate();
		
		populateDistributionsDatabase();
	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	@Override
	public void tearDown() throws Exception {
		modelEM.truncate();
		distributionEM.truncate();
		predictionPolicyEM.truncate();
	}
	
	/**
	 * @throws SAXException 
	 * @throws IOException 
	 * 
	 */
	@Test
	public void testDistributionGeneration() throws IOException, SAXException {		
		assertEquals(UMontrealDistributionFactory.getSupportedDistributions().length, distributionEM.count());
		
		PredictionPolicyType predictionPolicy = getPredictionPolicy();
		
		predictionPolicyEM.insert(predictionPolicy);
		
		ModelCalculator modelCalculator = new UMontrealDistributionCalculator(UMontrealDistributionCalculator.class.getName());
		
		String predictionPolicyId = predictionPolicy.getPredictionPolicyId();
		String qosId = predictionPolicy.getPredictionTarget().getVariable().getQoSId();
		double dataPoints[] = generateDataPoints(500);
		
		// update models
		modelCalculator.updateModels(predictionPolicy, predictionPolicy.getPredictionTarget().getVariable(), dataPoints);
		// get best fit distribution
		InferredDistributionEntity inferredDistributionEntity = modelEM.selectBestFit(predictionPolicyId, qosId);
		// get distribution class name
		String distributionClassName = distributionEM.selectDistributionById(inferredDistributionEntity.getDistributionId()).getClassName();
		// get distribution factory
		UMontrealDistributionFactory distributionFactory = (UMontrealDistributionFactory) ModelFactoryBuilder.createModelFactory(inferredDistributionEntity.getModelFactoryId());
		// create distribution instance
		DistributionModel  distributionModel = (DistributionModel) distributionFactory.createDistribution(distributionClassName, inferredDistributionEntity.getParameters());
		
		assertNotNull(distributionModel);
		
		logger.debug(distributionModel);
		logger.debug("CDF: " + distributionModel.getCDF(200));
		logger.debug("PDF: " + distributionModel.getPDF(200));
		
		assertTrue(0 < distributionModel.getCDF(200));
		assertTrue(0 < distributionModel.getPDF(200));
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * 
	 */
	private void populateDistributionsDatabase() {
		Class distributions[] = UMontrealDistributionFactory.getSupportedDistributions();
		
		for (Class distribution : distributions) {
			String className = distribution.getName();
			
			//logger.debug("distribution name: " + className);
			
			distributionEM.insert(
					className, // distribution id
					className.substring(className.lastIndexOf(".")+1, className.lastIndexOf("Dist")), // distribution name
					"University of Montreal distribution implementation", // descritpion
					className); // class name
		}
	}
	
	/**
	 * @param size
	 * @return
	 */
	private double[] generateDataPoints(int size) {
		double[] samples = new double[size];
		for (int i=0; i<size; i++) {
			samples[i] = Math.abs(new Random().nextInt(500)*new Random().nextGaussian());
			
			//logger.debug("Sample: " + samples[i]);
		}
		
		return samples;
	}
	
	/**
	 * @return
	 * @throws IOException
	 * @throws SAXException
	 */
	private PredictionPolicyType getPredictionPolicy() throws IOException, SAXException {
		String predictionPolicyXML = XMLUtils.fileToString(PREDICTION_POLICY);
		String predictionPolicySchemaXML = XMLUtils.fileToString(PREDICTION_POLICY_SCHEMA);
		
		logger.debug(predictionPolicyXML);
		
		PredictionPolicyType predictionPolicy = PredictionPolicyParser.parsePredictionPolicy(predictionPolicyXML, predictionPolicySchemaXML);
		
		return predictionPolicy;
	}
}
