/**
 * 
 */
package test.uk.ac.city.soi.everestplus.core;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;

import uk.ac.city.soi.everestplus.core.FrameworkContext;
import uk.ac.city.soi.everestplus.core.FrameworkContext.DatabaseManagers;
import uk.ac.city.soi.everestplus.core.FrameworkContextInitialisationException;
import uk.ac.city.soi.everestplus.core.FrameworkContextManager;

/**
 * @author Davide Lorenzoli
 * 
 * @date Jun 11, 2011
 */
public class TestFrameworkContext extends TestCase {
	static {
		try {
			FrameworkContextManager.initialiseFrameworkContext();
			String frameworkConfigurationFolder = FrameworkContextManager.getFrameworkContext().getFrameworkConfigurationfolder();
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		} catch (FrameworkContextInitialisationException e) {
			e.printStackTrace();
		}
	}
	private static Logger logger = Logger.getLogger(TestFrameworkContext.class);

	private static final String QOS_ENTITY_MANAGER_PROPERTY = "everestplus.store.class.qos";
	
	private FrameworkContext frameworkContext;
	
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		FrameworkContextManager.initialiseFrameworkContext();
		frameworkContext = FrameworkContextManager.getFrameworkContext();
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	@Test
	public void testDatabaseManagers() {
		assertNotNull(frameworkContext);
		
		// checks whether FrameworkContext loaded all database managers
		for (DatabaseManagers databaseManager: FrameworkContext.DatabaseManagers.values()) {
			assertNotNull(frameworkContext.getDatabaseManager(databaseManager));
		}
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								INNER CLASSES
	// ------------------------------------------------------------------------
}
