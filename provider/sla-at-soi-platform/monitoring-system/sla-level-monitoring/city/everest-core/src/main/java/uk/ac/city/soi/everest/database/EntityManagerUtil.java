/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.database;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.rowset.serial.SerialBlob;

/**
 * This class provides some utilities for managing databases and connections. For instance, it provides a method to
 * convert Object to Blob and vice versa, execute generic query, or manage resource cleaning up. This class should be
 * extended by any specific entity manager, e.g., by EventEntityManager, TemplateEntityManager, etc.
 * 
 * @author Davide Lorenzoli
 * 
 * @date 17 Mar 2009
 */
public class EntityManagerUtil {
    protected Connection connection;

    /**
     * Constructor by using a connection field value as parameter.
     * 
     * @param connection
     */
    public EntityManagerUtil(Connection connection) {
        this.connection = connection;
    }

    // ------------------------------------------------------------------------
    // PROTECTED METHODS
    // ------------------------------------------------------------------------

    /**
     * Given an object, it return a Blob object containing the serialised object.
     * 
     * @param object
     * @return
     * @throws IOException
     * @throws SQLException
     */
    protected Blob toBlob(Object object) throws IOException, SQLException {
        // serialise the graph
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(object);
        byte serialisedGraph[] = baos.toByteArray();

        // create blob
        /*
         * Blob generation code compatible to jdk 1.6 Blob blob = connection.createBlob(); blob.setBytes(1,
         * serialisedGraph);
         */

        // Blob generation code compatible to jdk 1.5
        Blob blob = new SerialBlob(serialisedGraph);

        // close streams
        oos.close();
        baos.close();

        return blob;
    }

    /**
     * Given a Blob object containing an serialised LogEvent, it return an object.
     * 
     * @param blob
     * @return the LogEvent object
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    protected Object toObject(Blob blob) throws IOException, SQLException, ClassNotFoundException {
        // convert a binary stream into an object
        ObjectInputStream s = new ObjectInputStream(blob.getBinaryStream());
        Object object = (Object) s.readObject();
        s.close();

        return object;
    }

    /**
     * It is a good idea to release resources in a finally{} block in reverse-order of their creation if they are
     * no-longer needed.
     * 
     * @param pstmt
     * @param resultSet
     */
    protected void releaseResources(PreparedStatement pstmt, ResultSet resultSet) {
        // it is a good idea to release
        // resources in a finally{} block
        // in reverse-order of their creation
        // if they are no-longer needed

        if (resultSet != null) {
            try {
                resultSet.close();
            }
            catch (SQLException sqlEx) {
            } // ignore

            resultSet = null;
        }

        if (pstmt != null) {
            try {
                pstmt.close();
            }
            catch (SQLException sqlEx) {
            } // ignore

            pstmt = null;
        }
    }

    /**
     * Executes the SQL query and returns the ArrayList<Object> generated by the query.
     * 
     * @param query
     * @return a ResultSet object that contains the data produced by the query; never null
     * @throws SQLException
     */
    public ResultSet executeQuery(String query) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = connection.prepareStatement(query);
            resultSet = pstmt.executeQuery();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return resultSet;
    }

    /**
     * Executes the SQL statement, which must be an SQL Data Manipulation Language (DML) statement, such as INSERT,
     * UPDATE or DELETE; or an SQL statement that returns nothing, such as a DDL statement.
     * 
     * @param query
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or (2) 0 for SQL statements
     *         that return nothing
     * @throws SQLException
     */
    /*
    public int executeUpdate(String query) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result = 0;

        try {
            pstmt = connection.prepareStatement(query);

            result = pstmt.executeUpdate();
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return result;
    }
    */
}
