/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.database.fluent;

import java.util.ArrayList;

import uk.ac.city.soi.everest.core.Fluent;
import uk.ac.city.soi.everest.database.EntityManagerInterface;
import uk.ac.city.soi.everest.monitor.Predicate;

/**
 * Interface providing methods for managing FluentEntity.
 * 
 */
public interface FluentEntityManagerInterface extends EntityManagerInterface {

    /**
     * Retrieves the initiates records of the FluentEntity.
     * 
     * @return
     */
    public ArrayList<Predicate> getInitiates();

    /**
     * Adds an Initiate predicate to the storer.
     * 
     * @param initiate
     */
    public void addInitiate(Predicate initiate);

    /**
     * Adds an Terminate predicate to the storer.
     * 
     * @param terminate
     */
    public void addTerminate(Predicate terminate);

    /**
     * Finds a initiate predicate that was stored in the storer before a given time point and that contains a given
     * fluent.
     * 
     * @param time -
     *            a time point
     * @param fluent -
     *            A fluent
     * @return
     */
    public Predicate getInitiateBefore(long time, Fluent fluent);

    /**
     * Finds all the Initiates predicates that were stored in the storer before a given time point and contain a fluent
     * with given type, name and number of variables.
     * 
     * @param time -
     *            a time point
     * @param fluent -
     *            A fluent
     * @return
     * @autor t7t
     */
    public ArrayList<Predicate> getInitiatesBefore(long time, Fluent fluent);

    /**
     * Finds a Terminate predicate that was stored in the storer after a given time point and that contains a given
     * fluent.
     * 
     * @param time -
     *            A time point
     * @param fluent -
     *            A fluent
     * @return
     */
    public Predicate getTerminateAfter(long time, Fluent fluent);

    /**
     * Finds all the Terminates predicates that were stored in the storer after a given time point and contain a fluent
     * with given type, name and number of variables.
     * 
     * @param time -
     *            A time point
     * @param fluent -
     *            A fluent
     * @return
     * @author t7t
     */
    public ArrayList<Predicate> getTerminatesAfter(long time, Fluent fluent);

    /**
     * Finds all the Initiated fluents that were stored in the storer before a given time point and contain a fluent
     * with given type, name and number and type of variables.
     * 
     * @param time -
     *            A time point
     * @param fluent -
     *            A fluent
     * @return
     * @author t7t
     */
    public ArrayList<Predicate> getAllInitiatedFluents(long time, Fluent fluent);

    /**
     * At the given time, it checks whether there is an already initiated fluent that has the given fluent name in the
     * storer.
     * 
     * @param time -
     *            A time point
     * @param fluentName -
     *            Given fluent name
     * @return
     * @author t7t
     */
    public ArrayList<Predicate> checkForInitiatedFluents(long time, String fluentName);

    /**
     * Given the fluent name, it returns a collection of fluent argument values for the whole history of the fluent
     * (i.e., for all the given fluents initiations).
     * 
     * @param fluentName
     * @return an array of type Object, an array of size 0 if no fluent was found
     */
    public Object[] getByName(String fluentName);

    /**
     * Executes the given query and returns an array of type Object. Each array item is an array of type Object
     * representing a fluent state.
     * 
     * @param query
     * @return an array of type Object, an array of size 0 if no fluent was found
     */
    public Object[] getByQuery(String query);
}
