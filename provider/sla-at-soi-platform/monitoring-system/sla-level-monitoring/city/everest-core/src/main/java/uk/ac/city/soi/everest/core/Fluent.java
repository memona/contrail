/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * Fluent.java
 *
 * Created on 16 May 2004, 15:32
 */

package uk.ac.city.soi.everest.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Java object to model fluent of event calculus formula. In the latest implementation version, there is only one type
 * of generic fluents. A name should be specified for a fluent, while there is fluent type does not exist anymore. The
 * current version supports two types of fluent arguments, Variable and Function objects. The fluent arguments are
 * stored in the argument ArrayList which is specified as a generic collection to contain arbitrarily object types.
 * 
 */
public class Fluent implements Serializable {

    private String fluentName = "";
    private ArrayList arguments = new ArrayList();

    /**
     * Creates a new instance of Fluent. This constructor creates default fluent that is valueOf(var1, var2).
     */
    public Fluent(Variable var) {
        arguments.add(var.makeCopy());
    }

    /**
     * Copy constructor.
     */
    public Fluent(Fluent fluent) {
        this.fluentName = new String(fluent.getFluentName());

        for (int i = 0; i < fluent.getArguments().size(); i++) {
            Object argument = fluent.getArguments().get(i);
            if (argument instanceof Variable) {
                arguments.add(((Variable) argument).makeCopy());
            }
            else if (argument instanceof Function) {
                arguments.add(new Function((Function) argument));
            }
        }
    }

    /**
     * Creates a new fluent with the given relation name.
     * 
     * @param name -
     *            name of the relation in the fluent.
     */
    public Fluent(String name) {
        fluentName = name;
    }

    /**
     * Adds a variable or function to the fluent arguments.
     * 
     * @param var -
     *            variable to be added.
     */
    public void addArgument(Object obj) {
        if ((obj instanceof Variable) || (obj instanceof Function)) {
            arguments.add(obj);
        }
    }

    /**
     * Sets the name of the relation in fluent.
     * 
     * @param name -
     *            name of the relation in fluent
     */
    public void setName(String name) {
        fluentName = name;
    }

    /**
     * Returns the name of the relation in fluent.
     * 
     * @return
     */
    public String getFluentName() {
        return fluentName;
    }

    /**
     * Returns the variable in fluent.
     * 
     * @return
     */
    public Variable getVariable() {
        return (Variable) arguments.get(0);
    }

    /**
     * Returns a list of variables in a fluent relation.
     * 
     * @return
     */
    public ArrayList getArguments() {
        return arguments;
    }

    /**
     * Returns the function in fluent (if any).
     * 
     * @return
     */
    
    public Function getFunction() {
        Function func = null;
        if (arguments.size() > 1) {
            func = (Function) arguments.get(1);
        }
        return func;
    }
	
    /**
     * Matches this fluent with a given fluent. Fluent1 matches to Fluent2 if they both have same relation name and same
     * fluent type, and all the variables in Fluent1 and in Fluent2 have same variable name and type.
     * 
     * @param fluent -
     *            Fluent to be matched with this fluent
     */
    
    public boolean matches(Fluent fluent) {
        boolean val = true;
        if (fluent != null) {

            if (this.fluentName.equals(fluent.getFluentName()) && arguments.size() == fluent.getArguments().size()) {
                for (int i = 0; i < arguments.size(); i++) {
                    Object argument = arguments.get(i);
                    if (argument instanceof Variable) {
                        if (!((Variable) argument).matches((Variable) fluent.getArguments().get(i)))
                            ;
                        {
                            val = false;
                            break;
                        }
                    }
                    else if (argument instanceof Function) {
                        if (!((Function) argument).equals((Function) fluent.getArguments().get(i)))
                            ;
                        {
                            val = false;
                            break;
                        }

                    }
                }
            }
            else {
                val = false;
            }

        }
        return val;
    }
	
    /**
     * Returns true if a Fluent is equalt to this Fluent. Fluent1 is equal to Fluent2 if they both have same relation
     * name and same fluent type, and all the variables in Fluent1 and in Fluent2 have same variable name, same variable
     * type and same value.
     * 
     * @param fluent
     * @return
     */
    public boolean equals(Fluent fluent) {
        return this.getHashValue().equals(fluent.getHashValue());
    }

    /**
     * Returns true if the fluent is partially instantiated. A fluent is partially instantiated if some of its variables
     * have been initialized to some value, but some others are not.
     * 
     * @param ufp -
     *            Map of all the initialized variables in the template which this fluent belongs to.
     * @param timeVarMap -
     *            Map of all the initialized time variables in the template which this fluent belongs to.
     * @return
     */
    public boolean isPartial(HashMap ufp, HashMap timeVarMap) {
        boolean val = false;

        for (int i = 0; i < arguments.size(); i++) {
            Object obj = arguments.get(i);
            if (obj instanceof Variable) {
                Variable var = (Variable) arguments.get(i);
                if (var.getType().equals(Constants.DATA_TYPE_CONST)) {
                    continue;
                }
                else {
                    if (!ufp.containsKey(var.getName() + var.getType())) {
                        val = true;
                        break;
                    }
                }
            }
            else if (obj instanceof Function) {
                Function func = (Function) arguments.get(i);
                val = func.isPartial(ufp, timeVarMap);
            }
        }

        return val;
    }

    /**
     * Returns the string representation of the fluent. -- NEW GENERIC VERSION: Supports fluents that can contain
     * arbitrarily functions and variables as arguments.
     */
    public String toString() {
        String val = "";

        val += fluentName + "(";
        for (int i = 0; i < arguments.size(); i++) {
            Object obj = arguments.get(i);
            if (obj instanceof Variable) {
                Variable var = (Variable) obj;
                val += var.getName();
            }
            else if (obj instanceof Function) {
                Function func = (Function) obj;
                val += func.toString();
            }
            if (i < arguments.size() - 1) {
                val += ",";
            }
        }
        val += ")";

        return val;
    }

    /**
     * Returns a has uk.ac.city.soi.everest for this fluent. -- NEW GENERIC VERSION: Supports fluents that can contain
     * arbitrarily functions and variables as arguments.
     * 
     * @return
     */
    public String getHashCode() {
        String val = "";

        val += fluentName;
        for (int i = 0; i < arguments.size(); i++) {
            Object obj = arguments.get(i);
            if (obj instanceof Variable) {
                Variable var = (Variable) obj;
                val += var.getHashCode();
            }
            else if (obj instanceof Function) {
                Function func = (Function) obj;
                val += func.getHashCode();
            }
        }
        return val;
    }

    /**
     * Returns a hash value for this fluent. -- NEW GENERIC VERSION: Supports fluents that can contain arbitrarily
     * functions and variables as arguments.
     * 
     * @return
     */
    public String getHashValue() {
        String val = "";

        val += fluentName;
        for (int i = 0; i < arguments.size(); i++) {
            Object obj = arguments.get(i);
            if (obj instanceof Variable) {
                Variable var = (Variable) obj;
                val += var.getHashValue();
            }
            else if (obj instanceof Function) {
                Function func = (Function) obj;
                val += func.getHashValue();
            }
        }
        return val;
    }

}
