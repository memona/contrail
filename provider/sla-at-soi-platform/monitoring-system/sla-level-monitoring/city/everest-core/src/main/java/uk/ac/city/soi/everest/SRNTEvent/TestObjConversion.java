/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.SRNTEvent;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import uk.ac.city.soi.everest.core.Constants;

/**
 * Class for checking object conversions.
 * 
 * @author Costas Ballas
 * 
 */
public class TestObjConversion {
    // logger
    //private static Logger logger = Logger.getLogger(TestObjConversion.class);

    /**
     * @param args
     */
    //static String pkg = Constants.PKG;

    /**
     * Makes an object convertion according to given ArgumentType and package.
     * 
     * @param arg
     * @param pkg
     * @return
     * @throws Exception
     */
    /*
    public static Object Translate(ArgumentType arg, String pkg) throws Exception {
        // logger.debug(arg.getName()+" "+arg.getType());
        Object returnObj = null;

        if (arg.struct == null) {
            // simple type argument
            if (arg.getValue() == null) {
                return null;
            }
            else {
                if (arg.getType().equals("string")) {
                    Element e = (Element) arg.getValue();
                    return new String(e.getTextContent());
                }
                else if (arg.getType().equals("int")) {
                    Element e = (Element) arg.getValue();
                    return new Integer(e.getTextContent());
                }
                else if (arg.getType().equals("double")) {
                    Element e = (Element) arg.getValue();
                    return new Double(e.getTextContent());
                }
                else if (arg.getType().equals("long")) {
                    Element e = (Element) arg.getValue();
                    return new Long(e.getTextContent());
                }
                else {
                    throw (new Exception("BadType"));
                }
            }
        }
        else {
            try {
                // object argument
                // The dir contains the compiled classes.
                File classesDir = new File(".");
                // The parent classloader
                ClassLoader parentLoader = TestObjConversion.class.getClassLoader();
                // Load class "sample.PostmanImpl" with our own classloader.
                URLClassLoader loader1 = new URLClassLoader(new URL[] { classesDir.toURL() }, parentLoader);
                Class c = loader1.loadClass(pkg + "." + arg.getType());
                // Postman postman1 = (Postman) cls1.newInstance();
                // Class c = Class.forName("uk.ac.city.soi.everest.datatypes."+arg.getType());
                Object o = c.newInstance();
                for (int i = 0; i < arg.getStruct().getArgument().size(); i++) {
                    Object fieldValue = Translate(arg.getStruct().getArgument().get(i), pkg);
                    String fieldName = arg.getStruct().getArgument().get(i).getName();
                    String fieldType = arg.getStruct().getArgument().get(i).getType();

                    Field f = c.getDeclaredField(fieldName);
                    Method m =
                            c.getMethod("set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1), f
                                    .getType());
                    Object[] arrayObj = new Object[1];
                    arrayObj[0] = fieldValue;
                    m.invoke(o, arrayObj);
                }
                returnObj = o;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        return returnObj;
    }
    
    */
}
