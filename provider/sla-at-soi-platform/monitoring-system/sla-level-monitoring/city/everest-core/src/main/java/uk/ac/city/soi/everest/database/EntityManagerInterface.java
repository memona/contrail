/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.database;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * This interface provides the basic set of methods for manipulating database entities. This interface should be
 * extended to add more functionalities for customised entities managing.
 * 
 * @author Davide Lorenzoli
 * 
 * @date 12 May 2009
 */
public interface EntityManagerInterface {

    /**
     * Insert the object into the database if the object doesn't exist, otherwise no action will be taken.
     * 
     * @param object
     */
    public void insert(Object object);

    /**
     * Update an already existing object, if the object doens't exist no action will be taken.
     * 
     * @param object
     */
    public void update(Object object);

    /**
     * Delete an object from the database.
     * 
     * @param object
     */
    public void delete(Object object);

    /**
     * Delete from the database the object corresponding to the given id.
     * 
     * @param id
     */
    public void delete(String entityId);

    /**
     * Select the object matching with the given id.
     * 
     * @param id
     * @return
     */
    public Object select(String entityId);

    /**
     * This method should provide an optimised implementation, not merely just an implementation just like, e.g., return
     * getAll().size().
     * 
     * @return the number of records in the database
     */
    public int count();

    /**
     * Delete all table records.
     */
    public void deleteAll();

    /**
     * Query for all record set contained into the database table.
     * 
     * @return an ArrayList<Object> of all table's record set, never null
     * @throws SQLException
     */
    public ArrayList<Object> selectAll();
}
