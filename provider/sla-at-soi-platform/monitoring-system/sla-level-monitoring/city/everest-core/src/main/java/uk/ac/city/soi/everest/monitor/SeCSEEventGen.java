/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * SeCSEEventGen.java
 *
 * Created on 23 September 2005, 15:18
 */

package uk.ac.city.soi.everest.monitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Function;
import uk.ac.city.soi.everest.core.TimeVar;
import uk.ac.city.soi.everest.core.Variable;
import uk.ac.city.soi.everest.database.fluent.FluentEntityManagerInterface;
import uk.ac.city.soi.everest.database.template.TemplateEntityManagerInterface;

/**
 * This is the class that generates and handles derived events.
 * 
 * @author am697
 */
public class SeCSEEventGen extends Observable implements Observer {
    // logger
    private static Logger logger = Logger.getLogger(SeCSEEventGen.class);

    private TemplateEntityManagerInterface templateStorer;
    private FluentEntityManagerInterface fluentStorer;
    private boolean goOn;
    private ArrayList tInfos;

    /**
     * Creates a new instance of SeCSEEventGen.
     */
    public SeCSEEventGen(TemplateEntityManagerInterface templateStorer, FluentEntityManagerInterface fluentStorer) {
        tInfos = new ArrayList();
        this.templateStorer = templateStorer;
        this.fluentStorer = fluentStorer;
    }

    /**
     * Returns the intersection of two time ranges, namely (t1, t2) and (t3, t4). The return value is it self a time
     * range (modelled by Range.java class). The isWithin field of teh returned Range object is set to true, if (t3, t4)
     * is within (t1, t2).
     * 
     * @param t1
     * @param t2
     * @param t3
     * @param t4
     * @return
     */
    /*
    private Range getIntersection(long t1, long t2, long t3, long t4) {
        Range range = null;
        if (Math.max(t1, t3) <= Math.min(t2, t4)) {
            range = new Range(Math.max(t1, t3), Math.min(t2, t4));
            range.setWithin((t1 >= t3 && t2 <= t4) ? true : false);
        }
        return range;
    }
    */

    /**
     * This is the method that generates and handles derived events.
     * 
     */
    private void mainLoop() {
        logger.debug("%%%%%%%%%%%%%%%%%%%%%%%%%\n^^^^^^^^^^^^^^^^^^\ntest1 DerEvGEn mainloop");
        long dEventTime2 = 0;
        if (tInfos.size() > 0) {
            logger.debug("%%%%%%%%%%%%%%%%%%%%%%%%%\n^^^^^^^^^^^^^^^^^^\ntest2 tInfos");
            TemplateInfo tInfo = (TemplateInfo) tInfos.get(0);
            Template template = tInfo.getTemplate();
            long time = tInfo.getTime();
            tInfos.remove(0);
            if (!template.isForChecking() && template.isBodyTrue()) {
                logger.debug("%%%%%%%%%%%%%%%%%%%%%%%%%\n^^^^^^^^^^^^^^^^^^\ntest3 Mixed");
                for (int j = template.bodySize(); j < template.totalPredicates(); j++) {
                    Predicate pred = template.getPredicate(j);
                    HashMap ufp = template.getProjection(pred);

                    if (pred.getEcName().equals(Constants.PRED_INITIATES)
                            || pred.getEcName().equals(Constants.PRED_TERMINATES)) {
                        logger.debug("%%%%%%%%%%%%%%%%%%%%%%%%%\n^^^^^^^^^^^^^^^^^^\ntest4 Initiates");
                        logger.debug(pred.getTruthVal());
                        logger.debug(pred.getTimeVarName());
                        if (!pred.isPartial(ufp, template.getTimeVarMap())
                                && pred.getTruthVal().equals(Constants.TRUTH_VAL_UK)
                                && !pred.getFluent().isPartial(ufp, template.getTimeVarMap())) {
                            logger.debug("%%%%%%%%%%%%%%%%%%%%%%%%%\n^^^^^^^^^^^^^^^^^^\ntest5 Initiates truth val");
                            Predicate tP = new Predicate(pred);                            
                            ArrayList fVars = new ArrayList();
                            ArrayList initiatedFluentVars = new ArrayList();
                            fVars.addAll(tP.getFluent().getArguments());
                            ArrayList<Predicate> initiatedFluents = fluentStorer.checkForInitiatedFluents(0, tP.getFluent().getFluentName());
                            if(initiatedFluents.size() > 0){
                            	Predicate initiatedFluent = initiatedFluents.get(0);
                            	initiatedFluentVars = initiatedFluent.getFluent().getArguments();
                            }
                            tP.getFluent().getArguments().clear();
                            
                            // for the time being we are presuming that time variables
                            // are not used in the fluent
                            for (int i = 0; i < fVars.size(); i++) {
                                Object obj = fVars.get(i);
                                if (obj instanceof Variable) {
                                    Variable fVar = (Variable) obj;
                                    if (fVar.getType().equals(Constants.DATA_TYPE_CONST))
                                        tP.getFluent().addArgument(new Variable(fVar));
                                    else {
                                        Variable rVar = (Variable) ufp.get(fVar.getName() + fVar.getType());
                                        tP.getFluent().addArgument(new Variable(rVar));
                                    }
                                }
                                else if (obj instanceof Function) {
                                    Function func = (Function) obj;
                                    Variable funcResult = func.execute(ufp, template.getTimeVarMap());
                                    if(initiatedFluentVars.size() > 0 && initiatedFluentVars.get(i) != null){
                                    	Object ifo = initiatedFluentVars.get(i);
                                    	if(ifo instanceof Variable){
                                    		Variable ifv = (Variable)ifo;
                                    		funcResult.setName(ifv.getName());
                                    		funcResult.setType(ifv.getType());
                                    	}
                                    	//else funcResult.setName(tP.getFluent().getFluentName()); 
                                    }
                                    //else funcResult.setName(tP.getFluent().getFluentName());
                                    tP.getFluent().addArgument(new Variable(funcResult));
                                    logger
                                    .debug("%%%%%%%%%%%%%%%%%%%%%%%%%\n^^^^^^^^^^^^^^^^^^\ntest5.5 add variable to initiate " + funcResult.getObjValue() + " for the variable " + funcResult.getName());                                    
                                }
                            }
                            
                            if(tP.getTimeVarName().contains(Constants.ARTIFICIAL_TIME_VARIABLE) && tP.isRangeSet()){
                            	tP.setTime(tP.getUpBound().getValue());
                            }
                            
                            if (tP.getTime() == Constants.TIME_UD) {
                                // search at time variables map
                                TimeVar tVar = template.getTimeVarMap().get(tP.getTimeVarName());
                                if (tVar.getValue() != (Constants.TIME_UD)) {
                                    tP.setTime(tVar.getValue());
                                }
                                // otherwise search at unifiers where time variables are stored as long
                                else {
                                    Variable tVarUnifiers =
                                            template.getUnifiers().get(tP.getTimeVarName() + Constants.DATA_TYPE_LONG);
                                    if (tVarUnifiers != null) {
                                        if (tVarUnifiers.getObjectValue() instanceof Long) {
                                            long val = (Long) tVarUnifiers.getObjectValue();
                                            tP.setTime(val);
                                        }
                                    }
                                }
                            }
                            if (pred.getEcName().equals(Constants.PRED_INITIATES)) {
                                logger
                                        .debug("%%%%%%%%%%%%%%%%%%%%%%%%%\n^^^^^^^^^^^^^^^^^^\ntest6 add initiates in fluentStorer. Fluent name " + tP.getFluent().getFluentName() + " and time " + tP.getTime());
                                fluentStorer.addInitiate(tP);
                            }
                            else{
                            	logger
                                .debug("%%%%%%%%%%%%%%%%%%%%%%%%%\n^^^^^^^^^^^^^^^^^^\ntest6 add terminates in fluentStorer. Fluent name " + tP.getFluent().getFluentName() + " and time " + tP.getTime());                            	
                                fluentStorer.addTerminate(tP);
                            }
                            pred.setTruthVal(Constants.TRUTH_VAL_TRUE);
                            pred.setID("DE_ID");
                            template.setRemovable(true);
                        }
                    }
                }// end for j (each predicate)
            } // end of template.isBodyTrue()
        } // end of tInfos.size() > 0
    }

    /**
     * Updates the observers for a given TemplateInfo object.
     * 
     * @param tInfo
     */
    public void update(TemplateInfo tInfo) {
        tInfos.add(tInfo);
        mainLoop();
    }

    /**
     * Receives a template instance and the current monitoring time wrapped in TemplateInfo object, then call the main
     * loop to check if event should be derived.
     * 
     * @param tInfo
     */
    
    public void update(Observable o, Object arg) {
        //tInfos.add((TemplateInfo) arg);
        //mainLoop();
    }
    
}
