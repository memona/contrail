/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.SRNTEvent;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringBufferInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Element;

/**
 * Represents Java object for Serenity event.
 * 
 * @author Costas Ballas
 * 
 */
public class SRNTEvent {

    private TypeOfEvent event;

    /**
     * Constructor for given string event.
     * 
     * @param event
     */
    /*
    public SRNTEvent(String event) {
        try {
            JAXBContext jc = JAXBContext.newInstance("uk.ac.city.soi.everest.SRNTEvent");
            Unmarshaller u = jc.createUnmarshaller();
            this.event = (TypeOfEvent) ((JAXBElement) u.unmarshal(new StringBufferInputStream(event))).getValue();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
	*/
    /**
     * Constructor for given TypeOfEvent object.
     * 
     * @param event
     */
    public SRNTEvent(TypeOfEvent event) {

        this.event = event;

    }

    /**
     * Returns the sender specified for the event.
     * 
     * @return
     */
    public Entity getSender() {
        return this.event.getMessageArgs().getSender();
    }

    /**
     * Returns the receiver specified for the event.
     * 
     * @return
     */
    public Entity getReceiver() {
        return this.event.getMessageArgs().getReceiver();
    }

    /**
     * Returns the Source specified for the event.
     * 
     * @return
     */
    public Entity getSource() {
        return this.event.getMessageArgs().getContext().getEventSource();
    }

    /**
     * Returns the operation name specified for the event.
     * 
     * @return
     */
    public String getOperationName() {
        return this.event.getType().getOperationMessage().getOperationName();
    }

    /**
     * Returns the OperationID specified for the event.
     * 
     * @return
     */
    public long getOperationID() {
        return this.event.getType().getOperationMessage().getOperationID();
    }

    /**
     * Returns the OperationStatus specified for the event.
     * 
     * @return
     */
    public String getOperationStatus() {
        return this.event.getType().getOperationMessage().getStatus();
    }

    /**
     * Returns the getOperationArguments specified for the event.
     * 
     * @return
     */
    public ArgumentsType getOperationArguments() {
        return this.event.getType().getOperationMessage().getOpArgs();
    }

    /**
     * Returns the length of getOperationArguments specified for the event.
     * 
     * @return
     */
    public int getOperationArgumentsLenght() {
        return this.event.getType().getOperationMessage().getOpArgs().getArgument().size();
    }

    /**
     * Returns the event time stamp.
     * 
     * @return
     */
    public long getTimestamp() {
        return this.event.getMessageArgs().getContext().getTimestamp();
    }

    /**
     * Returns the event id.
     * 
     * @return
     */
    public String getID() {
        return this.event.getType().getOperationMessage().getOperationName()
                + this.event.getType().getOperationMessage().getOperationID();
    }
}
