/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.monitor.simplex;

import org.apache.log4j.Logger;

/**
 * This class implements the Simplex linear constraint algorithm.
 *
 */
public class Simplex {
    // logger
    private static Logger logger = Logger.getLogger(Simplex.class);

    public int numVariables;
    public int numConstraints;
    public int numNonbasic;
    public int numConstraintsSoFar = 0;
    public int CurrentStep = 0;
    public int NumIterations = 0;

    private int i, j, k, l; /* Index Variables */

    public float[] reducedCost;
    public float[] cost;
    public float[] x;
    public float[] pi;
    public float[] yB;

    public float MinRatio;
    public int NumMinRatio;

    public Matrix Bt;
    public Matrix B;

    /* littleCost is the cost of the BasicVariables */
    public float[] littleCost;
    public float objectiveValue = 0;

    public float[][] A;
    public float[] b;
    public int[] constraintType;
    public int[] BasicVariables;
    public int[] NonBasicVariables;
    public int[] varType;
    public float[] colOfA;

    public int EnteringVariable;
    public int LeavingVariable;

    public boolean TypeMinimize;
    public boolean artificialPresent = false;

    public final int LessThan = 0;
    public final int GreaterThan = 1;
    public final int EqualTo = 2;

    public final int Continue = 0;
    public final int Optimal = 1;
    public final int Feasible = 2;
    public final int Unbounded = 3;

    public final int Regular = 0;
    public final int SlackOrSurplus = 1;
    public final int Artificial = 2;

    /* variables for two phase part */
    boolean oldOptType;
    float OriginalCost[];

    /**
     * Constructor for given number of variables and constraints.
     * 
     * @param numVariables
     * @param numConstraints
     */
    public Simplex(int numVariables, int numConstraints) {
        this.numVariables = numVariables;
        this.numConstraints = numConstraints;
        reducedCost = new float[numVariables + 2 * numConstraints];
        cost = new float[numVariables + 2 * numConstraints];
        OriginalCost = new float[numVariables + 2 * numConstraints];
        x = new float[numConstraints];
        pi = new float[numConstraints];
        yB = new float[numConstraints];
        littleCost = new float[numConstraints];
        A = new float[numConstraints][numVariables + 3 * numConstraints];
        b = new float[numConstraints];
        constraintType = new int[numConstraints];
        BasicVariables = new int[numConstraints];
        NonBasicVariables = new int[numVariables + 2 * numConstraints];
        varType = new int[numVariables + 2 * numConstraints];
        colOfA = new float[numConstraints];
        Bt = new Matrix(numConstraints);
        Bt.toString();
        B = new Matrix(numConstraints);
        B.toString();
    } /* end revisedSimplex procedure */

    /**
     * Performs all the steps of one iteration.
     * 
     * @return
     */
    public int iterate() {
        /* Perform all the steps of one iteration */

        NumIterations++;
        this.makeBt();
        Bt.solve(pi, littleCost);
        this.calculateReducedCosts();

        if (!this.testForOptimality()) {
            this.ChooseEnteringVariable();
        }
        else {
            objectiveValue = this.calculateObjective();
            return Optimal; /* We found the optimal solution!! */
        }
        this.makeB();

        /* make the column of A what we want */
        for (int i = 0; i < numConstraints; i++)
            colOfA[i] = A[i][NonBasicVariables[EnteringVariable]];

        B.solve(yB, colOfA);

        if (!this.testUnboundedness()) {
            this.chooseLeavingVariable();
            this.updateSolution();
            this.changeBasis();

            return Continue; /* We can keep going/! */
        }
        else
            return Unbounded; /* The problem is unbounded. */

    } /* end iterate procedure */

    /**
     * Calculates the objective and returns it.
     * 
     * @return
     */
    public float calculateObjective() {
        float value = 0;

        if (TypeMinimize == true)
            for (int i = 0; i < numConstraints; i++)
                value += (x[i] * cost[BasicVariables[i]]);
        else
            for (int i = 0; i < numConstraints; i++)
                value -= (x[i] * cost[BasicVariables[i]]);

        return value;
    } /* end calculateObjective procedure */

    /**
     * Chooses the variable that should leave.
     */
    public void chooseLeavingVariable() {
        float Ratio;
        int minIndex = -1;

        NumMinRatio = 0;
        MinRatio = 100000000000000F;

        for (i = 0; i < numConstraints; i++) {
            if (yB[i] > 0) {
                Ratio = x[i] / yB[i];
                if (Ratio < MinRatio) {
                    MinRatio = Ratio;
                    minIndex = i;
                    NumMinRatio = 1;
                }
                else if (Ratio == MinRatio)
                    NumMinRatio++;
            }
        }

        LeavingVariable = minIndex;

    } /* end chooseLeavingVariable procedure */

    /**
     * Updates the solution.
     */
    public void updateSolution() {
        for (i = 0; i < numConstraints; i++)
            x[i] -= (MinRatio * yB[i]);

        x[LeavingVariable] = MinRatio;

    } /* end updateSolution procedure */

    /**
     * Changes basis.
     */
    public void changeBasis() {
        int tmp;

        tmp = BasicVariables[LeavingVariable];

        BasicVariables[LeavingVariable] = NonBasicVariables[EnteringVariable];

        NonBasicVariables[EnteringVariable] = tmp;

    } /* end changeBasis procedure */

    /**
     * Chooses the variable to enter.
     */
    public void ChooseEnteringVariable() {
        int minIndex = 0;
        float minValue = 100000;

        /*
         * Actually any variable with a negative reduced cost will work. We choose the variable with lowest reduced cost
         * to enter.
         */

        for (i = 0; i < numNonbasic; i++)
            if (reducedCost[i] < 0 && reducedCost[i] < minValue) {
                minIndex = i;
                minValue = reducedCost[i];
            }
        EnteringVariable = minIndex;
    } /* end pickEnteringVariable procedure */

    /**
     * Checks whether the given problem in unbounded.
     * 
     * @return
     */
    public boolean testUnboundedness() {
        boolean isUnbounded = true;

        /* Problem is unbounded if yB > 0 in all elements */

        for (i = 0; i < numConstraints; i++)
            if (yB[i] > 0) {
                isUnbounded = false;
                break;
            }
        return isUnbounded;
    } /* end testUnboundedness procedure */

    /**
     * Calculates reducesd costs.
     */
    public void calculateReducedCosts() {
        for (i = 0; i < numNonbasic; i++) {
            for (j = 0; j < numConstraints; j++)
                colOfA[j] = A[j][NonBasicVariables[i]];

            reducedCost[i] = cost[NonBasicVariables[i]] - this.Dot(pi, colOfA, numConstraints);
        }
    } /* end calculateReducedCosts procedure */

    /**
     * Tests whether exist optimal solution.
     * 
     * @return
     */
    public boolean testForOptimality() {
        boolean isOptimal = true;

        for (int i = 0; i < numNonbasic; i++)
            if (reducedCost[i] < 0) {
                isOptimal = false;
                return isOptimal; /* false */
            }

        return isOptimal; /* true */
    } /* end testForOptimality procedure */

    /**
     * 
     */
    private void makeBt() {
        for (i = 0; i < numConstraints; i++) {
            littleCost[i] = cost[BasicVariables[i]];

            for (j = 0; j < numConstraints; j++)
                Bt.A[i][j] = A[j][BasicVariables[i]];
        }
    } /* end makeBt procedure */

    /**
     * 
     */
    private void makeB() {
        for (i = 0; i < numConstraints; i++)
            for (j = 0; j < numConstraints; j++)
                B.A[i][j] = A[i][BasicVariables[j]];
    } /* end makeB procedure */

    /**
     * Adds constraints.
     * 
     * @param coefficients
     * @param rhs
     * @param type
     */
    public void addConstraint(float[] coefficients, float rhs, int type) {
        for (i = 0; i < numVariables; i++) {
            A[numConstraintsSoFar][i] = coefficients[i];
        }
        x[numConstraintsSoFar] = rhs;
        b[numConstraintsSoFar] = rhs;

        constraintType[numConstraintsSoFar] = type;
        numConstraintsSoFar++;
    } /* end addConstraint procedure */

    /**
     * Specifies the objective for the given problem.
     * 
     * @param coefficients
     * @param type
     */
    public void specifyObjective(float[] coefficients, boolean type) {
        for (i = 0; i < numVariables; i++)
            cost[i] = coefficients[i];

        TypeMinimize = type;
    } /* end specifyObjective procedure */

    /**
     * Preprocess method.
     * 
     * @param numberOfVariables
     * @param numberOfConstraints
     * @return
     */
    public boolean preprocess(int numberOfVariables, int numberOfConstraints) {
        int LastCol;
        int[] ConstraintVariable = new int[numberOfConstraints];

        int slack;
        int surplus;

        oldOptType = TypeMinimize;
        LastCol = numberOfVariables;

        if (TypeMinimize == false) /* switch sign for maximize */
            for (i = 0; i < LastCol; i++)
                cost[i] *= -1;

        for (i = 0; i < LastCol; i++)
            NonBasicVariables[i] = i;

        /*
         * Add slacks and surplus first, this is slower but it makes displaying a lot easier for two phase problems.
         * Keep track of which variable was added for each constraint.
         */

        for (i = 0; i < numberOfConstraints; i++)
            switch (constraintType[i]) {
            case LessThan:
                A[i][LastCol] = 1;
                cost[LastCol] = 0;
                varType[LastCol] = SlackOrSurplus;
                ConstraintVariable[i] = LastCol;
                LastCol++;
                break;

            case GreaterThan:
                A[i][LastCol] = -1;
                cost[LastCol] = 0;
                varType[LastCol] = SlackOrSurplus;
                ConstraintVariable[i] = LastCol;
                LastCol++;
                break;

            case EqualTo: /* do nothing */
            } /* end switch */

        /* Add artificial variables if necessary, Assign basis */

        for (i = 0; i < numberOfConstraints; i++)
            switch (constraintType[i]) {
            case GreaterThan:
                if (b[i] > 0) {
                    /* Add artificial variable, make basic */
                    A[i][LastCol] = 1;
                    x[i] = b[i];
                    varType[LastCol] = Artificial;
                    artificialPresent = true;
                    BasicVariables[i] = LastCol;
                    /* surplus not basic */
                    surplus = ConstraintVariable[i];
                    NonBasicVariables[numberOfVariables + i] = surplus;
                    /* increment counter */
                    LastCol++;
                }
                else {
                    /* make surplus variable basic */
                    BasicVariables[i] = ConstraintVariable[i];
                    x[i] = -b[i];
                }
                break;

            case EqualTo: /* add artificial variable, make basic */
                if (b[i] >= 0) {
                    A[i][LastCol] = 1;
                    x[i] = b[i];
                }
                else { /* b[i] < 0 */
                    A[i][LastCol] = -1;
                    x[i] = -b[i];
                }

                varType[LastCol] = Artificial;
                artificialPresent = true;
                BasicVariables[i] = LastCol;
                LastCol++;
                break;

            case LessThan:
                if (b[i] >= 0) {
                    /* make slack variable basic */
                    BasicVariables[i] = ConstraintVariable[i];
                    x[i] = b[i];
                }
                else { /* b[i] < 0 */
                    /* add artificial variable, make basic */
                    A[i][LastCol] = -1;
                    x[i] = -b[i];
                    varType[LastCol] = Artificial;
                    artificialPresent = true;
                    BasicVariables[i] = LastCol;
                    /* slack not basic */
                    slack = ConstraintVariable[i];
                    NonBasicVariables[numberOfVariables + i] = slack;
                    /* increment counter */
                    LastCol++;
                }
                break;
            }

        numNonbasic = LastCol - numConstraints;
        numVariables = LastCol;

        if (artificialPresent == true)
            this.SetCostForPhaseOne();

        return true;
    } /* end preprocess procedure */

    /**
     * SetCostForPhaseOne sets feasibility cost function. Run getRidOfArtificials after the problem has been solved.
     */
    public void SetCostForPhaseOne() {
        float newCoefficients[] = new float[numVariables];

        /* Set the coefficients of the objective */
        for (int i = 0; i < numVariables; i++) {
            OriginalCost[i] = cost[i];

            if (varType[i] == Artificial)
                newCoefficients[i] = 1;
            else
                newCoefficients[i] = 0;
        }
        this.specifyObjective(newCoefficients, true); /* minimize */
    } /* end SetCostForPhaseOne procedure */

    /**
     * Removes artificial variables.
     */
    public void getRidOfArtificials() {
        int NumArtificials = 0;
        int LastBasic = 0;
        int LastNonBasic = 0;

        int[] BasisType = new int[numVariables];
        float[] TempX = new float[numVariables];

        int BasicType = 0;
        int NonBasicType = 1;

        /* Build index of variable type */

        for (int i = 0; i < numNonbasic; i++)
            BasisType[NonBasicVariables[i]] = NonBasicType;

        for (int i = 0; i < numConstraints; i++) {
            BasisType[BasicVariables[i]] = BasicType;
            TempX[BasicVariables[i]] = x[i];
        }

        /**
         * Reconstruct NonBasicVariables and BasicVariables without artificials.
         */

        for (int i = 0; i < numVariables; i++)
            if (varType[i] != Artificial) {
                if (BasisType[i] == BasicType) {
                    BasicVariables[LastBasic] = i;
                    x[LastBasic] = TempX[i];
                    LastBasic++;
                }

                if (BasisType[i] == NonBasicType) {
                    NonBasicVariables[LastNonBasic] = i;
                    LastNonBasic++;
                }
            }
            else
                NumArtificials++;

        /* this.showInfo(); */

        this.specifyObjective(OriginalCost, oldOptType);

        numNonbasic -= NumArtificials;
        numVariables -= NumArtificials;

        /* All the artificial variables have been eliminated */
        this.artificialPresent = false;

        /* reset TypeMinimize */
        this.TypeMinimize = oldOptType;

        CurrentStep = 0;

    } /* end getRidOfArtificials procedure */

    /**
     * @param row
     * @param col
     * @param size
     * @return
     */
    private float Dot(float[] row, float[] col, int size) {
        float result = 0;
        for (int i = 0; i < size; i++)
            result += row[i] * col[i];
        return result;
    } /* end Dot procedure */


} /* end revisedSimplex class */
