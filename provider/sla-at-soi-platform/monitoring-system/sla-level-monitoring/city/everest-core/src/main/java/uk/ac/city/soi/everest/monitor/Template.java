/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * Template.java
 *
 * Created on 01 June 2004, 16:03
 */
package uk.ac.city.soi.everest.monitor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.bse.Formula;
import uk.ac.city.soi.everest.bse.TimePredicate;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.Fluent;
import uk.ac.city.soi.everest.core.Function;
import uk.ac.city.soi.everest.core.TimeExpression;
import uk.ac.city.soi.everest.core.TimeVar;
import uk.ac.city.soi.everest.core.Variable;
import uk.ac.city.soi.everest.er.LogEvent;

/**
 * This class is used to model template (i.e. run time instance of a formula).
 * 
 * @author am697
 */
public class Template implements Serializable {
    // logger
    private static Logger logger = Logger.getLogger(Template.class);

    /*
     * Id of the template. The template id has the following structure: formulaId*[$eventId], i.e., the formulaId
     * followed by the sequence of eventId (unified with the template) separated by the $ symbol
     */
    private String templateId;
    /*
     * Type of the formula, e.g. future or past
     */
    private String type;
    /*
     * Holds the monitoring status of the template. This is set when truth values of all predicates are set
     */
    private String status;
    /*
     * This field is set to true if truth value of at least one predicate is set.
     */
    private boolean active;
    /*
     * This field is set to true, if the SeCSEAnalyzer or SeCSEEventGen updates at least one field of the template
     */
    private boolean updated;
    /*
     * This field is set to true by the SeCSEAnalyzer or SeCSEEventGen, if it is not possible to make a monitoring
     * decision from this template
     */
    private boolean removable;
    /*
     * List of links that specify which other templates are dependent on this template
     */
    //private ArrayList<Link> links;
    /*
     * Holds the predicates in the body of the template
     */
    private ArrayList<Predicate> body;
    /*
     * Holds the predicates in the head of the template
     */
    private ArrayList<Predicate> head;
    /*
     * holds the unification of variables of this template
     */
    private HashMap<String, Variable> uf;
    /*
     * holds the unification of variables of the fluent of HoldsAt predicates only of this template
     */
    private HashMap<String, Variable> huf;
    /*
     * holds the unification of the time variables of this template
     */
    private HashMap<String, TimeVar> timeVarMap;
    /*
     * Keeps track of the number of copies of this formula
     */
    private int copyCounter;
    /*
     * This field is set to true, if the formula is for checking. There are some formulas used only for deduction
     */
    private boolean forChecking;
    /*
     * This field is set to true, if diagnosis is required for the formula
     */
    private boolean diagnosisRequired;
    /*
     * This field is set to true, if threat detection is required for the formula
     */
    private boolean threatDetectionRequired;

    /*
     * Holds the time when consistency checker starts to make a decision for this template. (Used only for evaluation,
     * not directly needed for the functioning of the monitor).
     */
    private long DST;
    /*
     * Holds the time when consistency checker finishes to make a decision for this template. (Used only for evaluation,
     * not directly needed for the functioning of the monitor)
     */
    private long DET;
    /*
     * Holds the current time of the monitor (Used only for evaluation, not directly needed for the functioning of the
     * monitor)
     */
    private long MCT;
    /*
     * Holds the time of the event that was used to set the truth value of the last predicate of this template. (Used
     * only for evaluation, not directly needed for the functioning of the monitor)
     */
    private long EITLast;

    /*
     * Holds the threat likelihood estimated by the threat detection tool
     */
    private double threatLikelihood = Float.NaN;

    /*
     * List of constrained predicated subformulas
     */
    private ArrayList<ConstrainedPredicatesSubformula> constrainedPredicatesSubformulasList =
            new ArrayList<ConstrainedPredicatesSubformula>();

    /**
     * Creates a new instance of new template with a given formula ID and formula type.
     * 
     * @param formulaId
     * @param type
     *            can be <code>Constants.TYPE_PAST</code> or <code>Constants.TYPE_FUTURE</code>
     */
    /*
    public Template(String formulaId, String type) {
        this.type = new String(type);
        active = false;
        updated = false;
        removable = false;
        forChecking = true;
        diagnosisRequired = false;
        threatDetectionRequired = false;
        status = Constants.STATUS_UD;
        //links = new ArrayList<Link>();
        body = new ArrayList<Predicate>();
        head = new ArrayList<Predicate>();
        uf = new HashMap<String, Variable>();
        huf = new HashMap<String, Variable>();
        timeVarMap = new HashMap<String, TimeVar>();
        // ancestor = "";
        // descendants = new ArrayList();
        this.templateId = new String(formulaId);
        // timeInitiated = false;
        copyCounter = 1;
        // truthValCounter = 0;
        // setFormulaId(templateId + "-R" + copyCounter);
        DST = Constants.TIME_UD;
        DET = Constants.TIME_UD;
        MCT = Constants.TIME_UD;
        EITLast = Constants.TIME_UD;
        // nextPredNo = 1;
        constrainedPredicatesSubformulasList = new ArrayList<ConstrainedPredicatesSubformula>();
    }
	*/
    /**
     * Copy constructor.
     */
    public Template(Template template) {
        this.templateId = new String(template.getTemplateId());
        this.type = new String(template.getType());
        this.status = new String(template.getStatus());
        this.copyCounter = template.getCC();
        // this.truthValCounter = template.getTruthValCounter();
        this.active = template.isActive();
        this.updated = template.isUpdated();
        this.removable = template.isRemovable();
        this.forChecking = template.isForChecking();
        this.diagnosisRequired = template.isDiagnosisRequired();
        this.threatDetectionRequired = template.isThreatDetectionRequired();
        this.DST = template.getDST();
        this.DET = template.getDET();
        this.MCT = template.getMCT();
        this.EITLast = template.getEITLast();
        // this.nextPredNo = template.getNextPredNo();
        // nothing has been done about link so far, it should be handled later
//        
//        links = new ArrayList<Link>();
//        for (int i = 0; i < template.linkSize(); i++) {
//            Link link = template.getLink(i);
//            links.add(new Link(link));
//        }

        body = new ArrayList<Predicate>();
        for (int i = 0; i < template.bodySize(); i++) {
            Predicate pred = template.getPredicate(i);
            if (!template.isPredicateInSubformula(pred)) {
                body.add(new Predicate(pred));
            }
        }
        head = new ArrayList<Predicate>();
        for (int i = template.bodySize(); i < template.bodySize() + template.headSize(); i++) {
            Predicate pred = template.getPredicate(i);
            if (!template.isPredicateInSubformula(pred)) {
                head.add(new Predicate(pred));
            }
        }
        uf = new HashMap<String, Variable>();
        HashMap<String, Variable> tuf = template.getUnifiers();
        Iterator<String> it = tuf.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            Variable var = tuf.get(key);
            if (var.isStatic())
                uf.put(var.getName() + var.getType(), var);
            else
                uf.put(new String(var.getName() + var.getType()), new Variable(var));
        }
        huf = new HashMap<String, Variable>();
        HashMap<String, Variable> thuf = template.getHoldsUnifiers();
        Iterator<String> hit = thuf.keySet().iterator();
        while (hit.hasNext()) {
            String key = hit.next();
            Variable var = thuf.get(key);
            if (var.isStatic())
                huf.put(var.getName() + var.getType(), var);
            else
                huf.put(new String(var.getName() + var.getType()), new Variable(var));
        }
        timeVarMap = new HashMap<String, TimeVar>();
        Iterator<TimeVar> tvit = template.getTimeVarMap().values().iterator();
        while (tvit.hasNext()) {
            TimeVar tVar = tvit.next();
            timeVarMap.put(tVar.getName(), new TimeVar(tVar));
        }
        // ancestor = "";
        // descendants = new ArrayList();

        constrainedPredicatesSubformulasList = new ArrayList<ConstrainedPredicatesSubformula>();
        for (ConstrainedPredicatesSubformula initialSubformula : template.getConstrainedPredicatesSubformulasList()) {

            ConstrainedPredicatesSubformula subformula = new ConstrainedPredicatesSubformula(initialSubformula);

            for (Predicate pred : subformula.getConstrainedPredicates()) {
                if (subformula.getPositioningInFormula().equalsIgnoreCase(Constants.BODY)) {
                    body.add(pred);
                }
                else {
                    head.add(pred);
                }
            }

            for (Predicate pred : subformula.getConstraints()) {
                if (subformula.getPositioningInFormula().equalsIgnoreCase(Constants.BODY)) {
                    body.add(pred);
                }
                else {
                    head.add(pred);
                }
            }

            this.addConstrainedPredicatesSubformula(subformula);

        }
    }

    /**
     * Creates a new instance of template for a given formula.
     * 
     * @param formula
     */
    public Template(Formula formula) {
        this.type = new String(formula.getFormulaType());
        this.templateId = new String(formula.getFormulaId());
        active = false;
        updated = false;
        removable = false;
        forChecking = formula.isForChecking();
        diagnosisRequired = formula.isDiagnosisRequired();
        threatDetectionRequired = formula.isThreatDetectionRequired();
        status = Constants.STATUS_UD;
        //links = new ArrayList<Link>();
        uf = new HashMap<String, Variable>();
        huf = new HashMap<String, Variable>();
        timeVarMap = new HashMap<String, TimeVar>();
        copyCounter = 1;
        // truthValCounter = 0;
        DST = Constants.TIME_UD;
        DET = Constants.TIME_UD;
        MCT = Constants.TIME_UD;
        EITLast = Constants.TIME_UD;
        // nextPredNo = 1;
        body = new ArrayList<Predicate>();
        HashMap<String, TimePredicate> leftTimePredList = new HashMap<String, TimePredicate>();
        HashMap<String, TimePredicate> rightTimePredList = new HashMap<String, TimePredicate>();
        for (int i = 0; i < formula.getBody().size(); i++) {
            // Predicate pred = (Predicate)formula.getBody().get(i);
            Object obj = formula.getBody().get(i);
            if (obj instanceof Predicate) {
                Predicate pred = (Predicate) obj;
                body.add(new Predicate(pred));
                this.addStaticVarsToUnifiers(pred);
                /*
                 * if(pred.getEcName().equals("HoldsAt")){ Variable var1 = pred.getFluent().getFirstVariable();
                 * //if(!var1.getValue().equals("")) addVarToHoldsUnifiers(var1); Variable var2 =
                 * pred.getFluent().getSecondVariable(); //if(!var2.getValue().equals("")) addVarToHoldsUnifiers(var2); }
                 */
            }
            else if (obj instanceof TimePredicate) {
                TimePredicate timePred = (TimePredicate) obj;
                /*
                 * if(timePred.getOperator().equals(Constants.EQUAL)){ leftTimePredList.put( } else{
                 */
                TimePredicate normalisedTimePred = timePred.getNormalisedCopy();
                leftTimePredList.put(normalisedTimePred.getLTName(), normalisedTimePred);
                rightTimePredList.put(normalisedTimePred.getRTName(), normalisedTimePred);
                // }
            }
        }
        head = new ArrayList<Predicate>();
        for (int i = 0; i < formula.getHead().size(); i++) {
            Object obj = formula.getHead().get(i);
            // Predicate pred = (Predicate)formula.getHead().get(i);
            if (obj instanceof Predicate) {
                Predicate pred = (Predicate) obj;                
                head.add(new Predicate(pred));
                this.addStaticVarsToUnifiers(pred);
                /*
                 * if(pred.getEcName().equals("HoldsAt")){ Variable var1 = pred.getFluent().getFirstVariable();
                 * //if(!var1.getValue().equals("")) addVarToHoldsUnifiers(var1); Variable var2 =
                 * pred.getFluent().getSecondVariable(); //if(!var2.getValue().equals("")) addVarToHoldsUnifiers(var2); }
                 */
            }
            else if (obj instanceof TimePredicate) {
                TimePredicate timePred = (TimePredicate) obj;
                TimePredicate normalisedTimePred = timePred.getNormalisedCopy();
                leftTimePredList.put(normalisedTimePred.getLTName(), normalisedTimePred);
                rightTimePredList.put(normalisedTimePred.getRTName(), normalisedTimePred);
            }
        }
        for (int i = 0; i < totalPredicates(); i++) {
            Predicate pred = (Predicate) getPredicate(i);
            if (pred.getEcName().equals("HoldsAt")) {
                if (pred.getUBName().equals(null) || pred.getUBName().equals("")) {
                    pred.setUBName(pred.getTimeVarName());
                }
                if (pred.getLBName().equals(null) || pred.getLBName().equals("")) {
                    pred.setLBName(pred.getTimeVarName());
                }
            }
            else if (pred.getEcName().equals("Terminates") || pred.getEcName().equals("Initiates")) {
                if (pred.getUBName().equals(null) || pred.getUBName().equals("")) {
                    if (leftTimePredList.containsKey(pred.getTimeVarName())) {
                        TimePredicate timePred = leftTimePredList.get(pred.getTimeVarName());
                        TimeExpression rTex = timePred.getRightTimeExpression(pred.getTimeVarName());
                        if (rTex != null) {
                            pred.setUBName(rTex.getTimeVarName());
                            pred.setUBNumber(rTex.getNumber());
                            pred.getUpBound().setOperator(rTex.getOperator());
                        }
                    }
                    else {
                        pred.setUBName(pred.getTimeVarName());
                    }
                }
                if (pred.getLBName().equals(null) || pred.getLBName().equals("")) {
                    if (rightTimePredList.containsKey(pred.getTimeVarName())) {
                        TimePredicate timePred = rightTimePredList.get(pred.getTimeVarName());
                        TimeExpression lTex = timePred.getLeftTimeExpression(pred.getTimeVarName());
                        if (lTex != null) {
                            pred.setLBName(lTex.getTimeVarName());
                            pred.setLBNumber(lTex.getNumber());
                            pred.getLowBound().setOperator(lTex.getOperator());
                        }
                    }
                    else {
                        pred.setLBName(pred.getTimeVarName());
                    }
                }
            }
        }

        constrainedPredicatesSubformulasList = new ArrayList<ConstrainedPredicatesSubformula>();
        for (ConstrainedPredicatesSubformula initialSubformula : formula.getConstrainedPredicatesSubformulasList()) {

            ConstrainedPredicatesSubformula subformula = new ConstrainedPredicatesSubformula(initialSubformula);

            for (Predicate pred : subformula.getConstrainedPredicates()) {
                if (pred.getEcName().equals("HoldsAt")) {
                    if (pred.getUBName().equals(null) || pred.getUBName().equals("")) {
                        pred.setUBName(pred.getTimeVarName());
                    }
                    if (pred.getLBName().equals(null) || pred.getLBName().equals("")) {
                        pred.setLBName(pred.getTimeVarName());
                    }
                }
                if (subformula.getPositioningInFormula().equalsIgnoreCase(Constants.BODY)) {
                    body.add(pred);
                }
                else {
                    head.add(pred);
                }
            }

            for (Predicate pred : subformula.getConstraints()) {
                if (subformula.getPositioningInFormula().equalsIgnoreCase(Constants.BODY)) {
                    body.add(pred);
                }
                else {
                    head.add(pred);
                }
            }

            this.addConstrainedPredicatesSubformula(subformula);

        }

    }

    /**
     * This method adds the static variables of a predicate to the unifier list of the template. It should be noted that
     * static variables will have the same value for all instances of the template.
     * 
     * @param pred
     */
    private void addStaticVarsToUnifiers(Predicate pred) {
        ArrayList vars = pred.getVariables();
        for (int i = 0; i < vars.size(); i++) {
            Object obj = vars.get(i);
            if (obj instanceof Variable) {
                Variable var = (Variable) obj;
                if (var.isStatic())
                    handleStaticVar(var);
            }
            else if (obj instanceof Function) {
                Function func = (Function) obj;
                addStaticVarsToUnifiers(func);
            }
        }
        if (pred.getFluent() != null) {

            for (int j = 0; j < pred.getFluent().getArguments().size(); j++) {
                Object argument = pred.getFluent().getArguments().get(j);
                if (argument instanceof Variable) {
                    if (((Variable) argument).isStatic()) {
                        handleStaticVar((Variable) argument);
                    }
                }
                else if (argument instanceof Function) {
                    addStaticVarsToUnifiers((Function) argument);
                }
            }
        }
    }

    private void handleStaticVar(Variable var) {
        if (!uf.containsKey(var.getName() + var.getType())) {
            uf.put(var.getName() + var.getType(), new Variable(var));
        }
    }

    /**
     * This method adds the static variables of a function to the unifier list of the template. It should be noted that
     * static variables will have the same value for all instances of the template.
     * 
     * @param func
     */
    private void addStaticVarsToUnifiers(Function func) {
        // Iterator it = func.getParameters().values().iterator();
        ArrayList vars = func.getAllVariables();
        for (int i = 0; i < vars.size(); i++) {
            // while(it.hasNext()){
            // Variable var = (Variable)it.next();
            Variable var = (Variable) vars.get(i);
            if (var.isStatic())
                handleStaticVar(var);
        }
    }

    /**
     * Returns a true copy (not a reference) of this template.
     * 
     * @return
     */
    public Template getCopy() {
        // this.increaseCC();
        Template replica = new Template(this);
        // replica.increaseCC();
        // String tId = replica.getFormulaId();
        // replica.setFormulaId(tId.substring(0, tId.indexOf("-")) + "-R" + replica.getCC());
        return replica;
    }

    /**
     * Sets DST.
     * 
     * @param DST
     */
    public void setDST(long DST) {
        this.DST = DST;
    }

    /**
     * Sets DET.
     * 
     * @param DET
     */
    public void setDET(long DET) {
        this.DET = DET;
    }

    /**
     * Sets MCT.
     * 
     * @param MCT
     */
    public void setMCT(long MCT) {
        this.MCT = MCT;
    }

    /**
     * Sets EITLast.
     * 
     * @param EITLast
     */
    public void setEITLast(long EITLast) {
        this.EITLast = EITLast;
    }

    /**
     * Adds a time variable to the time variable unifier and the general unifiers.
     * 
     * @param tVar
     */
    public void addTimeVarToMap(TimeVar tVar) {
        if (!timeVarMap.containsKey(tVar.getName())) {
            timeVarMap.put(tVar.getName(), tVar);
        }
        /*
         * ATTENTION: New version 12/04/10 Copies the content of Time Variables objects to Variables objects and adds
         * them to template unifiers list
         */
        Variable timeVar =
                new Variable(tVar.getName(), "v" + tVar.getName(), Constants.DATA_TYPE_LONG, tVar.getValue());
        if (!uf.containsKey(timeVar.getName() + timeVar.getType())) {
            uf.put(timeVar.getName() + timeVar.getType(), timeVar);
        }

    }

    /**
     * Sets the status of the template.
     * 
     * @param status
     */
    public void setStatus(String status) {
        this.status = new String(status);
    }

    /**
     * Sets the copy counter of the template.
     * 
     * @param cc
     */
    public void setCC(int cc) {
        copyCounter = cc;
    }

    /**
     * Sets the active field of the template.
     */
    public void setActive(boolean val) {
        active = val;
    }

    /**
     * Sets the updated field of the template.
     * 
     * @param val
     */
    public void setUpdated(boolean val) {
        updated = val;
    }

    /**
     * Sets the removable field of the template.
     * 
     * @param val
     */
    public void setRemovable(boolean val) {
        removable = val;
    }

    /**
     * Adds a predicate to the body of the template.
     * 
     * @param pred
     */
    public void addPredicateToBody(Predicate pred) {
        body.add(pred);
    }

    /**
     * Adds a predicate to the head of the template.
     * 
     * @param pred
     */
    public void addPredicateToHead(Predicate pred) {
        head.add(pred);
    }

    /**
     * Adds a link to the template.
     * 
     * @param link
     */
    /*
    public void addLink(Link link) {
        links.add(link);
    }
	*/
    /**
     * Returns the list of constrained predicate sub formulas.
     * 
     * @return the constrainedPredicatesSubformulasList
     */
    public ArrayList<ConstrainedPredicatesSubformula> getConstrainedPredicatesSubformulasList() {
        return constrainedPredicatesSubformulasList;
    }

    /**
     * Adds a sub formula to the list of constrained predicate sub formulas.
     * 
     * @param constrainedPredicatesSubformula
     */
    public void addConstrainedPredicatesSubformula(ConstrainedPredicatesSubformula subformula) {
        this.constrainedPredicatesSubformulasList.add(subformula);
    }

    /**
     * Updates the IDs of appropriate predicates for a given event. Appropriatesness of the predciates depends on many
     * cases, for exampleIf an event E can be unified with a Happens predciate that signifies a receive activity and if
     * the template has another Happens predicate that signifies a reply activity that corresponds to the receive
     * activity, then both the predicate's ID should be updated by the event ID.
     * 
     * @param event
     */
    public void updateIDs(LogEvent event) {
        for (int i = 0; i < body.size() + head.size(); i++) {
            Predicate pred = getPredicate(i);
            if (pred.getID().equals("")) {
                if (event.getOperationName().equals(pred.getOperationName())
                        && pred.matchesPartnerID(event.getPartnerId())) {
                    if (type.equals(Constants.TYPE_FUTURE)) {
                        if (pred.getEcName().equals(Constants.PRED_INITIATES)
                                && pred.getPrefix().equals(event.getPrefix())
                                && (timeVarMap.containsKey(pred.getUBName()) || timeVarMap
                                        .containsKey(pred.getLBName())))
                            pred.setID(event.getID());
                        else if (pred.getEcName().equals(Constants.PRED_HAPPENS)
                                && event.getPrefix().equals(Constants.PREFIX_IC)
                                && pred.getPrefix().equals(Constants.PREFIX_IR)
                                && (timeVarMap.containsKey(pred.getUBName()) || timeVarMap
                                        .containsKey(pred.getLBName())))
                            pred.setID(event.getID());
                        else if (pred.getEcName().equals(Constants.PRED_HAPPENS)
                                && event.getPrefix().equals(Constants.PREFIX_RC)
                                && pred.getPrefix().equals(Constants.PREFIX_RE))
                            pred.setID(event.getID());
                    }
                    else if (type.equals(Constants.TYPE_PAST)) {
                        // following two conditions for updating predicates in the past formula
                        if ((pred.getEcName().equals(Constants.PRED_HAPPENS)
                                && event.getPrefix().equals(Constants.PREFIX_IR) && pred.getPrefix().equals(
                                Constants.PREFIX_IC))
                                || (pred.getEcName().equals(Constants.PRED_HAPPENS)
                                        && event.getPrefix().equals(Constants.PREFIX_RE) && pred.getPrefix().equals(
                                        Constants.PREFIX_RC))) {
                            TimeExpression tex = new TimeExpression(pred.getUpBound());
                            if (timeVarMap.containsKey(pred.getUBName()))
                                tex.setTimeValue(timeVarMap.get(pred.getUBName()).getValue());
                            if (tex.getValue() <= event.getTimestamp())
                                pred.setID(event.getID());
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns the size of the unifiers.
     */
    public int sizeOfUnifiers() {
        return uf.size();
    }

    /**
     * Returns true if the template is active, i.e. the truth value of at least one predicate is set to true.
     * 
     * @return
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Returns true if at least one field is updated by the SERENITYAnalyzer or SeCSEEventGen.
     * 
     * @return
     */
    public boolean isUpdated() {
        return updated;
    }

    /**
     * Returns true if the Template is no longer needed for monitoring.
     * 
     * @return
     */
    public boolean isRemovable() {
        return removable;
    }

    /**
     * Returns true if the template is for checking (not only for deduction).
     * 
     * @return
     */
    public boolean isForChecking() {
        return forChecking;
    }

    /**
     * Returns true if diagnosis is required for the template.
     * 
     * @return
     */
    public boolean isDiagnosisRequired() {
        return diagnosisRequired;
    }

    /**
     * Returns true if threat detection is required for the template.
     * 
     * @return
     */
    public boolean isThreatDetectionRequired() {
        return threatDetectionRequired;
    }

    /**
     * Returns the status of the template.
     * 
     * @return
     */
    public String getStatus() {
        return status;
    }

    /**
     * Returns the type of the formula.
     * 
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * Adds a collection of variables to the unifiers list of the template.
     * 
     * @param vars
     */
    public void addVarsToUnifiers(HashMap vars) {
        Iterator it = vars.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            Variable var = (Variable) vars.get(key);
            // logger.debug("*******" + var.getHashValue());
            // if(!uf.containsKey(var.getName() + var.getType()) && !var.getName().equals("vID")) uf.put(var.getName() +
            // var.getType(), var);
            // if(var.getConstName().equals("source")) continue;
            if (!var.getName().contains("vID"))
                uf.put(var.getName() + var.getType(), var);
            // logger.debug("Size of UF" + uf.size());

        }
    }

    /**
     * Adds a variable to the list of unifiers of the template.
     * 
     * @param var
     */
    public void addVarToUnifiers(Variable var) {
        if (!uf.containsKey(var.getName() + var.getType()) && !var.getName().equals("ID")) {
            uf.put(var.getName() + var.getType(), var);
        }
    }

    /**
     * Returns the collection of the unifiers of the time variables.
     * 
     * @return
     */
    public HashMap<String, TimeVar> getTimeVarMap() {
        return timeVarMap;
    }

    /**
     * Returns the projection of unifiers over the variables of a predicate. The formal definition of this projection is
     * given in Dynamic Validation Prototype Developers Guide. In simple word, this method returns a collection of
     * variables of a predicate, if those variables are already assigned to some values in the template.
     * 
     * @param pred
     * @return
     */
    public HashMap getProjection(Predicate pred) {
        HashMap projection = new HashMap();
        ArrayList predVars = pred.getVariables();
        for (int i = 0; i < predVars.size(); i++) {
            Object obj = predVars.get(i);
            if (obj instanceof Variable) {
                Variable var = (Variable) obj;
                if (uf.containsKey(var.getName() + var.getType())) {
                    projection.put(var.getName() + var.getType(), (Variable) uf.get(var.getName() + var.getType()));
                }
            }
            else {// relational predicate may have functions
                Function func = (Function) obj;
                ArrayList funcVars = func.getAllVariables();
                for (int j = 0; j < funcVars.size(); j++) {
                    Variable var = (Variable) funcVars.get(j);
                    if (uf.containsKey(var.getName() + var.getType()))
                        projection.put(var.getName() + var.getType(), (Variable) uf.get(var.getName() + var.getType()));
                }
            }
        }
        Fluent fluent = pred.getFluent();
        if (fluent != null) {
            for (int i = 0; i < fluent.getArguments().size(); i++) {
                Object argument = fluent.getArguments().get(i);
                if (argument instanceof Variable) {
                    Variable var = (Variable) argument;
                    if (uf.containsKey(var.getName() + var.getType())) {
                        projection.put(var.getName() + var.getType(), (Variable) uf.get(var.getName() + var.getType()));
                    }
                }
                else if (argument instanceof Function) {
                    Function func = (Function) argument;
                    ArrayList funcVars = func.getAllVariables();
                    for (int j = 0; j < funcVars.size(); j++) {
                        Variable var = (Variable) funcVars.get(j);
                        if (uf.containsKey(var.getName() + var.getType())) {
                            projection.put(var.getName() + var.getType(), (Variable) uf.get(var.getName()
                                    + var.getType()));
                        }
                    }
                }
            }
        }
        return projection;
    }

    /**
     * Returns the unifiers of the template.
     * 
     * @return
     */
    public HashMap<String, Variable> getUnifiers() {
        return uf;
    }

    /**
     * Returns the holds at unifiers of the template.
     * 
     * @return
     */
    public HashMap<String, Variable> getHoldsUnifiers() {
        return huf;
    }

    /**
     * Returns DST.
     * 
     * @return
     */
    public long getDST() {
        return DST;
    }

    /**
     * Returns DET.
     * 
     * @return
     */
    public long getDET() {
        return DET;
    }

    /**
     * Returns MCT.
     * 
     * @return
     */
    public long getMCT() {
        return MCT;
    }

    /**
     * Returns EITLast.
     * 
     * @return
     */
    public long getEITLast() {
        return EITLast;
    }

    /**
     * Returns linkSize.
     * 
     * @return
     */
    
    public int linkSize() {
        //return links.size();
    	return 0;
    }
	
    /**
     * Returns the i-th link.
     * 
     * @return
     */
    /*
    public Link getLink(int i) {
        return links.get(i);
    }
	*/
    /**
     * Returns the i-th predicate of the template.
     * 
     * @param i -
     *            location of the predicate
     * @return
     */
    public Predicate getPredicate(int i) {
        Predicate pred = null;
        if (i < (body.size() + head.size())) {
            if (i < body.size())
                pred = body.get(i);
            else
                pred = head.get(i - body.size());
        }
        return pred;
    }

    /**
     * Returns the number of total predicates in the template.
     * 
     * @return
     */
    public int totalPredicates() {
        return body.size() + head.size();
    }

    /**
     * Returns the number of predicates in the body.
     * 
     * @return
     */
    public int bodySize() {
        return body.size();
    }

    /**
     * Returns the number of predicates in the head.
     * 
     * @return
     */
    public int headSize() {
        return head.size();
    }

    /**
     * Returns true if the truth value of at least one predicate in the head of the template is set to false.
     * 
     * @return
     */
    public boolean isHeadFalse() {
        boolean val = false;
        for (int i = 0; i < head.size(); i++) {
            Predicate pred = head.get(i);
            if (!this.isPredicateInSubformula(pred) && pred.getTruthVal().equals(Constants.TRUTH_VAL_FALSE)) {
                val = true;
                break;
            }
        }
        // consider constrained predicates subformulas
        if (val == false) {
            for (ConstrainedPredicatesSubformula subformula : this.getConstrainedPredicatesSubformulasList()) {
                if (subformula.getPositioningInFormula().equals(Constants.HEAD)
                        && subformula.getTruthValue().equals(Constants.TRUTH_VAL_FALSE)) {
                    val = true;
                    break;
                }
            }
        }
        return val;
    }

    /**
     * Returns true if the truth values of all the predicates in the head of the template is set to true.
     * 
     * @return
     */
    public boolean isHeadTrue() {
        boolean val = true;
        for (int i = 0; i < head.size(); i++) {
            Predicate pred = head.get(i);
            if (!this.isPredicateInSubformula(pred)
                    && (pred.getTruthVal().equals(Constants.TRUTH_VAL_FALSE) || pred.getTruthVal().equals(
                            Constants.TRUTH_VAL_UK))) {
                val = false;
                break;
            }
        }

        // consider constrained predicates subformulas
        if (val == true) {
            for (ConstrainedPredicatesSubformula subformula : this.getConstrainedPredicatesSubformulasList()) {
                if (subformula.getPositioningInFormula().equals(Constants.HEAD)
                        && (subformula.getTruthValue().equals(Constants.TRUTH_VAL_FALSE) || subformula.getTruthValue()
                                .equals(Constants.TRUTH_VAL_UK))) {
                    val = false;
                    break;
                }
            }
        }

        return val;
    }

    /**
     * Returns true if the truth value of at least one predicate in the body of the template is set to false by a
     * derived event.
     * 
     * @return
     */
    /*
    public boolean bodyFalseByDerived() {
        boolean val = false;
        for (int i = 0; i < body.size(); i++) {
            Predicate pred = body.get(i);
            if (pred.getTruthVal().equals(Constants.TRUTH_VAL_FALSE) && pred.getSource().equals(Constants.SOURCE_DE)) {
                val = true;
                break;
            }
        }
        return val;
    }
	*/
    /**
     * Returns true if the truth value of at least one predicate in the body of the template is set to false.
     * 
     * @return
     */
    public boolean isBodyFalse() {
        boolean val = false;
        for (int i = 0; i < body.size(); i++) {
            Predicate pred = body.get(i);
            if (!this.isPredicateInSubformula(pred) && pred.getTruthVal().equals(Constants.TRUTH_VAL_FALSE)) {
                val = true;
                break;
            }
        }

        // consider constrained predicates subformulas
        if (val == false) {
            for (ConstrainedPredicatesSubformula subformula : this.getConstrainedPredicatesSubformulasList()) {
                if (subformula.getPositioningInFormula().equals(Constants.BODY)
                        && subformula.getTruthValue().equals(Constants.TRUTH_VAL_FALSE)) {
                    val = true;
                    break;
                }
            }
        }
        return val;
    }

    /**
     * Returns true if the truth values of all the predicates in the body of the template is set to true.
     * 
     * @return
     */
    public boolean isBodyTrue() {
        boolean val = true;
        for (int i = 0; i < body.size(); i++) {
            Predicate pred = body.get(i);
            if (!this.isPredicateInSubformula(pred)
                    && (pred.getTruthVal().equals(Constants.TRUTH_VAL_FALSE) || pred.getTruthVal().equals(
                            Constants.TRUTH_VAL_UK))) {
                val = false;
                break;
            }
        }

        // consider constrained predicates subformulas
        if (val == true) {
            for (ConstrainedPredicatesSubformula subformula : this.getConstrainedPredicatesSubformulasList()) {
                if (subformula.getPositioningInFormula().equals(Constants.BODY)
                        && (subformula.getTruthValue().equals(Constants.TRUTH_VAL_FALSE) || subformula.getTruthValue()
                                .equals(Constants.TRUTH_VAL_UK))) {
                    val = false;
                    break;
                }
            }
        }

        return val;
    }

    /**
     * Returns true if teh truth value of no predicate in the template is set by derived event.
     * 
     * @return
     */
    public boolean noSourceDE() {
        boolean val = true;
        for (int i = 0; i < body.size() + head.size(); i++) {
            Predicate pred = getPredicate(i);
            if (pred.getSource().equals(Constants.SOURCE_DE)) {
                val = false;
                break;
            }
        }
        return val;
    }

    /**
     * Returns true if the truth values of all the predicates in the template are set.
     * 
     * @return
     */
    public boolean noUnKnownTruthValue() {
        boolean val = true;
        for (int i = 0; i < body.size() + head.size(); i++) {
            Predicate pred = getPredicate(i);
            if (pred.getTruthVal().equals(Constants.TRUTH_VAL_UK)) {
                val = false;
                break;
            }
        }
        return val;
    }

    /**
     * Returns true if the pHappens field of at least one predicate is set to true.
     * 
     * @return
     */
    /*
    public boolean hasPHappens() {
        boolean val = false;
        for (int i = 0; i < body.size() + head.size() && !val; i++) {
            Predicate pred = getPredicate(i);
            val = pred.getPHappens();
        }
        return val;
    }
	*/
    /**
     * Returns the formula id. The formula id represents the first part of the template id. The template Id structure is
     * formulaId*[$eventId], hence, the formula id is any character before the first occurrence of the $ symbol
     * 
     * @return
     */
    public String getFormulaId() {
        String fId = "";
        String tId = getTemplateId();
        if (tId.contains("$")) {
            fId = tId.substring(0, tId.indexOf("$"));
        }
        else {
            fId = tId;
        }
        return fId;
    }

    /**
     * Returns the copy counter.
     * 
     * @return
     */
    public int getCC() {
        return copyCounter;
    }

    /**
     * Returns a string representation of the body of the template.
     * 
     * @param withPID -
     *            if true then the partner id of each predicate is included in string
     * @return
     */
    public String toStringBody(boolean withPID) {
        String val = "";
        for (int i = 0; i < body.size(); i++) {
            Predicate pred = body.get(i);
            val += pred.toString(withPID);
            if (i < body.size() - 1)
                val += " ^ ";
        }
        return val;
    }

    /**
     * Returns a string representation of the head of the template.
     * 
     * @param withPID -
     *            if true then the partner id of each predicate is included in string
     * @return
     */
    public String toStringHead(boolean withPID) {
        String val = "";
        for (int i = 0; i < head.size(); i++) {
            Predicate pred = head.get(i);
            val += pred.toString(withPID);
            if (i < head.size() - 1)
                val += " ^ ";
        }
        return val;
    }

    /**
     * Returns the template body.
     * 
     * @return
     */
    public ArrayList<Predicate> getBody() {
        return body;
    }

    /**
     * Returns the template head.
     * 
     * @return
     */
    public ArrayList<Predicate> getHead() {
        return head;
    }

    /**
     * Prints out the unifiers of the template.
     * 
     */
    public void printUnifiers() {
        Set set = this.getUnifiers().entrySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Map.Entry me = (Map.Entry) it.next();
            //logger.debug(me.getKey() + " : " + ((Variable) me.getValue()).toString(true));
        }
    }

    /**
     * Returns the templateId.
     * 
     * @return the templateId
     */
    public String getTemplateId() {
        return templateId;
    }

    /**
     * Sets the templateId.
     * 
     * @param templateId
     *            the templateId to set
     */
    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    /**
     * Adds static variables to the given structure.
     * 
     * @param staticVars
     */
    /*
    public void addStaticVars(HashMap staticVars) {
        Iterator it = uf.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            Variable ufVar = (Variable) uf.get(key);
            if (staticVars.containsKey(key)) {
                Variable staticVVar = (Variable) staticVars.get(key);
                logger.debug("Template: reference key:" + key + "-" + staticVVar);
                uf.put(key, staticVVar);
            }
            else {
                logger.debug("Template: new static var added reference key:" + key + "-" + ufVar);
                staticVars.put(key, ufVar);
            }
        }
    }
    */

    /**
     * Adjusts static variables of the given structure.
     * 
     * @param staticVars
     */
    public void adjustStaticVars(HashMap staticVars) {
        Iterator it = uf.keySet().iterator();
        ArrayList<String> keysToBeRemoved = new ArrayList<String>();
        while (it.hasNext()) {
            String key = (String) it.next();
            Variable ufVar = (Variable) uf.get(key);
            if (ufVar.isStatic()) {
                if (!staticVars.containsKey(key)) {
                    logger.debug("Template: new static var added reference key:" + key + "-" + ufVar);
                    staticVars.put(key, ufVar);
                }
                keysToBeRemoved.add(key);
                logger.debug("Template: size of uf:" + uf.size());
            }
        }
        for (String keyToBeRemoved : keysToBeRemoved) {
            uf.remove(keyToBeRemoved);
        }
        if (uf.size() == 0) {
            uf.clear();
        }
    }

    /**
     * Returns the threat likelihood of the template.
     * 
     * @return the threatLikelihood
     */
    public double getThreatLikelihood() {
        return threatLikelihood;
    }

    /**
     * Sets the threat likelihood of the template.
     * 
     * @param threatLikelihood
     *            the threatLikelihood to set
     */
    public void setThreatLikelihood(double threatLikelihood) {
        this.threatLikelihood = threatLikelihood;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String s =
                getClass().getName() + "{" + "templateId=" + templateId + "," + "formulaId=" + getFormulaId() + ","
                        + "status=" + status + "," + "type=" + type + "," + "body=" + body + "," + "head=" + head + "}";

        return s;
    }

    /**
     * checks whether a given predicate is included in a sub formula of the template.
     * 
     * @param pred
     * @return
     */
    public boolean isPredicateInSubformula(Predicate pred) {
        boolean inSubformula = false;
        for (ConstrainedPredicatesSubformula subformula : this.getConstrainedPredicatesSubformulasList()) {
            if (subformula.getConstrainedPredicates().contains(pred) || subformula.getConstraints().contains(pred)) {
                inSubformula = true;
            }
        }
        return inSubformula;
    }
}
