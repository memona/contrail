/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * FormulaReader.java
 *
 * Created on 24 October 2004, 16:20
 */
package uk.ac.city.soi.everest.bse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import uk.ac.city.soi.everest.SRNTEvent.OpStatus;
import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.ExpVariable;
import uk.ac.city.soi.everest.core.Fluent;
import uk.ac.city.soi.everest.core.Function;
import uk.ac.city.soi.everest.core.TimeExpression;
import uk.ac.city.soi.everest.core.TimeVar;
import uk.ac.city.soi.everest.core.Variable;
import uk.ac.city.soi.everest.monitor.ConstrainedPredicatesSubformula;
import uk.ac.city.soi.everest.monitor.Predicate;

/**
 * This class reads XML formula and generates Java object.
 * 
 * @author Khaled Mahbub
 */
public class FormulaReader {
    // logger
    private static Logger logger = Logger.getLogger(FormulaReader.class);

    // private ArrayList formula;
    // private ArrayList body;
    // private ArrayList head;
    private Document document;
    private HashMap quantifiers;
    private int artificialTimeVarCounter = 0;

    public FormulaReader() {
        quantifiers = new HashMap();
        quantifiers.put("", "");
    }

    /**
     * Reads XML formulas from a XML file and returns a collection of Formula objects.
     * 
     * @param fileName -
     *            name of the XML file
     * @return
     */
    /*
    public LinkedHashMap getFormula(String fileName) {
        // ArrayList formula = new ArrayList();
        parseFile(fileName);
        // formula = processRootNode(resolveEmptyNodes(document.getDocumentElement()));
        // return formula;
        return processRootNode(resolveEmptyNodes(document.getDocumentElement()));
    }
	*/
    /**
     * Receives XML formula and returns a collection of Formula objects.
     * 
     * @param docStr -
     *            String representation of XML formula
     * @param nameSpaceAware -
     *            whether the XML is name space aware
     * @return - A collection of Formula objects
     */
    public LinkedHashMap getFormula(String docStr, boolean nameSpaceAware) {
        // ArrayList formula = new ArrayList();
        // parseFile(fileName);
        // formula = processRootNode(resolveEmptyNodes(document.getDocumentElement()));
        // return formula;
        LinkedHashMap props = new LinkedHashMap();
        try {
            document = XMLConverter.toDocument(docStr, true);
            props = processRootNode(resolveEmptyNodes(document.getDocumentElement()));
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return props;
    }

    /**
     * Receives XML formulas as W3C document and returns a collection of Formula objects.
     * 
     * @param doc -
     *            W3C document object that contains XML formuals
     * @return - Collection of Formula objects
     */
    public LinkedHashMap getFormula(Document doc) {
        // ArrayList formula = new ArrayList();
        // parseFile(fileName);
        // formula = processRootNode(resolveEmptyNodes(document.getDocumentElement()));
        // return formula;
        LinkedHashMap props = new LinkedHashMap();
        try {
            document = doc;
            props = processRootNode(resolveEmptyNodes(document.getDocumentElement()));
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return props;
    }

    /**
     * Parse an XML file and builds the corresponding DOM tree and assign it to document.
     * 
     * @param fileName
     *            name of the XML file to be parsed.
     */
    /*
    private void parseFile(String fileName) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            // File xmlFile = new File(fileName);
            DocumentBuilder builder = factory.newDocumentBuilder();
            // document = builder.parse(xmlFile);
            document = builder.parse(fileName);
        }
        catch (SAXException sxe) {
            // Error generated by this application (or a parser-initialization error)
            Exception x = sxe;
            if (sxe.getException() != null)
                x = sxe.getException();
            x.printStackTrace();

        }
        catch (ParserConfigurationException pce) {
            // Parser with specified options can't be built
            pce.printStackTrace();

        }
        catch (IOException ioe) {
            // I/O error
            ioe.printStackTrace();
        }
    }
	*/
    /**
     * Receives a W3C Node that contains a XML variable element and returns Variable object
     * 
     * @param node -
     *            W3C node that contains variabel defintion in XML.
     * @return - A Variabel object
     */
    private Variable processVariable(Node node) {
        Variable var = new Variable("", "", "", "");
        try {
            NamedNodeMap attributes = node.getAttributes();
            Node forMatching = attributes.getNamedItem("forMatching");
            Node persistent = attributes.getNamedItem("persistent");
            if (persistent.getNodeValue().equals("true"))
                var.setStatic(true);
            boolean matching = forMatching.getNodeValue().equals("true");
            NodeList childs = node.getChildNodes();
            for (int i = 0; i < childs.getLength(); i++) {
                Node child = childs.item(i);
                if (child.getNodeName().equals("varName")) {
                    if (matching) {
                        var.setConstName(child.getFirstChild().getNodeValue());
                        var.setName("v" + child.getFirstChild().getNodeValue());
                    }
                    else
                        var.setName(child.getFirstChild().getNodeValue());
                }
                else if (child.getNodeName().equals("varType")) {
                    var.setType(child.getFirstChild().getNodeValue());

                }
                else if (child.getNodeName().equals("value")) {
                    try {
                    	var.setValue(child.getFirstChild().getNodeValue());
                    	/*
                        if (var.getType().equals("OpStatus")) {
                        	//System.err.println("=+=+=+=++ Opstatus value " + child.getFirstChild().getNodeValue());
                            OpStatus status = new OpStatus(child.getFirstChild().getNodeValue());
                            var.setValue(status.toString());
                        }
                        else {
                            var.setValue(child.getFirstChild().getNodeValue());
                        }
                        */
                    }
                    catch (Exception e) {

                    }

                }
                else if (child.getNodeName().equals("array")) {
                    NodeList arrayChilds = child.getChildNodes();
                    for (int acc = 0; acc < arrayChilds.getLength(); acc++) {
                        Node arrayChild = arrayChilds.item(acc);
                        if (arrayChild.getNodeName().equals("type"))
                            var.setType(arrayChild.getFirstChild().getNodeValue());
                        // else if(arrayChild.getNodeName().equals("index"))
                        // var.setArrayIndexName(arrayChild.getFirstChild().getNodeValue());
                        else if (arrayChild.getNodeName().equals("value")) {
                            Node indexValue = arrayChild.getFirstChild();
                            Node cellValue = arrayChild.getLastChild();
                            var.addValueAt(cellValue.getFirstChild().getNodeValue(), Integer.parseInt(indexValue
                                    .getFirstChild().getNodeValue()));
                        }
                    }
                    // var.setValue(child.getFirstChild().getNodeValue());
                }
            }

        }
        catch (Exception exp) {
            exp.printStackTrace();
        }

        logger.debug("********************VARIABLE**************");
        logger.debug("name: " + var.getName());
        logger.debug("Constname: " + var.getConstName());
        logger.debug("type: " + var.getType());
        logger.debug("value: " + var.getObjValue());

        return var;
    }

    /**
     * Receives a W3C Node that contains XML field element and returns a list of field values.
     * 
     * @param node -
     *            W3C node that contains XML field element
     * @return -
     */
    private ArrayList processFields(Node node) {
        ArrayList fList = new ArrayList();
        NodeList childs = node.getChildNodes();
        for (int i = 0; i < childs.getLength(); i++) {
            fList.add(childs.item(i).getFirstChild().getNodeValue());
        }

        return fList;
    }

    /**
     * Receives a W3C Node that contains a XML ExpressionVariable element and returns ExpVariable object.
     * 
     * @param node -
     *            W3C node that contains expression defintion in XML
     * @return - An ExpVariable object
     */
    private ExpVariable processExpVariable(Node node) {
        ExpVariable var = new ExpVariable("", "", "", null, "", "");

        try {
            NamedNodeMap attributes = node.getAttributes();
            Node forMatching = attributes.getNamedItem("forMatching");
            Node persistent = attributes.getNamedItem("persistent");
            if (persistent.getNodeValue().equals("true"))
                var.setStatic(true);
            boolean matching = forMatching.getNodeValue().equals("true");
            NodeList childs = node.getChildNodes();
            for (int i = 0; i < childs.getLength(); i++) {
                Node child = childs.item(i);
                if (child.getNodeName().equals("varName")) {
                    if (matching) {
                        var.setConstName(child.getFirstChild().getNodeValue());
                        var.setName("v" + child.getFirstChild().getNodeValue());
                    }
                    else
                        var.setName(child.getFirstChild().getNodeValue());
                }
                else if (child.getNodeName().equals("varType"))
                    var.setType(child.getFirstChild().getNodeValue());
                else if (child.getNodeName().equals("value"))
                    var.setValue(child.getFirstChild().getNodeValue());
                else if (child.getNodeName().equals("fields")) {

                    ArrayList field = processFields(child);
                    String path = "";

                    for (int t = 0; t < field.size() - 1; t++) {
                        path = path + field.get(t).toString() + ".";
                    }
                    var.setPath(path);
                    var.setField(field.get(field.size() - 1).toString());
                    var.setXPath(field);
                    // var.setFieldExp(field);

                }
                else if (child.getNodeName().equals("array")) {
                    NodeList arrayChilds = child.getChildNodes();
                    for (int acc = 0; acc < arrayChilds.getLength(); acc++) {
                        Node arrayChild = arrayChilds.item(acc);
                        if (arrayChild.getNodeName().equals("type"))
                            var.setType(arrayChild.getFirstChild().getNodeValue());
                        // else if(arrayChild.getNodeName().equals("index"))
                        // var.setArrayIndexName(arrayChild.getFirstChild().getNodeValue());
                        else if (arrayChild.getNodeName().equals("value")) {
                            Node indexValue = arrayChild.getFirstChild();
                            Node cellValue = arrayChild.getLastChild();
                            var.addValueAt(cellValue.getFirstChild().getNodeValue(), Integer.parseInt(indexValue
                                    .getFirstChild().getNodeValue()));
                        }
                    }
                    // var.setValue(child.getFirstChild().getNodeValue());
                }
            }

        }
        catch (Exception exp) {
            exp.printStackTrace();
        }

//        logger.debug("********************VARIABLE EXP**************");
//        logger.debug("name: " + var.getName());
//        logger.debug("Constname: " + var.getConstName());
//        logger.debug("type: " + var.getType());
//        logger.debug("value: " + var.getObjValue());
//        logger.debug("field type: " + var.getRealType());

        return var;
    }

    /**
     * Receives a W3C Node that contains a constant definition in XML and returns Variable object. It should be noted
     * that constant is modelled as Variable that does not have name, only value.
     * 
     * @param node -
     *            W3C node that contains constant defintion in XML
     * @return - A Variable object
     */
    private Variable processConstant(Node node) {
        Variable var = new Variable("", "", "", "");

        try {
            NodeList childs = node.getChildNodes();
            for (int i = 0; i < childs.getLength(); i++) {
                Node child = childs.item(i);
                if (child.getNodeName().equals("name")) {
                    var.setConstName(child.getFirstChild().getNodeValue());
                    var.setName("v" + child.getFirstChild().getNodeValue());
                }
                else if (child.getNodeName().equals("value"))
                    var.setValue(child.getFirstChild().getNodeValue());
            }
            var.setType(Constants.DATA_TYPE_CONST);

        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        /*
         * logger.debug("********************CONST**************"); logger.debug("name: "+var.getName());
         * logger.debug("Constname: "+var.getConstName()); logger.debug("type: "+var.getType()); logger.debug("value:
         * "+var.getObjValue());
         */
        return var;
    }

    /**
     * Receives a W3C Node that contains an XML quanitifier element and stores the quantifier information in the
     * quantifier map.
     * 
     * @param node -
     *            W3C node that contains quantifier definition in XML
     * @return - A Variabel object
     */
    private void processQuantifier(Node node) {
        Node var = node.getLastChild();
        if (var.getNodeName().equals("timeVariable")) {
            Node quantifier = node.getFirstChild();
            // String t = var.getFirstChild().getFirstChild().getNodeValue();
            // String t = quantifier.getNodeValue();
            quantifiers.put(var.getFirstChild().getFirstChild().getNodeValue(), quantifier.getFirstChild()
                    .getNodeValue());
        }
    }

    /**
     * Receives a W3C Node object that contains an xML RC_term or IR_Term element and returns a Predicate object.
     * 
     * @param node -
     *            W3C node that contains RC_Term/IR_Term in XML formula
     * @param ecName -
     *            EC name of the predicate (i.e. Happens, Initiates etc.)
     * @param prefix -
     *            RC or IR
     * @return - A Predicate object
     */
    private Predicate processRcIrTerm(Node node, String ecName, String prefix) {
        Predicate pred = null;
        try {
            String opn = node.getFirstChild().getFirstChild().getNodeValue();
            String partner = node.getFirstChild().getNextSibling().getFirstChild().getNodeValue();
            String id = node.getLastChild().getFirstChild().getNodeValue();
            pred = new Predicate(ecName, prefix, partner, opn, "");
            pred.setID("");
            NodeList vars = XPathAPI.selectNodeList(node, "variable");
            for (int i = 0; i < vars.getLength(); i++) {
                Node var = vars.item(i);
                // pred.addVariable(new Variable(var.getFirstChild().getFirstChild().getNodeValue(),
                // var.getLastChild().getFirstChild().getNodeValue(), ""));
                pred.addVariable(processVariable(var));
            }
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return pred;
    }

    /**
     * Receives a W3C Node object that contains an xML RE_term or IC_Term element and returns a Predicate object.
     * 
     * @param node -
     *            W3C node that contains RE_Term/IC_Term in XML formula
     * @param ecName -
     *            EC name of the predicate (i.e. Happens, Initiates etc.)
     * @param prefix -
     *            RE or IC
     * @return
     */
    private Predicate processReIcTerm(Node node, String ecName, String prefix) {
        Predicate pred = null;
        try {
            String opn = node.getFirstChild().getFirstChild().getNodeValue();
            String partner = node.getFirstChild().getNextSibling().getFirstChild().getNodeValue();
            String id = node.getFirstChild().getNextSibling().getNextSibling().getFirstChild().getNodeValue();
            pred = new Predicate(ecName, prefix, partner, opn, "");
            pred.setID("");
            NodeList vars = XPathAPI.selectNodeList(node, "variable");
            for (int i = 0; i < vars.getLength(); i++) {
                Node var = vars.item(i);
                // pred.addVariable(new Variable(var.getFirstChild().getFirstChild().getNodeValue(),
                // var.getLastChild().getFirstChild().getNodeValue(), ""));
                pred.addVariable(processVariable(var));
            }
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return pred;
    }

    /**
     * Receives a W3C Node object that contains an xML AS_term and returns a Predicate object.
     * 
     * @param node -
     *            W3C node that contains AS_Term in XML formula
     * @param ecName -
     *            EC name of the predicate (i.e. Happens, Initiates etc.)
     * @return
     */
    /*
    private Predicate processAsTerm(Node node, String ecName) {
        Predicate pred = null;
        try {
            pred = new Predicate(ecName, "as", "", node.getFirstChild().getFirstChild().getNodeValue(), "");
            // pred.setID(node.getLastChild().getFirstChild().getNodeValue());
            pred.setID("");
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return pred;
    }
	*/
    /**
     * Receives a W3C Node object that contains an xML TimeVariable element and returns TimeVar object.
     * 
     * @param node -
     *            W3C node that contains TimeVariable in XML formula
     * @return
     */
    private TimeVar processTimeVariable(Node node) {
        TimeVar tVar = new TimeVar("");
        try {
            NodeList nl = node.getChildNodes();
            for (int i = 0; i < nl.getLength(); i++) {
                Node child = nl.item(i);
                if (child.getNodeName().equals(Constants.VARIABLE_NAME)) {
                    tVar.setName(child.getFirstChild().getNodeValue());
                }
                else if (child.getNodeName().equals(Constants.VARIABLE_VALUE)) {
                    tVar.setValue(Long.parseLong(child.getFirstChild().getNodeValue()));
                }
            }
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return tVar;
    }

    /**
     * Receives a W3C Node object that contains an xML TimeExpression element and returns TimeExpression object.
     * 
     * @param node -
     *            W3C node that contains TimeExpression in XML formula
     * @return
     */
    private TimeExpression processTimeExpression(Node node) {
        TimeExpression tex = null;
        try {
            NodeList nl = node.getChildNodes();
            Node time = nl.item(0);
            // tex = new TimeExpression(time.getFirstChild().getFirstChild().getNodeValue());
            tex = new TimeExpression(processTimeVariable(time));
            if (nl.getLength() > 1) {
                Node operator = nl.item(1);
                if (operator.getNodeName().equals("plus"))
                    tex.setOperator(Constants.PLUS);
                else
                    tex.setOperator(Constants.MINUS);
                tex.setNumber(Long.parseLong(operator.getFirstChild().getNodeValue()));
            }
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return tex;
    }

    /**
     * Receives a W3C Node object that contains an xML happens element and returns a Predicate object.
     * 
     * @param node -
     *            W3C node that contains happens in XML formula
     * @return - Predciate object
     */

    private Predicate processHappens(Node node) {
        Predicate pred = null;
        try {
            NodeList childs = node.getChildNodes();
            for (int i = 0; i < childs.getLength(); i++) {
                Node child = childs.item(i);
                if (child.getNodeName().equals("rc_term"))
                    pred = processRcIrTerm(child, "Happens", "rc");
                else if (child.getNodeName().equals("ir_term"))
                    pred = processRcIrTerm(child, "Happens", "ir");
                else if (child.getNodeName().equals("ic_term"))
                    pred = processReIcTerm(child, "Happens", "ic");
                else if (child.getNodeName().equals("re_term"))
                    pred = processReIcTerm(child, "Happens", "re");
//                else if (child.getNodeName().equals("as_term"))
//                    pred = processAsTerm(child, "Happens");
                else if (child.getNodeName().equals("timeVar")) {
                    String timeVarName = child.getFirstChild().getFirstChild().getNodeValue();
                    pred.setTimeVarName(timeVarName);
                    if (quantifiers.containsKey(timeVarName)) {
                        String quantifier = (String) quantifiers.get(timeVarName);
                        if (quantifier.equals("forall"))
                            pred.setQuantifier(Constants.QUANT_FORALL);
                        else
                            pred.setQuantifier(Constants.QUANT_EXIST);
                    }
                }
                else if (child.getNodeName().equals("fromTime")) {
                    TimeExpression tex = processTimeExpression(child);
                    pred.setLBName(tex.getTimeVarName());
                    pred.getLowBound().setOperator(tex.getOperator());
                    if (tex.getTimeValue() != Constants.RANGE_LB && tex.getTimeValue() != Constants.RANGE_UB
                            && tex.getTimeValue() != Constants.TIME_UD)
                        pred.getLowBound().setTimeValue(tex.getTimeValue());
                    pred.setLBNumber(tex.getNumber());
                }
                else if (child.getNodeName().equals("toTime")) {
                    TimeExpression tex = processTimeExpression(child);
                    pred.setUBName(tex.getTimeVarName());
                    pred.getUpBound().setOperator(tex.getOperator());
                    if (tex.getTimeValue() != Constants.RANGE_LB && tex.getTimeValue() != Constants.RANGE_UB
                            && tex.getTimeValue() != Constants.TIME_UD)
                        pred.getUpBound().setTimeValue(tex.getTimeValue());
                    pred.setUBNumber(tex.getNumber());
                }
            }
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return pred;
    }

    /**
     * Receives a W3C Node object that contains an XML initiates element and returns Predicate object.
     * 
     * @param node -
     *            W3C node that contains initiates in XML formula
     * @return
     */
    private Predicate processInitiates(Node node) {
        Predicate pred = null;
        try {
            NodeList childs = node.getChildNodes();
            for (int i = 0; i < childs.getLength(); i++) {
                Node child = childs.item(i);
                if (child.getNodeName().equals("rc_term"))
                    pred = processRcIrTerm(child, "Initiates", "rc");
                else if (child.getNodeName().equals("ir_term"))
                    pred = processRcIrTerm(child, "Initiates", "ir");
                else if (child.getNodeName().equals("ic_term"))
                    pred = processReIcTerm(child, "Initiates", "ic");
//                else if (child.getNodeName().equals("as_term"))
//                    pred = processAsTerm(child, "Initiates");
                else if (child.getNodeName().equals("fluent"))
                    pred.setFluent(processFluent(child));
                else if (child.getNodeName().equals("timeVar")) {
                    String timeVarName = child.getFirstChild().getFirstChild().getNodeValue();
                    pred.setTimeVarName(timeVarName);
                    if (quantifiers.containsKey(timeVarName)) {
                        String quantifier = (String) quantifiers.get(timeVarName);
                        if (quantifier.equals("forall"))
                            pred.setQuantifier(Constants.QUANT_FORALL);
                        else
                            pred.setQuantifier(Constants.QUANT_EXIST);
                    }
                    if (!pred.getPrefix().equals("as")) {
                        TimeExpression tex = new TimeExpression(pred.getTimeVarName());
                        tex.setTimeValue(Constants.RANGE_LB);
                        pred.setLowBound(new TimeExpression(tex));
                        tex.setTimeValue(Constants.RANGE_UB);
                        tex.setNumber(1);
                        pred.setUpBound(new TimeExpression(tex));
                    }
                }
                /**
                 * There might be the case that a predicate has a time expression as a time variable. In such cases, the
                 * code generates an artificial time variable existentially quantified whose upper and lower boundaries
                 * is equal to the specified time expression.
                 */
                else if (child.getNodeName().equals(Constants.TIME_EXPRESSION)) {
                    TimeExpression timeExp = processTimeExpression(child);

                    pred.setQuantifier(Constants.QUANT_EXIST);
                    pred.setTimeVarName(Constants.ARTIFICIAL_TIME_VARIABLE + artificialTimeVarCounter);
                    pred.setLowBound(new TimeExpression(timeExp));
                    pred.setUpBound(new TimeExpression(timeExp));

                    artificialTimeVarCounter++;
                }
            }
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return pred;
    }

    /**
     * Receives a W3C Node object that contains an XML terminates element and returns Predicate object.
     * 
     * @param node -
     *            W3C node that contains terminates in XML formula
     * @return
     */
    private Predicate processTerminates(Node node) {
        Predicate pred = null;
        try {
            NodeList childs = node.getChildNodes();
            for (int i = 0; i < childs.getLength(); i++) {
                Node child = childs.item(i);
                if (child.getNodeName().equals("rc_term"))
                    pred = processRcIrTerm(child, "Terminates", "rc");
                else if (child.getNodeName().equals("ir_term"))
                    pred = processRcIrTerm(child, "Terminates", "ir");
                else if (child.getNodeName().equals("ic_term"))
                    pred = processRcIrTerm(child, "Terminates", "ic");
//                else if (child.getNodeName().equals("as_term"))
//                    pred = processAsTerm(child, "Terminates");
                else if (child.getNodeName().equals("fluent"))
                    pred.setFluent(processFluent(child));
                else if (child.getNodeName().equals("timeVar")) {
                    String timeVarName = child.getFirstChild().getFirstChild().getNodeValue();
                    pred.setTimeVarName(timeVarName);
                    if (quantifiers.containsKey(timeVarName)) {
                        String quantifier = (String) quantifiers.get(timeVarName);
                        if (quantifier.equals("forall"))
                            pred.setQuantifier(Constants.QUANT_FORALL);
                        else
                            pred.setQuantifier(Constants.QUANT_EXIST);
                    }
                    if (!pred.getPrefix().equals("as")) {
                        TimeExpression tex = new TimeExpression(pred.getTimeVarName());
                        tex.setTimeValue(Constants.RANGE_LB);
                        pred.setLowBound(new TimeExpression(tex));
                        tex.setTimeValue(Constants.RANGE_UB);
                        tex.setNumber(1);
                        pred.setUpBound(new TimeExpression(tex));
                    }
                }
                /**
                 * There might be the case that a predicate has a time expression as a time variable. In such cases, the
                 * code generates an artificial time variable existentially quantified whose upper and lower boundaries
                 * is equal to the specified time expression.
                 */
                else if (child.getNodeName().equals(Constants.TIME_EXPRESSION)) {
                    TimeExpression timeExp = processTimeExpression(child);

                    pred.setQuantifier(Constants.QUANT_EXIST);
                    pred.setTimeVarName(Constants.ARTIFICIAL_TIME_VARIABLE + artificialTimeVarCounter);
                    pred.setLowBound(new TimeExpression(timeExp));
                    pred.setUpBound(new TimeExpression(timeExp));

                    artificialTimeVarCounter++;
                }
            }
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return pred;
    }

    /**
     * Receives a W3C Node object that contains an XML holdsAt element and returns Predicate object.
     * 
     * @param node -
     *            W3C node that contains holdsAt in XML formula
     * @return
     */

    private Predicate processHoldsAt(Node node, String ecName) {
        Predicate pred = null;
        try {
            pred = new Predicate(ecName, "", "", "", "");
            NodeList childs = node.getChildNodes();
            for (int i = 0; i < childs.getLength(); i++) {
                Node child = childs.item(i);
                if (child.getNodeName().equals(Constants.FLUENT)) {
                    pred.setFluent(processFluent(child));
                }
                else if (child.getNodeName().equals(Constants.TIME_VARIABLE)) {
                    TimeVar timeVar = processTimeVariable(child);
                    pred.setTimeVarName(timeVar.getName());
                    if (quantifiers.containsKey(timeVar.getName())) {
                        String quantifier = (String) quantifiers.get(timeVar.getName());
                        if (quantifier.equals(Constants.QUANT_FORALL)) {
                            pred.setQuantifier(Constants.QUANT_FORALL);
                        }
                        else {
                            pred.setQuantifier(Constants.QUANT_EXIST);
                        }
                    }
                }
                /*
                 * There might be the case that a holdsAt predicate has a time expression as a time variable. In such
                 * cases, the code generates an artificial time variable existentially quantified whose upper and lower
                 * boundaries is equal to the specified time expression.
                 */
                else if (child.getNodeName().equals(Constants.TIME_EXPRESSION)) {
                    TimeExpression timeExp = processTimeExpression(child);

                    pred.setQuantifier(Constants.QUANT_EXIST);
                    pred.setTimeVarName(Constants.ARTIFICIAL_TIME_VARIABLE + artificialTimeVarCounter);
                    pred.setLowBound(new TimeExpression(timeExp));
                    pred.setUpBound(new TimeExpression(timeExp));

                    artificialTimeVarCounter++;
                }

            }
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return pred;
    }

    /**
     * @param node
     * @return
     */
    private ConstrainedPredicatesSubformula processConstrainedPredicatesSubformula(Node node) {
        ConstrainedPredicatesSubformula subformula = new ConstrainedPredicatesSubformula();

        NamedNodeMap attributes = node.getAttributes();
        if (attributes.getLength() > 0) {
            for (int a = 0; a < attributes.getLength(); a++) {
                Node attribute = attributes.item(a);
                if (attribute.getNodeName().equals(Constants.NEGATED)) {
                    String negatedAttributeValue = attribute.getNodeValue();
                    if (negatedAttributeValue.equals(Constants.FALSE)) {
                        subformula.setNegated(false);
                    }
                    else {
                        subformula.setNegated(true);
                    }
                }
            }
        }

        NodeList children = node.getChildNodes();
        if (children.getLength() > 0) {
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (child.getNodeName().equalsIgnoreCase(Constants.CONSTRAINED_PREDICATES)) {
                    NodeList gChildren = child.getChildNodes();
                    if (gChildren.getLength() > 0) {
                        for (int j = 0; j < gChildren.getLength(); j++) {
                            Node gChild = gChildren.item(j);
                            if (gChild.getNodeName().equalsIgnoreCase(Constants.PREDICATE)) {
                                NodeList ggChildren = gChild.getChildNodes();
                                if (ggChildren.getLength() > 0) {
                                    for (int k = 0; k < ggChildren.getLength(); k++) {
                                        Node ggChild = ggChildren.item(k);
                                        Predicate pred = null;
                                        /**
                                         * ATTENTION: In the current version only holdsAt predicates can be used as
                                         * constrained predicates in a sub-formula containing constrained predicates
                                         */
                                        if (ggChild.getNodeName().equalsIgnoreCase(Constants.PRED_HOLDSAT)) {
                                            pred = processHoldsAt(ggChild, Constants.PRED_HOLDSAT);
                                        }
                                        if (pred != null) {
                                            NamedNodeMap gCildAttributes = gChild.getAttributes();
                                            logger.debug(gCildAttributes.getLength());
                                            logger.debug(gCildAttributes.item(0).toString());
                                            // logger.debug(attributes.item(0).getNodeValue());
                                            Node negated = gCildAttributes.getNamedItem("negated");

                                            if (negated.getNodeValue().equals("true"))
                                                pred.setNegated(true);
                                            else
                                                pred.setNegated(false);

                                            Node unconstrained = gCildAttributes.getNamedItem("unconstrained");

                                            if (unconstrained.getNodeValue().equals("true"))
                                                pred.setUnconstrained(true);
                                            else
                                                pred.setUnconstrained(false);

                                            // Gets the recordable attribute from the EC XML document
                                            Node recordable = gCildAttributes.getNamedItem("recordable");

                                            if (recordable != null)
                                                if (recordable.getNodeValue().equals("true"))
                                                    pred.setRecordable(true);
                                                else
                                                    pred.setRecordable(false);
                                            else
                                                pred.setRecordable(false);

                                            // Gets the abducible attribute from the EC XML document
                                            Node abducible = gCildAttributes.getNamedItem("abducible");

                                            if (abducible != null)
                                                if (abducible.getNodeValue().equals("true"))
                                                    pred.setAbducible(true);
                                                else
                                                    pred.setAbducible(false);
                                            else
                                                pred.setAbducible(false);

                                            subformula.addConstrainedPredicate(pred);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (child.getNodeName().equalsIgnoreCase(Constants.CONSTRAINTS)) {
                    NodeList grandChildren = child.getChildNodes();
                    if (grandChildren.getLength() > 0) {
                        for (int j = 0; j < grandChildren.getLength(); j++) {
                            Node grandChild = grandChildren.item(j);
                            /**
                             * ATTENTION: In the current version only relational predicates can be considered as
                             * constraints in a sub-formula containing constrained predicates
                             */
                            if (grandChild.getNodeName().equals(Constants.CONSTRAINT)) {
                                Predicate constraint = processRelationalPredicate(grandChild);
                                subformula.addConstraint(constraint);
                            }
                        }
                    }
                }
            }
        }

        return subformula;
    }

    /**
     * Receives a W3C Node object that contains an XML Function element and returns Function object.
     * 
     * @param node -
     *            W3C node that contains function in XML formula
     * @return
     */
    private Function processFunction(Node node) {
        Function func = null;
        try {
            func = new Function();
            NodeList childs = node.getChildNodes();
            for (int i = 0; i < childs.getLength(); i++) {
                Node child = childs.item(i);
                if (child.getNodeName().equals(Constants.NAME))
                    func.setOperationName(child.getFirstChild().getNodeValue());
                else if (child.getNodeName().equals(Constants.PARTNER))
                    func.setPartner(child.getFirstChild().getNodeValue());
                else if (child.getNodeName().equals(Constants.VARIABLE)) {
                    Variable var = processVariable(child);
                    func.addParameter(var);
                }
                else if (child.getNodeName().equals(Constants.OPERATION_CALL)) {
                    Function paramFunc = processFunction(child);
                    func.addParameter(paramFunc);
                }
                else if (child.getNodeName().equals(Constants.DATA_TYPE_CONST)) {
                    Variable var = processConstant(child);
                    func.addParameter(var);
                }
                else if (child.getNodeName().equals(Constants.TIME_VARIABLE)) {
                    TimeVar timeVar = processTimeVariable(child);
                    Variable tVarCopy = new Variable(timeVar.getName(), Constants.DATA_TYPE_LONG, timeVar.getValue());
                    func.addParameter(tVarCopy);
                }
            }
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return func;

    }

    /**
     * Receives a W3C Node object that contains an XML fluent element and returns Fluent object. It should be noted that
     * only generic fluent which has the form FLUENT_NAME(var1, var2....) is supported. This method processes the
     * generic fluent.
     * 
     * @param node -
     *            W3C node that contains generic fluent in XML formula
     * @return
     */
    /*
    private Fluent processGenericFluent(Node node) {
        Fluent fluent = new Fluent(node.getNodeName());
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeName().equals(Constants.VARIABLE)) {
                fluent.addArgument(processVariable(child));
            }
            else if (child.getNodeName().equals(Constants.OPERATION_CALL)) {
                fluent.addArgument(processFunction(child));
            }
        }
        return fluent;
    }
	*/
    /**
     * Receives a W3C Node object that contains an XML fluent element and returns Fluent object.
     * 
     * @param node -
     *            W3C node that contains fluent in XML formula
     * @return
     */
    private Fluent processFluent(Node node) {
        Fluent fluent = null;
        Node name = node.getAttributes().getNamedItem(Constants.NAME);
        if (name != null) {
            fluent = new Fluent(name.getNodeValue());
            NodeList children = node.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (child.getNodeName().equals(Constants.ARGUMENTS)) {
                    NodeList gChildren = child.getChildNodes();
                    for (int j = 0; j < gChildren.getLength(); j++) {
                        Node gChild = gChildren.item(j);
                        if (gChild.getNodeName().equals(Constants.VARIABLE)) {
                            fluent.addArgument(processVariable(gChild));
                        }
                        else if (gChild.getNodeName().equals(Constants.OPERATION_CALL)) {
                            fluent.addArgument(processFunction(gChild));
                        }
                        else if (gChild.getNodeName().equals(Constants.DATA_TYPE_CONST)) {
                            fluent.addArgument(processConstant(gChild));
                        }
                    }
                }
            }
        }
        return fluent;
    }

    /**
     * Receives a W3C Node object that contains an XML TimeRelation element and returns TimePredicate object.
     * 
     * @param node -
     *            W3C node that contains TimeRelation in XML formula
     * @return
     */
    /*
    private TimePredicate processTimeRelation(Node node) {
        TimePredicate timePred = new TimePredicate();
        try {
            HashMap relNameMap = new HashMap();
            relNameMap.put("timeGreaterThanEqualTo", Constants.GREATER_THAN_EQUAL);
            relNameMap.put("timeGreaterThan", Constants.GREATER_THAN);
            relNameMap.put("timeLessThanEqualTo", Constants.LESS_THAN_EQUAL);
            relNameMap.put("timeLessThan", Constants.LESS_THAN);
            timePred.setOperator((String) relNameMap.get(node.getNodeName()));
            timePred.setLeftExpression(processTimeExpression(node.getFirstChild()));
            timePred.setRightExpression(processTimeExpression(node.getLastChild()));
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return timePred;
    }
	*/
    /**
     * Receives a W3C Node object that contains an XML relationalPredicate element and returns Predicate object It
     * should be noted that the same Predicate object that is used to model Happens, Initiates etc. is used to model
     * relational predicate. In case of Relational Predicate ecName field of the Predicate is set to
     * Constants.PRED_RELATION and prefix field of the Predciate is used to hold type of the relation (i.e. relation is
     * between two variables, or between a variable and a constant etc.) .
     * 
     * @param node -
     *            W3C node that contains relationalPredicate in XML formula
     * @return
     */
    private Predicate processRelationalPredicate(Node node) {
        Predicate pred = null;
        try {
            HashMap relName = new HashMap();
            relName.put("equal", Constants.EQUAL);
            relName.put("notEqualTo", Constants.NOT_EQUAL);
            relName.put("lessThan", Constants.LESS_THAN);
            relName.put("lessThanEqualTo", Constants.LESS_THAN_EQUAL);
            relName.put("greaterThan", Constants.GREATER_THAN);
            relName.put("greaterThanEqualTo", Constants.GREATER_THAN_EQUAL);
            String relationType = "";
            pred = new Predicate(Constants.PRED_RELATION, "", "", "", "");
            Node relation = node.getFirstChild();
            pred.setOperationName((String) relName.get(relation.getNodeName()));
            NodeList childs = relation.getChildNodes();
            for (int i = 0; i < childs.getLength(); i++) {
                Node child = childs.item(i);
                Node operand = child.getFirstChild();
                if (operand.getNodeName().equals(Constants.VARIABLE)) {
                    relationType += "v";
                    pred.addVariable(processVariable(operand));
                }
                else if (operand.getNodeName().equals(Constants.EXPRESSION)) {
                    relationType += "v";
                    pred.addVariable(processExpVariable(operand));
                }
                else if (operand.getNodeName().equals(Constants.DATA_TYPE_CONST)) {
                    relationType += "c";
                    pred.addVariable(processConstant(operand));
                }
                else if (operand.getNodeName().equals(Constants.OPERATION_CALL)) {
                    relationType += "f";
                    pred.addFunction(processFunction(operand));
                }
                else if (child.getNodeName().equals(Constants.TIME_VARIABLE)) {
                    relationType += "v";
                    pred.addVariable(processVariable(operand));
                    TimeVar timeVar = processTimeVariable(operand);
                    Variable tVarCopy = new Variable(timeVar.getName(), Constants.DATA_TYPE_LONG, timeVar.getValue());
                    pred.addVariable(tVarCopy);
                }
            }
            pred.setPrefix(relationType);
            Node time = node.getLastChild();
            String timeVarName = time.getFirstChild().getFirstChild().getNodeValue();
            pred.setTimeVarName(timeVarName);
            if (quantifiers.containsKey(timeVarName)) {
                String quantifier = (String) quantifiers.get(timeVarName);
                if (quantifier.equals("forall"))
                    pred.setQuantifier(Constants.QUANT_FORALL);
                else
                    pred.setQuantifier(Constants.QUANT_EXIST);
            }
            TimeExpression tex = new TimeExpression(pred.getTimeVarName());
            tex.setTimeValue(Constants.RANGE_LB);
            pred.setLowBound(new TimeExpression(tex));
            tex.setTimeValue(Constants.RANGE_UB);
            tex.setNumber(1);
            pred.setUpBound(new TimeExpression(tex));
            /*
             * logger.debug("********************PREDICATE**************"); logger.debug("name: "+pred.getEcName());
             * logger.debug("EPUT: "+pred.getEPUT()); logger.debug("name: "+pred.getEcName()); logger.debug("ID:
             * "+pred.getID()); logger.debug("LBName: "+pred.getLBName()); logger.debug("LBNumber:
             * "+pred.getLBNumber()); logger.debug("OperationName: "+pred.getOperationName()); logger.debug("partnerID:
             * "+pred.getPartnerId()); logger.debug("prefix: "+pred.getPrefix()); logger.debug("Quantifier:
             * "+pred.getQuantifier()); logger.debug("rangeLB: "+pred.getRangeLB()); logger.debug("rangeUB:
             * "+pred.getRangeUB()); logger.debug("source: "+pred.getSource()); logger.debug("time: "+pred.getTime());
             * logger.debug("time2: "+pred.getTime2()); logger.debug("timeVarName: "+pred.getTimeVarName());
             * logger.debug("TruthVal: "+pred.getTruthVal()); logger.debug("UBName: "+pred.getUBName());
             * logger.debug("UBNumber: "+pred.getUBNumber()); logger.debug("----------Variables-------------"); for(int
             * i =0;i<pred.getVariables().size();i++){ logger.debug("\t------var: "+i); logger.debug("\t name:
             * "+pred.getVariable(i).getName()); logger.debug("\t ConstName: "+pred.getVariable(i).getConstName());
             * logger.debug("\t type: "+pred.getVariable(i).getType()); logger.debug("\t value:
             * "+pred.getVariable(i).getValue()); logger.debug("\t Objtype: "+pred.getVariable(i).getObjType());
             * logger.debug("\t ObjValue: "+pred.getVariable(i).getObjValue()); logger.debug("\t-----------"+i); }
             * logger.debug("--------------------------------");
             */
        }
        catch (Exception exp) {
            exp.printStackTrace();
        }
        return pred;
    }

    /**
     * Receives Body or Head of a formula as W3C node parses the predicates and stores the predicates in a list.
     * 
     * @param node -
     *            W3C node that contains Body or Head of XML formula
     * @param list -
     *            List where parsed predicates are stored
     */
    /*
    private void processHeadBody(Node node, ArrayList list) {
        // ArrayList list = new ArrayList();
        NodeList childs = node.getChildNodes();
        for (int i = 0; i < childs.getLength(); i++) {
            Node child = childs.item(i);
            if (child.getNodeName().equals("predicate")) {
                Node gChild = child.getFirstChild();
                Predicate pred = null;
                if (gChild.getNodeName().equals("happens"))
                    pred = processHappens(gChild);
                else if (gChild.getNodeName().equals("initiates"))
                    pred = processInitiates(gChild);
                else if (gChild.getNodeName().equals("terminates"))
                    pred = processTerminates(gChild);
                else if (gChild.getNodeName().equals("holdsAt"))
                    pred = processHoldsAt(gChild, "HoldsAt");
                else if (gChild.getNodeName().equals("initially"))
                    pred = processHoldsAt(gChild, "Initially");

                if (pred != null) {
                    NamedNodeMap attributes = child.getAttributes();
                    logger.debug(attributes.getLength());
                    logger.debug(attributes.item(0).toString());
                    // logger.debug(attributes.item(0).getNodeValue());
                    Node negated = attributes.getNamedItem("negated");

                    if (negated.getNodeValue().equals("true"))
                        pred.setNegated(true);
                    else
                        pred.setNegated(false);

                    Node unconstrained = attributes.getNamedItem("unconstrained");

                    if (unconstrained.getNodeValue().equals("true"))
                        pred.setUnconstrained(true);
                    else
                        pred.setUnconstrained(false);

                    // Gets the recordable attribute from the EC XML document
                    Node recordable = attributes.getNamedItem("recordable");

                    if (recordable != null)
                        if (recordable.getNodeValue().equals("true"))
                            pred.setRecordable(true);
                        else
                            pred.setRecordable(false);
                    else
                        pred.setRecordable(false);

                    // Gets the abducible attribute from the EC XML document
                    Node abducible = attributes.getNamedItem("abducible");

                    if (abducible != null)
                        if (abducible.getNodeValue().equals("true"))
                            pred.setAbducible(true);
                        else
                            pred.setAbducible(false);
                    else
                        pred.setAbducible(false);

                    list.add(pred);
                }
            }
            else if (child.getNodeName().equals("relationalPredicate")) {
                list.add(processRelationalPredicate(child));
            }
            else if (child.getNodeName().equals("timePredicate")) {
                // TimePredicate timePred = ;
                list.add(processTimeRelation(child.getFirstChild()));
            }
        }
    }
    */

    /**
     * Receives Body or Head of a formula as W3C node parses the predicates and sub-formulas and stores the predicates
     * in corresponding lists.
     * 
     * @param node -
     *            W3C node that contains Body or Head of XML formula
     * @param list -
     *            List where parsed predicates are stored
     * @param subformulasList -
     *            List where parsed sub-formulas are stored
     * @param bodyOrHead -
     *            String indicating which part of the formula is being processed
     */
    private void processHeadBody(Node node, ArrayList list, ArrayList<ConstrainedPredicatesSubformula> subformulasList,
            String bodyOrHead) {
        // ArrayList list = new ArrayList();
        NodeList childs = node.getChildNodes();
        for (int i = 0; i < childs.getLength(); i++) {
            Node child = childs.item(i);
            if (child.getNodeName().equals("predicate")) {
                Node gChild = child.getFirstChild();
                Predicate pred = null;
                if (gChild.getNodeName().equals("happens"))
                    pred = processHappens(gChild);
                else if (gChild.getNodeName().equals("initiates"))
                    pred = processInitiates(gChild);
                else if (gChild.getNodeName().equals("terminates"))
                    pred = processTerminates(gChild);
                else if (gChild.getNodeName().equals("holdsAt"))
                    pred = processHoldsAt(gChild, "HoldsAt");
                else if (gChild.getNodeName().equals("initially"))
                    pred = processHoldsAt(gChild, "Initially");

                if (pred != null) {
                    NamedNodeMap attributes = child.getAttributes();
                    logger.debug(attributes.getLength());
                    logger.debug(attributes.item(0).toString());
                    // logger.debug(attributes.item(0).getNodeValue());
                    Node negated = attributes.getNamedItem("negated");

                    if (negated.getNodeValue().equals("true"))
                        pred.setNegated(true);
                    else
                        pred.setNegated(false);

                    Node unconstrained = attributes.getNamedItem("unconstrained");

                    if (unconstrained.getNodeValue().equals("true"))
                        pred.setUnconstrained(true);
                    else
                        pred.setUnconstrained(false);

                    // Gets the recordable attribute from the EC XML document
                    Node recordable = attributes.getNamedItem("recordable");

                    if (recordable != null)
                        if (recordable.getNodeValue().equals("true"))
                            pred.setRecordable(true);
                        else
                            pred.setRecordable(false);
                    else
                        pred.setRecordable(false);

                    // Gets the abducible attribute from the EC XML document
                    Node abducible = attributes.getNamedItem("abducible");

                    if (abducible != null)
                        if (abducible.getNodeValue().equals("true"))
                            pred.setAbducible(true);
                        else
                            pred.setAbducible(false);
                    else
                        pred.setAbducible(false);

                    list.add(pred);
                }
            }
            else if (child.getNodeName().equals("relationalPredicate")) {
                list.add(processRelationalPredicate(child));
            }
            else if (child.getNodeName().equals("timePredicate")) {
                // TimePredicate timePred = ;
                //list.add(processTimeRelation(child.getFirstChild()));
            }
            else if (child.getNodeName().equals(Constants.CONSTRAINED_PREDICATES_SUBFORMULA)) {
                ConstrainedPredicatesSubformula subformula = processConstrainedPredicatesSubformula(child);

                subformula.setPositioningInFormula(bodyOrHead);

                subformulasList.add(subformula);
            }
        }
    }

    /**
     * Receives a W3C Node that contains XML formula and returns Formula object.
     * 
     * @param formula -
     *            W3C node that contains XML formula.
     * @return
     */
    protected Formula processFormula(Node formula) {
        String id = "";
        Node formulaId = formula.getAttributes().getNamedItem("formulaId");
        if (formulaId != null)
            id += formulaId.getNodeValue();
        Node forChecking = formula.getAttributes().getNamedItem("forChecking");
        Node diagnosisRequired = formula.getAttributes().getNamedItem("diagnosisRequired");
        Node threatDetectionRequired = formula.getAttributes().getNamedItem("threatDetectionRequired");
        ArrayList body = new ArrayList();
        ArrayList head = new ArrayList();
        /**
         * Constrained Predicates sub-formulas are supported
         */
        ArrayList<ConstrainedPredicatesSubformula> subformulasList = new ArrayList<ConstrainedPredicatesSubformula>();
        NodeList childs = formula.getChildNodes();
        for (int i = 0; i < childs.getLength(); i++) {
            Node node = childs.item(i);
            if (node.getNodeName().equals("quantification")) {
                processQuantifier(node);
            }
            else if (node.getNodeName().equals("body")) {
                processHeadBody(node, body, subformulasList, Constants.BODY);
            }
            else if (node.getNodeName().equals("head")) {
                processHeadBody(node, head, subformulasList, Constants.HEAD);
            }
        }
        Formula vFormula = new Formula(id, body, head, subformulasList);
        if (forChecking != null) {
            String check = forChecking.getNodeValue();
            if (check.startsWith("f"))
                vFormula.setForChecking(false);
            else
                vFormula.setForChecking(true);
        }
        if (diagnosisRequired != null) {
            String check = diagnosisRequired.getNodeValue();
            if (check.startsWith("f"))
                vFormula.setDiagnosisRequired(false);
            else
                vFormula.setDiagnosisRequired(true);
        }
        if (threatDetectionRequired != null) {
            String check = threatDetectionRequired.getNodeValue();
            if (check.startsWith("f"))
                vFormula.setThreatDetectionRequired(false);
            else
                vFormula.setThreatDetectionRequired(true);
        }
        Node type = formula.getAttributes().getNamedItem("type");
        if (type != null) {
            String check = type.getNodeValue();
            if (check.startsWith("P"))
                vFormula.setFormulaType(Constants.TYPE_PAST);
            else
                vFormula.setFormulaType(Constants.TYPE_FUTURE);
        }
        return vFormula;
    }

    /**
     * Process the root node. It is the starting point of processing the whole tree.
     * 
     * @param root
     *            the root node of the tree
     */

    private LinkedHashMap processRootNode(Node root) {
        LinkedHashMap formulas = new LinkedHashMap();
        NodeList childs = root.getChildNodes();
        for (int i = 0; i < childs.getLength(); i++) {
            Node node = childs.item(i);
            if (node.getNodeName().equals("formula")) {
                Formula formula = processFormula(node);
                formulas.put(formula.getFormulaId(), formula);
            }
        }
        return formulas;
    }

    /**
     * Remove the empty text nodes from a DOM tree. and returns the root of the modified tree.
     * 
     * @param root
     *            the root of the DOM tree to be modified.
     * @return the root of the modified tree.
     */
    protected Node resolveEmptyNodes(Node root) {
        if (root.hasChildNodes()) {
            NodeList children = root.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                String val = child.getNodeValue();
                val = (val == null ? "" : val.trim());
                if (child.getNodeName().equals("#text") && val.equals("")) {
                    root.removeChild(child);
                    i--;
                }
                else
                    resolveEmptyNodes(child);
            }
        }
        return root;
    }
}
