/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.monitor;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.TimeExpression;

/**
 * This class holds the constraint matrix resulted from all the ranges of predicates in a formula. This matrix is
 * further used in Simplex method.
 * 
 * @author Khaled Mahbub
 * 
 */
public class ConstraintMatrix {

    // logger
    private static Logger logger = Logger.getLogger(ConstraintMatrix.class);

    private String formulaId = "";
    private ArrayList<String> timeVarNames = new ArrayList<String>();

    // <time variable name, partnerId>
    private LinkedHashMap<String, String> timeVars = new LinkedHashMap<String, String>();

    // Coefficients table A[row][column] where column is the number of time variables
    private long A[][];
    // Values table
    private long B[];
    private int rowCount = 0;
    private int colCount = 0;

    /**
     * Constructor for given template object.
     * 
     * @param template
     */
    public ConstraintMatrix(Template template) {
        this.formulaId = new String(template.getFormulaId());
        initializeMatrix(template);
    }

    private void initializeMatrix(Template template) {
        for (int i = 0; i < template.totalPredicates(); i++) {
            Predicate pred = template.getPredicate(i);
            if (!pred.getEcName().equals(Constants.PRED_RELATION)) {
                if (!timeVars.containsKey(pred.getTimeVarName())) {
                    timeVars.put(pred.getTimeVarName(), pred.getPartnerId());
                    timeVarNames.add(pred.getTimeVarName());
                }
            }
        }
        colCount = timeVars.size();
        rowCount = calculateRowNumbers(template);
        A = new long[rowCount][colCount];
        B = new long[rowCount];

        for (int i = 0, row = 0; i < template.totalPredicates(); i++) {
            Predicate pred = template.getPredicate(i);
            if ((pred.getEcName().equals(Constants.PRED_HAPPENS) || pred.getEcName().equals(Constants.PRED_INITIATES))
                    && !pred.isUnconstrained()) {
                String timeVariableName = pred.getTimeVarName();
                /*
                 * in current implementation, only one time variable is allowed in time expression so conversion to
                 * matrix is very easy. But this should be updated when multiple time variables will be allowed in time
                 * expression.
                 */
                int timeVariableColumnIndex = getColumnIndex(timeVariableName);
                // considering lower bound first
                TimeExpression lowerBound = pred.getLowBound();
                if (!timeVariableName.equals(lowerBound.getTimeVarName())) {
                    int lowerBoundTimeVariableColumnIndex = getColumnIndex(lowerBound.getTimeVarName());
                    for (int col = 0; col < getColumnCount(); col++) {
                        // in current implementation allowed coefficient of time variable in TimeExpression is 1.
                        if (col == timeVariableColumnIndex) {
                            A[row][col] = -1;
                        }
                        // in current implementation allowed coefficient of time variable in TimeExpression is 1.
                        else if (col == lowerBoundTimeVariableColumnIndex) {
                            A[row][col] = 1;
                        }
                        else {
                            A[row][col] = 0;
                        }
                    }
                    // if(lb.getOperator() == Constants.PLUS)
                    B[row] = ((-1) * lowerBound.getNumber());
                    // else if(lb.getOperator() == Constants.MINUS)
                    // B[row] = (int)lb.getNumber();
                    row++;
                }
                // considering upper bound
                TimeExpression upperBound = pred.getUpBound();
                if (!timeVariableName.equals(upperBound.getTimeVarName())) {
                    int upperBoundTimeVariableColumnIndez = getColumnIndex(upperBound.getTimeVarName());
                    for (int col = 0; col < getColumnCount(); col++) {
                        // in current implementation allowed coefficient of time
                        // variable in TimeExpression is 1.
                        if (col == timeVariableColumnIndex) {
                            A[row][col] = 1;
                        }
                        // in current implementation allowed coefficient of time
                        // variable in TimeExpression is 1.
                        else if (col == upperBoundTimeVariableColumnIndez) {
                            A[row][col] = -1;
                        }
                        else
                            A[row][col] = 0;
                    }
                    // if(rb.getOperator() == Constants.PLUS)
                    B[row] = upperBound.getNumber();
                    // else if(rb.getOperator() == Constants.MINUS)
                    // B[row] = (int)((-1) * rb.getNumber());
                    row++;
                }
                // considering abduced predicates
                if (pred.getSource().equals(Constants.SOURCE_ABD) && pred.getTime2() != Constants.TIME_UD) {
                    for (int col = 0; col < getColumnCount(); col++) {
                        if (col == timeVariableColumnIndex) {
                            A[row][col] = -1; // in current implementation allowed coefficient of time variable in
                                                // TimeExpression is 1
                            A[((int) row + 1)][col] = 1;
                        }
                        else {
                            A[row][col] = 0;
                            A[((int) row + 1)][col] = 0;
                        }
                    }
                    B[row] = ((-1) * pred.getTime1());
                    B[((int) row + 1)] = pred.getTime2();
                    row += 2;

                }
            }
        }
        /*
         * for(int j=0; j<rowCount; j++){ for(int k=0; k<colCount; k++){ logger.debug(A[j][k]); }
         * logger.debug(B[j]+"\n"); }
         */
    }

    private int calculateRowNumbers(Template t) {
        int r = 0;
        logger.debug("calculating rows for the template: " + t.getTemplateId());
        for (int i = 0; i < t.totalPredicates(); i++) {
            Predicate pred = t.getPredicate(i);
            if ((pred.getEcName().equals(Constants.PRED_HAPPENS) || pred.getEcName().equals(Constants.PRED_INITIATES))
                    && !pred.isUnconstrained()) {
                logger.debug("\n" + pred.toString(true) + "\n");
                if (pred.getSource().equals(Constants.SOURCE_ABD) && pred.getTime2() != Constants.TIME_UD) {
                    logger.debug("abduced found: " + pred.toString(true) + "\n");
                    logger.debug("ConstraintTest");
                    r++;
                    r++;
                }
                String timeVariableName = pred.getTimeVarName();
                TimeExpression lowerBound = pred.getLowBound();
                TimeExpression upperBound = pred.getUpBound();

                if (!timeVariableName.equals(lowerBound.getTimeVarName())) {
                    r++;
                }
                if (!timeVariableName.equals(upperBound.getTimeVarName())) {
                    r++;
                }
            }
        }
        return r;
    }

    /**
     * Retrieves the channel name, which is actually the name of the time variable.
     * 
     * @param timeVarName
     * @return
     */
    public String getChannelName(String timeVarName) {
        return timeVars.containsKey(timeVarName) ? timeVars.get(timeVarName) : "";
    }

    /**
     * Retrieves the ColumnIndex, which is actually the name of the time variable.
     * 
     * @param timeVarName
     * @return
     */
    public int getColumnIndex(String timeVarName) {
        return timeVarNames.contains(timeVarName) ? timeVarNames.indexOf(timeVarName) : -1;
    }

    /**
     * Returns the number of columns taken into account.
     * 
     * @return
     */
    public int getColumnCount() {
        return colCount;
    }

    /**
     * Returns the number of rows taken into account.
     * 
     * @return
     */
    public int getRowCount() {
        return rowCount;
    }

    /**
     * Returns the A matrix i.e., the matrix of Coefficients.
     * 
     * @return
     */
    public long[][] getA() {
        return A;
    }

    /**
     * Sets the A matrix i.e., the matrix of Coefficients.
     * 
     * @param a
     */
    public void setA(long[][] a) {
        A = a;
    }

    /**
     * Returns the B matrix i.e., the values matrix.
     * 
     * @return
     */
    public long[] getB() {
        return B;
    }

    /**
     * Sets the B matrix i.e., the values matrix.
     * 
     * @param b
     */
    public void setB(long[] b) {
        B = b;
    }

    /**
     * Returns the formula id.
     * 
     * @return
     */
    public String getFormulaId() {
        return formulaId;
    }

    /**
     * Sets the formula id.
     * 
     * @param formulaId
     */
    public void setFormulaId(String formulaId) {
        this.formulaId = formulaId;
    }

    /**
     * Returns the list of time variable names.
     * 
     * @return
     */
    public ArrayList getTimeVarNames() {
        return timeVarNames;
    }

    /**
     * Sets the list of time variable names.
     * 
     * @param timeVarNames
     */
    public void setTimeVarNames(ArrayList timeVarNames) {
        this.timeVarNames = timeVarNames;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = new String();

        // print the header
        for (String timeVariableName : timeVarNames) {
            result += timeVariableName + "\t";
        }

        result += "\n";

        // print coefficients table
        for (int r = 0; r < A.length; r++) {
            for (int c = 0; c < A[r].length; c++) {
                result += A[r][c] + "\t";
            }

            result += "<=\t " + B[r] + "\n";
        }

        return result;
    }
}
