/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * TimePredicate.java
 *
 * Created on 21 March 2005, 15:04
 */

package uk.ac.city.soi.everest.bse;

import uk.ac.city.soi.everest.core.Constants;
import uk.ac.city.soi.everest.core.TimeExpression;

/**
 * This class is used to model time predicates, i.e. relational predicate that involves only time variable. it is
 * assumed here that only the following expression are permitted T1 +|- number < | > | <= | >= | != T1 +|- number.
 * Although in the schema expressions like T1 + T2 or T1 - T2 + number is permitted, it is not implemented.
 * 
 * @author am697
 */
public class TimePredicate {
    private String operator;
    private TimeExpression lTex;
    private TimeExpression rTex;

    /** Creates a new instance of TimePredicate */
    
    public TimePredicate() {
        lTex = new TimeExpression();
        rTex = new TimeExpression();
        operator = Constants.LESS_THAN;
    }
	
    /**
     * Creates a new instance of TimePredicate.
     * 
     * @param lName -
     *            name of the left hand time variable
     * @param lOperator -
     *            operator of the left hand time expression, (plus/minus)
     * @param lNumber -
     *            a number in the left hand time expression
     * @param operator -
     *            operator of the time relation, e.g. timeLessThan, timeGreaterThan etc.
     * @param rName -
     *            name of the right hand time variable
     * @param rOperator -
     *            operator of the right hand time expression, (plus/minus)
     * @param rNumber -
     *            a number in the right hand time expression
     */
    
    public TimePredicate(String lName, String lOperator, long lNumber, String operator, String rName, String rOperator,
            long rNumber) {
        this.lTex = new TimeExpression(lName, lOperator, lNumber);
        this.rTex = new TimeExpression(rName, rOperator, rNumber);
        // operator = new String(operator);
        this.operator = operator;
    }
	
    /**
     * This method will send a copy of this TimePredicate using only <, <= or = relation. i.e. if the relation is like
     * T1 > T2, it will return T2 < T1. Or if the relation is T1 >= T2 it will return T2 <= T1.
     */
    
    public TimePredicate getNormalisedCopy() {
        TimePredicate timePred = new TimePredicate();
        if (this.operator.equals(Constants.GREATER_THAN)) {
            timePred.setLeftExpression(rTex);
            timePred.setRightExpression(lTex);
            timePred.setOperator(Constants.LESS_THAN);
        }
        else if (this.operator.equals(Constants.GREATER_THAN_EQUAL)) {
            timePred.setLeftExpression(rTex);
            timePred.setRightExpression(lTex);
            timePred.setOperator(Constants.LESS_THAN_EQUAL);
        }
        else {
            timePred.setLeftExpression(lTex);
            timePred.setRightExpression(rTex);
            timePred.setOperator(operator);
        }
        return timePred;
    }
	
    /**
     * Sets the left time expression of the tiem relation.
     * 
     * @param tex -
     *            TimeExpression to be used as left time expression
     */
    
    public void setLeftExpression(TimeExpression tex) {
        lTex = new TimeExpression(tex);
    }
	
    /**
     * Sets the right time expression of the time relation.
     * 
     * @param tex -
     *            TimeExpression to be used as right time expression
     */
    
    public void setRightExpression(TimeExpression tex) {
        rTex = new TimeExpression(tex);
    }
	
    /**
     * Sets the operator of the time relation.
     * 
     * @param operator -
     *            operator in the time relation (e.g. timeGreaterThan, etc)
     */
    
    public void setOperator(String operator) {
        this.operator = new String(operator);
    }
	
    /**
     * Returns the name of the time variable in the left time expression.
     * 
     * @return
     */
    
    public String getLTName() {
        return lTex.getTimeVarName();
    }
	
    /**
     * Returns the name of the time variable in the right time expression.
     * 
     * @return
     */
    
    public String getRTName() {
        return rTex.getTimeVarName();
    }
	
    /**
     * Returns the left time expression.
     * 
     * @return
     */
    
    public TimeExpression getLTimeExpression() {
        return lTex;
    }
	
    /**
     * Returns the right time expression.
     * 
     * @return
     */
    
    public TimeExpression getRTimeExpression() {
        return rTex;
    }
	
    /**
     * Retruns the operator of the time relation.
     * 
     * @return
     */
    
    public String getOperator() {
        return operator;
    }
	
    /**
     * Returns a TimeExpression object if the left time expression of this time predicate contains timeVarName otherwise
     * returns null. The returned time expression is based on the left time expression of this time predicate, whose
     * operator and number is adjusted as follows: T1 +|- n1 > T2 +|- n2 -> T1 +|- n1 -|+ n2 - 1 > T2 so it will return
     * a time expression that realises T1 +|- n1 -|+ n2 - 1 T1 +|- n1 < T2 +|- n2 -> T1 +|- n1 -|+ n2 + 1 < T2 so it
     * will return a time expression that realises T1 +|- n1 -|+ n2 + 1 T1 +|- n1 >= T2 +|- n2 -> T1 +|- n1 -|+ n2 >= T2
     * so it will return a time expression that realises T1 +|- n1 -|+ n2 T1 +|- n1 <= T2 +|- n2 -> T1 +|- n1 -|+ n2 <=
     * T2 so it will return a time expression that realises T1 +|- n1 -|+ n2 T1 +|- n1 = T2 +|- n2 -> T1 +|- n1 -|+ n2 =
     * T2 so it will return a time expression that realises T1 +|- n1 -|+ n2
     * 
     * @param timeVarName -
     *            name of the time variable
     * @return
     */
    
    public TimeExpression getLeftTimeExpression(String timeVarName) {
        TimeExpression tex = null;
        if (rTex.getTimeVarName().equals(timeVarName)) {
            tex = new TimeExpression(lTex);
            if (rTex.getOperator().equals(Constants.PLUS))
                tex.setNumber(lTex.getNumber() - rTex.getNumber());
            else
                tex.setNumber(lTex.getNumber() + rTex.getNumber());
            if (operator.equals(Constants.LESS_THAN))
                tex.setNumber(tex.getNumber() + 1);
            else if (operator.equals(Constants.GREATER_THAN))
                tex.setNumber(tex.getNumber() - 1);
            if (tex.getNumber() < 0) {
                tex.setNumber(Math.abs(tex.getNumber()));
                tex.setOperator(Constants.MINUS);
            }
        }
        return tex;
    }
	
    /**
     * Returns a TimeExpression object if the right time expression of this time predicate contains timeVarName
     * otherwise returns null. The returned time expression is based on the right time expression of this time
     * predicate, whose operator and number is adjusted as follows: T1 +|- n1 > T2 +|- n2 -> T1 > T2 +|- n2 -|+ n1 + 1
     * so it will return a time expression that realises T1 +|- n2 -|+ n1 + 1 T1 +|- n1 < T2 +|- n2 -> T1 < T2 +|- n2
     * -|+ n1 - 1 so it will return a time expression that realises T1 +|- n2 -|+ n1 - 1 T1 +|- n1 >= T2 +|- n2 -> T1 >=
     * T2 +|- n2 -|+ n1 so it will return a time expression that realises T1 +|- n2 -|+ n1 T1 +|- n1 <= T2 +|- n2 -> T1 <=
     * T2 +|- n2 -|+ n1 so it will return a time expression that realises T1 +|- n2 -|+ n1 T1 +|- n1 = T2 +|- n2 -> T1 =
     * T2 +|- n2 -|+ n1 so it will return a time expression that realises T1 +|- n2 -|+ n1
     * 
     * @param timeVarName -
     *            name of the time variable
     * @return
     */
    
    public TimeExpression getRightTimeExpression(String timeVarName) {
        TimeExpression tex = null;
        if (lTex.getTimeVarName().equals(timeVarName)) {
            tex = new TimeExpression(rTex);
            if (lTex.getOperator().equals(Constants.PLUS))
                tex.setNumber(rTex.getNumber() - lTex.getNumber());
            else
                tex.setNumber(rTex.getNumber() + lTex.getNumber());
            if (operator.equals(Constants.LESS_THAN))
                tex.setNumber(tex.getNumber() - 1);
            else if (operator.equals(Constants.GREATER_THAN))
                tex.setNumber(tex.getNumber() + 1);
            if (tex.getNumber() < 0) {
                tex.setNumber(Math.abs(tex.getNumber()));
                tex.setOperator(Constants.MINUS);
            }
        }
        return tex;
    }
	
    /**
     * Returns the string representation of the time relation.
     */
    
    public String toString() {
        return lTex.toString() + " " + operator + " " + rTex.toString();
    }
    
}
