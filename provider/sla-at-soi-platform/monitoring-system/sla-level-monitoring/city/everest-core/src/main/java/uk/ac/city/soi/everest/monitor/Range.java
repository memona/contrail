/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * Range.java
 *
 * Created on 01 September 2004, 15:45
 */

package uk.ac.city.soi.everest.monitor;

/**
 * Models a simple time range. This is not used to model time range in EC predciate. This range holds two numeric values
 * (lower and upper limit) to define a range. This class is used only once in SeCSEEventGenerator to determine if a
 * given range is within another range.
 * 
 * @author am697
 */
public class Range {
    private long lowerBound;
    private long upperbound;
    private boolean isWithin;

    /**
     * Creates a new instance of Range.
     * 
     */
    public Range(long lowerBound, long upperbound) {
        this.lowerBound = lowerBound;
        this.upperbound = upperbound;
        isWithin = false;
    }

    /**
     * @deprecated Use {@link #getLowerBound()} instead
     */
    public long getTime1() {
        return getLowerBound();
    }

    /**
     * Returns the lower boundary.
     * 
     * @return
     */
    public long getLowerBound() {
        return lowerBound;
    }

    /**
     * @deprecated Use {@link #getUpperBound()} instead
     */
    public long getTime2() {
        return getUpperBound();
    }

    /**
     * Returns the upper boundary.
     * 
     * @return
     */
    public long getUpperBound() {
        return upperbound;
    }

    /**
     * Checks whether the range is specified within.
     * 
     * @return
     */
    public boolean isWithin() {
        return isWithin;
    }

    /**
     * Sets the within field value.
     * 
     * @param val
     */
    public void setWithin(boolean val) {
        isWithin = val;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    
    public String toString() {
        return "[" + lowerBound + "," + upperbound + "]";
    }
}
