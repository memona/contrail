/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Khaled Mahbub - K.Mahbub@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package uk.ac.city.soi.everest.database.event;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import uk.ac.city.soi.everest.core.Signature;
import uk.ac.city.soi.everest.database.EntityManagerUtil;
import uk.ac.city.soi.everest.er.LogEvent;
import uk.ac.city.soi.everest.monitor.Range;

/**
 * This class implements the EventEntityManagerInterface for MySQL db event table.
 * 
 * @author Davide Lorenzoli
 * 
 * @date 13 Nov 2008
 */
public class EventMySQLDatabase extends EntityManagerUtil implements EventEntityManagerInterface {
    // logger
    private static Logger logger = Logger.getLogger(EventMySQLDatabase.class);

    /**
     * constructor.
     * 
     * @param connection
     */
    public EventMySQLDatabase(Connection connection) {
        super(connection);
    }

    /*******************************************************************************************************************
     * INHERITED METHODS FROM EntityManagerInterface
     ******************************************************************************************************************/

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#count()
     */
    public int count() {
        return 0;
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#delete(java.lang.Object)
     */
    public void delete(Object object) {
        // TODO Auto-generated method stub
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#delete(java.lang.String)
     */
    public void delete(String eventId) {

    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#deleteAll()
     */
    public void deleteAll() {

    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#insert(java.lang.Object)
     */
    public void insert(Object object) {

    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#select(java.lang.String)
     */
    public Object select(String entityId) {
        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#selectAll()
     */
    public ArrayList<Object> selectAll() {
        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.EntityManagerInterface#update(java.lang.Object)
     */

    public void update(Object object) {
        // TODO Auto-generated method stub

    }

    /*******************************************************************************************************************
     * INHERITED METHODS FROM EntityManagerInterface
     ******************************************************************************************************************/

    /**
     * @see uk.ac.city.soi.everest.database.event.EventEntityManagerInterface#add(uk.ac.city.soi.everest.er.LogEvent)
     * 
     * @Deprecated replaced by {@link #insert(Object)}
     */

    @Deprecated
    public void add(LogEvent event) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            // delete the existing graph
            /*
             * if (get(event.getID()) != null) { delete(event.getID()); }
             */

            Blob blob = toBlob(event);

            pstmt =
                    connection
                            .prepareStatement("INSERT INTO event "
                                    + "(eventId, timestamp, ecName, prefix, operationName, partnerId, negated, abducible, recordable, eventObject, eventString) "
                                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            pstmt.setString(1, event.getID());
            pstmt.setLong(2, event.getTimestamp());
            pstmt.setString(3, event.getEcName());
            pstmt.setString(4, event.getPrefix());
            pstmt.setString(5, event.getOperationName());
            pstmt.setString(6, event.getPartnerId());
            pstmt.setBoolean(7, event.isNegated());
            pstmt.setBoolean(8, event.isAbducible());
            pstmt.setBoolean(9, event.isRecordable());
            pstmt.setBlob(10, blob);
            pstmt.setString(11, event.toString());

            pstmt.executeUpdate();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }                            
//        catch (SQLException ex) {
//            // handle any errors
////            logger.debug("SQLException: " + ex.getMessage());
////            logger.debug("SQLState: " + ex.getSQLState());
////            logger.debug("VendorError: " + ex.getErrorCode());
//            ex.printStackTrace();
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }
    }

    /**
     * @see uk.ac.city.soi.everest.database.event.EventEntityManagerInterface#get(java.lang.String)
     */
    public LogEvent get(String eventId) {

        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.event.EventEntityManagerInterface#getEvent(int, int)
     * 
     * @param offset
     *            the offset of the first row to return. The offset of the initial row is 0 (not 1).
     * @param numberRows
     *            the maximum number of rows to return.
     * @return all row between from offset to numberRows. For instance, if offset=5 and numberRows=10 it returns rows
     *         [6-15]
     */
    public ArrayList<LogEvent> getEvent(int offset, int numberRows) {

        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.event.EventEntityManagerInterface#getEvents(uk.ac.city.soi.everest.core.Signature)
     */
    public ArrayList<LogEvent> getEvents(Signature eventSignature) {

        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.event.EventEntityManagerInterface#getEvent(uk.ac.city.soi.everest.core.Signature,
     *      long)
     */

    public ArrayList<LogEvent> getEvent(Signature eventSignature, long timestamp) {
        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.event.EventEntityManagerInterface#getEvents(uk.ac.city.soi.everest.core.Signature,
     *      uk.ac.city.soi.everest.monitor.Range)
     */
    public ArrayList<LogEvent> getEvents(Signature eventSignature, Range timeRange) {

        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.event.EventEntityManagerInterface#getEvents(uk.ac.city.soi.everest.core.Signature,
     *      uk.ac.city.soi.everest.monitor.Range, java.lang.String)
     */

    public ArrayList<LogEvent> getEvents(Signature eventSignature, Range timeRange, String eventSourceId) {
        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.event.EventEntityManagerInterface#getEvents(uk.ac.city.soi.everest.monitor.Range)
     */
    public ArrayList<LogEvent> getEvents(Range timeRange) {

        return null;
    }

    /**
     * @see uk.ac.city.soi.everest.database.event.EventEntityManagerInterface#getEvents(java.lang.String)
     */
    public ArrayList<LogEvent> getEvents(String eventSourceId) {
        return new ArrayList<LogEvent>();
    }

    /**
     * @see uk.ac.city.soi.everest.database.event.EventEntityManagerInterface#getEvents(java.lang.String,
     *      uk.ac.city.soi.everest.monitor.Range)
     */
    public ArrayList<LogEvent> getEvents(String eventSourceId, Range timeRange) {
        return new ArrayList<LogEvent>();
    }

    /**
     * @see uk.ac.city.soi.everest.database.event.EventEntityManagerInterface#getCaptorTimestamp(java.lang.String)
     */
    public long getCaptorTimestamp(String partnerId) {

        return 0;

    }
}
