@ECHO OFF

SET USER_NAME=root

ECHO ----------------------------------------------------------
ECHO MySQL script
ECHO ----------------------------------------------------------
ECHO Command:  CREATE USER, CREATE DATABASE, CREATE TABLE
ECHO Database: %DATABASE_NAME%
ECHO User:     %USER_NAME%
ECHO Impact:   The user, the database, and the tables structure
ECHO           are going to be create
ECHO ----------------------------------------------------------

pause 

@ECHO ON
mysql -u %USER_NAME% -p < .\setupUserAndDatabase.sql