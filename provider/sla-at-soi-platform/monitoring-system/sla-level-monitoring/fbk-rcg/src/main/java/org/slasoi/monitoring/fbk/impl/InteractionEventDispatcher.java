/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slasoi.common.eventschema.ArgumentType;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.EventMetaDataType;
import org.slasoi.common.eventschema.InteractionEventType;
import org.slasoi.common.eventschema.SimpleArgument;
import org.soa.monitor.runtime.core.MonitorEvent;
import org.soa.monitor.runtime.rtm.RuntimeMonitorEngine;

/**
 * Dispatch the interaction event.
 * 
 * @since 0.1
 */
public final class InteractionEventDispatcher {

    /** The instance. */
    private static InteractionEventDispatcher instance = null;

    /** Private constructor. */
    private InteractionEventDispatcher() {
        /* empty block */
    }

    /**
     * Get the instance of the singleton.
     * 
     * @return InteractionEventDispatcher the interaction event dispatcher instance
     */
    public static InteractionEventDispatcher getInstance() {
        if (instance == null) {
            instance = new InteractionEventDispatcher();
        }
        return instance;
    }

    /**
     * Get value.
     * 
     * @param sa
     *            the simple argument
     * @return String the value of argument
     */
    private String getValue(final SimpleArgument sa) {
        String value = sa.getValue().toString();
        return value;
    }

    /**
     * Trim the string by hands; the java trim is bugged yet.
     * 
     * @param inputString
     *            the input string
     * @return String the string without whitespace
     */
    private String withoutWhitespace(final String inputString) {
        return inputString.replaceAll("\\ ", "");
    }

    /**
     * Get event Id.
     * 
     * @param event
     *            the event instance
     * @return String the event id
     */
    private String getEventId(final EventInstance event) {
        return event.getEventID().getID() + "";
    }

    /**
     * Get event type.
     * 
     * @param eventInstance
     *            the event instance
     * @return String the event type
     */
    private String getEventType(final EventInstance eventInstance) {
        return eventInstance.getEventID().getEventTypeID();
    }

    /**
     * Send the event.
     * 
     * @param eventInstance
     *            the event instance
     */
    public synchronized void sendServiceOperationCallStartEvent(final EventInstance eventInstance) {
        //System.err.println("Processing event " + eventInstance);
        Boolean callStart = false;
        Boolean callEnd = false;
        Boolean accepted = false;
        InteractionEventType iet = eventInstance.getEventPayload().getInteractionEvent();
        String operationName = iet.getOperationName();
        String eventId = getEventId(eventInstance);
        String status = iet.getStatus();

        String processId =
                eventInstance.getEventContext().getSource().getSwServiceLayerSource().getReceiver().getProcessID();

        String eventType = getEventType(eventInstance);

        // prepare event to send
        MonitorEvent event = new MonitorEvent(eventType);
        event.getParameters().put("name", "ServiceOperationCallStopEvent");
        event.getParameters().put("event_id", new Long(eventId));
        event.getParameters().put("process_id", new Long(processId));
        event.getParameters().put("operation_name", operationName);

        //System.err.print("Forward " + event.getName());

        // check if event contains parameter.
        if (iet.getParameters() != null) {
            for (ArgumentType at : iet.getParameters().getArgument()) {
                    if (at.getSimple() != null) {
                    SimpleArgument sa = at.getSimple();
                    if ((sa.getArgName() == null) || (sa.getArgName().equals(""))) {
                        printWarning(eventId);
                        continue;
                    }
                    Object value = null;
                    String withoutWhiteSpaceArgument = withoutWhitespace(sa.getArgName());
                    String parType = sa.getArgType();
                    if ((parType != null) && (parType.equalsIgnoreCase("http://www.w3.org/2001/XMLSchema#float"))) {
                        value = Float.parseFloat(getValue(sa));
                    } else if ((parType != null)
                            && (parType.equalsIgnoreCase("http://www.w3.org/2001/XMLSchema#integer"))) {
                        value = Float.parseFloat(getValue(sa));
                    } else if (parType != null
                            && parType.equalsIgnoreCase("http://www.w3.org/2001/XMLSchema#boolean")) {
                            value = Boolean.parseBoolean(getValue(sa).toLowerCase());
                    } else { //simple string.
                        value = getValue(sa);
                    }
                    event.getParameters().put(withoutWhiteSpaceArgument, value);
                    //System.err.print(withoutWhiteSpaceArgument + "=" + value);
                    // identification of Call End event.
                    if (withoutWhiteSpaceArgument.equalsIgnoreCase("returnValue")
                            && value.toString().equalsIgnoreCase("accepted")) {
                        accepted = true;
                    }
                    //System.err.print(", ");
                }
            }
        }
        //parse Meta data
        for (EventMetaDataType mData : eventInstance.getEventMetadata()) {
            if (mData != null) {
                String argument = withoutWhitespace(mData.getKey());
                //correlators (callId, reservationId) args (trip_options_number)
                if (mData.getKey().equalsIgnoreCase("callId")
                    || mData.getKey().equalsIgnoreCase("reservationId")
                    || mData.getKey().equalsIgnoreCase("trip_options_number")) {
                    Object value = new Long(mData.getValue());
                    event.getParameters().put(argument, value);
                    //System.err.print(argument + "=" + value);
                } else if (mData.getKey().equalsIgnoreCase("request_time")) {
                    try {
                        //Sat Jan 01 08:10:56 CET 2011
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy");
                        Date date = sdf.parse(mData.getValue());
                        Object value = date.getTime();
                        event.getParameters().put(argument, value);
                        //System.err.print(argument + "=" + value);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (mData.getKey().equalsIgnoreCase("test")) { //test
                    Object value = mData.getValue();
                    event.getParameters().put(argument, new Boolean(value.toString()));
                    //System.err.print(argument + "=" + value);
                    // check if its callStart event
                    if (status.equalsIgnoreCase("REQ-A")
                            && mData.getValue().equalsIgnoreCase("false")) {
                        callStart = true;
                    }
                    // check if its CallEnd event
                    if (status.equalsIgnoreCase("RES-A")
                            && mData.getValue().equalsIgnoreCase("false")
                            && accepted) {
                        callEnd = true;
                    }
                }
            }
        }
        //System.err.println(")");
        long timestamp = 0L;
        if (eventInstance.getEventContext().getTime() != null) {
            timestamp = eventInstance.getEventContext().getTime().getTimestamp();
        }
        event.setTimestamp(timestamp);
        RuntimeMonitorEngine.getInstance().dispatchMessage(event);
        // send call start event.
        if (callStart) {
            send(eventInstance, "phoneCall");
        }
        // send call end event.
        if (callEnd) {
            send(eventInstance, "phoneCallEnd");
        }
    }

    /**
     * Print warning.
     * 
     * @param id
     *            the event id
     */
    private void printWarning(final String id) {
        System.err.print("The event " + id + " is not handled");

    }

    /**
     * send one way events to monitor runtime based on operation.
     * 
     * @param eventInstance
     *            EventInstance
     * @param operationName
     *            String
     */
    public void send(final EventInstance eventInstance, final String operationName) {

        InteractionEventType iet = eventInstance.getEventPayload().getInteractionEvent();
        // prepare event to send
        MonitorEvent event = new MonitorEvent(operationName);
        event.getParameters().put("name", operationName);
        // check for parameters.
        if (iet.getParameters() != null) {
               for (ArgumentType at : iet.getParameters().getArgument()) {
                   if (at.getSimple() != null) {
                    SimpleArgument sa = at.getSimple();
                    if ((sa.getArgName() == null) || (sa.getArgName().equals(""))) {
                        printWarning(operationName);
                        continue;
                    }

                    Object value = null;
                    String withoutWhiteSpaceArgument = withoutWhitespace(sa.getArgName());
                    String parType = sa.getArgType();
                    if ((parType != null) && (parType.equalsIgnoreCase("http://www.w3.org/2001/XMLSchema#float"))) {
                        value = Float.parseFloat(getValue(sa));
                    } else if ((parType != null)
                            && (parType.equalsIgnoreCase("http://www.w3.org/2001/XMLSchema#boolean"))) {
                        value = Boolean.parseBoolean(getValue(sa).toLowerCase());
                    } else if ((parType != null)
                            && (parType.equalsIgnoreCase("http://www.w3.org/2001/XMLSchema#long"))) {
                        value = Long.parseLong(getValue(sa));
                    } else if ((parType != null)
                            && (parType.equalsIgnoreCase("http://www.w3.org/2001/XMLSchema#integer"))) {
                        value = Float.parseFloat(getValue(sa));
                    } else if ((parType != null)
                            && (parType.equalsIgnoreCase("http://www.w3.org/2001/XMLSchema#dateTime"))) {
                        // can throw a ParseException if you get a string in the wrong format.
                        try {
                            //System.out.println("");
                            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy");
                            Date date = sdf.parse(getValue(sa));
                            value = date.getTime();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    } else { //simple string.
                        value = getValue(sa);
                    }

                    event.getParameters().put(withoutWhiteSpaceArgument, value);
                    //System.err.print(withoutWhiteSpaceArgument + "=" + value);
                    //System.err.print(", ");
                }
            }
        }

        //parse Meta data
        for (EventMetaDataType mData : eventInstance.getEventMetadata()) {
            if (mData != null) {
                String argument = withoutWhitespace(mData.getKey());
                //correlators (callId, reservationId) args (trip_options_number)
                if (mData.getKey().equalsIgnoreCase("callId")
                    || mData.getKey().equalsIgnoreCase("reservationId")
                    || mData.getKey().equalsIgnoreCase("processId")
                    || mData.getKey().equalsIgnoreCase("trip_options_number")) {
                    Object value = new Long(mData.getValue());
                    event.getParameters().put(argument, value);
                    //System.err.print(argument + "=" + value);
                } else if (mData.getKey().equalsIgnoreCase("request_time")) {
                    try {
                        //Sat Jan 01 08:10:56 CET 2011
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy");
                        Date date = sdf.parse(mData.getValue());
                        Object value = date.getTime();
                        event.getParameters().put(argument, value);
                        //System.err.print(argument + "=" + value);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (mData.getKey().equalsIgnoreCase("test")) { //test
                    Object value = mData.getValue();
                    event.getParameters().put(argument, new Boolean(value.toString()));
                    //System.err.print(argument + "=" + value);
                }
                //System.err.print(", ");
            }
        }
        //System.err.println(")");
        long timestamp = 0L;
        if (eventInstance.getEventContext().getTime() != null) {
            timestamp = eventInstance.getEventContext().getTime().getTimestamp();
        }
        event.setTimestamp(timestamp);
        RuntimeMonitorEngine.getInstance().dispatchMessage(event);
        System.err.println(event);

    }

}
