/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.slasoi.common.eventschema.AgreementTermType;
import org.slasoi.common.eventschema.AssessmentResultType;
import org.slasoi.common.eventschema.EventContextType;
import org.slasoi.common.eventschema.EventIdType;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.EventNotifier;
import org.slasoi.common.eventschema.EventPayloadType;
import org.slasoi.common.eventschema.EventSource;
import org.slasoi.common.eventschema.EventTime;
import org.slasoi.common.eventschema.GuatanteedStateType;
import org.slasoi.common.eventschema.MonitoringInfoType;
import org.slasoi.common.eventschema.MonitoringResultEventType;
import org.slasoi.common.eventschema.ObjectFactory;
import org.slasoi.common.eventschema.PropertiesType;
import org.slasoi.common.eventschema.SLAType;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLA;

/**
 * Monitoring result event generator.
 * @author nawazk
 *
 */
public class MonitoringResultEventGenerator {
    /** UUID of SLA. **/
    private String slaUuid = null;
    /** SLA. **/
    private SLA sla = null;
    /** FBK Rcg instance. **/
    private static FbkRCGServiceImpl fbkRcgInstance = null;
    /** Internal counter for the notified event. */
    private static int counter = 0;
    /** violation counter SLA1. **/
    private static int vCounter = 0;
    /** flag for time kpi. **/
    private Boolean timeFlag = false;
    /** LOGGER. **/
    private static final Logger LOGGER = Logger.getLogger(MonitoringResultEventGenerator.class);
    /** Logger. */
    private static PrintWriter log = null;

    /**
     * constructor.
     * @param slaIn SLA.
     */
    public MonitoringResultEventGenerator(final SLA slaIn) {
        sla = slaIn;
        fbkRcgInstance = new FbkRCGServiceImpl();
        slaUuid = sla.getUuid().getValue();
    }
    /**
     * reset violation counters.
     */
    public static final void resetViolationCounters() {
        vCounter = 0;
    }

    /**
     * generate events.
     */
    public final void generate() {
        try {

            if (log == null) {
                log = new PrintWriter(new java.io.FileWriter(new File("monitorResultEvent.txt")), true);
            }

            java.util.HashMap<String, Object> map =
                    (java.util.HashMap<String, Object>) ProviderKPIRepository.getProviderKPIMap(slaUuid);
            java.util.Set set = map.entrySet();
            Iterator iProvKey = set.iterator();

            while (iProvKey.hasNext()) {
                Map.Entry me = (Map.Entry) iProvKey.next();
                Object value = me.getValue();
                if (value != null) {
                    java.util.ArrayList<Object> list = (java.util.ArrayList<Object>) value;
                    // generate events for all states
                    for (Object o : list) {
                        if (o instanceof Guaranteed.State) {
                            // flag to identify if kpi is of datatype time
                            timeFlag = false;
                            Boolean stateValue = true;
                            Float qosValue = new Float(0);
                            Guaranteed.State state = (Guaranteed.State) o;
                            String gsName = state.getId().getValue();
                            TypeConstraintExpr tce = (TypeConstraintExpr) state.getState();
                            // name of KPI
                            Object tceValue = tce.getValue();
                            SimpleDomainExpr sde = (SimpleDomainExpr) tce.getDomain();
                            Object valueT = sde.getValue();
                            if (valueT instanceof CONST) {
                                CONST constant = (CONST) valueT;
                                //check if data type is time, set time flag to true.
                                if (constant.getDatatype() != null
                                        && (constant.getDatatype().getValue().equalsIgnoreCase(
                                    "http://www.slaatsoi.org/coremodel/units#s")
                                    || constant.getDatatype().getValue().equalsIgnoreCase(
                                            "http://www.slaatsoi.org/coremodel/units#hrs")
                                    || constant.getDatatype().getValue().equalsIgnoreCase(
                                            "http://www.slaatsoi.org/coremodel/units#min"))) {
                                    timeFlag = true;
                                }
                            }
                            Object gsState;
                            gsState = fbkRcgInstance.reportMonitoredTermValue(slaUuid, String.valueOf(gsName));
                            if (gsState != null) {
                                stateValue = Boolean.parseBoolean(String.valueOf(gsState));
                            }
                            Object valueKPI =
                                    fbkRcgInstance.reportMonitoredTermValue(slaUuid, String.valueOf(tceValue));
                            if (valueKPI != null) {
                                if (timeFlag) { //convert time to minutes.
                                    qosValue = Float.parseFloat(String.valueOf(valueKPI)) / (1000 * 60);
                                } else { // other KPIs
                                    qosValue = Float.parseFloat(String.valueOf(valueKPI));
                                }
                            }

                            ObjectFactory of = new ObjectFactory();
                            EventInstance eventInstance = of.createEventInstance();
                            String monitorName = gsName;
                            AgreementTerm att = foundAgreementTermTypeThatContainGuaranteeState(monitorName);
                            EventIdType eventId = of.createEventIdType();
                            eventId.setID(counter++);
                            eventInstance.setEventID(eventId);

                            EventPayloadType payload = of.createEventPayloadType();

                            MonitoringResultEventType evres = of.createMonitoringResultEventType();

                            EventContextType eventContext = of.createEventContextType();
                            EventTime eventTime = of.createEventTime();
                            eventTime.setTimestamp(System.currentTimeMillis());
                            eventContext.setTime(eventTime);
                            EventSource source = of.createEventSource();
                            eventContext.setSource(source);
                            EventNotifier notifier = of.createEventNotifier();
                            eventContext.setNotifier(notifier);
                            eventInstance.setEventContext(eventContext);
                            SLAType slaInfo = of.createSLAType();
                            AgreementTermType at = of.createAgreementTermType();
                            eventId.setEventTypeID(monitorName);

                            if (!stateValue) { // violation
                                System.err.println("Sending result event with violation info about monitor term: "
                                        + monitorName);
                                at.setAssessmentResult(AssessmentResultType.VIOLATION);
                                slaInfo.setAssessmentResult(AssessmentResultType.VIOLATION);
                                // violation counter increment.
                                vCounter++;
                                ViolationCounterRepository.updateCounter(slaUuid, vCounter);
                            } else {
                                at.setAssessmentResult(AssessmentResultType.SATISFACTION);
                                slaInfo.setAssessmentResult(AssessmentResultType.SATISFACTION);
                            }
                            if (att != null) {
                                at.setAgreementTermID(att.getId().getValue());
                            }

                            GuatanteedStateType ga = new GuatanteedStateType();
                            ga.setQoSName(tceValue.toString());
                            ga.setQoSValue(qosValue.toString());
                            // add guaranteed state to agreement term
                            at.getGuaranteedStateOrGuaranteedAction().add(ga);
                            // add agreement term to sla
                            slaInfo.setSlaUUID(slaUuid);
                            slaInfo.setSlaURI("http://www.slasoi.org/B6SLA.xml");
                            slaInfo.getAgreementTerm().add(at);

                            // PropertiesType
                            evres.setExtraProperties(new PropertiesType());
                            // MonitoringInfoType
                            evres.setMonitoringInfo(new MonitoringInfoType());
                            // SLA Info
                            evres.setSLAInfo(slaInfo);

                            payload.setMonitoringResultEvent(evres);
                            eventInstance.setEventPayload(payload);
                            MonitoringResutEventDeserializer ms = new MonitoringResutEventDeserializer(eventInstance);
                            String monitoringResultString = ms.deserialize();
                            LOGGER.info(monitoringResultString);
                            log.print(monitoringResultString);
                            log.print("\n");
                            /* The content inside the sw buffer must be put in the bus */
                            MonitoringResultEventForwarder.publish(monitoringResultString);

                        }
                    }
                }
            }
            // rerun the loop for guranteed actions.
            java.util.Set setGA = map.entrySet();
            Iterator  iProvKeyGA = setGA.iterator();
            while (iProvKeyGA.hasNext()) {
                Map.Entry me = (Map.Entry) iProvKeyGA.next();
                Object value = me.getValue();
                if (value != null) {
                    java.util.ArrayList<Object> list = (java.util.ArrayList<Object>) value;
                    for (Object o : list) {
                        if (o instanceof ValueExpr) {
                            //flag to identify if kpi is of datatype time
                            timeFlag = false;
                            Float gaKPIValue = new Float(0);
                            ValueExpr kpi = (ValueExpr) o;
                            String gaName = kpi.toString();
                            //filter on time kpis.
                            if (gaName.equalsIgnoreCase("max_health_book_time")
                                    || gaName.equalsIgnoreCase("max_mobil_book_time")
                                    || gaName.equalsIgnoreCase("max_book_time")
                                    || gaName.equalsIgnoreCase("mean_health_book_time")
                                    || gaName.equalsIgnoreCase("mean_mobil_book_time")
                                    || gaName.equalsIgnoreCase("mean_book_time")) {
                                timeFlag = true;
                            }
                            Object gaValue = fbkRcgInstance.reportMonitoredTermValue(slaUuid, String.valueOf(gaName));
                            if (gaValue != null) {
                                if (timeFlag) {
                                    gaKPIValue = Float.parseFloat(String.valueOf(gaValue)) / (1000 * 60);
                                } else { //normal KPIs
                                    gaKPIValue = Float.parseFloat(String.valueOf(gaValue));
                                }
                            }
                            ObjectFactory of = new ObjectFactory();
                            EventInstance eventInstance = of.createEventInstance();
                            String monitorName = gaName;
                            AgreementTerm att = foundAgreementTermTypeThatContainGuaranteeState(monitorName);
                            EventIdType eventId = of.createEventIdType();
                            eventId.setID(counter++);
                            eventInstance.setEventID(eventId);

                            EventPayloadType payload = of.createEventPayloadType();

                            MonitoringResultEventType evres = of.createMonitoringResultEventType();

                            EventContextType eventContext = of.createEventContextType();
                            EventTime eventTime = of.createEventTime();
                            eventTime.setTimestamp(System.currentTimeMillis());
                            eventContext.setTime(eventTime);
                            EventSource source = of.createEventSource();
                            eventContext.setSource(source);
                            EventNotifier notifier = of.createEventNotifier();
                            eventContext.setNotifier(notifier);
                            eventInstance.setEventContext(eventContext);
                            SLAType slaInfo = of.createSLAType();
                            AgreementTermType at = of.createAgreementTermType();
                            eventId.setEventTypeID(monitorName);
                            at.setAssessmentResult(AssessmentResultType.NOT_ASSESSED);
                            slaInfo.setAssessmentResult(AssessmentResultType.NOT_ASSESSED);
                            if (att != null) {
                                at.setAgreementTermID(att.getId().getValue());
                            }
                            GuatanteedStateType ga = new GuatanteedStateType();
                            ga.setQoSName(gaName);
                            ga.setQoSValue(gaKPIValue.toString());
                            // add guaranteed state to agreement term
                            at.getGuaranteedStateOrGuaranteedAction().add(ga);
                            // add agreement term to sla
                            slaInfo.setSlaUUID(slaUuid);
                            slaInfo.setSlaURI("http://www.slasoi.org/B6SLA.xml");
                            slaInfo.getAgreementTerm().add(at);

                            // PropertiesType
                            evres.setExtraProperties(new PropertiesType());
                            // MonitoringInfoType
                            evres.setMonitoringInfo(new MonitoringInfoType());
                            // SLA Info
                            evres.setSLAInfo(slaInfo);

                            payload.setMonitoringResultEvent(evres);
                            eventInstance.setEventPayload(payload);
                            MonitoringResutEventDeserializer ms = new MonitoringResutEventDeserializer(eventInstance);
                            String monitoringResultString = ms.deserialize();
                            LOGGER.info(monitoringResultString);
                            log.print(monitoringResultString);
                            log.print("\n");
                            /* The content inside the sw buffer must be put in the bus */
                            MonitoringResultEventForwarder.publish(monitoringResultString);
                        }
                    }
                }
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Found in the sla template the agreement term
     * that contains the guarantee state called by name.
     *
     * @param name the name of the guarantee state violated
     * @return AgreementTerm the agreement term violated
     */
    private AgreementTerm foundAgreementTermTypeThatContainGuaranteeState(final String name) {
        int lenght = sla.getAgreementTerms().length;
        for (int i = 0; i < lenght; i++) {
            AgreementTerm att = sla.getAgreementTerms()[i];
            int gaLenght = att.getGuarantees().length;
            for (int j = 0; j < gaLenght; j++) {
                Guaranteed gt = att.getGuarantees()[j];
                /*
                 * The monitor name is equals to
                 * the guaranteed for construction
                 */
                if (gt.getId().equals(name)) {
                    return att;
                }
            }

        }
        return null;
    }

}
