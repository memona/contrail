/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

/**
 * Emit the astro monitor class.
 *
 * @since 0.1
 * @author Michele Trainotti
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

/** Emit the monitor classes. */
public class MonitorClassEmitter {

    /** The target dir where put the generated classes. */
    private static String targetDir = ".";

    /** The print writer used for the emission. */
    private PrintWriter out = null;

    /**
     * Instantiate a new monitor class emitter passing
     * the location folder where deploy the generated code.
     *
     * @param targerDir the taget folder
     */
    public MonitorClassEmitter(final String targerDir) {
        targetDir = targerDir;
    }

    /**
     * The monitor formula is in the format expected by the fbk monitor runtime.
     *
     * @param id the monitor id
     * @param monitorName the monitor name
     * @param monitorFormula the astro monitor formula
     * @param correlationParameters the correlation parameters
     */
    public final void emitMonitorClass(final String id,
            final String monitorName, final String monitorFormula,
            final List<String> correlationParameters) {
        String targetFileName = targetDir + "/" + monitorName + ".java";
        boolean closeFile = false;
        try {
            if (out == null) {
                out = new PrintWriter(new File(targetFileName));
                closeFile = true;
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error in opening the target file "
                    + targetFileName);
            e.printStackTrace();
            System.exit(1);
        }

        // out = new PrintWriter(System.out);

        out.println("package monitor.instance;");

        out.println("import org.soa.monitor.runtime.core.*;");
        out.println("import org.soa.monitor.runtime.core.formula.*;");
        out.println("import java.text.ParseException;");
        out.println("public class " + monitorName
                + " extends AbstractMonitor {");
        out.println("public void init() throws ParseException{");
        out.println("addMonitorProperty(\"SLAT\", \"" + id + "\");");
        Iterator<String> ite = correlationParameters.iterator();
        while (ite.hasNext()) {
            out.println("addCorrelator(\"" + ite.next() + "\");");
        }

        out.print("setFormula(");
        out.print(monitorFormula);
        out.println(");");
        // _out.println("this.store()");
        out.println("}");
        out.println("public void updateValue(IMonitorEvent event)"
                + " {System.out.println(\"MonitorEvent \" " + "+ getName() "
                + "+ \"Value \"" + " + getValue());};");
        out.println("public void updateValue(IMonitor monitor)"
                + " {System.out.println(\"Monitor \" " + "+ getName() "
                + "+ \"Value \" " + "+ getValue());};");
        out.println("}");
        out.flush();
        if (closeFile) {
            out.close();
            out = null;
        }
    }

    /**
     * The monitor definition is in the format expected
     * by the fbk monitor runtime.
     *
     * @param monitorName  the monitor name
     * @param correlationParameters the correlation parameters
     * @param eventReference the event reference
     * @param monitorReference the monitor reference
     */
    public final void emitMonitorDefinition(final String monitorName,
            final List<String> correlationParameters,
            final List<String> eventReference,
            final List<String> monitorReference) {
        String targetFileName =
                targetDir + "/" + monitorName + "MonitorDefinition.java";
        boolean closeFile = false;
        try {
            if (out == null) {
                out = new PrintWriter(new File(targetFileName));
                closeFile = true;
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error in opening the target file "
                    + targetFileName);
            e.printStackTrace();
            System.exit(1);
        }

        // out = new PrintWriter(System.out);

        out.println("package monitor.instance;");

        out.println("import org.soa.monitor.runtime.core.*;");
        out.println("import java.text.ParseException;");
        out.println("public class " + monitorName
                + "MonitorDefinition extends MonitorDefinition {");

        out.println("public " + monitorName
                + "MonitorDefinition() throws ParseException {init();}");
        out.println("public void init() throws ParseException{");
        out.println("setName(\"" + monitorName + "\");");
        out.println("setClassname(\"monitor.instance." + monitorName + "\");");

        Iterator<String> evIte = eventReference.iterator();
        while (evIte.hasNext()) {
            out.println("addReferencedEvent(" + evIte.next() + ");");
        }

        Iterator<String> monIte = monitorReference.iterator();
        while (monIte.hasNext()) {
            out.println("addReferencedMonitor(" + monIte.next() + ");");
        }

        Iterator<String> ite = correlationParameters.iterator();
        while (ite.hasNext()) {
            out.println("addCorrelationParam(\"" + ite.next() + "\");");
        }

        out.println("}");
        out.println("}");
        out.flush();
        if (closeFile) {
            out.close();
            out = null;
        }
    }

    /**
     * The event definition is in the format
     * expected by the fbk monitor runtime.
     *
     * @param eventName the event name
     * @param correlationParameters  the correlation parameters
     * @param monitorName the monitor name
     */
    public final void emitEventDefinition(final String eventName,
            final List<String> correlationParameters,
            final String monitorName) {
        String targetFileName =
                targetDir + "/" + eventName + "EventDefinition.java";
        boolean closeFile = false;
        try {
            if (out == null) {
                out = new PrintWriter(new File(targetFileName));
                closeFile = true;
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error in opening the target file "
                    + targetFileName);
            e.printStackTrace();
            System.exit(1);
        }

        out.println("package monitor.instance;");

        out.println("import org.soa.monitor.runtime.core.*;");
        out.println("import java.text.ParseException;");
        out.println("public class " + eventName
                + "EventDefinition extends EventDefinition {");
        out.println("public " + eventName
                + "EventDefinition() throws ParseException {init();}");
        out.println("public void init() throws ParseException {");
        out.println("setName(\"" + eventName + "\");");
        out.println("setClassname("
                + "\"org.soa.monitor.runtime.core.MonitorEvent\");");

        Iterator<String> ite = correlationParameters.iterator();
        while (ite.hasNext()) {
            out.println("addEventParam(\"" + ite.next() + "\");");
        }

        out.println("}");
        out.println("}");
        out.flush();
        if (closeFile) {
            out.close();
            out = null;
        }
    }

}
