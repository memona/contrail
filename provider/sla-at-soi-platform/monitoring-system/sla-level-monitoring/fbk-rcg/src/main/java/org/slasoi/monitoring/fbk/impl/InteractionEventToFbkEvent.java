/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.InteractionEventType;

/**
 * Translate the Interaction event in the format of the astro monitor.
 *
 * @since 0.1
 */
public class InteractionEventToFbkEvent {

    /** The interaction event translator in the astro format. */
    private static InteractionEventToFbkEvent instance = null;

    /**
     * Get the instance.
     *
     * @return InteractionEventToFbkEvent the InteractionEventToFbkEvent
     * bridge instance
     */
    public static InteractionEventToFbkEvent getInstance() {
        if (instance == null) {
            instance = new InteractionEventToFbkEvent();
        }
        return instance;
    }

    /**
     * Get the operation name of the event instance.
     *
     * @param eventInstance the event instance
     * @return String the operation name
     */
    private String getOperationName(final EventInstance eventInstance) {
        InteractionEventType iet =
                eventInstance.getEventPayload().getInteractionEvent();
        String operationName = iet.getOperationName();
        return operationName;
    }

    /**
     * This starting from EventInstance (that represent the interaction event)
     * and produce the event for the fbk runtime monitor.
     *
     * @param eventInstance the event instance
     */
    public final void forwardToFbkRuntimeMonitor(
            final EventInstance eventInstance) {
        String operationName = getOperationName(eventInstance);

        /*if ((operationName.equalsIgnoreCase("phoneCall"))
                || (operationName.equalsIgnoreCase("getSatisfactionLevels"))) {
            InteractionEventDispatcher.getInstance().sendServiceOperationCallStartEvent(eventInstance);
        }

        // report generation event.
        if (operationName.equalsIgnoreCase("endOfMonth")) {
            FbkRCGServiceImpl fbkrcg = new FbkRCGServiceImpl();
            fbkrcg.generateReport();
        }

        // bookMobility bookTreatment
        if ((operationName.equalsIgnoreCase("bookMobility"))
                || (operationName.equalsIgnoreCase("bookTreatment"))
                || (operationName.equalsIgnoreCase("paymentValidation"))) {
            String status = getStatus(eventInstance);
            if (status.equalsIgnoreCase("REQ-A")) {
            InteractionEventDispatcher.getInstance().send(eventInstance, operationName + "Start");
            } else if (status.equalsIgnoreCase("RES-A")) {
            InteractionEventDispatcher.getInstance().send(eventInstance, operationName + "Stop");
            }
        }

        // operatorAnswer treatmentCheckin treatment pickup arrival bookTripOption
        if ((operationName.equalsIgnoreCase("operatorAnswer"))
                || (operationName.equalsIgnoreCase("treatmentCheckin"))
                || (operationName.equalsIgnoreCase("treatment"))
                || (operationName.equalsIgnoreCase("pickup"))
                || (operationName.equalsIgnoreCase("arrival"))
                || (operationName.equalsIgnoreCase("getTripOptions"))
                || (operationName.equalsIgnoreCase("bookTripOption"))) {
            InteractionEventDispatcher.getInstance().send(eventInstance, operationName);
        }*/
        if ((operationName.equalsIgnoreCase("phoneCall"))
                || (operationName.equalsIgnoreCase("getSatisfactionLevels"))) {
            InteractionEventDispatcher.getInstance().sendServiceOperationCallStartEvent(eventInstance);
        } else if (operationName.equalsIgnoreCase("endOfMonth")) { // report generation event.
            FbkRCGServiceImpl fbkrcg = new FbkRCGServiceImpl();
            fbkrcg.generateReport(eventInstance.getEventContext().getTime().getTimestamp());
        } else if ((operationName.equalsIgnoreCase("bookMobility")) // bookMobility bookTreatment
                || (operationName.equalsIgnoreCase("bookTreatment"))
                || (operationName.equalsIgnoreCase("paymentValidation"))) {
            String status = getStatus(eventInstance);
            if (status.equalsIgnoreCase("REQ-A")) {
                InteractionEventDispatcher.getInstance().send(eventInstance, operationName + "Start");
            } else if (status.equalsIgnoreCase("RES-A")) {
                InteractionEventDispatcher.getInstance().send(eventInstance, operationName + "Stop");
            }
        } else if ((operationName.equalsIgnoreCase("operatorAnswer"))
                || (operationName.equalsIgnoreCase("treatmentCheckin"))
                || (operationName.equalsIgnoreCase("treatment"))
                || (operationName.equalsIgnoreCase("pickup"))
                || (operationName.equalsIgnoreCase("arrival"))
                || (operationName.equalsIgnoreCase("getTripOptions"))
                || (operationName.equalsIgnoreCase("bookTripOption"))) {
            InteractionEventDispatcher.getInstance().send(eventInstance, operationName);
        }

    }

    /**
     * getStatus.
     * @param eventInstance EventInstance
     * @return status String
     */
    private String getStatus(final EventInstance eventInstance) {
        InteractionEventType iet = eventInstance.getEventPayload().getInteractionEvent();
        String status = iet.getStatus();
        return status;
    }

}
