/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

/**
 * This represent an atomic task.
 *
 * @since 0.1
 */
public class FbkRCGMonitoredTask {

    /** Id for the task. */
    private String taskId;

    /** start time of the task. */
    private Long startTimestamp;

    /** end time of the task. */
    private Long endTimestamp;

    /**
     * Instantiate a new task passing the id and the creation timestamp.
     *
     * @param taskIdIn the task id
     * @param timestamp the timestamp
     */
    public FbkRCGMonitoredTask(final String taskIdIn, final long timestamp) {
        this.taskId = taskIdIn;
        this.startTimestamp = timestamp;
    }

    /**
     * Get the task id.
     *
     * @return String the task id
     */
    public final String getTaskId() {
        return this.taskId;
    }

    /**
     * Set the task id.
     *
     * @param taskIdIn the task id
     */
    public final void setTaskId(final String taskIdIn) {
        this.taskId = taskIdIn;
    }

    /**
     * Get the start timestamp.
     *
     * @return Long the timestamp when the task is started
     */
    public final Long getStartTimestamp() {
        return this.startTimestamp;
    }

    /**
     * Set the start timestamp.
     *
     * @param startTimestampIn the start timestamp
     */
    public final void setStartTimestamp(final Long startTimestampIn) {
        this.startTimestamp = startTimestampIn;
    }

    /**
     * Get the end timestamp.
     *
     * @return Long the the end timestamp
     */
    public final Long getEndTimestamp() {
        return this.endTimestamp;
    }

    /**
     * Set the end timestamp.
     *
     * @param endTimestampIn the end timestamp
     */
    public final void setEndTimestamp(final Long endTimestampIn) {
        this.endTimestamp = endTimestampIn;
    }

}
