/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import org.slasoi.slamodel.core.CompoundDomainExpr;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ValueExpr;

/**
 * Utility to analyze the count event message.
 *
 * @since 0.1
 */
public class CountUtility {

    /** Expression value. */
    private ValueExpr valueExpr = null;

    /** Aspect. */
    private String aspect = null;

    /** Periodic: the timeslice in which do you want count. */
    private String periodic = null;

    /** Query to select the event to count. */
    private String query = null;

    /** The used operator. */
    private String queryOperator = null;

    /** use for identifying query type simple/compound. */
    private Boolean compoundQuery = false;

    /** simpleDomainExpr array for compound query. **/
    private SimpleDomainExpr[] compoundQueryExpr;

    /**
     * Get query operator.
     *
     * @return String
     */
    public final String getQueryOperator() {
        return this.queryOperator;
    }

    /**
     * Set query operator.
     *
     * @param queryOperatorIn the query operator
     */
    public final void setQueryOperator(final String queryOperatorIn) {
        this.queryOperator = queryOperatorIn;
    }

    /**
     * Constructor.
     *
     * @param opIn the expression
     */
    public CountUtility(final ValueExpr opIn) {
        this.valueExpr = opIn;
    }

    /**
     * Get the aspect.
     *
     * @return String the aspect
     */
    public final String getAspect() {
        return this.aspect;
    }

    /**
     * Set the aspect.
     *
     * @param aspectIn the aspect
     */
    public final void setAspect(final String aspectIn) {
        this.aspect = aspectIn;
    }

    /**
     * Get periodic.
     *
     * @return String the periodic
     */
    public final String getPeriodic() {
        return this.periodic;
    }

    /**
     * Set periodic.
     *
     * @param periodicIn the periodic
     */
    public final void setPeriodic(final String periodicIn) {
        this.periodic = periodicIn;
    }

    /**
     * Get the query.
     *
     * @return String the query
     */
    public final String getQuery() {
        return this.query;
    }

    /**
     * Set the query.
     *
     * @param queryIn the query
     */
    public final void setQuery(final String queryIn) {
        this.query = queryIn;
    }

    /**
     * Is the expression a count.
     *
     * @return boolean return if the formula is a count formula
     */
    public final boolean isCount() {
        if (!(this.valueExpr instanceof FunctionalExpr)) {
            return false;
        }
        FunctionalExpr fe = (FunctionalExpr) this.valueExpr;
        if (!(fe.getOperator().toString().equals("count"))) {
            return false;
        }
        if (fe.getParameters().length < 1) {
            return false;
        }
        ValueExpr par0l = fe.getParameters()[0];
        FunctionalExpr fe0l = (FunctionalExpr) par0l;

        if (fe0l.getParameters().length < 1) {
            return false;
        }

        if (!(fe0l.getOperator().toString().equals("series"))) {
            return false;
        }

        // count(Formula, condition)
        if (fe.getParameters().length > 1) {
            ValueExpr par0r = fe.getParameters()[1];
            // handle domain expr simple/compound
            if (par0r instanceof SimpleDomainExpr) {
                SimpleDomainExpr domExpr = (SimpleDomainExpr) par0r;
                String domOp = domExpr.getComparisonOp().toString();
                if (domExpr.getValue() instanceof CONST) {
                    CONST domExprToConst = (CONST) domExpr.getValue();
                    // if String datatype put ""
                    if (domExprToConst.getDatatype() != null
                            && domExprToConst.getDatatype().getValue().equalsIgnoreCase(
                                    "http://www.w3.org/2001/XMLSchema#string")) {
                        this.setQuery("\"" + domExprToConst.getValue() + "\"");
                    } else if (domExprToConst.getDatatype() != null // if unit is seconds.
                            && domExprToConst.getDatatype().getValue().equalsIgnoreCase(
                                    "http://www.slaatsoi.org/coremodel/units#s")) {
                        Float value = new Float(domExprToConst.getValue());
                        value = value * 1000;
                        this.setQuery(String.valueOf(value));
                    } else if (domExprToConst.getDatatype() != null // if unit is hours.
                            && domExprToConst.getDatatype().getValue().equalsIgnoreCase(
                                    "http://www.slaatsoi.org/coremodel/units#hrs")) {
                        Float value = new Float(domExprToConst.getValue());
                        value = value * 60 * 60 * 1000;
                        this.setQuery(String.valueOf(value));
                    } else if (domExprToConst.getDatatype() != null // if unit is hours.
                            && domExprToConst.getDatatype().getValue().equalsIgnoreCase(
                                    "http://www.slaatsoi.org/coremodel/units#min")) {
                        Float value = new Float(domExprToConst.getValue());
                        value = value * 60 * 1000;
                        this.setQuery(String.valueOf(value));
                    }
                } else {
                    String domValue = domExpr.getValue().toString();
                    this.setQuery(domValue);
                }
                this.setQueryOperator(domOp);
            } else if (par0r instanceof CompoundDomainExpr) {
                CompoundDomainExpr cde = (CompoundDomainExpr) par0r;
                this.setCompoundQuery(true);
                this.setQueryOperator(cde.getLogicalOp().toString());
                compoundQueryExpr = new SimpleDomainExpr[cde.getSubExpressions().length];
                for (int i = 0; i < cde.getSubExpressions().length; i++) {
                    compoundQueryExpr[i] = (SimpleDomainExpr) cde.getSubExpressions()[i];
                }
            }
        }
        return true;
    }

    /** Parse the expression. */
    public final void parse() {
        FunctionalExpr fe = (FunctionalExpr) this.valueExpr;
        ValueExpr par0l = fe.getParameters()[0];
        FunctionalExpr fe0l = (FunctionalExpr) par0l;
        ValueExpr par1l = fe0l.getParameters()[0];

        EventExpr ev1r = (EventExpr) fe0l.getParameters()[1];

        Expr e2 = ev1r.getParameters()[0];
        CONST cons = (CONST) e2;
        String datatype = cons.getDatatype().toString();
        String consValue = cons.getValue();
        this.setAspect(par1l.toString());
        this.setPeriodic(consValue + "" + datatype);
    }

    /**
     * method for setting compoundQuery boolean property.
     * @param compoundQuery Boolean
     */
    public final void setCompoundQuery(final Boolean compoundQuery) {
        this.compoundQuery = compoundQuery;
    }

    /**
     * method for checking if Query is compound.
     * @return true/false Boolean
     */
    public final Boolean getCompoundQuery() {
        return compoundQuery;
    }

    /**
     * getter for compound query array.
     * @return SimpleDomainExpr[] array of simple queries.
     */
    public final SimpleDomainExpr[] getCompoundQueries() {
        return compoundQueryExpr;
    }

}
