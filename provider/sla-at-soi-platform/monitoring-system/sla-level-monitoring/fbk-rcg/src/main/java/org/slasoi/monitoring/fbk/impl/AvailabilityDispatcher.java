/**
 * SVN FILE: $Id$
 * 
 * Copyright (c) 2010, FBK All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met: * Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer. * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or other materials provided with the
 * distribution. * Neither the name of FBK nor the names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * @author $Author$
 * @version $Rev$
 * @lastrevision $Date$
 * @filesource $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import org.slasoi.common.eventschema.EventInstance;
import org.soa.monitor.runtime.core.MonitorEvent;
import org.soa.monitor.runtime.rtm.RuntimeMonitorEngine;

/**
 * This handle the availability;
 * generate an available event if the task is closed or a not availability
 * if the time is greater than the violation time.
 *
 * @since 0.1
 */
public final class AvailabilityDispatcher extends TimerTask {

    /** Timer user to schedule. */
    private Timer timer = null;

    /** Dispatcher to send the availability message to the astro monitor. */
    private static AvailabilityDispatcher instance;

    /** Hash table contain the task info. */
    private static Hashtable<String, FbkRCGMonitoredTask> openTaskHash;

    /**
     * if the time from request is higher than this value in milliseconds
     *  without a response the availability fail.
     */
    private static Long violationTime;

    /** The availabilty Dispatcher inner class. */
    private AvailabilityDispatcher() {

        openTaskHash = new Hashtable<String, FbkRCGMonitoredTask>();

        /**
         * This is the default you can change this
         * with the setViolationTime even at runtime.
         */
        String violationtimeS = "5000";
        violationTime = new Long(violationtimeS);
        this.timer = new Timer();
    }

    /**
     * Get the instance of the availability dispatcher.
     *
     * @return AvailabilityDispatcher
     */
    public static AvailabilityDispatcher getInstance() {
        if (openTaskHash == null) {
            instance = new AvailabilityDispatcher();
        }
        return instance;
    }

    /**
     * Get the violation time.
     *
     * @return Long
     */
    public static Long getViolationTime() {
        return violationTime;
    }

    /**
     * Set the violation time.
     *
     * @param violationTimeIn the violation time
     */
    public static void setViolationTime(final Long violationTimeIn) {
        AvailabilityDispatcher.violationTime = violationTimeIn;
    }

    /**
     * Get time stamp.
     *
     * @param eventInstanceIn the event instance
     * @return long the timestamp of the event
     */
    private long getTimestamp(final EventInstance eventInstanceIn) {
        return eventInstanceIn.getEventContext().getTime().getTimestamp();
    }

    /**
     * Extract the process id of the sender of the availability message.
     *
     * @param eventInstanceIn the event instance
     * @return String the process id of the event sender
     */
    private String getSenderProcessId(final EventInstance eventInstanceIn) {
        return eventInstanceIn.getEventContext().getSource()
                .getSwServiceLayerSource().getSender().getProcessID();
    }

    /**
     * Extract the process id of the receiver of the availability message.
     *
     * @param eventInstanceIn the event instance
     * @return String the process id of the eventreceiver
     */
    private String getReceiverProcessId(final EventInstance eventInstanceIn) {
        return eventInstanceIn.getEventContext().getSource()
                .getSwServiceLayerSource().getReceiver().getProcessID();
    }

    @Override
    public void run() {
        /*
         * if the time greater that violationTime then
         * send to the core monitor the not availability message
         */
        Long currentTime = new Date().getTime();

        /* See if exists some open task that violate the time */

        synchronized (this) {
            Collection<FbkRCGMonitoredTask> openTaskCollection =
                    openTaskHash.values();
            Iterator<FbkRCGMonitoredTask> ite = openTaskCollection.iterator();
            while (ite.hasNext()) {
                FbkRCGMonitoredTask taskInfo = ite.next();
                Long startTaskTime = taskInfo.getStartTimestamp();
                /*
                 * delta_t from the open task timestamp
                 * to the current timestamp
                 */
                Long deltaT = currentTime - startTaskTime;
                if (deltaT >= violationTime) {
                    System.err.println(taskInfo.getTaskId()
                            + " Generate a NotAvail message:"
                            + " The  CurrentTime is " + currentTime
                            + "ms and the start task at " + startTaskTime
                            + "ms the task is open " + deltaT
                            + "ms that is greater than"
                            + "the violation time "
                            + violationTime + "ms");
                    /* send to the core monitor the not availability message */
                    MonitorEvent event = new MonitorEvent("NotAvailable");
                    String service = "VM_Access_Point";
                    event.addParameter("service", service);
                    event.setTimestamp(currentTime);
                    RuntimeMonitorEngine.getInstance().dispatchMessage(event);
                }
            }
        }

    }

    /**
     * The task is opened.
     *
     * @param eventInstanceIn the event instance
     */
    public void openTask(final EventInstance eventInstanceIn) {
        long timestamp = getTimestamp(eventInstanceIn);
        String taskId = getReceiverProcessId(eventInstanceIn);
        openTask(taskId, timestamp);

    }

    /**
     * The task is closed.
     *
     * @param eventInstanceIn the event instance
     */
    public void closeTask(final EventInstance eventInstanceIn) {
        long timestamp = getTimestamp(eventInstanceIn);
        String taskId = getSenderProcessId(eventInstanceIn);
        closeTask(taskId, timestamp);
    }

    /**
     * Open a task giving an id and timestamp at the task big-bang starting.
     *
     * @param taskIdIn the task
     * @param timestampIn the timestamp when the task has been opened
     */
    public void openTask(final String taskIdIn, final long timestampIn) {
        System.err.println("Arrived Start Message of process instance "
                + taskIdIn);
        FbkRCGMonitoredTask taskInfo =
                new FbkRCGMonitoredTask(taskIdIn, timestampIn);
        synchronized (this) {
            openTaskHash.put(taskIdIn, taskInfo);
        }
    }

    /**
     * Close a task giving an id and timestamp at the task apocalipse end.
     *
     * @param taskIdIn the task id
     * @param timestampIn the timestamp when the task has been close
     */
    public void closeTask(final String taskIdIn, final long timestampIn) {
        System.err.println("Arrived End Message of process instance "
                + taskIdIn);
        synchronized (this) {
            /* the agreement term is satisfied */
            openTaskHash.remove(taskIdIn);
        }
        /* send to the core monitor the availability message */
        MonitorEvent event = new MonitorEvent("Available");
        String service = "VM_Access_Point";
        event.addParameter("service", service);
        event.setTimestamp(timestampIn);
        RuntimeMonitorEngine.getInstance().dispatchMessage(event);
    }

    /**
     * The main method used to test the task scheduler.
     *
     * @param arguments args
     */
    public static void main(final String... arguments) {
        TimerTask availabilityTimer = new AvailabilityDispatcher();
        Timer timer = new Timer();
        /* each violationTime execute the run method */
        timer.schedule(availabilityTimer, violationTime);
    }

    /**
     * This start the availability manager; you must pass the violation time.
     *
     * @param millisec the clock of the scheduler in milliseconds
     */
    public void startSchedule(final long millisec) {
        AvailabilityDispatcher.violationTime = millisec;
        /* each violationTime execute the run method */
        timer.schedule(AvailabilityDispatcher.getInstance(), 0, millisec);
    }

    /**
     * Send the eventInstance message.
     *
     * @param eventInstance the event instance
     */
    public void sendMessage(final EventInstance eventInstance) {
        if (eventInstance.getEventID().getEventTypeID().equals(
                "ServiceOperationCallStartEvent")) {
            AvailabilityDispatcher.getInstance().openTask(eventInstance);
        } else if (eventInstance.getEventID().getEventTypeID().equals(
                "ServiceOperationCallEndEvent")) {

            AvailabilityDispatcher.getInstance().closeTask(eventInstance);
        }
    }

}
