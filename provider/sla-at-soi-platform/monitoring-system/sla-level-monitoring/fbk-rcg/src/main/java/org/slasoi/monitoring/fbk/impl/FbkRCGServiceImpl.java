/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * 
 */
package org.slasoi.monitoring.fbk.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.xmlbeans.XmlException;
import org.codehaus.janino.Scanner;
import org.codehaus.janino.SimpleCompiler;
import org.osgi.framework.BundleContext;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils.GenericSLAManagerUtilsException;
import org.slasoi.gslam.core.negotiation.ISyntaxConverter;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.monitoring.common.configuration.ConfigurationFactory;
import org.slasoi.monitoring.common.configuration.ReasonerConfiguration;
import org.slasoi.monitoring.common.configuration.impl.ConfigurationFactoryImpl;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.fbk.service.FbkRCGService;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.soa.monitor.runtime.core.AbstractMonitor;
import org.soa.monitor.runtime.core.EventDefinition;
import org.soa.monitor.runtime.core.IMonitor;
import org.soa.monitor.runtime.core.MonitorDefinition;
import org.soa.monitor.runtime.core.MonitorEvent;
import org.soa.monitor.runtime.notification.NotificationManager;
import org.soa.monitor.runtime.notification.NotificationRule;
import org.soa.monitor.runtime.rtm.RuntimeMonitorClassLoader;
import org.soa.monitor.runtime.rtm.RuntimeMonitorEngine;
import org.soa.monitor.runtime.rtm.RuntimeMonitorPropertiesManager;
import org.springframework.osgi.context.BundleContextAware;
import org.springframework.osgi.extensions.annotation.ServiceReference;

import eu.slaatsoi.slamodel.SLADocument;

/**
 * Implementation of the fbk reasoning component gateway.
 *
 * @author nawazk
 * @since 0.1
 */
public class FbkRCGServiceImpl implements FbkRCGService, BundleContextAware {

    /** Uiid of the monitor. */
    private String uiid = "FbkRCG";

    /** Osgi context to inject bundles. */
    private BundleContext osgiContext;

    /** The name of the channel where send the result event. */
    private String resultChannel = "FBKMonResultEventChannel2";

    /** The name of the channel where listen the event to be monitored. */
    private String interactionEventChannel = "InteractionEventChannel2";

    /** in memory storage for fbkrcg.**/
    private HashMap<String, Object> slaMonitorMap = new HashMap<String, Object>();

    /** in memory storage for provider-kip map.**/
    private HashMap<String, Object> providerKPIMap = null;

    /** The list of the reasoner configuration of the fbkrcg. */
    private ArrayList<ReasonerConfiguration> rcArray =
            new ArrayList<ReasonerConfiguration>();

    /** The deploy dir where put the generated astro monitor classes. */
    private String deployDir =
            System.getenv("SLASOI_HOME") + System.getProperty("file.separator")
                    + "fbkrcg" + System.getProperty("file.separator")
                    + "deploy_dir";

    /** SlamUtils; used for injection. */
    private GenericSLAManagerUtils gslaManagerUtils;

    /** SLAManagerContext; used for injection. */
    private SLAManagerContext sslamContext = null;

    /**
     * Load the bundle context if it is running with spring and osgi.
     *
     * @param osgiContextIn the osgi context inkected by spring
     */
    public final void setBundleContext(final BundleContext osgiContextIn) {
        setOsgiContext(osgiContextIn);
    }

    /**
     * Get osgi context.
     *
     * @return  BundleContext the bundle context
     */
    public final BundleContext getOsgiContext() {
        return osgiContext;
    }

    /**
     * Load the bundle context if it is running with spring and osgi.
     *
     * @param osgiContextIn the osgi context
     */
    public final void setOsgiContext(final BundleContext osgiContextIn) {
        this.osgiContext = osgiContextIn;
    }

    /**
     * Constructor.
     */
    public FbkRCGServiceImpl() {
        super();
    }

    /**
     * The ServiceReference flag need to inject the gslaManagerUtils
     * and the SslamContext in the spring way.
     *
     * @param gslaManagerUtilsIn the gslam utils useful to injection
     */
    @ServiceReference
    public final void setGenericSLAManagerUtils(
            final GenericSLAManagerUtils gslaManagerUtilsIn) {
        this.gslaManagerUtils = gslaManagerUtilsIn;
        injectSslamContext();
    }

    /** Inject the sslam contex. */
    private void injectSslamContext() {
        System.out.println("Attempt to inject SLAM Context");

        if (this.gslaManagerUtils != null) {
            try {
                SLAManagerContext[] context =
                        gslaManagerUtils.getContextSet("GLOBAL");

                for (SLAManagerContext c : context) {

                    /*
                     * This is a software layer service
                     * and the I inject only the software sla context
                     */
                    this.sslamContext = c;
                }

            } catch (GenericSLAManagerUtilsException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            System.err.println("gslaManagerUtil null");
        }
    }

    /**
     * Get the deploy dir.
     *
     * @return String the deploy dir for the monitor classes
     */
    public final String getDeployDir() {
        return this.deployDir;
    }

    /**
     * Set the deploy dir.
     *
     * @param deployDirIn the deploy dir for the monitor classes
     */
    public final void setDeployDir(final String deployDirIn) {
        this.deployDir = deployDirIn;
    }

    /** Unsuscribe to the interaction event channel on bus. */
    private void unsubscribeToInteractionEvents() {
        // TODO Auto-generated method stub
        InteractionEventSubscriber.unsubscribe();
    }

    /**
     * Translate the slat.
     *
     * @param slat the slat
     * @param agreementTerm the agreement term
     * @param guaranteed the guaranteed
     * @param map HashMap
     */
    private void translate(final SLATemplate slat,
            final AgreementTerm agreementTerm, final Guaranteed guaranteed, HashMap<String, Object>map) {
        SlatToMonitorClasses slattomc =
                new SlatToMonitorClasses(slat, agreementTerm, guaranteed,
                        this.deployDir);
        slattomc.translate(map);
    }

    /**
     * Add the slat to the internal repository.
     *
     * @param slat the slat
     */
    private void addToSlatRepo(final SLATemplate slat) {
        System.err.println("Add slat " + slat.getUuid().getValue()
                + " Hashcode " + String.valueOf(Math.abs(slat.getUuid().hashCode())));
        FbkRCGSlatRepository.addSlat(slat.getUuid().getValue(), slat);
    }

    /**
     * Start to monitor the slat.
     *
     * @param slat the slat
     */
    private void monitorSLAT(final SLATemplate slat) {
        addToSlatRepo(slat);
        //clear providerKPIMap
        providerKPIMap = new HashMap<String, Object>();
        int agreementTermsNumber = slat.getAgreementTerms().length;
        for (int i = 0; i < agreementTermsNumber; i++) {
            AgreementTerm agreementTerm = slat.getAgreementTerms()[i];
            int guaramteesNumber = agreementTerm.getGuarantees().length;
            for (int j = 0; j < guaramteesNumber; j++) {
                Guaranteed guaranteed = agreementTerm.getGuarantees()[j];
                translate(slat, agreementTerm, guaranteed, providerKPIMap);
            }
            if (agreementTerm.getPrecondition() != null) {
                translatePrecondition(slat, agreementTerm);
            }
        }
        addtoMapRepository(slat.getUuid().getValue(), providerKPIMap);
    }

    /**
     * add provider-kpi map corresponding to Sla.
     * @param slaId String
     * @param providerKPIMap2 HashMap
     */
    private void addtoMapRepository(final String slaId, final HashMap<String, Object> providerKPIMap2) {
        System.err.println("Add provider-kpi Map " + slaId);
        ProviderKPIRepository.addProviderKPIMap(slaId, providerKPIMap2);
    }

    /**
     * method for translation of precondition.
     * @param slat SLATemplate.
     * @param agreementTerm AgreementTerm
     */
    private void translatePrecondition(final SLATemplate slat, final AgreementTerm agreementTerm) {
        SlatToMonitorClasses slattomc =
            new SlatToMonitorClasses(slat, agreementTerm, this.deployDir);
        slattomc.translatePrecondition();
    }

    /**
     *  Add the configuration element to the reasoning gateway.
     *
     * @param reasonerConfiguration the reasoner configuration
     */
    public final void addConfiguration(
            final ReasonerConfiguration reasonerConfiguration) {
        this.rcArray.add(reasonerConfiguration);
        Object specification = reasonerConfiguration.getSpecification();
        if (specification instanceof SLATemplate) {
            SLATemplate slat = (SLATemplate) specification;
            monitorSLAT(slat);
        }
        loadClassesInRuntimeMonitor();

    }

    /**
     * This return the monitoring feature that the monitor is able to handle.
     *
     * @return ComponentMonitoringFeatures
     */
    public final ComponentMonitoringFeatures getComponentMonitoringFeatures() {
        return FbkRCGComponentMonitoringFeatures.getInstance();
    }

    /**
     * Get configuration.
     *
     * @return ArrayList of ReasonerConfiguration
     */
    public final ArrayList<ReasonerConfiguration> getConfigurations() {
        return this.rcArray;
    }

    /**
     * This return the uuid of the monitor.
     * @return String the monitor uuid
     */
    public final String getUuid() {
        return this.uiid;
    }

    /**
     * Remove a configuration.
     *
     * @param configurationId the configuration id
     */
    public final void removeConfiguration(final String configurationId) {
        ReasonerConfiguration toBeRemoved = null;
        Iterator<ReasonerConfiguration> iterator = this.rcArray.iterator();
        while (iterator.hasNext()) {
            ReasonerConfiguration rc = iterator.next();
            if (rc.getConfigurationId().equals(configurationId)) {
                toBeRemoved = rc;
            }
            /* if rc id is equal configurationId */
        }
        if (toBeRemoved != null) {
            this.rcArray.remove(toBeRemoved);
        }
    }

    /**
     * Set the Component monitoring features.
     *
     * @param  componentMonitoringFeatures the fbkrcg monitoring features
     */
    public void setComponentMonitoringFeatures(
            final ComponentMonitoringFeatures componentMonitoringFeatures) {
        /*
         * TODO This must fill the ComponentMonitoringFeatures
         * with the feature available on the reasoning monitor
         */
    }

    /**
     * Set uiid.
     *
     * @param value the uuid
     */
    public final void setUuid(final String value) {
        this.uiid = value;
    }

    /**
     * This class build in memory the classes using janino;
     * I use this because it is difficult or impossible use the sun compiler.
     *
     * @param source the string with the source code
     * @param file the file
     * @return Class the build class with janino
     */
    private Class<?> compileWithJanino(final String source, final File file) {
        try {
            String className =
                    "monitor.instance."
                            + file.getName().substring(0,
                                    file.getName().indexOf("."));

            Scanner scanner = new Scanner(className, new FileInputStream(file));

            ClassLoader parentClassLoader = this.getClass().getClassLoader();

            SimpleCompiler compiler =
                    new SimpleCompiler(scanner, parentClassLoader);

            System.err.println("Building " + file.getName());

            ClassLoader classLoader = compiler.getClassLoader();

            Class<?> clazz = classLoader.loadClass(className);

            return clazz;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /** Load the class in the astro runtime monitor. */
    private void loadClassesInRuntimeMonitor() {
        String[] ext = {"java"};
        for (Object fileObj : FileUtils.listFiles(new File(deployDir), ext,
                true).toArray()) {
            File file = (File) fileObj;

            try {
                Class<?> clazz = compileWithJanino(this.deployDir, file);
                Object obj = clazz.newInstance();
                if (obj instanceof AbstractMonitor) {
                    AbstractMonitor absMon = (AbstractMonitor) obj;
                    System.err.println("Add monitor " + absMon.getName());
                    RuntimeMonitorClassLoader.getInstance(
                            this.getClass().getClassLoader()).addClazz(
                            "monitor.instance." + absMon.getName(), clazz);
                } else if (obj instanceof MonitorDefinition) {
                    MonitorDefinition monDef = (MonitorDefinition) obj;
                    System.err.println("Add monitor definition "
                            + monDef.getName());
                    RuntimeMonitorEngine.getInstance().getInventory()
                            .addMonitorDefinition(monDef);

                } else if (obj instanceof EventDefinition) {
                    EventDefinition evDef = (EventDefinition) obj;
                    System.err.println("Add event definition "
                            + evDef.getName());
                    RuntimeMonitorEngine.getInstance().getInventory()
                            .addEventDefinition(evDef);
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    /**
     * Add to the astro monitor a notification rule
     * that will send the result event on the bus.
     *
     * @param nm the notification manager
     */
    private void addNotificationRule(final NotificationManager nm) {
        /*
         * Here is added the code that might
         * add the notification plugin to the monitor runtime
         */
        NotificationRule notificationRule = new NotificationRule();
        notificationRule.monitorName = "*";
        notificationRule.notificationAddress = "updatedFBKRCG.log";
        notificationRule.notificationChannel =
                "org.slasoi.monitoring.fbk.impl.FbkRCG";
        notificationRule.notificationSubject = "Monitor MONITOR_NAME updated";
        notificationRule.notificationMessage = "Monitor value = MONITOR_VALUE";
        nm.addNotificationRule(notificationRule);
    }

    /** Add the availability events to the reasoning component gateway. */
    private void addAvailabilityLowLevelEvents() {
        addAvailableLowLevelEvent();
        addNotAvailableLowLevelEvent();
    }

    /**
     * Add to the knowledge base the default low level events
     * that reasoning component gateway knows.
     */
    private void addDefaultLowLevelEvents() {
        addAvailabilityLowLevelEvents();
        addInteractionEvent();
        addORCEvent();
    }

    /** Add the interaction events to the reasoning component gateway. */
    private void addInteractionEvent() {
        EventDefinition interactionEvent = new EventDefinition();
        interactionEvent
                .setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        interactionEvent.setName("ServiceOperationCallEndEvent");
        interactionEvent.addEventParam("name");
        interactionEvent.addEventParam("event_id");
        interactionEvent.addEventParam("process_id");
        interactionEvent.addEventParam("operation_name");
        interactionEvent.addEventParam("OverallQuality");
        interactionEvent.addEventParam("CallCenterQuality");
        interactionEvent.addEventParam("MobilityQuality");
        interactionEvent.addEventParam("MedicalTreatmentQuality");
        interactionEvent.addEventParam("returnValue");
        interactionEvent.addEventParam("status");
        interactionEvent.addEventParam("test");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                interactionEvent);

        //Y3 B6 evetns
        //bookTreatmentStart
        EventDefinition bookTreatmentStart = new EventDefinition();
        bookTreatmentStart.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        bookTreatmentStart.setName("bookTreatmentStart");
        bookTreatmentStart.addEventParam("name");
        bookTreatmentStart.addEventParam("callId");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                bookTreatmentStart);
        //bookTreatmentStop
        EventDefinition bookTreatmentStop = new EventDefinition();
        bookTreatmentStop.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        bookTreatmentStop.setName("bookTreatmentStop");
        bookTreatmentStop.addEventParam("name");
        bookTreatmentStop.addEventParam("callId");
        bookTreatmentStop.addEventParam("reservationId");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                bookTreatmentStop);
        //bookMobilityStart
        EventDefinition bookMobilityStart = new EventDefinition();
        bookMobilityStart.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        bookMobilityStart.setName("bookMobilityStart");
        bookMobilityStart.addEventParam("name");
        bookMobilityStart.addEventParam("callId");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                bookMobilityStart);
        //bookMobilityStop
        EventDefinition bookMobilityStop = new EventDefinition();
        bookMobilityStop.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        bookMobilityStop.setName("bookMobilityStop");
        bookMobilityStop.addEventParam("name");
        bookMobilityStop.addEventParam("callId");
        bookMobilityStop.addEventParam("reservationId");
        bookMobilityStop.addEventParam("pickupTime");
        bookMobilityStop.addEventParam("arrivalTime");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                bookMobilityStop);
        //operatorAnswer
        EventDefinition operatorAnswer = new EventDefinition();
        operatorAnswer.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        operatorAnswer.setName("operatorAnswer");
        operatorAnswer.addEventParam("name");
        operatorAnswer.addEventParam("callId");
        operatorAnswer.addEventParam("status");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                operatorAnswer);
        //phoneCall
        EventDefinition phoneCall = new EventDefinition();
        phoneCall.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        phoneCall.setName("phoneCall");
        phoneCall.addEventParam("name");
        phoneCall.addEventParam("callId");
        phoneCall.addEventParam("returnValue");
        phoneCall.addEventParam("status");
        phoneCall.addEventParam("test");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                phoneCall);
        //phoneCallEnd
        EventDefinition phoneCallEnd = new EventDefinition();
        phoneCallEnd.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        phoneCallEnd.setName("phoneCallEnd");
        phoneCallEnd.addEventParam("name");
        phoneCallEnd.addEventParam("callId");
        phoneCallEnd.addEventParam("returnValue");
        phoneCallEnd.addEventParam("status");
        phoneCallEnd.addEventParam("test");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                phoneCallEnd);
        // arrival
        EventDefinition arrival = new EventDefinition();
        arrival.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        arrival.setName("arrival");
        arrival.addEventParam("name");
        arrival.addEventParam("reservationId");
        arrival.addEventParam("request_time");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(arrival);
        // pickup
        EventDefinition pickup = new EventDefinition();
        pickup.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        pickup.setName("pickup");
        pickup.addEventParam("name");
        pickup.addEventParam("reservationId");
        pickup.addEventParam("request_time");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(pickup);
        //treatment
        EventDefinition treatment = new EventDefinition();
        treatment.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        treatment.setName("treatment");
        treatment.addEventParam("name");
        treatment.addEventParam("callId");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                treatment);
        //treatmentCheckin
        EventDefinition treatmentCheckin = new EventDefinition();
        treatmentCheckin.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        treatmentCheckin.setName("treatmentCheckin");
        treatmentCheckin.addEventParam("name");
        treatmentCheckin.addEventParam("callId");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                treatmentCheckin);
        // bookTripOption.
        EventDefinition bookTripOption = new EventDefinition();
        bookTripOption.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        bookTripOption.setName("bookTripOption");
        bookTripOption.addEventParam("name");
        bookTripOption.addEventParam("callId");
        bookTripOption.addEventParam("reservationId");
        bookTripOption.addEventParam("arrivalTime");
        bookTripOption.addEventParam("pickupTime");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(bookTripOption);
        //endOfDay
        EventDefinition endOfMonth = new EventDefinition();
        endOfMonth.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        endOfMonth.setName("endOfMonth");
        endOfMonth.addEventParam("name");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                endOfMonth);
        //selectService
        EventDefinition selectService = new EventDefinition();
        selectService.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        selectService.setName("selectService");
        selectService.addEventParam("name");
        selectService.addEventParam("callId");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                selectService);
        //getTripOptions
        EventDefinition getTripOptions = new EventDefinition();
        getTripOptions.setClassname("org.soa.monitor.runtime.core.MonitorEvent");
        getTripOptions.setName("getTripOptions");
        getTripOptions.addEventParam("name");
        getTripOptions.addEventParam("trip_options_number");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(getTripOptions);

    }

    /** Add the ORC events to the rcg. **/
    private void addORCEvent() {
        EventDefinition paymentValidationStart = new EventDefinition();
        paymentValidationStart.setClassname("org.soa.monitor.runtime.core.EventDefinition");
        paymentValidationStart.setName("paymentValidationStart");
        paymentValidationStart.addEventParam("processId");
        //add definition to inventory.
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(paymentValidationStart);

        EventDefinition paymentValidationStop = new EventDefinition();
        paymentValidationStop.setClassname("org.soa.monitor.runtime.core.EventDefinition");
        paymentValidationStop.setName("paymentValidationStop");
        paymentValidationStop.addEventParam("processId");
        //add definition to inventory.
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(paymentValidationStop);

    }


    /** Add the not available event to the reasoning component gateway. */
    private void addNotAvailableLowLevelEvent() {
        EventDefinition evNotAvailable = new EventDefinition();
        evNotAvailable
                .setClassname("org.soa.monitor.runtime.core.EventDefinition");
        evNotAvailable.setName("NotAvailable");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                evNotAvailable);
    }

    /** Add the available event to the reasoning component gateway. */
    private void addAvailableLowLevelEvent() {
        EventDefinition evAvailable = new EventDefinition();
        evAvailable
                .setClassname("org.soa.monitor.runtime.core.EventDefinition");
        evAvailable.setName("Available");
        evAvailable.addEventParam("service");
        RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
                evAvailable);
    }

    /**
     * Take the RutimeProperties for the fbk runtime engine
     * and override some value.
     */
    public final void overrideDefaultRuntimeProperties() {
        RuntimeMonitorPropertiesManager rmp =
                RuntimeMonitorPropertiesManager.getInstance();

        rmp.setDatabaseName(System.getenv("SLASOI_HOME")
                + System.getProperty("file.separator") + "fbkrcg"
                + System.getProperty("file.separator") + "db_data"
                + System.getProperty("file.separator") + "monitor.db");
        rmp.setDeployDir(deployDir);
    }

    /** Initialize the fbk runtime monitor engine. */
    private void initializeRuntimeMonitorengine() {
        // delete previous db monitor
        new File(RuntimeMonitorPropertiesManager.getDatabaseName()).delete();

        /* override properties of monitor runtime by code */
        overrideDefaultRuntimeProperties();

        /* get the instance */
        RuntimeMonitorEngine rme = RuntimeMonitorEngine.getInstance();
        /* this clean the persistence db */
        // rme.getInventory().cleanInternalTables();
        /*
         * set the classloader this is done to be able the monitor
         * to see the class that will be build with janino this
         * outside osgi is not need
         */
        // rme.setClassLoader(this.getClass().getClassLoader());
        // add notification manager that send to bus the slat violations
        addNotificationRule(rme.getNotificationManager());
    }

    /**
     * Start the scheduler to handle the availability for the event
     * with id ServiceOperationCallStartEvent and ServiceOperationCallEndEvent.
     */
    private void startScheduler() {
        /* the violation time is fixed such as the sam event generation */
        long violationTime = 5000;
        AvailabilityDispatcher.getInstance().startSchedule(violationTime);
    }

    /**
     * Start the monitoring.
     *
     * @param  configurationId the configuration id
     * @return boolean return if the service is started correctly
     */
    public final boolean startMonitoring(final String configurationId) {
        System.out.println("Initialize the notification pluging"
                + " to send result events to channel " + resultChannel);
        MonitoringResultEventForwarder.instantiatePubSubManager(resultChannel);

        // start the scheduler
        //startScheduler();

        /*
         * The RCG subscribe with the bus to listen
         * the interaction event from sensors
         */
        InteractionEventSubscriber.subscribeToChannel(interactionEventChannel);
        System.out.println("CREATED CHANNEL WHERE FBKRCG"
                + " LISTEN THE INTERACTION EVENT " + interactionEventChannel);

        /* start the runtime monitor engine */
        initializeRuntimeMonitorengine();

        // add the low level handled events
        addDefaultLowLevelEvents();

        return true;
    }

    /**
     * Stop the monitoring system.
     *
     * @param  configurationId the configuration id
     * @return boolean return if the service is stopped correctly
     */
    public final boolean stopMonitoring(final String configurationId) {
        RuntimeMonitorEngine.getInstance().shutdown();
        // unsubscribe the specific service monitor events
        unsubscribeToInteractionEvents();
        MonitoringResultEventForwarder.unsubscribe();
        return true;
    }

    /**
     * Build the SLATemplate object starting from an
     * input stream that contains the slat.
     *
     * @param is the input stream contail the slat in a text format
     * @return SLATemplate the SLATemplate surfable object
     */
    private SLATemplate buildSLATemplate(final InputStream is) {
        StringBuilder sb = new StringBuilder();
        String line = new String();
        BufferedReader reader = null;

        SLA slaTemplate = null;
        if (this.sslamContext != null) {
            /* It is running in pax-runner or an osgi bundle */
            try {

                reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }

                java.util.Hashtable<ISyntaxConverter.SyntaxConverterType, ISyntaxConverter> systaxconverterslam =
                        this.sslamContext.getSyntaxConverters();

                ISyntaxConverter sc =
                        systaxconverterslam
                                .get(ISyntaxConverter.SyntaxConverterType.SLASOISyntaxConverter);

                slaTemplate =
                        (SLA) (sc.parseSLA(sb.toString()));

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            /* It is supposed that it run from the junit test case */
            try {
                /*
                 * Use directly the parser without use
                 * the syntax converter bundle
                 */
                SLADocument slaXml = SLADocument.Factory.parse(is);
                SLASOIParser slaParser = new SLASOIParser();
                slaTemplate = slaParser.parseSLA(slaXml.xmlText());
//                SLASOITemplateParser slasoiParser = new SLASOITemplateParser();
//                slaTemplate = slasoiParser.parseTemplate(sb.toString());
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return slaTemplate;
    }

    /**
     * Add to Fbkrcg configuration the SLAT.
     *
     * @param is the input stream containing the slat in text format
     */
    public final void addSLATToConfiguration(final InputStream is) {
        SLATemplate slaTemplate = buildSLATemplate(is);

        ConfigurationFactory cf = new ConfigurationFactoryImpl();
        ReasonerConfiguration rc = cf.createReasonerConfiguration();

        rc.setSpecification(slaTemplate);

        /*
         * addConfiguration belong to the public RCG API;
         * this method must be used to pass a SLA/T to the fbkrcg also
         */
        this.addConfiguration(rc);
    }

    /**
     * Add to Fbkrcg configuration the SLA.
     *
     * @param is the input stream containing the slat in text format
     */
    public final void addSLAToConfiguration(final InputStream is) {
        SLATemplate sla = buildSLA(is);

        ConfigurationFactory cf = new ConfigurationFactoryImpl();
        ReasonerConfiguration rc = cf.createReasonerConfiguration();

        rc.setSpecification(sla);

        this.addConfiguration(rc);
    }

    /**
     * Build the SLA passing an input stream;
     * remember that the SLA extends the SLAT object.
     *
     * @param is the input stream containing the sla in text format
     * @return SLA the SLA surfable object
     */
    private SLA buildSLA(final InputStream is) {
        try {
            SLADocument slaXml = SLADocument.Factory.parse(is);
            SLASOIParser slaParser = new SLASOIParser();
            SLA sla = slaParser.parseSLA(slaXml.xmlText());
            return sla;
        } catch (XmlException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * ORC event generation.
     */
    public final void generateEvents() {
        MonitorEvent eventStart1 = new MonitorEvent("paymentValidationStart");
        eventStart1.getParameters().put("name", "paymentValidationStart");
        eventStart1.getParameters().put("processId", new Integer(2));
        eventStart1.setTimestamp((long) 2000);
        RuntimeMonitorEngine.getInstance().dispatchMessage(eventStart1);

        MonitorEvent eventStop2 = new MonitorEvent("paymentValidationStop");
        eventStop2.getParameters().put("name", "paymentValidationStop");
        eventStop2.getParameters().put("processId",  new Integer(2));
        eventStop2.setTimestamp((long) 2498);
        RuntimeMonitorEngine.getInstance().dispatchMessage(eventStop2);

        MonitorEvent eventStart2 = new MonitorEvent("paymentValidationStart");
        eventStart2.getParameters().put("name", "paymentValidationStart");
        eventStart2.getParameters().put("processId", new Integer(4));
        eventStart2.setTimestamp((long) 5000);
        RuntimeMonitorEngine.getInstance().dispatchMessage(eventStart2);

        MonitorEvent eventStop1 = new MonitorEvent("paymentValidationStop");
        eventStop1.getParameters().put("name", "paymentValidationStop");
        eventStop1.getParameters().put("processId", new Integer(4));
        eventStop1.setTimestamp((long) 5699);
        RuntimeMonitorEngine.getInstance().dispatchMessage(eventStop1);

    }

    /**
     * Refresh the monitoring system.
     * @param  is InputStream <SLA>
     * @return boolean return if the service is refreshed correctly
     * @throws InterruptedException e
     */
    public final boolean replaceSLA(final InputStream is) throws InterruptedException {

       SLATemplate sla = buildSLATemplate(is);

       //step1 -> save monitoring terms value of previous version
       updateLocalInventory(sla.getUuid().getValue());

       //step2 -> clean instances of previous version
       RuntimeMonitorEngine.getInstance().getInventory().cleanSLAMonitors(sla.getUuid().getValue());

       //step3 -> clean existing .java files
       cleanDeployDir(sla.getUuid().toString());

       // check for cleaned SLA
       System.out.println("Number of monitors for SLA(" + sla.getUuid()
               + ") is " + reportAllMonitoredTermsValues(sla.getUuid().getValue()).size());

       //step4 --> load new SLA
       ConfigurationFactory cf = new ConfigurationFactoryImpl();
       ReasonerConfiguration rc = cf.createReasonerConfiguration();
       rc.setSpecification(sla);
       addConfiguration(rc);

       return true;
    }

    /**
     * update local inventory with sla monitor data.
     * @param slaUuid String
     */
    private void updateLocalInventory(final String slaUuid) {

        for (IMonitor m : RuntimeMonitorEngine.getInstance().getInventory().lookupMonitor()) {
            if (m.getName().contains(slaUuid)) {
                slaMonitorMap.put(m.getName(), m.getValue());
            }
        }
    }

    /**
    * API for getting monitored terms (KPIs/Variables/GSs) value for request SLA by name.
    * @param  monitorTermName String
    * @param  slaUuid String
    * @return Object Object(monitorTermValue) can be type casted to integer/boolean/string as per requirement
    * @throws ParseException e
    */
    public final Object reportMonitoredTermValue(final String slaUuid, final String monitorTermName)
    throws ParseException {
        Object obj = null;
        if (!monitorTermName.isEmpty()) {
            if (monitorTermName.indexOf("days_of_service") != -1) { // days_of_service
                obj = daysOfServiceKPI(slaUuid);
            } else if (monitorTermName.indexOf("penalty") != -1) { // penalty
                obj = penaltyKPI(slaUuid);
            } else if (monitorTermName.indexOf("percent_late_pickup") != -1) { //percent_late_pickup
                obj = percentLatePickup(slaUuid);
            } else if (monitorTermName.indexOf("percent_late_arrival") != -1) { //percent_late_arrival
                obj = percentLateArrival(slaUuid);
            } else {
                Object temp =
                        RuntimeMonitorEngine.getInstance().getInventory().lookupMonitorInstance(
                                new org.soa.monitor.runtime.core.MonitorReference(monitorTermName + "_" + slaUuid,
                                        null, ""));
                if (temp != null) {
                    obj = ((IMonitor) temp).getValue();
                }
            }
        }

        //mockup to be deleted later.
        /*if (slaUuid != null && monitorTermName != null) {
         if (monitorTermName.startsWith("G")) { //Guaranteed
             obj = "true";
         } else if (monitorTermName.indexOf("mean") != -1
                 || monitorTermName.indexOf("AVERAGE") != -1) { //mean Kpi
             obj = 4.0;
         } else if (monitorTermName.indexOf("max") != -1) { //max Kpi
             obj = 6.0;
         } else if (monitorTermName.indexOf("percent") != -1) { // percent Kpi
             obj = 0.25;
         } else if (monitorTermName.indexOf("numb") != -1) { // numb Kpi
             obj = 5.0;
         } else {
             obj = 3.0;
         }
        }*/
        return obj;
    }

    /**
    * API to report all monitored terms (KPI/variables/GSs) values.
    * @param  slaUuid String
    * @return Hashmap<String monitorTermName, Object monitorTermValue>
    */
    public final java.util.HashMap<String, Object> reportAllMonitoredTermsValues(final String slaUuid) {
        java.util.HashMap<String, Object> map = new java.util.HashMap<String, Object>();
        for (org.soa.monitor.runtime.core.IMonitor m : RuntimeMonitorEngine.getInstance().getInventory()
                .lookupMonitor()) {
            if (m.getName().contains(slaUuid)) {
                map.put(m.getName().substring(0, m.getName().lastIndexOf("_")), m.getValue());
            }
        }
        return map;
    }

    /**
    * API for getting monitored terms (KPIs/Variables/GSs) value for previous SLA by name.
    * @param  monitorTermName String
    * @param  slaUuid String
    * @return Object Object(monitorTermValue) can be type casted to integer/boolean/string as per requirement
    * @throws ParseException e
    */

    public final Object reportLastMonitoredTermValue(final String slaUuid, final String monitorTermName)
    throws ParseException {
        return slaMonitorMap.get(monitorTermName + "_" + slaUuid);
    }

    /**
    * API to report all monitored terms (KPI/variables/GSs) values.
    * @param  slaUuid String
    * @return Hashmap<String monitorTermName, Object monitorTermValue>
    */
    public final java.util.HashMap<String, Object> reportAllLastMonitoredTermsValues(final String slaUuid) {
        java.util.HashMap<String, Object> map = new java.util.HashMap<String, Object>();
        java.util.Set set = slaMonitorMap.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
          Map.Entry me = (Map.Entry) i.next();
          if (String.valueOf(me.getKey()).contains(slaUuid)) {
                map.put(String.valueOf(me.getKey()).substring(0, String.valueOf(me.getKey()).lastIndexOf("_")), me
                        .getValue());
          }
        }
        return map;
    }


    /**
     * Clears complete folder from the mess we make.
     */
    public final void cleanDeployDir() {
        // Declare variables
        File fLogDir = new File(deployDir);
        try {
        // Get all java files
        File[] fLogs = fLogDir.listFiles(new java.io.FilenameFilter()
        {
                public boolean accept(final File fDir, final String strName) {
                        return (strName.endsWith(".java"));
                }
        });

        // Delete all files
        for (int i = 0; i < fLogs.length; i++) {
                FileUtils.forceDelete(fLogs[i]);
                Thread.sleep(200);
        }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Clears folder from the mess we make.
     * @param pattern String
     */
    public final void cleanDeployDir(final String pattern) {
        // Declare variables
        File fLogDir = new File(deployDir);
        try {
        // Get all java files
        File[] fLogs = fLogDir.listFiles(new java.io.FilenameFilter()
        {
                public boolean accept(final File fDir, final String strName) {
                        return (strName.endsWith(".java"));
                }
        });

        // Delete all files
        for (int i = 0; i < fLogs.length; i++) {
            if (fLogs[i].getName().contains(pattern)) {
                FileUtils.forceDelete(fLogs[i]);
                Thread.sleep(200);
            }
        }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * method for retrieving threshold value for monitor term.
     * @param slaUuid String
     * @param monitorTermName String
     * @return threshold value String
     */
    public final String getMonitoringTermThreshold(final String slaUuid, final String monitorTermName) {
        String returnValue = "";
        Object slat = FbkRCGSlatRepository.getSlat(slaUuid);
        if (slat instanceof SLATemplate) {
            SLATemplate slatemplate = (SLATemplate) slat;
            int lenght = slatemplate.getAgreementTerms().length;
            for (int i = 0; i < lenght; i++) {
                AgreementTerm att = slatemplate.getAgreementTerms()[i];
                // check for precondition.
                if (String.valueOf(att.getPrecondition()).startsWith(monitorTermName)) {
                    TypeConstraintExpr exp = (TypeConstraintExpr) att.getPrecondition();
                    SimpleDomainExpr attT = (SimpleDomainExpr) exp.getDomain();
                    returnValue = String.valueOf(attT);
                    return returnValue;
                } else { // check for guarantee terms.
                    int gaLenght = att.getGuarantees().length;
                    for (int j = 0; j < gaLenght; j++) {
                        Guaranteed gt = att.getGuarantees()[j];
                        // check for GA.
                        if (!(gt instanceof Guaranteed.State)) {
                            continue;
                        }
                        Guaranteed.State state = (Guaranteed.State) gt;
                        TypeConstraintExpr tce = (TypeConstraintExpr) state.getState();
                        if (gt.getId().getValue().equalsIgnoreCase(monitorTermName)
                                || String.valueOf(tce.getValue()).equalsIgnoreCase(monitorTermName)) {
                        SimpleDomainExpr sde = (SimpleDomainExpr) tce.getDomain();
                        // name/operator/value.
                            returnValue =
                                    tce.getValue() + "/" + sde.getComparisonOp().toString() + "/" + String.valueOf(sde);
                        return returnValue;
                        }
                    }
                }
            }
        }
        return returnValue;
    }

    /**
     * method to generate reports for all SLA in xml format.
     * @param timestamp timestamp.
     */
    public final void generateReport(final Long timestamp) {
        int reportingYear = 0;
        int reportingMonth = 0;
        String mm = "";
        // get report time.
        java.util.Date date = new java.util.Date(timestamp);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        reportingYear = cal.get(Calendar.YEAR);
        reportingMonth = cal.get(Calendar.MONTH) + 1;
        if (reportingMonth < 10) {
            mm  = "0" + String.valueOf(reportingMonth);
        } else {
            mm = String.valueOf(reportingMonth);
        }
        java.util.Set set = FbkRCGSlatRepository.getInstance().entrySet();
        Iterator i = set.iterator();
        String customerName = null;
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            if (me.getKey() != null) {
                if (me.getValue() instanceof SLA) {
                    SLA sla = (SLA) me.getValue();
                    // customer name
                    for (Party party : sla.getParties()) {
                        if (party.getAgreementRole().getValue().equalsIgnoreCase(
                                "http://www.slaatsoi.org/slamodel#customer")) {
                            customerName = party.getId().getValue();
                            break;
                        }
                    }
                    // effectiveDateFrom
                    TIME fromDate = sla.getEffectiveFrom();
                    // effectiveDateTo
                    TIME toDate = sla.getEffectiveUntil();
                    //create report only if its provider-kpi map is not null.
                    if (ProviderKPIRepository.getProviderKPIMap(sla.getUuid().getValue()) != null) {
                        // generate monitoring result events.
                        MonitoringResultEventGenerator mreG = new MonitoringResultEventGenerator(sla);
                        System.err.println("monitoring result event generattion for SLA " + sla.getUuid().getValue());
                        mreG.generate();
                        mreG.resetViolationCounters();

                        // generate reports.
                        ReportGenerator repG =
                                new ReportGenerator(sla, customerName, fromDate, toDate, String.valueOf(reportingYear),
                                        mm);
                        repG.generateReport();

                    }

                }
            }
        }
        // clear map repository.
        //ProviderKPIRepository.getInstance().clear();
        // reset violation counters.
        //FbkRCGNotificationPlugin.resetViolationCounters();
    }

    /**
     * method to generate report for particular SLA in xml format.
     * @param slaUuid String
     * @param timestamp timestamp
     */
    public final void generateReport(final String slaUuid, final Long timestamp) {
       int reportingYear = 0;
       int reportingMonth = 0;
       String mm = "";
       // get report time.
       java.util.Date date = new java.util.Date(timestamp);
       Calendar cal = Calendar.getInstance();
       cal.setTime(date);
       reportingYear = cal.get(Calendar.YEAR);
       reportingMonth = cal.get(Calendar.MONTH) + 1;
       if (reportingMonth < 10) {
           mm  = "0" + String.valueOf(reportingMonth);
       } else {
           mm = String.valueOf(reportingMonth);
       }
       Object obj = FbkRCGSlatRepository.getSlat(slaUuid);
       String customerName = null;
       if (obj != null && obj instanceof SLA) {
           SLA sla = (SLA) obj;
           //customer name
           for (Party party : sla.getParties()) {
               if (party.getAgreementRole().getValue().equalsIgnoreCase(
                       "http://www.slaatsoi.org/slamodel#customer")) {
                   customerName = party.getId().getValue();
                   break;
               }
           }
           //effectiveDateFrom
           TIME fromDate = sla.getEffectiveFrom();
           //effectiveDateTo
           TIME toDate = sla.getEffectiveUntil();
           ReportGenerator repG = new ReportGenerator(sla,
                   customerName, fromDate, toDate, String.valueOf(reportingYear), mm);
           repG.generateReport();
       }
    }

    /**
     * days of service reporting KPI.
     * @param slaUuid String.
     * @return obj Object.
     */
    private Long daysOfServiceKPI(final String slaUuid) {
     //get SLA effective time.
     SLA sla = (SLA) FbkRCGSlatRepository.getSlat(slaUuid);
     Calendar cal = sla.getEffectiveFrom().getValue();
     //subtract from current time.
     Long diff = (System.currentTimeMillis() - cal.getTimeInMillis());
     // difference in seconds.
     Long days = diff / (1000 * 60 * 60 * 24);
     return days;
    }

    /**
     * penalty KPI.
     * @param slaUuid String
     * @return penalty Float
     */
    private Float penaltyKPI(final String slaUuid) {
        Float penalty = new Float(0.0);
        Float const1 = new Float(0.0);
        Float const2 = new Float(0.0);
        Float const3 = new Float(0.0);
        int vCount = 0;
        try {
            //get violation count
            if (ViolationCounterRepository.getInstance().get(slaUuid) != null) {
                vCount = ViolationCounterRepository.getInstance().get(slaUuid);
            }
            //get constants.
            SLA sla = (SLA) FbkRCGSlatRepository.getSlat(slaUuid);
            VariableDeclr var = sla.getVariableDeclr("penalty");
            Expr expr = var.getExpr();
            FunctionalExpr fe = (FunctionalExpr) expr;
            //*(pcc, "0.70" EUR) [0]
            FunctionalExpr valueExp1 = (FunctionalExpr) fe.getParameters()[0];
            ValueExpr op = valueExp1.getParameters()[1]; //
            CONST opToConst = (CONST) op;
            // first constant
            const1 = new Float(opToConst.getValue());
            //*(numb_violations, *("0.70" EUR, "1000")) [1]
            FunctionalExpr valueExp2 = (FunctionalExpr) fe.getParameters()[1];
            FunctionalExpr valueExp22 = (FunctionalExpr) valueExp2.getParameters()[1];
            ValueExpr op21 = valueExp22.getParameters()[0];
            ValueExpr op22 = valueExp22.getParameters()[1];
            CONST opToConst21 = (CONST) op21;
            const2 = new Float(opToConst21.getValue());
            CONST opToConst22 = (CONST) op22;
            const3 = new Float(opToConst22.getValue());
            //get pcc.
            Object valueKPI = reportMonitoredTermValue(slaUuid, String.valueOf("pcc"));
            if (valueKPI != null) {
                Float part1 = Float.parseFloat(String.valueOf(valueKPI)) * const1;
                Float part2 = vCount * const2 * const3;
                penalty = part1 + part2;
                System.out.println("part1 is " + valueKPI.toString() + "*" + const1);
                System.out.println("part2 is " + vCount + "*" + const2 + "*" + const3);
                System.out.println("penalty for SLA with UUID " + slaUuid + "is " + penalty);
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return penalty;
    }

    /**
     * percent_late_pickup KPI.
     * @param slaUuid String.
     * @return percent Float.
     */
    private Float percentLatePickup(final String slaUuid) {
        Float percent = new Float(0.0);
        Float nominator = new Float(0.0);
        Float denominator = new Float(0.0);

        try {
            // nominator -> num_late_pickup.
            Object nominatorKPIValue = reportMonitoredTermValue(slaUuid, String.valueOf("num_late_pickup"));
            if (nominatorKPIValue != null) {
                nominator = Float.parseFloat(String.valueOf(nominatorKPIValue));
            }
            // denominator -> numb_transported_citizens
            Object denominatorKPIValue = reportMonitoredTermValue(slaUuid, String.valueOf("numb_transported_citizens"));
            if (denominatorKPIValue != null) {
                denominator = Float.parseFloat(String.valueOf(denominatorKPIValue));
            }
            percent = nominator / denominator;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return percent;
    }

    /**
     * percent_late_arrival KPI.
     * @param slaUuid String.
     * @return percent Float.
     */
    private Float percentLateArrival(final String slaUuid) {
        Float percent = new Float(0.0);
        Float nominator = new Float(0.0);
        Float denominator = new Float(0.0);

        try {
            // nominator -> num_late_arrival.
            Object nominatorKPIValue = reportMonitoredTermValue(slaUuid, String.valueOf("num_late_arrival"));
            if (nominatorKPIValue != null) {
                nominator = Float.parseFloat(String.valueOf(nominatorKPIValue));
            }
            // denominator -> numb_arrived_citizens
            Object denominatorKPIValue = reportMonitoredTermValue(slaUuid, String.valueOf("numb_arrived_citizens"));
            if (denominatorKPIValue != null) {
                denominator = Float.parseFloat(String.valueOf(denominatorKPIValue));
            }
            percent = nominator / denominator;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return percent;
    }
}
