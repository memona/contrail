/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slasoi.monitoring.fbk.reporting.schema.ConstraintType;
import org.slasoi.monitoring.fbk.reporting.schema.ConstraintTypeType;
import org.slasoi.monitoring.fbk.reporting.schema.IndicatorType;
import org.slasoi.monitoring.fbk.reporting.schema.IndicatorsType;
import org.slasoi.monitoring.fbk.reporting.schema.ObjectFactory;
import org.slasoi.monitoring.fbk.reporting.schema.ProviderType;
import org.slasoi.monitoring.fbk.reporting.schema.SLAReportType;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.VariableDeclr;


/**
 * XML Report Generator.
 * @author nawazk
 */
@SuppressWarnings("restriction")
public class ReportGenerator {

    /** UUID of SLA. **/
    private String slaUuid = null;
    /** SLA. **/
    private SLA sla = null;
    /** report year.**/
    private String reportY = null;
    /** report month.**/
    private String reportM = null;
    /** customer name. **/
    private String customerName = null;
    /** time. **/
    private TIME from = null;
    /** report day.**/
    private TIME to = null;
    /** report. **/
    private PrintWriter report = null;
    /** flag for time kpi. **/
    private Boolean timeFlag = false;
    /** FBK Rcg instance. **/
    private static FbkRCGServiceImpl fbkRcgInstance = null;
    /** location of generated report. **/
    private String reportLocation =
            System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "fbkrcg"
                    + System.getProperty("file.separator") + "reports";

    /**
     * constructor.
     * @param slaIn SLA.
     * @param custName String.
     * @param fromDate TIME.
     * @param untilDate TIME.
     * @param reportYIn String.
     * @param reportMIn String.
     */
    public ReportGenerator(final SLA slaIn, final String custName,
            final TIME fromDate, final TIME untilDate,
            final String reportYIn, final String reportMIn) {
        sla = slaIn;
        customerName = custName;
        from = fromDate;
        to = untilDate;
        fbkRcgInstance = new FbkRCGServiceImpl();
        slaUuid = sla.getUuid().getValue();
        reportY = reportYIn;
        reportM = reportMIn;
    }

    /**
     * generate report for specific SLA.
     */
    @SuppressWarnings("restriction")
    public final void generateReport() {

        try {
            // report file/location
            report =
                    new PrintWriter(new File(reportLocation + "/b6_report_" + slaUuid + "_" + reportY + reportM
                            + ".xml"));
            SLAReportType slaReportType = null;
            System.err.println("report generation for sla id " + slaUuid);
            // retrieve SLA provider-kpi map from map repo.
            // map (Provider, GS/GAs)
                java.util.HashMap<String, Object> map =
                        (java.util.HashMap<String, Object>) ProviderKPIRepository.getProviderKPIMap(slaUuid);

                ObjectFactory of = new ObjectFactory();

                slaReportType = of.createSLAReportType();
                slaReportType.setCustomer(customerName);
                slaReportType.setSlaId(Long.parseLong(slaUuid));
                //from date.
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(from.getValue().getTime());
                XMLGregorianCalendar dateFrom = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
                slaReportType.setFromDate(dateFrom);
                //to date.
                cal.setTime(to.getValue().getTime());
                XMLGregorianCalendar dateTo = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
                slaReportType.setToDate(dateTo);

                java.util.Set set = map.entrySet();
                Iterator iProvKey = set.iterator();

                while (iProvKey.hasNext()) {

                    Map.Entry me = (Map.Entry) iProvKey.next();
                    String providerName = me.getKey().toString();
                    Object value = me.getValue();
                    IndicatorsType indTs = of.createIndicatorsType();
                    if (value != null) {
                        java.util.ArrayList<Object> list = (java.util.ArrayList<Object>) value;
                        for (Object o : list) {
                            IndicatorType indT = null;
                            if (o instanceof Guaranteed.State) {
                                // flag to identify if kpi is of datatype time
                                timeFlag = false;
                                // GS
                                Guaranteed.State state = (Guaranteed.State) o;
                                String gsName = state.getId().getValue();
                                indT = of.createIndicatorType();
                                indT.setId(gsName);
                                indT.setType("guaranteed_state");
                                TypeConstraintExpr tce = (TypeConstraintExpr) state.getState();
                                // name of KPI
                                Object tceValue = tce.getValue();
                                indT.setName(tceValue.toString());
                                VariableDeclr varD = sla.getVariableDeclr(tceValue.toString());
                                indT.setDescription(varD.getDescr());
                                SimpleDomainExpr sde = (SimpleDomainExpr) tce.getDomain();
                                ConstraintType conT = of.createConstraintType();
                                Object valueT = sde.getValue();
                                if (valueT instanceof CONST) {
                                    CONST constant = (CONST) valueT;
                                    conT.setValue(Long.parseLong(constant.getValue()));
                                    //check if data type is time, set time flag to true.
                                    if (constant.getDatatype() != null
                                            && (constant.getDatatype().getValue().equalsIgnoreCase(
                                        "http://www.slaatsoi.org/coremodel/units#s")
                                        || constant.getDatatype().getValue().equalsIgnoreCase(
                                                "http://www.slaatsoi.org/coremodel/units#hrs")
                                        || constant.getDatatype().getValue().equalsIgnoreCase(
                                                "http://www.slaatsoi.org/coremodel/units#min"))) {
                                        timeFlag = true;
                                    }
                                }
                                conT.setType(getDataType(sde.getComparisonOp().getValue()));
                                indT.setConstraint(conT);
                                Object valueKPI = fbkRcgInstance.reportMonitoredTermValue(
                                        slaUuid, String.valueOf(tceValue));
                                if (valueKPI != null) {
                                    //convert time in minutes.
                                    if (timeFlag) {
                                        indT.setValue(Float.parseFloat(String.valueOf(valueKPI)) / (1000 * 60));
                                    } else { // normal KPIs.
                                        indT.setValue(Float.parseFloat(String.valueOf(valueKPI)));
                                    }

                                }
                                Object gsState = fbkRcgInstance.reportMonitoredTermValue(
                                        slaUuid, String.valueOf(gsName));
                                if (gsState != null) {
                                    indT.setState(Boolean.parseBoolean(String.valueOf(gsState)));
                                }
                            } else if (o instanceof ValueExpr) {
                                //flag to identify if kpi is of datatype time
                                timeFlag = false;
                                // GA
                                ValueExpr kpi = (ValueExpr) o;
                                String gaName = kpi.toString();
                                VariableDeclr varD = sla.getVariableDeclr(gaName);
                                indT = of.createIndicatorType();
                                indT.setType("kpi");
                                indT.setName(gaName);
                                indT.setDescription(varD.getDescr());

                                //filter on time kpis.
                                if (gaName.equalsIgnoreCase("max_health_book_time")
                                        || gaName.equalsIgnoreCase("max_mobil_book_time")
                                        || gaName.equalsIgnoreCase("max_book_time")
                                        || gaName.equalsIgnoreCase("mean_health_book_time")
                                        || gaName.equalsIgnoreCase("mean_mobil_book_time")
                                        || gaName.equalsIgnoreCase("mean_book_time")) {
                                    timeFlag = true;
                                }

                                Object gaValue = fbkRcgInstance.reportMonitoredTermValue(
                                        slaUuid, String.valueOf(gaName));
                                if (gaValue != null) {
                                    if (timeFlag) { //convert time to seconds.
                                        indT.setValue(Float.parseFloat(String.valueOf(gaValue)) / (1000 * 60));
                                    } else { //normal KPIs.
                                        indT.setValue(Float.parseFloat(String.valueOf(gaValue)));
                                    }
                                }
                            }
                            // add indicators to indicator types after first loop
                            if (indT != null) {
                                indTs.getIndicator().add(indT);
                            }
                        }
                    }
                    // create provider entry here.
                    if (indTs != null) {
                        ProviderType provT = of.createProviderType();
                        provT.setName(providerName);
                        provT.setIndicators(indTs);
                        slaReportType.getProvider().add(provT);
                    }

                }
            if (slaReportType != null) {
                StringWriter sw = new StringWriter();
                JAXBElement<SLAReportType> slaReport = of.createSLAReport(slaReportType);
                JAXBContext context = null;
                String packageName = "org.slasoi.monitoring.fbk.reporting.schema";
                context = JAXBContext.newInstance(packageName);
                Marshaller marshaller = null;
                marshaller = context.createMarshaller();
                marshaller.marshal(slaReport, sw);
                System.out.println(sw.getBuffer().toString());
                // flush to file system.
                report.append(sw.getBuffer().toString());
                report.flush();
            }

        } catch (JAXBException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (FileNotFoundException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DatatypeConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Get ConstraintTypeType corresponding to STND.
     * @param operator String
     * @return type ConstraintTypeType
     */
    private ConstraintTypeType getDataType(final String operator) {
        ConstraintTypeType type = null;
        if (operator.equals("+")) {
            type = ConstraintTypeType.EQUAL;
        } else if (operator.equals("http://www.slaatsoi.org/coremodel#greater_than")) {
            type = ConstraintTypeType.GREATER;
        } else if (operator.equals("http://www.slaatsoi.org/coremodel#greater_than_or_equals")) {
            type = ConstraintTypeType.GREATER_EQUAL;
        } else if (operator.equals("http://www.slaatsoi.org/coremodel#less_than")) {
            type = ConstraintTypeType.LESSER;
        } else if (operator.equals("http://www.slaatsoi.org/coremodel#less_than_or_equals")) {
            type = ConstraintTypeType.LESSER_EQUAL;
        }
        return type;
    }

    /**
     * Flush a text file into a String.
     * @param fileName String.
     * @throws Exception Exception
     * @return contents String.
     */
    private String flushFileToString(final String fileName) throws Exception {
        File file = new File(fileName);
        StringBuffer contents = new StringBuffer();
        BufferedReader reader = null;
        reader = new BufferedReader(new FileReader(file));
        String text = null;
        // repeat until all lines is read
        while ((text = reader.readLine()) != null) {
            contents.append(text).append(System.getProperty("line.separator"));
        }
        // show file contents here
        return contents.toString();
    }

    /**
     * getReport.
     * @return reportObject Object
     */
    public final Object getReport() {
        Object reportObject = null;
        try {
            SLAReportType slaReportType = null;
            System.err.println("report object generation for sla id " + slaUuid);
            // retrieve SLA provider-kpi map from map repo.
            // map (Provider, GS/GAs)
                java.util.HashMap<String, Object> map =
                        (java.util.HashMap<String, Object>) ProviderKPIRepository.getProviderKPIMap(slaUuid);

                ObjectFactory of = new ObjectFactory();

                slaReportType = of.createSLAReportType();
                slaReportType.setCustomer(customerName);
                slaReportType.setSlaId(Long.parseLong(slaUuid));
                //from date.
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(from.getValue().getTime());
                XMLGregorianCalendar dateFrom = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
                slaReportType.setFromDate(dateFrom);
                //to date.
                cal.setTime(to.getValue().getTime());
                XMLGregorianCalendar dateTo = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
                slaReportType.setToDate(dateTo);

                java.util.Set set = map.entrySet();
                Iterator iProvKey = set.iterator();

                while (iProvKey.hasNext()) {

                    Map.Entry me = (Map.Entry) iProvKey.next();
                    String providerName = me.getKey().toString();
                    Object value = me.getValue();
                    IndicatorsType indTs = of.createIndicatorsType();
                    if (value != null) {
                        java.util.ArrayList<Object> list = (java.util.ArrayList<Object>) value;
                        for (Object o : list) {
                            IndicatorType indT = null;
                            if (o instanceof Guaranteed.State) {
                                // GS
                                Guaranteed.State state = (Guaranteed.State) o;
                                String gsName = state.getId().getValue();
                                indT = of.createIndicatorType();
                                indT.setId(gsName);
                                indT.setType("guaranteed_state");
                                TypeConstraintExpr tce = (TypeConstraintExpr) state.getState();
                                // name of KPI
                                Object tceValue = tce.getValue();
                                indT.setName(tceValue.toString());
                                VariableDeclr varD = sla.getVariableDeclr(tceValue.toString());
                                indT.setDescription(varD.getDescr());
                                SimpleDomainExpr sde = (SimpleDomainExpr) tce.getDomain();
                                ConstraintType conT = of.createConstraintType();
                                Object valueT = sde.getValue();
                                if (valueT instanceof CONST) {
                                    CONST constant = (CONST) valueT;
                                    conT.setValue(Long.parseLong(constant.getValue()));
                                }
                                conT.setType(getDataType(sde.getComparisonOp().getValue()));
                                indT.setConstraint(conT);
                                Object valueKPI = fbkRcgInstance.reportMonitoredTermValue(
                                        slaUuid, String.valueOf(tceValue));
                                if (valueKPI != null) {
                                    indT.setValue(Float.parseFloat(String.valueOf(valueKPI)));
                                }
                                Object gsState = fbkRcgInstance.reportMonitoredTermValue(
                                        slaUuid, String.valueOf(gsName));
                                if (gsState != null) {
                                    indT.setState(Boolean.parseBoolean(String.valueOf(gsState)));
                                }
                            } else if (o instanceof ValueExpr) {
                                // GA
                                ValueExpr kpi = (ValueExpr) o;
                                String gaName = kpi.toString();
                                VariableDeclr varD = sla.getVariableDeclr(gaName);
                                indT = of.createIndicatorType();
                                indT.setType("kpi");
                                indT.setName(gaName);
                                indT.setDescription(varD.getDescr());
                                Object gaValue = fbkRcgInstance.reportMonitoredTermValue(
                                        slaUuid, String.valueOf(gaName));
                                if (gaValue != null) {
                                    indT.setValue(Float.parseFloat(String.valueOf(gaValue)));
                                }
                            }
                            // add indicators to indicator types after first loop
                            if (indT != null) {
                                indTs.getIndicator().add(indT);
                            }
                        }
                    }
                    // create provider entry here.
                    if (indTs != null) {
                        ProviderType provT = of.createProviderType();
                        provT.setName(providerName);
                        provT.setIndicators(indTs);
                        slaReportType.getProvider().add(provT);
                    }

                }
            if (slaReportType != null) {
                StringWriter sw = new StringWriter();
                JAXBElement<SLAReportType> slaReport = of.createSLAReport(slaReportType);
                JAXBContext context = null;
                String packageName = "org.slasoi.monitoring.fbk.reporting.schema";
                context = JAXBContext.newInstance(packageName);
                Marshaller marshaller = null;
                marshaller = context.createMarshaller();
                marshaller.marshal(slaReport, sw);
                reportObject = sw.getBuffer().toString();
            }
        } catch (JAXBException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DatatypeConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return reportObject;
    }

}
