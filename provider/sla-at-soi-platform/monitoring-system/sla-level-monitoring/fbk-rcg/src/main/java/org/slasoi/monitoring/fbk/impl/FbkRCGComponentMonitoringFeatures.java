/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import org.slasoi.monitoring.common.features.Basic;
import org.slasoi.monitoring.common.features.Event;
import org.slasoi.monitoring.common.features.Function;
import org.slasoi.monitoring.common.features.MonitoringFeature;
import org.slasoi.monitoring.common.features.impl.ComponentMonitoringFeaturesImpl;
import org.slasoi.monitoring.common.features.impl.FeaturesFactoryImpl;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.meta;

/**
 * This contains the component monitoring feature handled by the fbk reasoning component gateway.
 * 
 * @since 0.1
 */
public final class FbkRCGComponentMonitoringFeatures extends ComponentMonitoringFeaturesImpl {

    /** The singleton monitoring feature instance. */
    private static FbkRCGComponentMonitoringFeatures instance = null;

    /** Extension Type Mappings. **/
    private String $arrayOfNUMBER = "http://www.slaatsoi.org/types#array_of_NUMBER";

    /**
     * Private constructor to avoid to have an instance without the get_instance call.
     */
    private FbkRCGComponentMonitoringFeatures() {

        FeaturesFactoryImpl ffi = new FeaturesFactoryImpl();
        MonitoringFeature[] monitoringFeatures = new MonitoringFeature[20];

        /** REASONER Features **/

        // LessThan
        Function lessThanF = ffi.createFunction();
        lessThanF.setName(core.$less_than);
        lessThanF.setDescription(core.$less_than);
        Basic[] lessThanFIs = new Basic[2];
        Basic lessThanFI1 = ffi.createPrimitive();
        lessThanFI1.setName("input1");
        lessThanFI1.setType(meta.$NUMBER);
        Basic lessThanFI2 = ffi.createPrimitive();
        lessThanFI2.setName("input2");
        lessThanFI2.setType(meta.$NUMBER);
        lessThanFIs[0] = lessThanFI1;
        lessThanFIs[1] = lessThanFI2;
        lessThanF.setInput(lessThanFIs);
        Basic lessThanFO = ffi.createPrimitive();
        lessThanFO.setName("output1");
        lessThanFO.setType(meta.$BOOLEAN);
        lessThanF.setOutput(lessThanFO);

        // GreaterThan
        Function greaterThanF = ffi.createFunction();
        greaterThanF.setName(core.$greater_than);
        greaterThanF.setDescription("none");
        Basic[] greaterThanFIs = new Basic[2];
        Basic greaterThanFI1 = ffi.createPrimitive();
        greaterThanFI1.setName("param1");
        greaterThanFI1.setType(meta.$NUMBER);
        Basic greaterThanFI2 = ffi.createPrimitive();
        greaterThanFI2.setName("param2");
        greaterThanFI2.setType(meta.$NUMBER);
        greaterThanFIs[0] = greaterThanFI1;
        greaterThanFIs[1] = greaterThanFI2;
        greaterThanF.setInput(greaterThanFIs);
        Basic greaterThanFO = ffi.createPrimitive();
        greaterThanFO.setName(meta.$BOOLEAN);
        greaterThanF.setOutput(greaterThanFO);

        // Availability Reasoner
        Function availabilityF = ffi.createFunction();
        availabilityF.setName(common.$availability);
        availabilityF.setDescription("none");
        Basic[] availabilityFIs = new Basic[1];
        Basic availabilityFI1 = ffi.createPrimitive();
        availabilityFI1.setName("param1");
        availabilityFI1.setType($arrayOfNUMBER);
        availabilityFIs[0] = availabilityFI1;
        availabilityF.setInput(availabilityFIs);
        Basic availabilityFO = ffi.createPrimitive();
        availabilityFO.setName(meta.$NUMBER);
        availabilityF.setOutput(lessThanFO);

        // Equals Reasoner
        Function equalsF = ffi.createFunction();
        equalsF.setName(core.$equals);
        equalsF.setDescription("none");
        Basic[] equalsFIs = new Basic[2];
        Basic equalsFI1 = ffi.createPrimitive();
        equalsFI1.setName("param1");
        equalsFI1.setType(meta.$NUMBER);
        Basic equalsFI2 = ffi.createPrimitive();
        equalsFI2.setName("param2");
        equalsFI2.setType(meta.$NUMBER);
        equalsFIs[0] = equalsFI1;
        equalsFIs[1] = equalsFI2;
        equalsF.setInput(equalsFIs);
        Basic equalsFO = ffi.createPrimitive();
        equalsFO.setName(meta.$BOOLEAN);
        equalsF.setOutput(equalsFO);

        // MEAN
        Function meanF = ffi.createFunction();
        meanF.setName(core.$mean);
        meanF.setDescription("none");
        Basic[] meanFIs = new Basic[1];
        Basic meanFI1 = ffi.createPrimitive();
        meanFI1.setName("param1");
        meanFI1.setType($arrayOfNUMBER);
        meanFIs[0] = meanFI1;
        meanF.setInput(meanFIs);
        Basic meanFO = ffi.createPrimitive();
        meanFO.setName("output1");
        meanFO.setType(meta.$NUMBER);
        meanF.setOutput(lessThanFO);

        // SERIES Reasoner
        Function seriesF = ffi.createFunction();
        seriesF.setName(core.$series);
        seriesF.setDescription("none");
        Basic[] seriesFIs = new Basic[2];
        Basic seriesFI1 = ffi.createPrimitive();
        seriesFI1.setName("param1");
        seriesFI1.setType(meta.$NUMBER);
        seriesFIs[0] = seriesFI1;
        Event seriesFI2 = ffi.createEvent();
        seriesFI2.setType("RESPONSE");
        seriesFIs[1] = seriesFI2;
        seriesF.setInput(seriesFIs);
        Basic seriesFO = ffi.createPrimitive();
        seriesFO.setName("output1");
        seriesFO.setType($arrayOfNUMBER);
        seriesF.setOutput(lessThanFO);

        // AND Reasoner
        Function andF = ffi.createFunction();
        andF.setName(core.$and);
        andF.setDescription(core.$and);
        Basic[] andIs = new Basic[2];
        Basic andI1 = ffi.createPrimitive();
        andI1.setName("input1");
        andI1.setType(meta.$BOOLEAN);
        Basic andI2 = ffi.createPrimitive();
        andI2.setName("input2");
        andI2.setType(meta.$BOOLEAN);
        andIs[0] = andI1;
        andIs[1] = andI2;
        andF.setInput(andIs);
        Basic andOs = ffi.createPrimitive();
        andOs.setName("output1");
        andOs.setType(meta.$BOOLEAN);
        andF.setOutput(andOs);

        // Count Formula
        Function countF = ffi.createFunction();
        countF.setName(core.$count);
        countF.setDescription("count the occurence of events");
        Basic[] countFIs = new Basic[1];
        Basic countFI1 = ffi.createEvent();
        countFI1.setType(meta.$EVENT);
        countFI1.setName("param");
        countFI1.setDescription("input events");
        countFIs[0] = countFI1;
        countF.setInput(countFIs);
        Basic countFO = ffi.createPrimitive();
        countFO.setType(meta.$NUMBER);
        countFO.setName("output");
        countFO.setDescription("number of occurence of event");
        countF.setOutput(countFO);

        // Division Formula
        Function divF = ffi.createFunction();
        divF.setName(core.$divide);
        divF.setDescription(core.$divide);
        Basic[] divFIs = new Basic[2];
        Basic divFI1 = ffi.createPrimitive();
        divFI1.setType(meta.$NUMBER);
        divFI1.setName("param1");
        Basic divFI2 = ffi.createPrimitive();
        divFI2.setName("param2");
        divFI2.setType(meta.$NUMBER);
        divFIs[0] = divFI1;
        divFIs[1] = divFI2;
        divF.setInput(divFIs);
        Basic divFO = ffi.createPrimitive();
        divFO.setName("result");
        divFO.setType(meta.$NUMBER);
        divF.setOutput(divFO);

        // EventReference Formula
        Function eventRF = ffi.createFunction();
        eventRF.setName("EventReference");
        eventRF.setDescription("report occurence of event");
        Basic[] eventRFIs = new Basic[1];
        Basic eventRFI = ffi.createEvent();
        eventRFI.setDescription("Interface/Operation/Related");
        eventRFI.setType(meta.$EVENT);
        eventRFIs[0] = eventRFI;
        eventRF.setInput(eventRFIs);
        Basic eventRFO = ffi.createPrimitive();
        eventRFO.setName("output");
        eventRFO.setType(meta.$BOOLEAN);
        eventRF.setOutput(eventRFO);

        // GreaterThanOrEquals
        Function greaterOEF = ffi.createFunction();
        greaterOEF.setName(core.$greater_than_or_equals);
        greaterOEF.setDescription(core.$greater_than_or_equals);
        Basic[] greaterOEIs = new Basic[2];
        Basic greaterOEI1 = ffi.createPrimitive();
        greaterOEI1.setName("param1");
        greaterOEI1.setType(meta.$NUMBER);
        Basic greaterOEI2 = ffi.createPrimitive();
        greaterOEI2.setName("param2");
        greaterOEI2.setType(meta.$NUMBER);
        greaterOEIs[0] = greaterOEI1;
        greaterOEIs[1] = greaterOEI2;
        greaterOEF.setInput(greaterOEIs);
        Basic greaterOEO = ffi.createPrimitive();
        greaterOEO.setName("output");
        greaterOEO.setType(meta.$BOOLEAN);
        greaterOEF.setOutput(greaterOEO);

        // LessThanOrEquals
        Function lessOEF = ffi.createFunction();
        lessOEF.setName(core.$less_than_or_equals);
        lessOEF.setDescription(core.$less_than_or_equals);
        Basic[] lessOEFIs = new Basic[2];
        Basic lessOEFI1 = ffi.createPrimitive();
        lessOEFI1.setName("param1");
        lessOEFI1.setType(meta.$NUMBER);
        Basic lessOEFI2 = ffi.createPrimitive();
        lessOEFI2.setName("param2");
        lessOEFI2.setType(meta.$NUMBER);
        lessOEFIs[0] = lessOEFI1;
        lessOEFIs[1] = lessOEFI2;
        lessOEF.setInput(lessOEFIs);
        Basic lessOEFO = ffi.createPrimitive();
        lessOEFO.setName("output");
        lessOEFO.setType(meta.$BOOLEAN);
        lessOEF.setOutput(lessOEFO);

        // Minus Formula
        Function subtractF = ffi.createFunction();
        subtractF.setName(core.$subtract);
        subtractF.setDescription(core.$subtract);
        Basic[] subtractIs = new Basic[2];
        Basic subtractI1 = ffi.createPrimitive();
        subtractI1.setName("param1");
        subtractI1.setType(meta.$NUMBER);
        Basic subtractI2 = ffi.createPrimitive();
        subtractI2.setName("param2");
        subtractI2.setType(meta.$NUMBER);
        subtractIs[0] = subtractI1;
        subtractIs[1] = subtractI2;
        subtractF.setInput(subtractIs);
        Basic subtractO = ffi.createPrimitive();
        subtractO.setName("result");
        subtractO.setType(meta.$NUMBER);
        subtractF.setOutput(subtractO);

        // NequFormula
        Function notEqualsF = ffi.createFunction();
        notEqualsF.setName(core.$not_equals);
        notEqualsF.setDescription("return boolean true if input params are not equal");
        Basic[] notEqualsFIs = new Basic[2];
        Basic notEqualsFI1 = ffi.createPrimitive();
        notEqualsFI1.setType(meta.$NUMBER);
        notEqualsFI1.setName("param1");
        Basic notEqualsFI2 = ffi.createPrimitive();
        notEqualsFI2.setName("param2");
        notEqualsFI2.setType(meta.$NUMBER);
        notEqualsFIs[0] = notEqualsFI1;
        notEqualsFIs[1] = notEqualsFI2;
        notEqualsF.setInput(notEqualsFIs);
        Basic notEqualsFO = ffi.createPrimitive();
        notEqualsFO.setType(meta.$BOOLEAN);
        notEqualsFO.setName("result");
        notEqualsFO.setDescription("true if not equals");
        notEqualsF.setOutput(notEqualsFO);

        // Periodic Formula
        Function periodicF = ffi.createFunction();
        periodicF.setName(core.$periodic);
        periodicF.setDescription("return time in millisecs of given period");
        Basic[] periodicFIs = new Basic[1];
        Basic periodicFI = ffi.createPrimitive();
        periodicFI.setName("param");
        periodicFI.setDescription("period");
        periodicFI.setType(meta.$NUMBER);
        periodicFIs[0] = periodicFI;
        periodicF.setInput(periodicFIs);
        Basic periodicFO = ffi.createPrimitive();
        periodicFO.setName("output");
        periodicFO.setType(meta.$NUMBER);
        periodicFO.setDescription("number of millisecs");
        periodicF.setOutput(periodicFO);

        // Star Formula
        Function starF = ffi.createFunction();
        starF.setName(core.$multiply);
        starF.setDescription(core.$multiply);
        Basic[] starFIs = new Basic[2];
        Basic starFI1 = ffi.createPrimitive();
        starFI1.setName("param1");
        starFI1.setType(meta.$NUMBER);
        Basic starFI2 = ffi.createPrimitive();
        starFI2.setName("param2");
        starFI2.setType(meta.$NUMBER);
        starFIs[0] = starFI1;
        starFIs[1] = starFI2;
        starF.setInput(starFIs);
        Basic starFO = ffi.createPrimitive();
        starFO.setName("result");
        starFO.setType(meta.$NUMBER);
        starF.setOutput(starFO);

        // Plus Formula
        Function plusF = ffi.createFunction();
        plusF.setName(core.$add);
        plusF.setDescription(core.$add);
        Basic[] plusFIs = new Basic[2];
        Basic plusFI1 = ffi.createPrimitive();
        plusFI1.setName("param1");
        plusFI1.setType(meta.$NUMBER);
        Basic plusFI2 = ffi.createPrimitive();
        plusFI2.setName("param2");
        plusFI2.setType(meta.$NUMBER);
        plusFIs[0] = plusFI1;
        plusFIs[1] = plusFI2;
        plusF.setInput(plusFIs);
        Basic plusFO = ffi.createPrimitive();
        plusFO.setName("result");
        plusFO.setType(meta.$NUMBER);
        plusF.setOutput(plusFO);

        // Completion Time Formula
        Function timeF = ffi.createFunction();
        timeF.setName(common.$completion_time);
        timeF.setDescription("calculate time span between two events");
        Basic[] timeFIs = new Basic[2];
        Basic timeFI1 = ffi.createEvent();
        timeFI1.setType(meta.$EVENT);
        timeFI1.setName("param1");
        timeFI1.setDescription("event start");
        Basic timeFI2 = ffi.createEvent();
        timeFI2.setType(meta.$EVENT);
        timeFI2.setName("param2");
        timeFI2.setDescription("event stop");
        timeFIs[0] = timeFI1;
        timeFIs[1] = timeFI2;
        timeF.setInput(timeFIs);
        Basic timeFO = ffi.createPrimitive();
        timeFO.setName("result");
        timeFO.setType(meta.$NUMBER);
        timeFO.setDescription("time difference");
        timeF.setOutput(timeFO);

        // Sum
        Function sumF = ffi.createFunction();
        sumF.setName(core.$sum);
        sumF.setDescription(core.$sum);
        Basic[] sumFIs = new Basic[1];
        Basic sumFI = ffi.createPrimitive();
        sumFI.setName("param");
        sumFI.setType($arrayOfNUMBER);
        sumFIs[0] = sumFI;
        Basic sumFO = ffi.createPrimitive();
        sumFO.setType(meta.$NUMBER);
        sumFO.setName("sum");
        sumF.setOutput(sumFO);

        // max
        Function maxF = ffi.createFunction();
        maxF.setName(core.$max);
        maxF.setDescription("");
        Basic[] maxFIs = new Basic[1];
        Basic maxFI1 = ffi.createPrimitive();
        maxFI1.setName("param");
        maxFI1.setType($arrayOfNUMBER);
        maxFIs[0] = maxFI1;
        maxF.setInput(maxFIs);
        Basic maxFO = ffi.createPrimitive();
        maxFO.setType(meta.$NUMBER);
        maxFO.setName("output");
        maxFO.setDescription("return maximum value witin input array");
        maxF.setOutput(maxFO);

        monitoringFeatures[0] = lessThanF;
        monitoringFeatures[1] = greaterThanF;
        monitoringFeatures[2] = availabilityF;
        monitoringFeatures[3] = equalsF;
        monitoringFeatures[4] = meanF;
        monitoringFeatures[5] = seriesF;
        monitoringFeatures[6] = andF;
        monitoringFeatures[7] = countF;
        monitoringFeatures[8] = divF;
        monitoringFeatures[9] = eventRF;
        monitoringFeatures[10] = greaterOEF;
        monitoringFeatures[11] = lessOEF;
        monitoringFeatures[12] = subtractF;
        monitoringFeatures[13] = notEqualsF;
        monitoringFeatures[14] = periodicF;
        monitoringFeatures[15] = starF;
        monitoringFeatures[16] = plusF;
        monitoringFeatures[17] = timeF;
        monitoringFeatures[18] = sumF;
        monitoringFeatures[19] = maxF;

        this.setType("REASONER");
        this.setUuid("e1998cd1-e4e7-4e5c-87db-4d065f93a632");
        this.setMonitoringFeatures(monitoringFeatures);
    }

    /**
     * This is a singleton; use this to get the instance.
     * 
     * @return FbkRCGComponentMonitoringFeatures the features that the fbkrcg can monitor
     */
    public static FbkRCGComponentMonitoringFeatures getInstance() {
        if (instance == null) {
            instance = new FbkRCGComponentMonitoringFeatures();
        }
        return instance;
    }

}
