/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.monitoring.fbk.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.log4j.Logger;
import org.slasoi.common.eventschema.AgreementTermType;
import org.slasoi.common.eventschema.AssessmentResultType;
import org.slasoi.common.eventschema.EventContextType;
import org.slasoi.common.eventschema.EventIdType;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.EventNotifier;
import org.slasoi.common.eventschema.EventPayloadType;
import org.slasoi.common.eventschema.EventSource;
import org.slasoi.common.eventschema.EventTime;
import org.slasoi.common.eventschema.GuatanteedStateType;
import org.slasoi.common.eventschema.MonitoringInfoType;
import org.slasoi.common.eventschema.MonitoringResultEventType;
import org.slasoi.common.eventschema.ObjectFactory;
import org.slasoi.common.eventschema.PropertiesType;
import org.slasoi.common.eventschema.SLAType;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.soa.monitor.runtime.core.IMonitor;
import org.soa.monitor.runtime.notification.INotificationPlugin;
import org.soa.monitor.runtime.notification.NotificationRule;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * This plugin handle the fbk monitor response and
 * forward it to the monitoring result event to the bus.
 *
 * @since 0.1
 */
public class FbkRCGNotificationPlugin implements INotificationPlugin {

    /** Logger. */
    private static PrintWriter log = null;

    /** Internal counter for the notified event. */
    private static int counter = 0;
    /** LOGGER. **/
    private static final Logger LOGGER = Logger.getLogger(FbkRCGNotificationPlugin.class);
    /** violation counter SLA1. **/
    private static int counter_SLA1 = 0;
    /** SLA1 Uuid. **/
    private static final String SLA1Uuid = "98989893010";
    /** violation counter SLA22Agreed. **/
    private static int counter_SLA2 = 0;
    /** SLA22Agreed Uuid. **/
    private static final String SLA22AgreedUuid = "98989893444";

    /**
     * Return the object instance.
     */
    public FbkRCGNotificationPlugin() {
        /* empty block */
    }

    /**
     * reset violation counters.
     */
    public static final void resetViolationCounters() {
        counter_SLA1 = 0;
        counter_SLA2 = 0;
    }
    /**
     * Take slatInfo and monitor info and create the EventInstance
     * for the monitoring result event.
     * @param monitor the monitor that create the violation
     * @param slatemplate the sla template
     * @return EventInstance the result eventinstance to be sended to the bus
     */
    private EventInstance createEventInstance(final IMonitor monitor,
            final SLATemplate slatemplate) {
        /*
         * This is the factory of the event produced
         * with jaxb starting from the xml schema
         * <Event>
         * <EventID/>
         * <EventContext/>
         * <EventPayload><MonitoringResultEvent/></EventPayload>
         * <Event>
         */
        ObjectFactory of = new ObjectFactory();
        EventInstance eventInstance = of.createEventInstance();
        String slatUid = slatemplate.getUuid().getValue();
        String monitorName = null;

        AgreementTerm att =
                foundAgreementTermTypeThatContainGuaranteeState(slatemplate,
                        monitor.getName());

        EventIdType eventId = of.createEventIdType();
        eventId.setID(counter++);
        eventInstance.setEventID(eventId);

        EventPayloadType payload = of.createEventPayloadType();

        MonitoringResultEventType evres = of.createMonitoringResultEventType();

        EventContextType eventContext = of.createEventContextType();
        EventTime eventTime = of.createEventTime();
        eventTime.setTimestamp(monitor.getTimestamp());
        eventContext.setTime(eventTime);
        EventSource source = of.createEventSource();
        eventContext.setSource(source);
        EventNotifier notifier = of.createEventNotifier();
        eventContext.setNotifier(notifier);
        eventInstance.setEventContext(eventContext);
        SLAType slaInfo = of.createSLAType();

        AgreementTermType at = of.createAgreementTermType();

        if (monitor.getName().contains("ORC")) {
            monitorName = monitor.getName();
        } else {
            monitorName = monitor.getName().substring(0, monitor.getName().lastIndexOf("_"));
        }

        eventId.setEventTypeID(monitorName);

        // fix for identifying violation in Guranteed states.
        if (matchGState(slatemplate, monitorName)
                && String.valueOf(monitor.getValue()).equalsIgnoreCase("false")) {
            //System.err.println("Sending result event with violation info about monitor term: " + monitor.getName());
            at.setAssessmentResult(AssessmentResultType.VIOLATION);
            slaInfo.setAssessmentResult(AssessmentResultType.VIOLATION);

            //violation counter increment.
            if (slatUid.equalsIgnoreCase(SLA1Uuid)) {
                counter_SLA1++;
                LOGGER.info("violation count for " + SLA1Uuid + " is " + counter_SLA1);
                ViolationCounterRepository.updateCounter(SLA1Uuid, counter_SLA1);

            } else if (slatUid.equalsIgnoreCase(SLA22AgreedUuid)) {
                counter_SLA2++;
                LOGGER.info("violation count for " + SLA22AgreedUuid + " is " + counter_SLA2);
                ViolationCounterRepository.updateCounter(SLA22AgreedUuid, counter_SLA2);
            }
        } else {
            at.setAssessmentResult(AssessmentResultType.SATISFACTION);
            slaInfo.setAssessmentResult(AssessmentResultType.SATISFACTION);
        }

        if (att != null) {
            at.setAgreementTermID(att.getId().getValue());
        }

        GuatanteedStateType ga = new GuatanteedStateType();
        ga.setQoSName(monitorName);
        ga.setQoSValue(monitor.getValue().toString());
        // add guaranteed state to agreement term
        at.getGuaranteedStateOrGuaranteedAction().add(ga);
        // add agreement term to sla
        slaInfo.setSlaUUID(slatUid);
        slaInfo.setSlaURI("http://www.slasoi.org/ORCSLAT1.xml");
        slaInfo.getAgreementTerm().add(at);

        //PropertiesType
        evres.setExtraProperties(new PropertiesType());
        //MonitoringInfoType
        evres.setMonitoringInfo(new MonitoringInfoType());
        //SLA Info
        evres.setSLAInfo(slaInfo);

        payload.setMonitoringResultEvent(evres);
        eventInstance.setEventPayload(payload);

        return eventInstance;
    }

    /**
     * Notify a monitor triggered event.
     * @param monitor the monitor
     * @param rule the notification rule
     */
    public final void notify(
            final IMonitor monitor, final NotificationRule rule) {

        //System.out.println("recieved notification from monitor");
        String slatid = null;
        /*
         * The Slat has been violated trace on file and
         * forward the monitorResultEvent to the bus
         */

        /*if (log == null) {
            try {
                log = new PrintWriter(new java.io.FileWriter(new File(rule.notificationAddress)), true);
            } catch (FileNotFoundException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }*/

        // doesnt work for multiple SLAs, fix is to take id from name for e.g overallq_<slatID>
        if (monitor.getName().contains("ORC")) {
            slatid = (String) monitor.getMonitorProperty("SLAT");
        } else {
            slatid = (String) monitor.getName().substring(monitor.getName().lastIndexOf("_") + 1);
        }

        /*
         * found in the slat repository the right slat
         * associate to the monitor
         */
        Object slat = FbkRCGSlatRepository.getSlat(slatid);

        if (slat instanceof SLATemplate) {

            SLATemplate slatemplate = (SLATemplate) slat;
            EventInstance eventInstance =
                    createEventInstance(monitor, slatemplate);
            MonitoringResutEventDeserializer ms =
                    new MonitoringResutEventDeserializer(eventInstance);
            String monitoringResultString = ms.deserialize();
//            log.print(monitoringResultString);
//            log.print("\n");
            LOGGER.info(monitoringResultString);
            /* The content inside the sw buffer must be put in the bus */
            MonitoringResultEventForwarder.publish(monitoringResultString);
//            log.flush();

        }

    }

    /**
     * Found in the sla template the agreement term
     * that contains the guarantee state called by name.
     *
     * @param slatemplate the sla template
     * @param name the name of the guarantee state violated
     * @return AgreementTerm the agreement term violated
     */
    private AgreementTerm foundAgreementTermTypeThatContainGuaranteeState(
            final SLATemplate slatemplate, final String name) {
        int lenght = slatemplate.getAgreementTerms().length;
        for (int i = 0; i < lenght; i++) {
            AgreementTerm att = slatemplate.getAgreementTerms()[i];
            int gaLenght = att.getGuarantees().length;
            for (int j = 0; j < gaLenght; j++) {
                Guaranteed gt = att.getGuarantees()[j];
                /*
                 * The monitor name is equals to
                 * the guaranteed for construction
                 */
                if (gt.getId().equals(name)) {
                    return att;
                }
            }

        }
        return null;
    }

    /**
     * check if incoming monitor name matches with sla GS.
     * @param slatemplate SLATemplate
     * @param name String
     * @return boolean
     */
    private boolean matchGState(final SLATemplate slatemplate, final String name) {
        //String gtRuntimeName = null;
        int lenght = slatemplate.getAgreementTerms().length;
        for (int i = 0; i < lenght; i++) {
            AgreementTerm att = slatemplate.getAgreementTerms()[i];
            int gaLenght = att.getGuarantees().length;
            for (int j = 0; j < gaLenght; j++) {
                Guaranteed gt = att.getGuarantees()[j];
                //gtRuntimeName = gt.getId() + "_" + slatemplate.getUuid().getValue();
                // monitor name is equal to GS.
                if (gt.getId().getValue().equals(name)) {
                    return true;
                }
            }

        }
        return false;
    }

}
