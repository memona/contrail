/**
 * SVN FILE: $Id$
 *
 * Copyright (c) 2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author$
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slasoi.monitoring.fbk.impl.FbkRCGServiceImpl;

/**
 * @author Khurshid
 * 
 */
public class FbkRCGTest {

    /** SLA@SOI platform configuration holder. **/
    private static final String SLASOI_HOME = System.getenv("SLASOI_HOME");
    /** FBK Rcg instance. **/
    private static final FbkRCGServiceImpl fbkRcg = new FbkRCGServiceImpl();

    /**
     * @throws java.lang.Exception e
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception e
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception e
     */
    @After
    public void tearDown() throws Exception {
    }

    // private void testAverageFormulaSLA() {
    //
    // System.out.println("*****************************************");
    // System.out.println("START TEST testThroughput");
    // System.out.println("*****************************************");
    // RuntimeMonitorEngine.getInstance().getInventory().cleanInternalTables();
    //
    // EventDefinition evDef = new EventDefinition();
    // evDef.setClassname("org.soa.monitor.runtime.core.EventDefinition");
    // evDef.setName("Request");
    // evDef.addEventParam("service");
    // evDef.addEventParam("provider");
    // evDef.addEventParam("customer");
    // RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
    // evDef);
    //
    // MonitorDefinition monDef = null;
    // try {
    // monDef = (MonitorDefinition) Class
    // .forName(
    // "monitor.instance.ORCThroughputConstraint_ORCProvider_ORCCustomerMonitorDefinition")
    // .newInstance();
    // } catch (InstantiationException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // } catch (IllegalAccessException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // } catch (ClassNotFoundException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // // monDef = new
    // // ORCThroughputConstraint_ORCProvider_ORCCustomerMonitorDefinition();
    //
    // String service = "PaymentService";
    // String provider = "ORCProvider";
    // String customer = "ORCCustomer";
    //
    // RuntimeMonitorEngine.getInstance().getInventory().addMonitorDefinition(
    // monDef);
    //
    // int number_of_dispatched_events = 1;
    // long mill = 0;
    // /* if you put mill_increment 11 with slat frequency>100 will fail */
    // long mill_increment = 10;
    // for (int i = 0; i < number_of_dispatched_events; i++) {
    //
    // // prepare event to send
    // MonitorEvent event = new MonitorEvent("Request");
    // event.addParameter("service", service);
    // event.addParameter("provider", provider);
    // event.addParameter("customer", customer);
    // mill = mill + mill_increment;
    // event.setTimestamp(mill);
    //
    // RuntimeMonitorEngine.getInstance().dispatchMessage(event);
    // }
    // }

    // private void testAverageFormulaSLAT() {
    //
    // System.out.println("*****************************************");
    // System.out.println("START TEST testThroughput");
    // System.out.println("*****************************************");
    // RuntimeMonitorEngine.getInstance().getInventory().cleanInternalTables();
    //
    // EventDefinition evDef = new EventDefinition();
    // evDef.setClassname("org.soa.monitor.runtime.core.EventDefinition");
    // evDef.setName("Request");
    // evDef.addEventParam("service");
    // RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(
    // evDef);
    //
    // MonitorDefinition monDef = null;
    // try {
    // monDef = (MonitorDefinition) Class
    // .forName(
    // "monitor.instance.ORCThroughputConstraintMonitorDefinition")
    // .newInstance();
    // } catch (InstantiationException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // } catch (IllegalAccessException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // } catch (ClassNotFoundException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // // monDef = new
    // // ORCThroughputConstraint_ORCProvider_ORCCustomerMonitorDefinition();
    //
    // String service = "PaymentService";
    //
    // RuntimeMonitorEngine.getInstance().getInventory().addMonitorDefinition(
    // monDef);
    //
    // int number_of_dispatched_events = 1;
    // long mill = 0;
    // /* if you put mill_increment 11 with slat frequency>100 will fail */
    // long mill_increment = 10;
    // for (int i = 0; i < number_of_dispatched_events; i++) {
    //
    // // prepare event to send
    // MonitorEvent event = new MonitorEvent("Request");
    // event.addParameter("service", service);
    // mill = mill + mill_increment;
    // event.setTimestamp(mill);
    //
    // RuntimeMonitorEngine.getInstance().dispatchMessage(event);
    // }
    // }

    //
    /* Dispatch the events to the low level fbk monitoring runtime */
    /*
     * private void testAvailabilityFormula() {
     * 
     * // Please use the eventGen-AvailabilityEventGen to send events
     * 
     * dispatchAvailabilytEvents();
     * 
     * }
     */

    /* This create the event for the fbk monitor runtime */
    /*
     * private void dispatchAvailabilytEvents() { int number_of_dispatched_events = 10; for (int i = 0; i <
     * number_of_dispatched_events; i++) { AvailabilityDispatcher.get_instance().openTask(i + "", i); if (i != 8) {
     * AvailabilityDispatcher.get_instance().closeTask(i + "", i); } try { Thread.sleep(1000); } catch
     * (InterruptedException e) { // TODO Auto-generated catch block e.printStackTrace(); } }
     * 
     * }
     */

    /*
     * private void testVmCountFormula() {
     * 
     * dispatchRequesttEvents();
     * 
     * }
     */

    /*
     * private void dispatchRequesttEvents() { String service = "VM_X";
     * 
     * // prepare event to send MonitorEvent event = new MonitorEvent("Request"); event.addParameter("service",
     * service); event.setTimestamp(0);
     * 
     * MonitorEvent event2 = new MonitorEvent("Request"); event2.addParameter("service", service);
     * event2.setTimestamp(10);
     * 
     * RuntimeMonitorEngine.getInstance().dispatchMessage(event);
     * RuntimeMonitorEngine.getInstance().dispatchMessage(event2); }
     */

    /**
     * test the A4 test case.
     */
    /*@Test
    public void testA4() {
        /*
         * System.out.println("*****************************************"); System.out.println("START TEST testA4");
         * System.out.println("*****************************************"); FbkRCGServiceImpl fbkRcg = new
         * FbkRCGServiceImpl(); String slatFileName = "A4_SLATemplate.xml"; try { InputStream is = new
         * FileInputStream(System.getenv("SLASOI_HOME") + System.getProperty("file.separator") + "fbkrcg" +
         * System.getProperty("file.separator") + "test" + System.getProperty("file.separator") + slatFileName); String
         * configuration_id = "gggg:ggggg:gggg:gggg"; fbkRcg.startMonitoring(configuration_id);
         * fbkRcg.addSLATToConfiguration(is);
         * 
         * // Removed the generation even this will be done by other bundles
         * 
         * //testAvailabilityFormula(); // Thread.sleep(4000);
         * 
         * // Start the vm count test // testVmCountFormula();
         * 
         * 
         * // This wait a while and then terminate Thread.sleep(114000);
         * 
         * fbkRcg.stopMonitoring(configuration_id); } catch (Exception e) { e.printStackTrace(); }


    }*/

    /**
     * test for monitoring single SLA.
     */
    @Test
    public final void testMonitoringSingleSLA() {
        try {
            System.out.println("**********************************************");
            System.out.println("START TEST testMonitoringSingleSLA");
            System.out.println("**********************************************");

            fbkRcg.cleanDeployDir();
            InputStream is =
                    new FileInputStream(SLASOI_HOME + System.getProperty("file.separator") + "fbkrcg"
                            + System.getProperty("file.separator") + "test" + System.getProperty("file.separator")
                            + "testSLA1.xml");

            String configuration_id1 = "gggg:ggggg:gggg:gggg";
            fbkRcg.startMonitoring(configuration_id1);
            fbkRcg.addSLATToConfiguration(is);

            org.soa.monitor.runtime.core.EventDefinition evDef = new org.soa.monitor.runtime.core.EventDefinition();
            evDef.setClassname("org.soa.monitor.runtime.core.EventDefinition");
            evDef.setName("ServiceOperationCallEndEvent");
            evDef.addEventParam("name");
            evDef.addEventParam("event_id");
            evDef.addEventParam("process_id");
            evDef.addEventParam("operation_name");
            evDef.addEventParam("MedicalTreatmentQuality");
            evDef.addEventParam("MobilityQuality");
            evDef.addEventParam("CallCenterQuality");
            evDef.addEventParam("OverallQuality");
            evDef.addEventParam("returnValue");
            evDef.addEventParam("test");

            org.soa.monitor.runtime.rtm.RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(evDef);

            // prepare event to send
            org.soa.monitor.runtime.core.MonitorEvent event1 =
                    new org.soa.monitor.runtime.core.MonitorEvent("ServiceOperationCallEndEvent");
            event1.getParameters().put("name", "ServiceOperationCallEndEvent");
            event1.getParameters().put("event_id", new Integer(1));
            event1.getParameters().put("process_id", new Integer(1));
            event1.getParameters().put("operation_name", "getSatisfactionLevels");
            event1.getParameters().put("MedicalTreatmentQuality", new Integer(1));
            event1.getParameters().put("MobilityQuality", new Integer(1));
            event1.getParameters().put("CallCenterQuality", new Integer(1));
            event1.getParameters().put("OverallQuality", new Integer(1));
            event1.setTimestamp((long) 500);

            org.soa.monitor.runtime.rtm.RuntimeMonitorEngine.getInstance().dispatchMessage(event1);

            String Uuid1 = "98989893947";
            System.out.println("ccq" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "ccq"));
            System.out.println("mtq" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "mtq"));
            System.out.println("overallq" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "overallq"));
            System.out.println("AVERAGE_SATISFACTION_LEVEL" + "_" + Uuid1 + "="
                    + fbkRcg.reportMonitoredTermValue(Uuid1, "AVERAGE_SATISFACTION_LEVEL"));
            System.out.println("G1" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "G1"));

            Thread.sleep(5000);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Monitoring Terms reporting API test.
     * @throws FileNotFoundException
     **/
    @Test
    public final void testMonitorMultipleSLAWithSameKPIs() {
        try {
            System.out.println("**********************************************");
            System.out.println("START TEST testMonitorMultipleSLAWithSameKPIs");
            System.out.println("**********************************************");

            InputStream is = new FileInputStream(SLASOI_HOME + System.getProperty("file.separator") + "fbkrcg"
                        + System.getProperty("file.separator") + "test" + System.getProperty("file.separator")
                        + "testSLA2.xml");

            fbkRcg.addSLATToConfiguration(is);

            //Thread.sleep(15000);

            org.soa.monitor.runtime.core.EventDefinition evDef = new org.soa.monitor.runtime.core.EventDefinition();
            evDef.setClassname("org.soa.monitor.runtime.core.EventDefinition");
            evDef.setName("ServiceOperationCallEndEvent");
            evDef.addEventParam("name");
            evDef.addEventParam("event_id");
            evDef.addEventParam("process_id");
            evDef.addEventParam("operation_name");
            evDef.addEventParam("MedicalTreatmentQuality");
            evDef.addEventParam("MobilityQuality");
            evDef.addEventParam("CallCenterQuality");
            evDef.addEventParam("OverallQuality");
            evDef.addEventParam("returnValue");
            evDef.addEventParam("test");

            org.soa.monitor.runtime.rtm.RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(evDef);

            // prepare event to send
            org.soa.monitor.runtime.core.MonitorEvent event1 =
                    new org.soa.monitor.runtime.core.MonitorEvent("ServiceOperationCallEndEvent");
            event1.getParameters().put("name", "ServiceOperationCallEndEvent");
            event1.getParameters().put("event_id", new Integer(1));
            event1.getParameters().put("process_id", new Integer(1));
            event1.getParameters().put("operation_name", "retrieveSatisfactionLevels"); //retrieveSatisfactionLevels
            event1.getParameters().put("MedicalTreatmentQuality", new Integer(1));
            event1.getParameters().put("MobilityQuality", new Integer(1));
            event1.getParameters().put("CallCenterQuality", new Integer(1));
            event1.getParameters().put("OverallQuality", new Integer(1));
            event1.setTimestamp((long) 500);

            org.soa.monitor.runtime.core.MonitorEvent event2 =
                new org.soa.monitor.runtime.core.MonitorEvent("ServiceOperationCallEndEvent");
            event2.getParameters().put("name", "ServiceOperationCallEndEvent");
            event2.getParameters().put("event_id", new Integer(1));
            event2.getParameters().put("process_id", new Integer(1));
            event2.getParameters().put("operation_name", "getSatisfactionLevels"); //getSatisfactionLevels--> SLA1
            event2.getParameters().put("MedicalTreatmentQuality", new Integer(500));
            event2.getParameters().put("MobilityQuality", new Integer(500));
            event2.getParameters().put("CallCenterQuality", new Integer(500));
            event2.getParameters().put("OverallQuality", new Integer(500));
            event2.setTimestamp((long) 500);

            org.soa.monitor.runtime.rtm.RuntimeMonitorEngine.getInstance().dispatchMessage(event1);
            //org.soa.monitor.runtime.rtm.RuntimeMonitorEngine.getInstance().dispatchMessage(event2);


            String Uuid1 = "98989893947";
            System.out.println("ccq" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "ccq"));
            System.out.println("mtq" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "mtq"));
            System.out.println("overallq" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "overallq"));
            System.out.println("AVERAGE_SATISFACTION_LEVEL" + "_" + Uuid1
                    + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "AVERAGE_SATISFACTION_LEVEL"));
            System.out.println("G1" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "G1"));

            Assert.assertEquals("1.0", String.valueOf(fbkRcg.reportMonitoredTermValue(Uuid1, "ccq")));
            Assert.assertEquals("1.0", String.valueOf(fbkRcg.reportMonitoredTermValue(Uuid1, "mtq")));
            Assert.assertEquals("1.0", String.valueOf(fbkRcg.reportMonitoredTermValue(Uuid1, "overallq")));
            Assert.assertEquals("1.0", String.valueOf(fbkRcg.reportMonitoredTermValue(
                    Uuid1, "AVERAGE_SATISFACTION_LEVEL")));
            Assert.assertEquals("false", String.valueOf(fbkRcg.reportMonitoredTermValue(Uuid1, "G1")));

            // test for loaded monitor
            Assert.assertEquals(6, fbkRcg.reportAllMonitoredTermsValues(Uuid1).size());

            // Monitor second SLA
            String Uuid2 = "98989893948";
            System.out.println("ccq" + "_" + Uuid2 + "=" + fbkRcg.reportMonitoredTermValue(Uuid2, "ccq"));
            System.out.println("mtq" + "_" + Uuid2 + "=" + fbkRcg.reportMonitoredTermValue(Uuid2, "mtq"));
            System.out.println("overallq" + "_" + Uuid2 + "=" + fbkRcg.reportMonitoredTermValue(Uuid2, "overallq"));
            System.out.println("AVERAGE_SATISFACTION_LEVEL" + "_"
                    + Uuid2 + "=" + fbkRcg.reportMonitoredTermValue(Uuid2, "AVERAGE_SATISFACTION_LEVEL"));
            System.out.println("G1" + "_" + Uuid2 + "=" + fbkRcg.reportMonitoredTermValue(Uuid2, "G1"));

            Assert.assertEquals("1.0", String.valueOf(fbkRcg.reportMonitoredTermValue(Uuid2, "ccq")));
            Assert.assertEquals("1.0", String.valueOf(fbkRcg.reportMonitoredTermValue(Uuid2, "mtq")));
            Assert.assertEquals("1.0", String.valueOf(fbkRcg.reportMonitoredTermValue(Uuid2, "overallq")));
            Assert.assertEquals("1.0", String.valueOf(fbkRcg.reportMonitoredTermValue(
                    Uuid2, "AVERAGE_SATISFACTION_LEVEL")));
            Assert.assertEquals("false", String.valueOf(fbkRcg.reportMonitoredTermValue(Uuid2, "G1")));

            // test for loaded monitor
            Assert.assertEquals(6, fbkRcg.reportAllMonitoredTermsValues(Uuid2).size());

            Thread.sleep(20000);
            org.soa.monitor.runtime.rtm.RuntimeMonitorEngine.getInstance().dispatchMessage(event2);
            Thread.sleep(20000);
            // take values after updates 250.5(ccq,mtq,overallq,mobq) 250(average satisfaction level)
            Uuid2 = "98989893947";
            System.out.println("ccq" + "_" + Uuid2 + "=" + fbkRcg.reportMonitoredTermValue(Uuid2, "ccq"));
            System.out.println("mtq" + "_" + Uuid2 + "=" + fbkRcg.reportMonitoredTermValue(Uuid2, "mtq"));
            System.out.println("overallq" + "_" + Uuid2 + "=" + fbkRcg.reportMonitoredTermValue(Uuid2, "overallq"));
            System.out.println("AVERAGE_SATISFACTION_LEVEL" + "_"
                    + Uuid2 + "=" + fbkRcg.reportMonitoredTermValue(Uuid2, "AVERAGE_SATISFACTION_LEVEL"));
            System.out.println("G1" + "_" + Uuid2 + "=" + fbkRcg.reportMonitoredTermValue(Uuid2, "G1"));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * refresh monitor engine test.
     * @throws FileNotFoundException e
     * @throws InterruptedException i
     * @throws ParseException  p
     **/
    @Test
    public final void testReplaceSLA() throws FileNotFoundException, InterruptedException, ParseException {

        System.out.println("*****************************************");
        System.out.println("START TEST testReplaceSLA");
        System.out.println("*****************************************");

        // read renegotiated SLA'
        InputStream is =
                new FileInputStream(SLASOI_HOME + System.getProperty("file.separator") + "fbkrcg"
                        + System.getProperty("file.separator") + "test" + System.getProperty("file.separator")
                        + "testSLA3.xml");

        // Refresh Steps
        //fbkRcg.generateReport();
        fbkRcg.replaceSLA(is);

        String Uuid = "98989893947";
        // test for empty monitor inventory for particular SLA.

        org.soa.monitor.runtime.core.EventDefinition evDef = new org.soa.monitor.runtime.core.EventDefinition();
        evDef.setClassname("org.soa.monitor.runtime.core.EventDefinition");
        evDef.setName("ServiceOperationCallEndEvent");
        evDef.addEventParam("name");
        evDef.addEventParam("event_id");
        evDef.addEventParam("process_id");
        evDef.addEventParam("operation_name");
        evDef.addEventParam("MedicalTreatmentQuality");
        evDef.addEventParam("MobilityQuality");
        evDef.addEventParam("CallCenterQuality");
        evDef.addEventParam("OverallQuality");
        evDef.addEventParam("returnValue");
        evDef.addEventParam("test");

        org.soa.monitor.runtime.rtm.RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(evDef);

        // prepare event to send
        org.soa.monitor.runtime.core.MonitorEvent event1 =
                new org.soa.monitor.runtime.core.MonitorEvent("ServiceOperationCallEndEvent");
        event1.getParameters().put("name", "ServiceOperationCallEndEvent");
        event1.getParameters().put("event_id", new Integer(1));
        event1.getParameters().put("process_id", new Integer(1));
        event1.getParameters().put("operation_name", "getSatisfactionLevels");
        event1.getParameters().put("MedicalTreatmentQuality", new Integer(5));
        event1.getParameters().put("MobilityQuality", new Integer(5));
        event1.getParameters().put("CallCenterQuality", new Integer(5));
        event1.getParameters().put("OverallQuality", new Integer(5));
        event1.setTimestamp((long) 500);

        org.soa.monitor.runtime.rtm.RuntimeMonitorEngine.getInstance().dispatchMessage(event1);

        System.out.println("ccq" + "_" + Uuid + "=" + fbkRcg.reportMonitoredTermValue(Uuid, "ccq"));
        System.out.println("mtq" + "_" + Uuid + "=" + fbkRcg.reportMonitoredTermValue(Uuid, "mtq"));
        System.out.println("overallq" + "_" + Uuid + "=" + fbkRcg.reportMonitoredTermValue(Uuid, "overallq"));
        System.out.println("AVERAGE_SATISFACTION_LEVEL" + "_"
                + Uuid + "=" + fbkRcg.reportMonitoredTermValue(Uuid, "AVERAGE_SATISFACTION_LEVEL"));
        System.out.println("G1" + "_" + Uuid + "=" + fbkRcg.reportMonitoredTermValue(Uuid, "G1"));

        // test for refresh/loaded monitor inventory
        Assert.assertEquals(6, fbkRcg.reportAllMonitoredTermsValues(Uuid).size());
        System.out.println("Number of monitors for SLA(" + Uuid
                + ") is " + fbkRcg.reportAllMonitoredTermsValues(Uuid).size());

    }

    /**
     * SLA Monitoring test.
     * @throws FileNotFoundException f
     * @throws InterruptedException i
     * @throws ParseException p
     */
    @Test
    public final void testReportLastSLAMonitoringTerms()
    throws FileNotFoundException, InterruptedException, ParseException {

      System.out.println("**********************************************");
      System.out.println("START TEST testReportLastSLAMonitoringTerms");
      System.out.println("**********************************************");

      String Uuid = "98989893947";
      System.out.println("Pevious value G1" + "_" + Uuid + "=" + fbkRcg.reportLastMonitoredTermValue(Uuid, "G1"));
      System.out.println("Pevious value ccq" + "_" + Uuid + "=" + fbkRcg.reportLastMonitoredTermValue(Uuid, "ccq"));
      System.out.println("Pevious value mtq" + "_" + Uuid + "=" + fbkRcg.reportLastMonitoredTermValue(Uuid, "mtq"));
      System.out.println("Pevious value overallq" + "_" + Uuid + "="
              + fbkRcg.reportLastMonitoredTermValue(Uuid, "overallq"));
      System.out.println("Pevious value AVERAGE_SATISFACTION_LEVEL" + "_"
              + Uuid + "=" + fbkRcg.reportLastMonitoredTermValue(Uuid, "AVERAGE_SATISFACTION_LEVEL"));

      // check for old values
      Assert.assertEquals("250.5", String.valueOf(fbkRcg.reportLastMonitoredTermValue(Uuid, "ccq")));
      Assert.assertEquals("250.5", String.valueOf(fbkRcg.reportLastMonitoredTermValue(Uuid, "mtq")));
      Assert.assertEquals("250.5", String.valueOf(fbkRcg.reportLastMonitoredTermValue(Uuid, "overallq")));
      Assert.assertEquals("250.5", String.valueOf(fbkRcg.reportLastMonitoredTermValue(Uuid, "mobq")));
      Assert.assertEquals("250.5", String.valueOf(fbkRcg.reportLastMonitoredTermValue(
              Uuid, "AVERAGE_SATISFACTION_LEVEL")));
      Assert.assertEquals("true", String.valueOf(fbkRcg.reportLastMonitoredTermValue(Uuid, "G1")));

      System.out.println("Pevious value G1" + "_" + Uuid + "=" + fbkRcg.reportLastMonitoredTermValue(Uuid, "G1"));
      System.out.println("Pevious Monitors set" + fbkRcg.reportAllLastMonitoredTermsValues(Uuid));

      //retrieve threshold value for monitorTerm
      System.out.println("G1 is " + fbkRcg.getMonitoringTermThreshold(Uuid, "G1"));
      System.out.println("G2 is " + fbkRcg.getMonitoringTermThreshold(Uuid, "G2"));
      System.out.println("G3 is " + fbkRcg.getMonitoringTermThreshold(Uuid, "G3"));
      System.out.println("PERCENT_LOST_CALLS is " + fbkRcg.getMonitoringTermThreshold(Uuid, "PERCENT_LOST_CALLS"));
      //fbkRcg.generateReport();
      }
    /**
     * SLA Monitoring test.
     * @throws FileNotFoundException f
     * @throws InterruptedException i
     * @throws ParseException p
     */
    /*@Test
    public final void testReportGeneration()
    throws FileNotFoundException, InterruptedException, ParseException {

      System.out.println("**********************************************");
      System.out.println("START TEST testReportGeneration");
      System.out.println("**********************************************");
      fbkRcg.cleanDeployDir();
      InputStream is =
              new FileInputStream(SLASOI_HOME + System.getProperty("file.separator") + "fbkrcg"
                      + System.getProperty("file.separator") + "test" + System.getProperty("file.separator")
                      + "testSLA1.xml");

      String configuration_id1 = "gggg:ggggg:gggg:gggg";
      fbkRcg.startMonitoring(configuration_id1);
      fbkRcg.addSLAToConfiguration(is);

      org.soa.monitor.runtime.core.EventDefinition evDef = new org.soa.monitor.runtime.core.EventDefinition();
      evDef.setClassname("org.soa.monitor.runtime.core.EventDefinition");
      evDef.setName("ServiceOperationCallStartEvent");
      evDef.addEventParam("name");
      evDef.addEventParam("event_id");
      evDef.addEventParam("process_id");
      evDef.addEventParam("operation_name");
      evDef.addEventParam("MedicalTreatmentQuality");
      evDef.addEventParam("MobilityQuality");
      evDef.addEventParam("CallCenterQuality");
      evDef.addEventParam("OverallQuality");
      evDef.addEventParam("returnValue");
      evDef.addEventParam("test");

      org.soa.monitor.runtime.rtm.RuntimeMonitorEngine.getInstance().getInventory().addEventDefinition(evDef);

      // prepare event to send
      org.soa.monitor.runtime.core.MonitorEvent event1 =
              new org.soa.monitor.runtime.core.MonitorEvent("ServiceOperationCallStartEvent");
      event1.getParameters().put("name", "ServiceOperationCallStartEvent");
      event1.getParameters().put("event_id", new Integer(1));
      event1.getParameters().put("process_id", new Integer(1));
      event1.getParameters().put("operation_name", "getSatisfactionLevels");
      event1.getParameters().put("MedicalTreatmentQuality", new Integer(1));
      event1.getParameters().put("MobilityQuality", new Integer(1));
      event1.getParameters().put("CallCenterQuality", new Integer(1));
      event1.getParameters().put("OverallQuality", new Integer(1));
      event1.setTimestamp((long) 500);

      org.soa.monitor.runtime.rtm.RuntimeMonitorEngine.getInstance().dispatchMessage(event1);

      String Uuid1 = "98989893947";
      System.out.println("ccq" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "ccq"));
      System.out.println("mtq" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "mtq"));
      System.out.println("overallq" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "overallq"));
      System.out.println("AVERAGE_SATISFACTION_LEVEL" + "_" + Uuid1 + "="
              + fbkRcg.reportMonitoredTermValue(Uuid1, "AVERAGE_SATISFACTION_LEVEL"));
      System.out.println("G1" + "_" + Uuid1 + "=" + fbkRcg.reportMonitoredTermValue(Uuid1, "G1"));
      Thread.sleep(5000);
      fbkRcg.generateReport(Uuid1);
      }*/
}
