package org.slasoi.isslam.poc.test.planhandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.slasoi.gslam.commons.plan.Plan;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;
import org.slasoi.gslam.pac.ProvisioningAndAdjustment;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.ism.occi.IsmOcciServiceImpl;
import org.slasoi.isslam.pac.InfrastructureProvisioningAdjustment;
import org.slasoi.isslam.poc.planhandler.impl.PlanHandlerImpl;
import org.slasoi.isslam.poc.servicesmanager.impl.ServiceManagerHandlerImpl;
import org.slasoi.slamodel.sla.SLA;
import junit.framework.TestCase;

/**
 * This class is for testing the functionalities of plan handler.
 * 
 * @author Kuan Lu
 * 
 */
public class PlanHandlerTest extends TestCase {
    private static final Logger LOGGER = Logger.getLogger(PlanHandlerTest.class);

    /**
     * Tests the plan maker.
     */
    public void testPlanMaker() {
        //ServiceManagerHandlerImpl.getInstance().setInfraServiceManager(new IsmOcciServiceImpl());
        FileReader read;
        try {
            read =
                    new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                            + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                            + "A4_SLA-2-VM-Correct.xml");

            BufferedReader br = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String row;
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }

            SLASOIParser slasoiParser = new SLASOIParser();
            SLA sla;
            sla = slasoiParser.parseSLA(sb.toString());
            assertNotNull(sla);
            PlanHandlerImpl plan = new PlanHandlerImpl(sla);
            LOGGER.info("Testing...Executing plan with only one node...");
            //InfrastructureProvisioningAdjustment pac = new InfrastructureProvisioningAdjustment();
            Plan p = plan.planMaker();
            assertNotNull(p);
            //pac.executePlan(p);
        }
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
