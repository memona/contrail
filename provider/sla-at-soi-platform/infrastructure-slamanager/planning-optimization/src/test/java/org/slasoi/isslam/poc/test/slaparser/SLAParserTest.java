package org.slasoi.isslam.poc.test.slaparser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.slasoi.gslam.syntaxconverter.SLASOITemplateParser;
import org.slasoi.isslam.poc.slaparser.SLAParser;
import org.slasoi.slamodel.sla.SLATemplate;

import junit.framework.TestCase;

/**
 * This class is for testing the functionalities of SLA parser.
 * 
 * @author Kuan Lu
 * 
 */
public class SLAParserTest extends TestCase {
    private static final Logger LOGGER = Logger.getLogger(SLAParserTest.class);

    /**
     * Tests the SLA parser.
     */
    public void testSLAParser() {
        try {
            FileReader read;

            read =
                    new FileReader(System.getenv("SLASOI_HOME") + File.separator + "infrastructure-slamanager"
                            + File.separator + "planning-optimization" + File.separator + "A4" + File.separator
                            +"A4_SLAT_24.05.2011"+File.separator+ "A4_SLAT_FIXED_VM.xml");

            BufferedReader br = new BufferedReader(read);
            StringBuffer sb = new StringBuffer();
            String row;
            while ((row = br.readLine()) != null) {
                sb.append(row);
            }

            SLASOITemplateParser slasoitParser = new SLASOITemplateParser();
            SLATemplate slat;

            slat = slasoitParser.parseTemplate(sb.toString());
            assertNotNull(slat);
            LOGGER.info("Start parsing SLA template");
            SLAParser parse = new SLAParser(slat);
            LOGGER.info("Parsing finished");
            assertNotNull(parse.getResourceRequest());
        }
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
