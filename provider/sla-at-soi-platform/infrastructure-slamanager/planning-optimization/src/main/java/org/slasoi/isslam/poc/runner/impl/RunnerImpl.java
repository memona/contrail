package org.slasoi.isslam.poc.runner.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.isslam.poc.resourceplanneroptimizer.exceptions.RequestNotCorrectException;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.CPU;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Memory;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Request;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.RequestProcessor;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Resource;
import org.slasoi.isslam.poc.runner.Runner;
import org.slasoi.isslam.poc.slaparser.SLAParser;
import org.slasoi.isslam.poc.utils.Constant;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * The implementation of Runner interface.
 * 
 * @see org.slasoi.isslam.poc.runner.Runner
 * @author Kuan Lu
 * @version 1.0-SNAPSHOT
 */
public class RunnerImpl implements Runner {
    private SLATemplate slat;
    private static final Logger LOGGER = Logger.getLogger(RunnerImpl.class);
    public static SLAManagerContext context;

    public RunnerImpl(SLATemplate sla, SLAManagerContext context) {
        // TODO: pass the real sla to this variable
        this.slat = sla;
        RunnerImpl.context = context;
    }

    /**
     * Starts to run.
     */
    public SLATemplate run() {
        // parser SLA;
        SLAParser parser = new SLAParser(this.slat);
        // BDD construction;
        //LOGGER.info("=============Starting to generate the BDD structure with requests list...===================");
        //BDDRunnerImpl bddRunner = new BDDRunnerImpl(parser.getResourceRequest());
        //bddRunner.transfer();
        //LOGGER.info("=============Generated the BDD structure with requests list=================");
        // all the BDDs that contains requests are located there.
        // BDD[] VariableBDD = bddRunner.getVariableBDD();
        //LOGGER.info("=============Starting to generate all the paths that might to lead to a successful deal=================");
        //ArrayList<LinkedHashMap<String, Request>> paths = this.bddPaths(bddRunner);
        // This is a temporal way for doing this
        ArrayList<LinkedHashMap<String, Request>> paths = new ArrayList<LinkedHashMap<String, Request>>();
        paths.add(parser.getResourceRequest());
        LOGGER.info("=============Generated all the paths that might to lead to a successful deal, the number of the paths is : "+paths.size()+"=================");
        // plan + query ISM + reservation
        RequestProcessor mainProvider = new RequestProcessor(Constant.Main_Provider, Constant.Main_FailureRate);
        LOGGER.info("Starting to analyze each path...");
        // for (Request request : parser.getResourceRequest().values()) {
        try {
            for (LinkedHashMap<String, Request> path : paths) {
                mainProvider.startProcess(path.values());
                if (mainProvider.isSlaChanged()) {
                    for (Request request : path.values()) {
                        int cpuNr;
                        int memoryNr;
                        // we don't have enough resource
                        for (Resource resource : request.getResource().values()) {
                            if (resource instanceof CPU) {
                                cpuNr = resource.getAmount();
                                CONST cpu = request.getCpuNr();
                                if (cpu != null) {
                                    cpu.setValue(String.valueOf(cpuNr));
                                }
                                else {
                                    LOGGER.error("Unable to modify the SLA Template---the number of CPU.");
                                }
                            }
                            else if (resource instanceof Memory) {
                                memoryNr = resource.getAmount() * 128;
                                CONST memory = request.getMemoryNr();
                                if (memory != null) {
                                    memory.setValue(String.valueOf(memoryNr));
                                }
                                else {
                                    LOGGER.error("Unable to modify the SLA Template---the number of Memory.");
                                }
                            }
                            else
                                continue;
                        }
                    }

                }
            }

        }
        catch (RequestNotCorrectException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // }
        // send feedback to gslam
        return this.slat;
    }

    public boolean getPlanOptimization() {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean getServiceEvaluation() {
        // TODO Auto-generated method stub
        return false;
    }

    /*public ArrayList<LinkedHashMap<String, Request>> bddPaths(BDDRunnerImpl bdd) {
        // <0:0, 1:0, 2:1><0:0, 1:1><0:1>
        ArrayList<LinkedHashMap<String, Request>> list = new ArrayList<LinkedHashMap<String, Request>>();
        String[] paths = bdd.getPathResults().split(">");
        BDD[] bddVariables = bdd.getVariableBDD();
        for (int i = 0; i < paths.length; i++) {
            paths[i] = paths[i].substring(1);
            String[] subPaths = paths[i].split(",");
            LinkedHashMap<String, Request> subList = new LinkedHashMap<String, Request>();
            for (int o = 0; o < subPaths.length; o++) {
                String[] subsubPaths = subPaths[o].split(":");
                // subsubPaths[0] : requestNr, subsubPaths[1] : true (1) false (0)
                if (subsubPaths[1].equals("0")) {
                    continue;
                }
                else if (subsubPaths[1].equals("1")) {
                    BDD bddValue = bddVariables[Integer.parseInt(subsubPaths[0].trim())];
                    subList.put(bddValue.getName(), bddValue.getValue());
                }
            }
            list.add(subList);
        }
        return list;
    }*/
}
