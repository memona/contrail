package org.slasoi.isslam.poc.monitoringconfigserialization;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.apache.log4j.Logger;

public class Serialization {

    private static Logger log = Logger.getLogger(Serialization.class);

    private static Serialization instance;

    public static Serialization getInstance() {
        if (instance == null) {
            instance = new Serialization();
        }
        return instance;
    }

    public String serialize(MonitoringSystemConfiguration cmf) {
        try {
            XStream xstream = new XStream(new DomDriver());
            xstream.setClassLoader(MonitoringSystemConfiguration.class.getClassLoader());

            xstream.alias(org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration.class
                    .getSimpleName(), org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration.class);

            String cmfXml = xstream.toXML(cmf);
            return cmfXml;
        }
        catch (Exception e) {
            e.printStackTrace();
            log.error("MonitoringSystemConfiguration can not be serialized to be XML String.");
            return null;
        }

    }
}
