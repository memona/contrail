/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Howard Foster - Howard.Foster.1@soi.city.ac.uk, George Spanoudakis G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev: 920 $
 * @lastrevision   $Date: 2011-03-10 17:49:37 +0100 (Do, 10 Mrz 2011) $
 * @filesource
 * $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/generic-slamanager/
 * monitoring-manager/src/test/java/org/slasoi/gslam/monitoring/manager/demos/features/
 * EverestTestMonitoringFeatures.java $
 */

package org.slasoi.isslam.poc.servicesmanager.impl;
/**
 * @author SLA@SOI (City)
 * @date May 12, 2010
 * 
 * Flags: SLASOI checkstyle: YES
 */

import java.util.Iterator;

import java.util.LinkedList;

import org.slasoi.monitoring.common.features.Basic;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.Event;
import org.slasoi.monitoring.common.features.Function;
import org.slasoi.monitoring.common.features.MonitoringFeature;
import org.slasoi.monitoring.common.features.Primitive;
import org.slasoi.monitoring.common.features.impl.FeaturesFactoryImpl;
import org.slasoi.slamodel.vocab.common;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.meta;
import org.slasoi.slamodel.vocab.units;

/**
 * SLA Component Monitoring Features Test for example Everest Test features.
 **/

public class EverestTestMonitoringFeatures {

    /***
     * A local instance of a FeaturesFactoryImpl.
     **/
    private static FeaturesFactoryImpl ffi = new FeaturesFactoryImpl();

    /** Standards Events. **/
    private static Event evtRequest = null;
    /** Standards Events. **/
    private static Event evtResponse = null;

    /***
     * Extension mapping for arrayOfNUMBER.
     **/
    private static String strArrayOfNumber = "http://www.slaatsoi.org/types#array_of_NUMBER";
    /***
     * Extension mapping for arrayOfBOOLEAN.
     **/
    private static String strArrayOfBOOLEAN = "http://www.slaatsoi.org/types#array_of_BOOLEAN";
    /***
     * Extension mapping for arrayOfTEXT.
     **/
    private static String strArrayOfTEXT = "http://www.slaatsoi.org/types#array_of_TEXT";

    /**
     * Constructor.
     * 
     */
    public EverestTestMonitoringFeatures() {
        evtRequest = ffi.createEvent();
        evtResponse = ffi.createEvent();
        evtRequest.setType("REQUEST");
        evtRequest.setName("REQUEST");
        evtResponse.setType("RESPONSE");
        evtResponse.setName("RESPONSE");
    }

    /**
     * Provides an arrayOfNUMBER SLA datatype.
     * 
     * @return String
     */
    public final String arrayOfNUMBER() {
        return strArrayOfNumber;
    }

    /**
     * Constructs a set of ComponentMonitoringFeatures.
     * 
     * @return org.slasoi.monitoring.common.features.ComponentMonitoringFeatures[]
     * 
     @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
    public final ComponentMonitoringFeatures[] buildTest() {

        LinkedList<ComponentMonitoringFeatures> featurestore = new LinkedList<ComponentMonitoringFeatures>();
        featurestore.add(ffi.createComponentMonitoringFeatures());
        featurestore.add(ffi.createComponentMonitoringFeatures());
        featurestore.add(ffi.createComponentMonitoringFeatures());
        ComponentMonitoringFeatures[] components = new ComponentMonitoringFeatures[featurestore.size()];

        // ////////////////////////////////////////////////////////////////////////////////
        // Core Component for Everest Reasoner Features
        // ////////////////////////////////////////////////////////////////////////////////
        components[0] = ffi.createComponentMonitoringFeatures();
        components[0].setUuid("777e8400-sss2-41d4-a716-406075043333");
        components[0].setType("REASONER");

        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        // Model Features
        mflist.addAll(buildModelFeatures());
        // Terms Features
        mflist.addAll(buildLogicalOpFeatures());
        mflist.addAll(buildComparisonOpFeatures());
        mflist.addAll(buildArithmeticFunctionFeatures());
        mflist.addAll(buildAggregateFunctionFeatures());
        mflist.addAll(buildTimeSeriesFeatures());
        mflist.addAll(buildContextFeatures());
        mflist.addAll(buildEventFunctionFeatures());
        mflist.addAll(buildQoSTermFeatures());
        components[0].setMonitoringFeatures(mfListToArray(mflist));

        // ////////////////////////////////////////////////////////////////////////////////
        // Core Component for Everest Sensor Features
        // ////////////////////////////////////////////////////////////////////////////////
        mflist.clear();
        mflist.addAll(buildStandardServiceFeatures());
        components[1] = ffi.createComponentMonitoringFeatures();
        components[1].setUuid("5558400-sss2-41d4-a716-406075043333");
        components[1].setType("SENSOR");
        components[1].setMonitoringFeatures(mfListToArray(mflist));

        return components;
    }

    /**
     * Returns a standard Request Event.
     * 
     * @return Event
     * 
     @see org.slasoi.monitoring.common.features.Event
     * 
     **/
    public static Event getRequestEvent() {
        return evtRequest;
    }

    /**
     * Returns a standard Response Event.
     * 
     * @return Event
     * 
     @see org.slasoi.monitoring.common.features.Event
     * 
     **/
    public static Event getResponseEvent() {
        return evtResponse;
    }

    /**
     * Builds a set of SLA Model Features.
     * 
     * @return LinkedList<MonitoringFeatures>
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private static LinkedList<MonitoringFeature> buildModelFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        // ////////////////////////////////////////////////////////////////////////////
        // AgreementTerm Reasoner (core.$series,arrayofBOOLEAN,core.$BOOLEAN
        // ////////////////////////////////////////////////////////////////////////////
        mflist.add(buildFunction(core.$series, "Reasoner for Series of BOOLEAN", strArrayOfBOOLEAN, "", meta.$BOOLEAN));

        return mflist;
    }

    /**
     * Build a set of Standard Service Sensors.
     * 
     * @return LinkedList<MonitoringFeatures>
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private static LinkedList<MonitoringFeature> buildStandardServiceFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildSensor(evtRequest));
        mflist.add(buildSensor(evtResponse));

        return mflist;

    }

    /**
     * Build a set of Logical Operator Features.
     * 
     * @return LinkedList<MonitoringFeatures>
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private static LinkedList<MonitoringFeature> buildLogicalOpFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(core.$and, "AND Reasoner", meta.$BOOLEAN, meta.$BOOLEAN, meta.$BOOLEAN));

        // special features for monitoring agreement terms
        mflist.add(buildFunction(core.$and, "AND arrayOfBOOLEAN Reasoner", strArrayOfBOOLEAN, "", meta.$BOOLEAN));

        mflist.add(buildFunction(core.$or, "OR Reasoner", meta.$BOOLEAN, meta.$BOOLEAN, meta.$BOOLEAN));
        mflist.add(buildFunction(core.$not, "NOT Reasoner", meta.$BOOLEAN, "", meta.$BOOLEAN));

        // special features for service path terms

        return mflist;

    }

    /**
     * Build a set of Comparison Operator Features.
     * 
     * @return LinkedList<MonitoringFeatures>
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private static LinkedList<MonitoringFeature> buildComparisonOpFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        // equals reasoners and primitives
        mflist.add(buildFunction(core.$equals, "equals NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER, meta.$BOOLEAN));
        mflist.add(buildFunction(core.$equals, "equals TEXT Reasoner", meta.$TEXT, meta.$TEXT, meta.$BOOLEAN));
        mflist.add(buildFunction(core.$equals, "equals TIME Reasoner", meta.$TIME_STAMP, meta.$TIME_STAMP,
                meta.$BOOLEAN));

        // not_equals reasoners and primitives
        mflist
                .add(buildFunction(core.$not_equals, "not_equals NUMBER Reasoner", meta.$STND, meta.$STND,
                        meta.$BOOLEAN));
        mflist.add(buildFunction(core.$not_equals, "not_equals TEXT Reasoner", meta.$TEXT, meta.$TEXT, meta.$BOOLEAN));
        mflist.add(buildFunction(core.$not_equals, "equals TIME Reasoner", meta.$TIME_STAMP, meta.$TIME_STAMP,
                meta.$BOOLEAN));

        // less_than reasoners and primitives
        mflist.add(buildFunction(core.$less_than, "less_than NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER,
                meta.$BOOLEAN));

        // less_than_or_equals reasoners and primitives
        mflist.add(buildFunction(core.$less_than_or_equals, "less_than_or_equals NUMBER Reasoner", meta.$NUMBER,
                meta.$NUMBER, meta.$BOOLEAN));

        // greater_than reasoners and primitives
        mflist.add(buildFunction(core.$greater_than, "greater_than NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER,
                meta.$BOOLEAN));
        mflist.add(buildFunction(core.$greater_than, "greater_than percentage Reasoner", units.$percentage,
                units.$percentage, meta.$BOOLEAN));

        // greater_than_or_equals reasoners and primitives
        mflist.add(buildFunction(core.$greater_than_or_equals, "greater_than_or_equals Reasoner", meta.$NUMBER,
                meta.$NUMBER, meta.$BOOLEAN));

        return mflist;

    }

    /**
     * Build a set of Arithmetic Operator Features.
     * 
     * @return LinkedList<MonitoringFeatures>
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private static LinkedList<MonitoringFeature> buildArithmeticFunctionFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(core.$add, "add NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER, meta.$NUMBER));
        mflist.add(buildFunction(core.$subtract, "subtract NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER, meta.$NUMBER));
        mflist.add(buildFunction(core.$multiply, "multiply NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER, meta.$NUMBER));
        mflist.add(buildFunction(core.$divide, "divide NUMBER Reasoner", meta.$NUMBER, meta.$NUMBER, meta.$NUMBER));

        return mflist;

    }

    /**
     * Build a set of Aggregate Operator Features.
     * 
     * @return LinkedList<MonitoringFeatures>
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private static LinkedList<MonitoringFeature> buildAggregateFunctionFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(core.$sum, "sum Reasoner", strArrayOfNumber, "", meta.$NUMBER));
        mflist.add(buildFunction(core.$mean, "mean Reasoner", strArrayOfNumber, "", meta.$NUMBER));
        mflist.add(buildFunction(core.$max, "sum Reasoner", strArrayOfNumber, "", meta.$NUMBER));
        mflist.add(buildFunction(core.$min, "min Reasoner", strArrayOfNumber, "", meta.$NUMBER));
        mflist.add(buildFunction(core.$count, "count NUMBER Reasoner", strArrayOfNumber, "", meta.$NUMBER));
        mflist.add(buildFunction(core.$count, "count TEXT Reasoner", strArrayOfTEXT, "", meta.$NUMBER));

        return mflist;

    }

    /**
     * Build a set of Time Series Operator Features.
     * 
     * @return LinkedList<MonitoringFeatures>
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private static LinkedList<MonitoringFeature> buildTimeSeriesFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(core.$series, "series with Event Reasoner", strArrayOfNumber, meta.$EVENT,
                meta.$NUMBER));

        return mflist;

    }

    /**
     * Build a set of Context Operator Features.
     * 
     * @return LinkedList<MonitoringFeatures>
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private static LinkedList<MonitoringFeature> buildContextFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(core.$time_is, "time_is reasoner", "", "", core.$time));
        mflist.add(buildFunction(core.$day_is, "day_is reasoner", "", "", meta.$DAY));

        return mflist;
    }

    /**
     * Build a set of Event Function Operator Features.
     * 
     * @return LinkedList<MonitoringFeatures>
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private static LinkedList<MonitoringFeature> buildEventFunctionFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic seconds", units.$s, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic ms", units.$ms, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic mins", units.$min, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic hours", units.$hrs, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic days", units.$day, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic month", units.$month, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic year", units.$year, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$periodic, "periodic year", units.$year, core.$periodic));
        mflist.add(buildFunctionUnitWithEvent(core.$time, "time sensor", "", core.$time));

        return mflist;
    }

    /**
     * Build a set of QoS Term Features.
     * 
     * @return LinkedList<MonitoringFeatures>
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    private static LinkedList<MonitoringFeature> buildQoSTermFeatures() {
        LinkedList<MonitoringFeature> mflist = new LinkedList<MonitoringFeature>();

        mflist.add(buildFunction(common.$availability, "availability reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$arrival_rate, "arrival_rate reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$throughput, "throughput reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$completion_time, "completion_time reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$mttr, "mttr reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$mttf, "mttf reasoner", "", "", meta.$NUMBER));
        mflist.add(buildFunction(common.$reliability, "reliability reasoner", "", "", meta.$NUMBER));

        return mflist;
    }

    /**
     * Converts a MonitoringFeature List to an Array.
     * 
     * @param mflist
     *            A LinkedList of MonitoringFeatures
     * @return MonitoringFeatures[]
     * 
     @see org.slasoi.monitoring.common.features.MonitoringFeature
     * 
     **/
    public static MonitoringFeature[] mfListToArray(final LinkedList<MonitoringFeature> mflist) {
        MonitoringFeature[] modelfeatures = new MonitoringFeature[mflist.size()];
        Iterator<MonitoringFeature> mflit = mflist.iterator();
        int counter = 0;
        while (mflit.hasNext()) {
            modelfeatures[counter++] = mflit.next();
        }
        return modelfeatures;
    }

    /**
     * Builds a generic feature for a Reasoner Function.
     * 
     * @param type
     *            Feature type
     * @param desc
     *            Feature description
     * @param param1Type
     *            Feature input 1
     * @param param2Type
     *            Feature input 2
     * @param resultType
     *            Feature result type
     * @return Function
     * 
     **/
    public static Function buildFunction(final String type, final String desc, final String param1Type,
            final String param2Type, final String resultType) {
        int noofparams = 1;
        if (param2Type.length() > 0) {
            noofparams = 2;
        }

        Function fimpl = ffi.createFunction();
        fimpl.setName(type);
        fimpl.setDescription(desc);

        if (param1Type.length() > 0) {
            Basic[] fInput = new Basic[noofparams];

            Basic fInput1 = ffi.createPrimitive();
            fInput1.setName("param1");
            fInput1.setType(param1Type);
            fInput[0] = fInput1;

            if (param2Type.length() > 0) {
                Basic fInput2 = ffi.createPrimitive();
                fInput2.setName("param2");
                fInput2.setType(param2Type);
                fInput[1] = fInput2;
            }

            fimpl.setInput(fInput);
        }

        if (resultType.length() > 0) {
            Basic fOutput = ffi.createPrimitive();
            fOutput.setName("output1");
            fOutput.setType(resultType);
            fimpl.setOutput(fOutput);
        }

        return fimpl;
    }

    /**
     * Builds a generic feature for a Reasoner Function with an output event.
     * 
     * @param type
     *            Feature type
     * @param desc
     *            Feature description
     * @param unit1Type
     *            Unit input 1
     * @param resultType
     *            Feature result type
     * @return Function
     * 
     **/
    public static Function buildFunctionUnitWithEvent(final String type, final String desc, final String unit1Type,
            final String resultType) {
        Function fimpl = ffi.createFunction();
        fimpl = buildFunction(type, desc, "", "", "");

        // add unit as first parameter
        if (unit1Type.length() > 0) {
            Basic[] fimplInputs = new Basic[1];
            Primitive periodicInput1 = ffi.createPrimitive();
            periodicInput1.setName(desc + " input");
            periodicInput1.setUnit(unit1Type);
            fimplInputs[0] = periodicInput1;
            fimpl.setInput(fimplInputs);
        }

        if (resultType.length() > 0) {
            // now add event as output
            Event eventImpl = ffi.createEvent();
            eventImpl.setName(resultType);
            eventImpl.setType(resultType);
            fimpl.setOutput(eventImpl);
        }
        return fimpl;
    }

    /**
     * Builds a generic feature with name and description for a Sensor with a given output event.
     * 
     * @param name
     *            Feature name
     * @param desc
     *            Feature description
     * @param event
     *            Event type provided by the Sensor
     * @return Function
     * 
     **/
    public static Function buildSensor(final String name, final String desc, final String event) {
        Function fimpl = ffi.createFunction();
        fimpl = buildFunction(name, desc, "", "", "");

        if (event.length() > 0) {
            // now add event as output
            Event eventImpl = ffi.createEvent();
            eventImpl.setName(event);
            eventImpl.setType(event);
            fimpl.setOutput(eventImpl);
        }
        return fimpl;
    }

    /**
     * Builds a generic feature for a Sensor with a given output event.
     * 
     * @param event
     *            Event type provided by the Sensor
     * @return Function
     * 
     **/
    public static Function buildSensor(final Event event) {
        Function fimpl = null;

        if (event != null) {
            fimpl = buildFunctionUnitWithEvent(event.getName(), event.getDescription(), "", event.getName());
        }
        return fimpl;
    }
}
