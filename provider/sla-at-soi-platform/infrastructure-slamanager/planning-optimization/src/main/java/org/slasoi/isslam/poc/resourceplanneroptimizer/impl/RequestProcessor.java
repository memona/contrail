/**
 * 
 */
package org.slasoi.isslam.poc.resourceplanneroptimizer.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;
import org.slasoi.isslam.poc.outresourcer.impl.OutResourceImpl;
import org.slasoi.isslam.poc.resourceplanneroptimizer.exceptions.RequestNotCorrectException;
import org.slasoi.isslam.poc.servicesmanager.impl.ServiceManagerHandlerImpl;
import org.slasoi.isslam.poc.slaparser.SLAParser;
import org.slasoi.isslam.poc.slaparser.exceptions.InvalidSLATemplateFormatException;
import org.slasoi.isslam.poc.utils.Constant;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;

/**
 * Each <code>RequestProcessor</code> represents an infrastructure provider.
 * 
 * @author Kuan Lu
 */
public class RequestProcessor {
    private double innerImplementCost = 0;
    private double externalImplementCost = 0;
    private double clientTotalPrice = 0;
    private String providerName;
    private Collection<Request> requestList;
    private double failureRate;
    private LinkedHashMap<Request, Integer> queryResult = new LinkedHashMap<Request, Integer>();
    private static final Logger LOGGER = Logger.getLogger(RequestProcessor.class);
    private boolean isSlaChanged;
    private String processorName = "INFRASTRUCTURE_SERVICE_PROVIDE_1";
    private String clientType;

    public RequestProcessor(String providerName, double failureRate) {
        this.providerName = providerName;
        this.failureRate = failureRate;
    }

    /**
     * Gets the name of provider.
     */
    public String getProviderName() {
        return providerName;
    }

    /*
     * public void addSubRequestProcessor(RequestProcessor requestProcessor){ if(this.subRequestProcessors!=null){
     * this.subRequestProcessors.add(requestProcessor); } else { this.subRequestProcessors = new
     * ArrayList<RequestProcessor>(); this.subRequestProcessors.add(requestProcessor); } }
     */

    /**
     * Gets the request.
     */
    public Collection<Request> getRequest() {
        return requestList;
    }

    /**
     * Gets the failure rate.
     */
    public double getFailureRate() {
        return failureRate;
    }

    /**
     * recursive be run and its sub-providers, if the return value is -1, means provider can not make agreement due to
     * high risk or low profit.
     * 
     * @return the total price to customer
     * @throws org.slasoi.isslam.poc.resourceplanneroptimizer.exceptions.RequestNotCorrectException
     */
    public void startProcess(Collection<Request> requestList) throws RequestNotCorrectException {
        this.innerImplementCost = 0;
        this.requestList = requestList;
        if (this.requestList == null) {
            throw new RequestNotCorrectException("The incoming request is either null or incorrect format.");
        }

        // set the client type
        Iterator<Request> it = this.requestList.iterator();
        while (it.hasNext()) {
            Request request = (Request) it.next();
            this.clientType = request.getClientType();
        }

        // start process
        this.recursive();
    }

    private void recursive() {
        if (this.compareRequestWithMainProvider(this.requestList) == false) {
            LOGGER.info(this.processorName + " can only handle part of the requests for resources");
            // calculate how many cores and memory still need, each vm needs still 2 and 128;.
            int vmStill = 1;
            int cpuStill = 2;
            int memoryStill = 128;
            // outsourcing , after query from ISM, this.provisionRequestType will be changed , from which POC can know
            // how much ISM has.
            LOGGER.info("Start to outsourcing...");
            OutResourceImpl outsourcing = new OutResourceImpl();
            // TODO: according to the request type, in the future, SLA template registry should give the proper SLA
            // templates list for outsourcing. but currently just
            // return the hard coded SLA template.
            ArrayList<SLATemplate> slatArray = outsourcing.getProperSLATemplates(this.requestList);
            // SLATemplate[] slatArrayCopy = slatArray.clone();
            if (slatArray != null) {
                // modify the SLAtemplates
                SLATemplate slat1;
                ArrayList<SLATemplate> deleteIndex = new ArrayList<SLATemplate>();
                for (int i = 0; i < slatArray.size(); i++) {
                    slat1 = slatArray.get(i);
                    SLAParser parser = new SLAParser(slat1);
                    for (Request request : this.requestList) {
                        Request requestFromSubProvider = parser.getResourceRequest().get(request.getVmName());
                        if (requestFromSubProvider != null) {
                            requestFromSubProvider.getVmNumber().setValue(String.valueOf(vmStill));
                            requestFromSubProvider.getCpuNr().setValue(String.valueOf(cpuStill));
                            requestFromSubProvider.getMemoryNr().setValue(String.valueOf(memoryStill));
                        }
                        else {
                            LOGGER.warn("SLATemplate " + slat1.getUuid() + " does not have required VM type of "
                                    + request.getVmName());
                            deleteIndex.add(slat1);
                            break;
                            // TODO: that SLATemplate should be deleted from slatArray;
                        }
                    }
                }
                for (int y = 0; y < deleteIndex.size(); y++) {

                    slatArray.remove(deleteIndex.get(y));
                }
                label: {
                    if (slatArray.size() == 0) {
                        LOGGER.warn("There is no quanlified SLA Template in the Template registry for outsourcing");
                        break label;
                    }

                    // sla templates will be changed by its sub-providers
                    ArrayList<SLATemplate> resultSLATFromSubProviders = outsourcing.negotiation(slatArray);
                    LinkedHashMap<SLAParser, SLATemplate> candidateSLATSubProviders =
                            new LinkedHashMap<SLAParser, SLATemplate>();
                    // parser each SLATemplate, and find out the first one who can fulfill the request.
                    float price = Float.MAX_VALUE;
                    if (resultSLATFromSubProviders != null) {
                        mostOutsideLoop: for (SLATemplate slat : resultSLATFromSubProviders) {
                            SLAParser parser = new SLAParser(slat);
                            for (Request request : this.requestList) {
                                Request requestFromSubProvider = parser.getResourceRequest().get(request.getVmName());
                                if (Integer.parseInt(requestFromSubProvider.getCpuNr().getValue()) == cpuStill) {
                                    if (Integer.parseInt(requestFromSubProvider.getMemoryNr().getValue()) == memoryStill) {
                                        if (Integer.parseInt(requestFromSubProvider.getVmNumber().getValue()) == vmStill) {
                                            continue;
                                        }
                                        else {
                                            continue mostOutsideLoop;
                                        }
                                    }
                                    else {
                                        continue mostOutsideLoop;
                                    }
                                }
                                else {
                                    continue mostOutsideLoop;
                                }
                            }
                            candidateSLATSubProviders.put(parser, slat);
                            continue mostOutsideLoop;
                        }

                        SLAParser finalParser = new SLAParser(null);
                        for (SLAParser parser : candidateSLATSubProviders.keySet()) {
                            if (parser.getPrice() > 0 & price > parser.getPrice()) {
                                finalParser = parser;
                                price = parser.getPrice();
                            }
                            else
                                continue;
                        }
                        if (!candidateSLATSubProviders.containsKey(finalParser)) {
                            LOGGER.error("Unable to find a subcontractor.");
                            return;
                        }
                        SLATemplate slat = candidateSLATSubProviders.get(finalParser);
                        LOGGER.info("We select one sub provider " + slat.getUuid()
                                + " and are setting up an agreement with him...");
                        if ((outsourcing.createSLA(slat)) instanceof SLA) {
                            // the subcontractor accept the SLA template and set up the SLA
                            // TODO: here I set the external implementation hard coded, actually it should come
                            // from sub contractor.

                            this.externalImplementCost = 100;
                            // number of external VMs, here suppose that only 1 vm is needed
                            this.externalImplementCost = this.externalImplementCost * 1;
                            LOGGER.info("We have external implementation cost of " + this.externalImplementCost
                                    + ". We will charge " + Constant.externalProfitRate + " of it.");
                            double finalProfit = 0;
                            double bulkPurchase = 0.75;

                            for (Request request : this.requestList) {
                                double extraQoS = this.calculateExtraQoSCosts(request);
                                double factor = bulkPurchase * extraQoS;
                                LOGGER.info("QoS extra requirement, we will charge client as factor of " + extraQoS
                                        + " more");
                                double temp = 0;
                                for (Resource requiredResource : request.getResource().values()) {
                                    LOGGER.info("Main provider can handle the request for resource "
                                            + requiredResource.getResourceName());
                                    LOGGER.info("Client requires " + requiredResource.getAmount() + " "
                                            + requiredResource.getResourceName() + ". We have "
                                            + (requiredResource.getAmount() - 2) + " "
                                            + requiredResource.getResourceName() + ". And we have 2 from outsourcers"
                                            + ". We will give client " + bulkPurchase + " discount from out side.");
                                    LOGGER.info("We will charge final factor of " + factor);
                                    temp += (requiredResource.getAmount() - 2) * requiredResource.getPrice() * factor;
                                }
                                // base price of 1 vm multiply with the number of vms
                                LOGGER.info(Integer.parseInt(request.getVmNumber().getValue())
                                        + " VM(s) is/are required");
                                this.innerImplementCost += temp * Integer.parseInt(request.getVmNumber().getValue());
                            }

                            LOGGER.info("The total inner implementation cost is : " + this.innerImplementCost);
                            finalProfit =
                                    Constant.innerProfitRate * this.innerImplementCost + this.externalImplementCost
                                            * Constant.externalProfitRate;
                            this.processFinalProfit(finalProfit);
                            return;
                        }
                        else {
                            LOGGER
                                    .warn("Ops! the selected sub contractor did not create an agreement with me due to some reasons.");
                            return;
                        }
                    }
                }

                LOGGER.warn("Outsourcer does not create an agreement due to some reason...");
                // all the sub providers do not have enough resources !!do combination
                // if(combination can satisify){ incoming SLAT keep it as it is;l }
                // else if all the combination still can not satisfy.
                /*
                 * if(more than one slaTemplate can satisfy the request) { select optimal one of them. externalPrice =
                 * SLATemplate.getPrice(); the innerPrince is the price of the whole storage. incoming SLAT keep it as
                 * it is; inform the subcontractors to createAgreement} else if(all subs can not satisfy) { do
                 * combination() until the combination of whole; externalPrice = combination Prices; the innerPrince is
                 * the price of the whole storage. if(combination can satisify){ incoming SLAT keep it as it is;l } else
                 * {
                 */
                // modify the SLAT to be the total storage.
                double finalProfit = 0;

                for (Request request : this.requestList) {
                    double extraQoS = this.calculateExtraQoSCosts(request);
                    LOGGER.info("QoS extra requirement, we will charge client as factor of " + extraQoS + " more");
                    double temp = 0;
                    for (Resource requiredResource : request.getResource().values()) {
                        int amount = requiredResource.getAmount();
                        if (amount > 2) {
                            requiredResource.setAmount((amount - 2));
                            double bulkPurchase = 0.75;
                            LOGGER.info("We will give client " + bulkPurchase + " discount.");
                            double factor = bulkPurchase * extraQoS;
                            LOGGER.info("We will charge final factor of " + factor);
                            temp += requiredResource.getAmount() * requiredResource.getPrice() * factor;
                        }
                        else {
                            requiredResource.setAmount(0);
                        }
                    }
                    // base price of 1 vm multiply with the number of vms
                    LOGGER.info(Integer.parseInt(request.getVmNumber().getValue()) + " VM(s) is/are required");
                    this.innerImplementCost += temp * Integer.parseInt(request.getVmNumber().getValue());
                }

                LOGGER.info("The final total implementation cost is " + this.innerImplementCost);
                this.isSlaChanged = true;
                finalProfit = Constant.innerProfitRate * this.innerImplementCost;
                LOGGER.info("**********************************************");
                LOGGER.info("Name                       Amount");
                for (Request request : this.requestList) {
                    LOGGER.info("For " + request.getVmName() + "                      "
                            + request.getVmNumber().getValue());
                    for (Resource resource : request.getResource().values()) {
                        if (resource.getResourceName().equals(Constant.Memory)) {
                            LOGGER.info(resource.getResourceName() + "                        " + resource.getAmount());
                        }
                        else if (resource.getResourceName().equals(Constant.CPU)) {
                            LOGGER.info(resource.getResourceName() + "                           "
                                    + resource.getAmount());
                        }

                    }
                }
                LOGGER.info("**********************************************");
                this.processFinalProfit(finalProfit);
                return;
                // }
                // }
            }
            // this service provider does not have any sub-providers
            else {
                double finalProfit = 0;

                for (Request request : this.requestList) {

                    double extraQoS = this.calculateExtraQoSCosts(request);
                    LOGGER.info("QoS extra requirement, we will charge client as factor of " + extraQoS + " more");
                    double temp = 0;
                    for (Resource requiredResource : request.getResource().values()) {
                        int amount = requiredResource.getAmount();
                        if (amount > 2) {
                            requiredResource.setAmount((amount - 2));
                            double bulkPurchase = 0.75;
                            LOGGER.info("We will give client " + bulkPurchase + " discount.");
                            double factor = bulkPurchase * extraQoS;
                            LOGGER.info("We will charge final factor of " + factor);
                            temp += requiredResource.getAmount() * requiredResource.getPrice() * factor;
                        }
                        else {
                            requiredResource.setAmount(0);
                        }
                    }

                    // base price of 1 vm multiply with the number of vms
                    LOGGER.info(Integer.parseInt(request.getVmNumber().getValue()) + " VM(s) is/are required");
                    this.innerImplementCost += temp * Integer.parseInt(request.getVmNumber().getValue());
                }

                LOGGER.info("The final total implementation cost is " + this.innerImplementCost);
                this.isSlaChanged = true;

                finalProfit = Constant.innerProfitRate * this.innerImplementCost;
                LOGGER.info("**********************************************");
                LOGGER.info("We will supply : ");
                LOGGER.info("Name                       Amount");
                for (Request request : this.requestList) {
                    LOGGER.info("For " + request.getVmName() + "                      "
                            + request.getVmNumber().getValue());
                    for (Resource resource : request.getResource().values()) {
                        if (resource.getResourceName().equals(Constant.Memory)) {
                            LOGGER.info(resource.getResourceName() + "                        " + resource.getAmount());
                        }
                        else if (resource.getResourceName().equals(Constant.CPU)) {
                            LOGGER.info(resource.getResourceName() + "                           "
                                    + resource.getAmount());
                        }

                    }
                }
                LOGGER.info("**********************************************");

                this.processFinalProfit(finalProfit);
                return;
            }

        }
        // main has enough resource, start to calculate the function
        // g(), the cost of implementation, local, profit
        else {
            double finalProfit = 0;
            // TODO: fix it!!!
            double bulkPurchase = 0.75;
            for (Request request : this.requestList) {
                LOGGER.info("For " + request.getVmName());
                double extraQoS = this.calculateExtraQoSCosts(request);
                LOGGER.info("QoS extra requirement, we will charge client as factor of " + extraQoS + " more");
                double factor = bulkPurchase * extraQoS;
                double temp = 0;
                for (Resource requiredResource : request.getResource().values()) {
                    LOGGER.info("Main provider can handle the request for resource "
                            + requiredResource.getResourceName());
                    LOGGER.info("Client requires " + requiredResource.getAmount() + " "
                            + requiredResource.getResourceName() + ". We have " + requiredResource.getResourceName()
                            + ". We will give client " + bulkPurchase + " discount.");
                    LOGGER.info("We will charge final factor of " + factor);
                    temp += requiredResource.getAmount() * requiredResource.getPrice() * factor;
                    LOGGER.info("Price for " + requiredResource.getResourceName() + " is "
                            + requiredResource.getPrice());
                }
                // base price of 1 vm multiply with the number of vms
                LOGGER.info(Integer.parseInt(request.getVmNumber().getValue()) + " VM(s) is/are required");
                this.innerImplementCost += temp * Integer.parseInt(request.getVmNumber().getValue());
            }

            LOGGER.info("The final total implementation cost is " + this.innerImplementCost);
            finalProfit = Constant.innerProfitRate * this.innerImplementCost;

            this.processFinalProfit(finalProfit);
            return;

        }
    }

    private void processFinalProfit(double finalProfit) {
        LOGGER.info("Final profit is : " + finalProfit);
        if (finalProfit > 0) {
            double betaMax =
                    (finalProfit - finalProfit * getMinimalProfitsRate(this.clientType)) / this.innerImplementCost;

            // TODO: failurerate if outresource then it should consider the subprovider's failure rate
            double betaMin =
                    (Constant.maxFailureRate - this.failureRate)
                            / ((Constant.minimalH - this.failureRate) / Constant.maxBeta);
            LOGGER.info("BetaMax is : " + betaMax);
            LOGGER.info("BetaMin is : " + betaMin);
            if (betaMax >= betaMin) {
                LOGGER.info("We will select BetaMin : " + betaMin + " to make profit maximal and acceptable risk.");
                clientTotalPrice +=
                        this.innerImplementCost + this.externalImplementCost
                                + (finalProfit - betaMin * this.innerImplementCost);
                // LOGGER.info("The total client price is : " + clientTotalPrice);
            }
            else {
                LOGGER.error(this.getProviderName()
                        + " can not satisfy the request because BetaMax is less than its BetaMin");
                return;
            }
        }
        else
            return;

        LOGGER.info(this.processorName + " will bid " + clientTotalPrice + " euro for all resources");
        LOGGER
                .info("=============================================================================================================================");
    }

    /**
     * To evaluate whether the SLA is changed or not.
     */
    public boolean isSlaChanged() {
        return isSlaChanged;
    }

    /**
     * find the most proper sub-provider
     * 
     * @param subRequest
     * @return
     */
    /*
     * private ArrayList<RequestProcessor> getBestSubProviders(Request subRequest) { ArrayList<RequestProcessor>
     * bestSubProviderList = new ArrayList<RequestProcessor>(); // only one type of resource String resourceName =
     * subRequest.getRequirements().get(0).getResourceName(); // find all the sub-providers that can directly handle the
     * requirement, // otherwise make a smallest combination of sub-poviders.
     * 
     * LOGGER.info("Start sorting the sub-providers according to the number of " + resourceName);
     * SubProviderResourceComparator comparator = new SubProviderResourceComparator(resourceName);
     * Collections.sort(this.subRequestProcessors, comparator);
     * 
     * LOGGER.info("Sorted list:"); for (RequestProcessor subProvider : this.subRequestProcessors) {
     * LOGGER.info(subProvider.getProviderName() + " with " + subProvider.getResourceByName(resourceName).getAmount() +
     * " " + resourceName); } // now the sorted the sub-providers list // put the subProvider with greater or equal
     * number of required // resources into the container for (RequestProcessor subProvider : this.subRequestProcessors)
     * { Resource resouce = subProvider.getResourceByName(resourceName); if (resouce != null) { if (resouce.getAmount()
     * >= subRequest.getRequirements().get(0).getAmount()) { bestSubProviderList.add(subProvider); } } }
     * 
     * if (bestSubProviderList.size() > 0) { RequestProcessor temp =
     * this.findBestSubProviderViaPriceFailureRate(bestSubProviderList, resourceName); bestSubProviderList.clear();
     * bestSubProviderList.add(temp); } // no sub-provider with greater or equal number of required resources, // we
     * have to make a combination of more than two sub-providers and try // to make its number as less as possible else
     * if (bestSubProviderList.size() == 0) { int requiredNr = subRequest.getRequirements().get(0).getAmount(); for
     * (RequestProcessor subProvider : this.subRequestProcessors) { Resource resouce =
     * subProvider.getResourceByName(resourceName); if (resouce != null) { if (resouce.getAmount() < requiredNr) {
     * bestSubProviderList.add(subProvider); requiredNr = requiredNr - resouce.getAmount(); continue; } else if
     * (resouce.getAmount() >= requiredNr) { bestSubProviderList.add(subProvider); break; } } } } return
     * bestSubProviderList; }
     */

    /**
     * There is at least one sub-provider can handle the request directly without any combination, then select the best
     * one.
     * 
     * @param bestSubProviderList
     * @param resourceName
     * @return
     */
    /*
     * private RequestProcessor findBestSubProviderViaPriceFailureRate(ArrayList<RequestProcessor> bestSubProviderList,
     * String resourceName) { double distance = -1; double distance_temple; int bestSubProvider = -1; int i = 0; for
     * (RequestProcessor provider : bestSubProviderList) { distance_temple =
     * Math.sqrt(Math.pow(provider.getResourceByName(resourceName).getPrice(), 2) + Math.pow(provider.getFailureRate(),
     * 2));
     * 
     * LOGGER.info(provider.getProviderName() + " has price failure rate distance of " + distance_temple); if (distance
     * == -1 & bestSubProvider == -1) { distance = distance_temple; bestSubProvider = i; i++; continue; } else if
     * (distance >= 0) { if (distance_temple < distance) { distance = distance_temple; bestSubProvider = i; i++;
     * continue; } else i++; }
     * 
     * } LOGGER.info("The sub-provider with shortest distance is " +
     * bestSubProviderList.get(bestSubProvider).getProviderName() + ". The distance is: " + distance); return
     * bestSubProviderList.get(bestSubProvider);
     * 
     * }
     */

    /**
     * Compares the required amount of specific resource with Main provider
     * 
     */
    private boolean compareRequestWithMainProvider(Collection<Request> requestList) {
        if(ServiceManagerHandlerImpl.getInstance().getInfraServiceManager()==null){
            return true;
        }
        
        queryResult = ServiceManagerHandlerImpl.getInstance().query(requestList);
        for (int value : queryResult.values()) {
            if (value < 0) {
                return false;
            }
            else
                continue;
        }
        return true;

    }

    /**
     * compare all the required resources with main and its sub-providers' storage
     * 
     * @return
     */
    /*
     * private boolean compareRequestWithStorage() { boolean flag = true; for (Resource requiredResource :
     * this.request.getResource().values()) { String resourceName = requiredResource.getResourceName(); if
     * (requiredResource.getAmount() > this.getAllResourceStorage(resourceName)) { LOGGER.warn("client needs " +
     * requiredResource.getAmount() + " " + resourceName + ", but we have only " +
     * this.getAllResourceStorage(resourceName)); // return false; flag = false; continue; } else continue; } if (flag
     * == false) { return false; } else return true; }
     */

    /**
     * get all the resource storage from a main provider point of view.
     * 
     * @param resourceName
     * @return
     */
    /*
     * private int getAllResourceStorage(String resourceName) { int totalAmount = 0; // main provider for (Resource
     * mainResource : this.getResources()) { if (mainResource.getResourceName().equals(resourceName)) { totalAmount +=
     * mainResource.getAmount(); break; } else continue; } // for each sub_provider for (RequestProcessor subProvider :
     * this.getSubRequestProcessors()) { for (Resource subResource : subProvider.getResources()) { if
     * (subResource.getResourceName().equals(resourceName)) { totalAmount += subResource.getAmount(); break; } else
     * continue; } } return totalAmount; }
     */

    /**
     * calculate the missing resources for outsourcing,
     * 
     * @return new request to sub-provider
     */
    /*
     * private Request calculateMissingResources(int missingResource, Resource requiredResource) { if
     * (this.isMainProvider == true) { Resource newResource = new Resource(requiredResource.getResourceName(),
     * missingResource, -1); ArrayList<Resource> newResourceList = new ArrayList<Resource>();
     * newResourceList.add(newResource); // it is supposed that the main provider is the gold type client to // all its
     * sub-provides // TODO: it is supposed that the main provider is the gold type // client to all its sub-provides
     * return new Request(newResourceList, this.request.getQos(), Constant.Client_Type_Gold); } else {
     * LOGGER.warn("This method can only be invoked by main provider."); return null; } }
     */

    /**
     * Calculates the bulk purchase reduction
     * 
     */
    private double calculateBulkPriceReduction(int reqiredAmount, int availableAmount) {
        double percentage =
                (Integer.valueOf(reqiredAmount).doubleValue() / Integer.valueOf(availableAmount).doubleValue());
        LOGGER.info("The purchase percentage is " + percentage);
        if (percentage >= 0.9) {
            return 0.75;
        }
        else if (percentage < 0.9 & percentage >= 0.8) {
            return 0.8;
        }
        else if (percentage < 0.8 & percentage >= 0.7) {
            return 0.85;
        }
        else if (percentage < 0.7 & percentage >= 0.6) {
            return 0.9;
        }
        else if (percentage < 0.6 & percentage >= 0.5) {
            return 0.95;
        }
        else
            return 1;
    }

    /**
     * Calculates the cost of extra QoS requests
     * 
     */
    private double calculateExtraQoSCosts(Request request) {
        double finalQoSCost = 1;
        if (request != null) {
            // availability
            double requiredAvailability = request.getAvailability();
            if (requiredAvailability > Constant.maxAvailability) {
                LOGGER.error("Can not supply the required QoS availability of " + requiredAvailability
                        + ". Maximal availability is 99%");
                LOGGER.info("We will supply the maximal availability of 99%");
                finalQoSCost += (Constant.maxAvailability - requiredAvailability) * 100 * 0.1;
            }
            else if (requiredAvailability < Constant.basicAvailability) {
                LOGGER.error("Can not supply the required QoS availability of " + requiredAvailability
                        + ". Basic availability is 95%");
                LOGGER.info("We will supply the basic availability of 95% without charging.");
            }
            else {
                finalQoSCost += (requiredAvailability - Constant.basicAvailability) * 100 * 0.1;
            }

            // isolation
            boolean isIsolation = request.isIsolation();
            if (isIsolation == true) {
                LOGGER.info("Client needs isolation, charge 50% more.");
                finalQoSCost += 0.5;
            }
            return finalQoSCost;
        }
        else {
            LOGGER.error("There is no request.");
            return finalQoSCost;
        }
    }

    /**
     * Calculates the minimal profits, which depends on the type of the client.
     * 
     */
    private double getMinimalProfitsRate(String clientType) {
        if (clientType.equalsIgnoreCase(Constant.Client_Type_Gold)) {
            return Constant.goldCustomerDiscount;
        }
        else if (clientType.equalsIgnoreCase(Constant.Client_Type_Silver)) {
            return Constant.silverCustomerDiscount;
        }
        else if (clientType.equalsIgnoreCase(Constant.Client_Type_Bronze)) {
            return Constant.bronzeCustomerDiscount;
        }
        else
            return Constant.bronzeCustomerDiscount;
    }

}
