package org.slasoi.isslam.poc.servicesmanager.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.servicemanager.exceptions.DescriptorException;
import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;
import org.slasoi.infrastructure.servicemanager.types.CapacityResponseType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;
import org.slasoi.infrastructure.servicemanager.types.ReservationResponseType;
import org.slasoi.ism.occi.IsmOcciService;
import org.slasoi.isslam.poc.monitoringconfigserialization.Serialization;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.CPU;
import org.slasoi.isslam.poc.resourceplanneroptimizer.impl.Request;
import org.slasoi.isslam.poc.servicesmanager.ServiceManagerHandler;
import org.slasoi.isslam.poc.slaparser.exceptions.InvalidSLATemplateFormatException;
import org.slasoi.isslam.poc.utils.Constant;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import eu.slasoi.infrastructure.model.infrastructure.Compute;

/**
 * The implementation of interface <code>ServiceManagerHandler</code>
 * 
 * @see org.slasoi.isslam.poc.servicesmanager.ServiceManagerHandler
 * @author Kuan Lu
 */
public class ServiceManagerHandlerImpl implements ServiceManagerHandler {
    public static ServiceManagerHandlerImpl instance;
    private IsmOcciService infraServiceManager;
    private String hostname = "ipoc";
    private String serviceName = "provisionID";
    private static String notificationURI = "planoptimization@sla-at-soi.eu";
    private static final Logger LOGGER = Logger.getLogger(ServiceManagerHandlerImpl.class);

    /**
     * Gets the infrastructure service manager.
     */
    public IsmOcciService getInfraServiceManager() {
        return infraServiceManager;
    }

    /**
     * Sets the infrastructure service manager.
     */
    public void setInfraServiceManager(IsmOcciService infraServiceManager) {
        this.infraServiceManager = infraServiceManager;
    }

    /**
     * Gets the instance of infrastructure service manager handler.
     */
    public static ServiceManagerHandlerImpl getInstance() {
        if (ServiceManagerHandlerImpl.instance == null) {
            // LOGGER.error("The context of infrastructure service manager is still not be injected into ServiceManagerHandlerImpl.");
            ServiceManagerHandlerImpl.instance = new ServiceManagerHandlerImpl();
            return ServiceManagerHandlerImpl.instance;
        }
        else
            return ServiceManagerHandlerImpl.instance;
    }
    
    /**
     * create the B4-based monitoring features according to "B4MonitoringFeatures" in monitoring manager
     * 
     * Constructs a set of ComponentMonitoringFeatures.
     * 
     * @return org.slasoi.monitoring.common.features.ComponentMonitoringFeatures[]
     * 
     @see org.slasoi.monitoring.common.features.ComponentMonitoringFeatures
     * 
     **/
   public ComponentMonitoringFeatures[] getMonitoringFeature() {
        try {
            if(infraServiceManager==null){
                return null;
            }
            else
            return infraServiceManager.getMonitoringFeatures(3000);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public boolean prepare() {
        return false;
    }

    /*
     * public ProvisionRequestType generateProvisionRequestType(Request request) { // get the resource request
     * LOGGER.info("Start to prepare " + request.getResource().get(Constant.CPU).getAmount() + " core(s)");
     * LOGGER.info("Start to prepare " + request.getResource().get(Constant.Memory).getAmount() * 128 +
     * "MB memory size");
     * 
     * Random rnd = new Random(System.currentTimeMillis()); ProvisionRequestType provisionRequestType =
     * this.infraServiceManager.createProvisionRequestType(request.getImage(), request.getClientType(),
     * request.getLocation(), request.getResource().get(Constant.CPU).getAmount(), ((CPU) request
     * .getResource().get(Constant.CPU)).getSpeed(), request.getResource() .get(Constant.Memory).getAmount() * 128,
     * this(), hostname + Integer.toString(rnd.nextInt(1000)), "notification URI"); //
     * this.infraServiceManager.createProvisionRequestType(imageID, slaTypeID, locationID, cores, speed, memory, //
     * monitoringRequest, hostName, notificationURI); return provisionRequestType; }
     */

    /**
     * generates ProvisionRequestType from infrastructure service manager.
     */
    public ProvisionRequestType generateProvisionRequestType(Collection<Request> requestList,
            MonitoringSystemConfiguration monitoringConfig) throws InvalidSLATemplateFormatException {     
        // get the resource request
        if (requestList != null) {
            Set<Compute> computeConfigurations = new HashSet<Compute>();
            LinkedHashMap<Request, String> sizeList = this.vmSizeMapping(requestList);
            Iterator<Request> it = sizeList.keySet().iterator();
            while (it.hasNext()) {
                Request request = it.next();
                LOGGER.info("Start to prepare " + request.getResource().get(Constant.CPU).getAmount() + " core(s)");
                LOGGER.info("Start to prepare " + request.getResource().get(Constant.Memory).getAmount() * 128
                        + "MB memory size");
                // Random rnd = new Random(System.currentTimeMillis());
                // add the VM according to the number of vm in the request
                int vmNumber = Integer.parseInt((request.getVmNumber().getValue()));
                if (vmNumber <= 0) {
                    throw new InvalidSLATemplateFormatException(
                            "The number of VM cannot be less than 1, check out the SLATamplate again.");
                }

                for (int i = 0; i < vmNumber; i++) {
                    Hashtable<String, String>  extension = new Hashtable<String, String>();
                    extension.put(Constant.startTtime, request.getStartTime());
                    extension.put(Constant.endTime, request.getFreeAt());
                    if(request.getResource().containsKey(Constant.Harddisk)){
                        extension.put(Constant.Harddisk, String.valueOf(request.getResource().get(Constant.Harddisk).getAmount()));
                    }
                    if(request.getResource().containsKey(Constant.Bandwidth)){
                        extension.put(Constant.Bandwidth, String.valueOf(request.getResource().get(Constant.Bandwidth).getAmount()));
                    } 
                       
                    computeConfigurations.add(this.infraServiceManager.createComputeConfiguration(request
                                .getImage(), sizeList.get(request), request.getLocation().equals("") ? "ie" : request
                                .getLocation(), extension));
                    
                }
            }
            return this.infraServiceManager.createProvisionRequestType(Serialization.getInstance().serialize(monitoringConfig),computeConfigurations);
        }
        else
            return null;
    }
    
    /**
     * classify the request to be corresponding type of VM (small medium or large)
     */
    private LinkedHashMap<Request, String> vmSizeMapping(Collection<Request> requestList){
        LinkedHashMap<Request, String> sizeList = new LinkedHashMap<Request, String>();
        for(Request request : requestList){
            if(Integer.parseInt(request.getCpuNr().getValue())==Constant.smallCPU){
                sizeList.put(request, Constant.small);
                continue;
            }
            else if(Integer.parseInt(request.getCpuNr().getValue())==Constant.mediumCPU){
                sizeList.put(request, Constant.medium);
                continue;
            }
            else if(Integer.parseInt(request.getCpuNr().getValue())>=Constant.largeCPU){
                sizeList.put(request,Constant.large);
                continue;
            }
        }
        return sizeList;
    }
    

    /**
     * Queries the resource capability of service manager and compare it with its requests.
     * return value will be the left capacity.
     */
    public LinkedHashMap<Request, Integer> query(Collection<Request> requestList) {
            LinkedHashMap<String, Integer> infrastructuresCapacityList = new LinkedHashMap<String, Integer>();
            
            List<CapacityResponseType> capacityList;
            try {
                capacityList = ServiceManagerHandlerImpl.getInstance().infraServiceManager.queryCapacity();
                if(capacityList==null || capacityList.size()!=3){
                    LOGGER.warn("The return value of query operation is null or there should only be three types of infrastructures, small, medium and large.");
                }
                
                for (CapacityResponseType capacity : capacityList) {
                    // System.out.println("THE DETAILED INFORMATION IS :: "+capacity.toString());
                    String type = capacity.getType();
                    if (type.equalsIgnoreCase(Constant.small)) {
                        infrastructuresCapacityList.put(Constant.small, capacity.getAvail());
                        continue;
                    }
                    else if (type.equalsIgnoreCase(Constant.medium)) {
                        infrastructuresCapacityList.put(Constant.medium, capacity.getAvail());
                        continue;
                    }
                    else if (type.equalsIgnoreCase(Constant.large)) {
                        infrastructuresCapacityList.put(Constant.large, capacity.getAvail());
                        continue;
                    }
                }
            }
            catch (DescriptorException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            LinkedHashMap<Request, Integer> returnValue = new LinkedHashMap<Request, Integer>(); 
            // start to compare
            LinkedHashMap<Request, String> list = this.vmSizeMapping(requestList);
            for(Request request : list.keySet()){
                int result = infrastructuresCapacityList.get(list.get(request))-Integer.valueOf(request.getVmNumber().getValue());
                returnValue.put(request, result);
            }
            
            return returnValue;
    }

    /**
     * Reserves the resources from infrastructure service manager.
     */
    public ReservationResponseType reserve(ProvisionRequestType provisioningRequest) {
        try {
            return this.infraServiceManager.reserve(provisioningRequest);
        }
        catch (DescriptorException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Commit the resources from infrastructure service manager.
     */
    public ReservationResponseType commit(ReservationResponseType reservationResponseType){
        try {
            return this.infraServiceManager.commit(reservationResponseType.getInfrastructureID());
        }
        catch (UnknownIdException e) {
            e.printStackTrace();
            return null;
        }
    }

}
