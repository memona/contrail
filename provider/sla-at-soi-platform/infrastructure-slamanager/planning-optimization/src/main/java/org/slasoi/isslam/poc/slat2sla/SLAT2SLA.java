package org.slasoi.isslam.poc.slat2sla;

import org.slasoi.slamodel.sla.SLA;

/**
 * The interface <code>SLAT2SLA</code> for transforming SLA template to be SLA.
 * 
 * @author Kuan Lu
 */
public interface SLAT2SLA {

    /**
     * Starts transferring.
     */
    public SLA transfer();

}
