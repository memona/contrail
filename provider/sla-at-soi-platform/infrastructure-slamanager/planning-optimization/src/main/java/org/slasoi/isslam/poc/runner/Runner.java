/**
 * 
 */
package org.slasoi.isslam.poc.runner;

/**
 * The interface <code>Runner</code> represents starting optimization.
 * 
 * @author Kuan Lu
 */
public interface Runner {
    /**
     * Gets service evaluation.
     */
    public boolean getServiceEvaluation(/*
                                         * SLA obeject, SLADependencies dependencies
                                         */);

    /**
     * Gets plan and optimization.
     */
    public boolean getPlanOptimization(/* OCCI occi, QoS qos */);
}
