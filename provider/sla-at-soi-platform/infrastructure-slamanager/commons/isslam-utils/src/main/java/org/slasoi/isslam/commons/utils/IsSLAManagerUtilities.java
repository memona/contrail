package org.slasoi.isslam.commons.utils;

public interface IsSLAManagerUtilities {
    public String name();

    public String lowercase(String source);
}
