/**
 * 
 */
package org.slasoi.isslam.pac;

import org.slasoi.gslam.core.context.SLAManagerContext;
import org.slasoi.gslam.core.negotiation.SLARegistry;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.gslam.pac.ActionExecutionTask;
import org.slasoi.gslam.pac.ManagedElement;
import org.slasoi.gslam.pac.ProvisioningAndAdjustment;
import org.slasoi.gslam.pac.Task;
import org.slasoi.gslam.pac.TaskStatus;
import org.slasoi.gslam.pac.config.TaskConfiguration;
import org.slasoi.infrastructure.servicemanager.Infrastructure;
import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionResponseType;
import org.slasoi.isslam.commons.InfrastructureTask;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;

/**
 * Class in charge of executing a given action that in turn means an invocation to some external component (typically,
 * Service Manager)
 * 
 * @author Beatriz Fuentes (TID)
 * 
 */
public class InfrastructureActionExecutionTask extends ActionExecutionTask {

    protected ProvisionResponseType response = null;

    /**
     * @param config
     */
    public InfrastructureActionExecutionTask(TaskConfiguration config, org.slasoi.gslam.commons.plan.Task action,
            Task parent) {
        super(config, action, parent);
    }

    /**
     * Needed for RTTI purposes
     */
    public InfrastructureActionExecutionTask() {
        logger.debug("Creating infrastructure InfrastructureActionExecutionTask");
    }

    public ProvisionResponseType getProvisionResponse() {
        return response;
    }

    public void run() {

        if (action != null) {
            logger.info("Triggering action " + action.getActionName() + " to ServiceManager "
                    + action.getServiceManagerId());

            action.setStatus(TaskStatus.CREATED.toString());

            ManagedElement serviceManager = agent.getManagedElement(action.getServiceManagerId());

            Object element = null;

            if (serviceManager != null) {
                element = serviceManager.getElement();
            }
            else {
                // workaround in case the id sent by the POC is wrong
                // valid in Y2, where only one ServiceManager exists
                Object[] elements = agent.getManagedElements().values().toArray();
                if (elements != null)
                    serviceManager = (ManagedElement) elements[0];

                if (serviceManager != null)
                    element = serviceManager.getElement();
            }

            logger.info(String.format( "Invoking provision on Infrastructure Service Manager <%s>", serviceManager) );
            
            if (action instanceof InfrastructureTask) {
                ProvisionRequestType request = ((InfrastructureTask) action).getProvisionRequest();
                logger.debug("ProvisionRequestType: ");
                logger.debug(request);

                try {
                    response = ((Infrastructure) element).provision(request);
                    action.setStatus(TaskStatus.PROVISIONED_OK.toString());
                }
                catch (Exception e) {
                    e.printStackTrace();
                    action.setStatus(TaskStatus.PROVISIONING_FAILED.toString());
                }

                try {
                    logger.info("Response: ");
                    logger.info(response);

                    // SLARegistry registry = ProvisioningAndAdjustment.getSLAManagerContext().getSLARegistry();
                    SLAManagerContext context = ProvisioningAndAdjustment.getSLAManagerContext();
                    if (context != null) {
                        SLARegistry registry = context.getSLARegistry();
                        if (registry != null) {
                            UUID[] id = new UUID[1];
                            id[0] = new UUID(action.getSlaId());

                            logger.info("Retrieving SLA " + action.getSlaId() + " from registry");
                            SLA[] slas = registry.getIQuery().getSLA(id);

                            if (slas != null) {
                                logger.info("Got " + slas.length + " slas");

                                for (int i = 0; i < slas.length; i++) {
                                    SLA aux = slas[i];
                                    logger.debug(aux);
                                }
                                if (slas.length > 0) {
                                    SLA sla = slas[0];
                                    // OJO
                                    // ((InfrastructureSLA) sla).setProvisionId(response.getInfrastructureID());
                                    registry.getIRegister().update(id[0], sla, null, SLAState.OBSERVED);
                                }
                            }
                            else {
                                logger.info("ERROR: Got 0 SLAs from registry");

                            }
                        }
                        else {
                            logger.info("********ERROR : SLA REGISTRY IS NULL******");
                        }
                    }
                    else {
                        logger.info("*************** ERROR : CONTEXT IS NULL ********");
                    }
                }
                catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    // parent.notify(this);
                }
            }

            // trick while the ServiceManagers are still being developed: not all the tests will throw messages from the
            // Manageability Agents
            boolean wait = false;
            wait = (config.getValue(WAIT_MANAGEABILITY_AGENT_MESSAGE).equals(TRUE));
            if (wait) {
                // set the infrastructure id as status of this node, so when the MA message arrives, we can
                // match both node and message
                action.setStatus(response.getInfrastructureID());

                // wait for the ManageabilityAgentMessage to arrive
                try {
                    logger.debug("Waiting for the message from the ManageabilityAgent...");
                    synchronized (this) {
                        wait();
                    }
                }
                catch (InterruptedException e) {
                    logger.debug("Someone has interrupted my dreams...");
                }

                logger.debug("Awaiking...");
            }
            parent.notify(this);

        }
    }

}
