package org.slasoi.isslam.pac;

import org.slasoi.gslam.core.builder.ProvisioningAdjustmentBuilder;
import org.slasoi.gslam.core.pac.ProvisioningAdjustment;

public class InfrastructureProvisioningAdjustmentServices implements ProvisioningAdjustmentBuilder
{
    public ProvisioningAdjustment create()
    {
        return new InfrastructureProvisioningAdjustment();
    }
}
