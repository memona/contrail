package org.slasoi.isslam.pac;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.servicemanager.types.Host;

import com.thoughtworks.xstream.XStream;

public class HostMessageTranslator {
    private static final Logger logger = Logger.getLogger(HostMessageTranslator.class.getName());

    public HostMessageTranslator() {
        // just for RTTI purposes
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Host> fromXML(String messageStr) {
        logger.debug("HostMessageTranslator from XML");
        logger.debug(messageStr);

        XStream xstream = new XStream();
        xstream.alias(org.slasoi.infrastructure.servicemanager.types.Host.class.getSimpleName(),
                org.slasoi.infrastructure.servicemanager.types.Host.class);
        xstream.alias(org.slasoi.infrastructure.servicemanager.types.Host.STATE.class.getSimpleName(),
                org.slasoi.infrastructure.servicemanager.types.Host.STATE.class);

        Host message = new Host();
        Map<String, Host> mapHosts = new HashMap<String, Host>();

        mapHosts = (Map<String, Host>) xstream.fromXML(messageStr);

        return (mapHosts);

    }

}
