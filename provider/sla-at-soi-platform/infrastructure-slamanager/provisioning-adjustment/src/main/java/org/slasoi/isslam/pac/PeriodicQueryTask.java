/**
 * 
 */
package org.slasoi.isslam.pac;

import java.util.Map;

import org.drools.builder.ResourceType;
import org.slasoi.gslam.pac.AnalysisTask;
import org.slasoi.gslam.pac.ManagedElement;
import org.slasoi.gslam.pac.SharedKnowledgePlane;
import org.slasoi.gslam.pac.config.TaskConfiguration;
import org.slasoi.infrastructure.servicemanager.Infrastructure;
import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;
import org.slasoi.infrastructure.servicemanager.types.Host;
import org.slasoi.isslam.pac.types.ISMHost;
import org.slasoi.isslam.pac.types.Service;

/**
 * @author Beatriz
 * 
 */
public class PeriodicQueryTask extends AnalysisTask {

    private int period = 0;
    private int minutes = 0;

    private final String PERIOD = "PERIOD";
    private final String HOSTNAME_VM = "network";
    private final String METRIC_VM = "network";
    private final String CPU = "cpu";
    private final String MEMORY = "mem";
    private final int MINUTES_VM = 0;
    private final String MINUTES_MONITORING = "MINUTES";
    private final int MILLISECONDS = 1000;
    
    public PeriodicQueryTask(String slamId) {
    	super(slamId);
    }

    public void configure(TaskConfiguration config) {
        logger.debug("PeriodicQueryTask::configure");

        rulesFile = config.getValue(RULES_FILE);

        if (rulesFile != null) {
            logger.debug("Configuring rules file: " + rulesFile);
            SharedKnowledgePlane.getInstance("IPAC").addRulesFile(rulesFile, ResourceType.DRL);
        }

        String minutesStr = config.getValue(MINUTES_MONITORING);
        if (minutesStr != null)
            minutes = new Integer(minutesStr).intValue();

        String periodStr = config.getValue(PERIOD);
        if (periodStr != null)
            period = new Integer(periodStr).intValue() * MILLISECONDS; // period in seconds
    }

    public void run() {
        logger.debug("PeriodicQueryTask running...");
        while (running) {
            try {
                Thread.sleep(period);

                // Send http request
                Object[] managedElements = agent.getManagedElements().values().toArray();

                for (Object managedElement : managedElements) {
                    Object serviceManager = ((ManagedElement) managedElement).getElement();
                    String serviceManagerId = ((ManagedElement) managedElement).getId();
                    String vmListResponse =
                            ((Infrastructure) serviceManager).predictionQuery(HOSTNAME_VM, METRIC_VM, MINUTES_VM);
                    logger.debug("Http response: \n" + vmListResponse);

                    if (vmListResponse != null) {
                        Map<String, Host> mapHosts = HostMessageTranslator.fromXML(vmListResponse);

                        logger.debug("Number of hosts: " + mapHosts.size());
                        for (String key : mapHosts.keySet()) {
                            logger.debug("Key " + key);
                            Host host = mapHosts.get(key);
                            logger.debug("Host " + host);
                            ISMHost ismHost = new ISMHost(host, serviceManagerId);
                            SharedKnowledgePlane.getInstance("IPAC").getStatefulSession().insert(ismHost);

                            if (host.getState() == Host.STATE.UP) {
                                // Request monitoring data
                                String hostname = host.getId();
                                hostname = hostname.replaceAll(":", "").toLowerCase();
                                String cpuMonitoring =
                                        ((Infrastructure) serviceManager).predictionQuery(hostname, CPU, minutes);
                                logger.debug("CPU monitoring response: " + cpuMonitoring);

                                Service cpuData = Service.fromXML(cpuMonitoring);

                                String memMonitoring =
                                        ((Infrastructure) serviceManager).predictionQuery(hostname, MEMORY, minutes);
                                logger.debug("Memory monitoring response: " + memMonitoring);

                                Service memData = Service.fromXML(memMonitoring);
                            }
                        }
                    }
                    SharedKnowledgePlane.getInstance("IPAC").getStatefulSession().fireAllRules();
                }

            }
            catch (InterruptedException e) {
                logger.debug("PeriodicQueryTask. InterruptedException");
                stop();
                return;
            }
            catch (UnknownIdException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
