package org.slasoi.isslam.pac.mockups;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pubsub.Channel;
import org.slasoi.common.messaging.pubsub.PubSubFactory;
import org.slasoi.common.messaging.pubsub.PubSubManager;
import org.slasoi.common.messaging.pubsub.PubSubMessage;
import org.slasoi.gslam.pac.events.ManageabilityAgentMessage;

public class ManageabilityAgentMockup implements Runnable {

    private static final Logger logger = Logger.getLogger(ManageabilityAgentMockup.class.getName());
    private String messageId = null;
    private PubSubManager pubSubManager = null;
    private String channel = null;

    public ManageabilityAgentMockup(String id, String eventBusPropertiesFile, String channel) {
        this.messageId = id;
        this.channel = channel;
        configureMessaging(eventBusPropertiesFile, channel);
    }

    public void run() {
        // Wait a random amount of time
        // First simulation: sleep a little bit (between 0 and 10 seconds)
        int responseTime = (int) (Math.random() * 10000);

        logger.info(" going to sleep for " + responseTime + " milliseconds");

        try {
            Thread.sleep(responseTime);
        }
        catch (InterruptedException e) {
            logger.debug(" sleeping thread interrupted.");
        }

        // Build the message
        ManageabilityAgentMessage message = createMessage();

        // Send the message through the event bus
        PubSubMessage pubsubMessage = new PubSubMessage(channel, message.toXML());
        try {
            pubSubManager.publish(pubsubMessage);
        }
        catch (MessagingException e) {
            logger.debug("Error when publishing a ManageabilityAgent message");
            e.printStackTrace();
        }

    }

    private void configureMessaging(String propertiesFile, String channel) {

        // Create manager instance
        try {
            pubSubManager = PubSubFactory.createPubSubManager(propertiesFile);
        }
        catch (MessagingException e) {
            logger.error("PubSubManager could not be created" + e.getMessage());
            return;
        }
        catch (FileNotFoundException e) {
            logger.error("Properties file " + propertiesFile + " not found");
        }
        catch (IOException e) {
            logger.error("EventBusHandlingTask, IO error in configureMessaging");
            logger.error(e.getMessage());
        }

        logger.debug("EventBusHandlingTask::configureMessaging.PubSubManager created");

        // Channel creation
        try {
            if (!pubSubManager.isChannel(channel)) {
                pubSubManager.createChannel(new Channel(channel));
                logger.debug("Channel " + channel + " created");
            }
        }
        catch (MessagingException e) {
            logger.error("Error subscribing to channel " + channel);
            logger.error(e.getMessage());
        }

    }

    private ManageabilityAgentMessage createMessage() {
        logger.info("Creating new ManageabilityAgentMessage...");

        ManageabilityAgentMessage message = new ManageabilityAgentMessage();
        message.setTimestamp(System.currentTimeMillis());
        message.setEventType("Provision_Event");
        message.setValue("Id", messageId);
        message.setValue("ProvisioningStatus", "READY");

        logger.debug(message);

        return message;
    }

    public void start() {
        run();
    }

}
