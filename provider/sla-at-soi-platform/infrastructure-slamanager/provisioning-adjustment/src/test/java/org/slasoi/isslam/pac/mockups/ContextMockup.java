/**
 * 
 */
package org.slasoi.isslam.pac.mockups;

import org.slasoi.gslam.core.context.SLAManagerContextAdapter;
import org.slasoi.gslam.core.negotiation.SLARegistry;

/**
 * @author Beatriz
 * 
 */
public class ContextMockup extends SLAManagerContextAdapter {

    SLARegistryMockup slaRegistry;

    public SLARegistryMockup getSlaRegistry() {
        return slaRegistry;
    }

    public void setSlaRegistry(SLARegistryMockup slaRegistry) {
        this.slaRegistry = slaRegistry;
    }

    public SLARegistry getSLARegistry() throws SLAManagerContextException {
        return slaRegistry;
    }

}
