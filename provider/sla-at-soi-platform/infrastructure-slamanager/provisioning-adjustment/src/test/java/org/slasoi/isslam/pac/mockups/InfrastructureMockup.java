package org.slasoi.isslam.pac.mockups;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.infrastructure.servicemanager.Infrastructure;
import org.slasoi.infrastructure.servicemanager.exceptions.DescriptorException;
import org.slasoi.infrastructure.servicemanager.exceptions.UnknownIdException;
import org.slasoi.infrastructure.servicemanager.registry.Registry;
import org.slasoi.infrastructure.servicemanager.types.CapacityResponseType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionRequestType;
import org.slasoi.infrastructure.servicemanager.types.ProvisionResponseType;
import org.slasoi.infrastructure.servicemanager.types.ReservationResponseType;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;

import eu.slasoi.infrastructure.model.Category;
import eu.slasoi.infrastructure.model.infrastructure.Compute;

public  class InfrastructureMockup implements Infrastructure {
    private static final Logger logger = Logger.getLogger(InfrastructureMockup.class.getName());

    private boolean provisionCalled = false;
    private boolean commitCalled = false;
    private boolean releaseCalled = false;

    private String eventBusProperties = null;
    private String channel = null;

    // just for testing purposes, this method is not implemented by the real ISM
    public void setEventBusProperties(String eventBusProperties, String channel) {
        this.eventBusProperties = eventBusProperties;
        this.channel = channel;
    }

    public ReservationResponseType commit(String arg0) throws UnknownIdException {
        logger.info("Infrastructure Service Manager, commit...");
        logger.info(arg0);
        setCommitCalled(true);
        return null;
    }
    
    public ProvisionResponseType provision(ProvisionRequestType request) throws DescriptorException {
        logger.info("Infrastructure Service Manager, provision...");
        logger.info("ProvisionRequestType: ");
        setProvisionCalled(true);
        // First simulation: sleep a little bit (between 0 and 10 seconds)
        int responseTime = (int) (Math.random() * 10000);

        logger.info(" going to sleep for " + responseTime + " milliseconds");

        try {
            Thread.sleep(responseTime);
        }
        catch (InterruptedException e) {
            logger.debug(" sleeping thread interrupted.");
        }

        ProvisionResponseType response = new ProvisionResponseType();
        String infrastructureId = java.util.UUID.randomUUID().toString();
        response.setInfrastructureID(infrastructureId);

        // ManageabilityAgent should now send a message to the event bus
        ManageabilityAgentMockup managAgent =
                new ManageabilityAgentMockup(infrastructureId, eventBusProperties, channel);
        new Thread(managAgent).start();

        return response;
    }

    public void release(String arg0) throws UnknownIdException {
        logger.info("Infrastructure Service Manager, release...");
        logger.info(arg0);
        setReleaseCalled(true);
    }

    public ProvisionResponseType getDetails(String arg0) throws UnknownIdException {
        // TODO Auto-generated method stub
        return null;
    }

    public ProvisionResponseType reprovision(String arg0, ProvisionRequestType arg1) throws DescriptorException,
            UnknownIdException {
        // TODO Auto-generated method stub
        return null;
    }

    public void startResource(String arg0) throws UnknownIdException {
        // TODO Auto-generated method stub

    }

    public void stop(String arg0) throws UnknownIdException {
        // TODO Auto-generated method stub

    }

    public void stopResource(String arg0) throws UnknownIdException {
        // TODO Auto-generated method stub

    }

    public String query(String id, String metricName, int minutes) throws UnknownIdException {
        // TODO Auto-generated method stub
        return null;
    }

    public ProvisionRequestType query(ProvisionRequestType arg0) throws DescriptorException {
        // TODO Auto-generated method stub
        return null;
    }

    public ReservationResponseType reserve(ProvisionRequestType arg0) throws DescriptorException {
        // TODO Auto-generated method stub
        return null;
    }

    public ProvisionRequestType createProvisionRequestType(String arg0, String arg1, String arg2, int arg3, int arg4,
            String arg5, String arg6) {
        // TODO Auto-generated method stub
        return null;
    }

    public void setProvisionCalled(boolean provisionCalled) {
        this.provisionCalled = provisionCalled;
    }

    public boolean isProvisionCalled() {
        return provisionCalled;
    }

    public void setCommitCalled(boolean commitCalled) {
        this.commitCalled = commitCalled;
    }

    public boolean isCommitCalled() {
        return commitCalled;
    }

    public void setReleaseCalled(boolean releaseCalled) {
        this.releaseCalled = releaseCalled;
    }

    public boolean isReleaseCalled() {
        return releaseCalled;
    }

    public String predictionQuery(String arg0, String arg1, int arg2) throws UnknownIdException {
        // TODO Auto-generated method stub
        return null;
    }

    public ProvisionRequestType createProvisionRequestType(String arg0, String arg1, String arg2, int arg3, int arg4,
            int arg5, String arg6, String arg7, String arg8) {
        // TODO Auto-generated method stub
        return null;
    }

    public Compute createComputeConfiguration(String arg0, String arg1, String arg2, int arg3, int arg4, int arg5,
            String arg6, String arg7, String arg8) {
        // TODO Auto-generated method stub
        return null;
    }
    
    public Compute createComputeConfiguration( String arg0, String arg1, String arg2, int arg3, float arg4, int arg5, String arg6, String arg7, String arg8, Hashtable<String, String> arg9 )
    {
        // TODO Auto-generated method stub
        return null;
    }

    public ProvisionRequestType createProvisionRequestType(Set<Compute> arg0, String arg1) {
        // TODO Auto-generated method stub
        return null;
    }

    public Compute createComputeConfiguration(String arg0, String arg1, String arg2, int arg3, float arg4, int arg5,
            String arg6, String arg7, String arg8) {
        // TODO Auto-generated method stub
        return null;
    }

    public ProvisionRequestType createProvisionRequestType(String arg0, String arg1, String arg2, int arg3, float arg4,
            int arg5, String arg6, String arg7, String arg8) {
        // TODO Auto-generated method stub
        return null;
    }

    public Compute createComputeConfiguration(String arg0, String arg1, String arg2, int arg3, float arg4, int arg5,
            String arg6, String arg7, String arg8, Set<Category> arg9) {
        // TODO Auto-generated method stub
        return null;
    }

    public Compute createComputeConfiguration(String arg0, String arg1, String arg2, int arg3, float arg4, int arg5,
            MonitoringSystemConfiguration arg6, String arg7, String arg8, Set<Category> arg9) {
        // TODO Auto-generated method stub
        return null;
    }

	public ComponentMonitoringFeatures[] getMonitoringCapabilities(String arg0,
			String[] arg1) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public ComponentMonitoringFeatures[] getMonitoringFeatures( int arg0 ) throws Exception
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Compute createComputeConfiguration( String arg0, String arg1, String arg2, Hashtable<String, String> arg3 )
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Compute createComputeConfiguration( int arg0, float arg1, int arg2, String arg3, String arg4, Hashtable<String, String> arg5 )
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ProvisionRequestType createProvisionRequestType( String arg0, Set<Compute> arg1 )
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ProvisionRequestType createProvisionRequestType( String arg0, String arg1, String arg2, String arg3, String arg4 )
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Registry getImageregistry()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Registry getLocregistry()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Registry getMetricregistry()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Registry getOsregistry()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public HashSet<String> listServicesOrResources( String arg0 )
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<CapacityResponseType> queryCapacity() throws DescriptorException
    {
        // TODO Auto-generated method stub
        return null;
    }
}
