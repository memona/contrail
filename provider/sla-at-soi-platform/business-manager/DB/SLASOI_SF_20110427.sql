CREATE DATABASE  IF NOT EXISTS `slasoi` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `slasoi`;

-- MySQL dump 10.11
--
-- Host: emarketplace2    Database: SLASOI
-- ------------------------------------------------------
-- Server version	5.0.83-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `EM_ACCOUNT_EVENT`
--

DROP TABLE IF EXISTS `EM_ACCOUNT_EVENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_ACCOUNT_EVENT` (
  `NU_EVENTID` bigint(20) NOT NULL,
  `NU_EVENTID_PAYMENT` bigint(20) default NULL,
  `TC_TYPE` char(1) NOT NULL,
  `NU_PARTYID_VENDOR` bigint(20) default NULL,
  `NU_PARTYID_PAYER` bigint(20) NOT NULL default '0',
  `NU_BANK_ID` bigint(20) default NULL,
  `NU_EVENT_TYPEID` bigint(20) NOT NULL,
  `NU_CURRENCY_ID` bigint(20) NOT NULL,
  `TS_TIMESTAMP` datetime default NULL,
  `TX_SLAID` varchar(255) default NULL,
  `TX_VIOLATIONID` varchar(255) default NULL,
  `NU_AMOUNT` decimal(19,2) NOT NULL,
  `TX_DESCRIPTION` varchar(255) default NULL,
  PRIMARY KEY  (`NU_EVENTID`),
  KEY `FK74D60A711631AA4F` (`NU_EVENTID_PAYMENT`),
  KEY `FK74D60A711B473B2A` (`NU_BANK_ID`),
  KEY `FK74D60A71871FFC37` (`NU_EVENT_TYPEID`),
  KEY `FK74D60A71657FF173` (`NU_BANKID`),
  KEY `FK74D60A71E2C44514` (`NU_PARTYID_VENDOR`),
  KEY `FK74D60A717D154275` (`NU_PARTYID_PAYER`),
  KEY `FK74D60A71905E3410` (`NU_CURRENCY_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_ACCOUNT_EVENT`
--

LOCK TABLES `EM_ACCOUNT_EVENT` WRITE;
/*!40000 ALTER TABLE `EM_ACCOUNT_EVENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_ACCOUNT_EVENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_ACCOUNT_EVENT_TYPE`
--

DROP TABLE IF EXISTS `EM_ACCOUNT_EVENT_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_ACCOUNT_EVENT_TYPE` (
  `NU_EVENT_TYPEID` bigint(20) NOT NULL,
  `TX_NAME` varchar(255) default NULL,
  `TX_DESCRIPTION` varchar(255) default NULL,
  PRIMARY KEY  (`NU_EVENT_TYPEID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_ACCOUNT_EVENT_TYPE`
--

LOCK TABLES `EM_ACCOUNT_EVENT_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_ACCOUNT_EVENT_TYPE` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_ACCOUNT_EVENT_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_AREAS_COUNTRIES`
--

DROP TABLE IF EXISTS `EM_AREAS_COUNTRIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_AREAS_COUNTRIES` (
  `NU_AREA_ID` bigint(20) NOT NULL default '0',
  `NU_COUNTRY` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_AREA_ID`,`NU_COUNTRY`),
  KEY `FK3ED5F564F3037D69` (`NU_COUNTRY`),
  KEY `FK3ED5F5641A968BF0` (`NU_AREA_ID`),
  CONSTRAINT `EM_AREAS_COUNTRIES_ibfk_1` FOREIGN KEY (`NU_AREA_ID`) REFERENCES `EM_GEOGRAPHICAL_AREAS` (`NU_AREA_ID`),
  CONSTRAINT `FK3ED5F564F3037D69` FOREIGN KEY (`NU_COUNTRY`) REFERENCES `EM_COUNTRIES` (`NU_COUNTRY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_AREAS_COUNTRIES`
--

LOCK TABLES `EM_AREAS_COUNTRIES` WRITE;
/*!40000 ALTER TABLE `EM_AREAS_COUNTRIES` DISABLE KEYS */;
INSERT INTO `EM_AREAS_COUNTRIES` VALUES (2,1),(3,1),(1,2),(3,2),(3,3),(3,6),(3,7);
/*!40000 ALTER TABLE `EM_AREAS_COUNTRIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_ATTRIBUTES`
--

DROP TABLE IF EXISTS `EM_ATTRIBUTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_ATTRIBUTES` (
  `NU_ATTRIBUTEID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(40) NOT NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `NU_MIN_VALUE` bigint(20) default NULL,
  `NU_MAX_VALUE` bigint(20) default NULL,
  PRIMARY KEY  (`NU_ATTRIBUTEID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_ATTRIBUTES`
--

LOCK TABLES `EM_ATTRIBUTES` WRITE;
/*!40000 ALTER TABLE `EM_ATTRIBUTES` DISABLE KEYS */;
INSERT INTO `EM_ATTRIBUTES` VALUES (1,'Response Time',NULL,1,5),(2,'Fiability','',1,10),(3,'Quality','',1,5),(4,'SERVICE_NAME','',1,1);
/*!40000 ALTER TABLE `EM_ATTRIBUTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_BILLING_FRECUENCY`
--

DROP TABLE IF EXISTS `EM_BILLING_FRECUENCY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_BILLING_FRECUENCY` (
  `NU_BILLING_FRECUENCY_ID` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(40) default NULL,
  `TX_NAME` varchar(15) default NULL,
  PRIMARY KEY  (`NU_BILLING_FRECUENCY_ID`),
  UNIQUE KEY `NU_BILLING_FRECUENCY_ID` (`NU_BILLING_FRECUENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_BILLING_FRECUENCY`
--

LOCK TABLES `EM_BILLING_FRECUENCY` WRITE;
/*!40000 ALTER TABLE `EM_BILLING_FRECUENCY` DISABLE KEYS */;
INSERT INTO `EM_BILLING_FRECUENCY` VALUES (1,'Month Billing','per_month'),(3,'Once every request','per_request'),(4,'Once a year','per_year'),(6,'Daily','per_day');
/*!40000 ALTER TABLE `EM_BILLING_FRECUENCY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CHARACTERISTIC`
--

DROP TABLE IF EXISTS `EM_CHARACTERISTIC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CHARACTERISTIC` (
  `NU_CHARACTERISTIC_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(20) default NULL,
  `TX_DESCRIPTION` varchar(40) default NULL,
  `NU_CHARACT_TYPEID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_CHARACTERISTIC_ID`),
  KEY `NU_CHARACT_TYPEID` (`NU_CHARACT_TYPEID`),
  CONSTRAINT `EM_CHARACTERISTIC_ibfk_1` FOREIGN KEY (`NU_CHARACT_TYPEID`) REFERENCES `EM_CHARACTERISTIC_TYPE` (`NU_CHARACT_TYPEID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CHARACTERISTIC`
--

LOCK TABLES `EM_CHARACTERISTIC` WRITE;
/*!40000 ALTER TABLE `EM_CHARACTERISTIC` DISABLE KEYS */;
INSERT INTO `EM_CHARACTERISTIC` VALUES (1,'ENDPOINT',NULL,1),(2,'RESPONSE TIME','RESPONSE TIME DESCRIPTION',2),(3,'SERVICE NAME QP','',2),(4,'SERVICE NAME UP','',3),(5,'SERVICE NAME','',1);
/*!40000 ALTER TABLE `EM_CHARACTERISTIC` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CHARACTERISTIC_TYPE`
--

DROP TABLE IF EXISTS `EM_CHARACTERISTIC_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CHARACTERISTIC_TYPE` (
  `NU_CHARACT_TYPEID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(20) default NULL,
  PRIMARY KEY  (`NU_CHARACT_TYPEID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CHARACTERISTIC_TYPE`
--

LOCK TABLES `EM_CHARACTERISTIC_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_CHARACTERISTIC_TYPE` DISABLE KEYS */;
INSERT INTO `EM_CHARACTERISTIC_TYPE` VALUES (1,'URL'),(2,'QUALITY PARAMETER'),(3,'USAGE PARAMETER');
/*!40000 ALTER TABLE `EM_CHARACTERISTIC_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_COMPONENT_PRICE`
--

DROP TABLE IF EXISTS `EM_COMPONENT_PRICE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_COMPONENT_PRICE` (
  `NU_IDCOMPONENTPRICE` bigint(20) NOT NULL auto_increment,
  `NU_IDPRODUCTOFFERING` bigint(20) NOT NULL,
  `NU_PRICE` decimal(12,4) NOT NULL default '0.0000',
  `NU_QUANTITY` bigint(20) NOT NULL,
  `DT_VALID_FROM` date default NULL,
  `DT_VALID_TO` date default NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(40) NOT NULL,
  `NU_CURRENCY_ID` bigint(20) NOT NULL,
  `NU_IDPRICE_TYPE` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_IDCOMPONENTPRICE`),
  KEY `FK508C7DB0F67450FD` (`NU_IDPRICE_TYPE`),
  KEY `FK508C7DB0E72C5B49` (`NU_CURRENCY_ID`),
  KEY `NU_IDPRODUCTOFFERING` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `EM_COMPONENT_PRICE_ibfk_1` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK508C7DB0E72C5B49` FOREIGN KEY (`NU_CURRENCY_ID`) REFERENCES `EM_CURRENCIES` (`NU_CURRENCY_ID`),
  CONSTRAINT `FK508C7DB0F67450FD` FOREIGN KEY (`NU_IDPRICE_TYPE`) REFERENCES `EM_PRICE_TYPE` (`NU_IDPRICE_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_COMPONENT_PRICE`
--

LOCK TABLES `EM_COMPONENT_PRICE` WRITE;
/*!40000 ALTER TABLE `EM_COMPONENT_PRICE` DISABLE KEYS */;
INSERT INTO `EM_COMPONENT_PRICE` VALUES (103,61,'5.0000',1,'2010-11-12','2012-12-31','Subcription','Subcription',1,1),(104,62,'3.0000',1,'2010-11-12','2012-12-31','Subcription','Subcription',1,1),(105,63,'8.0000',1,'2010-11-12','2012-12-31','Subcription','Subcription',1,1);
/*!40000 ALTER TABLE `EM_COMPONENT_PRICE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_COUNTRIES`
--

DROP TABLE IF EXISTS `EM_COUNTRIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_COUNTRIES` (
  `NU_COUNTRY` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(40) default NULL,
  `NU_CURRENCY_ID` bigint(20) default NULL,
  `NU_LANGUAGE` bigint(20) default NULL,
  PRIMARY KEY  (`NU_COUNTRY`),
  UNIQUE KEY `NU_COUNTRY` (`NU_COUNTRY`),
  KEY `FK79A3FBBDC174AC82` (`NU_LANGUAGE`),
  KEY `FK79A3FBBDE72C5B49` (`NU_CURRENCY_ID`),
  CONSTRAINT `FK79A3FBBDC174AC82` FOREIGN KEY (`NU_LANGUAGE`) REFERENCES `EM_LANGUAGES` (`NU_LANGUAGE`),
  CONSTRAINT `FK79A3FBBDE72C5B49` FOREIGN KEY (`NU_CURRENCY_ID`) REFERENCES `EM_CURRENCIES` (`NU_CURRENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_COUNTRIES`
--

LOCK TABLES `EM_COUNTRIES` WRITE;
/*!40000 ALTER TABLE `EM_COUNTRIES` DISABLE KEYS */;
INSERT INTO `EM_COUNTRIES` VALUES (1,'Spain',1,1),(2,'United Kingdom',2,2),(3,'France',1,3),(4,'Japan',5,6),(6,'Italy',1,5),(7,'Belgium',1,9),(8,'Austria',1,16);
/*!40000 ALTER TABLE `EM_COUNTRIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CURRENCIES`
--

DROP TABLE IF EXISTS `EM_CURRENCIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CURRENCIES` (
  `NU_CURRENCY_ID` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`NU_CURRENCY_ID`),
  UNIQUE KEY `NU_CURRENCY_ID` (`NU_CURRENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CURRENCIES`
--

LOCK TABLES `EM_CURRENCIES` WRITE;
/*!40000 ALTER TABLE `EM_CURRENCIES` DISABLE KEYS */;
INSERT INTO `EM_CURRENCIES` VALUES (1,'Euro','EUR'),(2,'United Kingdom, Pounds','GBP'),(3,'United States of America, Dollars','USD'),(5,'Japan, Yen','JPY'),(11,'United Arab Emirates, Dirhams','AED'),(12,'Afghanistan, Afghanis','AFN'),(13,'Albania, Leke','ALL'),(14,'Armenia, Drams','AMD'),(15,'Netherlands Antilles, Guilders (also called Florins)','ANG'),(16,'Angola, Kwanza','AOA'),(17,'Argentina, Pesos','ARS'),(18,'Australia, Dollars','AUD'),(19,'Aruba, Guilders (also called Florins)','AWG'),(20,'Azerbaijan, New Manats','AZN'),(21,'Bosnia and Herzegovina, Convertible Marka','BAM'),(22,'Barbados, Dollars','BBD'),(23,'Bangladesh, Taka','BDT'),(24,'Bulgaria, Leva','BGN'),(25,'Bahrain, Dinars','BHD'),(26,'Burundi, Francs','BIF'),(27,'Bermuda, Dollars','BMD'),(28,'Brunei Darussalam, Dollars','BND'),(29,'Bolivia, Bolivianos','BOB'),(30,'Brazil, Brazil Real','BRL'),(31,'Bahamas, Dollars','BSD'),(32,'Bhutan, Ngultrum','BTN'),(33,'Botswana, Pulas','BWP'),(34,'Belarus, Rubles','BYR'),(35,'Belize, Dollars','BZD'),(36,'Canada, Dollars','CAD'),(37,'Congo/Kinshasa, Congolese Francs','CDF'),(38,'Switzerland, Francs','CHF'),(39,'Chile, Pesos','CLP'),(40,'China, Yuan Renminbi','CNY'),(41,'Colombia, Pesos','COP'),(42,'Costa Rica, Colones','CRC'),(43,'Cuba, Pesos','CUP'),(44,'Cape Verde, Escudos','CVE'),(45,'Cyprus, Pounds (expires 2008-Jan-31)','CYP'),(46,'Czech Republic, Koruny','CZK'),(47,'Djibouti, Francs','DJF'),(48,'Denmark, Kroner','DKK'),(49,'Dominican Republic, Pesos','DOP'),(50,'Algeria, Algeria Dinars','DZD'),(51,'Estonia, Krooni','EEK'),(52,'Egypt, Pounds','EGP'),(53,'Eritrea, Nakfa','ERN'),(54,'Ethiopia, Birr','ETB'),(55,'Fiji, Dollars','FJD'),(56,'Falkland Islands (Malvinas), Pounds','FKP'),(57,'Georgia, Lari','GEL'),(58,'Guernsey, Pounds','GGP'),(59,'Ghana, Cedis','GHS'),(60,'Gibraltar, Pounds','GIP'),(61,'Gambia, Dalasi','GMD'),(62,'Guinea, Francs','GNF'),(63,'Guatemala, Quetzales','GTQ'),(64,'Guyana, Dollars','GYD'),(65,'Hong Kong, Dollars','HKD'),(66,'Honduras, Lempiras','HNL'),(67,'Croatia, Kuna','HRK'),(68,'Haiti, Gourdes','HTG'),(69,'Hungary, Forint','HUF'),(70,'Indonesia, Rupiahs','IDR'),(71,'Israel, New Shekels','ILS'),(72,'Isle of Man, Pounds','IMP'),(73,'India, Rupees','INR'),(74,'Iraq, Dinars','IQD'),(75,'Iran, Rials','IRR'),(76,'Iceland, Kronur','ISK'),(77,'Jersey, Pounds','JEP'),(78,'Jamaica, Dollars','JMD'),(79,'Jordan, Dinars','JOD'),(80,'Kenya, Shillings','KES'),(81,'Kyrgyzstan, Soms','KGS'),(82,'Cambodia, Riels','KHR'),(83,'Comoros, Francs','KMF'),(84,'Korea (North), Won','KPW'),(85,'Korea (South), Won','KRW'),(86,'Kuwait, Dinars','KWD'),(87,'Cayman Islands, Dollars','KYD'),(88,'Kazakhstan, Tenge','KZT'),(89,'Laos, Kips','LAK'),(90,'Lebanon, Pounds','LBP'),(91,'Sri Lanka, Rupees','LKR'),(92,'Liberia, Dollars','LRD'),(93,'Lesotho, Maloti','LSL'),(94,'Lithuania, Litai','LTL'),(95,'Latvia, Lati','LVL'),(96,'Libya, Dinars','LYD'),(97,'Morocco, Dirhams','MAD'),(98,'Moldova, Lei','MDL'),(99,'Madagascar, Ariary','MGA'),(100,'Macedonia, Denars','MKD'),(101,'Myanmar (Burma), Kyats','MMK'),(102,'Mongolia, Tugriks','MNT'),(103,'Macau, Patacas','MOP'),(104,'Mauritania, Ouguiyas','MRO'),(105,'Malta, Liri (expires 2008-Jan-31)','MTL'),(106,'Mauritius, Rupees','MUR'),(107,'Maldives (Maldive Islands), Rufiyaa','MVR'),(108,'Malawi, Kwachas','MWK'),(109,'Mexico, Pesos','MXN'),(110,'Malaysia, Ringgits','MYR'),(111,'Mozambique, Meticais','MZN'),(112,'Namibia, Dollars','NAD'),(113,'Nigeria, Nairas','NGN'),(114,'Nicaragua, Cordobas','NIO'),(115,'Norway, Krone','NOK'),(116,'Nepal, Nepal Rupees','NPR'),(117,'New Zealand, Dollars','NZD'),(118,'Oman, Rials','OMR'),(119,'Panama, Balboa','PAB'),(120,'Peru, Nuevos Soles','PEN'),(121,'Papua New Guinea, Kina','PGK'),(122,'Philippines, Pesos','PHP'),(123,'Pakistan, Rupees','PKR'),(124,'Poland, Zlotych','PLN'),(125,'Paraguay, Guarani','PYG'),(126,'Qatar, Rials','QAR'),(127,'Romania, New Lei','RON'),(128,'Serbia, Dinars','RSD'),(129,'Russia, Rubles','RUB'),(130,'Rwanda, Rwanda Francs','RWF'),(131,'Saudi Arabia, Riyals','SAR'),(132,'Solomon Islands, Dollars','SBD'),(133,'Seychelles, Rupees','SCR'),(134,'Sudan, Pounds','SDG'),(135,'Sweden, Kronor','SEK'),(136,'Singapore, Dollars','SGD'),(137,'Saint Helena, Pounds','SHP'),(138,'Sierra Leone, Leones','SLL'),(139,'Somalia, Shillings','SOS'),(140,'Seborga, Luigini','SPL'),(141,'Suriname, Dollars','SRD'),(142,'São Tome and Principe, Dobras','STD'),(143,'El Salvador, Colones','SVC'),(144,'Syria, Pounds','SYP'),(145,'Swaziland, Emalangeni','SZL'),(146,'Thailand, Baht','THB'),(147,'Tajikistan, Somoni','TJS'),(148,'Turkmenistan, Manats','TMM'),(149,'Tunisia, Dinars','TND'),(150,'Tonga, Pa\'anga','TOP'),(151,'Turkey, New Lira','TRY'),(152,'Trinidad and Tobago, Dollars','TTD'),(153,'Tuvalu, Tuvalu Dollars','TVD'),(154,'Taiwan, New Dollars','TWD'),(155,'Tanzania, Shillings','TZS'),(156,'Ukraine, Hryvnia','UAH'),(157,'Uganda, Shillings','UGX'),(158,'Uruguay, Pesos','UYU'),(159,'Uzbekistan, Sums','UZS'),(160,'Venezuela, Bolivares (expires 2008-Jun-30)','VEB'),(161,'Venezuela, Bolivares Fuertes','VEF'),(162,'Viet Nam, Dong','VND'),(163,'Vanuatu, Vatu','VUV'),(164,'Samoa, Tala','WST'),(165,'Communauté Financière Africaine BEAC, Francs','XAF'),(166,'Silver, Ounces','XAG'),(167,'Gold, Ounces','XAU'),(168,'East Caribbean Dollars','XCD'),(169,'International Monetary Fund (IMF) Special Drawing Rights','XDR'),(170,'Communauté Financière Africaine BCEAO, Francs','XOF'),(171,'Palladium Ounces','XPD'),(172,'Comptoirs Français du Pacifique Francs','XPF'),(173,'Platinum, Ounces','XPT'),(174,'Yemen, Rials','YER'),(175,'South Africa, Rand','ZAR'),(176,'Zambia, Kwacha','ZMK'),(177,'Zimbabwe, Zimbabwe Dollars','ZWD');
/*!40000 ALTER TABLE `EM_CURRENCIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CURRENCY_CHANGES`
--

DROP TABLE IF EXISTS `EM_CURRENCY_CHANGES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CURRENCY_CHANGES` (
  `DT_FROM` datetime NOT NULL,
  `DT_TO` datetime NOT NULL,
  `NU_CURRENCY_ID` bigint(20) NOT NULL,
  `NU_CURRENCY_ID_DEST` bigint(20) NOT NULL,
  `NU_CONVERSION_FACTOR` bigint(20) NOT NULL,
  PRIMARY KEY  (`DT_FROM`,`DT_TO`,`NU_CURRENCY_ID`,`NU_CURRENCY_ID_DEST`),
  KEY `FKEADA932C95F97A48` (`NU_CURRENCY_ID_DEST`),
  KEY `FKEADA932CE72C5B49` (`NU_CURRENCY_ID`),
  CONSTRAINT `FKEADA932C95F97A48` FOREIGN KEY (`NU_CURRENCY_ID_DEST`) REFERENCES `EM_CURRENCIES` (`NU_CURRENCY_ID`),
  CONSTRAINT `FKEADA932CE72C5B49` FOREIGN KEY (`NU_CURRENCY_ID`) REFERENCES `EM_CURRENCIES` (`NU_CURRENCY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CURRENCY_CHANGES`
--

LOCK TABLES `EM_CURRENCY_CHANGES` WRITE;
/*!40000 ALTER TABLE `EM_CURRENCY_CHANGES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_CURRENCY_CHANGES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CUSTOMERS_PRODUCTS`
--

DROP TABLE IF EXISTS `EM_CUSTOMERS_PRODUCTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CUSTOMERS_PRODUCTS` (
  `TX_BSLAID` varchar(100) NOT NULL default '',
  `DT_DATE_BEGIN` datetime NOT NULL default '0000-00-00 00:00:00',
  `DT_DATE_END` datetime default NULL,
  `DT_INSERT_DATE` datetime NOT NULL default '0000-00-00 00:00:00',
  `NU_PARTYID` bigint(20) default NULL,
  `NU_IDPRODUCTOFFERING` bigint(20) default NULL,
  `STATE` varchar(10) NOT NULL default '',
  `TERMINATION_REASON` varchar(40) default NULL,
  PRIMARY KEY  USING BTREE (`TX_BSLAID`),
  UNIQUE KEY `NU_PROVISIONID` USING BTREE (`TX_BSLAID`),
  KEY `FK9D043F25B47E1006` (`NU_PARTYID`),
  KEY `FK9D043F252E546354` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `FK9D043F252E546354` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `FK9D043F25B47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CUSTOMERS_PRODUCTS`
--

LOCK TABLES `EM_CUSTOMERS_PRODUCTS` WRITE;
/*!40000 ALTER TABLE `EM_CUSTOMERS_PRODUCTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_CUSTOMERS_PRODUCTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_CUSTPRODS_SLAS`
--

DROP TABLE IF EXISTS `EM_CUSTPRODS_SLAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_CUSTPRODS_SLAS` (
  `TX_BSLAID` varchar(100) NOT NULL default '',
  `TX_SLAID` varchar(100) NOT NULL default '',
  `NU_SERVICEID` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`TX_BSLAID`,`TX_SLAID`),
  KEY `NU_SERVICEID` (`NU_SERVICEID`),
  CONSTRAINT `EM_CUSTPRODS_SLAS_ibfk_1` FOREIGN KEY (`TX_BSLAID`) REFERENCES `EM_CUSTOMERS_PRODUCTS` (`TX_BSLAID`),
  CONSTRAINT `EM_CUSTPRODS_SLAS_ibfk_2` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_CUSTPRODS_SLAS`
--

LOCK TABLES `EM_CUSTPRODS_SLAS` WRITE;
/*!40000 ALTER TABLE `EM_CUSTPRODS_SLAS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_CUSTPRODS_SLAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_GEOGRAPHICAL_AREAS`
--

DROP TABLE IF EXISTS `EM_GEOGRAPHICAL_AREAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_GEOGRAPHICAL_AREAS` (
  `NU_AREA_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`NU_AREA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_GEOGRAPHICAL_AREAS`
--

LOCK TABLES `EM_GEOGRAPHICAL_AREAS` WRITE;
/*!40000 ALTER TABLE `EM_GEOGRAPHICAL_AREAS` DISABLE KEYS */;
INSERT INTO `EM_GEOGRAPHICAL_AREAS` VALUES (1,'UK'),(2,'SP'),(3,'Euro Zone');
/*!40000 ALTER TABLE `EM_GEOGRAPHICAL_AREAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_INDIVIDUAL`
--

DROP TABLE IF EXISTS `EM_INDIVIDUAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_INDIVIDUAL` (
  `NU_INDIVIDUALID` bigint(20) NOT NULL auto_increment,
  `DT_REGISTRATION` date NOT NULL,
  `TX_ADDRESS` varchar(200) default NULL,
  `TX_EMAIL` varchar(100) NOT NULL,
  `TX_FAX` varchar(40) default NULL,
  `TX_FIRST_NAME` varchar(40) NOT NULL,
  `TX_JOBDEPARTMENT` varchar(100) default NULL,
  `TX_JOBTITLE` varchar(100) default NULL,
  `TX_LAST_NAME` varchar(60) default NULL,
  `TX_PHONE_NUMBER` varchar(15) default NULL,
  `NU_COUNTRY` bigint(20) NOT NULL,
  `NU_LANGUAGE` bigint(20) NOT NULL,
  `NU_ORGANIZATION_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_INDIVIDUALID`),
  UNIQUE KEY `NU_IND` (`NU_INDIVIDUALID`),
  KEY `FKF4C56290F3037D69` (`NU_COUNTRY`),
  KEY `FKF4C56290C174AC82` (`NU_LANGUAGE`),
  KEY `NU_ORGANIZATION_ID` (`NU_ORGANIZATION_ID`),
  CONSTRAINT `EM_INDIVIDUAL_ibfk_1` FOREIGN KEY (`NU_ORGANIZATION_ID`) REFERENCES `EM_ORGANIZATION` (`NU_ORGANIZATIONID`),
  CONSTRAINT `FKF4C56290C174AC82` FOREIGN KEY (`NU_LANGUAGE`) REFERENCES `EM_LANGUAGES` (`NU_LANGUAGE`),
  CONSTRAINT `FKF4C56290F3037D69` FOREIGN KEY (`NU_COUNTRY`) REFERENCES `EM_COUNTRIES` (`NU_COUNTRY`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_INDIVIDUAL`
--

LOCK TABLES `EM_INDIVIDUAL` WRITE;
/*!40000 ALTER TABLE `EM_INDIVIDUAL` DISABLE KEYS */;
INSERT INTO `EM_INDIVIDUAL` VALUES (38,'2011-02-09','C/Canarias, 32','raul.gonzalez@mail.es','','Raúl','','','Gonzalez','917575756',1,1,1);
/*!40000 ALTER TABLE `EM_INDIVIDUAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_LANGUAGES`
--

DROP TABLE IF EXISTS `EM_LANGUAGES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_LANGUAGES` (
  `NU_LANGUAGE` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(20) NOT NULL,
  PRIMARY KEY  (`NU_LANGUAGE`),
  UNIQUE KEY `NU_LANGUAGE` (`NU_LANGUAGE`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_LANGUAGES`
--

LOCK TABLES `EM_LANGUAGES` WRITE;
/*!40000 ALTER TABLE `EM_LANGUAGES` DISABLE KEYS */;
INSERT INTO `EM_LANGUAGES` VALUES (1,'Spanish'),(2,'English'),(3,'French'),(5,'Italian'),(6,'Japanese'),(7,'Portuguese'),(8,'Flemish'),(9,'German'),(10,'Slovenian'),(16,'Austrian');
/*!40000 ALTER TABLE `EM_LANGUAGES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `EM_OFFER_RATING`
--

DROP TABLE IF EXISTS `EM_OFFER_RATING`;
/*!50001 DROP VIEW IF EXISTS `EM_OFFER_RATING`*/;
/*!50001 CREATE TABLE `EM_OFFER_RATING` (
  `NU_IDPRODUCTOFFERING` bigint(20),
  `NUM_RATES` bigint(21),
  `SATISFACTION_RATE` decimal(53,8)
) ENGINE=MyISAM */;

--
-- Table structure for table `EM_ORGANIZATION`
--

DROP TABLE IF EXISTS `EM_ORGANIZATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_ORGANIZATION` (
  `NU_ORGANIZATIONID` bigint(20) NOT NULL auto_increment,
  `TX_TRADINGNAME` varchar(20) NOT NULL default '',
  `TX_DESCRIPTION` varchar(40) default NULL,
  `TX_FISCALID` varchar(40) default NULL,
  `NU_COUNTRY` bigint(20) default NULL,
  PRIMARY KEY  (`NU_ORGANIZATIONID`),
  UNIQUE KEY `NU_ORGANIZATIONID` (`NU_ORGANIZATIONID`),
  KEY `NU_COUNTRY` (`NU_COUNTRY`),
  CONSTRAINT `EM_ORGANIZATION_ibfk_1` FOREIGN KEY (`NU_COUNTRY`) REFERENCES `EM_COUNTRIES` (`NU_COUNTRY`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_ORGANIZATION`
--

LOCK TABLES `EM_ORGANIZATION` WRITE;
/*!40000 ALTER TABLE `EM_ORGANIZATION` DISABLE KEYS */;
INSERT INTO `EM_ORGANIZATION` VALUES (1,'TELEFONICA','Telefonica description','',7),(2,'IaaS Provider','IaaS Provider company','',1),(3,'Yellow Pages','Yellow PAges company','',1),(4,'BABYLON','BABYLON Translator','',1);
/*!40000 ALTER TABLE `EM_ORGANIZATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARAMS`
--

DROP TABLE IF EXISTS `EM_PARAMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARAMS` (
  `NU_PARAM_ID` bigint(20) NOT NULL auto_increment,
  `TX_PARAM_NAME` varchar(100) NOT NULL,
  `TX_PARAM_RULETYPE` varchar(4) default NULL,
  `ID_PARAM_TYPE` bigint(20) default NULL,
  `TX_PARAM_DESCRIPTION` varchar(100) default NULL,
  `TX_PARAM_VIEWNAME` varchar(100) default NULL,
  PRIMARY KEY  (`NU_PARAM_ID`),
  UNIQUE KEY `NU_PARAM_ID` (`NU_PARAM_ID`),
  KEY `FK62DA54FDC762B44E` (`ID_PARAM_TYPE`),
  CONSTRAINT `FK62DA54FDC762B44E` FOREIGN KEY (`ID_PARAM_TYPE`) REFERENCES `EM_PARAM_TYPE` (`ID_PARAM_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARAMS`
--

LOCK TABLES `EM_PARAMS` WRITE;
/*!40000 ALTER TABLE `EM_PARAMS` DISABLE KEYS */;
INSERT INTO `EM_PARAMS` VALUES (1,'DATE_BEGIN','WHEN',3,'Date of init of the promotion','Init Date'),(2,'DATE_END','WHEN',3,'Date of the end of the promotion','End Date'),(3,'NU_COUNTRY','WHEN',4,'Identification of the country the promotion applies to','Country'),(4,'CATEGORY','WHEN',2,'The category of the product applies to','Category'),(5,'AGE','WHEN',1,'The age of the person the promotion applied to','Age'),(6,'DISCOUNT','THEN',1,'Discount offer by the promotion','Discount'),(7,'PRICE_TYPE','WHEN',5,'Type of discount associated (subcription,monthly)','Price Type'),(8,'MODIF_TYPE','WHEN',6,'Modification Type (Increment, Decrement)','Modification Type'),(9,'PARAM_COMPONENT','WHEN',7,'Policy Parameter Component ','Parameter Component'),(10,'PARAM_VARIATION','WHEN',1,'Percentage Paremeter Variation','Parameter Variation'),(11,'VARIATION_TYPE','THEN',8,'Percentage Price Variation','Price Variation Type'),(12,'SOCIOECONOMIC LEVEL','WHEN',9,'Socioeconomic Level Customer','Socioeconomic Level'),(13,'EDUCATIONAL LEVEL','WHEN',9,'Educational Level Customer','Educational Level'),(14,'ESTIMATED AGE','WHEN',10,'Estimated Customer Age','Estimated Age'),(15,'SLSP','WHEN',11,'Service Level Specification Parameter','SLSP'),(16,'VARIATION_MIN','WHEN',1,'Minimum variation of SLSP','Minor Variation'),(17,'VARIATION_MAX','WHEN',1,'Maximum variation of SLSP','Maximum Variation'),(18,'PRICE_VARIATION','THEN',1,'Price Variation','Price Variation'),(20,'NEW_PRICE','THEN',1,'Adjustment price for changes in duration type of contract ','New Price'),(21,'DURATION_TYPE','WHEN',12,'Duration type of contract','Duration Type'),(22,'VALUE_MIN','WHEN',1,'Minimum value of parameter','Min Value'),(23,'VALUE_MAX','WHEN',1,'Maximum value of parameter','Max Value'),(25,'GUARANTEE_ID','WHEN',2,'Guarantee Term Id','Guarantee Id');
/*!40000 ALTER TABLE `EM_PARAMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARAM_TYPE`
--

DROP TABLE IF EXISTS `EM_PARAM_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARAM_TYPE` (
  `ID_PARAM_TYPE` bigint(20) NOT NULL auto_increment,
  `TX_PARAM_TYPE_NAME` varchar(30) NOT NULL,
  `TX_PARAM_TYPE_VIEW_NAME` varchar(250) default NULL,
  `TX_PARAM_VALUES` varchar(1000) default NULL,
  PRIMARY KEY  (`ID_PARAM_TYPE`),
  UNIQUE KEY `ID_PARAM_TYPE` (`ID_PARAM_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARAM_TYPE`
--

LOCK TABLES `EM_PARAM_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_PARAM_TYPE` DISABLE KEYS */;
INSERT INTO `EM_PARAM_TYPE` VALUES (1,'NUMBER','NUMBER',NULL),(2,'STRING','STRING',NULL),(3,'DATE','DATE',NULL),(4,'SELECT','COUNTRY','EM_COUNTRIES,NU_COUNTRY,TX_NAME'),(5,'SELECT','PRICE TYPE','EM_PRICE_TYPE,NU_IDPRICE_TYPE,TX_NAME'),(6,'LIST','MODIFICATION TYPE','Increment,Decrement'),(7,'LIST','PARAMETER COMPONENT','QualifayingCondition,CustomLevelPercentage,CustomLevelValue '),(8,'SELECT','PRICE VARIATION','EM_PRICE_VARIATION_TYPE,NU_ID_VARIATION_TYPE,TX_DESCRIPTION'),(9,'LIST','LEVEL','Hight,Medium,Low'),(10,'LIST','AGE CATEGORY','Adolescent,Young Adult,Adult,Elderly'),(11,'SELECT','SLSP TYPE','EM_SLSP,TX_SLSP_NAME,TX_SLSP_NAME'),(12,'LIST','DURATION_TYPE','month,weekend,day,hrs');
/*!40000 ALTER TABLE `EM_PARAM_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARTY`
--

DROP TABLE IF EXISTS `EM_PARTY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARTY` (
  `NU_PARTYID` bigint(20) NOT NULL auto_increment,
  `TX_PARTY_IDENTITY_TYPE` varchar(1) NOT NULL default '',
  `NU_BALANCE` decimal(12,2) NOT NULL default '0.00',
  `NU_CREDITLIMIT` decimal(12,2) NOT NULL default '0.00',
  `NU_CURRENCY_ID` bigint(10) NOT NULL default '0',
  `NU_INDIVIDUALID` bigint(20) default NULL,
  `NU_ORGANIZATIONID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_PARTYID`),
  UNIQUE KEY `NU_PARTYID` (`NU_PARTYID`),
  KEY `NU_CURRENCY_ID` (`NU_CURRENCY_ID`),
  KEY `NU_INDIVIDUALID` (`NU_INDIVIDUALID`),
  KEY `NU_ORGANIZATIONID` (`NU_ORGANIZATIONID`),
  CONSTRAINT `EM_PARTY_ibfk_1` FOREIGN KEY (`NU_CURRENCY_ID`) REFERENCES `EM_CURRENCIES` (`NU_CURRENCY_ID`),
  CONSTRAINT `EM_PARTY_ibfk_2` FOREIGN KEY (`NU_INDIVIDUALID`) REFERENCES `EM_INDIVIDUAL` (`NU_INDIVIDUALID`),
  CONSTRAINT `EM_PARTY_ibfk_3` FOREIGN KEY (`NU_ORGANIZATIONID`) REFERENCES `EM_ORGANIZATION` (`NU_ORGANIZATIONID`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARTY`
--

LOCK TABLES `EM_PARTY` WRITE;
/*!40000 ALTER TABLE `EM_PARTY` DISABLE KEYS */;
INSERT INTO `EM_PARTY` VALUES (1,'O','100.00','100.00',1,NULL,1),(2,'O','0.00','100.00',1,NULL,2),(3,'O','0.00','100.00',1,NULL,3),(4,'O','0.00','100.00',1,NULL,4);
/*!40000 ALTER TABLE `EM_PARTY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARTY_BANK`
--

DROP TABLE IF EXISTS `EM_PARTY_BANK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARTY_BANK` (
  `NU_BANK_ID` bigint(20) NOT NULL,
  `NU_PARTYID` bigint(20) NOT NULL default '0',
  `TX_ACCOUNT_NUMBER` varchar(20) NOT NULL default '',
  `NU_ISSUE_NUMBER` bigint(20) NOT NULL default '0',
  `TX_ACCOUNT_NAME` varchar(80) NOT NULL default '',
  `TX_DESCRIPTION` varchar(100) default NULL,
  `DT_UPDATED` date NOT NULL,
  `TC_DEFAULT_YN` char(1) NOT NULL,
  `TX_SWIFT_CODE` varchar(25) NOT NULL default '',
  `TX_IBAN_CODE` varchar(25) default NULL,
  `TX_BANK_OFFICE_ADDRESS` varchar(100) default NULL,
  PRIMARY KEY  (`NU_BANK_ID`),
  UNIQUE KEY `NU_BANK_ID` (`NU_BANK_ID`),
  KEY `FK15D6340CB47E1006` (`NU_PARTYID`),
  CONSTRAINT `FK15D6340CB47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARTY_BANK`
--

LOCK TABLES `EM_PARTY_BANK` WRITE;
/*!40000 ALTER TABLE `EM_PARTY_BANK` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PARTY_BANK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARTY_CARD`
--

DROP TABLE IF EXISTS `EM_PARTY_CARD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARTY_CARD` (
  `NU_CARD_ID` bigint(20) NOT NULL,
  `NU_PARTYID` bigint(20) NOT NULL default '0',
  `TX_CARD_NUMBER` varchar(20) NOT NULL default '',
  `NU_ISSUE_NUMBER` bigint(20) NOT NULL default '0',
  `TX_NAME_ON_CARD` varchar(80) NOT NULL default '',
  `TX_DESCRIPTION` varchar(100) default NULL,
  `DT_EXPIRY_DATE` date NOT NULL,
  `DT_UPDATED` date NOT NULL,
  `TC_DEFAULT_YN` char(1) NOT NULL,
  `TX_CARD_PROVIDER` varchar(10) NOT NULL,
  PRIMARY KEY  (`NU_CARD_ID`),
  UNIQUE KEY `NU_CARD_ID` (`NU_CARD_ID`),
  KEY `FK15D6A8E0B47E1006` (`NU_PARTYID`),
  CONSTRAINT `FK15D6A8E0B47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARTY_CARD`
--

LOCK TABLES `EM_PARTY_CARD` WRITE;
/*!40000 ALTER TABLE `EM_PARTY_CARD` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PARTY_CARD` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARTY_PARTYROLE`
--

DROP TABLE IF EXISTS `EM_PARTY_PARTYROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARTY_PARTYROLE` (
  `NU_PARTYID` bigint(20) NOT NULL,
  `NU_PARTYROLEID` bigint(20) NOT NULL,
  `TX_CUSTOMERRANK` varchar(18) default NULL,
  `TX_STATUS` varchar(10) default NULL,
  `TX_REJECTION_CAUSE` varchar(255) default NULL,
  PRIMARY KEY  (`NU_PARTYID`,`NU_PARTYROLEID`),
  KEY `FKF423B46C379AD212` (`NU_PARTYROLEID`),
  KEY `FKF423B46CB47E1006` (`NU_PARTYID`),
  CONSTRAINT `FKF423B46C379AD212` FOREIGN KEY (`NU_PARTYROLEID`) REFERENCES `EM_PARTY_ROLE` (`NU_PARTYROLEID`),
  CONSTRAINT `FKF423B46CB47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARTY_PARTYROLE`
--

LOCK TABLES `EM_PARTY_PARTYROLE` WRITE;
/*!40000 ALTER TABLE `EM_PARTY_PARTYROLE` DISABLE KEYS */;
INSERT INTO `EM_PARTY_PARTYROLE` VALUES (1,1,NULL,'APPROVED',''),(1,2,NULL,'APPROVED',''),(1,3,NULL,'APPROVED',''),(2,2,NULL,'APPROVED',''),(3,2,NULL,'APPROVED','');
/*!40000 ALTER TABLE `EM_PARTY_PARTYROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PARTY_ROLE`
--

DROP TABLE IF EXISTS `EM_PARTY_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PARTY_ROLE` (
  `NU_PARTYROLEID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(20) default NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  PRIMARY KEY  (`NU_PARTYROLEID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PARTY_ROLE`
--

LOCK TABLES `EM_PARTY_ROLE` WRITE;
/*!40000 ALTER TABLE `EM_PARTY_ROLE` DISABLE KEYS */;
INSERT INTO `EM_PARTY_ROLE` VALUES (1,'ADMIN','ADMINISTRATOR EMARKETPLACE'),(2,'SERVICEPROVIDER','PUBLISH SERVICES'),(3,'CUSTOMER','CONTRACT PRODUCTS');
/*!40000 ALTER TABLE `EM_PARTY_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PENALTIES`
--

DROP TABLE IF EXISTS `EM_PENALTIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PENALTIES` (
  `NU_PENALTY_ID` bigint(20) NOT NULL,
  `TX_DESCRIPTION` varchar(100) NOT NULL,
  `TX_NAME` varchar(20) NOT NULL,
  `NU_PENALTY_TYPE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_PENALTY_ID`),
  UNIQUE KEY `NU_PENALTY_ID` (`NU_PENALTY_ID`),
  KEY `FK69676BF0C7406F4C` (`NU_PENALTY_TYPE_ID`),
  CONSTRAINT `FK69676BF0C7406F4C` FOREIGN KEY (`NU_PENALTY_TYPE_ID`) REFERENCES `EM_PENALTY_TYPE` (`NU_PENALTY_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PENALTIES`
--

LOCK TABLES `EM_PENALTIES` WRITE;
/*!40000 ALTER TABLE `EM_PENALTIES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PENALTIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PENALTY_TYPE`
--

DROP TABLE IF EXISTS `EM_PENALTY_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PENALTY_TYPE` (
  `NU_PENALTY_TYPE_ID` bigint(20) NOT NULL,
  `TX_DESCRIPTION` varchar(60) NOT NULL,
  `TX_NAME` varchar(20) NOT NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_PENALTY_TYPE_ID`),
  UNIQUE KEY `NU_PENALTY_TYPE_ID` (`NU_PENALTY_TYPE_ID`),
  KEY `FK17078787CC0A0369` (`NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `FK17078787CC0A0369` FOREIGN KEY (`NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_TEMPLATES` (`NU_RULE_TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PENALTY_TYPE`
--

LOCK TABLES `EM_PENALTY_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_PENALTY_TYPE` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PENALTY_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PENALTY_VALUES`
--

DROP TABLE IF EXISTS `EM_PENALTY_VALUES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PENALTY_VALUES` (
  `NU_PARAM_ID` bigint(20) NOT NULL,
  `NU_PENALTY_ID` bigint(20) NOT NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) NOT NULL,
  `TX_PARAM_VALUE` varchar(40) default NULL,
  PRIMARY KEY  (`NU_PARAM_ID`,`NU_PENALTY_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FK7559B22F22C84C25` (`NU_PARAM_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FK7559B22F6FABCE87` (`NU_PENALTY_ID`),
  CONSTRAINT `FK7559B22F22C84C25` FOREIGN KEY (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_PARAMS` (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `FK7559B22F6FABCE87` FOREIGN KEY (`NU_PENALTY_ID`) REFERENCES `EM_PENALTIES` (`NU_PENALTY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PENALTY_VALUES`
--

LOCK TABLES `EM_PENALTY_VALUES` WRITE;
/*!40000 ALTER TABLE `EM_PENALTY_VALUES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PENALTY_VALUES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POL`
--

DROP TABLE IF EXISTS `EM_POL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POL` (
  `NU_POL_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(100) default NULL,
  `TX_STATE` varchar(20) default NULL,
  `NU_POL_TYPE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_POL_ID`),
  KEY `NU_POL_TYPE_ID` (`NU_POL_TYPE_ID`),
  CONSTRAINT `EM_POL_ibfk_1` FOREIGN KEY (`NU_POL_TYPE_ID`) REFERENCES `EM_POL_TYPE` (`NU_POL_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POL`
--

LOCK TABLES `EM_POL` WRITE;
/*!40000 ALTER TABLE `EM_POL` DISABLE KEYS */;
INSERT INTO `EM_POL` VALUES (1,'test 1','APPROVED',2),(3,'test 2','PENDING',2),(4,'test 3','REJECTED',2);
/*!40000 ALTER TABLE `EM_POL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POLICIES`
--

DROP TABLE IF EXISTS `EM_POLICIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POLICIES` (
  `NU_POLICY_ID` bigint(20) NOT NULL auto_increment,
  `NU_IDPRODUCTOFFERING` bigint(20) default NULL,
  `NU_POLICY_TYPE_ID` bigint(20) default NULL,
  `NU_SERVICEID` bigint(20) default NULL,
  `TX_STATUS` varchar(10) default NULL,
  PRIMARY KEY  (`NU_POLICY_ID`),
  KEY `NU_POLICY_TYPE_ID` (`NU_POLICY_TYPE_ID`),
  KEY `NU_IDPRODUCTOFFERING` (`NU_IDPRODUCTOFFERING`),
  KEY `NU_SERVICEID` (`NU_SERVICEID`),
  CONSTRAINT `EM_POLICIES_ibfk_4` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `EM_POLICIES_ibfk_1` FOREIGN KEY (`NU_POLICY_TYPE_ID`) REFERENCES `EM_POLICY_TYPE` (`NU_POLICY_TYPE_ID`),
  CONSTRAINT `EM_POLICIES_ibfk_3` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POLICIES`
--

LOCK TABLES `EM_POLICIES` WRITE;
/*!40000 ALTER TABLE `EM_POLICIES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_POLICIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POLICY_TYPE`
--

DROP TABLE IF EXISTS `EM_POLICY_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POLICY_TYPE` (
  `NU_POLICY_TYPE_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(100) default NULL,
  `TX_DESCRIPTION` varchar(255) default NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_POLICY_TYPE_ID`),
  KEY `NU_RULE_TEMPLATE_ID` (`NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `EM_POLICY_TYPE_ibfk_1` FOREIGN KEY (`NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_TEMPLATES` (`NU_RULE_TEMPLATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POLICY_TYPE`
--

LOCK TABLES `EM_POLICY_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_POLICY_TYPE` DISABLE KEYS */;
INSERT INTO `EM_POLICY_TYPE` VALUES (1,'Policy Percentage','Policy Percentage changes in GTs',9);
/*!40000 ALTER TABLE `EM_POLICY_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POLICY_VALUES`
--

DROP TABLE IF EXISTS `EM_POLICY_VALUES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POLICY_VALUES` (
  `NU_POLICY_ID` bigint(20) NOT NULL auto_increment,
  `NU_PARAM_ID` bigint(20) NOT NULL default '0',
  `NU_RULE_TEMPLATE_ID` bigint(20) NOT NULL default '0',
  `TX_PARAM_VALUE` varchar(255) default NULL,
  PRIMARY KEY  (`NU_POLICY_ID`,`NU_RULE_TEMPLATE_ID`,`NU_PARAM_ID`),
  KEY `NU_PARAM_ID` (`NU_PARAM_ID`),
  KEY `NU_RULE_TEMPLATE_ID` (`NU_RULE_TEMPLATE_ID`,`NU_PARAM_ID`),
  KEY `NU_PARAM_ID_2` (`NU_PARAM_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FK8D522D78CBFA24EC` (`NU_RULE_TEMPLATE_ID`,`NU_PARAM_ID`),
  CONSTRAINT `FK8D522D78CBFA24EC` FOREIGN KEY (`NU_RULE_TEMPLATE_ID`, `NU_PARAM_ID`) REFERENCES `EM_RULE_PARAMS` (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `EM_POLICY_VALUES_ibfk_2` FOREIGN KEY (`NU_POLICY_ID`) REFERENCES `EM_POLICIES` (`NU_POLICY_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `EM_POLICY_VALUES_ibfk_3` FOREIGN KEY (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_PARAMS` (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POLICY_VALUES`
--

LOCK TABLES `EM_POLICY_VALUES` WRITE;
/*!40000 ALTER TABLE `EM_POLICY_VALUES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_POLICY_VALUES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POL_RULES`
--

DROP TABLE IF EXISTS `EM_POL_RULES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POL_RULES` (
  `EM_POL_ID` bigint(20) NOT NULL default '0',
  `NU_RULE_ID` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`EM_POL_ID`,`NU_RULE_ID`),
  KEY `NU_RULE_ID` (`NU_RULE_ID`),
  CONSTRAINT `EM_POL_RULES_ibfk_2` FOREIGN KEY (`NU_RULE_ID`) REFERENCES `EM_RULE` (`NU_RULE_ID`),
  CONSTRAINT `EM_POL_RULES_ibfk_1` FOREIGN KEY (`EM_POL_ID`) REFERENCES `EM_POL` (`NU_POL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POL_RULES`
--

LOCK TABLES `EM_POL_RULES` WRITE;
/*!40000 ALTER TABLE `EM_POL_RULES` DISABLE KEYS */;
INSERT INTO `EM_POL_RULES` VALUES (1,1);
/*!40000 ALTER TABLE `EM_POL_RULES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_POL_TYPE`
--

DROP TABLE IF EXISTS `EM_POL_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_POL_TYPE` (
  `NU_POL_TYPE_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(100) default NULL,
  `TX_DESCRIPTION` varchar(255) default NULL,
  PRIMARY KEY  (`NU_POL_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_POL_TYPE`
--

LOCK TABLES `EM_POL_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_POL_TYPE` DISABLE KEYS */;
INSERT INTO `EM_POL_TYPE` VALUES (1,'Adjustment','Adjusment Policy'),(2,'Negotiation','Negotiation Policy');
/*!40000 ALTER TABLE `EM_POL_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRICE_TYPE`
--

DROP TABLE IF EXISTS `EM_PRICE_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRICE_TYPE` (
  `NU_IDPRICE_TYPE` bigint(20) NOT NULL auto_increment,
  `TC_PERIOD` varchar(12) NOT NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`NU_IDPRICE_TYPE`),
  UNIQUE KEY `NU_IDPRICE_TYPE` (`NU_IDPRICE_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRICE_TYPE`
--

LOCK TABLES `EM_PRICE_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_PRICE_TYPE` DISABLE KEYS */;
INSERT INTO `EM_PRICE_TYPE` VALUES (1,'ONE TIME','one_time_charge','one_time_charge'),(2,'MONTH','Monthly payment','per_month'),(3,'EVENT','Per request','per_request'),(4,'EVENT','Tariff usage','tariff_usage'),(5,'WEEK','Pay for week','per_week'),(11,'MONTH','Flat Rate','flat_rate');
/*!40000 ALTER TABLE `EM_PRICE_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRICE_VARIATION`
--

DROP TABLE IF EXISTS `EM_PRICE_VARIATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRICE_VARIATION` (
  `NU_ID_PRICE_VARIATION` bigint(20) NOT NULL,
  `NU_POLICY_ID` bigint(20) default NULL,
  `NU_IDCOMPONENTPRICE` bigint(20) NOT NULL,
  `NU_ID_VARIATION_TYPE` bigint(20) default NULL,
  `TX_BSLAID` varchar(100) default NULL,
  `NU_PROMOTION_ID` bigint(20) default NULL,
  `NU_QUANTITY` decimal(22,0) default NULL,
  `NU_VALUE` decimal(22,0) default NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  PRIMARY KEY  (`NU_ID_PRICE_VARIATION`),
  KEY `FKD21CD4C6D9855630` (`NU_POLICY_ID`),
  KEY `FKD21CD4C6CDBFDBEF` (`NU_PROMOTION_ID`),
  KEY `FKD21CD4C61A2EC446` (`NU_ID_VARIATION_TYPE`),
  KEY `FKD21CD4C6C64FFC05` (`TX_BSLAID`),
  KEY `FKD21CD4C691D9181B` (`NU_IDCOMPONENTPRICE`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRICE_VARIATION`
--

LOCK TABLES `EM_PRICE_VARIATION` WRITE;
/*!40000 ALTER TABLE `EM_PRICE_VARIATION` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PRICE_VARIATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRICE_VARIATION_TYPE`
--

DROP TABLE IF EXISTS `EM_PRICE_VARIATION_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRICE_VARIATION_TYPE` (
  `NU_ID_VARIATION_TYPE` bigint(20) NOT NULL auto_increment,
  `TC_MODIFICATION_TYPE` char(1) default NULL,
  `TC_UNIT_TYPE` char(1) default NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  PRIMARY KEY  (`NU_ID_VARIATION_TYPE`),
  UNIQUE KEY `NU_ID_VARIATION_TYPE` (`NU_ID_VARIATION_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRICE_VARIATION_TYPE`
--

LOCK TABLES `EM_PRICE_VARIATION_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_PRICE_VARIATION_TYPE` DISABLE KEYS */;
INSERT INTO `EM_PRICE_VARIATION_TYPE` VALUES (1,'I','P','Percent Increment'),(2,'I','F','Fixed Increment'),(3,'D','P','Percent Decrement'),(4,'D','F','Decrement fixed price');
/*!40000 ALTER TABLE `EM_PRICE_VARIATION_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRODUCTS_AREAS`
--

DROP TABLE IF EXISTS `EM_PRODUCTS_AREAS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRODUCTS_AREAS` (
  `NU_IDPRODUCTOFFERING` bigint(20) NOT NULL,
  `NU_AREA_ID` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`NU_IDPRODUCTOFFERING`,`NU_AREA_ID`),
  KEY `FKA397D3421A968BF0` (`NU_AREA_ID`),
  KEY `FKA397D3422E546354` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `EM_PRODUCTS_AREAS_ibfk_2` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `EM_PRODUCTS_AREAS_ibfk_1` FOREIGN KEY (`NU_AREA_ID`) REFERENCES `EM_GEOGRAPHICAL_AREAS` (`NU_AREA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRODUCTS_AREAS`
--

LOCK TABLES `EM_PRODUCTS_AREAS` WRITE;
/*!40000 ALTER TABLE `EM_PRODUCTS_AREAS` DISABLE KEYS */;
INSERT INTO `EM_PRODUCTS_AREAS` VALUES (61,3),(62,3),(63,3);
/*!40000 ALTER TABLE `EM_PRODUCTS_AREAS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRODUCT_OFFER_SLAT`
--

DROP TABLE IF EXISTS `EM_PRODUCT_OFFER_SLAT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRODUCT_OFFER_SLAT` (
  `NU_IDPRODUCTOFFERING` bigint(20) NOT NULL,
  `NU_SERVICEID` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_IDPRODUCTOFFERING`,`NU_SERVICEID`),
  KEY `NU_SERVICEID` (`NU_SERVICEID`),
  CONSTRAINT `EM_PRODUCT_OFFER_SLAT_ibfk_3` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `EM_PRODUCT_OFFER_SLAT_ibfk_2` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRODUCT_OFFER_SLAT`
--

LOCK TABLES `EM_PRODUCT_OFFER_SLAT` WRITE;
/*!40000 ALTER TABLE `EM_PRODUCT_OFFER_SLAT` DISABLE KEYS */;
INSERT INTO `EM_PRODUCT_OFFER_SLAT` VALUES (61,25),(62,26),(63,27);
/*!40000 ALTER TABLE `EM_PRODUCT_OFFER_SLAT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRODUCT_PENALTY`
--

DROP TABLE IF EXISTS `EM_PRODUCT_PENALTY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRODUCT_PENALTY` (
  `NU_PRODUCTID` bigint(20) NOT NULL,
  `NU_PENALTY_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_PRODUCTID`,`NU_PENALTY_ID`),
  KEY `FK9B0C93626FABCE87` (`NU_PENALTY_ID`),
  KEY `FK9B0C93624168D1FC` (`NU_PRODUCTID`),
  CONSTRAINT `FK9B0C93624168D1FC` FOREIGN KEY (`NU_PRODUCTID`) REFERENCES `EM_SP_PRODUCTS` (`NU_PRODUCTID`),
  CONSTRAINT `FK9B0C93626FABCE87` FOREIGN KEY (`NU_PENALTY_ID`) REFERENCES `EM_PENALTIES` (`NU_PENALTY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRODUCT_PENALTY`
--

LOCK TABLES `EM_PRODUCT_PENALTY` WRITE;
/*!40000 ALTER TABLE `EM_PRODUCT_PENALTY` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PRODUCT_PENALTY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PRODUCT_PROMOTION`
--

DROP TABLE IF EXISTS `EM_PRODUCT_PROMOTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PRODUCT_PROMOTION` (
  `NU_PRODUCTID` bigint(20) NOT NULL,
  `NU_PROMOTION_ID` bigint(20) NOT NULL,
  `TX_STATUS` varchar(10) default NULL,
  PRIMARY KEY  (`NU_PRODUCTID`,`NU_PROMOTION_ID`),
  KEY `FK9A2F013C248E0328` (`NU_PROMOTION_ID`),
  KEY `FK9A2F013C4168D1FC` (`NU_PRODUCTID`),
  CONSTRAINT `EM_PRODUCT_PROMOTION_ibfk_1` FOREIGN KEY (`NU_PRODUCTID`) REFERENCES `EM_SP_PRODUCTS` (`NU_PRODUCTID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK9A2F013C248E0328` FOREIGN KEY (`NU_PROMOTION_ID`) REFERENCES `EM_PROMOTIONS` (`NU_PROMOTION_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PRODUCT_PROMOTION`
--

LOCK TABLES `EM_PRODUCT_PROMOTION` WRITE;
/*!40000 ALTER TABLE `EM_PRODUCT_PROMOTION` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PRODUCT_PROMOTION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `EM_PRODUCT_RATING`
--

DROP TABLE IF EXISTS `EM_PRODUCT_RATING`;
/*!50001 DROP VIEW IF EXISTS `EM_PRODUCT_RATING`*/;
/*!50001 CREATE TABLE `EM_PRODUCT_RATING` (
  `NU_PRODUCTID` bigint(20),
  `NUM_RATES` bigint(21),
  `SATISFACTION_RATE` decimal(53,8)
) ENGINE=MyISAM */;

--
-- Table structure for table `EM_PROMOTIONS`
--

DROP TABLE IF EXISTS `EM_PROMOTIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PROMOTIONS` (
  `NU_PROMOTION_ID` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(100) default NULL,
  `NU_PROMOTION_TYPE_ID` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`NU_PROMOTION_ID`),
  UNIQUE KEY `NU_PROMOTION_ID` (`NU_PROMOTION_ID`),
  KEY `FK32CA582717BEC4C0` (`NU_PROMOTION_TYPE_ID`),
  CONSTRAINT `FK32CA582717BEC4C0` FOREIGN KEY (`NU_PROMOTION_TYPE_ID`) REFERENCES `EM_PROMOTION_TYPE` (`NU_PROMOTION_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PROMOTIONS`
--

LOCK TABLES `EM_PROMOTIONS` WRITE;
/*!40000 ALTER TABLE `EM_PROMOTIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PROMOTIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PROMOTION_TYPE`
--

DROP TABLE IF EXISTS `EM_PROMOTION_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PROMOTION_TYPE` (
  `NU_PROMOTION_TYPE_ID` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(60) default NULL,
  `TX_NAME` varchar(20) default NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_PROMOTION_TYPE_ID`),
  UNIQUE KEY `NU_PROMOTION_TYPE_ID` (`NU_PROMOTION_TYPE_ID`),
  KEY `FK98D48DEDCC0A0369` (`NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `FK98D48DEDCC0A0369` FOREIGN KEY (`NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_TEMPLATES` (`NU_RULE_TEMPLATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PROMOTION_TYPE`
--

LOCK TABLES `EM_PROMOTION_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_PROMOTION_TYPE` DISABLE KEYS */;
INSERT INTO `EM_PROMOTION_TYPE` VALUES (1,'Promo discount range data Contract','Contract Dates',8),(2,'Promo discount range Data Price period','Month Price',8),(3,'Promo discount acording to Customer contract Country','Country Contract',4),(4,'Promo discount acording to Customer events Country','Country Event',4),(6,'Socioeconomic Customer Level','Socioeconomic Level',11);
/*!40000 ALTER TABLE `EM_PROMOTION_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_PROMOTION_VALUES`
--

DROP TABLE IF EXISTS `EM_PROMOTION_VALUES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_PROMOTION_VALUES` (
  `NU_PARAM_ID` bigint(20) NOT NULL,
  `NU_PROMOTION_ID` bigint(20) NOT NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) NOT NULL,
  `TX_PARAM_VALUE` varchar(40) default NULL,
  PRIMARY KEY  (`NU_PARAM_ID`,`NU_PROMOTION_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FKB7FEB71522C84C25` (`NU_PARAM_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FKB7FEB715248E0328` (`NU_PROMOTION_ID`),
  CONSTRAINT `FKB7FEB71522C84C25` FOREIGN KEY (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_PARAMS` (`NU_PARAM_ID`, `NU_RULE_TEMPLATE_ID`),
  CONSTRAINT `FKB7FEB715248E0328` FOREIGN KEY (`NU_PROMOTION_ID`) REFERENCES `EM_PROMOTIONS` (`NU_PROMOTION_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_PROMOTION_VALUES`
--

LOCK TABLES `EM_PROMOTION_VALUES` WRITE;
/*!40000 ALTER TABLE `EM_PROMOTION_VALUES` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_PROMOTION_VALUES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_RESTRICTIONS`
--

DROP TABLE IF EXISTS `EM_RESTRICTIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_RESTRICTIONS` (
  `NU_RESTRICTION_ID` bigint(20) NOT NULL,
  `TX_DESCRIPTION` varchar(200) NOT NULL,
  `TX_NAME` varchar(20) NOT NULL,
  PRIMARY KEY  (`NU_RESTRICTION_ID`),
  UNIQUE KEY `NU_RESTRICTION_ID` (`NU_RESTRICTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_RESTRICTIONS`
--

LOCK TABLES `EM_RESTRICTIONS` WRITE;
/*!40000 ALTER TABLE `EM_RESTRICTIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_RESTRICTIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_RULE`
--

DROP TABLE IF EXISTS `EM_RULE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_RULE` (
  `NU_RULE_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(255) default NULL,
  `TX_TEMPLATE` text,
  `NU_RULE_CONTEXT_ID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_RULE_ID`),
  UNIQUE KEY `TX_NAME` (`TX_NAME`),
  KEY `NU_RULE_CONTEXT_ID` (`NU_RULE_CONTEXT_ID`),
  CONSTRAINT `EM_RULE_ibfk_1` FOREIGN KEY (`NU_RULE_CONTEXT_ID`) REFERENCES `EM_RULE_CONTEXT` (`NU_RULE_CONTEXT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_RULE`
--

LOCK TABLES `EM_RULE` WRITE;
/*!40000 ALTER TABLE `EM_RULE` DISABLE KEYS */;
INSERT INTO `EM_RULE` VALUES (1,'BootstrapStateMachine_Rule','rule \"BootstrapStateMachine_Rule\"\n    when\n    	//An empty would also run the then part.\n    	event : Event(eventName == EventName.StartNegotiationEvent);\n    then\n    	insert(new State(StateName.START));\n    	event.setProcessedSuccessfully(true);\n    	retract(event);\n        System.out.println(\"BootstrapStateMachine_Rule fired\" );\nend',1),(2,'CreateState_Rule','rule \"CreateState_Rule\"\r\n    when\r\n    	startState : State(name == StateName.START, status == StateStatus.NOT_STARTED);\r\n    then\r\n        System.out.println(\"CreateState_Rule fired\" );\r\n        System.out.println(\"-State Name :\" +startState.getName()+ \" has Status :\" +startState.getStatus());\r\n        startState.setStatus( StateStatus.RUNNING );\r\n        update(startState);\r\n        System.out.println(\"-State changed its status to :\" +startState.getStatus());\r\nend',1),(3,'StartState_OnEntry_Rule','rule \"StartState_OnEntry_Rule\" \r\n	salience 3\r\n	when \r\n		startState : State(name == StateName.START, status == StateStatus.RUNNING);		\r\n	then \r\n		startState.onEntry();\r\n        System.out.println(\"StartState_OnEntry_Rule fired\");\r\nend',1),(4,'Start_To_Negotiate_Transition_Rule','rule \"Start_To_Negotiate_Transition_Rule\"\n	salience 2\n	when \n		startState : State(name == StateName.START, status == StateStatus.RUNNING);\n	then \n		startState.setStatus(StateStatus.READY_TO_TRANSIT); //we dont need to modify this fact due to PropertyChangeListener.\n		System.out.println(\"State Name :\" +startState.getName()+ \" has Status :\" +startState.getStatus());		\n		insert(new State(StateName.NEGOTIATE)); //instantiating new State, without any hardcoding inside Platform components!\n        System.out.println(\"Start_To_Negotiate_Transition_Rule fired\");\nend',1),(5,'NegotiateState_ProposalArrived_Rule','rule \"NegotiateState_ProposalArrived_Rule\" \r\n	when \r\n		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.NOT_STARTED);\r\n		event01 : Event(eventName == EventName.ProposalArrivedEvent);\r\n	then \r\n		negotiateState.setStatus(StateStatus.RUNNING); //we dont need to modify this fact due to PropertyChangeListener.\r\n		negotiateState.onEntry();\r\n		negotiateState.setNumberOfHopsAllowed(3);\r\n		negotiateState.setCurrentHop(1);\r\n		event01.setProcessedSuccessfully(true);\r\n		retract(event01);\r\n		update(negotiateState);\r\n		System.out.println(\"State Name :\" +negotiateState.getName()+ \" has Status :\" +negotiateState.getStatus());\r\n        System.out.println(\"NegotiateState_ProposalArrived_Rule fired\");\r\nend',1),(6,'NegotiateState_Evaluate_ProposalArrivedEvent_Rule','rule \"NegotiateState_Evaluate_ProposalArrivedEvent_Rule\" \r\n	when\r\n		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.RUNNING, currentHop < numberOfHopsAllowed );\r\n		event1 : Event(eventName == EventName.ProposalArrivedEvent);\r\n	then \r\n		negotiateState.setCurrentHop(negotiateState.getCurrentHop()+1);\r\n		update(negotiateState);//This update is essential coz without it, the Working Memory wont execute last statement.\r\n		event1.setProcessedSuccessfully(true);\r\n		retract(event1);//Remove event as soon as it is processed.\r\n		System.out.println(\"State Name :\" +negotiateState.getName()+ \" has Status :\" +negotiateState.getStatus()+\" HopCount :\"+negotiateState.getCurrentHop());		\r\n        System.out.println(\"NegotiateState_Evaluate_ProposalArrivedEvent_Rule fired\");\r\nend',1),(7,'NegotiateState_Evaluate_Event_Rule2','rule \"NegotiateState_Evaluate_Event_Rule2\" \r\n	when		\r\n		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.RUNNING, currentHop >= numberOfHopsAllowed );\r\n	then \r\n		negotiateState.setStatus(StateStatus.READY_TO_TRANSIT);\r\n		update(negotiateState);\r\n		System.out.println(\"State Name :\" +negotiateState.getName()+ \" has Status :\" +negotiateState.getStatus()+\" HopCount :\"+negotiateState.getCurrentHop());		\r\n        System.out.println(\"NegotiateState_Evaluate_Event_Rule2 fired\");\r\nend',1),(8,'Negotiate_To_Decide_Transition_RequestAgreementEvent_Rule','rule \"Negotiate_To_Decide_Transition_RequestAgreementEvent_Rule\" \r\n	when \r\n		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.READY_TO_TRANSIT );\r\n		event : Event(eventName == EventName.RequestAgreementEvent, eval(offer.getParties() != null) );\r\n	then \r\n		negotiateState.onExit();\r\n		System.out.println(\"State Name :\" +negotiateState.getName()+ \" has Status :\" +negotiateState.getStatus());\r\n		event.setProcessedSuccessfully(true);\r\n		insert(new State(StateName.DECIDE));\r\n		System.out.println(\"event.offer.getParties().length = \"+event.getOffer().getParties().length);\r\n		retract(event);		\r\n        System.out.println(\"Negotiate_To_Decide_Transition_RequestAgreementEvent_Rule fired\");\r\nend',1),(9,'Negotiate_To_Decide_Transition_AgreementRequestedEvent_Rule','rule \"Negotiate_To_Decide_Transition_AgreementRequestedEvent_Rule\" \r\n	when \r\n		negotiateState : State(name == StateName.NEGOTIATE, status == StateStatus.READY_TO_TRANSIT );\r\n		event : Event(eventName == EventName.AgreementRequestedEvent);\r\n	then \r\n		negotiateState.onExit();\r\n		System.out.println(\"State Name :\" +negotiateState.getName()+ \" has Status :\" +negotiateState.getStatus());\r\n		event.setProcessedSuccessfully(true);\r\n		insert(new State(StateName.DECIDE));\r\n		retract(event);\r\n        System.out.println(\"Negotiate_To_Decide_Transition_AgreementRequestedEvent_Rule fired\");\r\nend',1),(10,'DecideState_OnEntry_Rule','rule \"DecideState_OnEntry_Rule\" \r\n	when \r\n		decideState : State(name == StateName.DECIDE, status == StateStatus.NOT_STARTED);\r\n	then \r\n		decideState.setStatus(StateStatus.RUNNING); //we dont need to modify this fact due to PropertyChangeListener.\r\n		decideState.onEntry();\r\n		update(decideState);\r\n        System.out.println(\"DecideState_OnEntry_Rule fired\" + decideState.getStatus());\r\nend',1),(11,'Hello World','rule \"Hello World\"\n	when\n		m : Message( status == Message.HELLO, message : message )\n	then\n		System.out.println( \"Im the stupid Hello-World rule: \"+message ); \nend',1);
/*!40000 ALTER TABLE `EM_RULE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_RULE_CONTEXT`
--

DROP TABLE IF EXISTS `EM_RULE_CONTEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_RULE_CONTEXT` (
  `NU_RULE_CONTEXT_ID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(255) default NULL,
  `TX_CONTENT` text,
  PRIMARY KEY  (`NU_RULE_CONTEXT_ID`),
  UNIQUE KEY `TX_NAME` (`TX_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_RULE_CONTEXT`
--

LOCK TABLES `EM_RULE_CONTEXT` WRITE;
/*!40000 ALTER TABLE `EM_RULE_CONTEXT` DISABLE KEYS */;
INSERT INTO `EM_RULE_CONTEXT` VALUES (1,'protocol','package protocol;\n\nimport org.slasoi.gslam.protocolengine.impl.StateEngine.Message;\nimport org.slasoi.gslam.protocolengine.impl.State;\nimport org.slasoi.gslam.protocolengine.impl.StateName;\nimport org.slasoi.gslam.protocolengine.impl.StateStatus;\nimport org.slasoi.gslam.protocolengine.impl.Event;\nimport org.slasoi.gslam.protocolengine.impl.EventName;\nimport org.slasoi.slamodel.sla.SLATemplate;\nimport java.util.List;');
/*!40000 ALTER TABLE `EM_RULE_CONTEXT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_RULE_PARAMS`
--

DROP TABLE IF EXISTS `EM_RULE_PARAMS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_RULE_PARAMS` (
  `NU_PARAM_ID` bigint(20) NOT NULL,
  `NU_RULE_TEMPLATE_ID` bigint(20) NOT NULL,
  `NU_PARAM_RULE_ORDER` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_PARAM_ID`,`NU_RULE_TEMPLATE_ID`),
  KEY `FK78707312CC0A0369` (`NU_RULE_TEMPLATE_ID`),
  KEY `FK78707312DA3250D4` (`NU_PARAM_ID`),
  CONSTRAINT `EM_RULE_PARAMS_ibfk_2` FOREIGN KEY (`NU_PARAM_ID`) REFERENCES `EM_PARAMS` (`NU_PARAM_ID`),
  CONSTRAINT `EM_RULE_PARAMS_ibfk_3` FOREIGN KEY (`NU_RULE_TEMPLATE_ID`) REFERENCES `EM_RULE_TEMPLATES` (`NU_RULE_TEMPLATE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_RULE_PARAMS`
--

LOCK TABLES `EM_RULE_PARAMS` WRITE;
/*!40000 ALTER TABLE `EM_RULE_PARAMS` DISABLE KEYS */;
INSERT INTO `EM_RULE_PARAMS` VALUES (1,8,1),(2,8,2),(3,4,1),(6,4,4),(6,8,5),(6,11,4),(7,4,2),(7,8,3),(7,9,2),(7,11,2),(7,12,2),(8,9,1),(8,12,1),(11,4,3),(11,8,4),(11,9,6),(11,11,3),(11,12,6),(12,11,1),(15,9,3),(15,12,3),(16,9,4),(17,9,5),(18,9,7),(18,12,7),(22,12,4),(23,12,5);
/*!40000 ALTER TABLE `EM_RULE_PARAMS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_RULE_TEMPLATES`
--

DROP TABLE IF EXISTS `EM_RULE_TEMPLATES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_RULE_TEMPLATES` (
  `NU_RULE_TEMPLATE_ID` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(40) default NULL,
  `TX_NAME` varchar(20) NOT NULL,
  `TX_TEMPLATE_DRT` text NOT NULL,
  PRIMARY KEY  (`NU_RULE_TEMPLATE_ID`),
  UNIQUE KEY `NU_RULE_TEMPLATE_ID` (`NU_RULE_TEMPLATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_RULE_TEMPLATES`
--

LOCK TABLES `EM_RULE_TEMPLATES` WRITE;
/*!40000 ALTER TABLE `EM_RULE_TEMPLATES` DISABLE KEYS */;
INSERT INTO `EM_RULE_TEMPLATES` VALUES (4,'Discount by Country and Price Type','Country','template header\r\nRULE_ID\r\nRULE_NAME\r\nNU_COUNTRY\r\nPRICE_TYPE\r\nVARIATION_TYPE\r\nDISCOUNT\r\n\r\npackage org.slasoi.businessManager.common.drools;\r\n\r\nimport org.slasoi.businessManager.common.model.EmCustomersProducts;\r\nimport org.slasoi.businessManager.common.model.EmPromotions;\r\nimport org.slasoi.businessManager.common.model.EmComponentPrice;\r\nimport org.slasoi.businessManager.common.model.EmPriceVariationType;\r\nimport org.slasoi.businessManager.common.model.EmPriceVariation;\r\nimport java.math.BigDecimal;\r\n\r\ntemplate \"Date Range Promotion Template\"\r\n\r\nrule \"Rule @{RULE_ID} - @{RULE_NAME}\"\r\n	when\r\n		cp : EmComponentPrice()\r\n		customer_product : EmCustomersProducts()\r\n		eval (customer_product.getEmParty().getNuCountry()==@{NU_COUNTRY})\r\n		eval (cp.getEmPriceType().getNuIdpriceType()==@{PRICE_TYPE})\r\n	then\r\n		EmPromotions promo = new EmPromotions(new Long(@{RULE_ID}));\r\n		EmPriceVariationType pvt = new EmPriceVariationType(new Long(@{VARIATION_TYPE}));\r\n		EmPriceVariation pv = new EmPriceVariation(promo, pvt, cp, customer_product, \"@{RULE_NAME}\", new BigDecimal(1),new BigDecimal(@{DISCOUNT}) );\r\n		customer_product.getEmPriceVariations().add(pv);\r\nend\r\nend template\r\n'),(8,'Discount by Date Range and Price Type','Date Range','template header\nRULE_ID\nRULE_NAME\nDATE_BEGIN\nDATE_END\nPRICE_TYPE\nVARIATION_TYPE\nDISCOUNT\n\npackage org.slasoi.businessManager.common.drools;\n\nimport org.slasoi.businessManager.common.util.SimpleDate;\nimport org.slasoi.businessManager.common.model.EmCustomersProducts;\nimport org.slasoi.businessManager.common.model.EmComponentPrice;\nimport org.slasoi.businessManager.common.model.EmPromotions;\nimport org.slasoi.businessManager.common.model.EmPriceVariationType;\nimport org.slasoi.businessManager.common.model.EmPriceVariation;\nimport java.math.BigDecimal;\n\ntemplate \"Date Range Promotion Template\"\n\nrule \"Rule @{RULE_ID} - @{RULE_NAME}\"\n	when\n		cp : EmComponentPrice()\n		customer_product : EmCustomersProducts()\n		eval (new SimpleDate(cp.getDtValidFrom()).isBetwen(\"@{DATE_BEGIN}\",\"@{DATE_END}\"))\n		eval (cp.getEmPriceType().getNuIdpriceType()==@{PRICE_TYPE})\n	then\n		EmPriceVariationType pvt = new EmPriceVariationType(new Long(@{VARIATION_TYPE}));\n		EmPromotions promo = new EmPromotions(new Long(@{RULE_ID}));\n		EmPriceVariation pv = new EmPriceVariation(promo, pvt, cp, customer_product, \"@{RULE_NAME}\", new BigDecimal(1), new BigDecimal(@{DISCOUNT}));\n		customer_product.getEmPriceVariations().add(pv);\n\nend\nend template\n			'),(9,'Percentage change in GT and Price','Policy percentage','template header\nRULE_ID\nRULE_NAME\nMODIF_TYPE\nPRICE_TYPE\nSLSP\nVARIATION_MIN\nVARIATION_MAX\nVARIATION_TYPE\nPRICE_VARIATION\n\n\npackage es.tid.emarketplace.drools;\n\nimport org.slasoi.businessManager.common.model.EmPolicies;\nimport org.slasoi.businessManager.common.model.EmSpServices;\nimport org.slasoi.businessManager.common.model.EmComponentPrice;\nimport org.slasoi.businessManager.common.model.EmPriceVariationType;\nimport org.slasoi.businessManager.common.model.EmPriceVariation;\nimport org.slasoi.businessManager.common.util.GTDifference;\nimport java.math.BigDecimal;\n\n\nimport java.lang.Long;\n\ntemplate \"Policy Template\"\n\nrule \"Rule @{RULE_ID} - @{RULE_NAME}\"\n	when\n		service : EmSpServices()\n		cp : EmComponentPrice()\n		gtDif : GTDifference()\n		\n		eval(cp.getEmPriceType().getNuIdpriceType()==@{PRICE_TYPE})	\n		eval(service.getTxTemplateServiceid().equals(gtDif.getServiceName().trim()))\n		eval(gtDif.getParameter().equals(\"@{SLSP}\"))\n		eval(gtDif.getModificationType().equals(\"@{MODIF_TYPE}\"))\n		eval(gtDif.getPercentage()>=@{VARIATION_MIN})\n		eval(gtDif.getPercentage()<=@{VARIATION_MAX})\n		\n	then\n		EmPolicies policy = new EmPolicies(new Long(@{RULE_ID}));\n		EmPriceVariationType pvt = new EmPriceVariationType(new Long(@{VARIATION_TYPE}));\n		EmPriceVariation pv = new EmPriceVariation(policy, pvt, cp, \"@{RULE_NAME}\", new BigDecimal(1),new BigDecimal(@{PRICE_VARIATION}) );\n		cp.getEmPriceVariations().add(pv);\n		System.out.println(\"Se aplica @{RULE_NAME}\");\n\nend\nend template'),(11,'Template Socioeconomic Customer Level','Socioeconomic','template header\nRULE_ID\nRULE_NAME\nSOCIOECONOMIC_LEVEL\nPRICE_TYPE\nVARIATION_TYPE\nDISCOUNT\n\npackage org.slasoi.businessManager.common.drools;\n\nimport org.slasoi.businessManager.common.model.EmCustomersProducts;\nimport org.slasoi.businessManager.common.model.EmPromotions;\nimport org.slasoi.businessManager.common.model.EmComponentPrice;\nimport org.slasoi.businessManager.common.model.EmPriceVariationType;\nimport org.slasoi.businessManager.common.model.EmPriceVariation;\nimport org.slasoi.businessManager.common.bssCustomerProfile.beans.BssCustomerProfile;\n\nimport java.math.BigDecimal;\n\ntemplate \"Date Range Promotion Template\"\n\nrule \"Rule @{RULE_ID} - @{RULE_NAME}\"\n	when\n		cp : EmComponentPrice()\n		customer_product : EmCustomersProducts()\n		customer_profile: BssCustomerProfile()\n		eval (customer_profile.getFeature(\"v1:segment\").equals(\"@{SOCIOECONOMIC_LEVEL}\"))\n		eval (cp.getEmPriceType().getNuIdpriceType()==@{PRICE_TYPE})\n	then\n		EmPromotions promo = new EmPromotions(new Long(@{RULE_ID}));\n		EmPriceVariationType pvt = new EmPriceVariationType(new Long(@{VARIATION_TYPE}));\n		EmPriceVariation pv = new EmPriceVariation(promo, pvt, cp, customer_product, \"@{RULE_NAME}\", new BigDecimal(1),new BigDecimal(@{DISCOUNT}) );\n		customer_product.getEmPriceVariations().add(pv);\n		System.out.println(\"Se lanza la promocion Socioeconomic\");\n\nend\nend template\n'),(12,'Template for range values policies','Policy Range Values','template header\r\nRULE_ID\r\nRULE_NAME\r\nMODIF_TYPE\r\nPRICE_TYPE\r\nSLSP\r\nVALUE_MIN\r\nVALUE_MAX\r\nVARIATION_TYPE\r\nPRICE_VARIATION\r\n\r\n\r\npackage es.tid.emarketplace.drools;\r\n\r\nimport org.slasoi.businessManager.common.model.EmPolicies;\r\nimport org.slasoi.businessManager.common.model.EmSpServices;\r\nimport org.slasoi.businessManager.common.model.EmComponentPrice;\r\nimport org.slasoi.businessManager.common.model.EmPriceVariationType;\r\nimport org.slasoi.businessManager.common.model.EmPriceVariation;\r\nimport org.slasoi.businessManager.common.util.GTDifference;\r\nimport java.math.BigDecimal;\r\n\r\n\r\nimport java.lang.Long;\r\n\r\ntemplate \"Policy Template\"\r\n\r\nrule \"Rule @{RULE_ID} - @{RULE_NAME}\"\r\n	when\r\n		service : EmSpServices()\r\n		cp : EmComponentPrice()\r\n		gtDif : GTDifference()\r\n\r\n		eval(cp.getEmPriceType().getNuIdpriceType()==@{PRICE_TYPE})	\r\n		eval(service.getTxTemplateServiceid().equals(gtDif.getServiceName().trim()))\r\n		eval(gtDif.getParameter().equals(\"@{SLSP}\"))\r\n		eval(gtDif.getModificationType().equals(\"@{MODIF_TYPE}\"))\r\n		eval(gtDif.getValue()>=@{VALUE_MIN})\r\n		eval(gtDif.getValue()<=@{VALUE_MAX})\r\n\r\n		\r\n	then\r\n		EmPolicies policy = new EmPolicies(new Long(@{RULE_ID}));\r\n		EmPriceVariationType pvt = new EmPriceVariationType(new Long(@{VARIATION_TYPE}));\r\n		EmPriceVariation pv = new EmPriceVariation(policy, pvt, cp, \"@{RULE_NAME}\", new BigDecimal(1),new BigDecimal(@{PRICE_VARIATION}) );\r\n		cp.getEmPriceVariations().add(pv);\r\n		System.out.println(\"Se aplica @{RULE_NAME}\");\r\n\r\nend\r\nend template');
/*!40000 ALTER TABLE `EM_RULE_TEMPLATES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SERVICE_CATEGORIES`
--

DROP TABLE IF EXISTS `EM_SERVICE_CATEGORIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SERVICE_CATEGORIES` (
  `NU_IDCATEGORY` bigint(20) NOT NULL,
  `NU_SERVICEID` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_IDCATEGORY`,`NU_SERVICEID`),
  KEY `FKDF2022FD2B70AE8` (`NU_IDCATEGORY`),
  KEY `FKDF2022FD290B13C` (`NU_SERVICEID`),
  CONSTRAINT `EM_SERVICE_CATEGORIES_ibfk_1` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKDF2022FD2B70AE8` FOREIGN KEY (`NU_IDCATEGORY`) REFERENCES `EM_SERVICE_SPECIFICATION` (`NU_IDCATEGORY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SERVICE_CATEGORIES`
--

LOCK TABLES `EM_SERVICE_CATEGORIES` WRITE;
/*!40000 ALTER TABLE `EM_SERVICE_CATEGORIES` DISABLE KEYS */;
INSERT INTO `EM_SERVICE_CATEGORIES` VALUES (1,25),(1,26),(1,27),(14,25),(14,26),(14,27);
/*!40000 ALTER TABLE `EM_SERVICE_CATEGORIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `EM_SERVICE_RATING`
--

DROP TABLE IF EXISTS `EM_SERVICE_RATING`;
/*!50001 DROP VIEW IF EXISTS `EM_SERVICE_RATING`*/;
/*!50001 CREATE TABLE `EM_SERVICE_RATING` (
  `NU_SERVICEID` bigint(20),
  `NUM_RATES` bigint(21),
  `SATISFACTION_RATE` decimal(53,8)
) ENGINE=MyISAM */;

--
-- Table structure for table `EM_SERVICE_SPECIFICATION`
--

DROP TABLE IF EXISTS `EM_SERVICE_SPECIFICATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SERVICE_SPECIFICATION` (
  `NU_IDCATEGORY` bigint(20) NOT NULL auto_increment,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`NU_IDCATEGORY`),
  UNIQUE KEY `NU_IDCATEGORY` (`NU_IDCATEGORY`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SERVICE_SPECIFICATION`
--

LOCK TABLES `EM_SERVICE_SPECIFICATION` WRITE;
/*!40000 ALTER TABLE `EM_SERVICE_SPECIFICATION` DISABLE KEYS */;
INSERT INTO `EM_SERVICE_SPECIFICATION` VALUES (1,'Mobile Service','Mobile'),(2,'Text Messaging Service','Messaging'),(4,'Contents: Audio, video, text, Enriched text Service','Multimedia'),(5,'Audio Service','Audio'),(6,'Video Service','Video'),(7,'Voice Service','Voice'),(8,'IP Voice','IP Voice'),(12,'Infrastructure Service','Infrastructure'),(13,'Fixed Line Service','Fixed'),(14,'Internet Service','Internet'),(15,'Leisure Time Service','Leisure Time'),(16,'IP Television Service','IPTV'),(21,'Gaming Service','Gaming'),(22,'USB connection','USB'),(26,'Generic connection','Generic');
/*!40000 ALTER TABLE `EM_SERVICE_SPECIFICATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SERVICE_TYPE`
--

DROP TABLE IF EXISTS `EM_SERVICE_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SERVICE_TYPE` (
  `NU_SERVICE_TYPE` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(40) NOT NULL,
  PRIMARY KEY  (`NU_SERVICE_TYPE`),
  UNIQUE KEY `NU_SERVICE_TYPE` (`NU_SERVICE_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SERVICE_TYPE`
--

LOCK TABLES `EM_SERVICE_TYPE` WRITE;
/*!40000 ALTER TABLE `EM_SERVICE_TYPE` DISABLE KEYS */;
INSERT INTO `EM_SERVICE_TYPE` VALUES (1,'Service'),(2,'Foreing Service'),(3,'Application'),(4,'Foreign Application'),(6,'Gadget'),(7,'Back-end Service'),(9,'BPEL service');
/*!40000 ALTER TABLE `EM_SERVICE_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SERVS_SERVS`
--

DROP TABLE IF EXISTS `EM_SERVS_SERVS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SERVS_SERVS` (
  `NU_SERVICEID` bigint(20) NOT NULL,
  `NU_SERVICEID_MAIN` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`NU_SERVICEID`,`NU_SERVICEID_MAIN`),
  KEY `NU_SERVICEID_MAIN` (`NU_SERVICEID_MAIN`),
  CONSTRAINT `EM_SERVS_SERVS_ibfk_2` FOREIGN KEY (`NU_SERVICEID_MAIN`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`),
  CONSTRAINT `EM_SERVS_SERVS_ibfk_1` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SERVS_SERVS`
--

LOCK TABLES `EM_SERVS_SERVS` WRITE;
/*!40000 ALTER TABLE `EM_SERVS_SERVS` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SERVS_SERVS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SERVS_SLSP`
--

DROP TABLE IF EXISTS `EM_SERVS_SLSP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SERVS_SLSP` (
  `NU_SERVICEID` bigint(20) NOT NULL auto_increment,
  `NU_SLSP_ID` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`NU_SERVICEID`,`NU_SLSP_ID`),
  KEY `NU_SLSP_ID` (`NU_SLSP_ID`),
  CONSTRAINT `EM_SERVS_SLSP_ibfk_3` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `EM_SERVS_SLSP_ibfk_2` FOREIGN KEY (`NU_SLSP_ID`) REFERENCES `EM_SLSP` (`NU_SLSP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SERVS_SLSP`
--

LOCK TABLES `EM_SERVS_SLSP` WRITE;
/*!40000 ALTER TABLE `EM_SERVS_SLSP` DISABLE KEYS */;
INSERT INTO `EM_SERVS_SLSP` VALUES (25,1),(26,1),(27,1),(25,22),(26,22),(27,22),(25,23),(26,23),(27,23);
/*!40000 ALTER TABLE `EM_SERVS_SLSP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SLAMANAGERS`
--

DROP TABLE IF EXISTS `EM_SLAMANAGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SLAMANAGERS` (
  `NU_SLAMANAGERID` bigint(20) NOT NULL auto_increment,
  `TX_NAME` varchar(20) NOT NULL,
  `TX_LAYER_TYPE` varchar(30) NOT NULL,
  `TX_ADDRESS` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`NU_SLAMANAGERID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SLAMANAGERS`
--

LOCK TABLES `EM_SLAMANAGERS` WRITE;
/*!40000 ALTER TABLE `EM_SLAMANAGERS` DISABLE KEYS */;
INSERT INTO `EM_SLAMANAGERS` VALUES (1,'SLA Manager','SW','http://localhost:8080/services/SWNegotiation?wsdl'),(2,'SLA Manager','BUSINESS','http://localhost:8080/services/BZNegotiation?wsdl');
/*!40000 ALTER TABLE `EM_SLAMANAGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SLSP`
--

DROP TABLE IF EXISTS `EM_SLSP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SLSP` (
  `NU_SLSP_ID` bigint(20) NOT NULL auto_increment,
  `TX_SLSP_NAME` varchar(255) default NULL,
  PRIMARY KEY  (`NU_SLSP_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SLSP`
--

LOCK TABLES `EM_SLSP` WRITE;
/*!40000 ALTER TABLE `EM_SLSP` DISABLE KEYS */;
INSERT INTO `EM_SLSP` VALUES (1,'availability'),(2,'accessibility'),(3,'arrival_rate'),(4,'data_volume'),(5,'throughput'),(6,'completion_time'),(7,'mttr'),(8,'mttf'),(9,'mttv'),(10,'reliability'),(11,'isolation'),(12,'accuracy'),(13,'non_repudiation'),(14,'supported_standards'),(15,'regulatory'),(16,'integrity'),(17,'authentication'),(18,'auditability'),(19,'authorisation'),(20,'data_encryption'),(21,'bandwidth'),(22,'duration'),(23,'latency'),(24,'count'),(25,'vm_cores'),(26,'cpu_speed'),(27,'memory'),(28,'vm_image');
/*!40000 ALTER TABLE `EM_SLSP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_OFFER_RESTRIC`
--

DROP TABLE IF EXISTS `EM_SP_OFFER_RESTRIC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_OFFER_RESTRIC` (
  `NU_IDPRODUCTOFFERING` bigint(20) NOT NULL,
  `NU_RESTRICTION_ID` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_IDPRODUCTOFFERING`,`NU_RESTRICTION_ID`),
  KEY `FK4008E9EA6745D116` (`NU_RESTRICTION_ID`),
  KEY `FK4008E9EA2E546354` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `FK4008E9EA2E546354` FOREIGN KEY (`NU_IDPRODUCTOFFERING`) REFERENCES `EM_SP_PRODUCTS_OFFER` (`NU_IDPRODUCTOFFERING`),
  CONSTRAINT `FK4008E9EA6745D116` FOREIGN KEY (`NU_RESTRICTION_ID`) REFERENCES `EM_RESTRICTIONS` (`NU_RESTRICTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_OFFER_RESTRIC`
--

LOCK TABLES `EM_SP_OFFER_RESTRIC` WRITE;
/*!40000 ALTER TABLE `EM_SP_OFFER_RESTRIC` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SP_OFFER_RESTRIC` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_PRODUCTS`
--

DROP TABLE IF EXISTS `EM_SP_PRODUCTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_PRODUCTS` (
  `NU_PRODUCTID` bigint(20) NOT NULL auto_increment,
  `DT_INSERTDATE` datetime default NULL,
  `DT_VALIDFROM` date default NULL,
  `DT_VALIDTO` date default NULL,
  `TX_BRAND` varchar(40) default NULL,
  `TX_PRODUCTDESC` varchar(100) default NULL,
  `TX_PRODUCTNAME` varchar(40) default NULL,
  `TX_RELEASE` varchar(20) default NULL,
  `TX_STATUS` varchar(1) default NULL,
  `NU_PARTYID` bigint(20) default NULL,
  `NU_MAX_PENALTIES_CUST` bigint(20) default NULL,
  `NU_MAX_PENALTY_CANCEL` bigint(20) default NULL,
  `NU_MAX_PENALTY_REMOVE` bigint(20) default NULL,
  `NU_UNSUCCESS_ROUNDS_NEG` bigint(20) default NULL,
  `NU_MAX_PRICE_DECR` bigint(20) default NULL,
  `TX_NEGOTIATION_TYPE` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY  (`NU_PRODUCTID`),
  UNIQUE KEY `NU_PRODUCTID` (`NU_PRODUCTID`),
  KEY `FKE6CEBEFB47E1006` (`NU_PARTYID`),
  CONSTRAINT `FKE6CEBEFB47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_PRODUCTS`
--

LOCK TABLES `EM_SP_PRODUCTS` WRITE;
/*!40000 ALTER TABLE `EM_SP_PRODUCTS` DISABLE KEYS */;
INSERT INTO `EM_SP_PRODUCTS` VALUES (14,'2010-11-12 09:59:03','2010-11-12','2010-12-31','MOVISTAR','Product Test 1','Product Test 1','1','A',1,10,10,10,10,40);
/*!40000 ALTER TABLE `EM_SP_PRODUCTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_PRODUCTS_OFFER`
--

DROP TABLE IF EXISTS `EM_SP_PRODUCTS_OFFER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_PRODUCTS_OFFER` (
  `NU_IDPRODUCTOFFERING` bigint(20) NOT NULL auto_increment,
  `DT_VALID_FROM` date NOT NULL,
  `DT_VALID_TO` date NOT NULL,
  `TX_DESCRIPTION` varchar(100) default NULL,
  `TX_NAME` varchar(40) NOT NULL,
  `TX_REVISIONID` varchar(20) NOT NULL,
  `TX_STATUS` varchar(1) NOT NULL,
  `NU_BILLING_FRECUENCY_ID` bigint(20) default NULL,
  `NU_PRODUCTID` bigint(20) default NULL,
  `TX_BSLATID` varchar(256) default NULL,
  PRIMARY KEY  (`NU_IDPRODUCTOFFERING`),
  UNIQUE KEY `NU_IDPRODUCTOFFERING` (`NU_IDPRODUCTOFFERING`),
  KEY `FKEF5881EC25E1C284` (`NU_BILLING_FRECUENCY_ID`),
  KEY `FKEF5881EC4168D1FC` (`NU_PRODUCTID`),
  CONSTRAINT `EM_SP_PRODUCTS_OFFER_ibfk_1` FOREIGN KEY (`NU_PRODUCTID`) REFERENCES `EM_SP_PRODUCTS` (`NU_PRODUCTID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKEF5881EC25E1C284` FOREIGN KEY (`NU_BILLING_FRECUENCY_ID`) REFERENCES `EM_BILLING_FRECUENCY` (`NU_BILLING_FRECUENCY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_PRODUCTS_OFFER`
--

LOCK TABLES `EM_SP_PRODUCTS_OFFER` WRITE;
/*!40000 ALTER TABLE `EM_SP_PRODUCTS_OFFER` DISABLE KEYS */;
INSERT INTO `EM_SP_PRODUCTS_OFFER` VALUES (61,'2010-11-12','2012-12-31','Product Offer Test 1','Product Offer Test 1','1','A',1,14,'ORCBUSINESSSLAT1'),(62,'2010-11-12','2012-12-31','Product Offer Test 2','Product Offer Test 2','1','A',1,14,'ORCBUSINESSSLAT1'),(63,'2010-11-12','2012-12-31','Product Offer Test 3','Product Offer Test 3','1','A',1,14,'ORCBUSINESSSLAT1');
/*!40000 ALTER TABLE `EM_SP_PRODUCTS_OFFER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERVICES`
--

DROP TABLE IF EXISTS `EM_SP_SERVICES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERVICES` (
  `NU_SERVICEID` bigint(20) NOT NULL auto_increment,
  `NU_PARTYID` bigint(20) NOT NULL default '0',
  `TX_SERVICENAME` varchar(40) NOT NULL default '',
  `TX_SERVICEDESC` varchar(100) default NULL,
  `TC_SERVICETYPE` char(1) NOT NULL default '',
  `TX_RELEASE` varchar(4) NOT NULL default '',
  `DT_INSERTDATE` datetime default NULL,
  `NU_SERVICE_TYPE` bigint(20) default NULL,
  `TX_SLATID` varchar(40) default NULL,
  `NU_SLAMANAGERID` bigint(20) default NULL,
  `TX_TEMPLATE_SERVICEID` varchar(255) default NULL,
  PRIMARY KEY  (`NU_SERVICEID`),
  UNIQUE KEY `NU_SERVICEID` (`NU_SERVICEID`),
  KEY `FK9C762AE942048D62` (`NU_SERVICE_TYPE`),
  KEY `FK9C762AE9B47E1006` (`NU_PARTYID`),
  KEY `NU_SLAMANAGERID` (`NU_SLAMANAGERID`),
  CONSTRAINT `EM_SP_SERVICES_ibfk_1` FOREIGN KEY (`NU_SLAMANAGERID`) REFERENCES `EM_SLAMANAGERS` (`NU_SLAMANAGERID`),
  CONSTRAINT `FK9C762AE942048D62` FOREIGN KEY (`NU_SERVICE_TYPE`) REFERENCES `EM_SERVICE_TYPE` (`NU_SERVICE_TYPE`),
  CONSTRAINT `FK9C762AE9B47E1006` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERVICES`
--

LOCK TABLES `EM_SP_SERVICES` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERVICES` DISABLE KEYS */;
INSERT INTO `EM_SP_SERVICES` VALUES (25,1,'ORC Service 1','ORC Service 1','S','1','2010-11-12 09:49:55',1,'ORCBUSINESS_SW_SLAT1',1,'ORCPaymentService'),(26,1,'ORC Service 2','ORC Service 2','S','1','2010-11-12 09:50:36',1,'ORCBUSINESS_SW_SLAT1',1,'ORCPaymentService'),(27,1,'ORC Service 3','ORC Service 3','S','1','2010-11-12 09:52:15',1,'ORCBUSINESS_SW_SLAT1',1,'ORCPaymentService');
/*!40000 ALTER TABLE `EM_SP_SERVICES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERVICES_ATTRIBUTES`
--

DROP TABLE IF EXISTS `EM_SP_SERVICES_ATTRIBUTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERVICES_ATTRIBUTES` (
  `NU_SERVICEID` bigint(20) NOT NULL,
  `NU_ATTRIBUTEID` bigint(20) NOT NULL,
  PRIMARY KEY  (`NU_SERVICEID`,`NU_ATTRIBUTEID`),
  KEY `NU_ATTRIBUTEID` (`NU_ATTRIBUTEID`),
  CONSTRAINT `EM_SP_SERVICES_ATTRIBUTES_ibfk_3` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `EM_SP_SERVICES_ATTRIBUTES_ibfk_2` FOREIGN KEY (`NU_ATTRIBUTEID`) REFERENCES `EM_ATTRIBUTES` (`NU_ATTRIBUTEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='InnoDB free: 11264 kB; (`NU_SERVICEID`) REFER `SLASOI_AUX/EM';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERVICES_ATTRIBUTES`
--

LOCK TABLES `EM_SP_SERVICES_ATTRIBUTES` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERVICES_ATTRIBUTES` DISABLE KEYS */;
INSERT INTO `EM_SP_SERVICES_ATTRIBUTES` VALUES (25,1),(26,1),(27,1),(25,2),(26,2),(27,2),(25,3),(26,3),(27,3);
/*!40000 ALTER TABLE `EM_SP_SERVICES_ATTRIBUTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERVICES_CHARACTERISTIC`
--

DROP TABLE IF EXISTS `EM_SP_SERVICES_CHARACTERISTIC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERVICES_CHARACTERISTIC` (
  `NU_SERVICEID` bigint(20) NOT NULL,
  `NU_CHARACTERISTIC_ID` bigint(20) NOT NULL default '0',
  `TX_VALUE` varchar(100) default NULL,
  PRIMARY KEY  (`NU_SERVICEID`,`NU_CHARACTERISTIC_ID`),
  KEY `NU_CHARACTERISTIC_ID` (`NU_CHARACTERISTIC_ID`),
  CONSTRAINT `EM_SP_SERVICES_CHARACTERISTIC_ibfk_3` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`),
  CONSTRAINT `EM_SP_SERVICES_CHARACTERISTIC_ibfk_2` FOREIGN KEY (`NU_CHARACTERISTIC_ID`) REFERENCES `EM_CHARACTERISTIC` (`NU_CHARACTERISTIC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERVICES_CHARACTERISTIC`
--

LOCK TABLES `EM_SP_SERVICES_CHARACTERISTIC` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERVICES_CHARACTERISTIC` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SP_SERVICES_CHARACTERISTIC` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERVICES_CONTRACT`
--

DROP TABLE IF EXISTS `EM_SP_SERVICES_CONTRACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERVICES_CONTRACT` (
  `NU_SERVICEID` bigint(20) NOT NULL auto_increment,
  `NU_PARTYID` bigint(20) NOT NULL default '0',
  `TX_SPSLAID` varchar(40) NOT NULL default '',
  `DT_CONTRACT_DATE` date default NULL,
  PRIMARY KEY  (`NU_SERVICEID`,`NU_PARTYID`,`TX_SPSLAID`),
  KEY `NU_PARTYID` (`NU_PARTYID`),
  CONSTRAINT `EM_SP_SERVICES_CONTRACT_ibfk_1` FOREIGN KEY (`NU_PARTYID`) REFERENCES `EM_PARTY` (`NU_PARTYID`),
  CONSTRAINT `EM_SP_SERVICES_CONTRACT_ibfk_2` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERVICES_CONTRACT`
--

LOCK TABLES `EM_SP_SERVICES_CONTRACT` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERVICES_CONTRACT` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SP_SERVICES_CONTRACT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERV_SATISFACTION_RATE`
--

DROP TABLE IF EXISTS `EM_SP_SERV_SATISFACTION_RATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERV_SATISFACTION_RATE` (
  `NU_SERVICEID` bigint(20) NOT NULL,
  `NU_ATTRIBUTEID` bigint(20) NOT NULL,
  `TX_BSLAID` varchar(40) NOT NULL default '0',
  `NU_SATISFACTION_RATE` bigint(20) NOT NULL,
  `TX_REVIEW_COMMENT` varchar(255) default NULL,
  PRIMARY KEY  (`NU_SERVICEID`,`NU_ATTRIBUTEID`,`TX_BSLAID`),
  KEY `FK_EM_SP_SERV_SATISFACTION_RATE_2` (`TX_BSLAID`),
  KEY `FK_EM_SP_SERV_SATISFACTION_RATE_3` (`NU_ATTRIBUTEID`),
  CONSTRAINT `EM_SP_SERV_SATISFACTION_RATE_ibfk_3` FOREIGN KEY (`TX_BSLAID`) REFERENCES `EM_CUSTOMERS_PRODUCTS` (`TX_BSLAID`),
  CONSTRAINT `EM_SP_SERV_SATISFACTION_RATE_ibfk_1` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`),
  CONSTRAINT `EM_SP_SERV_SATISFACTION_RATE_ibfk_2` FOREIGN KEY (`NU_ATTRIBUTEID`) REFERENCES `EM_ATTRIBUTES` (`NU_ATTRIBUTEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERV_SATISFACTION_RATE`
--

LOCK TABLES `EM_SP_SERV_SATISFACTION_RATE` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERV_SATISFACTION_RATE` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SP_SERV_SATISFACTION_RATE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_SP_SERV_SAT_RATE_AGR`
--

DROP TABLE IF EXISTS `EM_SP_SERV_SAT_RATE_AGR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_SP_SERV_SAT_RATE_AGR` (
  `DT_DATE` date NOT NULL,
  `NU_SERVICEID` bigint(20) NOT NULL,
  `NU_ATTRIBUTEID` bigint(20) NOT NULL,
  `NU_SURVEYS` bigint(20) default NULL,
  `NU_AVG_RATE` decimal(10,2) default NULL,
  PRIMARY KEY  (`DT_DATE`,`NU_SERVICEID`,`NU_ATTRIBUTEID`),
  KEY `FK_EM_SP_SERV_SAT_RATE_AGR_1` (`NU_SERVICEID`,`NU_ATTRIBUTEID`),
  KEY `FK_EM_SP_SERV_SAT_RATE_AGR_2` (`NU_ATTRIBUTEID`),
  CONSTRAINT `EM_SP_SERV_SAT_RATE_AGR_ibfk_1` FOREIGN KEY (`NU_SERVICEID`) REFERENCES `EM_SP_SERVICES` (`NU_SERVICEID`),
  CONSTRAINT `EM_SP_SERV_SAT_RATE_AGR_ibfk_2` FOREIGN KEY (`NU_ATTRIBUTEID`) REFERENCES `EM_ATTRIBUTES` (`NU_ATTRIBUTEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_SP_SERV_SAT_RATE_AGR`
--

LOCK TABLES `EM_SP_SERV_SAT_RATE_AGR` WRITE;
/*!40000 ALTER TABLE `EM_SP_SERV_SAT_RATE_AGR` DISABLE KEYS */;
/*!40000 ALTER TABLE `EM_SP_SERV_SAT_RATE_AGR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EM_USERS`
--

DROP TABLE IF EXISTS `EM_USERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EM_USERS` (
  `NU_USERID` bigint(20) NOT NULL auto_increment,
  `TX_USER_LOGIN` varchar(10) NOT NULL default '',
  `TX_PASSWD` varchar(10) NOT NULL default '',
  `TC_DEPARTMENT_ROLE` varchar(1) default NULL,
  `NU_PARTYID` bigint(20) default NULL,
  `NU_PARTYROLEID` bigint(20) default NULL,
  `NU_INDIVIDUALID` bigint(20) default NULL,
  PRIMARY KEY  (`NU_USERID`),
  UNIQUE KEY `NU_USERID` (`NU_USERID`),
  KEY `NU_PARTYROLEID` (`NU_PARTYROLEID`),
  KEY `NU_INDIVIDUALID` (`NU_INDIVIDUALID`),
  KEY `NU_PARTYID` (`NU_PARTYID`,`NU_PARTYROLEID`),
  CONSTRAINT `EM_USERS_ibfk_4` FOREIGN KEY (`NU_PARTYID`, `NU_PARTYROLEID`) REFERENCES `EM_PARTY_PARTYROLE` (`NU_PARTYID`, `NU_PARTYROLEID`),
  CONSTRAINT `EM_USERS_ibfk_3` FOREIGN KEY (`NU_INDIVIDUALID`) REFERENCES `EM_INDIVIDUAL` (`NU_INDIVIDUALID`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EM_USERS`
--

LOCK TABLES `EM_USERS` WRITE;
/*!40000 ALTER TABLE `EM_USERS` DISABLE KEYS */;
INSERT INTO `EM_USERS` VALUES (1,'admin','admin','A',1,1,NULL),(20,'bm','bm','B',1,1,NULL),(21,'cm','cm','R',1,1,NULL),(22,'sm','sm','S',1,1,NULL);
/*!40000 ALTER TABLE `EM_USERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `EM_OFFER_RATING`
--

/*!50001 DROP TABLE `EM_OFFER_RATING`*/;
/*!50001 DROP VIEW IF EXISTS `EM_OFFER_RATING`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `EM_OFFER_RATING` AS (select `cp`.`NU_IDPRODUCTOFFERING` AS `NU_IDPRODUCTOFFERING`,count(0) AS `NUM_RATES`,(sum((`sat`.`NU_SATISFACTION_RATE` / ((`at`.`NU_MAX_VALUE` - (`at`.`NU_MIN_VALUE` - 1)) / 5))) / count(0)) AS `SATISFACTION_RATE` from (((`EM_ATTRIBUTES` `at` join `EM_SP_SERV_SATISFACTION_RATE` `sat`) join `EM_CUSTOMERS_PRODUCTS` `cp`) join `EM_PRODUCT_OFFER_SLAT` `offer`) where ((`at`.`NU_ATTRIBUTEID` = `sat`.`NU_ATTRIBUTEID`) and (`offer`.`NU_SERVICEID` = `sat`.`NU_SERVICEID`) and (`cp`.`NU_IDPRODUCTOFFERING` = `offer`.`NU_IDPRODUCTOFFERING`) and (`cp`.`TX_BSLAID` = `sat`.`TX_BSLAID`)) group by `cp`.`NU_IDPRODUCTOFFERING`) */;

--
-- Final view structure for view `EM_PRODUCT_RATING`
--

/*!50001 DROP TABLE `EM_PRODUCT_RATING`*/;
/*!50001 DROP VIEW IF EXISTS `EM_PRODUCT_RATING`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `EM_PRODUCT_RATING` AS (select `offer`.`NU_PRODUCTID` AS `NU_PRODUCTID`,count(0) AS `NUM_RATES`,(sum((`sat`.`NU_SATISFACTION_RATE` / ((`at`.`NU_MAX_VALUE` - (`at`.`NU_MIN_VALUE` - 1)) / 5))) / count(0)) AS `SATISFACTION_RATE` from ((((`EM_ATTRIBUTES` `at` join `EM_SP_SERV_SATISFACTION_RATE` `sat`) join `EM_CUSTOMERS_PRODUCTS` `cp`) join `EM_PRODUCT_OFFER_SLAT` `offer_slat`) join `EM_SP_PRODUCTS_OFFER` `offer`) where ((`at`.`NU_ATTRIBUTEID` = `sat`.`NU_ATTRIBUTEID`) and (`offer_slat`.`NU_SERVICEID` = `sat`.`NU_SERVICEID`) and (`offer`.`NU_IDPRODUCTOFFERING` = `offer_slat`.`NU_IDPRODUCTOFFERING`) and (`cp`.`NU_IDPRODUCTOFFERING` = `offer_slat`.`NU_IDPRODUCTOFFERING`) and (`cp`.`TX_BSLAID` = `sat`.`TX_BSLAID`)) group by `offer`.`NU_PRODUCTID`) */;

--
-- Final view structure for view `EM_SERVICE_RATING`
--

/*!50001 DROP TABLE `EM_SERVICE_RATING`*/;
/*!50001 DROP VIEW IF EXISTS `EM_SERVICE_RATING`*/;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `EM_SERVICE_RATING` AS (select `offer`.`NU_SERVICEID` AS `NU_SERVICEID`,count(0) AS `NUM_RATES`,(sum((`sat`.`NU_SATISFACTION_RATE` / ((`at`.`NU_MAX_VALUE` - (`at`.`NU_MIN_VALUE` - 1)) / 5))) / count(0)) AS `SATISFACTION_RATE` from (((`EM_ATTRIBUTES` `at` join `EM_SP_SERV_SATISFACTION_RATE` `sat`) join `EM_CUSTOMERS_PRODUCTS` `cp`) join `EM_PRODUCT_OFFER_SLAT` `offer`) where ((`at`.`NU_ATTRIBUTEID` = `sat`.`NU_ATTRIBUTEID`) and (`offer`.`NU_SERVICEID` = `sat`.`NU_SERVICEID`) and (`cp`.`NU_IDPRODUCTOFFERING` = `offer`.`NU_IDPRODUCTOFFERING`) and (`cp`.`TX_BSLAID` = `sat`.`TX_BSLAID`)) group by `offer`.`NU_SERVICEID`) */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;


-- -----------------------------------------------------
-- Table `SLASOI`.`pac_slaviolation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pac_slaviolation` ;

CREATE  TABLE IF NOT EXISTS `pac_slaviolation` (
  `violation_id` VARCHAR(45) NOT NULL ,
  `sla_id` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL ,
  `guaranteeterm_id` VARCHAR(45) NOT NULL ,
  `date_begin` DATETIME NOT NULL ,
  `date_end` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`violation_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `SLASOI`.`pac_penalty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pac_penalty` ;

CREATE  TABLE IF NOT EXISTS `pac_penalty` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `date` DATETIME NOT NULL ,
  `violation_id` VARCHAR(45) NOT NULL ,
  `value` FLOAT NOT NULL ,
  `obligated_party` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `violation_id` (`violation_id` ASC) ,
  CONSTRAINT `violation_id`
    FOREIGN KEY (`violation_id` )
    REFERENCES `pac_slaviolation` (`violation_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `SLASOI`.`pac_slawarning`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pac_slawarning` ;

CREATE  TABLE IF NOT EXISTS `pac_slawarning` (
  `warning_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `sla_id` VARCHAR(45) NULL DEFAULT NULL ,
  `date_warning` DATETIME NOT NULL ,
  `notification_id` VARCHAR(10) NOT NULL ,
  PRIMARY KEY (`warning_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-04-27 15:58:57
