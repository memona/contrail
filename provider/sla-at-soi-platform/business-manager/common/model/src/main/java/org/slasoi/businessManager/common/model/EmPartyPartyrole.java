/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/model/EmPartyPartyrole.java $
 */

package org.slasoi.businessManager.common.model;

// Generated 02-mar-2010 15:10:09 by Hibernate Tools 3.2.4.GA

import java.util.HashSet;
import java.util.Set;

/**
 * EmPartyPartyrole generated by hbm2java
 */
public class EmPartyPartyrole implements java.io.Serializable {

	private EmPartyPartyroleId id;
	private EmPartyRole emPartyRole;
	private EmParty emParty;
	private String txCustomerrank;
	private String txStatus;
	private String txRejectionCause;
	private Set<EmUsers> emUserses = new HashSet<EmUsers>(0);

	public EmPartyPartyrole() {
	}
	

	public EmPartyPartyrole(EmPartyPartyroleId id) {
		super();
		this.id = id;
	}


	public EmPartyPartyrole(EmPartyPartyroleId id, EmPartyRole emPartyRole,
			EmParty emParty) {
		this.id = id;
		this.emPartyRole = emPartyRole;
		this.emParty = emParty;
	}

	public EmPartyPartyrole(EmPartyPartyroleId id, EmPartyRole emPartyRole,
			EmParty emParty, String txCustomerrank, String txStatus,
			Set<EmUsers> emUserses) {
		this.id = id;
		this.emPartyRole = emPartyRole;
		this.emParty = emParty;
		this.txCustomerrank = txCustomerrank;
		this.txStatus = txStatus;
		this.emUserses = emUserses;
	}

	public EmPartyPartyroleId getId() {
		return this.id;
	}

	public void setId(EmPartyPartyroleId id) {
		this.id = id;
	}

	public EmPartyRole getEmPartyRole() {
		return this.emPartyRole;
	}

	public void setEmPartyRole(EmPartyRole emPartyRole) {
		this.emPartyRole = emPartyRole;
	}

	public EmParty getEmParty() {
		return this.emParty;
	}

	public void setEmParty(EmParty emParty) {
		this.emParty = emParty;
	}

	public String getTxCustomerrank() {
		return this.txCustomerrank;
	}

	public void setTxCustomerrank(String txCustomerrank) {
		this.txCustomerrank = txCustomerrank;
	}

	public String getTxStatus() {
		return this.txStatus;
	}

	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}
	
	public String getTxRejectionCause() {
		return this.txRejectionCause;
	}

	public void setTxRejectionCause(String txRejectionCause) {
		this.txRejectionCause = txRejectionCause;
	}
	

	public Set<EmUsers> getEmUserses() {
		return this.emUserses;
	}

	public void setEmUserses(Set<EmUsers> emUserses) {
		this.emUserses = emUserses;
	}

}
