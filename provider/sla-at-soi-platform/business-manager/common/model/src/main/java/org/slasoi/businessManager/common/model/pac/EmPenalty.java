/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/violation-penalty/src/main/java/org/slasoi/businessManager/violationPenalty/db/model/Penalty.java $
 */

package org.slasoi.businessManager.common.model.pac;

import java.util.Date;

public class EmPenalty implements java.io.Serializable{

    
    private static final long serialVersionUID = 8893313613987631015L;
    
    private long penaltyId;
    private Date penaltyDate;
    private EmSlaViolation violation;
    private float value;
    private String obligatedParty;
    private String description;

    public EmPenalty() {

    }

    public EmPenalty(long penaltyId, Date penaltyDate, EmSlaViolation violation, float value, String obligatedParty,
            String description) {
        super();
        this.penaltyId = penaltyId;
        this.penaltyDate = penaltyDate;
        this.violation = violation;
        this.value = value;
        this.obligatedParty = obligatedParty;
        this.description = description;
    }
    
    public EmPenalty(Date date, EmSlaViolation violation, float value, String obligatedParty, String description) {
        this.penaltyDate = date;
        this.violation = violation;
        this.value = value;
        this.obligatedParty = obligatedParty;
        this.description = description;
    }

    public long getPenaltyId() {
        return penaltyId;
    }

    public void setPenaltyId(long penaltyId) {
        this.penaltyId = penaltyId;
    }

    public Date getPenaltyDate() {
        return penaltyDate;
    }

    public void setPenaltyDate(Date penaltyDate) {
        this.penaltyDate = penaltyDate;
    }

    public EmSlaViolation getViolation() {
        return violation;
    }

    public void setViolation(EmSlaViolation violation) {
        this.violation = violation;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getObligatedParty() {
        return obligatedParty;
    }

    public void setObligatedParty(String obligatedParty) {
        this.obligatedParty = obligatedParty;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
