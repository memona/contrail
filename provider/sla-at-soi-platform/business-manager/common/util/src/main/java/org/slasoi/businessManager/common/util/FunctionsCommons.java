/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/FunctionsCommons.java $
 */

/*
 * 
 */
package org.slasoi.businessManager.common.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;



/**
 * The Class FunctionsCommons.
 */
public class FunctionsCommons {
	
	private static final Logger log = Logger.getLogger(FunctionsCommons.class);
	

	
	
	/**
	 * Gets the list to string.
	 * 
	 * @param charSplit the char split
	 * 
	 * @return the list to string
	 * 
	 * @throws Exception the exception
	 */
	public static List<String> getListToString(String charSplit)throws Exception{
		List<String> lList = new ArrayList<String>(0);
		try{
			String[] sCharSplit = charSplit.split(",");
			for(int i = 0;i<sCharSplit.length;i++){
				if(sCharSplit[i]!=null && sCharSplit[i].length()>0)
					lList.add(sCharSplit[i]);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception (e.getMessage());
	
		}
		return lList;
	}
	
	public static List<String> getListToStringPoint(String charSplit)throws Exception{
		List<String> lList = new ArrayList<String>(0);
		try{
			String[] sCharSplit = charSplit.split(":");
			for(int i = 0;i<sCharSplit.length;i++){
				if(sCharSplit[i]!=null && sCharSplit[i].length()>0)
					lList.add(sCharSplit[i]);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception (e.getMessage());
	
		}
		return lList;
	}	

	/**
	 * 
	 * @param charSplit: Array of Long (,) separated
	 * @return List of Long
	 * @throws Exception
	 */
	public static List<Long> getListToLong(String charSplit)throws Exception{
		List<Long> lList = new ArrayList<Long>(0);
		try{
			String[] sCharSplit = charSplit.split(",");
			for(int i = 0;i<sCharSplit.length;i++){
				if(sCharSplit[i]!=null && sCharSplit[i].length()>0)
					lList.add(new Long(sCharSplit[i]));
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception (e.getMessage());
	
		}
		return lList;
	}
	
	/**
	 * Fechas diferencia en dias.
	 * 
	 * @param fechaInicial the fecha inicial
	 * @param fechaFinal the fecha final
	 * 
	 * @return the int
	 */
	public static int fechasDiferenciaEnDias(Date fechaInicial, Date fechaFinal) {

		DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
		String fechaInicioString = df.format(fechaInicial);
		try {
		fechaInicial = df.parse(fechaInicioString);
		}
		catch (Exception ex) {
		}

		String fechaFinalString = df.format(fechaFinal);
		try {
		fechaFinal = df.parse(fechaFinalString);
		}
		catch (Exception ex) {
		}

		long fechaInicialMs = fechaInicial.getTime();
		long fechaFinalMs = fechaFinal.getTime();
		long diferencia = fechaFinalMs - fechaInicialMs;
		double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
		return ( (int) dias);
		}	
	
	/**
	 * Gets the name month.
	 * 
	 * @param iMonth the i month
	 * 
	 * @return the name month
	 */
	public static String getNameMonth(int iMonth){
		String[] aMonths = {"January","February","March","April","May","June","July","August","September","October","November","December"};
		String nameMonth = null;
		nameMonth = aMonths[iMonth-1];
		return nameMonth;
	}
	
	/**
	 * Gets the number decimal format.
	 * 
	 * @param numberForm the number form
	 * 
	 * @return the number decimal format
	 */
	public static  String getNumberDecimalFormat(String numberForm){
		String numberReturn = null;
		try{
			if(numberForm!=null && numberForm.contains(",")){
				numberForm = numberForm.replace(",", ".");
			}
			double numberFormDouble = new Double(numberForm);
		    NumberFormat formatter = new DecimalFormat("#0.00");
		    log.info("The Decimal Value is:"+formatter.format(numberFormDouble));	
		    numberReturn = formatter.format(numberFormDouble);
			if(numberReturn!=null && numberReturn.contains(",")){
				numberReturn = numberReturn.replace(",", ".");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return numberReturn;
	}	
	
	/**
	 * Validate string.
	 * 
	 * @param string the string
	 * 
	 * @return true, if successful
	 */
	public static boolean validateString(String string){
		boolean isValid = true;
		try{
			if(string==null || "".equals(string)){
				isValid = false;
			}					
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return isValid;
	}
	
	/**
	 * Validate long.
	 * 
	 * @param lon the lon
	 * 
	 * @return true, if successful
	 */
	public static boolean validateLong(long lon){
		boolean isValid = true;
		try{
			if(lon==0){
				isValid = false;
			}					
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return isValid;
	}		
}
