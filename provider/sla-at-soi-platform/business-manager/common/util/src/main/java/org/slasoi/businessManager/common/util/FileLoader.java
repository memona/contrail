/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author        Agustin Escamez - escamez@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */
package org.slasoi.businessManager.common.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Utility Class for loading resources from bundle, either
 * inside a jar or from the filesystem
 */
public final class FileLoader {

	private static final Logger logger = Logger.getLogger(FileLoader.class.getName());

	private FileLoader() {

	}

	/**
     * Loads the content of a file into a String
     * 
     * @param fileName The name of file to load
     * @return The content of a file as a byte array
     * @throws IOException 
     * @throws Exception
     */
    public static byte[] loadFileBytes(String fileName) throws Exception {

        // load File contents to String

        InputStream is = null;
        byte[] buffer = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int n = 0;

        try {
            // Checks for file in jar
            URL url = checkJarFile(fileName);

            if (url != null) {
                is = url.openStream();
                while (-1 != (n = is.read(buffer))) {
                    baos.write(buffer, 0, n);
                 }
                           
                return baos.toByteArray();
            }

            // in case the file is not in jar
            File file = checkFsFile(fileName);

            if (file != null) {
                is = new FileInputStream(file);
                while (-1 != (n = is.read(buffer))) {
                    baos.write(buffer, 0, n);
                 }
                           
                return baos.toByteArray();
            }
            
        } catch (IOException e) {
            
            throw new Exception(e);
            
        } finally {
            if (is != null) {
                is.close();
                is=null;
            }
        }

        throw new Exception(fileName + "NOT found");
    }
    
	/**
	 * Loads the content of a file into a String
	 * 
	 * @param fileName The name of fiel to load
	 * @return The content of a file as a String
	 * @throws IOException 
	 * @throws Exception
	 */
	public static String loadFileContent(String fileName) throws Exception {

		// load File contents to String

		StringBuilder buf = null;
		InputStream is = null;
		int i = 0;

		try {
			// Checks for file in jar
			URL url = checkJarFile(fileName);

			if (url != null) {
				is = url.openStream();
				buf = new StringBuilder();
				while (is.available() != 0 && (i = is.read()) != -1) {
					buf.append((char) i);
				}			
				return buf.toString();
			}

			// in case the file is not in jar
			File file = checkFsFile(fileName);

			if (file != null) {
				is = new FileInputStream(file);
				buf = new StringBuilder();
				while (is.available() != 0 && (i = is.read()) != -1) {
					buf.append((char) i);
				}				
				return buf.toString();
			}

		} catch (IOException e) {
			
			throw new Exception(e);
			
		} finally {
			if (is != null) {
				is.close();
				is=null;
			}
		}

		throw new Exception(fileName + "NOT found");
	}

	/**
	 * Checks the existence of file inside a jar
	 * 
	 * @param fileName The name of file to load
	 * @return The URL with the file location
	 */
	public static URL checkJarFile(String fileName) {

		// checks for a file location inside a jar file
		
		URL url = FileLoader.class.getClassLoader().getResource(fileName);

		if (url == null) {
			return null;
		} else {
			return url;
		}
	}

	/**
	 * Checks the existence of file in the filesystem, relative to the current execution dir
	 * 
	 * @param fileName The name of file to load
	 * @return The file found 
	 */
	public static File checkFsFile(String fileName) {

		// checks for a file location in the FS
		
		File file = new File(System.getProperty("app.dir")
				+ System.getProperty("file.separator") + fileName);

		if (!file.exists()) {
			return null;
		} else {
			return file;
		}
	}

	/**
	 * Loads the properties from a <b>properties</b> file
	 * 
	 * @param fileName The name of file to load
	 * @return The content of the file as a <b>properties</b> object
	 */
	public static Properties loadPropertiesFile(String fileName)
			throws Exception {

		// load properties from a .properties file
		
		Properties properties = null;
		URL url = checkJarFile(fileName);
		File file = checkFsFile(fileName);

		try {
			if (url != null) {
				properties = new Properties();
				properties.load(url.openStream());
			} else if (file != null) {
				properties = new Properties();
				properties.load(new FileInputStream(file));
			} else {
				logger.info("properties file: " + fileName + " NOT found");
				throw new Exception(fileName + " NOT Found");
			}
		} catch (IOException ioe) {
			throw new Exception("IOException loading " + fileName + "\t " + ioe);
		}

		logger.info(fileName + " file LOADED");

		return properties;
	}

}
