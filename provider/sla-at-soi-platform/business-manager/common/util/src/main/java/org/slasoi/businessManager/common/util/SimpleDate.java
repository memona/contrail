/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 809 $
 * @lastrevision   $Date: 2011-02-23 16:40:17 +0100 (Wed, 23 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/util/SimpleDate.java $
 */

package org.slasoi.businessManager.common.util;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
public class SimpleDate extends Date {
	
	private static final long serialVersionUID = 1L;
	static Logger log = Logger.getLogger(SimpleDate.class);
	private static final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
	
	public SimpleDate(){
	}
	
	public SimpleDate(Date date){
		this.setTime(date.getTime());
	}
	
    public SimpleDate(String datestr) throws Exception {             
        setTime(format.parse(datestr).getTime());
    }

    public boolean isBetwen(String fecha_ini, String fecha_fin) throws Exception{
    	try{
    		log.info("Comprobando "+fecha_ini+" < "+this.toString()+" < "+fecha_fin);
	    	SimpleDate ini = new SimpleDate(fecha_ini);
	    	SimpleDate fin = new SimpleDate(fecha_fin);
	    	return this.compareTo(ini)>=0 && this.compareTo(fin)<=0;  
	    	
		}catch (Exception e) {
			log.info("Error en el formato de las fechas ");
			throw new Exception("Formato de fecha incorrecto");
		}
    }
    
    public String toString(){
    	return format.format(this);
    }
    
    public String getDateFormat(){
    	SimpleDateFormat formatDT = new SimpleDateFormat("dd/MM/yyyy");
    	return formatDT.format(this);
    }

    public String getDateTime(){
    	SimpleDateFormat formatDT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    	return formatDT.format(this);
    }
    
    public boolean isGreaterOrEqual(String fecha){
    	try{
    		return (this.compareTo(new SimpleDate(fecha))<=0);
    	}catch (Exception e) {
    		return false;
		}
    }
    
    public boolean isBefore(String fecha){
    	try{
    		return this.before(new SimpleDate(fecha));
    	}catch (Exception e) {
			return false;
		}
    }
    
    public void addDays(int num) {
    	Calendar c = Calendar.getInstance();
		c.setTime(this);
		c.add(Calendar.DATE, num);
		this.setTime(c.getTime().getTime());
    }
        
    public void addMinutes(int num) {
    	Calendar c = Calendar.getInstance();
		c.setTime(this);
		c.add(Calendar.MINUTE, num);
		this.setTime(c.getTime().getTime());
    }
    
    public void setLastDayOfNextMonth(){
    	Calendar c = Calendar.getInstance();
		c.setTime(this);
		c.add(Calendar.MONTH, 1);
		int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		c.set(Calendar.DAY_OF_MONTH, lastDay);
		this.setTime(c.getTime().getTime());
    }
    
}
