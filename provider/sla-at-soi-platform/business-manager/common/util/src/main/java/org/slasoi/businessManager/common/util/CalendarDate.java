/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Description: <br/>
  *  Utility class to perform operations on Date objects, 
  *  such as add days, substract, convert String <-> Date,...
  *   
 * @author        Agustin Escamez - escamez@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */
package org.slasoi.businessManager.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/** 
 * <p>Enumeration of Custom Date formats</p>
 * <p>
 * G Era designator Text AD <br/>
 * y Year Year 1996; 96 <br/>
 * M Month in year Month July; Jul; 07 <br/>
 * w Week in year Number 27 <br/>
 * W Week in month Number 2 <br/>
 * D Day in year Number 189 <br/>
 * d Day in month Number 10 <br/>
 * F Day of week in month Number 2 <br/>
 * E Day in week Text Tuesday; Tue <br/>
 * a Am/pm marker Text PM <br/>
 * H Hour in day (0-23) Number 0 <br/>
 * k Hour in day (1-24) Number 24 <br/>
 * K Hour in am/pm (0-11) Number 0 <br/>
 * h Hour in am/pm (1-12) Number 12 <br/>
 * m Minute in hour Number 30 <br/>
 * s Second in minute Number 55 <br/>
 * S Millisecond Number 978 <br/>
 * </p>
 * 
 * <p>Formats Supported:</p>
 * <p>
 * Y4M2D2_HHMMSS 	   -> "yyyy/MM/dd HH:mm:ss" <br/>
 * Y4M2D2	 	   -> "yyyy/MM/dd" <br/>
 * D2M2Y4_HHMMSS 	   -> "dd/MM/yyyy HH:mm:ss" <br/>
 * D2M2Y4        	   -> "dd-MM-yyyy"          <br/>
 * D2BM2BY4      	   -> "dd/MM/yyyy"          <br/>
 * MONTH2_YEAR2  	   -> "MM-yy"               <br/>
 * MONTH2B_YEAR2  	   -> "MM/yy"               <br/>
 * DAY_NUMBER_IN_MONTH -> "dd"                  <br/>
 * MONTH_NUMBER 	   -> "MM"                  <br/>
 * MONTH_NAME          -> "MMMMM"               <br/>
 * YEAR4               -> "yyyy"                <br/>
 * DAY_OF_WEEK_NUMER   -> "F"                   <br/>
 * DAY_NAME_OF_WEEK    -> "EEEE"                <br/>
 * HOURS_MINUTES 	   -> "HH:mm"               <br/>
 * MYSQL_DATETIME      -> "yyyyMMddHHmmss"      <br/>
 * </p>
 * 
 */    
public enum CalendarDate {

	/** yyyy/MM/dd HH:mm:ss  */
	Y4M2D2_HHMMSS("yyyy/MM/dd HH:mm:ss"),
	
	/** yyyy/MM/dd */
	Y4M2D2("yyyy/MM/dd"),
	
	/** dd/MM/yyyy HH:mm:ss  */	
	D2M2Y4_HHMMSS("dd/MM/yyyy HH:mm:ss"),
	
	/** dd-MM-yyyy */
	D2M2Y4("dd-MM-yyyy"),
	
	/** dd/MM/yyyy */
	D2BM2BY4("dd/MM/yyyy"),
	
	/** MMM dd yyyy HH:mm:ss */
    M3_D2_Y4_HHMMSS("MMM dd yyyy HH:mm:ss"),
	
	/** MM-yy */
	MONTH2_YEAR2("MM-yy"),
	
	/** MM/yy */
	MONTH2B_YEAR2("MM-yy"),
	
	/** dd */
	DAY_NUMBER_IN_MONTH("dd"), 
	
	/** MM */
	MONTH_NUMBER("MM"),
	
	/** MMMMM */
	MONTH_NAME("MMMMM"),
	
	/** yyyy */
	YEAR4("yyyy"),
	
	/** F */
	DAY_OF_WEEK_NUMER("F"),
	
	/** EEEE */
	DAY_NAME_OF_WEEK("EEEE"),
	
	/** HH:mm */
	HOURS_MINUTES("HH:mm"),
	
	/** yyyyMMddHHmmss */
	MYSQL_DATETIME("yyyyMMddHHmmss");
		
	private String pattern;
	private static final SimpleDateFormat SDF = new SimpleDateFormat();	
	
    private static final GregorianCalendar CAL = 
    	(GregorianCalendar) GregorianCalendar.getInstance(Locale.getDefault());
    
    /**
     * Constant to convert from [ms] to days
     */
    static final long ms_TO_DAYS = 86400000;
    
    
	CalendarDate(String pattern) {
		this.pattern =pattern;
	}	
	
	/**
	 * Provides the associated patter to parse
	 * dates with this CalendarDate
	 *
	 * @return The pattern as a <code>String</code>
	 */
	public String getPattern(){
		return pattern;
	}
	
    
    /**
     * Provides a string with the representation of the supplied Date object
     * 
     * @param date Date to apply the pattern to
     * @return An String with the supplied date
     */
    public String getDateAsString(Date date){
        SDF.applyLocalizedPattern(pattern);
        return SDF.format(date);        
    }
	    
    /**
     * Checks the supplied string with pattern,
     * and returns a date object if it's correct
     *
     * @param date Date to check
     * @throws ParseException Error during parsing
     * @throws IllegalArgumentException Error in supplied date
     * @return A date as an object
     */
    public Date checkAndGetDate(String dateAsString) 
    throws IllegalArgumentException,ParseException    {
        
        SDF.applyLocalizedPattern(pattern);        
        SDF.setLenient(false);//Strict parsing set to false
        
        return SDF.parse(dateAsString);        
    }
    
    /**
     * Checks the supplied string with pattern,
     * and returns a date object if it's correct,
     * with day/month/year set to current date.<br/>
     * Time data is not changed
     *
     * @param date Date to check
     * @throws ParseException Error during parsing
     * @throws IllegalArgumentException Error in supplied date
     * @return A date as an object
     */
    public Date checkAndGetCurrentDate(String dateAsString) 
    throws IllegalArgumentException,ParseException    {
        
    	Date result=null;
    	
    	CAL.setTime(new Date());//to extract current data        
        int year = CAL.get(CAL.YEAR);
        int month = CAL.get(CAL.MONTH);
        int day = CAL.get(CAL.DATE);
        
        SDF.applyLocalizedPattern(pattern);        
        SDF.setLenient(false);//Strict parsing set to false
        result = SDF.parse(dateAsString);        
        
        //finally we set the desired values
        CAL.setTime(result);
        CAL.set(CAL.YEAR, year);
        CAL.set(CAL.MONTH, month);
        CAL.set(CAL.DATE, day);
        
        return CAL.getTime();
    }
    
    
    /**
     * Changes the time of the supplied date,
     * and returns a date object.
     *
     * @param date Date to change time
     * @param hours In 24 hour format, i.e. 17,23,...
     * @param minutes 0-59
     * @param seconds 0-59
     * @throws ParseException Error during parsing
     * @throws IllegalArgumentException Error in supplied date
     * @return A date as an object
     */
    public Date changeTime(Date date,int hours,int minutes,int seconds) 
    throws IllegalArgumentException,ParseException    {
        
    	CAL.setTime(date);        
    	CAL.set(CAL.HOUR_OF_DAY, hours);
    	CAL.set(CAL.MINUTE, minutes);
    	CAL.set(CAL.SECOND, seconds);
    	
        return CAL.getTime();
    }
    
    /**
     * Changes the time of the current date,
     * and returns the date.
     *
     * @param hours In 24 hour format, i.e. 17,23,...
     * @param minutes 0-59
     * @param seconds 0-59
     * @throws ParseException Error during parsing
     * @throws IllegalArgumentException Error in supplied date
     * @return A date as an object
     */
    public Date changeTime(int hours,int minutes,int seconds) 
    throws IllegalArgumentException,ParseException    {
        
    	CAL.setTime(new Date());        
    	CAL.set(CAL.HOUR_OF_DAY, hours);
    	CAL.set(CAL.MINUTE, minutes);
    	CAL.set(CAL.SECOND, seconds);
    	
        return CAL.getTime();
    }
    
    /* *************
     *   OPERATIONS
     * *************/
    
    /**
     * Provides the name of the day in the week
     * 
     * @param A date object
     * @return The name of the day in a week, monday, tuesday, ...
     */
    public static String getNameDayInWeek(Date date){
        SDF.applyLocalizedPattern(DAY_NAME_OF_WEEK.pattern);
        return SDF.format(date);
    }
    
    /**
     * Adds a given number of days to another date, and returns the new date
     * 
     * @param date Beginning date
     * @param numDays Num days to add to beginning date
     * 
     * @return The new date with added days
     */
    public static Date addDaysToDate(Date date, int numDays){
        
        CAL.setTime(date);
        CAL.add(GregorianCalendar.DAY_OF_MONTH,+numDays);
                
        date = CAL.getTime();
        CAL.clear();
        
        return date;
    }
    
    /**
     * Takes away a given number of days to another date, and returns the new date
     * 
     * @param date Beginning date
     * @param numDays Number of days to take away from beginning date
     * 
     * @return The new date with taken away days
     */
    public static Date takeAwayDaysFromDate(Date date, int numDays){
        
    	CAL.setTime(date);
    	CAL.add(GregorianCalendar.DAY_OF_MONTH,-numDays);
        
    	 date = CAL.getTime();
         CAL.clear();
        
        return date;
    }
    
    /**    
     * Provides the number of days between two dates
     * If result is negative, then date1 is bigger that date2
     * 
     * @param date1 Beginning date
     * @param fecha2 Last date
     * 
     * @return Number of days between dates 
     */
    public static int daysBetweenDates(Date date1, Date date2){        
        double diff = date2.getTime() - date1.getTime();
        int days = (int) Math.round(diff/ms_TO_DAYS);//to round up correctly
        
        return days;        
    }
    
    /**    
     * Provides the number of months between two dates
     * If result is negative, then date1 is bigger that date2
     * 
     * @param date1 Beginning date
     * @param fecha2 Last date
     * 
     * @return Number of months between dates 
     */
     public static int monthsBetweenDates(Date date1, Date date2){
         int months = (int) Math.round(daysBetweenDates(date1,date2)/30);//to round up correctly
         return months;        
     }
        
    /**    
     * Provides the number of years between two dates
     * If result is negative, then date1 is bigger that date2
     * 
     * @param date1 Beginning date
     * @param fecha2 Last date
     * 
     * @return Number of years between dates 
     */
    public static int yearsBetweenDates(Date date1, Date date2){        
        int years = (int) Math.round(daysBetweenDates(date1,date2)/365);//to round up correctly
        return years;        
    }
    
    
    /**
     * Returns the same date as a String, without the supplied separator
     * 
     * @param date The date to format supplied as a String
     * @param separator The separator thet has to be removed from date object
     *
     * @return The same date without specified separator
     *
     */
    public static String removeSeparator(String date, String separator){    	
    	return date.replaceAll(separator, "");    	
    }
    
}
