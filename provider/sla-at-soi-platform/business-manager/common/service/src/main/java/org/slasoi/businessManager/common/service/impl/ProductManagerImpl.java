/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 747 $
 * @lastrevision   $Date: 2011-02-17 16:42:40 +0100 (Thu, 17 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/ProductManagerImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.util.FunctionsCommons;
import org.slasoi.businessManager.common.dao.ComponentPriceDAO;
import org.slasoi.businessManager.common.dao.GeographicalAreaDAO;
import org.slasoi.businessManager.common.dao.PartyDAO;
import org.slasoi.businessManager.common.dao.ProductDAO;
import org.slasoi.businessManager.common.dao.ProductOfferDAO;
import org.slasoi.businessManager.common.model.EmBillingFrecuency;
import org.slasoi.businessManager.common.model.EmComponentPrice;
import org.slasoi.businessManager.common.model.EmCurrencies;
import org.slasoi.businessManager.common.model.EmGeographicalAreas;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmPriceType;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.service.ProductManager;
import org.slasoi.businessManager.common.util.ProductOffersAttr;
import org.slasoi.businessManager.common.util.SimpleDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(value="productService")
public class ProductManagerImpl implements ProductManager {

		static Logger log = Logger.getLogger(ProductManagerImpl.class);
			
		@Autowired
		private ProductDAO productDAO;

		@Autowired
		private ProductOfferDAO productOfferDAO;
		
		@Autowired
		private ComponentPriceDAO componentPriceDAO;
		
		@Autowired
		private GeographicalAreaDAO geoAreaDAO;
		
		@Autowired
		private PartyDAO partyDAO;
					
		
		@Transactional(propagation=Propagation.REQUIRED)
		public EmSpProducts getProductById(Long id){
	    	log.info("getProductById():Id:"+id);
	    	EmSpProducts result = productDAO.load(id);
	    	//Load Promotions reference
	    	productDAO.getHibernateTemplate().initialize(result.getEmProductPromotions());
	    	
	    	//Load all relations
	    	productDAO.getHibernateTemplate().initialize(result.getEmParty());
	    	productDAO.getHibernateTemplate().initialize(result.getEmParty().getEmCustomersProductses());
	    	for(EmSpProductsOffer emSpProductsOffer : result.getEmSpProductsOffers()){
	    		productDAO.getHibernateTemplate().initialize(emSpProductsOffer.getEmComponentPrices());
				productDAO.getHibernateTemplate().initialize(emSpProductsOffer.getEmGeographicalAreases());
			    for(EmSpServices service : emSpProductsOffer.getEmSpServiceses())
			    	productDAO.getHibernateTemplate().initialize(service.getEmParty());

	    	}
	    	
	    	return result;
		}
		
		@Transactional(propagation=Propagation.REQUIRED)
		public EmSpProducts getProductWithOffersById(Long id){
	    	log.info("getProductById():Id:"+id);
	    	EmSpProducts result = productDAO.load(id);
	    	//Load Promotions reference
    		for(EmSpProductsOffer offer : result.getEmSpProductsOffers()){
    			for(EmSpServices service : offer.getEmSpServiceses()){
    				productDAO.getHibernateTemplate().initialize(service.getEmServiceSpecifications());
    			}
    		}
	    	return result;
		}
		
		@Transactional(propagation=Propagation.REQUIRED)
	    public List<EmSpProducts> findProducts(String filter){
	    	log.info("findProducts()");
	    	return productDAO.findByFilter(filter);
	    }

		@Transactional(propagation=Propagation.REQUIRED)
	    public List<EmSpProducts> findProducts(List<String> filters){
	    	log.info("findProducts()");
	    	return productDAO.findByFilters(filters);
	    }

		@Transactional(propagation=Propagation.REQUIRED)
        public List<EmSpProducts> findProductsActive(String filter){
            log.info("findProductsActive()");
            return productDAO.findByStatusAndFilter(Constants.STATUS_ACTIVE, filter);
        }
		
        public List<EmSpProducts> findProductsActive(List<String> filters){
            log.info("findByStatusAndFilter()");
            return productDAO.findByStatusAndFilters(Constants.STATUS_ACTIVE, filters);
        }

		@Transactional(propagation=Propagation.REQUIRED)
		public EmSpProducts getProductForPolicy(Long id){
	    	log.info("getProductForPolicy():Id:"+id);
	    	EmSpProducts result = productDAO.load(id);
	    	//Load Product Offers and Services references
	    	productDAO.getHibernateTemplate().initialize(result.getEmSpProductsOffers());
	    	
	    	return result;
		}

		@Transactional(propagation = Propagation.REQUIRED)
		public List<EmSpProducts> findPartyProducts(Long partyId, String filter) {
			log.info("findPartyProducts()");
			return productDAO.findByPartyAndFilter(partyDAO.load(partyId), filter);
		}

		@Transactional(propagation = Propagation.REQUIRED)
		public List<EmSpProducts> findPartyProducts(EmParty party, String filter) {
			log.info("findPartyProducts()");
			return productDAO.findByPartyAndFilter(party, filter);
		}
		
		@Transactional(propagation = Propagation.REQUIRED)
		public void saveOrUpdate(EmSpProducts product) {
	
			this.productDAO.save(product);
		}
		   
		   @Transactional(propagation = Propagation.REQUIRED)
		    public List<EmSpProducts> getAllInit(){
		       log.info("getAllInit()");
		       log.debug(">>>>>>>>>>>>>>>> DAO: "+this.productDAO);
		       return this.productDAO.getAll();
		   } 
		   
		   @Transactional(propagation = Propagation.REQUIRED)
           public List<EmSpProducts> getAllInitActive(){
              log.info("getAllInitActive()");
              log.debug(">>>>>>>>>>>>>>>> DAO: "+this.productDAO);
              return this.productDAO.getAll(Constants.STATUS_ACTIVE);
          } 
		    
		    @Transactional(propagation = Propagation.REQUIRED)
		    public EmSpProducts getProductOffersLoaded(Long id){
		        
		        return this.productDAO.getProductOffersLoaded(id);
		    }
		    
		    
			@Transactional(propagation=Propagation.REQUIRED)
			public List<EmSpProducts> findProductForContract(String filter, Long nuCountryCustomer){
				log.info("findProductForContract()");
				log.info("Filter >>> "+filter);
				
				if(filter!=null) filter="%"+filter+"%";
				List<EmSpProducts> products = productDAO.findProductForContract(filter, Constants.STATUS_ACTIVE, Constants.STATUS_ACTIVE, nuCountryCustomer);
				return products;
			}
		    
			@Transactional(propagation=Propagation.REQUIRED)
			public List<EmSpProducts> findProductForPublishBSLAT(String filter){
			log.info("findProductForPublishBSLAT()");
				log.info("Filter >>> "+filter);
				
				if(filter!=null) filter="%"+filter+"%";
				List<EmSpProducts> products = productDAO.findProductForPublishBSLAT(filter);
				return products;
			}
		    
		    
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		public void saveProduct(EmSpProducts product, ProductOffersAttr productOffers) throws Exception{
	    	
	    	//Save product
	    	productDAO.saveOrUpdate(product);
	    	
	    	//si se trata de una modificación, eliminamos las ofertas anteriores que tuviera el producto, para insrtar las nuevas...
	    	if(product.getNuProductid()!=null && product.getNuProductid()>0)
	    		productOfferDAO.deleteProductOffers(product.getNuProductid());

	    	//Get the Product Offers
	    	ArrayList<EmSpProductsOffer> offers = new ArrayList<EmSpProductsOffer>(); 
	    	for(int i=0; i<productOffers.getOfferName().size(); i++){
	    		EmSpProductsOffer offer = getProductOffer(productOffers, i); 
	    		
	    		//Put the product create.
	    		offer.setEmSpProducts(product);
	    		
	    		//Save the product offer and put in the ArrayList 
	    		productOfferDAO.save(offer);
	    		offers.add(offer);
	    	}
	    	
	    	//After save Product Offer, save the component price
	    	String offerCP = null;
	    	int offerIndex=0;
	    	for(int i=0; i<productOffers.getComponentPriceOffer().size(); i++){
	    		
	    		if(offerCP!=null && !offerCP.equals(productOffers.getComponentPriceOffer().get(i)))
	    			offerIndex++;
	    		offerCP = productOffers.getComponentPriceOffer().get(i);
	    		
	    		EmComponentPrice cp = getComponentPrice(productOffers, i);
	    		
	    		//Get the offer asigned at the Component Price.
	    		EmSpProductsOffer offer = offers.get(offerIndex);
	    		cp.setEmSpProductsOffer(offer);
	    		
	    		//Save the component price.
	    		componentPriceDAO.save(cp);
	    	}
			
		}
		
		/**
		 * 
		 * @param form
		 * @param i
		 * @return
		 */
		public EmSpProductsOffer getProductOffer(ProductOffersAttr productOffers, int i) throws Exception{
    		EmSpProductsOffer offer = new EmSpProductsOffer();
    		
    		offer.setTxName(productOffers.getOfferName().get(i));
    		offer.setTxDescription(productOffers.getOfferDescription().get(i));
    		offer.setTxRevisionid(productOffers.getOfferRelease().get(i));
    		offer.setTxStatus(productOffers.getOfferStatus().get(i));
    		offer.setDtValidFrom(new SimpleDate(productOffers.getDateValidFrom().get(i)));
    		offer.setDtValidTo(new SimpleDate(productOffers.getDateValidTo().get(i)));
    		
    		//Put the billing frecuency
    		EmBillingFrecuency bf = new EmBillingFrecuency();
    		bf.setNuBillingFrecuencyId(productOffers.getBillingCode().get(i));
    		offer.setEmBillingFrecuency(bf);
    		
    		//Put the Geographical Areas
    		//El areaId contiene tupla (offerRef,areaId) introducimos en la oferta solo si coicide con el offerReference..
    		Long offerReference = productOffers.getNuAreasIdsReference().get(i);
    		for(String areaId : productOffers.getNuAreaIds()){
    			List<Long> lAreaId = FunctionsCommons.getListToLong(areaId);
    			if(lAreaId.get(0).longValue()==offerReference.longValue()){
    				EmGeographicalAreas area = geoAreaDAO.load(lAreaId.get(1));
    				offer.getEmGeographicalAreases().add(area);
    			}
    		}
    		
    		String[] offerServices = productOffers.getOfferServices().get(i);
	    	//Associate the offer services
	    	for(String serviceId : offerServices){
	    		EmSpServices service = new EmSpServices();
	    		service.setNuServiceid(new Long(serviceId));
	    		offer.getEmSpServiceses().add(service);
	    	}
    		
    		return offer;
		}
		
		public EmComponentPrice getComponentPrice(ProductOffersAttr componentPrices, int i) throws Exception{
			
			EmComponentPrice cp = new EmComponentPrice();
			
    		cp.setTxName(componentPrices.getComponentName().get(i));
    		cp.setTxDescription(componentPrices.getComponentDescription().get(i));
    		cp.setDtValidFrom(new SimpleDate(componentPrices.getOfferDateValidFrom().get(i)));
    		cp.setDtValidTo(new SimpleDate(componentPrices.getOfferDateValidTo().get(i)));
    		cp.setNuPrice(componentPrices.getPryce().get(i));
    		cp.setNuQuantity(componentPrices.getQuantity().get(i));
    		
    		EmPriceType priceType = new EmPriceType();
    		priceType.setNuIdpriceType(componentPrices.getPryceType().get(i));
    		cp.setEmPriceType(priceType);
    		
    		EmCurrencies currency = new EmCurrencies();
    		currency.setNuCurrencyId(componentPrices.getCurrency().get(i));
    		cp.setEmCurrencies(currency);
    		
    		return cp;
		}
		
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		public void saveProductsStatus(List<Long> productsIds, List<String> productsStatus){
			
			for(int i=0; i<productsIds.size(); i++){
				EmSpProducts product = productDAO.load(productsIds.get(i));
				product.setTxStatus(productsStatus.get(i));
				productDAO.saveOrUpdate(product);
			}
		}
		

		@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
		public void deleteProduct(Long productId){
			EmSpProducts obj = productDAO.load(productId);
			productDAO.delete(obj);
		}
		
		@Transactional(propagation = Propagation.REQUIRED)
		public boolean isModifiable(Long productId){
			EmSpProducts obj = productDAO.load(productId);
			boolean modifiable = true;
			//If have contract or price variations, then modifiable = false
			for(EmSpProductsOffer offer : obj.getEmSpProductsOffers()){
				if(offer.getEmCustomersProductses().size()>0){
					modifiable = false;
					break;
				}
				for(EmComponentPrice price : offer.getEmComponentPrices()){
					if(price.getEmPriceVariations().size()>0){
						modifiable=false;
						break;
					}
				}	
			}
			return modifiable;
		}
		
		
		@Transactional(propagation = Propagation.REQUIRED)
		public List<EmSpProducts> getActiveProductsWithAllCategories(List<String> categories) {
			return productDAO.getProductsWithAllCategories(categories, Constants.STATUS_ACTIVE);
		}
		
}
