/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/policy/Main.java $
 */

package org.slasoi.businessManager.common.service.policy;




import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main {

	private static String[] files = new String[] {"spring-config.xml"}; 	
	static Logger log = Logger.getLogger(Main.class);
	
	public static void main(String[] args) {

		try{
			
			final ApplicationContext context = new ClassPathXmlApplicationContext(files);
			
			final PolicyManagement pm = (PolicyManagement)context.getBean("policyManagement");

			System.out.println(">>>>>>>>> PROMOTION TYPES <<<<<<<<<<<");
			List<PolicyType> policyTypes = pm.getPolicyTypes();
			for(PolicyType promo : policyTypes)
				System.out.println(promo);
			
			Policy policy = pm.getPolicyByType(policyTypes.get(0).getNuPolicyTypeId());
			System.out.println(">>>>>>>>> PROMOTION PARAMETERS <<<<<<<<<<<");
			for(PolicyParameter param : policy.getPolicyParameters())
				System.out.println(param);

			
			policy.getPolicyParameters().get(0).setTxParamValue("Increment");
			policy.getPolicyParameters().get(1).setTxParamValue("1");
			policy.getPolicyParameters().get(2).setTxParamValue("QualifayingCondition");
			policy.getPolicyParameters().get(3).setTxParamValue("15");
			policy.getPolicyParameters().get(4).setTxParamValue("15");

			policy.setNuIdproductoffering(new Long(9));
			policy.setNuServiceid(new Long(2));
			policy.setNuSlspId(new Long(1));

			//pm.savePolicy(policy);
			
			log.info("Los objetos deberían estar cargados.");
			System.out.println("Los objetos deberían estar cargados.");
			
		}catch (Exception e) {
			log.error("Process Error >>> "+e.getMessage());
			System.out.println("Process Error >>> "+e.getMessage());
		}
	}

}
