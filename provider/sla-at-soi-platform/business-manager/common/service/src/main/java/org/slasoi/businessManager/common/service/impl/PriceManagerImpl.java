/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.dao.CompoundFeeDAO;
import org.slasoi.businessManager.common.dao.CompoundResourceDAO;
import org.slasoi.businessManager.common.dao.FixedFeeDAO;
import org.slasoi.businessManager.common.dao.GuaranteeDAO;
import org.slasoi.businessManager.common.dao.PriceDAO;
import org.slasoi.businessManager.common.dao.PricedItemDAO;
import org.slasoi.businessManager.common.dao.UnitResourceDAO;
import org.slasoi.businessManager.common.dao.VariableFeeDAO;
import org.slasoi.businessManager.common.model.pricing.CompoundFee;
import org.slasoi.businessManager.common.model.pricing.FixedFee;
import org.slasoi.businessManager.common.model.pricing.Guarantee;
import org.slasoi.businessManager.common.model.pricing.Price;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.slasoi.businessManager.common.model.pricing.VariableFee;
import org.slasoi.businessManager.common.service.PriceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="priceService")
public class PriceManagerImpl implements PriceManager{

	
	static Logger log = Logger.getLogger(PriceManagerImpl.class);
	
	@Autowired
	private PriceDAO priceDAO;
	
	@Autowired
	private FixedFeeDAO fixedFeeDAO;
	
	@Autowired
	private VariableFeeDAO variableFeeDAO;
	
	@Autowired
	private PricedItemDAO pricedItemDAO;
	
	@Autowired
	private GuaranteeDAO guaranteeDAO;
	
	@Autowired
	private CompoundFeeDAO compoundFeeDAO;
	
	@Transactional(propagation=Propagation.REQUIRED)
	public Price getPriceById(Long id){
		log.info("getPriceById");
		return priceDAO.load(id);
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public Long createPrice(BigDecimal value, String currencyType, String period, String type, int quantity, String dataType, Date validFrom, Date validUntil) {
		log.info("createPrice");
		Price p=new Price();
		p.setPriceValue(value);
		p.setCurrencyType(currencyType);
		p.setPeriod(period);
		p.setType(type);
		p.setQuantity(quantity);
		p.setDatatype(dataType);
		p.setValidFrom(validFrom);
		p.setValidUntil(validUntil);
		saveOrUpdatePrice(p);
		return p.getPriceId();
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public void saveOrUpdatePrice(Price price) {
		log.info("saveOrUpdatePrice");
		priceDAO.saveOrUpdate(price);		
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deletePrice(Long priceId) {
		log.info("deletePrice");
		Price p = priceDAO.load(priceId);
		if (isCompound(p.getPriceId()))
			deleteCompoundFee(p.getPriceId());
		else if (isFixedFee(p.getPriceId()))
			deleteFixedFee(p.getPriceId());
		else
			deleteVariableFee(p.getPriceId());
		priceDAO.delete(p);
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public FixedFee getFixedFeeById(Long id) {
		log.info("getFixedFeeById");
		return fixedFeeDAO.load(id);
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public Long createFixedFee(BigDecimal value, String currencyType, String period, String type, Date validFrom, Date validUntil, int quantity, String dataType) {
		log.info("createFixedFee");
		Long priceId=createPrice(value, currencyType, period, type, quantity, dataType, validFrom, validUntil);
		Price p=priceDAO.load(priceId);
		FixedFee ff=new FixedFee();
		ff.setPrice(p);
		p.setFixedFee(ff);
		saveOrUpdatePrice(p);
		saveOrUpdateFixedFee(ff);
		return ff.getFixedFeeId();
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public void saveOrUpdateFixedFee(FixedFee fee) {
		log.info("saveOrUpdateFixedFee");
		fixedFeeDAO.saveOrUpdate(fee);		
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public VariableFee getVariableFeeById(Long id) {
		log.info("getVariableFeeById");
		return variableFeeDAO.load(id);
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public Long createVariableFee(BigDecimal value, String currencyType, String period, String type, Date validFrom, Date validUntil, int quantity, String dataType, String cond, String condValue,
			String condDataType) {
		log.info("createVariableFee");
		Long priceId=createPrice(value, currencyType, period, type, quantity, dataType, validFrom, validUntil);
		Price p=priceDAO.load(priceId);
		VariableFee vf=new VariableFee();
		vf.setPrice(p);
		vf.setCond(cond);
		vf.setCondValue(condValue);
		vf.setCondDataType(condDataType);
		p.setVariableFee(vf);
		saveOrUpdatePrice(p);
		saveOrUpdateVariableFee(vf);
		return vf.getVariableFeeId();
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public void saveOrUpdateVariableFee(VariableFee fee) {
		log.info("saveOrUpdateVariableFee");
		variableFeeDAO.saveOrUpdate(fee);		
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public CompoundFee getCompoundFeeById(Long id) {
		log.info("getCompoundFeeById");
		return compoundFeeDAO.load(id);
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Long createCompoundFee(String currencyType, String period, Date validFrom, Date validUntil, Set<VariableFee> variableFees, Set<FixedFee> fixedFees) {
		log.info("createCompoundFee");
		BigDecimal value = new BigDecimal("0");
		String type = "";
		int quantity = 0;
		String dataType = "";
		Long priceId = createPrice(value, currencyType, period, type, quantity, dataType, validFrom, validUntil);
		Price p = priceDAO.load(priceId);
		CompoundFee cf = new CompoundFee();
		cf.setPrice(p);
		if (fixedFees != null && fixedFees.size() != 0) {
			cf.setFixedFees(fixedFees);
			for (FixedFee ff : fixedFees) {
				ff.setCompoundFee(cf);
				saveOrUpdateFixedFee(ff);
			}
		}
		if (variableFees != null && variableFees.size() != 0) {
			cf.setVariableFees(variableFees);
			for (VariableFee vf : variableFees) {
				vf.setCompoundFee(cf);
				saveOrUpdateVariableFee(vf);
			}
		}
		p.setCompoundFee(cf);
		saveOrUpdatePrice(p);
		saveOrUpdateCompoundFee(cf);
		return cf.getCompoundFeeId();
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public void saveOrUpdateCompoundFee(CompoundFee fee) {
		log.info("saveOrUpdateCompoundFee");
		compoundFeeDAO.saveOrUpdate(fee);		
	}

	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public Price findCurrentPrice(Long productId) {
		log.info("findCurrentPrice");
		PricedItem pI=pricedItemDAO.load(productId);
		Price price=getCurrentPrice(pI.getPrices());
		return price;
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public Price findPriceOfGuarantee(Long guaranteeId) {
		log.info("findPriceOfGuarantee");
		Guarantee pI=guaranteeDAO.load(guaranteeId);
		Price price=getCurrentPrice(pI.getPrices());
		return price;
	}
	
	/*@Transactional(propagation=Propagation.REQUIRED)
	public double retrieveTotalPrice(CompoundFee compoundFee) {
		log.info("retrieveTotalPrice");
		double amount=0;
		Set<FixedFee> fixedFees=compoundFee.getFixedFees();
		Set<VariableFee> variableFees=compoundFee.getVariableFees();
		for(FixedFee f: fixedFees){
			amount+=f.getPrice().getPriceValue().doubleValue();
		}
		for(VariableFee v: variableFees){
			amount+=v.getPrice().getPriceValue().doubleValue();
		}
		return amount;
	}*/

	private Price getCurrentPrice(Set<Price> prices){
		log.info("getCurrentPrice");
		Date currentDate=GregorianCalendar.getInstance().getTime();
		for(Price p : prices){
			if(p.getValidFrom().before(currentDate) && p.getValidUntil().after(currentDate)){
				return p;
			}
		}
		return null;
	}

	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public void associatePrice(Long productId, Long priceId) {
		log.info("associatePrice: Id product "+productId+" Id price: "+priceId);
		PricedItem pi=pricedItemDAO.load(productId);
		Price p=priceDAO.load(priceId);
		Set<Price> prices=pi.getPrices();
		prices.add(p);
		pi.setPrices(prices);
		p.setPricedItem(pi);
		saveOrUpdatePrice(p);
		pricedItemDAO.saveOrUpdate(pi);
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public void associatePriceToGuarantee(Long guaranteeId, Long priceId) {
		log.info("associatePriceToGuarantee: Id guarantee "+guaranteeId+" Id price: "+priceId);
		Guarantee g=guaranteeDAO.load(guaranteeId);
		Price p=priceDAO.load(priceId);
		Set<Price> prices=g.getPrices();
		prices.add(p);
		g.setPrices(prices);
		p.setGuarantee(g);
		saveOrUpdatePrice(p);
		guaranteeDAO.saveOrUpdate(g);
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void deleteFixedFee(Long fixedFeeId) {
		log.info("deleteFixedFee");
		FixedFee ff = fixedFeeDAO.load(fixedFeeId);
		if (ff != null) {
			fixedFeeDAO.delete(ff);
			if (ff.getCompoundFee() != null)
				deleteCompoundFee(ff.getCompoundFee().getCompoundFeeId());
			priceDAO.delete(ff.getPrice());
		}
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void deleteVariableFee(Long variableFeeId) {
		log.info("deleteVariableFee");
		VariableFee vf = variableFeeDAO.load(variableFeeId);
		if (vf != null) {
			variableFeeDAO.delete(vf);
			if (vf.getCompoundFee() != null)
				deleteCompoundFee(vf.getCompoundFee().getCompoundFeeId());
			priceDAO.delete(vf.getPrice());
		}
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void deleteCompoundFee(Long compoundFeeId) {
		log.info("deleteCompoundFee");
		CompoundFee cf = compoundFeeDAO.load(compoundFeeId);
		if (cf != null) {
			Set<FixedFee> ff = cf.getFixedFees();
			for (FixedFee f : ff)
				deleteFixedFee(f.getFixedFeeId());
			Set<VariableFee> vf = cf.getVariableFees();
			for (VariableFee v : vf)
				deleteVariableFee(v.getVariableFeeId());
			compoundFeeDAO.delete(cf);
			priceDAO.delete(cf.getPrice());
		}
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public boolean isCompound(Long priceId){
		log.info("isCompound");
		return priceDAO.isCompound(priceId);
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public boolean isVariableFee(Long priceId){
		log.info("isVariableFee");
		return priceDAO.isVariableFee(priceId);
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public boolean isFixedFee(Long priceId){
		log.info("isFixedFee");
		return isFixedFee(priceId);
	}
}
