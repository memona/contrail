/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 938 $
 * @lastrevision   $Date: 2011-03-14 15:03:36 +0100 (Mon, 14 Mar 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/CustomerProductManagerImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.dao.CustomerProductDAO;
import org.slasoi.businessManager.common.dao.PartyDAO;
import org.slasoi.businessManager.common.dao.PriceVariationDAO;
import org.slasoi.businessManager.common.dao.PriceVariationTypeDAO;
import org.slasoi.businessManager.common.dao.ProductOfferDAO;
import org.slasoi.businessManager.common.drools.DroolsManager;
import org.slasoi.businessManager.common.drools.Rule;
import org.slasoi.businessManager.common.drools.RuleParam;
import org.slasoi.businessManager.common.drools.Template;
import org.slasoi.businessManager.common.model.EmCharacteristic;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmPriceVariation;
import org.slasoi.businessManager.common.model.EmProductPromotion;
import org.slasoi.businessManager.common.model.EmPromotionType;
import org.slasoi.businessManager.common.model.EmPromotionValues;
import org.slasoi.businessManager.common.model.EmPromotions;
import org.slasoi.businessManager.common.model.EmRuleTemplates;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.model.EmSpServicesCharacteristic;
import org.slasoi.businessManager.common.service.CharacteristicManager;
import org.slasoi.businessManager.common.service.CustomerProductManager;
import org.slasoi.businessManager.common.service.ServiceManager;
import org.slasoi.businessManager.common.util.SimpleDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(value = "customerProductService")
public class CustomerProductManagerImpl implements CustomerProductManager {

    static Logger log = Logger.getLogger(CustomerProductManagerImpl.class);

    @Autowired
    private CustomerProductDAO cpDAO;

    @Autowired
    private PriceVariationDAO priceVariationDAO;

    @Autowired
    private ProductOfferDAO productOfferDAO;

    @Autowired
    private PartyDAO partyDAO;

    @Autowired
    private PriceVariationTypeDAO priceVariationTypeDAO;
    
    @Autowired
    private ServiceManager serviceService;    

    @Autowired
    private CharacteristicManager characteristicService;
	
    //@Autowired
    //private ClientProfile clientProfile;


    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.businessManager.service.CustomerProductManager#getCustomerProducts()
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public List<EmCustomersProducts> getCustomerProducts() {
        log.info("getCustomerProducts()");
        return cpDAO.getList();
    }
    
    /**
     * 
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public List<EmCustomersProducts> getCustomerProductsByDateAndState(Date date, String typeDate){
        log.info("getCustomerProductsByDateAndState()");
        return cpDAO.getCustomerProductsByDateAndState(date,typeDate);
    }
    /**
     * Get Customer products active that have not ended before the given Date
     * @param date
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public  List<EmCustomersProducts> getCustomerProductsActiveByDate(Date date){
        log.info("getCustomerProductsActiveByDate()");
        return cpDAO.getCustomerProductsActiveByDate(date);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.businessManager.service.CustomerProductManager#getCustomerProductById(java.lang.String)
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public EmCustomersProducts getCustomerProductById(String id) {

        log.info("getCustomerProductById(" + id + ")");
        EmCustomersProducts cp = cpDAO.load(id);
        cpDAO.getHibernateTemplate().initialize(cp.getEmPriceVariations());
        cpDAO.getHibernateTemplate().initialize(cp.getEmSpProductsOffer().getEmComponentPrices());
        cpDAO.getHibernateTemplate().initialize(cp.getEmSpProductsOffer().getEmGeographicalAreases());
        // Initialize Services with Party
        for (EmSpServices service : cp.getEmSpProductsOffer().getEmSpServiceses())
            cpDAO.getHibernateTemplate().initialize(service.getEmParty());

        return cp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.slasoi.businessManager.service.CustomerProductManager#getCustomerProductsOfParty(org.slasoi.businessManager
     * .model.EmParty)
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public List<EmCustomersProducts> getCustomerProductsOfParty(EmParty party) {
        log.info("getCustomerProductsByParty()");
        return cpDAO.getCustomerProductOfParty(party);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.slasoi.businessManager.service.CustomerProductManager#getCustomerProductsByPublisher(org.slasoi.businessManager
     * .model.EmParty)
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public List<EmCustomersProducts> getCustomerProductsByPublisher(EmParty party) {
        log.info("getCustomerProductsByPublisher()");
        return cpDAO.getCustomerProductsByPublisher(party);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<EmCustomersProducts> getCustomerProductsForRating(EmParty party) {
        log.info("getCustomerProductsByPublisher()");
        return cpDAO.getCustomerProductsAndServicesAttributes(party);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<EmCustomersProducts> getCustomerProductsByCategory(EmParty party, String category) {
        log.info("getCustomerProductsByPublisher()");
        return cpDAO.getCustomerProductsAndServicesAttributes(party, category);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * org.slasoi.businessManager.service.CustomerProductManager#contractProductsOffer(org.slasoi.businessManager.model
     * .EmParty, java.util.List)
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void contractProductsOffer(EmParty party, List<Long> productsOffer) throws Exception {

        for (Long productOfferId : productsOffer) {
            EmSpProductsOffer offer = productOfferDAO.load(productOfferId);
            EmCustomersProducts cp = new EmCustomersProducts();
            cp.setEmParty(party);
            cp.setEmSpProductsOffer(offer);
            cp.setState(Constants.SLA_STATE_OBS);
            cp.setDtInsertDate(new SimpleDate());
            cp.setDtDateBegin(offer.getDtValidFrom());
            cp.setDtDateEnd(offer.getDtValidTo());

            // TODO: El txBslaid habrá que generarlo de alguna manera, por ahora asigno esto para insertar...
            cp.setTxBslaid(java.util.UUID.randomUUID().toString());
            // cp.setTxBslaid(new Long(new Date().getTime()).toString());
            log.info("ID >>>>>>>>>>>>>>>>> " + cp.getTxBslaid());

            // TODO: por ahora para probar no guardo...
            cpDAO.save(cp);

            // Check applicable promotions
            applyPromotions(cp);

            // Obtenemos los price variation creados y añadimos la relacción con la promoción...
            log.info("Prices Variation Resultantes >>>>>>>>>>>>>>>> " + cp.getEmPriceVariations().size());
            for (EmPriceVariation pv : cp.getEmPriceVariations()) {
                log.info(">>>>>>>>>>>> PriceVarition : " + pv.getTxDescription());
                priceVariationDAO.save(pv);
            }

        }
    }

    /**
 * 
 */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public EmCustomersProducts getVariationsByPromotion(Long party, Long productOfferId) throws Exception {

        EmSpProductsOffer offer = productOfferDAO.load(productOfferId);
        EmCustomersProducts cp = new EmCustomersProducts();
        cp.setEmParty(partyDAO.load(party));
        cp.setEmSpProductsOffer(offer);
        cp.setDtInsertDate(new SimpleDate());
        cp.setDtDateBegin(offer.getDtValidFrom());
        cp.setDtDateEnd(offer.getDtValidTo());

        // Check applicable promotions
        applyPromotions(cp);
        // load price variation type data
        Set<EmPriceVariation> variations = cp.getEmPriceVariations();
        if (variations != null && variations.size() > 0) {
            Set<EmPriceVariation> newVariations = new HashSet();
            Iterator<EmPriceVariation> ite = variations.iterator();
            EmPriceVariation variation;
            while (ite.hasNext()) {
                variation = ite.next();
                variation.setEmPriceVariationType(priceVariationTypeDAO.load(variation.getEmPriceVariationType()
                        .getNuIdVariationType()));
                newVariations.add(variation);
            }
            cp.setEmPriceVariations(newVariations);
        }
        return cp;
    }

    /*
     * (non-Javadoc)
     * 
     * @seeorg.slasoi.businessManager.service.CustomerProductManager#applyPromotions(org.slasoi.businessManager.model.
     * EmCustomersProducts)
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void applyPromotions(EmCustomersProducts customer_product) throws Exception {

        // Get the promotions for the id_provision product.
        EmSpProductsOffer product_offer = customer_product.getEmSpProductsOffer();
        EmSpProducts product = product_offer.getEmSpProducts();

        log.info(">>>> Num Promotions: " + product.getEmProductPromotions().size());
        Iterator<EmProductPromotion> product_promos = product.getEmProductPromotions().iterator();

        // Prepare the WorkingMemory add the ComponentPrice at Facts
        ArrayList<Object> arrayFacts = new ArrayList<Object>(product_offer.getEmComponentPrices());
        // Add the customer_product necesary for the rules.
        arrayFacts.add(customer_product);

		// Here is the place in which can be added the call to retrieve the profile from the Profile Server
		// and add this object to the array of facts of Drools engine
		// Example. Get the Customer profile necesary for some rules and load it in facts array
        //CustomerProfile customerProfile =
        //        clientProfile.getUserProfile(customer_product.getEmParty().getNuPartyid().toString());
        //log.info("ClientProfile: " + clientProfile);
        //if (customerProfile != null) {
        //    log.info("Profile: " + customerProfile);
        //    arrayFacts.add(customerProfile);
        //}
		
        HashMap<Long, Template> templates = new HashMap<Long, Template>();

        while (product_promos.hasNext()) {
            // TODO: Tener en cuenta el estado de la product_promotion
            EmProductPromotion product_promo = product_promos.next();

            // do nothing if not active rule.
            if (product_promo.getTxStatus().equals(Constants.STATE_ACTIVE)) {

                EmPromotions promo = product_promo.getEmPromotions();

                // Add Promo as fact
                log.info(">>> Promotion:  " + promo.getTxName());
                EmPromotionType promoType = promo.getEmPromotionType();
                log.info(">>> Tipo : " + promoType.getTxName());
                EmRuleTemplates ruleTplt = promoType.getEmRuleTemplates();
                log.info(">>> Template : " + ruleTplt.getTxName());

                // We created and insert, the template if not exist
                Template template = templates.get(ruleTplt.getNuRuleTemplateId());
                if (template == null) {
                    template =
                            new Template(ruleTplt.getNuRuleTemplateId(), promo.getTxName(),
                                    ruleTplt.getTxDescription(), ruleTplt.getTxTemplateDrt());
                    templates.put(template.getId(), template);
                }

                // Get the rule params. Rule == Promotion (in this case)
                ArrayList<RuleParam> ruleParams = new ArrayList<RuleParam>();
                Iterator<EmPromotionValues> promoValues = promo.getEmPromotionValueses().iterator();
                while (promoValues.hasNext()) {
                    EmPromotionValues promoValue = promoValues.next();
                    log.info(promoValue.getEmRuleParams().getNuParamRuleOrder() + " - "
                            + promoValue.getEmRuleParams().getEmParams().getTxParamName() + ": "
                            + promoValue.getTxParamValue());
                    // New RuleParam(order,value)
                    RuleParam rp =
                            new RuleParam(promoValue.getEmRuleParams().getNuParamRuleOrder(), promoValue
                                    .getTxParamValue());
                    ruleParams.add(rp);
                }

                // Add the rule at specific template...
                Rule rule = new Rule(promo.getNuPromotionId(), promo.getTxName(), promo.getTxDescription(), ruleParams);
                template.getRules().add(rule);

            }

        }

        if (templates.isEmpty()) {
            log.info(">>>> Not Exist Active Promotions. ");
            return;
        }

        log.debug("Array Facts >>> " + arrayFacts.size());
        // Create DroolsManager, with array templates and facts
        DroolsManager dm = new DroolsManager(templates.values(), arrayFacts);
        dm.processRules();
        log.debug("<<<<<<<< Rules Executed >>>>>>>>>");
        log.debug("Array Facts After >>> " + arrayFacts.size());

    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void saveCustomer(EmCustomersProducts customer_product) throws Exception {
        cpDAO.save(customer_product);
    }

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void updateCustomer(EmCustomersProducts customer_product) throws Exception {
        cpDAO.update(customer_product);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.slasoi.businessManager.service.CustomerProductManager#getCustomerProductById(java.lang.String)
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public EmCustomersProducts getCustomerProductByIdLoaded(String id) {
        return cpDAO.getCustomerProductByIdLoaded(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.slasoi.businessManager.service.CustomerProductManager#getCustomerProductsByProductOfferAndParty(EmSpProductsOffer
     * , EmParty)
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public List<EmCustomersProducts> getCustomerProductsByProductOfferAndParty(EmSpProductsOffer productOffer,
            EmParty party) {
        return cpDAO.getCustomerProductsByProductOfferAndParty(productOffer, party);
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public  String getServiceProvisioningName(EmCustomersProducts customerProduct){   
        log.debug("Into getServiceProvisioningName method ");
        EmCharacteristic characterType =characteristicService.getCharacteristicByName(Constants.SERVICE_CHARACTERISTIC_NAME);
        if(characterType!= null) {
            log.debug("CharacteristicBaseID: "+ characterType.getNuCharacteristicId());
            EmSpProductsOffer offer = productOfferDAO.getByBslat(customerProduct.getEmSpProductsOffer().getTxBslatid());
            if(offer !=null) {
                Set<EmSpServices> services = offer.getEmSpServiceses();
                if(services !=null && services.size()>0){
                    for(EmSpServices service : services){
                        EmSpServices serviceAux = serviceService.getServiceById(service.getNuServiceid());
                        Set<EmSpServicesCharacteristic> characteristics = serviceAux.getEmSpServicesCharacteristics();
                        if(characteristics!= null && characteristics.size()>0){
                            for(EmSpServicesCharacteristic character:characteristics){
                                if(characterType.getNuCharacteristicId().longValue()==
                                   character.getEmCharacteristic().getNuCharacteristicId().longValue()){
                                    log.debug("Value Returned: "+character.getTxValue());
                                    return character.getTxValue();
                                }
                                
                            }
                            
                        }                                        
                    }
                }
            }
        }
        return "";
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public List<String> getBslatIds(EmParty party){
    	ArrayList<String> bslatIds = new ArrayList<String>(); 
    	for(EmCustomersProducts cp : cpDAO.getCustomerProductOfParty(party))
    		bslatIds.add(cp.getTxBslaid());

    	return bslatIds;
    	
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public List<EmCustomersProducts> getCustomerProductsByProduct(EmSpProducts product){        
        return cpDAO.getCustomerProductsByProduct(product);
    }
}
