/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/RuleContextManagerImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.drools.compiler.DrlParser;
import org.drools.lang.descr.PackageDescr;
import org.slasoi.businessManager.common.dao.RuleContextDAO;
import org.slasoi.businessManager.common.drools.DroolsException;
import org.slasoi.businessManager.common.model.EmRuleContext;
import org.slasoi.businessManager.common.service.RuleContextManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="ruleContextService")
public class RuleContextManagerImpl implements RuleContextManager{

		static Logger log = Logger.getLogger(RuleContextManagerImpl.class);
	
		@Autowired
		private RuleContextDAO ruleContextDAO;
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.common.service.impl.RuleContextManager#getRuleContexts()
		 */
		@Transactional(propagation=Propagation.REQUIRED)
	    public List<EmRuleContext> getRuleContexts(){
	    	log.info("getRuleContext()");
	    	return ruleContextDAO.getList();
	    }
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.common.service.impl.RuleContextManager#getRuleContextById(java.lang.Long)
		 */
		@Transactional(propagation=Propagation.REQUIRED)
	    public EmRuleContext getRuleContextById(Long id){
	    	log.info("getRuleContextById():Id:"+id);
	    	EmRuleContext result = ruleContextDAO.load(id);
	    	return result;
	    }

		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.common.service.impl.RuleContextManager#saveRuleContext(org.slasoi.businessManager.common.model.EmRuleContext)
		 */
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	    public void saveRuleContext(EmRuleContext object) throws Exception{
	    	log.info("saveRuleContext()");
	    	try{
	    		DrlParser parser = new DrlParser();
	    		PackageDescr pd = parser.parse(object.getTxContent());
	    		if(pd==null || pd.getNamespace()==null)
	    			throw new Exception();
	    		log.info("Package: "+pd.getNamespace());
	    		log.info("Imports: "+pd.getImports());
	    	}catch (Exception e) {
	    		throw new DroolsException("Invalid Context Rule.");
			}

	    	ruleContextDAO.saveOrUpdate(object);
	    }
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.common.service.impl.RuleContextManager#deletRuleContexts(java.util.List)
		 */
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	    public void deleteRuleContexts(List<Long> ids){
	    	log.info("deleteRuleContext()");
	    	
	    	//Delete all Rule Context of the list Ids
	    	Iterator<Long> itIds = ids.iterator();
	    	while(itIds.hasNext()){
	    		EmRuleContext obj = ruleContextDAO.load(itIds.next());
	    		ruleContextDAO.delete(obj);
	    	}
	    }
}
