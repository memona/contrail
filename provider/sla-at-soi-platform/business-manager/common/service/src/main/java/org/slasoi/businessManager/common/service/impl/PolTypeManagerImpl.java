/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/PolTypeManagerImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.dao.PolTypeDAO;
import org.slasoi.businessManager.common.model.EmPolType;
import org.slasoi.businessManager.common.service.PolTypeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="polTypeService")
public class PolTypeManagerImpl implements PolTypeManager {

		static Logger log = Logger.getLogger(PolTypeManagerImpl.class);
	
		@Autowired
		private PolTypeDAO policyTypeDAO;
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyTypeManager#getPolicyTypes()
		 */
		@Transactional(propagation=Propagation.REQUIRED)
	    public List<EmPolType> getPolicyTypes(){
	    	log.info("getPolicyTypes()");
	    	return policyTypeDAO.getList();
	    }
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyTypeManager#getPolicyTypeById(java.lang.Long)
		 */
		@Transactional(propagation=Propagation.REQUIRED)
	    public EmPolType getPolicyTypeById(Long id){
	    	log.info("getRuleTemplateById():Id:"+id);
	    	EmPolType result = policyTypeDAO.load(id);
	    	return result;
	    }
		

		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyTypeManager#savePolicyType(org.slasoi.businessManager.model.EmPolicyType)
		 */
		@Transactional(propagation=Propagation.REQUIRED)
	    public void savePolicyType(EmPolType object){
	    	log.info("savePolicyType()");
	    	policyTypeDAO.saveOrUpdate(object);
	    }
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyTypeManager#deletePolicyTypes(java.util.List)
		 */
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	    public void deletePolicyTypes(List<Long> ids){
	    	log.info("deletePolicyTypes()");
	    	
	    	//Delete all PolicyType of the list Ids
	    	Iterator<Long> itIds = ids.iterator();
	    	while(itIds.hasNext()){
	    		EmPolType polType = policyTypeDAO.load(itIds.next());
	    		policyTypeDAO.delete(polType);
	    	}
	    }

		
		
		
	
}
