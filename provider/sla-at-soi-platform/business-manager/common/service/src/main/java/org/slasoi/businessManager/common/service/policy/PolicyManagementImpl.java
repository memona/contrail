/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/policy/PolicyManagementImpl.java $
 */

package org.slasoi.businessManager.common.service.policy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.dao.ParamDAO;
import org.slasoi.businessManager.common.dao.ParamTypeDAO;
import org.slasoi.businessManager.common.dao.PolicyDAO;
import org.slasoi.businessManager.common.dao.PolicyTypeDAO;
import org.slasoi.businessManager.common.dao.PolicyValueDAO;
import org.slasoi.businessManager.common.dao.ProductOfferDAO;
import org.slasoi.businessManager.common.dao.ServiceDAO;
import org.slasoi.businessManager.common.model.EmParams;
import org.slasoi.businessManager.common.model.EmPolicies;
import org.slasoi.businessManager.common.model.EmPolicyType;
import org.slasoi.businessManager.common.model.EmPolicyValues;
import org.slasoi.businessManager.common.model.EmPolicyValuesId;
import org.slasoi.businessManager.common.model.EmRuleParams;
import org.slasoi.businessManager.common.model.EmRuleTemplates;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.util.SimpleDate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="policyManagement")
public class PolicyManagementImpl implements PolicyManagement {

		static Logger log = Logger.getLogger(PolicyManagementImpl.class);
		
		@Resource
		private PolicyTypeDAO policyTypeDAO;
		
		@Resource
		private PolicyDAO policyDAO;
		
		@Resource
		private PolicyValueDAO policyValueDAO;
		
		@Resource
		private ProductOfferDAO productOfferDAO;
		
		@Resource
		private ServiceDAO serviceDAO;

		@Resource
		private ParamDAO paramDAO;
		
		@Resource
		private ParamTypeDAO paramTypeDAO;

		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.common.service.policy.PolicyManagement#getPolicyTypes()
		 */
		public List<PolicyType> getPolicyTypes(){
			try{
				List<EmPolicyType> emTypes = policyTypeDAO.getList();
				List<PolicyType> types = new ArrayList<PolicyType>();
				
				for(EmPolicyType emType : emTypes){
					PolicyType type = new PolicyType();
					BeanUtils.copyProperties(type, emType);
					type.setTxTemplateDrt(emType.getEmRuleTemplates().getTxTemplateDrt());
					types.add(type);
				}
				return types;
			}catch (Exception e) {
				log.error("Error load Policy Types: "+e.getMessage());
				return null;
			}
		}
				
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.common.service.policy.PolicyManagement#getPolicyByType(java.lang.Long)
		 */
		@Transactional(propagation=Propagation.REQUIRED)
		public Policy getPolicyByType(Long policyTypeId) throws Exception{
			
			Policy policy = new Policy();
			
			//Load Parameter of template...
			EmPolicyType policyType = policyTypeDAO.load(policyTypeId);
			policy.setNuPolicyTypeId(policyTypeId);

			EmRuleTemplates template = policyType.getEmRuleTemplates();
			
			//Load Template parameters, and asociate with Policy
			Iterator<EmRuleParams> itRuleParams = template.getEmRuleParamses().iterator();
			while(itRuleParams.hasNext()){
				EmRuleParams ruleParam = itRuleParams.next();
				PolicyParameter policyParam = new PolicyParameter();
				BeanUtils.copyProperties(policyParam, ruleParam.getEmParams());
				policyParam.setTxParamType(ruleParam.getEmParams().getEmParamType().getTxParamTypeName());
				
				//Set list of valid values, if type = SELECT
				paramTypeDAO.loadParamValues(ruleParam.getEmParams().getEmParamType());
				policyParam.setListValues(ruleParam.getEmParams().getEmParamType().getListValues());
				
				policy.getPolicyParameters().add(policyParam); 
			}
			
			return policy;
			
		}
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.common.service.policy.PolicyManagement#savePolicy(org.slasoi.businessManager.common.service.policy.Policy)
		 */
		@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
		public void savePolicy(Policy policy) throws Exception{
			
			//Check valid ProductOfferId
			EmSpProductsOffer emProdOffer = productOfferDAO.load(policy.getNuIdproductoffering());
			if(emProdOffer==null)
				throw new Exception("Producct Offer Id "+policy.getNuIdproductoffering()+" does not exist in database.");
			
			//Check valid ServiceId
			EmSpServices emService = serviceDAO.load(policy.getNuServiceid());
			if(emService==null)
				throw new Exception("Service Id "+policy.getNuServiceid()+" does not exist in database.");
			
			EmPolicyType ept = policyTypeDAO.load(policy.getNuPolicyTypeId());
			if(ept==null)
				throw new Exception("Must enter a valid type of policy, the right does not exist in database.");
			
			//Check the number of parameters is correct
			if(policy.getPolicyParameters().size()!=ept.getEmRuleTemplates().getEmRuleParamses().size())
				throw new Exception("Wrong number of parameters for the selected type of policy. Must have "+ept.getEmRuleTemplates().getEmRuleParamses().size()+1);
			
			EmPolicies emp = new EmPolicies();
			BeanUtils.copyProperties(emp, policy);
			emp.setEmPolicyType(ept);
			
			emp.setEmSpProductsOffer(emProdOffer);
			emp.setEmSpServices(emService);
			emp.setTxStatus(Constants.STATE_PENDING);
			//Save the policie
			policyDAO.save(emp);
			

			//Save the policy values...
			for(PolicyParameter policyParam : policy.getPolicyParameters()){
				//Check if valid paramId for select template
				checkPolicyParameter(policyParam, ept.getEmRuleTemplates());
				EmPolicyValuesId id = new EmPolicyValuesId(emp.getNuPolicyId(), ept.getEmRuleTemplates().getNuRuleTemplateId(), policyParam.getNuParamId());
				EmPolicyValues epv = new EmPolicyValues(id, policyParam.getTxParamValue());
				policyValueDAO.saveOrUpdate(epv);
			}
			
		}
		
		
		
		@Transactional(propagation=Propagation.REQUIRED)
		private boolean checkPolicyParameter(PolicyParameter policyParam, EmRuleTemplates template) throws Exception{

			boolean valid=false;
			try{
				EmParams emParam = paramDAO.load(policyParam.getNuParamId());
				if(emParam==null)
					throw new Exception("Must enter valid parameter. ParamId: "+policyParam.getNuParamId()+" not exist in database.");
				
				if(policyParam.getTxParamValue()==null || policyParam.getTxParamValue().trim().length()==0)
					throw new Exception("Must enter all parameters value");
				//Validate if parameterId is valid for template.
				for(EmRuleParams param : template.getEmRuleParamses() ){
					if(param.getId().getNuParamId()==policyParam.getNuParamId()){
						valid=true;
						break;
					}
				}
				
				//check value if ParamType = DATE. 
				if(emParam.getEmParamType().getTxParamTypeName().equals(Constants.PARAM_TYPE_DATE)){
					try{
						new SimpleDate(policyParam.getTxParamValue());
					}catch (Exception e) {
						throw new Exception("Value of "+policyParam.getTxParamViewname()+": "+policyParam.getTxParamValue()+" no is a valid Date.");
					}
				}
				//check value if ParamType = NUMBER.
				if(emParam.getEmParamType().getTxParamTypeName().equals(Constants.PARAM_TYPE_NUMBER)){
					try{
						Double.parseDouble(policyParam.getTxParamValue()); 
					}catch (Exception e) {
						throw new Exception("Value of "+policyParam.getTxParamViewname()+": "+policyParam.getTxParamValue()+" no is a valid Number.");
					}
				}
					
			}catch (Exception e) {
				log.error("checkPolicyParameter Error: "+e.getMessage());
				throw e;
			}
			return valid;
		}
		
		
		
		
		
}
