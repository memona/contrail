/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.service.impl;

import java.math.BigDecimal;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.dao.GuaranteeDAO;
import org.slasoi.businessManager.common.dao.PriceDAO;
import org.slasoi.businessManager.common.dao.PricedItemDAO;
import org.slasoi.businessManager.common.model.pricing.CompoundResource;
import org.slasoi.businessManager.common.model.pricing.Guarantee;
import org.slasoi.businessManager.common.model.pricing.Price;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.slasoi.businessManager.common.service.GuaranteeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service(value="guaranteeService")
public class GuaranteeManagerImpl implements GuaranteeManager{

	static Logger log = Logger.getLogger(GuaranteeManagerImpl.class);
	
	@Autowired
	private GuaranteeDAO guaranteeDAO;
	
	@Autowired
	private PricedItemDAO pricedItemDAO;
	
	
	@Transactional(propagation=Propagation.REQUIRED)
	public Guarantee getGuaranteeById(Long id) {
		return guaranteeDAO.load(id);
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public Guarantee getGuaranteeByName(String name) {
		log.info("getGuaranteeByName():Name:"+name);
		return guaranteeDAO.findGuaranteeByName(name);
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	public Guarantee getGuaranteeByType(String type) {
		log.info("getGuaranteeByType():Type:"+type);
		return guaranteeDAO.findGuaranteeByType(type);
	}

	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public Long createGuarantee(String name, String slaTemplateId, String agreementTerm, String description, String type, String operator, String value, String valueType, double per_variation) {
		log.info("createGuarantee");
		Guarantee g=new Guarantee();
		g.setGuaranteeName(name);
		g.setSlaTemplateId(slaTemplateId);
		g.setAgreementTerm(agreementTerm);
		g.setGuaranteeDescr(description);
		g.setGuaranteeType(type);
		g.setGuaranteeOp(operator);
		g.setGuaranteeVal(value);
		g.setGuaranteeValType(valueType);
		g.setPercPriceVariation(new BigDecimal(per_variation));
		saveOrUpdateGuarantee(g);
		return g.getGuaranteeId();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void saveOrUpdateGuarantee(Guarantee guarantee) {
		log.info("saveOrUpdateGuarantee");
		guaranteeDAO.saveOrUpdate(guarantee);
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void deleteGuarantee(Long guaranteeId) {
		log.info("deleteGuarantee()");
		Guarantee obj = guaranteeDAO.load(guaranteeId);
		guaranteeDAO.delete(obj);
	}
	
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public void associateProduct(Long productId, Long guaranteeId) {
		log.info("associateProduct: Id product "+productId+" Id guarantee: "+guaranteeId);
		PricedItem pi=pricedItemDAO.load(productId);
		Guarantee g=guaranteeDAO.load(guaranteeId);
/*		Set<Guarantee> guarantees=pi.getGuarantees();
		guarantees.add(g);
		pi.setGuarantees(guarantees);*/
		Set<PricedItem> pricedItems=g.getPricedItems();
		pricedItems.add(pi);
		g.setPricedItems(pricedItems);
		saveOrUpdateGuarantee(g);
		pricedItemDAO.saveOrUpdate(pi);
	}
}
