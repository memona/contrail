/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 837 $
 * @lastrevision   $Date: 2011-02-25 13:44:24 +0100 (Fri, 25 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/PolicyManagerImpl.java $
 */

package org.slasoi.businessManager.common.service.impl;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.dao.ParamTypeDAO;
import org.slasoi.businessManager.common.dao.PolicyDAO;
import org.slasoi.businessManager.common.dao.PolicyTypeDAO;
import org.slasoi.businessManager.common.dao.PolicyValueDAO;
import org.slasoi.businessManager.common.dao.ProductDAO;
import org.slasoi.businessManager.common.model.EmParamType;
import org.slasoi.businessManager.common.model.EmPolicies;
import org.slasoi.businessManager.common.model.EmPolicyType;
import org.slasoi.businessManager.common.model.EmPolicyValues;
import org.slasoi.businessManager.common.model.EmPolicyValuesId;
import org.slasoi.businessManager.common.model.EmRuleParams;
import org.slasoi.businessManager.common.model.EmRuleTemplates;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.service.PolicyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service(value="policyService")
public class PolicyManagerImpl implements PolicyManager {

		static Logger log = Logger.getLogger(PolicyManagerImpl.class);
	
		@Autowired
		private PolicyDAO policyDAO;

		@Autowired
		private PolicyValueDAO policyValueDAO;
		
		@Autowired
		private PolicyTypeDAO policyTypeDAO;
		
		@Autowired
		private ParamTypeDAO paramTypeDAO;

		@Autowired
		private ProductDAO productDAO;
		
		
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyManager#getPolicies()
		 */
		@Transactional(propagation=Propagation.REQUIRED)
	    public List<EmPolicies> getPolicies(){
	    	log.info("getPolicies()");
	    	return policyDAO.getList();
	    }
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyManager#getPolicyById(java.lang.Long)
		 */
		@Transactional(propagation=Propagation.REQUIRED)
	    public EmPolicies getPolicyById(Long id){
	    	log.info("getPolicyById():Id:"+id);
	    	EmPolicies result = policyDAO.load(id);
	    	policyDAO.getHibernateTemplate().initialize(result.getEmSpProductsOffer());
	    	policyDAO.getHibernateTemplate().initialize(result.getEmSpServices());
	    	policyDAO.getHibernateTemplate().initialize(result.getEmPolicyValueses());
	    	
	    	//Change the value if ruleParamType = SELECT
	    	loadPolicyRefValues(result.getEmPolicyValueses());
	    	
	    	return result;
	    }
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyManager#getTemplateParams(java.lang.Long)
		 */
		@Transactional(propagation=Propagation.REQUIRED)
		public List<EmPolicyValues> getTemplateParams(Long policyTypeId) throws Exception{
			
			log.info("getTemplateParams(): TypeId >> "+policyTypeId);
			
			EmPolicyType promoType = policyTypeDAO.load(policyTypeId);
			EmRuleTemplates template = promoType.getEmRuleTemplates();

			//Load Template parameters, and asociate with PromotionValues
			Vector<EmPolicyValues> params = new Vector<EmPolicyValues>();
			Iterator<EmRuleParams> itRuleParams = template.getEmRuleParamses().iterator();
			while(itRuleParams.hasNext()){
				EmRuleParams ruleParam = itRuleParams.next();
				EmPolicyValues promoValue = new EmPolicyValues();
				promoValue.setEmRuleParams(ruleParam);
				
				//Load Param Type Values, in function of txParamTypeName
				EmParamType paramType = ruleParam.getEmParams().getEmParamType();
				paramTypeDAO.loadParamValues(paramType);
				params.add(promoValue);
			}
			
			return params;
		}

		@Transactional(propagation=Propagation.REQUIRED)
		public List<EmPolicyValues> loadPolicyParams(Long id) throws Exception{
			
			log.info("loadPolicyParams(): Id >> "+id);
			
			EmPolicies policy = policyDAO.load(id);
			policyDAO.getHibernateTemplate().initialize(policy.getEmPolicyValueses());

			//Load Template parameters, and asociate with PromotionValues
			Vector<EmPolicyValues> params = new Vector<EmPolicyValues>();
			Iterator<EmPolicyValues> itPolicyValues = policy.getEmPolicyValueses().iterator();
			while(itPolicyValues.hasNext()){
				EmPolicyValues promoValue = itPolicyValues.next();
				EmRuleParams ruleParam = promoValue.getEmRuleParams();
				promoValue.setEmRuleParams(ruleParam);
				
				//Load Param Type Values, in function of txParamTypeName
				EmParamType paramType = ruleParam.getEmParams().getEmParamType();
				paramTypeDAO.loadParamValues(paramType);
				params.add(promoValue);
			}
			
			Collections.sort(params);
			
			return params;
		}
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyManager#loadPolicyRefValues(java.util.Set)
		 */
		@Transactional(propagation=Propagation.REQUIRED)
		public void loadPolicyRefValues(Set<EmPolicyValues> policyValues){
			
			//if param Type = SELECT, load value of reference TABLE
			Iterator<EmPolicyValues> itValues = policyValues.iterator();
			while(itValues.hasNext()){
				EmPolicyValues value = itValues.next();
				EmParamType paramType = value.getEmRuleParams().getEmParams().getEmParamType(); 
				if(paramType.getTxParamTypeName().equals(Constants.PARAM_TYPE_SELECT)){
					String refValue = paramTypeDAO.getRefValue(paramType, value.getTxParamValue());
					if(refValue!=null && refValue.length()>0)
						value.setTxRefParamValue(refValue);
				}
			}
		}
		
		
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyManager#deletePolicy(org.slasoi.businessManager.common.model.EmPolicies)
		 */
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		public void deletePolicy(EmPolicies policy){
			log.info("deletePolicy()");
			policyDAO.delete(policy);
		}
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyManager#getProductOffersPolicies(java.lang.Long)
		 */
		@Transactional(propagation=Propagation.REQUIRED)
		public EmSpProducts getProductOffersPolicies(Long productId){
			EmSpProducts product = productDAO.load(productId);
			
			Iterator<EmSpProductsOffer> offers = product.getEmSpProductsOffers().iterator();
			
			//Load policies of all offers
			while(offers.hasNext()){
				EmSpProductsOffer offer = offers.next();
				Hibernate.initialize(offer.getEmPolicieses());
			}
			
			return product;
		}
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyManager#savePolicy(org.slasoi.businessManager.common.model.EmPolicies, java.lang.Long, java.lang.Long, java.lang.Long, java.util.List, java.util.List)
		 */
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		public void savePolicy(EmPolicies policy, Long nuPolicyTypeId, Long nuProductOfferId, Long nuServiceId, 
				List<Long> paramIds, List<String> paramValues) throws Exception{
			
			log.info("savePolicy()");
			
			EmPolicyType policyType = policyTypeDAO.load(nuPolicyTypeId);
			Long ruleTemplateId = policyType.getEmRuleTemplates().getNuRuleTemplateId();
			policy.setEmPolicyType(policyType);
			policy.setEmSpProductsOffer(new EmSpProductsOffer(nuProductOfferId));
			policy.setEmSpServices(new EmSpServices(nuServiceId));
			
			log.info("Policy ID >>>>>>>> "+policy.getNuPolicyId());
			if(policy.getNuPolicyId()==null || policy.getNuPolicyId()==0){
				policy.setTxStatus(Constants.STATE_PENDING);
				policyDAO.save(policy);
			}else{
				policyDAO.update(policy);
			}
			
			log.info("Policy ID After Save >>>>>>>> "+policy.getNuPolicyId());
			
			log.info("Param Size >>>>>>> "+paramIds.size());
			//After save Policy Adds the parameter values... 
			for(int i=0; i<paramIds.size(); i++){
				EmPolicyValuesId id = new EmPolicyValuesId(policy.getNuPolicyId(), ruleTemplateId, paramIds.get(i));
				EmPolicyValues policyValue = new EmPolicyValues(id, paramValues.get(i));
				policy.getEmPolicyValueses().add(policyValue);
				log.info("New Policy Value Added: "+paramIds.get(i) +" - "+paramValues.get(i));
				policyValueDAO.saveOrUpdate(policyValue);
			}
		}
		
		/* (non-Javadoc)
		 * @see org.slasoi.businessManager.service.PolicyManager#savePoliciesStatus(java.util.List, java.util.List)
		 */
		@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
		public void savePoliciesStatus(List<Long> policiesIds, List<String> policiesStatus){
			
			for(int i=0; i<policiesIds.size(); i++){
				EmPolicies policy = policyDAO.load(policiesIds.get(i));
				policy.setTxStatus(policiesStatus.get(i));
			}
		}			

		
}
