/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 688 $
 * @lastrevision   $Date: 2011-02-11 14:52:14 +0100 (Fri, 11 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/service/impl/PartyManagerImpl.java $
 */

/*
 * 
 */
package org.slasoi.businessManager.common.service.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.model.pac.EmPenalty;
import org.slasoi.businessManager.common.service.SlaUtilService;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.DomainExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Guaranteed.Action.Defn;
import org.slasoi.slamodel.sla.Guaranteed.State;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.business.Penalty;
import org.slasoi.slamodel.sla.business.Termination;
import org.slasoi.slamodel.vocab.core;
import org.springframework.stereotype.Service;

@Service(value = "slaUtilService")
public class SlaUtilServiceImpl implements SlaUtilService {

    private Logger logger = Logger.getLogger(SlaUtilServiceImpl.class);

    public EmPenalty getPenaltiesFromSLA(SLA sla, String guaranteeTerm) {

        Penalty slaPenalty = null;
        String obligatedParty = null;
        AgreementTerm[] terms = sla.getAgreementTerms();

        for (AgreementTerm term : terms) {
            Guaranteed[] guaranteed = term.getGuarantees();

            for (Guaranteed g : guaranteed) {

                String agreementTermId = term.getId().toString();
                if (agreementTermId.equals(guaranteeTerm)) {
                    if (g instanceof Guaranteed.Action) {
                        Guaranteed.Action ga = (Guaranteed.Action) g;
                        obligatedParty = ga.getActorRef().toString();
                        Defn action = ga.getPostcondition();
                        if (action instanceof org.slasoi.slamodel.sla.business.Penalty) {
                            slaPenalty = (org.slasoi.slamodel.sla.business.Penalty) action;
                        }
                    }
                }
            } // guaranteed
        } // terms

        EmPenalty penalty = null;
        if (slaPenalty != null) {

            logger.info("Penalty = " + slaPenalty.getPrice().getValue() + " " + slaPenalty.getPrice().getDatatype());

            penalty = new EmPenalty();
            String description = "Penalty following a violation";
            penalty.setDescription(description);
            penalty.setObligatedParty(obligatedParty);
            penalty.setPenaltyDate(new Date());
            penalty.setValue(new Float(slaPenalty.getPrice().getValue()).floatValue());
        }
        else {
            logger.info("No penalties for  guarantee term " + guaranteeTerm);
        }

        return penalty;
    }

    public boolean checkTermination(SLA sla, String guaranteeTerm) {

        boolean result = false;
        AgreementTerm[] terms = sla.getAgreementTerms();

        for (AgreementTerm term : terms) {
            Guaranteed[] guaranteed = term.getGuarantees();

            for (Guaranteed g : guaranteed) {

                String agreementTermId = term.getId().toString();
                if (agreementTermId.equals(guaranteeTerm)) {
                    if (g instanceof Guaranteed.Action) {
                        Guaranteed.Action ga = (Guaranteed.Action) g;
                        Defn action = ga.getPostcondition();
                        if (action instanceof Termination) {
                            result = true;
                        }
                    }
                }
            } // guaranteed
        } // terms

        logger.info("Termination = " + result);

        return result;
    }

    public boolean checkAvailability(SLA sla, float availability) {

        boolean result = false;
        AgreementTerm[] terms = sla.getAgreementTerms();

        for (AgreementTerm term : terms) {
            Guaranteed[] guaranteed = term.getGuarantees();

            for (Guaranteed g : guaranteed) {

                String agreementTermId = term.getId().toString();
                if (agreementTermId.contains(AVAILABILITY)) {
                    if (g instanceof Guaranteed.State) {
                        Guaranteed.State ga = (Guaranteed.State) g;
                        ConstraintExpr ce = ((State) g).getState();
                        if (ce instanceof TypeConstraintExpr) {
                            // TypeConstraintExpr
                            TypeConstraintExpr tce = (TypeConstraintExpr) ce;
                            DomainExpr de = tce.getDomain();
                            if (de instanceof SimpleDomainExpr) {
                                SimpleDomainExpr sde = (SimpleDomainExpr) de;
                                String operator = sde.getComparisonOp().toString();
                                ValueExpr ve = sde.getValue();
                                if (ve instanceof CONST) {
                                    String value = ((CONST) ve).getValue();
                                    String unit = ((CONST) ve).getDatatype().toString();

                                    result = checkExpression(availability, operator, value);
                                }
                            } // if SimpleDomainExpr
                        } // if TypeConstraintExpr
                    } // if Guaranteed.State

                }
            } // guaranteed
        } // terms

        logger.info("Availability check = " + result);

        return result;
    }

    public boolean checkGuaranteed(SLA sla, String agreementTermId, float measurement) {

        logger.debug("Checking " + agreementTermId + " agreement term in " + sla.getUuid().toString());
        boolean result = false;
        AgreementTerm[] terms = sla.getAgreementTerms();
        
        for (AgreementTerm term : terms) {
            Guaranteed[] guaranteed = term.getGuarantees();
            
            for (Guaranteed g : guaranteed) {

                String agreementTermIdSLA = term.getId().toString();
                if (agreementTermIdSLA.contains(agreementTermId)) {
                    if (g instanceof Guaranteed.State) {
                        Guaranteed.State ga = (Guaranteed.State) g;
                        ConstraintExpr ce = ((State) g).getState();
                        if (ce instanceof TypeConstraintExpr) {
                            // TypeConstraintExpr
                            TypeConstraintExpr tce = (TypeConstraintExpr) ce;
                            DomainExpr de = tce.getDomain();
                            if (de instanceof SimpleDomainExpr) {
                                SimpleDomainExpr sde = (SimpleDomainExpr) de;
                                String operator = sde.getComparisonOp().toString();
                                ValueExpr ve = sde.getValue();
                                if (ve instanceof CONST) {
                                    String value = ((CONST) ve).getValue();
                                    String unit = ((CONST) ve).getDatatype().toString();

                                    result = checkExpression(measurement, operator, value);
                                }
                            } // if SimpleDomainExpr
                        } // if TypeConstraintExpr
                    } // if Guaranteed.State

                }
            } // guaranteed
        } // terms

        logger.info("Check result = " + result);

        return result;
    }

    public boolean checkExpression(float value, String operator, String reference) {

        logger.debug("Checking if " + value + " " + operator + " " + reference);
        boolean result = false;

        if (operator.equals(core.identical_to))
            result = (value == new Float(reference).floatValue());
        else if (operator.equals(core.not_equals))
            result = (value != new Float(reference).floatValue());
        else if (operator.equals(core.less_than))
            result = (value < new Float(reference).floatValue());
        else if (operator.equals(core.less_than_or_equals))
            result = (value <= new Float(reference).floatValue());
        else if (operator.equals(core.greater_than))
            result = (value > new Float(reference).floatValue());
        else if (operator.equals(core.greater_than_or_equals))
            result = (value >= new Float(reference).floatValue());
        else if (operator.equals("=="))
            result = (value == new Float(reference).floatValue());
        else if (operator.equals("!="))
            result = (value != new Float(reference).floatValue());
        else if (operator.equals("<"))
            result = (value < new Float(reference).floatValue());
        else if (operator.equals("<="))
            result = (value <= new Float(reference).floatValue());
        else if (operator.equals(">"))
            result = (value > new Float(reference).floatValue());
        else if (operator.equals(">="))
            result = (value >= new Float(reference).floatValue());

        return result;
    }
}
