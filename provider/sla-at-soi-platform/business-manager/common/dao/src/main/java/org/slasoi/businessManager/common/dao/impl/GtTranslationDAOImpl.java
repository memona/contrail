package org.slasoi.businessManager.common.dao.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.GtTranslationDAO;
import org.slasoi.businessManager.common.model.EmGtTranslation;
import org.slasoi.businessManager.common.model.EmGtTranslationId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class GtTranslationDAOImpl extends AbstractHibernateDAOImpl<EmGtTranslation, EmGtTranslationId>  implements GtTranslationDAO{
    private static final Logger log = Logger.getLogger( GtTranslationDAOImpl.class );
    
    @Autowired
    public GtTranslationDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
    
    @Override
    protected Class<EmGtTranslation> getDomainClass() {
            return EmGtTranslation.class;
    }
    
    /**
     * Get the translation for the given term 
     * @param Type
     * @param translstion
     * @return
     */
    public  EmGtTranslation getGtTranslation(String type, EmGtTranslation translation){
        log.debug("Into getGtTranslation methos. Type:"+type);        
        Criteria criteria = this.getSession().createCriteria(EmGtTranslation.class);
        criteria.setFetchMode("id", FetchMode.JOIN);
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
        //criteria.createAlias("id","id",CriteriaSpecification.LEFT_JOIN);
        
        if("business".equalsIgnoreCase(type)){
            log.info("Business search");
            criteria.add(Restrictions.eq("id.txBslatId", translation.getId().getTxBslatId()));
            criteria.add(Restrictions.eq("id.txMixAgreedId", translation.getId().getTxMixAgreedId()));
            criteria.add(Restrictions.eq("id.txMixAgreementTermId", translation.getId().getTxMixAgreementTermId()));
        }else if("lowLayers".equalsIgnoreCase(type)){
            log.info("Low layer search");  
            criteria.add(Restrictions.eq("id.txBslatId", translation.getId().getTxBslatId()));
            criteria.add(Restrictions.eq("id.txSlatId", translation.getId().getTxSlatId()));
            criteria.add(Restrictions.eq("txAgreedId", translation.getTxAgreedId()));
            criteria.add(Restrictions.eq("txAgreementTermId", translation.getTxAgreementTermId()));        
        }
        List<EmGtTranslation> result = criteria.list();
        if(result != null && result.size() > 0){
            return  result.get(0);
        }else{
            return null;
        }
    }
}
