/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.PricedItemDAO;
import org.slasoi.businessManager.common.model.pricing.Price;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value="pricedItemDAO")
public class PricedItemDAOImpl extends AbstractHibernateDAOImpl<PricedItem, Long> implements PricedItemDAO{
	
	private static final Logger log = Logger.getLogger(PricedItemDAOImpl.class);
	
	@Autowired
	public PricedItemDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
	
    @Override
    protected Class<PricedItem> getDomainClass() {
            return PricedItem.class;
    }

    
    /* Finds
     * 
     */
    
    public PricedItem findPricedItemByName(String name){
    	log.debug("Inside findPricedItemByName");
    	if(name!=null){
    	Criteria criteria=this.getSession().createCriteria(PricedItem.class);
    	criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
    	criteria.add(Restrictions.eq("productName", name));
    	List<PricedItem> results=(List<PricedItem>)criteria.list();
    	return results.get(0);
    	}
    	return null;
    }
   
}
