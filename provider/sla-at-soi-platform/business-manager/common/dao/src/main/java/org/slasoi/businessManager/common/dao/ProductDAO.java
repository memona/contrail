/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 747 $
 * @lastrevision   $Date: 2011-02-17 16:42:40 +0100 (Thu, 17 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/dao/ProductDAO.java $
 */

package org.slasoi.businessManager.common.dao;

import java.util.List;

import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmSpProducts;

public interface ProductDAO extends AbstractHibernateDAO<EmSpProducts, Long>{
    
    public List<EmSpProducts> findByFilter(String filter);
    public List<EmSpProducts> findByFilters(List<String> filters);
    public List<EmSpProducts> findByPartyAndFilter(EmParty party, String filter);
    public List<EmSpProducts> findByStatusAndFilter(String status, String filter);
    public List<EmSpProducts> findByStatusAndFilters(String status, List<String> filters);
    public List<EmSpProducts> getAll();  
    public List<EmSpProducts> getAll(String Status);  
    public EmSpProducts getProductOffersLoaded(Long id);
    public List<EmSpProducts> findProductForContract(String filter, String productStatus, String offerStatus, Long customerCountryId);
    public List<EmSpProducts> findProductForPublishBSLAT(String filter);
    public List<EmSpProducts> getProductByService(Long nuServiceid);
    public List<EmSpProducts> getProductsWithAllCategories(List<String> categories, String productStatus);

}
