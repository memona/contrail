/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 493 $
 * @lastrevision   $Date: 2011-01-25 17:03:20 +0100 (Tue, 25 Jan 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/common/src/main/java/org/slasoi/businessManager/common/dao/impl/ServiceDAOImpl.java $
 */

package org.slasoi.businessManager.common.dao.impl;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.ServiceDAO;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;



@Repository(value="serviceDAO")
public class ServiceDAOImpl extends AbstractHibernateDAOImpl<EmSpServices, Long> implements ServiceDAO  {
	
	private static final Logger log = Logger.getLogger( ServiceDAOImpl.class );

	
	@Autowired
	public ServiceDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
	
    @Override
    protected Class<EmSpServices> getDomainClass() {
            return EmSpServices.class;
    }
    
    
    
    @Override
	public List<EmSpServices> getList() {
    	log.debug(">>>> getList()");
		
		Criteria criteria = this.getSession().createCriteria(EmSpServices.class);
		criteria.createAlias("emParty","party",CriteriaSpecification.LEFT_JOIN);
		
		return criteria.addOrder(Order.asc("dtInsertdate")).list();
	}

	public List<EmSpServices> getServicesByParty(Long partyId){

    	log.debug(">>>> getServicesByParty()");
		
		Criteria criteria = this.getSession().createCriteria(EmSpServices.class);
		criteria.createAlias("emParty","party",CriteriaSpecification.LEFT_JOIN);
		criteria.add(Restrictions.eq("party.nuPartyid", partyId));
		
		return criteria.addOrder(Order.asc("dtInsertdate")).list();
    }
    

	public List<EmSpServices> findByParty(Long partyId, String filter) {
		return find(null, filter);
	}

	public List<EmSpServices> findByFilter(String filter) {
		return find(null, filter);
	}
	
	private List<EmSpServices> find(Long partyId, String filter) {
		try {
		    
			log.debug(">>>> findByFilter() : finding EmSpServices By Filter");
			
			Criteria criteria = this.getSession().createCriteria(EmSpServices.class);
			
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		    	//Para poder filtrar por provider
		    	criteria.createAlias("emParty","party",CriteriaSpecification.LEFT_JOIN);
		    	
		    	criteria.createAlias("emParty.emOrganization","org",CriteriaSpecification.LEFT_JOIN);
		    	criteria.createAlias("emParty.emIndividual","ind",CriteriaSpecification.LEFT_JOIN);
		    	
		    	criteria.createAlias("emServiceSpecifications","sp",CriteriaSpecification.LEFT_JOIN);
		    	
		    	if(partyId!=null)
		    		criteria.add(Restrictions.like("party.nuPartyid", filter));

		    	if(filter!=null){
			    	criteria.add(
			    			Restrictions.disjunction()
			    				.add(Restrictions.like("sp.txName", filter))
			    				.add(Restrictions.like("txServicename", filter))
			    				.add(Restrictions.like("txServicedesc", filter))
			    				.add(Restrictions.like("org.txTradingname", filter))
			    				.add(Restrictions.like("ind.txFirstName", filter))
			    	);
		    	}
		    
		    List<EmSpServices> result = criteria.addOrder(Order.asc("dtInsertdate")).list();
		    
		    return result;				
		
		} catch (RuntimeException re) {
			log.error("findByFilter failed", re);
			throw re;
		}
	}	
	
	public EmSpServices getServiceBySlatId(String txSlatid){
		
		Criteria criteria = this.getSession().createCriteria(EmSpServices.class);
		List<EmSpServices> services = criteria.add(Restrictions.eq("txSlatid", txSlatid)).list();
		EmSpServices service = null; 
		if(services!=null && !services.isEmpty())
			service = services.get(0);
		
		return service;
		
	}
    
}
