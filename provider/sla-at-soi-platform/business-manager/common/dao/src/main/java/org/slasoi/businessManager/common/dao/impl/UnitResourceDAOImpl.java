/**
 * Copyright 2014 Hewlett-Packard Development Company, L.P.                
 *                                                                          
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                          
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                          
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.                                          
 */

package org.slasoi.businessManager.common.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.UnitResourceDAO;
import org.slasoi.businessManager.common.model.pricing.PricedItem;
import org.slasoi.businessManager.common.model.pricing.Services;
import org.slasoi.businessManager.common.model.pricing.UnitResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository(value="unitResourceDAO")
public class UnitResourceDAOImpl extends AbstractHibernateDAOImpl<UnitResource, Long> implements UnitResourceDAO{
	
	private static final Logger log = Logger.getLogger(UnitResourceDAOImpl.class);
	
	@Autowired
	public UnitResourceDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
	
    @Override
    protected Class<UnitResource> getDomainClass() {
            return UnitResource.class;
    }
    
    
    public UnitResource findUnitResourceByType(String type){
    	log.debug("Inside findUnitResourceByType");
    	if(type!=null){
    		Criteria criteria=this.getSession().createCriteria(UnitResource.class);
    		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
    		criteria.add(Restrictions.eq("unitResourceType", type));
    		List<UnitResource> results=(List<UnitResource>)criteria.list();
    		return results.get(0);
    	}
    	return null;
    }
}
