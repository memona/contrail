package org.slasoi.businessManager.productSLA.SLATComposition.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.drools.DroolsManager;
import org.slasoi.businessManager.common.drools.Rule;
import org.slasoi.businessManager.common.drools.RuleParam;
import org.slasoi.businessManager.common.drools.Template;
import org.slasoi.businessManager.common.util.GTDifference;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.common;




public class SLATMerging {
    static Logger log = Logger.getLogger(SLATMerging.class);                 
 
 /**
  * This method merges the different gts into one.   
  * @param gtDiffs
  * @return
  * @throws Exception
  */
    @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
    public GTDifference mergeSLATs(List<GTDifference> gtDiffs) throws Exception{       
        log.debug("Into mergeSLATs method ");
        //Prepare the WorkingMemory, add the ComponentPrice at Facts
        ArrayList<Object> arrayFacts;
        //load the different rules to apply.   
        HashMap<Long, Template> templates = this.loadRules();
        GTDifference gtResult = null;
        for(int i=1;i<gtDiffs.size();i++){
            arrayFacts = new ArrayList<Object>();
            GTDifference initial;
            if(i==1){
                initial = gtDiffs.get(0);                             
            }else{
                initial = new GTDifference();
                initial.setOperationComparator(gtResult.getOperationComparator());
                initial.setParameter(gtResult.getParameter());
                initial.setValue(gtResult.getValue());
                initial.setGuaranteeId(gtResult.getGuaranteeId());
                initial.setServiceName(gtResult.getServiceName());
            }
            //difference is used to set the order in the rule execution
            initial.setDiference(0);
            arrayFacts.add(initial); 
            GTDifference comparator = gtDiffs.get(i);
            comparator.setDiference(1);
            arrayFacts.add(comparator);
            gtResult = new GTDifference();
            gtResult.setDiference(2);
            arrayFacts.add(gtResult);
            //Call the rule
            log.debug("Array Facts >>> "+arrayFacts.size());
            //Create DroolsManager, with array templates and facts
            log.debug("Generating Drools Manager");
            DroolsManager dm = new DroolsManager(templates.values(), arrayFacts);
            log.debug("Process rules");
            dm.processRules();
            log.debug("<<<<<<<< Rules Executed >>>>>>>>>");
            log.debug("Array Facts After >>> "+arrayFacts.size());                       
        }  
        gtResult.setServiceName("MergedService");
        gtResult.setGuaranteeId("AvailavilityMergedGT");
        return gtResult;
    }

 /**
  * This method load the corresponding rules to apply
  * @return
  */
   private HashMap<Long, Template> loadRules(){
       log.debug("Into loadRules method.");
       HashMap<Long, Template> templates = new HashMap<Long, Template>();       
       //
       //Template template = templates.get(ruleTplt.getNuRuleTemplateId());
       //Currently we have only a template.
       Template template = templates.get(new Long(1));
       if(template==null){
           //si no esta se a�ade
           //template = new Template(ruleTplt.getNuRuleTemplateId(), policyType.getTxName(), ruleTplt.getTxDescription(), ruleTplt.getTxTemplateDrt());
           template = new Template(new Long(1), "Merging Template", "Merging Template Description",getRule());
           templates.put(template.getId(), template);
       }
       
       ArrayList<RuleParam> ruleParams = new ArrayList<RuleParam>();            
       //RuleParam rp = new RuleParam(promoValue.getEmRuleParams().getNuParamRuleOrder(), promoValue.getTxParamValue());
       //Generate the specific param for this rule
       RuleParam rp = new RuleParam(new Long(1), common.$availability);
       ruleParams.add(rp);
       RuleParam rp2 = new RuleParam(new Long(2), core.$greater_than);
       ruleParams.add(rp2);
       //Specific Rule and its parameters
       Rule rule = new Rule(new Long(1),"MGTS Merging", "GTS Merging", ruleParams);
       template.getRules().add(rule);       
       return templates;          
   }

/**
 * Get the string with the template rule from a file.
 * @return
 */
    private String getRule(){
        log.info("Get Rule from drl file"); 
        String rule ="";
        String ruleFileName = "/rule.drl";
        //File archivo = null;
        //FileReader fr = null;
        BufferedReader br = null;

        try {
           //archivo = new File (ruleFileName);
           //fr = new FileReader (archivo);
           //br = new BufferedReader(fr);
           br = new BufferedReader(new InputStreamReader(SLATMerging.class.getResourceAsStream(ruleFileName)));                              
           // Lectura del fichero
           String linea;
           while((linea=br.readLine())!=null){
              rule += linea + "\n";
           }
        }
        catch(Exception e){
           e.printStackTrace();
        }
        return rule;
    
    }
    
}
