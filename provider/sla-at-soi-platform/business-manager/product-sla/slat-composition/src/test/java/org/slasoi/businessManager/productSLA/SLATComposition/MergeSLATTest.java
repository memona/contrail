package org.slasoi.businessManager.productSLA.SLATComposition;

import java.util.ArrayList;
import java.util.List;

import org.slasoi.businessManager.common.util.GTDifference;
import org.slasoi.businessManager.productSLA.SLATComposition.impl.SLATMerging;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.common;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class MergeSLATTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MergeSLATTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MergeSLATTest.class );
    }
    
    
    public void testMergeSLATs()
    {
        System.out.println("#################testMergeSLATs");
        try{
            SLATMerging merge = new SLATMerging();
            List<GTDifference> gtDiffs = new ArrayList<GTDifference>();
            GTDifference gt1 = new GTDifference();
            gt1.setValue(90);
            gt1.setDiference(0);
            gt1.setParameter(common.$availability);
            gt1.setOperationComparator(core.$greater_than);
            gtDiffs.add(gt1);
            GTDifference gt2 = new GTDifference();
            gt2.setValue(95);
            gt2.setDiference(1);
            gt2.setOperationComparator(core.$greater_than);
            gt2.setParameter(common.$availability);
            gtDiffs.add(gt2);              
            System.out.println("#################Apply rules");
            GTDifference gt3 = merge.mergeSLATs(gtDiffs);          
            //Solutions 
            System.out.println("#################Name:" +gt3.getGuaranteeId());
            System.out.println("#################Value:" +gt3.getValue());
            
            
        }catch(Exception e ){
            e.printStackTrace();
        }        
        
    }

}
