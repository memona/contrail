/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/product-sla/product-discovery/src/test/java/org/slasoi/businessManager/productSLA/productDiscovery/_examples.java $
 */

package org.slasoi.businessManager.productSLA.productDiscovery;

import org.slasoi.gslam.core.negotiation.SLATemplateRegistry.Metadata;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.Interface;
import org.slasoi.slamodel.service.Interface.Specification;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.Party.Operative;
import org.slasoi.slamodel.vocab.sla;

class Examples {
    
    final static String $slam_id = "{business45slamanager451}";
    final static String $another_provider_id = "{tid}";
    final static String $registrar_id = "{tid}";
    // parties ..
    final static ID provider_id = new ID("{tid_id}");
    final static ID provider_agent_1_id = new ID("{tid_agent_1}");
    final static ID customer_id = new ID("{customer_id}");
    // interface specifications ...
    final static String $i_simple1 = "i_simple1";
    final static String $i_simple2 = "i_simple2";
    final static String $i_composite1 = "i_composite1";
    // template ids ...
    final static UUID template_all_id = new UUID("template_all");
    final static UUID template_simple1_id = new UUID("BSLATID1");
    final static UUID template_simple2_id = new UUID("BSLATID2");
    final static UUID template_composite1_id = new UUID("Btemplate_composite1");
    // other ..
    final static ID idec_1_id = new ID("{idec_1_id}");
    final static ID idec_1_endpoint_id = new ID("{idec_1_endpoint_id}");
    final static ID idec_2_id = new ID("{idec_2_id}");
    final static ID idec_3_id = new ID("{idec_3_id}");
   
    
    static Metadata p_METADATA_1(){
        Metadata m = new Metadata();
        m.setPropertyValue(Metadata.provider_uuid, $slam_id);
        m.setPropertyValue(Metadata.registrar_id, $registrar_id);
        return m;
    }
    
    static Metadata p_METADATA_2(){
        Metadata m = new Metadata();
        m.setPropertyValue(Metadata.provider_uuid, $another_provider_id);
        m.setPropertyValue(Metadata.registrar_id, $registrar_id);
        return m;
    }
    
    static SLATemplate p_SLAT_all(){
        SLATemplate slat = new SLATemplate();
        slat.setUuid(template_all_id);
        Party provider = new Party(provider_id, sla.provider);
            Operative provider_agent_1 = new Operative(provider_agent_1_id);
            provider.setOperatives(new Operative[]{
                provider_agent_1
            });
        Party customer = new Party(customer_id, sla.customer);
        slat.setParties(new Party[]{
            provider,
            customer
        });
        InterfaceDeclr iface_declr_1 = new InterfaceDeclr(idec_1_id, sla.provider, p_ISPEC_simple1());
            iface_declr_1.setEndpoints(new Endpoint[]{ 
                new Endpoint(idec_1_endpoint_id, sla.telephone)
            });
        InterfaceDeclr ispec_1 = new InterfaceDeclr(idec_2_id, sla.provider, p_ISPEC_simple2());
        InterfaceDeclr ispec_2 = new InterfaceDeclr(idec_3_id, sla.provider, p_ISPEC_composite1());
        slat.setInterfaceDeclrs(new InterfaceDeclr[]{
            iface_declr_1, 
            ispec_1, 
            ispec_2 
        });
        return slat;
    }
    
    static SLATemplate p_SLAT_simple1(){
        SLATemplate slat = new SLATemplate();
        slat.setUuid(template_simple1_id);
        Party provider = new Party(provider_id, sla.provider);
        Party customer = new Party(customer_id, sla.customer);
        slat.setParties(new Party[]{
            provider,
            customer
        });
        InterfaceDeclr idec_1 = new InterfaceDeclr(idec_1_id, sla.provider, p_ISPEC_simple1());
        idec_1.setEndpoints(new Endpoint[]{ 
                new Endpoint(idec_1_endpoint_id, sla.telephone)
            });
        slat.setInterfaceDeclrs(new InterfaceDeclr[]{ idec_1, });
        return slat;
    }
    
    static SLATemplate p_SLAT_simple2(){
        SLATemplate slat = new SLATemplate();
        slat.setUuid(template_simple2_id);
        Party provider = new Party(provider_id, sla.provider);
        Party customer = new Party(customer_id, sla.customer);
        slat.setParties(new Party[]{
            provider,
            customer
        });
        InterfaceDeclr idec_1 = new InterfaceDeclr(idec_1_id, sla.provider, p_ISPEC_simple2());
        slat.setInterfaceDeclrs(new InterfaceDeclr[]{ idec_1, });
        return slat;
    }
    
    static SLATemplate p_SLAT_composite1(){
        SLATemplate slat = new SLATemplate();
        slat.setUuid(template_composite1_id);
        Party provider = new Party(provider_id, sla.provider);
        slat.setParties(new Party[]{
            provider
        });
        InterfaceDeclr idec_1 = new InterfaceDeclr(idec_1_id, sla.provider, p_ISPEC_composite1());
        slat.setInterfaceDeclrs(new InterfaceDeclr[]{ idec_1, });
        return slat;
    }

    static Specification p_ISPEC_composite1(){
        Specification spec = new Specification();
        spec.setName($i_composite1);
        spec.setExtended(new UUID[]{ new UUID($i_simple1), new UUID($i_simple2) });
        return spec;
    }
    
    static Specification p_ISPEC_simple1(){
        Specification spec = new Specification();
        spec.setName($i_simple1);
        Interface.Operation op1 = new Interface.Operation(new ID("op1"));
        spec.setOperations(new Interface.Operation[]{ op1 });
        return spec;
    }
    
    static Specification p_ISPEC_simple2(){
        Specification spec = new Specification();
        spec.setName($i_simple2);
        Interface.Operation op1 = new Interface.Operation(new ID("opX"));
        spec.setOperations(new Interface.Operation[]{ op1 });
        return spec;
    }

}

