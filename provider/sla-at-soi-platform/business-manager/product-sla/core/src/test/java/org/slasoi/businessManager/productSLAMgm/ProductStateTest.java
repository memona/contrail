/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/product-sla/core/src/test/java/org/slasoi/businessManager/productSLAMgm/ProductStateTest.java $
 */

package org.slasoi.businessManager.productSLAMgm;


import javax.annotation.Resource;
import junit.framework.Test;
import junit.framework.TestSuite;
import org.slasoi.businessManager.common.util.ProductState;
import org.slasoi.businessManager.common.service.ProductManager;
import org.slasoi.businessManager.productSLA.management.ProductManagement;
import org.springframework.test.AbstractTransactionalSpringContextTests;

public class ProductStateTest extends AbstractTransactionalSpringContextTests{
    
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bm-productsla-core-context.xml"};
    }
    @Resource
    private ProductManager productService;
    

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ProductStateTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ProductStateTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testCheckState()
    { System.out.println("Init testCheckState");
      try{                
          ProductManagement management = new ProductManagement();
          int result = management.ckeckState(ProductState.PROMOTED, ProductState.ACTIVE);
          System.out.println("Result OK:"+ result);
          assertEquals(0, result);
          result = management.ckeckState(ProductState.DEFINED, ProductState.REJECTED);
          System.out.println("Result KO:"+ result);
          assertEquals(-1, result);
      }catch(Exception e){
         e.printStackTrace();
      }
    }
    
    /**
     * Rigourous Test :-)
     */
    public void testChangeProductState()
    { System.out.println("Init testChangeProductState");
      try{        
          ProductManagement manager = new ProductManagement();

          logger.info("**************");
          logger.info("USING AUTOWIRE");
          logger.info("**************");
          //manager.setproductManager(this.productService);
          
          boolean result = manager.changeProductState(1, ProductState.INACTIVE);
          System.out.println("Result:"+ result);
      }catch(Exception e){
         e.printStackTrace();
      }
    }
    
   
}
