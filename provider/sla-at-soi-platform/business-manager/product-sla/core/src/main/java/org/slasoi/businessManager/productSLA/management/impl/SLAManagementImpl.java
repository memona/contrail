/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 807 $
 * @lastrevision   $Date: 2011-02-23 16:37:57 +0100 (Wed, 23 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/product-sla/core/src/main/java/org/slasoi/businessManager/productSLA/management/impl/SLAManagementImpl.java $
 */

package org.slasoi.businessManager.productSLA.management.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.slasoi.bslamanager.main.context.BusinessContextService;
import org.slasoi.businessManager.billingEngine.core.BillingEngine;
import org.slasoi.businessManager.common.model.EmComponentPrice;
import org.slasoi.businessManager.common.model.EmCustomersProducts;
import org.slasoi.businessManager.common.model.EmCustprodsSlas;
import org.slasoi.businessManager.common.model.EmCustprodsSlasId;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmPriceVariation;
import org.slasoi.businessManager.common.model.EmPriceVariationType;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.common.model.EmSpServices;
import org.slasoi.businessManager.common.service.CustomerProductManager;
import org.slasoi.businessManager.common.service.PriceVariationTypeManager;
import org.slasoi.businessManager.common.service.ProductOfferManager;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.util.LowSLAInfo;
import org.slasoi.businessManager.productSLA.management.SLAManagement;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Guaranteed.Action;
import org.slasoi.slamodel.sla.Guaranteed.Action.Defn;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.business.ComponentProductOfferingPrice;
import org.slasoi.slamodel.sla.business.PriceModification;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.vocab.business;
import org.slasoi.slamodel.vocab.sla;
import org.slasoi.slamodel.vocab.units;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("slaManagement")
public class SLAManagementImpl implements SLAManagement {

    private static final Logger log = Logger.getLogger(SLAManagementImpl.class);

    @Autowired
    private CustomerProductManager customerProductManager;

    @Autowired
    private ProductOfferManager productOfferService;

    @Autowired
    private PriceVariationTypeManager priceVariationTypeManager;

    @Autowired
    private BillingEngine billingEngineService;

    @Autowired
    private BusinessContextService businessContextService;


    /*
     * @ServiceReference // obtained automatically by Spring-DM and OSGi Container. public void
     * setCustomerProductManager(CustomerProductManager customerProductManager) {
     * log.info("***************Registered Service CustomerProductManager Found*************");
     * this.customerProductManager = customerProductManager; }
     */

    /*
     * @ServiceReference // obtained automatically by Spring-DM and OSGi Container. public void
     * setProductOfferservice(ProductOfferManager productOfferService) {
     * log.info("***************Registered Service ProductOfferManager Found*************"); this.productOfferService =
     * productOfferService; }
     */

    /*
     * @ServiceReference // obtained automatically by Spring-DM and OSGi Container. public void
     * setPriceVariationTypeManager(PriceVariationTypeManager priceVariationTypeManager) {
     * log.info("***************Registered Service priceVariationTypeManager Found*************");
     * this.priceVariationTypeManager = priceVariationTypeManager; }
     */

    /*
     * @ServiceReference // obtained automatically by Spring-DM and OSGi Container. public void
     * setBillingEngine(BillingEngine billingEngineService) {
     * log.info("***************Registered Service BillingService Found*************"); this.billingEngineService =
     * billingEngineService; }
     */

    /**
     * This method initiate an SLA inside SLAManager
     * 
     * @param bSLA
     * @return
     */
    public int startSLA(SLA bSLA, List<LowSLAInfo> lowSLAInfo) {
        log.info("Inside StartSLAMetod");
        log.info("SLAID: " + bSLA.getUuid().getValue());
        // Instances
        EmCustomersProducts customersProducts = new EmCustomersProducts();
        EmParty emParty = new EmParty();
        Set<EmPriceVariation> set = new HashSet<EmPriceVariation>(0);
        EmPriceVariation emPriceVariation;
        EmComponentPrice emComponentPrice = new EmComponentPrice();

        // Fill the pojo EmCustomerProducts
        customersProducts.setTxBslaid(bSLA.getUuid().getValue());
        for (int i = 0; i < bSLA.getParties().length; i++) {
            Party party = bSLA.getParties()[i];
            if (party.getAgreementRole().getValue() != null
                    && party.getAgreementRole().getValue().equalsIgnoreCase(sla.customer.getValue())) {
                emParty.setNuPartyid(party.getId().getValue() != null ? Long.parseLong(party.getId().getValue()) : 0);
            }
        }
        customersProducts.setEmParty(emParty);
        customersProducts.setDtInsertDate(new Date());
        customersProducts.setDtDateBegin(bSLA.getEffectiveFrom().getValue().getTime());
        customersProducts.setDtDateEnd(bSLA.getEffectiveUntil().getValue().getTime());
        // Get Price Offer
        ProductOfferingPrice offerPrice = null;
        try {
            offerPrice = this.getProductOfferingPrice(bSLA.getAgreementTerms());
        }
        catch (Exception e) {
            log.error("Error: " + e.toString());
        }
        if (offerPrice == null) {
            log.error("It is needed an offer price inside the SLA");
            return -1;
        }
        EmSpProductsOffer initialOffer =
                this.productOfferService.getProductOfferById(new Long(offerPrice.getId().getValue()));
        if (initialOffer == null) {
            log.error("The product offer does not exist");
            return -1;
        }
        // set the customer product
        customersProducts.setEmSpProductsOffer(initialOffer);
        customersProducts.setState(Constants.SLA_STATE_OBS);
        ComponentProductOfferingPrice[] components = offerPrice.getComponentProductOfferingPrices();
        ComponentProductOfferingPrice component;
        if (components != null && components.length > 0) {
            for (int i = 0; i < components.length; i++) {
                component = components[i];
                if (component.getPriceModifications() != null && component.getPriceModifications().length > 0) {
                    PriceModification[] modifications = component.getPriceModifications();
                    PriceModification modification;
                    // find the component price
                    Set<EmComponentPrice> componentsPrice = initialOffer.getEmComponentPrices();
                    if (componentsPrice != null && componentsPrice.size() > 0) {
                        Iterator<EmComponentPrice> it = componentsPrice.iterator();
                        EmComponentPrice compo;
                        while (it.hasNext()) {
                            compo = it.next();
                            String name = compo.getEmPriceType().getTxName();
                            int index = component.getPriceType().getValue().lastIndexOf("#") + 1;
                            String type = component.getPriceType().getValue().substring(index);
                            if (name.equalsIgnoreCase(type)) {
                                emComponentPrice = compo;
                                break;
                            }
                        }
                    }
                    //
                    for (int j = 0; j < modifications.length; j++) {
                        modification = modifications[j];
                        emPriceVariation = new EmPriceVariation();
                        STND inc = modification.getType();
                        STND percent = modification.getValue().getDatatype();
                        emPriceVariation.setEmCustomersProducts(customersProducts);
                        emPriceVariation.setEmComponentPrice(emComponentPrice);
                        emPriceVariation.setEmPriceVariationType(getEmPriceVariationType(inc, percent));
                        emPriceVariation.setNuQuantity(new BigDecimal(1));
                        emPriceVariation.setNuValue(new BigDecimal(modification.getValue().getValue()));
                        set.add(emPriceVariation);
                    }
                }
            }
            customersProducts.setEmPriceVariations(set);
        }
        if (lowSLAInfo != null && lowSLAInfo.size() > 0) {
            Set<EmCustprodsSlas> emCustprodsSlases = new HashSet<EmCustprodsSlas>();
            EmCustprodsSlas productSLA;
            EmSpServices service;
            EmCustprodsSlasId id;
            for (LowSLAInfo info : lowSLAInfo) {
                productSLA = new EmCustprodsSlas();
                id = new EmCustprodsSlasId();
                id.setTxBslaid(customersProducts.getTxBslaid());
                id.setTxSlaid(info.getSLATID());
                productSLA.setId(id);
                service = new EmSpServices();
                service.setNuServiceid(info.getServiceID());
                productSLA.setEmSpServices(service);
                emCustprodsSlases.add(productSLA);
            }
            customersProducts.setEmCustprodsSlases(emCustprodsSlases);
        }
        try {
            customerProductManager.saveCustomer(customersProducts);
            // call start billing
            log.error("Call start charging to customer");
            if (!this.billingEngineService.startChargingBSLA(bSLA))
                return -1;
        }
        catch (Exception e) {
            log.error("Error:" + e.toString());
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    /**
     * This method terminate an SLA inside the SLAmanager
     * 
     * @param bSLAID
     * @param reason
     * @param source
     * @return
     */
    public int stopSLA(String bSLAID, String reason, String source) {
        log.info("Inside stopSLA");
        log.info("SLAID: " + bSLAID);
        log.info("Reason: " + reason);
        log.info("Source: " + source);
        try {
            EmCustomersProducts customerProduct = customerProductManager.getCustomerProductById(bSLAID);
            if (customerProduct == null) {
                log.error("There is no SLA with the ID given");
                return -1;
            }
            customerProduct.setDtDateEnd(new Date());
            customerProduct.setState(Constants.SLA_STATE_EXPI);
            customerProduct.setTerminationReason(reason);
            // call stop billing
            customerProductManager.saveCustomer(customerProduct);
            if (businessContextService != null) {
                SLA[] slas = businessContextService.getBusinessContext().getSLARegistry().getIQuery().getSLA(new UUID[] { new UUID(bSLAID) });
                if (slas != null && slas.length > 0) {
                    this.billingEngineService.stopChargingBSLA(slas[0], reason);
                }
            }
            return 0;

        }
        catch (Exception e) {
            log.equals("Error: " + e.toString());
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Obtain the product Offering Price
     * 
     * @param terms
     * @return
     * @throws Exception
     */
    private ProductOfferingPrice getProductOfferingPrice(AgreementTerm[] terms) throws Exception {
        log.info("Inside getProductOfferingPrice method");
        try {
            if (terms != null && terms.length > 0) {
                Action action;
                for (int i = 0; i < terms.length; i++) {
                    Guaranteed[] guarantees = terms[i].getGuarantees();
                    for (int j = 0; j < guarantees.length; j++) {
                        if (guarantees[j] instanceof Action) {
                            log.info("Guarantee is a Guarantee.Action");
                            action = (Action) guarantees[j];
                            Defn upperAction = action.getPostcondition();
                            if (upperAction instanceof ProductOfferingPrice) {
                                log.info("Product offering Price Found");
                                return (ProductOfferingPrice) upperAction;

                            }
                        }
                    }

                }
            }

        }
        catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method obtain the modification Type
     * 
     * @param inc
     * @param percent
     * @return
     */
    private EmPriceVariationType getEmPriceVariationType(STND inc, STND percent) {
        log.info("Inside getEmPriceVariationType method");
        String modificationType = "";
        String incrementType = "";
        if (business.increment.getValue().equalsIgnoreCase(inc.getValue())) {
            modificationType = Constants.MODIFICATION_INCREMENT;
        }
        else if (business.discount.getValue().equalsIgnoreCase(inc.getValue())) {
            modificationType = Constants.MODIFICATION_DECREMENT;
        }

        if (units.percentage.getValue().equalsIgnoreCase(percent.getValue())) {
            incrementType = Constants.UNIT_PERCENTAGE;
        }
        else {
            incrementType = Constants.UNIT_AMOUNT;
        }
        List<EmPriceVariationType> types = priceVariationTypeManager.getPriceVariationTypes();
        if (types != null && types.size() > 0) {
            for (EmPriceVariationType type : types) {
                if (type.getTcModificationType().equalsIgnoreCase(modificationType)
                        && type.getTcUnitType().equalsIgnoreCase(incrementType)) {
                    return type;
                }
            }
        }
        return null;
    }

}
