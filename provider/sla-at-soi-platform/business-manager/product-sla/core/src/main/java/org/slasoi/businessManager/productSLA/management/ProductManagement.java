/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/product-sla/core/src/main/java/org/slasoi/businessManager/productSLA/management/ProductManagement.java $
 */

package org.slasoi.businessManager.productSLA.management;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.businessManager.common.service.ProductManager;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.util.ProductState;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductManagement {
    private static final Logger LOGGER = Logger.getLogger(ProductManagement.class);
    
    private ProductManager productService;

    @Autowired
    /*
    // obtained automatically by Spring-DM and OSGi Container.
    public void setproductManager(ProductManager productService) {
        LOGGER.info("***************Registered Service productDAO Found*************");
        this.productService = productService;
    }
    */
    
    /**
     * This method  checks if the transition between states of a product is valid    
     * @param currentState
     * @param nextStage
     * @return
     */
     public int ckeckState(ProductState currentState, ProductState nextStage ){
           
            if(currentState==ProductState.DEFINED){
                if(nextStage!=ProductState.PROMOTED&&nextStage!=ProductState.ACTIVE){
                    LOGGER.error("Transition not allowed:"+currentState+"-->"+nextStage);
                    return -1;
                }            
            }else if(currentState==ProductState.PROMOTED){
                if(nextStage!=ProductState.ACTIVE&&nextStage!=ProductState.REJECTED){
                    LOGGER.error("Transition not allowed:"+currentState+"-->"+nextStage);
                    return -1;
                }            
            }else if(currentState==ProductState.ACTIVE){
                if(nextStage!=ProductState.INACTIVE){
                    LOGGER.error("Transition not allowed:"+currentState+"-->"+nextStage);
                    return -1;
                }
            }else if(currentState==ProductState.REJECTED){
                if(nextStage!=ProductState.DEFINED&&nextStage!=ProductState.INACTIVE){
                    LOGGER.error("Transition not allowed:"+currentState+"-->"+nextStage);
                    return -1;
                }  
            }else{ //case Inactive
                    LOGGER.error("Transition not allowed:"+currentState+"-->"+nextStage);
                    return -1; 
            }
            return 0;
        }
 /**
  * This method updates the state of a product.    
  * @param productId
  * @param nextStage
  * @return
  */
     public boolean changeProductState(long productId,ProductState nextStage){
         LOGGER.debug("Inside method: changeProductState()");
         LOGGER.debug("ProductId:"+productId);
         LOGGER.debug("Next State:"+nextStage.toString());
         EmSpProducts product = this.productService.getProductById(Long.valueOf(productId));
         String status ="";
         if(Constants.STATUS_ACTIVE.equalsIgnoreCase(product.getTxStatus())){
             status = Constants.STATUS_ACTIVE_NAME;
         }else if(Constants.STATUS_INACTIVE.equalsIgnoreCase(product.getTxStatus())){
             status = Constants.STATUS_INACTIVE_NAME;
         }else if(Constants.STATUS_PROMOTIONED.equalsIgnoreCase(product.getTxStatus())){
             status = Constants.STATUS_PROMOTIONED_NAME;
         }else if(Constants.STATUS_DEFINED.equalsIgnoreCase(product.getTxStatus())){
             status = Constants.STATUS_DEFINED_NAME;
         }else if(Constants.STATUS_REJECTED.equalsIgnoreCase(product.getTxStatus())){
             status = Constants.STATUS_REJECTED_NAME;
         }        
         
         if(ckeckState(ProductState.getStateFromString(status),nextStage)==0){
             LOGGER.debug("Valid transition: Uptdatign bbdd");
             product.setTxStatus(nextStage.toString());
             this.productService.saveOrUpdate(product);
             LOGGER.debug("Product state updated to:"+nextStage.toString());
           return true;  
         }else{
             LOGGER.error("Not possible to uptate the product state");
             return false;
         }
    }

}
