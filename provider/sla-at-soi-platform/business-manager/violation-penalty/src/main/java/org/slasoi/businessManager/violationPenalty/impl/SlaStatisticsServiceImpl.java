/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/violation-penalty/src/main/java/org/slasoi/businessManager/violationPenalty/db/services/SLAStatisticsDBManager.java $
 */

/**
 * 
 */
package org.slasoi.businessManager.violationPenalty.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.bslamanager.main.context.BusinessContextService;
import org.slasoi.businessManager.common.model.pac.EmPenalty;
import org.slasoi.businessManager.common.model.pac.EmSlaViolation;
import org.slasoi.businessManager.common.service.PenaltyManager;
import org.slasoi.businessManager.common.service.SlaViolationManager;
import org.slasoi.businessManager.common.ws.types.SlaStatistics;
import org.slasoi.businessManager.violationPenalty.SlaStatisticsService;
import org.slasoi.gslam.core.context.SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.negotiation.SLARegistry.InvalidUUIDException;
import org.slasoi.gslam.core.negotiation.SLARegistry.SLAState;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.SLA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
@Service(value = "slaStatisticsService")
public class SlaStatisticsServiceImpl implements SlaStatisticsService{
  
    private static Logger logger = Logger.getLogger(SlaStatisticsServiceImpl.class.getName());
  
    @Autowired
    private BusinessContextService businessContextService;
    
    @Autowired
    private SlaViolationManager slaViolationService;

    @Autowired
    private PenaltyManager penaltyService;
    
    public SlaStatistics getSLAStatistics(String slatId) {

        // First, retrieve the SLAs with this slatId from sla registry
        SLA[] slas = null;
        try {
            slas = businessContextService.getBusinessContext()
                .getSLARegistry().getIQuery().getSLAsByTemplateId(new UUID(slatId)); // NEW
        }
        catch (InvalidUUIDException e1) {
            logger.error(getClass().getSimpleName() +"# getSLAStatistics# InvalidUUIDException \n\t"+e1);
        }
        catch (SLAManagerContextException e1) {
            logger.error(getClass().getSimpleName() +"# getSLAStatistics# SLAManagerContextException \n\t"+e1);
        }

        int numberSLAsOK = 0;
        int numberSLAsViolated = 0;
        int numberSLAsViolatedNoPenalty = 0;
        int numberSLAsViolatedWithPenalty = 0;

        for (SLA sla : slas) {
            // get the current status of the SLA
            try {
                UUID slaId = sla.getUuid();
                
                SLAState state = businessContextService.getBusinessContext()
                    .getSLARegistry().getIQuery().getStateHistory(slaId, true)[0].state;
                
                if (state.equals(SLAState.OBSERVED) || state.equals(SLAState.WARN)){
                    numberSLAsOK = numberSLAsOK + 1;
                }
                else if (state.equals(SLAState.VIOLATED)){
                    numberSLAsViolated = numberSLAsViolated + 1;
                }

                /*
                 * query for finding if the sla has a penalty attached => check if there is at least one penalty for
                 * this SLA select * from pac_penalty p where p.violation.slaId = sla.getUuid();
                 */                
   
                if (penaltyService.getPenaltiesByViolationSlaId(slaId.toString()).size() > 0){
                    numberSLAsViolatedWithPenalty = numberSLAsViolatedWithPenalty + 1;
                }
            }
            catch (Exception e) {
                // Possible exceptions: InvalidUUIDException, InvalidStateException, SLAManagerContextException
                e.printStackTrace();
            }

        }
        numberSLAsViolatedNoPenalty = numberSLAsViolated - numberSLAsViolatedWithPenalty;
        SlaStatistics stat =
                new SlaStatistics(slas.length, numberSLAsOK, numberSLAsViolatedNoPenalty, numberSLAsViolatedWithPenalty);
        return stat;
    }

    public LinkedHashMap<Date, SlaStatistics> getDailySLAStatistics(String slatId, Date startDate, Date endDate) {
        
        LinkedHashMap<Date, SlaStatistics> list = new LinkedHashMap<Date, SlaStatistics>();

        // Create the structure
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        Date auxDate = c.getTime();
        while (auxDate.compareTo(endDate) <= 0) {
            list.put(auxDate, new SlaStatistics(0, 0, 0, 0));
            c.add(Calendar.DAY_OF_MONTH, 1);
            auxDate = c.getTime();
        }

        // First, retrieve the SLAs with this slatId from sla registry
        SLA[] slas = null;
        List<String> slaIds = null;

        try {
            logger.info("Getting SLAs for template " + slatId);
            slas = businessContextService.getBusinessContext()
                .getSLARegistry().getIQuery().getSLAsByTemplateId(new UUID(slatId));

            if (slas == null) {
                logger.info("No SLAs found for template " + slatId);
                return null;
            }
                
                slaIds= new ArrayList<String>(slas.length);
                
                for (SLA sla : slas) {
                    // get the current status of the SLA
                    UUID slaId = sla.getUuid();
                    slaIds.add(slaId.toString());

                    Date effectiveFrom = sla.getEffectiveFrom().getValue().getTime();
                    Date effectiveTo = sla.getEffectiveUntil().getValue().getTime();
                    logger.info("Got sla " + slaId.toString());
                    logger.info("effective from = " + effectiveFrom.toString());
                    logger.info("effective to = " + effectiveTo.toString());

                    // First, fill SLAStatistic with total number of SLAs per day
                    if (effectiveTo.compareTo(startDate) >= 0 && effectiveFrom.compareTo(endDate) <= 0) {
                        logger.info("This sla was active during the requested dates");
                        Date initIterDate = startDate;
                        Date endIterDate = endDate;

                        if (effectiveFrom.after(startDate)){
                            initIterDate = effectiveFrom;
                        }
                        
                        if (effectiveTo.before(endDate)){
                            endIterDate = effectiveTo;
                        }
                        c.setTime(initIterDate);
                        auxDate = c.getTime();
                        while (auxDate.compareTo(endIterDate) <= 0) {
                            list.get(auxDate).incrNumberSLAs();
                            c.add(Calendar.DAY_OF_MONTH, 1); // number of days to add
                            auxDate = c.getTime();
                        }
                    }
                } // loop in SLAs

            // Find penalties for the SLAs
            // First, fill SLAStatistic with total number of SLAs per day
            c.setTime(startDate);
            auxDate = c.getTime();
            List<EmPenalty> penaltyList = null;
            while (auxDate.compareTo(endDate) <= 0) {
                
                penaltyList = penaltyService.getPenaltiesByDateAndViolationSlaIdInList(auxDate, slaIds);
                
                int penalties = penaltyList.size();
                
                if (penalties>0) {                   
                    logger.info("Found " + penalties + " penalties");
                    
                    List<String> visitedSLAs = new ArrayList<String>();
                    
                    for (EmPenalty penalty: penaltyList) {                     
                        String slaId = penalty.getViolation().getSlaId();
                        
                        if (!visitedSLAs.contains(slaId)) {
                            // Check if we have already count a penalty for this sla and day
                            visitedSLAs.add(slaId);
                            // list.get(penaltyDate).incrNumberSLAsViolatedWithPenalty();
                        }
                    }
                    list.get(auxDate).setNumberSLAsViolatedWithPenalty(visitedSLAs.size());
                }

                // get SLAViolations active in auxDate
               
                List<EmSlaViolation> violationsList = slaViolationService.getViolationsBySlaIdInListAndTime(slaIds,auxDate);
                
                int violations = violationsList.size();
                if (violations>0) {
                    
                    logger.info("Found " + violations + " violations");
                    
                    List<String> visitedSLAs = new ArrayList<String>();
                    
                    for (EmSlaViolation violation:violationsList) {
                        Date violationDate = violation.getInitialTime();
                        String slaId = violation.getSlaId();

                        if (!visitedSLAs.contains(slaId)) {
                            // Check if we have already count a penalty for this sla and day
                            visitedSLAs.add(slaId);
                            list.get(violationDate).incrNumberSLAsViolatedWithPenalty();
                        }
                    }
                    SlaStatistics stat = list.get(auxDate);
                    if (stat != null) {
                        stat.setNumberSLAsViolatedNoPenalty(violations - stat.getNumberSLAsViolatedWithPenalty());
                        stat.setNumberSLAsOK(stat.getNumberSLAs() - violations);
                    }

                }

                c.add(Calendar.DAY_OF_MONTH, 1); // number of days to add
                auxDate = c.getTime();
            }

        }
        catch (InvalidUUIDException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return null;
        }
        catch (SLAManagerContextException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return null;
        }

        return list;
    }
}
