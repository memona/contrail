/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 669 $
 * @lastrevision   $Date: 2011-02-10 12:23:38 +0100 (Thu, 10 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/customer/src/main/java/org/slasoi/businessManager/customer/impl/PartyWS.java $
 */

package org.slasoi.businessManager.customer.impl;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.util.Constants;
import org.slasoi.businessManager.common.util.FunctionsCommons;
import org.slasoi.businessManager.common.model.EmCountries;
import org.slasoi.businessManager.common.model.EmCurrencies;
import org.slasoi.businessManager.common.model.EmIndividual;
import org.slasoi.businessManager.common.model.EmLanguages;
import org.slasoi.businessManager.common.model.EmOrganization;
import org.slasoi.businessManager.common.model.EmParty;
import org.slasoi.businessManager.common.model.EmPartyPartyrole;
import org.slasoi.businessManager.common.model.EmPartyPartyroleId;
import org.slasoi.businessManager.common.model.EmPartyRole;
import org.slasoi.businessManager.common.model.EmUsers;
import org.slasoi.businessManager.common.service.CountryService;
import org.slasoi.businessManager.common.service.CurrencyService;
import org.slasoi.businessManager.common.service.LanguageService;
import org.slasoi.businessManager.common.service.PartyManager;
import org.slasoi.businessManager.common.service.PartyPartyRoleManager;
import org.slasoi.businessManager.common.service.PartyRoleManager;
import org.slasoi.businessManager.common.service.UserService;
import org.slasoi.businessManager.customer.PartyWSIf;
import org.slasoi.businessManager.common.ws.types.AuthenticateUserResponseType;
import org.slasoi.businessManager.common.ws.types.CreatePartyResponseType;
import org.slasoi.businessManager.common.ws.types.GetParameterListResponseType;
import org.slasoi.businessManager.common.ws.types.IndividualType;
import org.slasoi.businessManager.common.ws.types.ParameterDataType;
import org.slasoi.businessManager.common.ws.types.PartyType;
import org.slasoi.businessManager.common.ws.types.UserType;
import org.springframework.osgi.extensions.annotation.ServiceReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service (value="partyAccess")
public class PartyWS implements PartyWSIf {

        Logger log = Logger.getLogger(PartyWS.class);
        private PartyManager partyService;
        private PartyRoleManager partyRoleService;
        private PartyPartyRoleManager partyPartyRoleManager;
        private UserService userService;
        private CurrencyService currencyService;
        private CountryService countryService;  
        private LanguageService languageService;  
        
        @ServiceReference
        // obtained automatically by Spring-DM and OSGi Container.
        public void setPartyService(PartyManager partyService) {
            log.info("***************Registered Service partyService Found*************");
            this.partyService = partyService;
        }
        @ServiceReference
        // obtained automatically by Spring-DM and OSGi Container.
        public void setPartyRoleService(PartyRoleManager partyRoleService) {
            log.info("***************Registered Service partyRoleService Found*************");
            this.partyRoleService = partyRoleService;
        }
        
        @ServiceReference
        // obtained automatically by Spring-DM and OSGi Container.
        public void setPartyPartyRoleService(PartyPartyRoleManager partyRoleService) {
            log.info("***************Registered Service partyPartyRoleManager Found*************");
            this.partyPartyRoleManager = partyRoleService;
        }        
        @ServiceReference
        // obtained automatically by Spring-DM and OSGi Container.
        public void setUserService(UserService userService) {
            log.info("***************Registered Service userService Found*************");
            this.userService = userService;
        }
        @ServiceReference
        // obtained automatically by Spring-DM and OSGi Container.
        public void setCurrencyService(CurrencyService currencyService) {
            log.info("***************Registered Service currencyService Found*************");
            this.currencyService = currencyService;
        }
        @ServiceReference
        // obtained automatically by Spring-DM and OSGi Container.
        public void setCountryService(CountryService countryService) {
            log.info("***************Registered Service countryService Found*************");
            this.countryService = countryService;
        }
        @ServiceReference
        // obtained automatically by Spring-DM and OSGi Container.
        public void setLanguageService(LanguageService languageService) {
            log.info("***************Registered Service languageService Found*************");
            this.languageService = languageService;
        }
        

        /**
         * Save or update party.
         * 
         * @param type the type
         * @param partyOri the party ori
         * @param party the party
         * @param typeUser the type user
         * 
         * @throws Exception the exception
         */
        @Transactional(rollbackFor=Exception.class,propagation=Propagation.REQUIRED)
        public void saveOrUpdateParty(String type, PartyType partyOri, EmParty party, Long rolId){
            
            log.info("saveOrUpdatePartyCustomer()");
            //Save Party
            partyService.saveOrUpdate(party);
            //Save PartyPartyRole
            EmPartyPartyrole emPartyPartyRole = new EmPartyPartyrole();
            EmPartyRole emPartyRole = new EmPartyRole();
            emPartyRole.setNuPartyroleid(rolId);
            
            EmPartyPartyroleId emPartyPartyroleId = new EmPartyPartyroleId(party.getNuPartyid(), emPartyRole.getNuPartyroleid());
            emPartyPartyRole.setId(emPartyPartyroleId);
            emPartyPartyRole.setTxStatus(Constants.PARTY_STATE_PENDING);
            partyRoleService.saveOrUpdate(emPartyPartyRole);
            
            //Save Users
            EmUsers emUsers = null;
            //one individual must have a user as a maximum.
            if(type!=null && Constants.PARTY_INDIVIDUAL.equalsIgnoreCase(type)){
                if (partyOri.getUsers()!=null && partyOri.getUsers().length==1){
                    emUsers = new EmUsers();
                    emUsers.setTxUserLogin(partyOri.getUsers()[0].getUserLogin());
                    emUsers.setTxPasswd(partyOri.getUsers()[0].getPasswd());
                    
                    EmPartyRole emPartyRole2 = new EmPartyRole();
                    EmParty party2 = new EmParty();
                    emPartyRole2.setNuPartyroleid(emPartyPartyRole.getId().getNuPartyroleid());
                    party2.setNuPartyid(emPartyPartyRole.getId().getNuPartyid());
                    emPartyPartyRole.setEmParty(party2);
                    emPartyPartyRole.setEmPartyRole(emPartyRole2);
                    log.info("PartyRoleID: " + emPartyPartyRole.getId().getNuPartyroleid());
                    log.info("PartyID: " + emPartyPartyRole.getId().getNuPartyid());
                    log.info("PartyRoleID: " + emPartyPartyRole.getEmPartyRole().getNuPartyroleid());
                    log.info("PartyID: " + emPartyPartyRole.getEmParty().getNuPartyid());                 
                    log.info("IndividualId: " + party.getEmIndividual().getNuIndividualid());                 
                    emUsers.setEmPartyPartyrole(emPartyPartyRole);  
                    emUsers.setEmIndividual(party.getEmIndividual());
                    userService.saveOrUpdate(emUsers);
                }else{
                    log.error("[ERROR], solo puede haber un usuario por individual... ");
                }
                
            }else if(type!=null && Constants.PARTY_ORGANIZATION.equalsIgnoreCase(type)){
                List lIndividuals = new ArrayList<EmIndividual>(0);
                if(party.getEmOrganization().getEmIndividuals()!=null ){
                    for(EmIndividual emIndividual:party.getEmOrganization().getEmIndividuals()){
                        lIndividuals.add(emIndividual);
                    }
                }
                int cont = 0;
                for (UserType userType:partyOri.getUsers()){
                    emUsers = new EmUsers();
                    emUsers.setTxUserLogin(userType.getUserLogin());
                    emUsers.setTxPasswd(userType.getPasswd());
                    emUsers.setEmPartyPartyrole(emPartyPartyRole);
                    //mapping individual users in order.If there are more individual users that these are created without relation to user.
                    if (lIndividuals.size()>cont){
                        emUsers.setEmIndividual((EmIndividual)lIndividuals.get(cont));
                    }
                    userService.saveOrUpdate(emUsers);
                    cont++;
                }               
            }           
            

            
        }       
        
        
        
        
          
        /**
         * Save or update type.
         * 
         * @param type the type
         * @param party the party
         * 
         * @return the creates the party response type
         */
        
        public CreatePartyResponseType createParty(String type, PartyType party, Long rolId){
            log.info("Into createParty() method");
            CreatePartyResponseType responseType = new CreatePartyResponseType();
            StringBuffer sbResponseMessage = new StringBuffer(0);
            int statusCode = 0;
            try{
            	
                //BEGIN MODULE REPEAT PARTY
                Properties prop = getProperties();
                String repeatParty = getProperty(prop,"partyWS.repeateddata_insert");
                if(repeatParty !=null && "false".equals(repeatParty)){
                	log.info("Check if the user already exists in the DB, partyWS.repeateddata_insert=false");
                	int partyId = checkNameParty(party);
                	if(partyId!=-1){
                		log.info("The party already exist en the DB");
                		responseType = new CreatePartyResponseType();
                		responseType.setResponseCode(partyId);
                		responseType.setResponseMessage("The party already exist en the DB");
                	
                		//Find the party existing
                		EmParty emPartyFound = partyService.getPartyById(new Long(partyId));
                		
                		//Check that role customer exist in partypartyrole if not is create
                		EmPartyPartyrole partyrole = null;
                		Iterator itParties = emPartyFound.getEmPartyPartyroles().iterator();
                		while (itParties.hasNext()){
                			EmPartyPartyrole emPartyPartyrole = (EmPartyPartyrole)itParties.next();
                			if(emPartyPartyrole.getEmPartyRole().getNuPartyroleid()==3){
                				partyrole = emPartyPartyrole;
                				break;
                			}
                		}
                		if(partyrole==null){
                			
                			partyrole = new EmPartyPartyrole();
                			partyrole.setTxStatus("APPROVED");
                			EmPartyPartyroleId id = new EmPartyPartyroleId();
                			id.setNuPartyid(emPartyFound.getNuPartyid());
                			id.setNuPartyroleid(new Long(3));
                			partyrole.setId(id);
                			partyPartyRoleManager.saveOrUpdate(partyrole);
                		}
                		//Check insert users
                		if(party.getUsers()!=null){
                			for(UserType user:party.getUsers()){
                				//If user not exist
                				EmUsers userFound = userService.getUserByParty(emPartyFound.getNuPartyid(), user.getUserLogin());
                				//If not found create the user associated to party
                				if(userFound==null){
                    				EmUsers emUsers = new EmUsers();
                                    emUsers.setTxUserLogin(user.getUserLogin());
                                    emUsers.setTxPasswd(user.getPasswd());
                                    emUsers.setEmPartyPartyrole(partyrole);
                                    userService.saveOrUpdate(emUsers);
                                    log.debug("UserID: " +emUsers.getNuUserid());
                                    responseType.setResponseMessage("The party already exist en the DB. User created");
                				}else{
                					responseType.setResponseMessage("The party already exist en the DB. User already exist");
                				}
                			}
                		}
                		return responseType;
                	}
                }       
                //END MODULE REPEAT PARTY
                log.info("saveOrUpdateType()"); 
                //Validates
                if(!FunctionsCommons.validateString(type)){
                    sbResponseMessage.append("\nType can`t be empty");
                    statusCode = 1;
                }               
                if(!FunctionsCommons.validateLong(party.getCurrencyId())){
                    sbResponseMessage.append("\nCurrency can`t be empty");
                    statusCode = 1;
                }
        
                //Variables
                EmParty emParty = new EmParty();
                EmCurrencies emCurrencies = null;
                EmIndividual emIndividual = null;
                EmLanguages emLanguages = null;
                EmCountries emCountries = null;
                EmOrganization emOrganization = null;
                EmUsers emUsers = null;
                
                
                //Implementation
                
                //Set party
    
                emCurrencies = new EmCurrencies();
                emCurrencies.setNuCurrencyId(party.getCurrencyId());
                emParty.setEmCurrencies(emCurrencies);
                emParty.setTxPartyIdentityType(type);
                emParty.setNuBalance(new BigDecimal(0));
                emParty.setNuCreditLimit(new BigDecimal(0));
                //Set individual or Organization 
                if(type!=null && Constants.PARTY_INDIVIDUAL.equalsIgnoreCase(type)){
                    emIndividual = new EmIndividual();
                    emLanguages = new EmLanguages();
                    emCountries = new EmCountries();
                    if (party.getUsers()==null || party.getUsers().length==0 || party.getUsers().length>1){
                        sbResponseMessage.append("\nFor a individual party must have a user");
                        statusCode = 1; 
                    }else{
                        if(!FunctionsCommons.validateString(party.getUsers()[0].getUserLogin())|| !FunctionsCommons.validateString(party.getUsers()[0].getPasswd())){
                            sbResponseMessage.append("\nIn type individual the user is mandatory fill the fields login and password");
                            statusCode = 1;                     
                        }
                    }
                    if(party.getIndividual()!=null){
                        //Validate country and language
                        if(!FunctionsCommons.validateLong(party.getIndividual().getCountryId()) || !FunctionsCommons.validateLong(party.getIndividual().getLanguageId())){
                            sbResponseMessage.append("\nIn type individual is mandatory fill the fields country and language");
                            statusCode = 1;
                        }
                        if(!FunctionsCommons.validateString(party.getIndividual().getEmail())|| !FunctionsCommons.validateString(party.getIndividual().getFirstName())){
                            sbResponseMessage.append("\nIn type individual is mandatory fill the fields mail and first name");
                            statusCode = 1;                     
                        }
                        
                        //Set country
                        emCountries.setNuCountry(party.getIndividual().getCountryId());
                        //Set language              
                        emLanguages.setNuLanguage(party.getIndividual().getLanguageId());
                        //Set individual
                        emIndividual.setTxAddress(party.getIndividual().getAddress());
                        emIndividual.setTxEmail(party.getIndividual().getEmail());
                        emIndividual.setTxFax(party.getIndividual().getFax());
                        emIndividual.setTxFirstName(party.getIndividual().getFirstName());
                        emIndividual.setTxJobdepartment(party.getIndividual().getJobdepartment());
                        emIndividual.setTxJobtitle(party.getIndividual().getJobtitle());
                        emIndividual.setTxLastName(party.getIndividual().getLastName());
                        emIndividual.setTxPhoneNumber(party.getIndividual().getPhoneNumber());
                        emIndividual.setDtRegistration(new Date());
                        emIndividual.setEmCountries(emCountries);
                        emIndividual.setEmLanguages(emLanguages);
                        emParty.setEmIndividual(emIndividual);
//                        aggiunta per modifica partyId ...
//
//                        
                        emParty.setNuPartyid(party.getPartyId());
                    }else{
                        sbResponseMessage.append("\nIn type individual is mandatory fill the object individual");
                        statusCode = 1;
                    }
                }else if(type!=null && Constants.PARTY_ORGANIZATION.equalsIgnoreCase(type)){
                    if(!FunctionsCommons.validateString(party.getOrganization().getTradingName())){
                        sbResponseMessage.append("\nIn type Organization is mandatory fill the field trading name");
                        statusCode = 1;                     
                    }
                    if(!FunctionsCommons.validateLong(party.getOrganization().getCountryId())){
                        sbResponseMessage.append("\nIn type Organization is mandatory fill the field CountryId");
                        statusCode = 1;                     
                    }
                    log.info("Paso por aqui");
                    emOrganization = new EmOrganization();
                    emOrganization.setTxDescription(party.getOrganization().getDescription());
                    emOrganization.setTxTradingname(party.getOrganization().getTradingName());
                    emOrganization.setTxFiscalid(party.getOrganization().getFiscalId());
                    EmCountries emCountriesOrg = new EmCountries();
                    emCountriesOrg.setNuCountry(party.getOrganization().getCountryId());
                    log.info("Insert CountryID:"+party.getOrganization().getCountryId());
                    emOrganization.setEmCountries(emCountriesOrg);
                    List l = new ArrayList<EmIndividual>();
                    java.util.Set setIndividuals = new HashSet<EmIndividual>();
                    if(party.getOrganization().getIndividuals()!=null ){
                        for(IndividualType individualType:party.getOrganization().getIndividuals() ){
                            emIndividual = new EmIndividual();
                            emLanguages = new EmLanguages();
                            emCountries = new EmCountries();
                            //Validate country and language
                            if(!FunctionsCommons.validateLong(individualType.getCountryId()) || !FunctionsCommons.validateLong(individualType.getLanguageId())){
                                sbResponseMessage.append("\nIn type individual is mandatory fill the fields country and language");
                                statusCode = 1;
                            }
                            if(!FunctionsCommons.validateString(individualType.getEmail())|| !FunctionsCommons.validateString(individualType.getFirstName())){
                                sbResponseMessage.append("\nIn type individual is mandatory fill the fields mail and first name");
                                statusCode = 1;                     
                            }
                            //Set country
                            emCountries.setNuCountry(individualType.getCountryId());
                            //Set language              
                            emLanguages.setNuLanguage(individualType.getLanguageId());
                            //Set individual
                            emIndividual.setTxAddress(individualType.getAddress());
                            emIndividual.setTxEmail(individualType.getEmail());
                            emIndividual.setTxFax(individualType.getFax());
                            emIndividual.setTxFirstName(individualType.getFirstName());
                            emIndividual.setTxJobdepartment(individualType.getJobdepartment());
                            emIndividual.setTxJobtitle(individualType.getJobtitle());
                            emIndividual.setTxLastName(individualType.getLastName());
                            emIndividual.setTxPhoneNumber(individualType.getPhoneNumber());
                            emIndividual.setDtRegistration(new Date());     
                            emIndividual.setEmCountries(emCountries);
                            emIndividual.setEmLanguages(emLanguages);
                            emIndividual.setEmOrganization(emOrganization);
                            //Set into Organization
                            setIndividuals.add(emIndividual);
                        }
                    }
                    emOrganization.setEmIndividuals(setIndividuals);
                    emParty.setEmOrganization(emOrganization);
                }else{
                    sbResponseMessage.append("\nType must be Individual or Organization");
                    statusCode = -1;
                }
            
                //SAVE THE PARTY
                if(statusCode==0){
                    this.saveOrUpdateParty(type,party,emParty,rolId);
                    sbResponseMessage.append("\nParty created successfully");
                    //In teh code return the id of the created party
                    log.info("Party Code:"+emParty.getNuPartyid());
                    responseType.setResponseCode(emParty.getNuPartyid().intValue());
                    responseType.setResponseMessage(sbResponseMessage.toString());                  
                }
                responseType.setResponseMessage(sbResponseMessage.toString());              
            }catch(Exception e){
                e.printStackTrace();
                sbResponseMessage.append("\nError creating Party!!!\n"+ e.getMessage());
                statusCode = -1;
                responseType.setResponseCode(statusCode);
                responseType.setResponseMessage(sbResponseMessage.toString());
                return responseType;
            }
            return responseType;
        }
           
        
        public CreatePartyResponseType createCustomer(String type, PartyType party){
        	return createParty(type, party, Constants.PARTY_ROLE_CUSTOMER);
        }

        public CreatePartyResponseType createProvider(String type, PartyType party){
        	return createParty(type, party, Constants.PARTY_ROLE_PROVIDER);
        }

           //Authenticate
            /**
         * Checks if is authenticated.
         * 
         * @param user the user
         * @param pass the pass
         * 
         * @return the list< em users>
         */
        @Transactional(propagation=Propagation.REQUIRED)
            public AuthenticateUserResponseType isAuthenticated(String user, String pass){
            log.info("Into isAuthenticated() method");
                AuthenticateUserResponseType responseType = new AuthenticateUserResponseType();
                List results=null;
                try{
                log.info("getParties()");
                results = userService.getUsersByLoginPass(user, pass);
                if(results!=null && results.size()>0){
                    log.info("User found");
                    responseType.setResponseCode(0);
                    responseType.setResponseMessage("Authentication performed successfully");
                    Iterator it = results.iterator();
                    while(it.hasNext()){
                        EmUsers pojo = (EmUsers)it.next();
                        log.debug("User: " + pojo.getTxUserLogin());
                        log.debug("Pass: " + pojo.getTxPasswd());
                    }
                }else{
                    responseType.setResponseCode(1);
                    responseType.setResponseMessage("Authentication failure. You are not registered in the system.");
                }               
                }catch(Exception e){
                    e.printStackTrace();
                }
                return responseType;
            }   
            
            //GetParameterlIST
              
            /**
             * Gets the parameter list.
             * 
             * @param getParameterType the get parameter type
             * 
             * @return the parameter list
             */
            @Transactional(propagation=Propagation.REQUIRED)
            public GetParameterListResponseType getParameterList(String getParameterType) {
                log.info("Into getParameterList() method");
                int statusCode = 0;
                StringBuffer sbResponse = new StringBuffer(0);
                ParameterDataType[] parameterDataTypes = null;
                GetParameterListResponseType responseType = new GetParameterListResponseType();
                List lPojos = null;
                try{
                    log.info("GetParameterListResponseType()");
                    if(getParameterType!=null && Constants.PARTY_WS_COUNTRY.equalsIgnoreCase(getParameterType)){
                        lPojos = countryService.getAllCountries();
                    }else if(getParameterType!=null && Constants.PARTY_WS_LANGUAGE.equalsIgnoreCase(getParameterType)){
                        lPojos = languageService.getAllLanguages();
                    }else if(getParameterType!=null && Constants.PARTY_WS_CURRENCY.equalsIgnoreCase(getParameterType)){
                        lPojos = currencyService.getAllCurrencies();
                    }else{
                        sbResponse.append("The received parameter is incorrect. Expected parameters: currency, language, country");
                        statusCode = 1;
                    }
                    
                    //Response correctly.
                    if(statusCode==0){
                        sbResponse.append("Data type ").append(getParameterType).append(" obtained correctly");
                        parameterDataTypes = fillDataType(lPojos);
                    }
                    
                    responseType.setParameterList(parameterDataTypes);
                    responseType.setResponseMessage(sbResponse.toString());
                    responseType.setResponseCode(statusCode);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return responseType;
            }               
           
        
            /**
             * Fill data type.
             * 
             * @param lPojos the l pojos
             * 
             * @return the parameter data type[]
             */
            private ParameterDataType[] fillDataType(List lPojos){
                log.info("Into fillDataType() method");
                ParameterDataType[] parameterDataTypes = new ParameterDataType[lPojos.size()];
                ParameterDataType parameterDataType = null;
                int cont = 0;
                try{
                    for(Object obj:lPojos){
                        parameterDataType = new ParameterDataType();
                        //Check the object type.
                        if(obj instanceof EmCountries){
                            EmCountries pojo = (EmCountries) obj;
                            parameterDataType.setParameterId(pojo.getNuCountry());
                            parameterDataType.setParameterName(pojo.getTxName());
                            log.debug(pojo.getTxName());
                        }else if(obj instanceof EmLanguages){
                            EmLanguages pojo = (EmLanguages) obj;
                            parameterDataType.setParameterId(pojo.getNuLanguage());
                            parameterDataType.setParameterName(pojo.getTxName());   
                            log.debug(pojo.getTxName());                           
                        }if(obj instanceof EmCurrencies){
                            EmCurrencies pojo = (EmCurrencies) obj;
                            parameterDataType.setParameterId(pojo.getNuCurrencyId());
                            parameterDataType.setParameterName(pojo.getTxName());   
                            log.debug(pojo.getTxName());                           
                        }
                        parameterDataTypes[cont] = parameterDataType;
                        cont++;
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return parameterDataTypes;
            }
            
            /**
             * Check name party.
             * 
             * @param party the party
             * 
             * @return the int
             */
            private int checkNameParty(PartyType party){
            	int partyId = -1;
            	try{
            		EmParty emParty = null; 
            		if(party.getIndividual()!=null){
            			//Find the party name in Individual
            			emParty = partyService.getByIndividualName(party.getIndividual().getFirstName(), 
            																	party.getIndividual().getLastName());     
                    }else{
                    	//Find the party name in Organization
                    	emParty = partyService.getByOrganizationName(party.getOrganization().getTradingName());
                    }
            		if (emParty!=null){
            			partyId = emParty.getNuPartyid().intValue();
            		}
            	}catch(Exception e){
            		e.printStackTrace();
            	}
            	return partyId;
            }
            /**
             * Gets the properties.
             * 
             * @return the properties
             */
            private Properties getProperties(){
            	 Properties prop = new Properties();
        		try{	    
        		    if(System.getenv( "SLASOI_HOME" )==null){
        		    	log.debug(">>>> Use local customerProfile.properties");
        		        prop.load(getClass().getResourceAsStream("/provider.properties"));
        		    }else{
        		    	log.debug(">>>> Use SLASOI_HOME customerProfile.properties");
        		        String propertyFile = System.getenv("SLASOI_HOME")+ System.getProperty( "file.separator" ) 
        		           + "BusinessManager"+System.getProperty( "file.separator" )+"provider.properties";
        			    FileInputStream inStream = new FileInputStream(new File( propertyFile));
        				prop.load(inStream);
        		    }
        		}catch (Exception e) {
        			e.printStackTrace();
        			log.debug("Error initializing Profile properties");
        		}   
        		return prop;
            }
            
            /**
             * Gets the property.
             * 
             * @param prop the prop
             * @param property the property
             * 
             * @return the property
             */
            private String getProperty(Properties prop, String property){
            	String propertyValue = null;
            	try{
            		propertyValue = prop.getProperty(property);
            	}catch(Exception e){
            		e.printStackTrace();
            	}
            	return propertyValue;
            }            
       
}
