/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/main/java/org/slasoi/businessManager/postSale/reporting/impl/PDFConversor.java $
 */

/**
 * 
 */
package org.slasoi.businessManager.postSale.reporting.impl;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.slaatsoi.business.schema.DurationType;
import org.slasoi.businessManager.billingEngine.util.BillingConstants;
import org.slasoi.businessManager.common.model.EmSpProductsOffer;
import org.slasoi.businessManager.postSale.reporting.exception.ImageException;
import org.slasoi.businessManager.postSale.reporting.impl.mapping.Group;
import org.slasoi.businessManager.postSale.reporting.impl.mapping.Report;
import org.slasoi.businessManager.postSale.reporting.impl.types.ParamNames;
import org.slasoi.businessManager.postSale.reporting.impl.types.ParametersList;
import org.slasoi.businessManager.postSale.reporting.impl.types.ReportType;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Class that generates reports in pdf format
 * 
 * @author Beatriz Fuentes (TID)
 * 
 */

public final class PDFConversor {
    private static Logger logger = Logger.getLogger(PDFConversor.class);

    private final static String TITLE_REPORT = "Detailed customer report";
    private final static String AUTHOR = "SLA@SOI framework";
    private final static String DATE_TEXT = "Date: ";
    private final static String PRODUCT_OFFER_DATA = "Product-Offer data:";
    private final static String TABLE = "table";
    private final static String HEADER2 = "header2";

    private final static String BLANK = " ";
    private final static String EMPTY_CELL = "-";
    private final static String NEWLINE = "\n";
    private final static String TAB = "     ";
    private final static String HALFTAB = "  ";

    private final static String LOGO_FILE = "logo.jpg";

    private static final Phrase EMPTY_CELL_PHRASE = new Phrase(EMPTY_CELL, FontType.CELL.getFont());

    public static void toPDFFile(Report report, ParametersList params, String filename) throws DocumentException, IOException, ImageException {

        logger.info("PDFConversor# toPDFFile# Creating document with title " + report.getTitle());
        logger.info("Author = " + report.getAuthor());

        Document document = DocumentFactory.newDocument(filename, report.getTitle(), report.getAuthor());
        document.add(createLogoParagraph(LOGO_FILE));
        document.add(createHeaderParagraph(report.getTitle()));
        document.add(createDateParagraph(params));

        switch (report.getType()) {
        case BILLING_REPORT: {
            document.add(createCustomerInfoParagraph(params));
            document.add(createBillingInfoParagraph(params));
            document.add(createSlaInfoParagraph(params));
            // Add one paragraph per group        
            for (Group group: report.getGroups()){
                Paragraph paragraph = createParagraph(group, params,report.getType());
                if (paragraph != null){
                    document.add(paragraph);
                }
            }
            break;
        }
        default: {
            // Add one paragraph per group        
            for (Group group: report.getGroups()){
                Paragraph paragraph = createParagraph(group, params,report.getType());
                if (paragraph != null){
                    document.add(paragraph);
                }
            }
            break;
        }
        }



        document.close();

        logger.info("Report created at " + filename);
    }

    private static Paragraph createSlaInfoParagraph(ParametersList params){

        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setLeading(13f);

        paragraph.add(new Phrase("SLA Summary",FontType.HEADER2.getFont()));
        addEmptyLines(paragraph, 2);           

        //Phrase agreedAt = new Phrase();
        Phrase label = new Phrase(TAB+"Signed on"+TAB,FontType.HEADER3.getFont(10,Font.ITALIC,ColorType.PRICE.getColor()));
        Phrase date = new Phrase(params.get(ParamNames.AGREEMENT_DATE.name()).get(0),FontType.BILLING_INFO.getFont());        
        //agreedAt.add(label+TAB);
        //agreedAt.add(date);

        paragraph.add(label);
        paragraph.add(date);
        addEmptyLines(paragraph, 1);

        paragraph.add(new Phrase(TAB+"Support",FontType.HEADER3.getFont(9,Font.ITALIC,ColorType.PRICE.getColor())));        
        addEmptyLines(paragraph, 1);

        if (params.get(ParamNames.SUPPORT_PHONE_NUMBER.name())!=null){
            paragraph.add(new Phrase(TAB+TAB+"phone:"+TAB+ params.get(ParamNames.SUPPORT_PHONE_NUMBER.name()).get(0),FontType.BILLING_INFO.getFont()));    
        }        
        addEmptyLines(paragraph, 1);

        if (params.get(ParamNames.SUPPORT_DAY_WEEK.name())!=null){
            paragraph.add(new Phrase(TAB+TAB+"period:"+TAB+ params.get(ParamNames.SUPPORT_DAY_WEEK.name()).get(0),FontType.BILLING_INFO.getFont()));
            paragraph.add(new Chunk(TAB));
            paragraph.add(new Chunk("from: ",FontType.BILLING_INFO.getFont(9,Font.NORMAL,ColorType.PRICE.getColor())));
            paragraph.add(new Chunk(params.get(ParamNames.SUPPORT_INIT_TIME.name()).get(0),FontType.BILLING_INFO.getFont()));
            paragraph.add(new Chunk(HALFTAB));
            paragraph.add(new Chunk("to: ",FontType.BILLING_INFO.getFont(9,Font.NORMAL,ColorType.PRICE.getColor())));
            paragraph.add(new Chunk(params.get(ParamNames.SUPPORT_FINISH_TIME.name()).get(0),FontType.BILLING_INFO.getFont()));
        }        
        addEmptyLines(paragraph, 1);

        paragraph.add(new Phrase(TAB+"Monitoring",FontType.HEADER3.getFont(10,Font.ITALIC,ColorType.PRICE.getColor())));        
        addEmptyLines(paragraph, 1);

        int index = 0;
        String unit=null;
        String value=null;
        for (String param: params.get(ParamNames.MONITORING_PARAMETER_NAME.name())){
            paragraph.add(new Phrase(TAB+TAB+new Chunk(String.valueOf(index+1),FontType.HEADER3.getFont())+TAB+ param +
                    TAB+"unit:"+TAB+ params.get(ParamNames.MONITORING_PARAMETER_UNIT.name()).get(index),FontType.BILLING_INFO.getFont()));            
            //addEmptyLines(paragraph, 1);
            unit = params.get(ParamNames.MONITORING_DATA_COLLECTION_INTERVAL_UNIT.name()).get(index);
            value = params.get(ParamNames.MONITORING_DATA_COLLECTION_INTERVAL_VALUE.name()).get(index);

            paragraph.add(new Phrase(TAB+"coll. interval: ["+value+"/"+unit+"]" ,FontType.BILLING_INFO.getFont()));

            unit = params.get(ParamNames.MONITORING_AGGREGATION_INTERVAL_UNIT.name()).get(index);
            value = params.get(ParamNames.MONITORING_AGGREGATION_INTERVAL_VALUE.name()).get(index);

            paragraph.add(new Phrase(TAB+"aggr. interval: ["+value+"/"+unit+"]" ,FontType.BILLING_INFO.getFont()));

            addEmptyLines(paragraph, 1);            

            index++;
        }
        //addEmptyLines(paragraph, 1); 

        paragraph.add(new Phrase(TAB+"Support Procedures",FontType.HEADER3.getFont(10,Font.ITALIC,ColorType.PRICE.getColor())));        
        addEmptyLines(paragraph, 1);

        //Support Procedures
        paragraph.add(new Phrase(TAB+TAB+"Notif Media:"+TAB+ params.get(ParamNames.SUPPORT_PROCEDURES_NOTIF_MEDIA.name()).get(0),FontType.BILLING_INFO.getFont()));
        addEmptyLines(paragraph, 1);  
        paragraph.add(new Phrase(TAB+TAB+"Levels:",FontType.BILLING_INFO.getFont()));
        addEmptyLines(paragraph, 1);  

        index=0;
        value=null;
        for (String name: params.get(ParamNames.SUPPORT_PROCEDURES_SEVERITY_LEVEL_NAME.name())){
            paragraph.add(new Phrase(TAB+TAB+TAB+TAB+new Chunk(String.valueOf(index+1),FontType.HEADER3.getFont())+TAB+ name,FontType.BILLING_INFO.getFont()));
            paragraph.add(new Phrase(TAB+"Description:"+TAB+ params.get(ParamNames.SUPPORT_PROCEDURES_SEVERITY_LEVEL_DESCRIPTION.name()).get(index),FontType.BILLING_INFO.getFont()));

            addEmptyLines(paragraph, 1);  
            unit = params.get(ParamNames.SUPPORT_PROCEDURES_RESOLUTION_TIME_UNIT.name()).get(index);
            value = params.get(ParamNames.SUPPORT_PROCEDURES_RESOLUTION_TIME_VALUE.name()).get(index);

            paragraph.add(new Phrase(TAB+TAB+TAB+TAB+TAB+TAB+"Resolution Time: ["+value+"/"+unit+"]" ,FontType.BILLING_INFO.getFont()));

            unit = params.get(ParamNames.SUPPORT_PROCEDURES_RESPONSE_TIME_UNIT.name()).get(index);
            value = params.get(ParamNames.SUPPORT_PROCEDURES_RESPONSE_TIME_VALUE.name()).get(index);

            paragraph.add(new Phrase(TAB+"Response Time: ["+value+"/"+unit+"]" ,FontType.BILLING_INFO.getFont()));

            addEmptyLines(paragraph, 1);  

            index++;
        }        
        //addEmptyLines(paragraph, 1); 

        paragraph.add(new Phrase(TAB+"Reporting",FontType.HEADER3.getFont(10,Font.ITALIC,ColorType.PRICE.getColor())));        
        addEmptyLines(paragraph, 1);

        String starts=null;
        String ends=null;
        String periodicity = null;        
        index=0;        
        for (String receiverName: params.get(ParamNames.REPORT_RECEIVER_DELIVERYENDPOINT.name())){
            paragraph.add(new Phrase(TAB+TAB+new Chunk(String.valueOf(index+1),FontType.HEADER3.getFont())+TAB+ receiverName,FontType.BILLING_INFO.getFont()));
            paragraph.add(new Phrase(TAB+"Format:"+TAB+params.get(ParamNames.REPORT_RECEIVER_FORMATTYPE.name()).get(index),FontType.BILLING_INFO.getFont()));
            paragraph.add(new Phrase(TAB+"Method:"+TAB+params.get(ParamNames.REPORT_RECEIVER_DELIVERYMETHOD.name()).get(index),FontType.BILLING_INFO.getFont()));
            addEmptyLines(paragraph, 1);

            periodicity = params.get(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name()).get(index);
            starts = params.get(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name()).get(index);
            ends = params.get(ParamNames.REPORT_CREATIONSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name()).get(index);

            paragraph.add(new Phrase(TAB+TAB+TAB+"Creation Period:" +TAB+ periodicity,FontType.BILLING_INFO.getFont()));
            paragraph.add(new Chunk(TAB+"from: ",FontType.BILLING_INFO.getFont(9,Font.NORMAL,ColorType.PRICE.getColor())));
            paragraph.add(new Chunk(starts,FontType.BILLING_INFO.getFont()));
            paragraph.add(new Chunk(HALFTAB));
            paragraph.add(new Chunk("to: ",FontType.BILLING_INFO.getFont(9,Font.NORMAL,ColorType.PRICE.getColor())));
            paragraph.add(new Chunk(ends,FontType.BILLING_INFO.getFont()));

            addEmptyLines(paragraph, 1);      

            periodicity = params.get(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_PERIODICITY.name()).get(index);
            starts = params.get(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_STARTSON.name()).get(index);
            ends = params.get(ParamNames.REPORT_DELIVERYSCHEDULE_AUTOMATIC_PERIODIC_TIMEPERIOD_ENDS_ON_DATETIME.name()).get(index);

            paragraph.add(new Phrase(TAB+TAB+TAB+"Delivery Period:" +TAB+ periodicity,FontType.BILLING_INFO.getFont()));
            paragraph.add(new Chunk(TAB+"from: ",FontType.BILLING_INFO.getFont(9,Font.NORMAL,ColorType.PRICE.getColor())));
            paragraph.add(new Chunk(starts,FontType.BILLING_INFO.getFont()));
            paragraph.add(new Chunk(HALFTAB));
            paragraph.add(new Chunk("to: ",FontType.BILLING_INFO.getFont(9,Font.NORMAL,ColorType.PRICE.getColor())));
            paragraph.add(new Chunk(ends,FontType.BILLING_INFO.getFont()));

            addEmptyLines(paragraph, 1);

            index++;
        }

        return paragraph;
    }


    private static Paragraph createBillingInfoParagraph(ParametersList params){

        Paragraph paragraph = new Paragraph();
        paragraph.setLeading(12f);

        paragraph.setAlignment(Element.ALIGN_RIGHT);

        String currency = params.get(ParamNames.ACCOUNTEVENT_CURRENCY_NAME.name()).get(0);

        String from = params.get(ParamNames.ACCOUNTEVENT_INITIAL_DATE.name()).get(0);
        String until = params.get(ParamNames.ACCOUNTEVENT_FINAL_DATE.name()).get(0);

        paragraph.add(new Phrase(params.get(ParamNames.ACCOUNTEVENT_TOTAL_AMOUNT.name()).get(0)+BLANK+currency ,FontType.PRICE.getFont()));
        addEmptyLines(paragraph, 2);

        //paragraph.add(new Phrase("Billing period:",FontType.BILLING_INFO.getFont()));
        //paragraph.add(new Chunk(TAB));
        paragraph.add(new Phrase("Report Paramaters",FontType.BILLING_INFO.getFont(9,Font.NORMAL,ColorType.TITLE.getColor())));
        addEmptyLines(paragraph, 1);
        paragraph.add(new Chunk("from: ",FontType.BILLING_INFO.getFont(9,Font.NORMAL,ColorType.PRICE.getColor())));
        paragraph.add(new Chunk(from,FontType.BILLING_INFO.getFont()));
        paragraph.add(new Chunk(HALFTAB));
        paragraph.add(new Chunk("to: ",FontType.BILLING_INFO.getFont(9,Font.NORMAL,ColorType.PRICE.getColor())));
        paragraph.add(new Chunk(until,FontType.BILLING_INFO.getFont()));

        return paragraph;
    }

    private static Paragraph createCustomerInfoParagraph(ParametersList params){

        Paragraph customerInfo = new Paragraph();
        customerInfo.setLeading(12f);
        customerInfo.add(new Phrase("Customer Information",FontType.CONTACTINFO.getFont(8,Font.BOLD,ColorType.TITLE.getColor())));
        addEmptyLines(customerInfo, 1);
        customerInfo.add(new Phrase(params.get(ParamNames.CONTACTPOINTNAME.name()).get(0),FontType.CONTACTINFO.getFont()));
        addEmptyLines(customerInfo, 1);
        customerInfo.add(new Phrase(params.get(ParamNames.CONTACTPOINTADDRESS.name()).get(0),FontType.CONTACTINFO.getFont()));
        addEmptyLines(customerInfo, 1);
        customerInfo.add(new Phrase("phone: "+params.get(ParamNames.CONTACTPOINTPHONE.name()).get(0)+
                "  fax: "+params.get(ParamNames.CONTACTPOINTFAX.name()).get(0),FontType.CONTACTINFO.getFont()));
        addEmptyLines(customerInfo, 1);
        customerInfo.add(new Phrase(params.get(ParamNames.CONTACTPOINTEMAIL.name()).get(0),FontType.CONTACTINFO.getFont()));
        addEmptyLines(customerInfo, 1);

        String account = params.get(ParamNames.ACCOUNTEVENT_BANK_ACCOUNT_NUMBER.name()).get(0);
        customerInfo.add(new Phrase("Bank info: "+account ,FontType.BILLING_INFO.getFont()));

        if (!account.equals("Unknown")){
            addEmptyLines(customerInfo, 1);
            customerInfo.add(new Phrase("\t At: "+params.get(ParamNames.ACCOUNTEVENT_BANK_OFFICE_ADDRESS.name()).get(0) ,FontType.BILLING_INFO.getFont()));
        }

        customerInfo.setAlignment(Element.ALIGN_LEFT);

        return customerInfo;
    }

    private static Paragraph createLogoParagraph(String logoFile) throws ImageException {

        Chunk ck = null;
        ck = new Chunk(ImageFactory.newImage(logoFile), 0, -25);

        Paragraph headerLogo = new Paragraph(ck);
        headerLogo.setAlignment(Element.ALIGN_LEFT);

        return headerLogo;
    }

    private static Paragraph createHeaderParagraph(String tittle) throws DocumentException, IOException{

        Paragraph headerTitle = new Paragraph(tittle,FontType.TITLE.getFont());
        headerTitle.setAlignment(Element.ALIGN_RIGHT);
        addEmptyLines(headerTitle,1);

        return headerTitle;
    }

  private static Paragraph createDateParagraph(ParametersList params) throws DocumentException, IOException, ImageException{

      Format formatter = new SimpleDateFormat("EEEEEEEE, MMM dd yyyy", Locale.ENGLISH);

      Paragraph paragraph = new Paragraph(new Phrase("On: "+formatter.format( new Date()),FontType.HEADER_DATE.getFont()));
      paragraph.setAlignment(Paragraph.ALIGN_RIGHT);      

      return paragraph;
  }

  private static void addEmptyLines(Paragraph paragraph, int number) {
      for (int i = 0; i < number; i++) {
          paragraph.add( new Phrase(NEWLINE));
      }
  }

  //OLD 


   private static Paragraph createHeader2(Group group) {

       logger.info("Creating header2 for group " + group.getName());

       try {
          Map<String, String> paramsGroup = group.getParameters();

          Paragraph paragraph = new Paragraph(group.getName(), FontType.HEADER2.getFont());
          addEmptyLines(paragraph, 1);

          Phrase p = null;
          for (String key:paramsGroup.keySet()){
              p = new Phrase(paramsGroup.get(key), FontType.PARAGRAPH.getFont());
              paragraph.add(p);
          }    

          return paragraph;
      }
      catch (Exception e) {
          return null;
      }
  }

  private static Paragraph createParagraph(Group group, ParametersList params,ReportType type) {

      logger.info("Creating paragraph for group " + group.getName());

      Paragraph paragraph = null;
      PdfPTable table = null;

      try {
          if (group.getFormat().toLowerCase().equals(TABLE)) {
              table = createTable(group, params,type);

              if (table != null) {
                  paragraph =
                          new Paragraph(group.getName(),FontType.HEADER2.getFont());
                  paragraph.add(table);
              }
              else {
                  String messageNoInfo = group.getMessageNoInfo();
                  if (messageNoInfo != null) {
                      paragraph = new Paragraph(group.getName(),FontType.HEADER2.getFont());
                      addEmptyLines(paragraph, 1);
                      paragraph.add(new Chunk(messageNoInfo,FontType.PARAGRAPH.getFont()));
                  }
              }
          }
          else if (group.getFormat().toLowerCase().equals(HEADER2)) {
              paragraph = createHeader2(group);
          }

          if (paragraph != null){
              addEmptyLines(paragraph,1);
          }

          return paragraph;
      }
      catch (Exception e) {
          return null;
      }
  }

   private static PdfPTable createTable(Group group, ParametersList params,ReportType type) {

      logger.info("Creating table for group " + group.getName());

      PdfPTable table = null;
      Map<String, String> paramsGroup = null;

      try {
          paramsGroup = group.getParameters();

          table = new PdfPTable(paramsGroup.size());

          // First, add the headers
          table.getDefaultCell().setBackgroundColor(ColorType.TABLE_HEADER_BG.getColor());

          PdfPCell cell=null;
          for (String key:paramsGroup.keySet()){//Headers
              cell = new PdfPCell(new Phrase(key, FontType.TABLE_HEADER.getFont(9,Font.ITALIC,ColorType.TITLE.getColor())));
              switch (type){
              case BILLING_REPORT:{
                  cell.setBorder(Rectangle.BOTTOM);
                  break;
              }
              }
              cell.setHorizontalAlignment(Element.ALIGN_CENTER);
              table.addCell(cell);
          }

          // Now, add the values
          // Use the same iterator to guarantee the same ordering
          table.getDefaultCell().setBackgroundColor(ColorType.TABLE_CELL_BG.getColor());

          logger.info("Number of params defined for this group = " + paramsGroup.size());

          // Check number of rows
          Iterator<String> iter = paramsGroup.keySet().iterator();
          String aux = iter.next();
          List<String> listAux = params.get(paramsGroup.get(aux));
          if (listAux != null) {
              int rows = listAux.size();
              logger.info("Number of rows = " + rows);

              for (int index = 0; index < rows; index++) {
                  logger.info("Iteration " + index);

                  iter = paramsGroup.keySet().iterator();
                  while (iter.hasNext()) {
                      // String value = paramsGroup.get(iter.next());
                      // table.addCell(value);
                      String name = iter.next();
                      logger.info("Parameter " + name);
                      List<String> list = params.get(paramsGroup.get(name));

                      if (list != null && list.size() > index) {

                          String item = list.get(index);
                          logger.info("Item = " + item);

                          Phrase p = null;
                          String currency = params.get(ParamNames.ACCOUNTEVENT_CURRENCY_NAME.name()).get(0).toLowerCase();
                          if (item != null){

                              switch (type){
                              case BILLING_REPORT:{
                                  if (name.equals("Amount")) {                                                                            
                                      cell = new PdfPCell(new Phrase(item+BLANK+currency, FontType.TABLE_HEADER.getFont()));
                                  }else{
                                      cell = new PdfPCell(new Phrase(item, FontType.TABLE_HEADER.getFont()));
                                  }
                                  cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                                  cell.setBorder(Rectangle.NO_BORDER);    
                                  break;
                              }
                              default:{
                                  cell = new PdfPCell(new Phrase(item, FontType.TABLE_HEADER.getFont()));
                                  break;
                              }
                              }

                              table.addCell(cell);
                          }
                          else{
                              table.addCell(EMPTY_CELL_PHRASE);
                          }

                      }
                      else {
                          logger.info("ERROR: Parameter " + name + " not found in iteration " + index);
                          table.addCell(new Phrase(EMPTY_CELL));
                      }
                  }
              }
          }
          else {
              table = null;
          }

          return table;
      }
      catch (Exception e) {
          return null;
      }

  }

  static public void toPDFFile(String filename, EmSpProductsOffer productOffer) throws NullArgumentException, DocumentException, IOException, ImageException {

      Format formatter = new SimpleDateFormat("EEEEEEEE, MMM dd yyyy", Locale.ENGLISH);

      Document document = DocumentFactory.newDocument(filename, TITLE_REPORT, AUTHOR);
      document.add(createLogoParagraph(LOGO_FILE));
      document.add(createHeaderParagraph(TITLE_REPORT));      

      Paragraph paragraphDate = new Paragraph(new Phrase("On: "+formatter.format( new Date()),FontType.HEADER_DATE.getFont()));
      paragraphDate.setAlignment(Paragraph.ALIGN_RIGHT);      

      document.add(paragraphDate);

      if (productOffer == null){
          throw new NullArgumentException("productOffer");
      }

      document.add(createProductOfferParagraph(productOffer));
      document.close();
  }

  static private Paragraph createProductOfferParagraph(EmSpProductsOffer productOffer) throws NullArgumentException{

      if (productOffer == null){
          throw new NullArgumentException("productOffer");
      }

      Paragraph par =null;

      try {


          par = new Paragraph(PRODUCT_OFFER_DATA, new Font(
                          BaseFont.createFont(BaseFont.HELVETICA, "Cp1252", false), 10, Font.BOLD, new Color(0, 151,
                                  200)));
          Format formatter = new SimpleDateFormat("MMM-dd-yyyy", Locale.ENGLISH);

          PdfPTable table = new PdfPTable(6);
          table.getDefaultCell().setBackgroundColor(new Color(202, 200, 242));
          table.addCell("Name");
          table.addCell("Description");
          table.addCell("Revision");
          table.addCell("Status");
          table.addCell("Valid from");
          table.addCell("Valid to");
          table.getDefaultCell().setBackgroundColor(new Color(255, 255, 255));
          table.addCell(productOffer.getTxName());
          table.addCell(productOffer.getTxDescription());
          table.addCell(productOffer.getTxRevisionid());
          table.addCell(productOffer.getTxStatusName());
          table.addCell(formatter.format(productOffer.getDtValidFrom()));
          table.addCell(formatter.format(productOffer.getDtValidTo()));

          par.add(table);

      }
      catch (Exception e) {
          return null;
      }

      return par;
  }


  static public void generateEmptyReport(String filename) throws FileNotFoundException, DocumentException {

      Document document = DocumentFactory.newDocument(filename, TITLE_REPORT, AUTHOR);

      Phrase notAvailable = new Phrase("Report not available yet");
      document.add(notAvailable);
      document.close();
  }
}
