/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/main/java/org/slasoi/businessManager/postSale/reporting/impl/util/DateConverter.java $
 */

/**
 * 
 */
package org.slasoi.businessManager.postSale.reporting.impl.util;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * @author fuentes
 * 
 */
public class DateConverter {
    private static Logger logger = Logger.getLogger(DateConverter.class.getName());

    private static final int MONTH_SHIFT = 0; // Calendar.JANUARY = 0

    public static Date toDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }

    public static Date firstHourOfDay(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        // calendar.setLenient(false);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month + MONTH_SHIFT);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        int hour = calendar.getActualMinimum(Calendar.HOUR_OF_DAY);
        calendar.set(Calendar.HOUR_OF_DAY, hour);

        int minute = calendar.getActualMinimum(Calendar.MINUTE);
        calendar.set(Calendar.MINUTE, minute);

        int second = calendar.getActualMinimum(Calendar.SECOND);
        calendar.set(Calendar.SECOND, second);

        return calendar.getTime();
    }

    public static Date lastHourOfDay(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month + MONTH_SHIFT);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        int hour = calendar.getActualMaximum(Calendar.HOUR_OF_DAY);
        calendar.set(Calendar.HOUR_OF_DAY, hour);

        int minute = calendar.getActualMaximum(Calendar.MINUTE);
        calendar.set(Calendar.MINUTE, minute);

        int second = calendar.getActualMaximum(Calendar.SECOND);
        calendar.set(Calendar.SECOND, second);

        return calendar.getTime();
    }

    public static Date firstDayOfMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        // calendar.setLenient(false);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month + MONTH_SHIFT);

        int day = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        int hour = calendar.getActualMinimum(Calendar.HOUR_OF_DAY);
        calendar.set(Calendar.HOUR_OF_DAY, hour);

        int minute = calendar.getActualMinimum(Calendar.MINUTE);
        calendar.set(Calendar.MINUTE, minute);

        int second = calendar.getActualMinimum(Calendar.SECOND);
        calendar.set(Calendar.SECOND, second);

        return calendar.getTime();
    }

    public static Date lastDayOfMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month + MONTH_SHIFT);

        int day = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        int hour = calendar.getActualMaximum(Calendar.HOUR_OF_DAY);
        calendar.set(Calendar.HOUR_OF_DAY, hour);

        int minute = calendar.getActualMaximum(Calendar.MINUTE);
        calendar.set(Calendar.MINUTE, minute);

        int second = calendar.getActualMaximum(Calendar.SECOND);
        calendar.set(Calendar.SECOND, second);

        return calendar.getTime();
    }
}
