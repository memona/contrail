/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/postsale/src/main/java/org/slasoi/businessManager/postSale/reporting/impl/types/ParametersList.java $
 */

/**
 * 
 */
package org.slasoi.businessManager.postSale.reporting.impl.types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

/**
 * @author Beatriz Fuentes(TID)
 * 
 */
public class ParametersList extends HashMap<String, ArrayList<String>> {
    /**
     * 
     */
    private static final long serialVersionUID = -1278278204635696775L;
    private static final Logger logger = Logger.getLogger(ParametersList.class);

    public void put(String name, String value) {

        logger.info("Storing parameter: " + name + " " + value);
        ArrayList<String> list = this.get(name);
        if (list == null) {
            list = new ArrayList<String>();
            this.put(name, list);
        }
        list.add(value);

    }

    public int size(String key) {
        int size = 0;
        if (this.containsKey(key))
            size = this.get(key).size();
        return size;
    }

    public int rows() {
        int rows = 0;

        logger.info("Checking size...");

        Iterator<String> iter = this.keySet().iterator();
        String name = iter.next();
        rows = this.get(name).size();

        logger.info("Param " + name + " size = " + rows);
        return rows;
    }
    
}
