package org.slasoi.businessManager.postSale.reporting.impl;

import java.awt.Color;

public enum ColorType {

    TITLE(0,151,200),
    PRICE(22,106,151),
    HEADER2(0, 151, 200),
    PARAGRAPH(0, 0, 0),
    TABLE_HEADER(0, 0, 0),
    TABLE_HEADER_BG(203,233,255),
    TABLE_CELL(0, 0, 0),
    TABLE_CELL_BG(255, 255, 255);
    
    private Color color;
    
    private ColorType(int red,int green,int blue){
        color = new Color(red,green,blue);
    }
    
    public Color getColor(){
        return color;
    }
}
