package org.slasoi.businessManager.postSale.reporting;

import java.util.Date;

public interface IBillingReport {

    
    public String getBillingReportBySlaId(String businessSLAId, Date startDate,Date endDate) ;
        
    public String getBillingReportByPartyId(Long partyId, Date startDate,Date endDate);
    
    
    
}
