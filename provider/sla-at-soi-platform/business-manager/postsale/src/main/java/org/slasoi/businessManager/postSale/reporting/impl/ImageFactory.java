package org.slasoi.businessManager.postSale.reporting.impl;

import java.io.IOException;
import java.net.MalformedURLException;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.postSale.reporting.exception.ImageException;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Image;

public final class ImageFactory {

    private static Logger logger = Logger.getLogger(ImageFactory.class);
    
    private ImageFactory() {

    }

    public static Image newImage(String imageName) throws ImageException{

        Image image = null;

        try {
            image = Image.getInstance(PDFConversor.class.getClassLoader().getResource(imageName));
        }
        catch (BadElementException e) {
            logger.error("ImageFactory# newImage# BadElementException# imageName:= "+imageName,e);
            throw new ImageException("ImageFactory# newImage# BadElementException# imageName:= "+imageName,e);
        }
        catch (MalformedURLException e) {
            logger.error("ImageFactory# newImage# MalformedURLException# imageName:= "+imageName,e);
            throw new ImageException("ImageFactory# newImage# BadElementException# imageName:= "+imageName,e);
        }
        catch (IOException e) {
            logger.error("ImageFactory# newImage# IOException# imageName:= "+imageName,e);
            throw new ImageException("ImageFactory# newImage# BadElementException# imageName:= "+imageName,e);
        }
        
        image.scalePercent(50);
        image.setAlignment(Image.LEFT);
        
        return image;

    }

}
