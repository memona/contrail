package org.slasoi.businessManager.ws.interfaces;

import org.slasoi.businessManager.common.ws.types.AuthenticateUserResponseType;
import org.slasoi.businessManager.common.ws.types.CreatePartyResponseType;
import org.slasoi.businessManager.common.ws.types.GetParameterListResponseType;
import org.slasoi.businessManager.common.ws.types.PartyType;


public interface PartyIf {
    /**
     * 
     * @param userLogin
     * @param passwd
     * @return
     */
    public AuthenticateUserResponseType authenticateUser(String userLogin,String passwd);  
/**
 * 
 * @param type
 * @param party
 * @return
 */
     public CreatePartyResponseType createParty(String type, PartyType party);
/**
 * 
 * @param getParameterType
 * @return
 */
     public GetParameterListResponseType getParameterList(String getParameterType);

}
