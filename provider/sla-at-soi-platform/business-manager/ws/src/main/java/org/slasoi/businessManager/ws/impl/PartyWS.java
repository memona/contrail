package org.slasoi.businessManager.ws.impl;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slasoi.businessManager.customer.PartyWSIf;
import org.slasoi.businessManager.ws.interfaces.PartyIf;
import org.slasoi.businessManager.ws.internal.Activator;
import org.slasoi.businessManager.common.ws.types.AuthenticateUserResponseType;
import org.slasoi.businessManager.common.ws.types.CreatePartyResponseType;
import org.slasoi.businessManager.common.ws.types.GetParameterListResponseType;
import org.slasoi.businessManager.common.ws.types.PartyType;

public class PartyWS implements PartyIf{
    private static final Logger LOGGER = Logger.getLogger(PartyWS.class);
/**
 * 
 */
    public AuthenticateUserResponseType authenticateUser(String userLogin,String passwd){      
        try {
            LOGGER.debug("Inside authenticateUser method");
            PartyWSIf partyService = getServiceReference();
            AuthenticateUserResponseType response = partyService.isAuthenticated(userLogin, passwd);
            LOGGER.debug("Response Code:"+response.getResponseCode());
            LOGGER.debug("Response Message:"+response.getResponseMessage());
            return response;
        }
        catch (Exception ex) {
             LOGGER.debug("Error: "+ex.getMessage());
              AuthenticateUserResponseType response = new AuthenticateUserResponseType(-1, ex.getMessage());
              return response;
        }  
    }
   
/**
 * 
 */
    public CreatePartyResponseType createParty(String type, PartyType party){
        try {
            LOGGER.debug("Inside createParty method");
            PartyWSIf partyService = getServiceReference();
            CreatePartyResponseType response = partyService.createCustomer(type, party);
            LOGGER.debug("Response:"+response.getResponseCode()+" : "+response.getResponseMessage());
            return response;
        }
        catch (Exception ex) {
            LOGGER.debug("Error: "+ex.getMessage());
            CreatePartyResponseType response = new CreatePartyResponseType(-1, ex.getMessage());
              return response;
        }        
    }
/**
 * 
 */
    public GetParameterListResponseType getParameterList(String getParameterType){
        try {
            LOGGER.debug("Inside getParameterList method");
            PartyWSIf partyService = getServiceReference();
            return partyService.getParameterList(getParameterType);
        }catch (Exception ex) {
              LOGGER.debug("Error: "+ex.getMessage());
              GetParameterListResponseType response = new GetParameterListResponseType(-1, ex.getMessage());
              return response;
        }  
    }
    
    private PartyWSIf getServiceReference() throws Exception{
        PartyWSIf partyService = null;
        BundleContext bc = Activator.getContext();
        if (bc!=null){
           ServiceReference ref =  bc.getServiceReference(PartyWSIf.class.getName());
        if (ref!=null)
            partyService = (PartyWSIf) bc.getService(ref);
        }else{
            LOGGER.error("**********************Servicio NULL");
            throw new Exception("Service party not Found");
        }
        return partyService;
    }

}
