package org.slasoi.businessManager.ws.impl;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slasoi.businessManager.customer.PartyWSIf;
import org.slasoi.businessManager.productSLA.productDiscovery.ProductDiscovery;
import org.slasoi.businessManager.ws.internal.Activator;
import org.slasoi.businessManager.common.ws.types.AuthenticateUserResponseType;
import org.slasoi.businessManager.common.ws.types.CreatePartyResponseType;
import org.slasoi.businessManager.common.ws.types.GetParameterListResponseType;
import org.slasoi.businessManager.common.ws.types.PartyType;
import org.slasoi.businessManager.common.ws.types.Product;
import org.slasoi.businessManager.common.ws.types.ProductOffer;
import org.slasoi.businessManager.common.ws.types.Service;

public class ProviderWS {

	private static final Logger log = Logger.getLogger(ProviderWS.class);
	
    public AuthenticateUserResponseType authenticateUser(String userLogin,String passwd){      
        try {
            log.debug("Inside authenticateUser method");
            PartyWSIf partyService = getServiceParty();
            AuthenticateUserResponseType response = partyService.isAuthenticated(userLogin, passwd);
            log.debug("Response Code:"+response.getResponseCode());
            log.debug("Response Message:"+response.getResponseMessage());
            return response;
        }
        catch (Exception ex) {
             log.debug("Error: "+ex.getMessage());
              AuthenticateUserResponseType response = new AuthenticateUserResponseType(-1, ex.getMessage());
              return response;
        }  
    }
   
/**
 * 
 */
    public CreatePartyResponseType createParty(String type, PartyType party){
        try {
            log.debug("Inside createParty method");
            PartyWSIf partyService = getServiceParty();
            CreatePartyResponseType response = partyService.createCustomer(type, party);
            log.debug("Response:"+response.getResponseCode()+" : "+response.getResponseMessage());
            return response;
        }
        catch (Exception ex) {
            log.debug("Error: "+ex.getMessage());
            CreatePartyResponseType response = new CreatePartyResponseType(-1, ex.getMessage());
              return response;
        }        
    }
/**
 * 
 */
    public GetParameterListResponseType getParameterList(String getParameterType){
        try {
            log.debug("Inside getParameterList method");
            PartyWSIf partyService = getServiceParty();
            return partyService.getParameterList(getParameterType);
        }catch (Exception ex) {
              log.debug("Error: "+ex.getMessage());
              GetParameterListResponseType response = new GetParameterListResponseType(-1, ex.getMessage());
              return response;
        }  
    }
    	
    public Product[] getProducts(Long partyId, String filter){
        try{
            log.debug("Inside getProducts method");
            log.debug("partyID"+partyId);
            log.debug("filter : "+filter);
            ProductDiscovery discovery = getProductDiscovery();
            Product[] productsOffer = discovery.getWsPartyProducts(partyId, filter);
            return productsOffer;    
            
        }catch(Exception e){
            log.error("Error: "+e.toString());
            e.printStackTrace();
            return null;
        }    
    }
    public ProductOffer[] getProductOffers(Long partyId, String filter){
    	try{
	        log.debug("Inside getProductOffers method");
	        log.debug("partyID"+partyId);
	        log.debug("filter : "+filter);
	        ProductDiscovery discovery = getProductDiscovery();
	        ProductOffer[] productOffers = discovery.getWsPartyProductOffers(partyId, filter);
	        return productOffers;    
	        
	    }catch(Exception e){
	        log.error("Error: "+e.toString());
	        e.printStackTrace();
	        return null;
	    }    
    }
    public Service[] getServices(Long partyId, String filter){
    	try{
	        log.debug("Inside getServices method");
	        log.debug("partyID"+partyId);
	        log.debug("filter : "+filter);
	        ProductDiscovery discovery = getProductDiscovery();
	        Service[] services = discovery.getWsPartyServices(partyId, filter);
	        return services;    
	        
	    }catch(Exception e){
	        log.error("Error: "+e.toString());
	        e.printStackTrace();
	        return null;
	    }    
	}
    
    
    private ProductDiscovery getProductDiscovery() throws Exception{
        BundleContext bc= Activator.getContext();
        if (bc==null)
     	   throw new Exception("Bundle Context Null.");
        ServiceReference ref = bc.getServiceReference(ProductDiscovery.class.getName());
        if (ref==null)
     	   throw new Exception("No Service Product Discovery found.");
        
        return (ProductDiscovery) bc.getService(ref);
    }
    
    private PartyWSIf getServiceParty() throws Exception{
        PartyWSIf partyService = null;
        BundleContext bc = Activator.getContext();
        if (bc!=null){
           ServiceReference ref =  bc.getServiceReference(PartyWSIf.class.getName());
        if (ref!=null)
            partyService = (PartyWSIf) bc.getService(ref);
        }else{
            throw new Exception("Service party not Found");
        }
        return partyService;
    }    
    
	
}
