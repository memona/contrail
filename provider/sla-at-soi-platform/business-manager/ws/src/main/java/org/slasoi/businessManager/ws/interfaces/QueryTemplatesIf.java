package org.slasoi.businessManager.ws.interfaces;

import org.slasoi.businessManager.common.ws.types.Product;
import org.slasoi.slamodel.sla.SLATemplate;

public interface QueryTemplatesIf {
    /**
     * 
     * @return SLATemplate[] object parse into base64
     */
    public String getTemplates();

}
