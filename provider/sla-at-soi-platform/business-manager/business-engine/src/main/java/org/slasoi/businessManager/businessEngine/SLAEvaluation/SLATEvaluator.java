/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 827 $
 * @lastrevision   $Date: 2011-02-24 17:39:51 +0100 (Thu, 24 Feb 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/business-engine/src/main/java/org/slasoi/businessManager/businessEngine/SLAEvaluation/SLATEvaluator.java $
 */

package org.slasoi.businessManager.businessEngine.SLAEvaluation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.util.GTDifference;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.Expr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.ValueExpr;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.CustomAction;
import org.slasoi.slamodel.sla.Customisable;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.Party;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.sla.VariableDeclr;
import org.slasoi.slamodel.sla.Guaranteed.Action;
import org.slasoi.slamodel.sla.Guaranteed.State;
import org.slasoi.slamodel.sla.Guaranteed.Action.Defn;
import org.slasoi.slamodel.sla.business.Penalty;
import org.slasoi.slamodel.sla.business.ProductOfferingPrice;
import org.slasoi.slamodel.sla.business.Termination;
import org.slasoi.slamodel.sla.business.TerminationClause;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.meta;


public class SLATEvaluator {
    private static final Logger log = Logger.getLogger(SLATEvaluator.class); 
    private SLATemplate offeredTemplate;
    private SLATemplate originalTemplate;
    private SLA  finalSLA;
 
    public SLATEvaluator(SLATemplate offeredTemplate,SLATemplate originalTemplate,SLA finalSLA){
        this.offeredTemplate = offeredTemplate;
        this.originalTemplate = originalTemplate;
        this.finalSLA = finalSLA;
    }

    /**
     * This method compares the template given to the offer it come from.
     * @return
     */
  public  List<GTDifference> evaluateSLAT()throws Exception {
      log.info("Inside evaluateSLAT Methdod. TemplateID" + offeredTemplate.getUuid().getValue());
      try{
          SLATData dataOri = getSLATData(this.originalTemplate);          
          SLATData dataOffered = getSLATData(offeredTemplate);
          return compareAgreementTerms(dataOri.getAgreementsTerms(),dataOffered.getAgreementsTerms());      
      }catch (Exception e){
          log.error("Error: "+e.toString());
          e.printStackTrace();
          throw new Exception(e.getMessage());
      }
  }
    
  /**
  * This method compares the SLA given to the offer it come from.
  * @return
  */
  public  List<GTDifference> evaluateSLA() throws Exception{
      log.info("Inside evaluateSLA Methdod. SLAID: " + finalSLA.getUuid().getValue());
      try{
          SLATData dataOri = getSLATData(this.originalTemplate);          
          SLAData dataOffered = getSLAData(finalSLA);
          return compareAgreementTerms(dataOri.getAgreementsTerms(),dataOffered.getAgreementsTerms());      
      }catch (Exception e){
          log.error("Error: "+e.toString());
          e.printStackTrace();
          throw new Exception(e.getMessage());
      }
  }

  
    /**
     * Calculate the differences between to list of guarantee Terms.
     * @param dataOri
     * @param dataOffered
     * @return
     */
    public  List<GTDifference> compareAgreementTerms(List<AgreementTermData> dataOri, List<AgreementTermData> dataOffered){
        log.info("Inside compareAgreementTerms");
        List<GTDifference> differences = new ArrayList<GTDifference>();
        try{          
          //SLATData dataOri = getSLATData(this.originalTemplate);          
          //SLATData dataOffered = getSLATData(offeredTemplate);
          //compare guarantee terms
          //List<AgreementTermData> list= dataOffered.getAgreementsTerms();
          for(AgreementTermData term:dataOffered){
              String termID = term.getAgreementId();
              List<GuaranteeData> listGuarantee = term.getGuarantees();
              GuaranteeData guaranteeOri;
              for(GuaranteeData guarantee: listGuarantee){
               if(meta.$STATE.equalsIgnoreCase(guarantee.getType())){
                   //for the moment only compare guaranteeStates.
                if("Numerical".equalsIgnoreCase(guarantee.getGtParameterType())){
                  //only compare numerical values for the moment
                  String gtID = guarantee.getGtId();                  
                      log.info("Comparing GT: " + gtID);
                      guaranteeOri = searchGuarantee(dataOri,termID, gtID);
                      if(guaranteeOri!=null){
                          //compare the two guarantee
                          log.info("GuaranteeFinded-->Comparing");
                          if(guaranteeOri.getGtServiceName().equals(guarantee.getGtServiceName())){
                              if(guaranteeOri.getGtComparisonOperator().equals(guarantee.getGtComparisonOperator())){
                                  if(guaranteeOri.getGtComparisonOperator().equals(guarantee.getGtComparisonOperator())){
                                      if(guaranteeOri.getGtParameterValue()==guarantee.getGtParameterValue()){
                                          log.info("The values are equal");                                      
                                      }else{
                                          log.info("The value are different");
                                          log.info("Value original:"+guaranteeOri.getGtParameterValue());
                                          log.info("New Value:"+guarantee.getGtParameterValue());
                                          log.info("Comparator:"+guaranteeOri.getGtComparisonOperator());
                                          GTDifference difference = new GTDifference();
                                          difference.setGuaranteeId(guarantee.getGtId());
                                          difference.setOperationComparator(guarantee.getGtComparisonOperator());
                                          difference.setServiceName(guarantee.getGtServiceName().trim());
                                          difference.setParameter(guarantee.getGtParameterName());
                                           difference.setPercentage((java.lang.Math.abs(guaranteeOri.getGtParameterValue()
                                                 -guarantee.getGtParameterValue())/guaranteeOri.getGtParameterValue())*100);
                                           //get the different and check its increment or decrement taking into
                                           //account the comparison operator.
                                           if(guaranteeOri.getGtParameterValue()>guarantee.getGtParameterValue()){                                          
                                               if(guaranteeOri.getGtComparisonOperator().equalsIgnoreCase(core.$greater_than)||
                                                  guaranteeOri.getGtComparisonOperator().equalsIgnoreCase(core.$greater_than_or_equals)||
                                                  guaranteeOri.getGtComparisonOperator().equalsIgnoreCase(core.$equals)){
                                                   log.info("Less restricted-->Decrement");
                                                   difference.setModificationType("Decrement");
                                               }else{
                                                   log.info("More restricted-->Increment");
                                                   difference.setModificationType("Increment");
                                               }
                                           }else{
                                               if(guaranteeOri.getGtComparisonOperator().equalsIgnoreCase(core.$less_than)||
                                                  guaranteeOri.getGtComparisonOperator().equalsIgnoreCase(core.$less_than_or_equals)||
                                                  guaranteeOri.getGtComparisonOperator().equalsIgnoreCase(core.$equals)){
                                                   log.info("More restricted-->Increment");
                                                   difference.setModificationType("Increment");
                                               }else{
                                                   log.info("Less restricted-->Decrement");
                                                   difference.setModificationType("Decrement");
                                               }
                                           }
                                           //add to the list containing the different.
                                           differences.add(difference);                                      
                                      }
                                  
                                  }else{
                                      log.error("The unit is different");
                                  }
                              }else{
                                  log.error("The operator is different");
                              }
                         
                          }else{
                              log.error("The service name is diferent");
                          }
                      }else{
                          log.error("Guarantee not finded:"+termID+"-->"+gtID);
                      }                  
                  }
                }
              }
           }         
        }catch (Exception e){
            log.error("Error: "+e.toString());
            e.printStackTrace();
        }
        ///return diffences
        log.debug("Differences found:"+differences.size());
        return differences;      
    }
/**
 * Find the guarantee inside a term list.   
 * @param dataOri
 * @param termID
 * @param gtID
 * @return
 */
    public static GuaranteeData searchGuarantee(List<AgreementTermData> listTerms,String termID,String gtID){
        log.info("Inside searchGuarantee method");
        log.debug("TermID:"+termID);
        log.debug("gtID"+gtID);
        for(AgreementTermData term:listTerms){
            if(term.getAgreementId().equals(termID)){
                //term finded
                List<GuaranteeData> listGuarantee = term.getGuarantees();
                for(GuaranteeData guarantee: listGuarantee){
                    if(guarantee.getGtId().equals(gtID)){
                        //guarantee finded
                        return guarantee;
                    }                              
                }
            }     
        }
        log.debug("Guarantee not Found");
        return null;        
    }
    
    /**
     * This method will parse the SLA in order to obtain the required data in a more 
     * easy form to use   
     * @param sla
     * @return
     * @throws Exception
     */
    public static SLAData getSLAData(SLA sla) throws Exception{
        log.info("Inside getSLATData method");
        //Initialize the objects
        SLAData slaData = new SLAData();
        String templateId = null;
        HashMap<String, String> parties = new HashMap<String, String>(0);
        HashMap<String, String> contactPoints = new HashMap<String, String>(0);
        
        try{       
//TemplateId
        templateId = sla.getTemplateId()!=null?sla.getTemplateId().getValue():null;
//parse party data 
         Party[] partiesAux = sla.getParties();
         for(Party party:partiesAux){
             parties.put(party.getId().getValue(), party.getAgreementRole().getValue());
             log.info("PartyID:"+party.getId());
             log.info("Party Role:"+party.getAgreementRole());
             for(STND key:party.getPropertyKeys()){
                 contactPoints.put(key.getValue(), party.getPropertyValue(key));
                 //log.info("Property key:" + key.getValue());
                 //log.info("Property value:" +endPoint.getPropertyValue(key));
             }
             
         }            
//parse contact Point
         InterfaceDeclr[] interfaces = sla.getInterfaceDeclrs();      
         if(interfaces!=null && interfaces.length>0){
             for(InterfaceDeclr localInterface:interfaces){
                 log.info(localInterface.getId());             
                 /*for(Endpoint endPoint:localInterface.getEndpoints()){
                     endPoint.getPropertyKeys();
                     for(STND key:endPoint.getPropertyKeys()){
                         contactPoints.put(key.getValue(), endPoint.getPropertyValue(key));
                         //log.info("Property key:" + key.getValue());
                         //log.info("Property value:" +endPoint.getPropertyValue(key));
                     }
                 }*/
             }
         }//end else
         //Fill the main object in         
         slaData.setUuid(sla.getUuid());
         slaData.setParties(parties);
         slaData.setContactPoints(contactPoints);
         //Obtain agreement Terms
         Object[] terms = getAgrementTerms(sla.getAgreementTerms());
         if(terms[0]!=null){
             slaData.setBusinessParameters((AgreementTermData)terms[0]);
         }            
         if(terms[1]!=null){
             slaData.setAgreementsTerms((List<AgreementTermData>)terms[1]);
         }else  if(terms[1]!=null){
             slaData.setProductOfferingPrice((ProductOfferingPrice)terms[2]);
         }
         //SLA specific data
         slaData.setTemplateId(templateId);
         slaData.setAgreedAt(sla.getAgreedAt());
         slaData.setEffectiveFrom(sla.getEffectiveFrom());
         slaData.setEffectiveUntil(sla.getEffectiveUntil());

       }catch(Exception e){
           log.error(e.getMessage());
           e.printStackTrace();
           throw e;
       }    
       return slaData;
    }
    
    /**
     * This method will parse the template in order to obtain the required data in a more 
     * easy form to use   
     * @param template
     * @return
     * @throws Exception
     */
      public static SLATData getSLATData(SLATemplate template) throws Exception{
           log.info("Inside getSLATData method");
           //Initialize the objects
           SLATData slatData = new SLATData();
           String templateId = null;
           HashMap<String, String> parties = new HashMap<String, String>(0);
           HashMap<String, String> contactPoints = new HashMap<String, String>(0);
           try{       
   //TemplateId
           templateId = template.getUuid()!=null?template.getUuid().getValue():null;
   //parse party data 
            Party[] partiesAux = template.getParties();
            for(Party party:partiesAux){
                parties.put(party.getId().getValue(), party.getAgreementRole().getValue());
                log.info("PartyID:"+party.getId());
                log.info("Party Role:"+party.getAgreementRole());
                for(STND key:party.getPropertyKeys()){
                    contactPoints.put(key.getValue(), party.getPropertyValue(key));
                    //log.info("Property key:" + key.getValue());
                    //log.info("Property value:" +endPoint.getPropertyValue(key));
                }
            }            
   //parse contact Point
            InterfaceDeclr[] interfaces = template.getInterfaceDeclrs();      
            if(interfaces!=null && interfaces.length>0){
                for(InterfaceDeclr localInterface:interfaces){
                    log.info(localInterface.getId());             
                    /*for(Endpoint endPoint:localInterface.getEndpoints()){
                        endPoint.getPropertyKeys();
                        for(STND key:endPoint.getPropertyKeys()){
                            contactPoints.put(key.getValue(), endPoint.getPropertyValue(key));
                            //log.info("Property key:" + key.getValue());
                            //log.info("Property value:" +endPoint.getPropertyValue(key));
                        }
                    }*/
                }
            }//end else
            //Fill the main object in
            slatData.setTemplateId(templateId);
            slatData.setParties(parties);
            slatData.setContactPoints(contactPoints);
            //Obtain agreement Terms
            Object[] terms = getAgrementTerms(template.getAgreementTerms());
            if(terms[0]!=null){
                slatData.setBusinessParameters((AgreementTermData)terms[0]);
            }            
            if(terms[1]!=null){
                slatData.setAgreementsTerms((List<AgreementTermData>)terms[1]);
            }else  if(terms[2]!=null){
                slatData.setProductOfferingPrice((ProductOfferingPrice)terms[2]);
            }
          }catch(Exception e){
              log.error(e.getMessage());
              e.printStackTrace();
              throw e;
          }    
          return slatData;
           
       }
    
/**
 * This method obtain the guaranteeTerms from 
 * @param terms
 * @return Array that contains the Agreement and Business Terms 
 * @throws Exception
 */
    private  static Object[] getAgrementTerms(AgreementTerm[] terms ) throws Exception{      
    log.info("Inside getAgrementTerms method"); 
    //this object contains the agrementTerms and the business Terms.
    Object[] result = new Object[3];
    List<AgreementTermData> agreementsTerms = new ArrayList<AgreementTermData>(0);
    try{   
        List<GuaranteeData> guaranteeList = new ArrayList<GuaranteeData>(0);
        AgreementTermData agreementTermData = new AgreementTermData();
        GuaranteeData guaranteeData = new GuaranteeData();
       //parse guarantee terms            
          if(terms!=null && terms.length>0){
              for(int i=0;i<terms.length;i++){        
                  agreementTermData = new AgreementTermData();
                  Guaranteed[] guarantees = terms[i].getGuarantees();
                  log.info("Term "+i+" ID:"+terms[i].getId());
                  agreementTermData.setAgreementId(terms[i].getId().getValue());              
                  State aux;
                  guaranteeList = new ArrayList<GuaranteeData>(0);
                  for(int j=0;j<guarantees.length;j++){ 
            	  
                      guaranteeData = new GuaranteeData();
                      log.info("Guarantee "+j+" ID:"+guarantees[j].getId().getValue());
                      guaranteeData.setGtId(guarantees[j].getId().getValue());
                      //State part
                      if(guarantees[j] instanceof State ){
                          log.info("Guarantee is a Guarantee.State");
                          aux = (State) guarantees[j];  
                          guaranteeData.setType(meta.$STATE);
                          ConstraintExpr cons = aux.getState();
                          if(cons instanceof TypeConstraintExpr){  
                              log.info("ConstraintExpr is TypeConstraintExpr");
                              TypeConstraintExpr constraint = (TypeConstraintExpr)cons;
                              STND Operator =((FunctionalExpr)constraint.getValue()).getOperator();
                              log.info("Operator value:"+Operator.getValue());
                              int index = Operator.getValue().lastIndexOf("#")+1;
                              guaranteeData.setGtParameterName(Operator.getValue().substring(index));
                              ValueExpr[] parameters =((FunctionalExpr)constraint.getValue()).getParameters();
                              if(parameters[0] instanceof ID){                              
                                  ID idService = (ID)parameters[0];
                                  log.info("Id Service:"+idService.getValue());
                                  guaranteeData.setGtServiceName(idService.getValue());
                              }
                              //Domain expresion
                              if(constraint.getDomain()instanceof SimpleDomainExpr){
                                  SimpleDomainExpr domain = (SimpleDomainExpr)constraint.getDomain();
                                  STND comparisonOp = domain.getComparisonOp();
                                  log.info("comparison Operation:"+comparisonOp.getValue());
                                  //index = comparisonOp.getValue().lastIndexOf("#")+1;
                                  guaranteeData.setGtComparisonOperator(comparisonOp.getValue());//.substring(index));
                                  if(domain.getValue() instanceof CONST){
                                      log.info("Value is CONTS type");
                                      CONST  data = (CONST)domain.getValue();
                                      log.info("Parameter Value: "+data.getValue());
                                      if(data.getDatatype()!=null){
                                          log.info("Parameter Type: "+data.getDatatype().getValue());
                                          guaranteeData.setGtParameterUnit(data.getDatatype().getValue());
                                      }
                                      try{
                                         guaranteeData.setGtParameterValue(Double.parseDouble(data.getValue())); 
                                         guaranteeData.setGtParameterType("Numerical");
                                         log.debug("It is a numerical value");
                                      }catch(Exception e){
                                          log.debug("It is not a numerical value");
                                          try{
                                              guaranteeData.setGtParameterValueString(data.getValue());
                                              Boolean.parseBoolean(data.getValue());
                                              guaranteeData.setGtParameterType("Boolean");
                                              log.debug("It is a boolean value");
                                          }catch(Exception ex){
                                              log.debug("It is not a boolean value");
                                              guaranteeData.setGtParameterType("String");
                                              log.debug("It is a string value");
                                          }
                                      }                                                                            
                                  }else if(domain.getValue() instanceof ID){
                                      //this is for infra domain
                                     /* log.info("Value is ID type");
                                      ID  data = (ID)domain.getValue();
                                      VariableDeclr vari = terms[i].getVariableDeclr(data.getValue());
                                      if(vari instanceof Customisable){
                                          Customisable custom = (Customisable) vari;
                                          if(custom.getValue().getDatatype()!=null){
                                              log.info("Parameter Type: "+custom.getValue().getDatatype().getValue());
                                              guaranteeData.setGtParameterUnit(custom.getValue().getDatatype().getValue());
                                          }
                                          try{
                                              guaranteeData.setGtParameterValue(Double.parseDouble(custom.getValue().getValue())); 
                                              guaranteeData.setGtParameterType("Numerical");
                                              log.debug("It is a numerical value");
                                           }catch(Exception e){
                                               log.debug("It is not a numerical value");
                                               try{
                                                   guaranteeData.setGtParameterValueString(custom.getValue().getValue());
                                                   Boolean.parseBoolean(custom.getValue().getValue());
                                                   guaranteeData.setGtParameterType("Boolean");
                                                   log.debug("It is a boolean value");
                                               }catch(Exception ex){
                                                   log.debug("It is not a boolean value");
                                                   guaranteeData.setGtParameterType("String");
                                                   log.debug("It is a string value");
                                               }
                                           }                                          
                                      }else{
                                          break;
                                      }*/
                                      
                                  }else break;
                              } 
                          }
                          //Action part   
                      }else if(guarantees[j] instanceof Action) {
                          log.info("Guarantee is a Guarantee.Action");
                          guaranteeData.setType(meta.$ACTION);
                          Action action =(Action)guarantees[j];
                          log.info("Operator Name:"+action.getPrecondition().getOperator());
                          guaranteeData.setGtParameterName(action.getPrecondition().getOperator().getValue());
                          Expr[] parameters =action.getPrecondition().getParameters();
                          //precondition data
                          if(parameters[0] instanceof CONST){                              
                              log.info("Value is CONTS type");
                              CONST  data = (CONST)parameters[0];
                              log.info("Parameter Value: "+data.getValue());
                              log.info("Parameter Type: "+data.getDatatype().getValue());
                              guaranteeData.setGtParameterUnit(data.getDatatype().getValue());
                              guaranteeData.setGtParameterValue(Long.parseLong(data.getValue()));
                          }   
                          Defn upperAction = action.getPostcondition();
                          if(upperAction instanceof CustomAction){         
                              //business Terms introduced as xml
                              CustomAction localAction = (CustomAction)action.getPostcondition();
                              log.info("Description: "+localAction.getDescr());
                              guaranteeData.setDescription(localAction.getDescr());
                              STND[] properties = localAction.getPropertyKeys();
                              log.info("properties length:" + properties.length);
                              HashMap<String, String> mapProperties = new HashMap<String, String>(0);
                              for(STND property:properties){
                                  //log.info("Property:"+ property.getValue());
                                  //log.info("Value:"+ localAction.getPropertyValue(property));
                                  mapProperties.put(property.getValue(), localAction.getPropertyValue(property));
                              }  
                              guaranteeData.setProperties(mapProperties);
                          } else if(upperAction instanceof  ProductOfferingPrice){ 
                              log.info("Product offering Price Found");
                              //set the product offering price
                              result[2] = upperAction;
                          }else if(upperAction instanceof  Termination){
                              log.info("Termination Action Found");
                              
                          }else if(upperAction instanceof  TerminationClause){
                              log.info("Termination Clause Found");
                              
                          }else if(upperAction instanceof  Penalty){
                              log.info("Penalty Action Found");
                              
                          }
                              
                      }
                      guaranteeList.add(guaranteeData);
                      //Add the guarantees to the agreement term.
                      agreementTermData.setGuarantees(guaranteeList);
                  }
                  if(terms[i].getId().getValue().equalsIgnoreCase("BusinessTerm")){
                      log.info("This GT containts the businessTerms");
                      result[0]=agreementTermData;
                  }else{
                      agreementsTerms.add(agreementTermData);
                  } 
              }
          }
          
        }catch(Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
            throw e;
        } 
        //set the agreement terms
        result[1]=agreementsTerms;
        return result;
    }
//End of class
}
