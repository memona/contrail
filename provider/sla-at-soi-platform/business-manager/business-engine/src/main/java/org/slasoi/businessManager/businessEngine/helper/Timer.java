package org.slasoi.businessManager.businessEngine.helper;

public class Timer {
    // Var crono
    private static java.util.Date t0, t1;

    /**
     * Inicializator.
     */
    public static void initTime() {
        t0 = new java.util.Date();
    }

    /**
     * Time passed.
     *
     * @return the long
     */
    public static long passedTime() {
        t1 = new java.util.Date();
        return (t1.getTime() - t0.getTime());
    }

}