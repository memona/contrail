package org.slasoi.businessManager.businessEngine.assessment;

import org.slasoi.businessManager.common.model.EmSpProducts;
import org.slasoi.slamodel.sla.SLATemplate;

public interface SemiAutomaticChecker {
    
 /**
  * This method checks if a semiautomaticNegotiation has to be manual or automatic  
  * @param template
  * @param product
  * @return
  */
    public boolean checkNegotiationType(SLATemplate template, EmSpProducts product); 

}
