/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/business-engine/src/main/java/org/slasoi/businessManager/businessEngine/SLAEvaluation/GuaranteeData.java $
 */

package org.slasoi.businessManager.businessEngine.SLAEvaluation;

import java.util.HashMap;

public class GuaranteeData {
	
	private String gtId;
	private String type;
	private String gtParameterName;
	private String gtServiceName;
	private String gtComparisonOperator;
    private String gtParameterUnit;
	private double gtParameterValue;
	private String gtParameterValueString;
	private String gtParameterType;
	private String description;
	private HashMap<String, String> properties;
	
		
    public String getGtParameterValueString() {
        return gtParameterValueString;
    }

    public void setGtParameterValueString(String gtParameterValueString) {
        this.gtParameterValueString = gtParameterValueString;
    }

    public String getGtParameterType() {
        return gtParameterType;
    }

    public void setGtParameterType(String gtParameterType) {
        this.gtParameterType = gtParameterType;
    }

    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public String getGtId() {
        return gtId;
    }
    public void setGtId(String gtId) {
        this.gtId = gtId;
    }
    public String getGtParameterName() {
        return gtParameterName;
    }
    public void setGtParameterName(String gtParameterName) {
        this.gtParameterName = gtParameterName;
    }
    public String getGtServiceName() {
        return gtServiceName;
    }
    public void setGtServiceName(String gtServiceName) {
        this.gtServiceName = gtServiceName;
    }
    public String getGtComparisonOperator() {
        return gtComparisonOperator;
    }
    public void setGtComparisonOperator(String gtComparisonOperator) {
        this.gtComparisonOperator = gtComparisonOperator;
    }
    public String getGtParameterUnit() {
        return gtParameterUnit;
    }
    public void setGtParameterUnit(String gtParameterUnit) {
        this.gtParameterUnit = gtParameterUnit;
    }
    public double getGtParameterValue() {
        return gtParameterValue;
    }
    public void setGtParameterValue(double gtParameterValue) {
        this.gtParameterValue = gtParameterValue;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public HashMap<String, String> getProperties() {
        return properties;
    }
    public void setProperties(HashMap<String, String> properties) {
        this.properties = properties;
    }
	
	
	}
