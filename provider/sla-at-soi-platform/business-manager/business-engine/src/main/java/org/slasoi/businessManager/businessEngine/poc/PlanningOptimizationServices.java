/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/trunk/business-manager/business-engine/src/main/java/org/slasoi/businessManager/businessEngine/poc/PlanningOptimizationServices.java $
 */

package org.slasoi.businessManager.businessEngine.poc;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.businessEngine.poc.impl.ManualNegotiation;
import org.slasoi.businessManager.businessEngine.poc.impl.PlanningOptimizationImpl;
import org.slasoi.businessManager.common.service.CustomerProductManager;
import org.slasoi.businessManager.common.service.GtTranslationManager;
import org.slasoi.businessManager.common.service.ProductOfferManager;
import org.slasoi.businessManager.common.service.ServiceManager;
import org.slasoi.businessManager.productSLA.management.SLAManagement;
import org.slasoi.gslam.core.builder.PlanningOptimizationBuilder;
import org.slasoi.gslam.core.poc.PlanningOptimization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.osgi.extensions.annotation.ServiceReference;

public class PlanningOptimizationServices implements PlanningOptimizationBuilder{   
    private static final Logger log = Logger.getLogger(PlanningOptimizationImpl.class);
    private ProductOfferManager productOfferService;  
    private CustomerProductManager customerProductManager;
    private ServiceManager serviceManager;
    private SLAManagement slaManagement;    
    @Autowired
    private ManualNegotiation manualNegotiation;
    @Autowired
    private GtTranslationManager gtTranslationManager;
    
    @ServiceReference
    // obtained automatically by Spring-DM and OSGi Container.
    public void setSLAManagement(SLAManagement slaManagement) {
        log.info("***************Registered Service productOfferservice Found*************");
        this.slaManagement = slaManagement;
    }    
    @ServiceReference
    // obtained automatically by Spring-DM and OSGi Container.
    public void setCustomerProductManager(CustomerProductManager customerProductManager) {
        log.info("***************Registered Service productOfferservice Found*************");
        this.customerProductManager = customerProductManager;
    }    
    @ServiceReference
    // obtained automatically by Spring-DM and OSGi Container.
    public void setProductOfferservice(ProductOfferManager productOfferService) {
        log.info("***************Registered Service productOfferservice Found*************");
        this.productOfferService = productOfferService;
    }    
    @ServiceReference
    // obtained automatically by Spring-DM and OSGi Container.
    public void setServiceManager(ServiceManager serviceManager) {
        log.info("***************Registered Service serviceManager Found*************");
        this.serviceManager = serviceManager;    }
                   
    public PlanningOptimization create()
    {
        PlanningOptimizationBus planing = new PlanningOptimizationBus();
        planing.iAssessmentAndCustomizeImpl.setSLAManagement(slaManagement);
        planing.iAssessmentAndCustomizeImpl.setCustomerProductManager(customerProductManager);
        planing.iAssessmentAndCustomizeImpl.setProductOfferservice(productOfferService);
        planing.iAssessmentAndCustomizeImpl.setServiceManager(serviceManager);
        planing.iAssessmentAndCustomizeImpl.setGtTranslationManager(gtTranslationManager);
        planing.setManualNegotiation(manualNegotiation);
        return planing;
    }
}
