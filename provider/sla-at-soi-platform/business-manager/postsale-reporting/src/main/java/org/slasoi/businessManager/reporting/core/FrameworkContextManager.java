/**
 * 
 */
package org.slasoi.businessManager.reporting.core;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 31, 2011
 */
public class FrameworkContextManager {
	// logger
	private static Logger logger = Logger.getLogger(FrameworkContextManager.class);
	private static final String CONF_FOLDER_NAME = "conf";
	private static FrameworkContext frameworkContext;
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return The FrameworkContext object
	 */
	public static FrameworkContext getFrameworkContext() {
		// if the context is not initialised then do it
		if (frameworkContext == null) {
			frameworkContext = initFrameworkContext();
		}
		return frameworkContext;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return The FrameworkContext object
	 */
	private static FrameworkContext initFrameworkContext() {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		
		String fileSeparator = System.getProperty("file.separator");
		String slasoiHome = System.getenv("SLASOI_HOME");
		
		// path to configuration folder within SLA@SOI OSGI environment
		if (slasoiHome != null) {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(fileSeparator);
			frameworkConfigurationFolder.append("bmanager-postsale-reporting");
		}
		// path to configuration folder as stand alone framework
		else {
			logger.debug("Environmental variable JAVA_HOME not set.");			
			frameworkConfigurationFolder = new StringBuilder(ClassLoader.getSystemResource(CONF_FOLDER_NAME).getPath());
		}
		
		try {
			logger.debug("Loading context properties from: " + new File(frameworkConfigurationFolder.toString()).getCanonicalPath());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		
		return new FrameworkContext(frameworkConfigurationFolder.toString());
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
