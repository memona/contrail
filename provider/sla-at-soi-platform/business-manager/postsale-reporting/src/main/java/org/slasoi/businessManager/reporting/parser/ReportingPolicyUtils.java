/**
 * 
 */
package org.slasoi.businessManager.reporting.parser;

import org.apache.log4j.Logger;
import org.slaatsoi.business.schema.PeriodicFrequencyType;
import org.slaatsoi.business.schema.PeriodicScheduleType;
import org.slasoi.businessManager.reporting.aggregator.temporal.TemporalAggregator;
import org.slasoi.businessManager.reporting.aggregator.temporal.TemporalAggregatorFactory;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 16, 2011
 */
public class ReportingPolicyUtils {
	// logger
	private static Logger logger = Logger.getLogger(ReportingPolicyUtils.class);
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @param reportingPolicy
	 * @return
	 */
	public static TemporalAggregator getTemporalAggregator(PeriodicScheduleType periodicSchedule) {
		TemporalAggregator temporalAggregator = null;
		
		PeriodicFrequencyType periodicFrequencyType = getPeriodicFrequencyType(periodicSchedule);
		
		temporalAggregator = TemporalAggregatorFactory.create(periodicFrequencyType);
		
		return temporalAggregator;
	}
	
	/**
	 * @param functionalAggreagtorClassName
	 * @return The instance of the given functional aggregator, <code>null</code> if any error occurs
	 */
	public static org.slasoi.businessManager.reporting.aggregator.functional.FunctionalAggregator getFunctionalAggregator(String functionalAggreagtorClassName) {
		try {
			// dynamically load and create a new class instance
			return  (org.slasoi.businessManager.reporting.aggregator.functional.FunctionalAggregator) Class.forName(functionalAggreagtorClassName).newInstance();
		} catch (InstantiationException e) {
			logger.error(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			logger.error(e.getMessage(), e);
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}
	
	/**
	 * @param periodicSchedule
	 * @return A periodic frequency instance, <code>null</code> otherwise
	 */
	public static PeriodicFrequencyType getPeriodicFrequencyType(PeriodicScheduleType periodicSchedule) {
		PeriodicFrequencyType periodicFrequency = null;
		
		if ((periodicFrequency = periodicSchedule.getRepeatSecondly()) != null) return periodicFrequency;
		else if ((periodicFrequency = periodicSchedule.getRepeatMinutely()) != null) return periodicFrequency;
		else if ((periodicFrequency = periodicSchedule.getRepeatHourly()) != null) return periodicFrequency;
		else if ((periodicFrequency = periodicSchedule.getRepeatWeekday()) != null) return periodicFrequency;
		else if ((periodicFrequency = periodicSchedule.getRepeatWeekend()) != null) return periodicFrequency;
		else if ((periodicFrequency = periodicSchedule.getRepeatWeekly()) != null) return periodicFrequency;
		else if ((periodicFrequency = periodicSchedule.getRepeatMonthly()) != null) return periodicFrequency;
		else if ((periodicFrequency = periodicSchedule.getRepeatYearly()) != null) return periodicFrequency;
		
		return periodicFrequency;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
