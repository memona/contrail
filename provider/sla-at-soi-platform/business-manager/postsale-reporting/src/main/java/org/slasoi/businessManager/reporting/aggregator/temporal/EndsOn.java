/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.temporal;

import java.util.Date;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public class EndsOn implements Ends {
	private Date date;

	/**
	 * @param date
	 */
	public EndsOn(Date date) {
		this.date = date;
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	public String toString() {
		return getClass().getName() + "{date=" + date + "}";
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
