/**
 * 
 */
package org.slasoi.businessManager.reporting.store;

import org.slasoi.businessManager.reporting.parser.ReportingPolicy;

import uk.ac.city.soi.database.EntityManagerInterface;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 19, 2011
 */
public interface ReportingPolicyEntityManager extends EntityManagerInterface<ReportingPolicy> {
	
	/**
	 * @param slaId
	 * @param reportingPolicyId
	 * @return
	 */
	public ReportingPolicy selectBySlaIdAndReportingPolicyId(String slaId, String reportingPolicyId);
}
