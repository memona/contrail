/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.temporal;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public enum Weekdays {
	MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}
