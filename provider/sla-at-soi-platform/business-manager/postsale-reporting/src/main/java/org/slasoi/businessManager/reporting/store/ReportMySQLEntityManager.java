/**
 * 
 */
package org.slasoi.businessManager.reporting.store;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.reporting.report.Report;

import uk.ac.city.soi.database.EntityManagerCommons;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 18, 2011
 */
public class ReportMySQLEntityManager extends EntityManagerCommons implements ReportEntityManager {
    // logger
    private static Logger logger = Logger.getLogger(ReportMySQLEntityManager.class);
    
	private static final String DATABASE_TABLE_NAME = "reports";
	
	/**
	 * @param connection
	 */
	public ReportMySQLEntityManager(Connection connection) {
		super(connection);
		this.setDatabaseTable(DATABASE_TABLE_NAME);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see org.slasoi.businessManager.reporting.store.ReportEntityManager#insert(org.slasoi.businessManager.reporting.report.Report)
	 */
	
	public int insert(Report report) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        int result = 0;

        try {
            Blob blob = toBlob(report);

            pstmt = getConnection().prepareStatement(
            		"INSERT INTO " + getDatabaseTable() + " " +
                    "(report_id, sla_id, creation_date, format, report_object) " +
                    "VALUES (?, ?, ?, ?, ?)");
            
            pstmt.setString(1, report.getReportId());
            pstmt.setString(2, report.getSlaId());
            pstmt.setLong(3, report.getCreationDate().getTime());
            pstmt.setString(4, report.getFormat().toString());
            pstmt.setBlob(5, blob);

            //logger.debug("Inserting report: " + pstmt);
            
            result = pstmt.executeUpdate();
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
        	logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }
        
        return result;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.ReportEntityManager#selectBySlaId(java.lang.String, java.lang.String, java.util.Date, java.util.Date, java.lang.String)
	 */
	
	public ArrayList<Report> selectBySlaId(String slaId, String reportingPolicyId, Date fromDate, Date toDate, String format) {
        ArrayList<Report> reports = new ArrayList<Report>();
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = getConnection().prepareStatement(
            		"SELECT report_object " + "FROM " + getDatabaseTable() +  " " +
                    "WHERE report_id = ? AND sla_id = ? AND creation_date >= ? AND creation_date <= ? AND format = ?");
            
            pstmt.setString(1, reportingPolicyId);
            pstmt.setString(2, slaId);
            pstmt.setLong(3, fromDate.getTime());
            pstmt.setLong(4, toDate.getTime());
            pstmt.setString(5, format);
            
            //logger.debug("Selecting reports: " + pstmt);
            
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                    // get the blob
                    Blob blob = resultSet.getBlob("report_object");
                    Report report;
					try {
						report = (Report) toObject(blob);
						reports.add(report);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
                }
                while (resultSet.next());
            }
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        catch (IOException e) {
        	logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return reports;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.ReportEntityManager#countBySlaId(java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	
	public int countBySlaId(String slaId, String reportingPolicyId, Date fromDate, Date toDate) {
        int reports = 0;
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;

        try {
            pstmt = getConnection().prepareStatement(
            		"SELECT COUNT(*) AS reports " + "FROM " + getDatabaseTable() +  " " +
                    "WHERE report_id = ? AND sla_id = ? AND creation_date >= ? AND creation_date <= ?");
            
            pstmt.setString(1, reportingPolicyId);
            pstmt.setString(2, slaId);
            pstmt.setLong(3, fromDate.getTime());
            pstmt.setLong(4, toDate.getTime());
            
            resultSet = pstmt.executeQuery();

            // if any blob has been returned
            if (resultSet.first()) {
                do {
                	reports = resultSet.getInt("reports");
                } while (resultSet.next());
            }
        }
        catch (SQLException e) {
            // handle any errors
            logger.error(e.getMessage(), e);
        }
        finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            releaseResources(pstmt, resultSet);
        }

        return reports;
	}

	/**
	 * @see org.slasoi.businessManager.reporting.store.ReportEntityManager#count()
	 */
	
	public int count() {
		return super.count();
	}

	// ------------------------------------------------------------------------
	// 						PUBLIC METHODS NOT IMPLEMENTED
	// ------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#update(java.lang.Object)
	 */
	
	public int update(Report paramT) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#delete(java.lang.Object)
	 */
	
	public int delete(Report paramT) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#deleteByPrimaryKey(java.lang.String)
	 */
	
	public int deleteByPrimaryKey(String paramString) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#selectByPrimaryKey(java.lang.String)
	 */
	
	public Report selectByPrimaryKey(String paramString) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#select()
	 */
	
	public ArrayList<Report> select() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see uk.ac.city.soi.database.EntityManagerInterface#executeQuery(java.lang.String)
	 */
	
	public ArrayList<Report> executeQuery(String paramString) {
		// TODO Auto-generated method stub
		return null;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
