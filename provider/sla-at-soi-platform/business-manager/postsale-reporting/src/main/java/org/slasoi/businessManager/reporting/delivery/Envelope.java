/**
 * 
 */
package org.slasoi.businessManager.reporting.delivery;

import org.slasoi.businessManager.reporting.report.Report;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 3, 2011
 */
public class Envelope {
	private String message;
	private Report[] reports;
	private Actor sender;
	private Actor[] receivers;
	
	/**
	 * @param reports
	 * @param sender
	 * @param receivers
	 */
	public Envelope(String message, Report[] reports, Actor sender, Actor[] receivers) {
		this.message = message;
		this.reports = reports;
		this.sender = sender;
		this.receivers = receivers;
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * @return the reports
	 */
	public Report[] getReports() {
		return reports;
	}
	
	/**
	 * @return the sender
	 */
	public Actor getSender() {
		return sender;
	}
	
	/**
	 * @return the receivers
	 */
	public Actor[] getReceivers() {
		return receivers;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
