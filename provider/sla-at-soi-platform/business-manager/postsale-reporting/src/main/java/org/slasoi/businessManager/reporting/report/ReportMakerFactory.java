/**
 * 
 */
package org.slasoi.businessManager.reporting.report;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.slaatsoi.business.schema.ReportFormatType;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public class ReportMakerFactory {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	Hashtable<ReportFormatType, ReportMaker> reportMakers;
	
	/**
	 * 
	 */
	public ReportMakerFactory() {
		reportMakers = new Hashtable<ReportFormatType, ReportMaker>();
		
		// init report makers
		reportMakers.put(ReportFormatType.PDF, new PDFReportMaker());
		reportMakers.put(ReportFormatType.XML, new XMLReportMaker());
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @param jobResult
	 * @return the created report, <code>null</code> if errors occurred 
	 */
	public ReportMaker createReportMaker(ReportFormatType reportFormat) {
		ReportMaker reportMaker = null;
		
		// check is suitable report maker exists
		if (reportMakers.containsKey(reportFormat)) {
			
			// retrieve the appropriate report maker
			reportMaker = reportMakers.get(reportFormat);
		}
		
		return reportMaker;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
