/**
 * 
 */
package org.slasoi.businessManager.reporting.aggregator.temporal;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public class EndsNever implements Ends {
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see java.lang.Object#toString()
	 */
	
	public String toString() {
		// TODO Auto-generated method stub
		return getClass().getName();
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
