/**
 * 
 */
package org.slasoi.businessManager.reporting.schedule;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slaatsoi.business.schema.PeriodicScheduleType;
import org.slasoi.businessManager.reporting.aggregator.temporal.TemporalAggregator;
import org.slasoi.businessManager.reporting.core.FrameworkContext.PropertiesFileNames;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.parser.ReportingPolicy;
import org.slasoi.businessManager.reporting.parser.ReportingPolicyUtils;
import org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager;
import org.slasoi.businessManager.reporting.store.ReportEntityManager;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public class JobScheduler implements Runnable {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	private MonitoringResultEntityManager monitoringResultEntityManager;
	private ReportEntityManager reportEntityManager;
	private Scheduler scheduler;
	private boolean isStopped;

	/**
	 * the 'default' scheduler is defined in "quartz.properties" found in the
	 * current working directory, in the classpath, or resorts to a fall-back
	 * default that is in the quartz.jar
	 * 
	 * @param monitoringResultEntityManager
	 * @param reportEntityManager
	 * @throws SchedulerException
	 */
	public JobScheduler(MonitoringResultEntityManager monitoringResultEntityManager, ReportEntityManager reportEntityManager) throws SchedulerException {
		this.monitoringResultEntityManager = monitoringResultEntityManager;
		this.reportEntityManager = reportEntityManager;
		
		// retrieves from the framework context Quartz properties
		Properties quartzProperties = FrameworkContextManager.getFrameworkContext().getFrameworkProperties(PropertiesFileNames.QUARTZ);
		
		SchedulerFactory chedulerFactory = new StdSchedulerFactory(quartzProperties);
		scheduler = chedulerFactory.getScheduler();
		
		isStopped = false;
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see java.lang.Runnable#run()
	 */
	
	public void run() {
		while (!isStopped) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
	}
	
	/**
	 * Scheduler will not execute jobs until it has been started (though
	 * they can be scheduled before start())
	 * @throws SchedulerException 
	 * 
	 */
	public void start() throws SchedulerException {
		scheduler.start();
		isStopped = false;
	}
	
	/**
	 * To shutdown / destroy a scheduler
	 * 
	 * @throws SchedulerException
	 */
	public void stop() throws SchedulerException {
		scheduler.shutdown();
		isStopped = true;
	}
	
	/**
	 * @param reportingPolicy
	 * @return
	 * @throws SchedulerException 
	 */
	public boolean addCreateReportJob(ReportingPolicy reportingPolicy, String reportTemplateFile) {
		PeriodicScheduleType periodicSchedule = reportingPolicy.getBusinessTerm().getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule();
		
		TemporalAggregator temporalAggregator = ReportingPolicyUtils.getTemporalAggregator(periodicSchedule);
		
		String jobId = JobTypes.CREATE_REPORT_JOB.toString() + "$" + reportingPolicy.getSlaId() + "$" + reportingPolicy.getReportingPolicyId();
		
		// Define job instance
		JobDetail job = new JobDetail(
				jobId,									// job name id 
				JobTypes.CREATE_REPORT_JOB.toString(),	// group id
				CreateReportJob.class					// job class
			);
		
		// set job properties
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put(CreateReportJob.DataMapProperties.REPORTING_CONFIGURATION.toString(), reportingPolicy);
		jobDataMap.put(CreateReportJob.DataMapProperties.MONITORING_RESULT_DB_MANAGER.toString(), monitoringResultEntityManager);
		jobDataMap.put(CreateReportJob.DataMapProperties.REPORT_DB_MANAGER.toString(), reportEntityManager);
		jobDataMap.put(CreateReportJob.DataMapProperties.REPORT_TEMPLATE_FILE.toString(), reportTemplateFile);
		
		job.setJobDataMap(jobDataMap);
		
		//creates one or more triggers according to the current reporting configuration
		ArrayList<Trigger> triggers = TriggerFactory.createTrigger(
				job.getName(),		// job id 
				job.getGroup(),		// group id
				temporalAggregator	// temporal aggregator
				);
		
		// schedule job and triggers
		for (Trigger trigger : triggers) {
			// Schedule the job with the trigger
			try {
				scheduler.scheduleJob(job, trigger);
				
				logger.debug("Trigger create report: " + trigger);
				logger.debug("Job Scheduled: " + job.getName());
				logger.debug("- Temporal aggregator: " + temporalAggregator);
				logger.debug("- Now date:\t" + new Date());
				logger.debug("- Trigger starts on:\t" + trigger.getStartTime());
				logger.debug("- Trigger ends on:\t" + trigger.getEndTime());
				logger.debug("- Next scheduled job:\t" + trigger.getNextFireTime());
			} catch (SchedulerException e) {
				logger.error(e.getMessage(), e);
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * @param reportingPolicy
	 * @return
	 * @throws SchedulerException 
	 */
	public boolean addDeliverReportJob(ReportingPolicy reportingPolicy) {
		PeriodicScheduleType periodicSchedule = reportingPolicy.getBusinessTerm().getReporting().getReportDeliverySchedule().getAutomaticSchedule().getPeriodicSchedule();
		
		TemporalAggregator temporalAggregator = ReportingPolicyUtils.getTemporalAggregator(periodicSchedule);
		
		String jobId = JobTypes.DELIVER_REPORT_JOB.toString() + "$" + reportingPolicy.getSlaId() + "$" + reportingPolicy.getReportingPolicyId();
		
		// Define job instance
		JobDetail job = new JobDetail(
				jobId,									// job id 
				JobTypes.DELIVER_REPORT_JOB.toString(),	// group id
				DeliverReportJob.class					// job class
			);
		
		// set job properties
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put(CreateReportJob.DataMapProperties.REPORTING_CONFIGURATION.toString(), reportingPolicy);
		jobDataMap.put(CreateReportJob.DataMapProperties.REPORT_DB_MANAGER.toString(), reportEntityManager);
		
		job.setJobDataMap(jobDataMap);
		
		//creates one or more triggers according to the current reporting configuration
		ArrayList<Trigger> triggers = TriggerFactory.createTrigger(
				job.getName(),		// job id 
				job.getGroup(),		// group id
				temporalAggregator	// temporal aggregator
				);
		
		// schedule job and triggers
		for (Trigger trigger : triggers) {
			// Schedule the job with the trigger
			try {
				scheduler.scheduleJob(job, trigger);
				
				logger.debug("Trigger delivery report: " + trigger);
				logger.debug("Job Scheduled: " + job.getName());
				logger.debug("- Temporal aggregator: " + temporalAggregator);
				logger.debug("- Now date:\t" + new Date());
				logger.debug("- Trigger starts on:\t" + trigger.getStartTime());
				logger.debug("- Trigger ends on:\t" + trigger.getEndTime());
				logger.debug("- Next scheduled job:\t" + trigger.getNextFireTime());
			} catch (SchedulerException e) {
				logger.error(e.getMessage(), e);
				return false;
			}
		}
		
		return true;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
