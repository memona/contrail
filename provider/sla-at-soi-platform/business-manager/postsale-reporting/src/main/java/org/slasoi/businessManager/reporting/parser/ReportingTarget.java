/**
 * 
 */
package org.slasoi.businessManager.reporting.parser;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 19, 2011
 */
public class ReportingTarget {
	private String functionalAggregatorClassName;
	private String slaId;
	private String agreementTermId;
	private String guaranteedId;
	
	// ------------------------------------------------------------------------
	// 								CONSTRUCTOR
	// ------------------------------------------------------------------------
	
	/**
	 * @param reportingTargetTypes
	 * @param reportingTargetId
	 */
	public ReportingTarget(String functionalAggregatorClassName, String slaId, String agreementTermId, String guaranteedId) {
		this.functionalAggregatorClassName = functionalAggregatorClassName;
		this.slaId = slaId;
		this.agreementTermId = agreementTermId;
		this.guaranteedId = guaranteedId;
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the functionalAggregatorClassName
	 */
	public String getFunctionalAggregatorClassName() {
		return functionalAggregatorClassName;
	}
	
	/**
	 * @return the slaId
	 */
	public String getSlaId() {
		return slaId;
	}
	
	/**
	 * @return the agreementTermId
	 */
	public String getAgreementTermId() {
		return agreementTermId;
	}
	
	/**
	 * @return the guaranteedId
	 */
	public String getGuaranteedId() {
		return guaranteedId;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	
	public String toString() {
		return getClass().getName() + "{" +
				"functionalAggregatorClassName=" + functionalAggregatorClassName + "," + 
				"slaId=" + slaId + "," +
				"agremmentTermId=" + agreementTermId + "," +
				"guaranteedId=" + guaranteedId + "}";
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
