/**
 * 
 */
package org.slasoi.businessManager.reporting.delivery;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 3, 2011
 */
public interface ReportDeliverer {
	
	/**
	 * Deliver the envelope to the specified receivers 
	 * 
	 * @param envelope
	 * @return true is the delivery succeeded, false otherwise 
	 */
	public boolean deliver(Envelope envelope);
}
