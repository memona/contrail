/**
 * 
 */
package org.slasoi.businessManager.reporting.report;

import javax.xml.bind.JAXBException;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.reporting.utils.XMLUtils;
import org.slasoi.common.reportschema.PostSaleReportType;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public abstract class ReportMaker {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	/**
	 * Creates a report from a PostSaleReportType object
	 * 
	 * @param reportTemplateFile The file containing the report template to be filled with data
	 * @param jobResult A PostSaleReportType object
	 * @return A Report object, null is any error occurs
	 */
	public Report createReport(String reportTemplateFile, PostSaleReportType postSaleReport) {
		Report report = null;
		
		try {
			JasperReport reportTemplate = (JasperReport) JRLoader.loadObject(reportTemplateFile);
			report = createReport(reportTemplate, postSaleReport);
		} catch (JRException e) {
			logger.error(e.getMessage(), e);
		}
		
		return report;
	}
	
	/**
	 * Creates a report from an String object containing the XML representation
	 * of a PostSaleReportType object
	 * 
	 * @param reportTemplateFile The file containing the report template to be filled with data
	 * @param xmlPostSaleReport The XML representation of a PostSaleReportType object
	 * @return A Report object, null is any error occurs
	 */
	public Report createReport(String reportTemplateFile, String xmlPostSaleReport) {
		Report report = null;
		
		try {
			JasperReport reportTemplate = (JasperReport) JRLoader.loadObject(reportTemplateFile);
			report = createReport(reportTemplate, xmlPostSaleReport);
		} catch (JRException e) {
			logger.error(e.getMessage(), e);
		}
		
		return report;
	}
	
	/**
	 * Creates a report from a PostSaleReportType object
	 * 
	 * @param reportTemplate The report template to be filled with data
	 * @param jobResult A PostSaleReportType object
	 * @return A Report object, null is any error occurs
	 */
	public abstract Report createReport(JasperReport reportTemplate, String xmlPostSaleReport);

	
	/**
	 * Creates a report from an String object containing the XML representation
	 * of a PostSaleReportType object
	 * 
	 * @param reportTemplate The report template to be filled with data
	 * @param xmlPostSaleReport The XML representation of a PostSaleReportType object
	 * @return A Report object, null is any error occurs
	 */
	public Report createReport(JasperReport reportTemplate, PostSaleReportType postSaleReport) {
		String dataSource = null;
		
		// Marshal the object into an XML string
		try {
			dataSource = XMLUtils.marshall(postSaleReport);
		} catch (JAXBException e) {
			logger.error(e.getMessage(), e);
		}
		
		return createReport(reportTemplate, dataSource);
	}
}
