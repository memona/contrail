/**
 * 
 */
package org.slasoi.businessManager.reporting.delivery;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.slaatsoi.business.schema.ReportFormatType;
import org.slasoi.businessManager.reporting.core.FrameworkContext.PropertiesFileNames;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.report.Report;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 3, 2011
 */
public class EmailReportDeliverer implements ReportDeliverer {
	// Logger
	Logger logger = Logger.getLogger(getClass());
	
	String propertiesFile = System.getenv("SLASOI_HOME") + "/bmanager-postsale-reporting/javamail.properties";
	Properties properties;
	
	private static final String MAIL_SMTP_AUTH_USER = "mail.smtp.auth.user";
	private static final String MAIL_SMTP_AUTH_PASSWORD = "mail.smtp.auth.password";

	/**
	 * Load properties from the default properties file
	 * "/conf/smtp.properties"
	 */
	public EmailReportDeliverer() {	
		initProperties();
	}

	EmailReportDeliverer(String propertiesFile) {
		this.propertiesFile = propertiesFile;
		
		initProperties();
	}
	
	// ------------------------------------------------------------------------
	// PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @see org.slasoi.businessManager.reporting.delivery.ReportDeliverer#deliver(org.slasoi.businessManager.reporting.delivery.Envelope)
	 */
	
	public boolean deliver(Envelope envelope) {
		//for (Object key : properties.keySet()) {
		//	logger.debug(key + ": " + properties.getProperty((String) key));
		//}
		
		boolean result = true;
		
		try {
			result = postMail(envelope);
		} catch (MessagingException e) {
			result = false;
			
			logger.error(e.getMessage(), e);
		}

		return result;
	}

	// ------------------------------------------------------------------------
	// PRIVATE METHODS
	// ------------------------------------------------------------------------

	/**
	 * @param sender
	 * @param recipients
	 * @param subject
	 * @param reports
	 * @return
	 * @throws MessagingException
	 */
	private boolean postMail(Envelope envelope) throws MessagingException {
		ArrayList<String> recipients = new ArrayList<String>();
		
		// Retrieves all receivers' emails		
		for (Actor actor : envelope.getReceivers()) {
			for (String email : actor.getEmails()) {
				recipients.add(email);
			}
		}
		
		Report[] reports = envelope.getReports();
		
		boolean result = true;

		String user = properties.getProperty(MAIL_SMTP_AUTH_USER);
		String password = properties.getProperty(MAIL_SMTP_AUTH_PASSWORD);
		Authenticator authenticator = new SMTPAuthenticator(user, password);
		System.err.println("User & Password " + user + " " + password);
		Session session = Session.getInstance(properties, authenticator);
		
		try {
			// create a message
			MimeMessage mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(envelope.getSender().getEmails()[0]));
			InternetAddress[] address = parseRecipients(recipients.toArray(new String[0]));
			mimeMessage.setRecipients(Message.RecipientType.TO, address);
			mimeMessage.setSubject("Reporting Manager - Report");

			// create and fill the first message part
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(envelope.getMessage());

			// create the Multipart and add its parts to it
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);
			
			// add attachments
			for( int i = 0; i < reports.length; i++ ) {
				// create the second message part
				MimeBodyPart mimeBodyPartAttachment = new MimeBodyPart();
				
				// get attachment content
				Object messageAttachment = reports[i].getContent();
				// get attachment type
				String messageAttachmentMimeType = parseReportType(reports[i].getFormat());
				
				mimeBodyPartAttachment.setDataHandler(new DataHandler(messageAttachment, messageAttachmentMimeType));
				mimeBodyPartAttachment.setFileName(reports[i].getCreationDate().getTime() + "$" + reports[i].getSlaId() + "$" + reports[i].getReportId());
				multipart.addBodyPart(mimeBodyPartAttachment);
			}

			// add the Multipart to the message
			mimeMessage.setContent(multipart);

			// set the Date: header
			mimeMessage.setSentDate(new Date());

			// send the message			
			Transport.send(mimeMessage);

		} catch (MessagingException e) {
			result = false;
			
			logger.error(e.getMessage(), e);
			Exception ex = null;
			if ((ex = e.getNextException()) != null) {
				logger.error(ex.getMessage(), ex);
			}
		}

		return result;
	}

	/**
	 * @param recipients
	 * @return
	 */
	private InternetAddress[] parseRecipients(String[] recipients) {
		InternetAddress[] internetAddresses = new InternetAddress[recipients.length];

		for (int i = 0; i < internetAddresses.length; i++) {
			try {
				internetAddresses[i] = new InternetAddress(recipients[i]);
			} catch (AddressException e) {
				logger.error(e.getMessage(), e);
			}
		}

		return internetAddresses;
	}

	/**
	 * Converts business report formats in RFC mime types
	 * 
	 * @param messageType
	 * @return
	 */
	private String parseReportType(ReportFormatType messageType) {
		String mimeType = new String();

		if (messageType.compareTo(ReportFormatType.HTML) == 0) {
			mimeType = "text/html";
		} else if (messageType.compareTo(ReportFormatType.PDF) == 0) {
			mimeType = "application/pdf";
		} else if (messageType.compareTo(ReportFormatType.TEXT) == 0) {
			mimeType = "text/plain";
		} else if (messageType.compareTo(ReportFormatType.XML) == 0) {
			mimeType = "text/xml";
		}

		return mimeType;
	}

	/**
	 * Load the properties file
	 */
	private void initProperties( ) {
		properties = FrameworkContextManager.getFrameworkContext().getFrameworkProperties(PropertiesFileNames.JAVAMAIL);
		
		// add other system properties
		properties.putAll(System.getProperties());
	}
	
	// ------------------------------------------------------------------------
	// ABSTRACT METHODS
	// ------------------------------------------------------------------------
    
	// ------------------------------------------------------------------------
	// INNER CLASSES
	// ------------------------------------------------------------------------
	
    /**
     * @author Davide Lorenzoli
     * 
     * @date Mar 3, 2011
     */
    private class SMTPAuthenticator extends Authenticator {
    	private String user;
    	private String password;
    	
		/**
		 * @param user
		 * @param passoword
		 */
		public SMTPAuthenticator(String user, String passoword) {
			this.user = user;
			this.password = passoword;
		}
    	
        /**
         * @see javax.mail.Authenticator#getPasswordAuthentication()
         */
        public PasswordAuthentication getPasswordAuthentication() {
        	return new PasswordAuthentication(user, password);
        }
    }
}
