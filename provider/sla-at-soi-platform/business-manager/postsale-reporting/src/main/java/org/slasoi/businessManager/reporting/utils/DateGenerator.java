/**
 * 
 */
package org.slasoi.businessManager.reporting.utils;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 6, 2011
 */
public class DateGenerator {
	
	public static final long SECOND = 1000;
	public static final long MINUTE = SECOND*60;
	public static final long HOUR = MINUTE*60;
	public static final long DAY = HOUR*24;
	public static final long WEEK = DAY*7;
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	public static ArrayList<Date> generateDates(Date fromDate, Date toDate, long offset) {
		ArrayList<Date> dates = new ArrayList<Date>();
		Date currentDate = fromDate;
		
		while (currentDate.getTime() <= toDate.getTime()) {
			dates.add(currentDate);
			
			currentDate.setTime(currentDate.getTime()+offset);
			currentDate = (Date) currentDate.clone();
		}
		
		return dates;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
