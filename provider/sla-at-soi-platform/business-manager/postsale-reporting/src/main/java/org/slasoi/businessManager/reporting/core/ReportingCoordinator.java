/**
 * 
 */
package org.slasoi.businessManager.reporting.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.quartz.SchedulerException;
import org.slaatsoi.business.schema.ReportFormatType;
import org.slasoi.businessManager.reporting.communication.MonitoringResultEventSubscriber;
import org.slasoi.businessManager.reporting.core.FrameworkContext.PropertiesFileNames;
import org.slasoi.businessManager.reporting.parser.ReportingPolicy;
import org.slasoi.businessManager.reporting.parser.ReportingPolicyParser;
import org.slasoi.businessManager.reporting.report.Report;
import org.slasoi.businessManager.reporting.schedule.JobScheduler;
import org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager;
import org.slasoi.businessManager.reporting.store.ReportEntityManager;
import org.slasoi.businessManager.reporting.store.ReportingPolicyEntityManager;
import org.slasoi.slamodel.sla.SLA;
import org.xml.sax.SAXException;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public class ReportingCoordinator {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	// framework properties
	private final static String REPORT_TEMPLATE_FILE = "reporting.report.template.file";
	
	private JobScheduler jobScheduler;
	private MonitoringResultEventSubscriber monitoringResultEventSubscriber;
	private ReportEntityManager reportEntityManager;
	private MonitoringResultEntityManager monitoringResultEntityManager;
	private ReportingPolicyEntityManager reportingPolicyEntityManager;
	private String reportTemplateFile;
	
	/**
	 * Dynamically initiates all entity managers and other properties by
	 * reading at the ./conf/reporting.properties properties file
	 * 
	 * @throws SchedulerException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws InterruptedException 
	 * 
	 */
	public ReportingCoordinator() throws SchedulerException, InstantiationException, IllegalAccessException, ClassNotFoundException, FileNotFoundException, IOException, InterruptedException {
		// retrieves from the framework context the framework properties
		Properties frameworkProperties = FrameworkContextManager.getFrameworkContext().getFrameworkProperties(PropertiesFileNames.FRAMEWORK);
		
		// initiates report entity manager
		reportEntityManager = (ReportEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(FrameworkContext.DatabaseManagers.REPORTS);
		
		// initiates monitoring result entity manager
		monitoringResultEntityManager = (MonitoringResultEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(FrameworkContext.DatabaseManagers.MONITORING_RESULT_EVENTS);
				
		// initiates reporting policy entity manager
		reportingPolicyEntityManager = (ReportingPolicyEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(FrameworkContext.DatabaseManagers.REPORTING_POLICIES);
		
		// initiates monitoring result event subscriber
		monitoringResultEventSubscriber = new MonitoringResultEventSubscriber(monitoringResultEntityManager);
		
		//System.out.println("RRRRRRRRR Coordinatyor " + frameworkProperties.getProperty(REPORT_TEMPLATE_FILE));
		
		monitoringResultEventSubscriber.subscribe();
		
		//reportTemplateFile = ClassLoader.getSystemResource(frameworkProperties.getProperty(REPORT_TEMPLATE_FILE)).getPath();
		reportTemplateFile = System.getenv("SLASOI_HOME") + frameworkProperties.getProperty(REPORT_TEMPLATE_FILE);
		
		//System.out.println("RRRRRRRRR " + reportTemplateFile);
		// initiates the job scheduler
		jobScheduler = new JobScheduler(monitoringResultEntityManager, reportEntityManager);
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param test.org.slasoi.businessManager.reporting.utils
	 * @return
	 */
	public boolean addReportingPolicy(SLA sla) {
		boolean result = false;
		ReportingPolicy[] reportingConfigurations = new ReportingPolicy[0];
		String businessTermXmlSchema = null;
		
		try {
			reportingConfigurations = ReportingPolicyParser.parseSLA(sla, businessTermXmlSchema);
			
			for (ReportingPolicy reportingPolicy : reportingConfigurations) {
				addReportingPolicy(reportingPolicy);
			}
			
			result = true;
		} catch (SAXException e) {
			logger.error(e.getMessage(), e);
		}
		
		return result;
	}
	
	/**
	 * @param slaId
	 * @param reportingPolicyId
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public ArrayList<Report> select(String slaId, String reportingPolicyId, Date fromDate, Date toDate, ReportFormatType format) {
		return reportEntityManager.selectBySlaId(slaId, reportingPolicyId, fromDate, toDate, format.toString());
	}
	
	/**
	 * @param reportingPolicy
	 * @return
	 */
	public boolean addReportingPolicy(ReportingPolicy reportingPolicy) {
		// create a job which it will be executed by the scheduler to produce a report
		reportingPolicyEntityManager.insert(reportingPolicy);
		
		boolean addCreateReport = jobScheduler.addCreateReportJob(reportingPolicy, reportTemplateFile);
		boolean addDeliveryReport = jobScheduler.addDeliverReportJob(reportingPolicy);
		
		return addCreateReport && addDeliveryReport;
	}

	/**
	 * @param slaId
	 * @return
	 */
	public boolean removeSLA(String slaId) {
		return true;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
