/**
 * 
 */
package org.slasoi.businessManager.reporting.schedule;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slasoi.businessManager.reporting.aggregator.functional.FunctionalAggregator;
import org.slasoi.businessManager.reporting.parser.ReportingPolicy;
import org.slasoi.businessManager.reporting.parser.ReportingTarget;
import org.slasoi.businessManager.reporting.store.JobResultEntityManager;
import org.slasoi.businessManager.reporting.store.MonitoringResult;
import org.slasoi.businessManager.reporting.store.MonitoringResultEntityManager;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 16, 2011
 */
public class FetchMonitoringResultsJob implements Job {
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	public static enum DataMapProperties { 
		REPORTING_CONFIGURATION,
		JOB_RESULT_DB_MANAGER,
		MONITORING_RESULT_DB_MANAGER,
		FUNCTIONAL_AGGREGATOR;
	};
	
	/**
	 * 
	 */
	public FetchMonitoringResultsJob() { }
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {		
		JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
		
		// get job properties from job data map
		ReportingPolicy reportingPolicy = (ReportingPolicy) jobDataMap.get(DataMapProperties.REPORTING_CONFIGURATION.toString());
		FunctionalAggregator functionalAggregator = (FunctionalAggregator) jobDataMap.get(DataMapProperties.FUNCTIONAL_AGGREGATOR.toString());
		JobResultEntityManager jobResultEntityManager = (JobResultEntityManager) jobDataMap.get(DataMapProperties.JOB_RESULT_DB_MANAGER.toString());
		MonitoringResultEntityManager monitoringResultEntityManager = (MonitoringResultEntityManager) jobDataMap.get(DataMapProperties.MONITORING_RESULT_DB_MANAGER.toString());
		
		// init parameters to retieve monitoring results
		String slaId = reportingPolicy.getSlaId();
		Date fromDate = jobExecutionContext.getPreviousFireTime();
		Date toDate = jobExecutionContext.getFireTime();
		
		logger.debug("Functional aggregator: " + functionalAggregator);
		logger.debug("Fetching for: " + slaId + " from " + fromDate + " to " + toDate);
		
		// retrieve monitoring results
		ArrayList<MonitoringResult> monitoringResults = monitoringResultEntityManager.select(slaId, fromDate, toDate);
		
		logger.debug("Fetched monitoring results: " + monitoringResults);
		
		JobResult jobResult = null;
		
		// apply functional aggregator if any
		if (functionalAggregator != null) {
			logger.debug("Applying functional aggregator: " + functionalAggregator);
			
			for (ReportingTarget reportingTarget : reportingPolicy.getReportingTargets()) {
				// filter the monitoring results with respect to the reporting targets
				ArrayList<MonitoringResult> filteredMonitoringResults = filterMonitoringResult(monitoringResults, reportingTarget.getSlaId(), reportingTarget.getAgreementTermId(), reportingTarget.getGuaranteedId());
				
				// execute the functional aggregator
				double value = functionalAggregator.execute(filteredMonitoringResults);
				
				// create tje functional aggregator result object
				FunctionalAggregatorResult functionalAggregatorResult = new FunctionalAggregatorResult(
						reportingTarget.getSlaId(),
						reportingTarget.getAgreementTermId(),
						reportingTarget.getGuaranteedId(),
						functionalAggregator.getClass().getSimpleName(),
						value);
				
				// create job result
				jobResult = new JobResult(reportingPolicy, monitoringResults);
				jobResult.getFunctionalAggregatorResults().add(functionalAggregatorResult);
			}
		} else {
			// create job result
			jobResult = new JobResult(reportingPolicy, monitoringResults);
		}
		
		logger.debug("Storing job result: " + jobResult);
		
		// store job result
		jobResultEntityManager.put(jobResult);
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param monitoringResults
	 * @param sldId
	 * @param agreementTermId
	 * @param guaranteedId
	 * @return
	 */
	private ArrayList<MonitoringResult> filterMonitoringResult(ArrayList<MonitoringResult> monitoringResults, String sldId, String agreementTermId, String guaranteedId) {
		ArrayList<MonitoringResult> results = new ArrayList<MonitoringResult>();
		
		for (MonitoringResult monitoringResult : monitoringResults) {
			if (monitoringResult.getSlaId().equals(sldId) && monitoringResult.getAgreementTermId().equals(agreementTermId) && monitoringResult.getGuaranteedId().equals(guaranteedId)) {
				results.add(monitoringResult);
			}
		}
		
		return results;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
