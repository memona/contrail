/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Davide Lorenzoli - Davide.Lorenzoli.1@soi.city.ac.uk, George Spanoudakis - G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */
package org.slasoi.businessManager.reporting.service;

import java.util.ArrayList;
import java.util.Date;

import org.slaatsoi.business.schema.ReportFormatType;
import org.slasoi.businessManager.reporting.report.Report;
import org.slasoi.slamodel.sla.SLA;

/**
 * This interface must be implemented by the post-sale reporting manager service.
 * It provides the methods for querying the generated report historical database
 * and for managing reporting policies.
 * 
 * @author Davide Lorenzoli
 * 
 * @date Feb 24, 2011
 */
public interface BSLAMReportingManagerService {

	/**
	 * Configure the post-sale reporting manager. Post-sale reporting manager
	 * reads reporting policies and sets up reporting generation and delivery
	 * accordingly.
	 * 
	 * @param test.org.slasoi.businessManager.reporting.utils The <code>SLA</code> object containing the reporting policy
	 * @return <code>true</code> if the operation succeed, <code>false</code> otherwise
	 */
	public boolean addSLA(SLA sla);
	
	/**
	 * Remove all reporting policies contained by an already added SLA
	 * whose unique identifier matches the given one.
	 * 
	 * @param slaId The SLA unique identifier
	 * @return <code>true</code> if the operation succeed, <code>false</code> otherwise
	 */
	public boolean removeSLA(String slaId);
	
	/**
	 * Selects, from the reporting manager historical database, already generated
	 * reports matching the selection criteria
	 *  
	 * @param slaId The SLA unique identifier
	 * @param reportingPolicyId The reporting policy unique identifier
	 * @param fromDate The past time point from which generated reports are considered
	 * @param toDate The past time point to which generated reports are considered
	 * @param format The report format type 
	 * @return The generated reports matching the selection criteria
	 */
	public ArrayList<Report> select(String slaId, String reportingPolicyId, Date fromDate, Date toDate, ReportFormatType format);
}
