/**
 * 
 */
package org.slasoi.businessManager.reporting.utils;

import java.io.IOException;
import java.util.ArrayList;

import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.impl.MonitoringEventServiceImpl;

/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 31, 2011
 */
public class MonitoringEventUtils {	
	private static final String BEGIN_TAG = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	private static final String END_TAG = "</ns2:Event>"; 
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static ArrayList<EventInstance> loadMonitoringEvents(String fileName) {
		ArrayList<EventInstance> events = new ArrayList<EventInstance>();
	
		try {
			String eventsXML = XMLUtils.fileToString(fileName);
			events = parseEventsXML2EventInstanceList(eventsXML);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return events;
	}

	/**
	 * @param eventsXML
	 * @return
	 */
	public static ArrayList<String> parseEventsXML2StringList(String eventsXML) {
		ArrayList<String> events = new ArrayList<String>();
		
		int lastBeginTagIndex = 0;
		int lastEndTagIndex = 0;
		
		while (lastBeginTagIndex < eventsXML.length() || lastEndTagIndex < eventsXML.length()) {
			int currentBeginTagIndex = eventsXML.indexOf(BEGIN_TAG, lastBeginTagIndex);
			int currentEndTagIndex = eventsXML.indexOf(END_TAG, lastEndTagIndex) + END_TAG.length();
			
			// if no more events are left
			if (currentBeginTagIndex == -1 || currentEndTagIndex == -1) {
				break;
			}
			
			//System.out.println("currentBeginTagIndex: " + currentBeginTagIndex);
			//System.out.println("currentEndTagIndex: " + currentEndTagIndex);
			
			// get event XML
			String eventXML = eventsXML.substring(currentBeginTagIndex, currentEndTagIndex);
			
			events.add(eventXML);
			
			// move the index to the end of the current event
			lastBeginTagIndex = currentEndTagIndex;
			lastEndTagIndex = currentEndTagIndex;
		}
		
		return events;
	}
	
	/**
	 * @param eventsXML
	 * @return
	 */
	public static ArrayList<EventInstance> parseEventsXML2EventInstanceList(String eventsXML) {
		ArrayList<EventInstance> events = new ArrayList<EventInstance>();
		MonitoringEventServiceImpl service = new MonitoringEventServiceImpl();
		
		for (String eventXML : parseEventsXML2StringList(eventsXML)) {
			try {
				// marshall to EventInstance object
				//events.add((EventInstance) XMLUtils.unmarshall(eventXML, null, EventInstance.class));
				events.add(service.unmarshall(eventXML));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return events;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
