/**
 * 
 */
package org.slasoi.businessManager.reporting.delivery;


/**
 * @author Davide Lorenzoli
 * 
 * @date Mar 3, 2011
 */
public class Message {
	private Object messageContent;
	private Object messageType;
	
	/**
	 * @param messageContent
	 * @param messageType
	 */
	public Message(Object messageContent, Object messageType) {
		this.messageContent = messageContent;
		this.messageType = messageType;
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @return the messageContent
	 */
	public Object getMessageContent() {
		return messageContent;
	}
	
	/**
	 * @return the messageType
	 */
	public Object getMessageType() {
		return messageType;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
