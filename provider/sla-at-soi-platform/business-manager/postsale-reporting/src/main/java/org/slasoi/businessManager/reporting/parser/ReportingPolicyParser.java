/**
 * 
 */
package org.slasoi.businessManager.reporting.parser;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;

import org.apache.log4j.Logger;
import org.slaatsoi.business.schema.BusinessTerm;
import org.slaatsoi.business.schema.ReceiverType;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.CustomAction;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.SLA;
import org.xml.sax.SAXException;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 14, 2011
 */
public class ReportingPolicyParser {
	// Logger
	private static Logger logger = Logger.getLogger(ReportingPolicyParser.class);
	
	private final static String BUSINESS_POLICY_PROPERTY_NAME = "BusinessTermReporting";
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * Parse an SLA object to an array of reporting configuration objects
	 * 
	 * @param test.org.slasoi.businessManager.reporting.utils
	 * @return An array of reporting configuration objects
	 * @throws SAXException 
	 */
	public static ReportingPolicy[] parseSLA(SLA sla, String businessTermXmlSchema) throws SAXException {
		ArrayList<ReportingPolicy> reportingPolicies = new ArrayList<ReportingPolicy>();
		
		String slaId = "";
		String agreementTermId = "";
		String guaranteedTermId = "";
		
		// set the slaId value
		slaId = sla.getUuid().getValue();
		
		logger.debug("Parsing SLA id: " + slaId);
		
		// loop over the agreement terms
		for (AgreementTerm agreementTerm : sla.getAgreementTerms()) {
			// set the agreementTermId value
			agreementTermId = agreementTerm.getId().getValue();
		
			logger.debug("Parsing agreement term id: " + agreementTermId);
			
			// loop over the guaranteed terms
			for (Guaranteed guaranteed : agreementTerm.getGuarantees()) {
				
				// filter by Guaranteed.Action
				if (guaranteed instanceof Guaranteed.Action) {
					Guaranteed.Action action = (Guaranteed.Action) guaranteed;
					
					// set the guaranteedTermId value
					guaranteedTermId = action.getId().getValue();
					
					logger.debug("Parsing guaranteed Id: " + guaranteedTermId);
					
					if (action.getPostcondition() instanceof CustomAction) {
					
						// get custom action
						CustomAction customAction = (CustomAction) action.getPostcondition();
						
						// get the business term reporting
						String xmlBusinessTerm = customAction.getPropertyValue(new STND(BUSINESS_POLICY_PROPERTY_NAME));
						
						logger.debug("Business term found:\n" + xmlBusinessTerm);
						
						// parse the xml to a BusinessTerm object
						BusinessTerm businessTerm = parseBusinessTerm(xmlBusinessTerm, businessTermXmlSchema);
						
						if (businessTerm != null) {
							logger.debug("Report id: " + businessTerm.getReporting().getReportId());
							logger.debug("Description: " + businessTerm.getReporting().getDescription());
							logger.debug("Receivers:");
							for (ReceiverType receiverType : businessTerm.getReporting().getReceivers().getReceiver()) {
								logger.debug("\tDelivery method: " + receiverType.getDeliveryMethod());
								logger.debug("\tEnd point: " + receiverType.getDeliveryEndPoint());
								logger.debug("\tFormat: " + receiverType.getReportFormat());
							}
							logger.debug("Creation schedule: " + businessTerm.getReporting().getReportCreationSchedule());
							logger.debug("Delivery schedule: " + businessTerm.getReporting().getReportDeliverySchedule());
							logger.debug("Target: " + businessTerm.getReporting().getReportingTargets());
							
							
							// add the business term to the list
							reportingPolicies.add(new ReportingPolicy(sla, businessTerm));
						}
					}
				}
			}
		}
		
		return reportingPolicies.toArray(new ReportingPolicy[0]);
	}
	
	
	/**
	 * Parse an XML containing a business term to an instance of
	 * BusinessTerm class.
	 * 
	 * @param xmlBusinessTerm
	 * @return An instance of BusinessTerm class, <code>null</code> if
	 *         the given XML in invalid, e.g., doesn't comply to the expected schema
	 */
	public static BusinessTerm parseBusinessTerm(String xmlBusinessTerm) {
		BusinessTerm businessTerm = null;
		
		try {
			businessTerm = parseBusinessTerm(xmlBusinessTerm, null);
		} catch (SAXException e) {
			logger.error(e.getMessage(), e);
		}
		
		return businessTerm;
	}
	
	/**
	 * Parse an XML containing a business term to an instance of
	 * BusinessTerm class.
	 *  
	 * @param xmlBusinessTerm
	 * @param xmlBusinessTermSchema
	 * @return An instance of BusinessTerm class, <code>null</code> if
	 *         the given XML in invalid, e.g., doesn't comply to the expected schema
	 * @throws SAXException 
	 */
	public static BusinessTerm parseBusinessTerm(String xmlBusinessTerm, String xmlBusinessTermSchema) throws SAXException {
		BusinessTerm businessTerm = null;
		Schema schema = null;
		
		// check for null input
		if (xmlBusinessTerm == null) return businessTerm;
		
		if (xmlBusinessTermSchema != null) {
			//SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.XML_NS_PREFIX);
			//schema = schemaFactory.newSchema(new StreamSource(new StringReader(xmlBusinessTermSchema)));
		}
		
		//logger.debug("Parsing business term:\n" + xmlBusinessTerm);
		
		ByteArrayInputStream xmlContentBytes = new ByteArrayInputStream(xmlBusinessTerm.getBytes());
		
		try {
            // create a JAXBContext
            JAXBContext jc = JAXBContext.newInstance(BusinessTerm.class);
            // create an Unmarshaller
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            //note: setting schema to null will turn validator off
            unmarshaller.setSchema(schema);
            
            businessTerm = (BusinessTerm) unmarshaller.unmarshal(xmlContentBytes);
        } catch( JAXBException e ) {
        	logger.error(e.getMessage(), e);
        }
        
		return businessTerm;
	}
	
	/**
	 * @param businessTerm
	 * @return
	 * @throws SAXException
	 */
	public static String parseBusinessTerm(BusinessTerm businessTerm) throws SAXException {
		StringWriter businessTermXML = new StringWriter();
		
		try {
            // create a JAXBContext
            JAXBContext jc = JAXBContext.newInstance(BusinessTerm.class);
            // create an marshaller
            Marshaller marshaller = jc.createMarshaller();
            
            marshaller.marshal(businessTerm, businessTermXML);
        } catch( JAXBException e ) {
        	logger.error(e.getMessage(), e);
        }
        
        return businessTermXML.toString();
	}
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
}
