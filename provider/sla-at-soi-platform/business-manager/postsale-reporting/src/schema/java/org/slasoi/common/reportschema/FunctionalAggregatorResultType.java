//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2011.05.05 at 06:33:50 PM BST 
//


package org.slasoi.common.reportschema;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				The functional aggregator result summary contains information
 * 				about aggregative functions (max, min, average, etc) computed
 * 				against a set of monitoring results. 
 * 			
 * 
 * <p>Java class for FunctionalAggregatorResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FunctionalAggregatorResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="slaId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="agreementTermId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="guaranteedTermId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="functionalAggregatorId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="aggregateValue" type="{http://www.w3.org/2001/XMLSchema}double" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FunctionalAggregatorResultType")
public class FunctionalAggregatorResultType
    implements Serializable
{

    @XmlAttribute
    protected String slaId;
    @XmlAttribute
    protected String agreementTermId;
    @XmlAttribute
    protected String guaranteedTermId;
    @XmlAttribute
    protected String functionalAggregatorId;
    @XmlAttribute
    protected Double aggregateValue;

    /**
     * Gets the value of the slaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSlaId() {
        return slaId;
    }

    /**
     * Sets the value of the slaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSlaId(String value) {
        this.slaId = value;
    }

    /**
     * Gets the value of the agreementTermId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgreementTermId() {
        return agreementTermId;
    }

    /**
     * Sets the value of the agreementTermId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgreementTermId(String value) {
        this.agreementTermId = value;
    }

    /**
     * Gets the value of the guaranteedTermId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaranteedTermId() {
        return guaranteedTermId;
    }

    /**
     * Sets the value of the guaranteedTermId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaranteedTermId(String value) {
        this.guaranteedTermId = value;
    }

    /**
     * Gets the value of the functionalAggregatorId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFunctionalAggregatorId() {
        return functionalAggregatorId;
    }

    /**
     * Sets the value of the functionalAggregatorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFunctionalAggregatorId(String value) {
        this.functionalAggregatorId = value;
    }

    /**
     * Gets the value of the aggregateValue property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAggregateValue() {
        return aggregateValue;
    }

    /**
     * Sets the value of the aggregateValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAggregateValue(Double value) {
        this.aggregateValue = value;
    }

}
