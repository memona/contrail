/**
 * 
 */
package test.org.slasoi.businessManager.reporting.store;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.slaatsoi.business.schema.ReportFormatType;
import org.slasoi.businessManager.reporting.core.FrameworkContext;
import org.slasoi.businessManager.reporting.core.FrameworkContextManager;
import org.slasoi.businessManager.reporting.report.Report;
import org.slasoi.businessManager.reporting.store.ReportMySQLEntityManager;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 18, 2011
 */
public class TestReportMySQLDatabaseManager extends TestCase {
	static {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		String slasoiHome = System.getenv("SLASOI_HOME");
		if (slasoiHome == null) {
			PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
		} else {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(System.getProperty("file.separator"));
			frameworkConfigurationFolder.append("bmanager-postsale-reporting");
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		}
	}
	
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	private static final String BUSINESS_REPORT_NAME = "business-report-2.0.pdf";
	
	private byte[] reportBytes;
	private ReportMySQLEntityManager reportDatabase;
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	
	protected void setUp() throws Exception {
		reportBytes = loadBusinessReportPdf();
		reportDatabase = (ReportMySQLEntityManager) FrameworkContextManager.getFrameworkContext().getDatabaseManager(FrameworkContext.DatabaseManagers.REPORTS);
	}

	@Test
	public void testInsert() {
		reportDatabase.truncate();
		assertEquals(0, reportDatabase.count());
		
		Report report = new Report("R1", "SLA1", new Date(), ReportFormatType.PDF, reportBytes);
		
		reportDatabase.insert(report);
		
		assertEquals(1, reportDatabase.count());
	}
	
	@Test
	public void testSelectBySlaId() {
		reportDatabase.truncate();
		assertEquals(0, reportDatabase.count());
		
		long minute = 1000*60;
		long currentTime = System.currentTimeMillis();
		
		// add a report every 5 minutes (4 in the past, 4 in the future, and 1 to date)
		reportDatabase.insert(new Report("R1", "SLA1", new Date(currentTime-20*minute), ReportFormatType.PDF, reportBytes));
		reportDatabase.insert(new Report("R1", "SLA1", new Date(currentTime-15*minute), ReportFormatType.PDF, reportBytes));
		reportDatabase.insert(new Report("R1", "SLA1", new Date(currentTime-10*minute), ReportFormatType.PDF, reportBytes));
		reportDatabase.insert(new Report("R1", "SLA1", new Date(currentTime-5*minute), ReportFormatType.PDF, reportBytes));
		reportDatabase.insert(new Report("R1", "SLA1", new Date(currentTime), ReportFormatType.PDF, reportBytes));
		reportDatabase.insert(new Report("R1", "SLA1", new Date(currentTime+5*minute), ReportFormatType.PDF, reportBytes));
		reportDatabase.insert(new Report("R1", "SLA1", new Date(currentTime+10*minute), ReportFormatType.PDF, reportBytes));
		reportDatabase.insert(new Report("R1", "SLA1", new Date(currentTime+15*minute), ReportFormatType.PDF, reportBytes));
		reportDatabase.insert(new Report("R1", "SLA1", new Date(currentTime+20*minute), ReportFormatType.PDF, reportBytes));
		
		assertEquals(9, reportDatabase.count());
		
		ArrayList<Report> reports = reportDatabase.selectBySlaId("SLA1", "R1", new Date(currentTime-20*minute), new Date(currentTime-10*minute), ReportFormatType.PDF.toString());
		
		assertEquals(3, reports.size());
		
		assertEquals("R1", reports.get(0).getReportId());
		assertEquals("SLA1", reports.get(0).getSlaId());
		assertEquals(new Date(currentTime-20*minute).getTime(), reports.get(0).getCreationDate().getTime());
		assertEquals(ReportFormatType.PDF, reports.get(0).getFormat());
		assertEquals(reportBytes.length, ((byte[]) reports.get(0).getContent()).length);
	}
	
	@Test
	public void testSelectNoResult() {
		reportDatabase.truncate();
		assertEquals(0, reportDatabase.count());
		
		Date date = new Date();
		
		Report report = new Report("R1", "SLA1", date, ReportFormatType.PDF, reportBytes);
		
		reportDatabase.insert(report);
		
		assertEquals(1, reportDatabase.count());
		
		ArrayList<Report> reports = reportDatabase.selectBySlaId("SLA1", "R1", new Date(4534864), new Date(543), ReportFormatType.PDF.toString());
		
		assertEquals(0, reports.size());
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	private byte[] loadBusinessReportPdf() throws IOException {
		String fileName = ClassLoader.getSystemResource(BUSINESS_REPORT_NAME).getPath();
		FileInputStream fileInputStream = new FileInputStream(new File(fileName));
		
		byte[] buffer = new byte[fileInputStream.available()];
		
		fileInputStream.read(buffer);
		
		return buffer;
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
