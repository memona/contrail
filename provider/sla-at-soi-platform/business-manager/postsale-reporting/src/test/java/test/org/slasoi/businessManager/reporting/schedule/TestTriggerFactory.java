/**
 * 
 */
package test.org.slasoi.businessManager.reporting.schedule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.slaatsoi.business.schema.BusinessTerm;
import org.slaatsoi.business.schema.RepeatDailyType;
import org.slaatsoi.business.schema.RepeatSecondlyType;
import org.slaatsoi.business.schema.RepeatWeeklyType;
import org.slaatsoi.business.schema.WeekdaysType;
import org.slasoi.businessManager.reporting.aggregator.temporal.EndsAfterOccurrences;
import org.slasoi.businessManager.reporting.aggregator.temporal.EndsNever;
import org.slasoi.businessManager.reporting.aggregator.temporal.EndsOn;
import org.slasoi.businessManager.reporting.aggregator.temporal.TemporalAggregator;
import org.slasoi.businessManager.reporting.aggregator.temporal.TemporalAggregatorFactory;
import org.slasoi.businessManager.reporting.aggregator.temporal.Weekday;
import org.slasoi.businessManager.reporting.aggregator.temporal.Weekend;
import org.slasoi.businessManager.reporting.aggregator.temporal.Yearly;
import org.slasoi.businessManager.reporting.parser.ReportingPolicyParser;
import org.slasoi.businessManager.reporting.schedule.TriggerFactory;
import org.slasoi.businessManager.reporting.utils.XMLUtils;

/**
 * @author Davide Lorenzoli
 * 
 * @date Feb 15, 2011
 */
public class TestTriggerFactory extends TestCase {
	static {
		StringBuilder frameworkConfigurationFolder = new StringBuilder();
		String slasoiHome = System.getenv("SLASOI_HOME");
		if (slasoiHome == null) {
			PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
		} else {
			frameworkConfigurationFolder.append(slasoiHome);
			frameworkConfigurationFolder.append(System.getProperty("file.separator"));
			frameworkConfigurationFolder.append("bmanager-postsale-reporting");
			PropertyConfigurator.configure(frameworkConfigurationFolder + "/log4j.properties");
		}
	}
	
	// logger
	Logger logger = Logger.getLogger(getClass());
	
	private final static String SECONDLY_ENDS_ON_REPORTING_POLICY = "ReportingPolicy-Secondly.xml";
	private final static String SECONDLY_ENDS_AFTER_OCCURRENCES_REPORTING_POLICY = "ReportingPolicy-Secondly-EndsAfterOccurrences.xml";
	private final static String SECONDLY_ENDS_NEVER_REPORTING_POLICY = "ReportingPolicy-Secondly-EndsNever.xml";
	
	private final static String DAILY_ENDS_ON_REPORTING_POLICY = "ReportingPolicy-Daily.xml";
	private final static String DAILY_ENDS_AFTER_OCCURRENCES_REPORTING_POLICY = "ReportingPolicy-Daily-EndsAfterOccurrences.xml";
	private final static String DAILY_ENDS_NEVER_REPORTING_POLICY = "ReportingPolicy-Daily-EndsNever.xml";
	
	private final static String WEEKLY_ENDS_ON_REPORTING_POLICY = "ReportingPolicy-Weekly.xml";
	private final static String WEEKLY_ENDS_AFTER_OCCURRENCES_REPORTING_POLICY = "ReportingPolicy-Weekly-EndsAfterOccurrences.xml";
	private final static String WEEKLY_ENDS_NEVER_REPORTING_POLICY = "ReportingPolicy-Weekly-EndsNever.xml";
	
	private final static String APERIODIC_REPORTING_POLICY = "ReportingPolicy-Aperiodic.xml";
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	// ------------------------------------------------------------------------
	// 								Secondly trigger
	// ------------------------------------------------------------------------
	
	@Test
	public void testCreateSecondlyEndsOnTrigger() throws IOException, DatatypeConfigurationException {
		XMLGregorianCalendar startsOnXml = getXMLGregorianCalendar(getStartsOn().getTime());
		XMLGregorianCalendar endsOnXml = getXMLGregorianCalendar(nextMinute().getTime());
		int repeatEvery = 3;
		
		// load reporting policy from file 
		BusinessTerm businessTerm = getBusinessTerm(SECONDLY_ENDS_ON_REPORTING_POLICY);
		
		// set actual values
		RepeatSecondlyType periodicSchedule = businessTerm.getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly();
		periodicSchedule.getTimePeriod().setStartsOn(startsOnXml);
		periodicSchedule.getTimePeriod().getEnds().getEndsOn().setDateTime(endsOnXml);
		periodicSchedule.setRepeatEvery(repeatEvery);
		
		// creates temporal aggregator
		TemporalAggregator temporalAggregator = TemporalAggregatorFactory.create(periodicSchedule);
		
		ArrayList<Trigger> triggers = TriggerFactory.createTrigger("report-creation", businessTerm.getReporting().getReportId(), temporalAggregator);
		
		for (Trigger trigger : triggers) {
			assertEquals(20, TriggerUtils.computeFireTimes(trigger, null, 100).size());
		}
	}
	
	@Test
	public void testCreateSecondlyEndsAfterOccurrencesTrigger() throws DatatypeConfigurationException, IOException {
		XMLGregorianCalendar startsOnXml = getXMLGregorianCalendar(getStartsOn().getTime());
		int repeatEvery = 1;
		int endsAfterOccurrences = 15;
		
		// load reporting policy from file 
		BusinessTerm businessTerm = getBusinessTerm(SECONDLY_ENDS_AFTER_OCCURRENCES_REPORTING_POLICY);
		
		// set actual values
		RepeatSecondlyType periodicSchedule = businessTerm.getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly();
		periodicSchedule.getTimePeriod().setStartsOn(startsOnXml);
		periodicSchedule.getTimePeriod().getEnds().getEndsAfterOccurrences().setAfterOccurrences(endsAfterOccurrences);
		periodicSchedule.setRepeatEvery(repeatEvery);
		
		// creates temporal aggregator
		TemporalAggregator temporalAggregator = TemporalAggregatorFactory.create(periodicSchedule);
		
		ArrayList<Trigger> triggers = TriggerFactory.createTrigger("report-creation", businessTerm.getReporting().getReportId(), temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			assertEquals(14, TriggerUtils.computeFireTimes(trigger, null, 100).size());
		}
	}
	
	@Test
	public void testCreateSecondlyEndsNeverTrigger() throws Exception {
		XMLGregorianCalendar startsOnXml = getXMLGregorianCalendar(getStartsOn().getTime());
		int repeatEvery = 1;
		
		// load reporting policy from file 
		BusinessTerm businessTerm = getBusinessTerm(SECONDLY_ENDS_NEVER_REPORTING_POLICY);
		
		// set actual values
		RepeatSecondlyType periodicSchedule = businessTerm.getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatSecondly();
		periodicSchedule.getTimePeriod().setStartsOn(startsOnXml);
		periodicSchedule.setRepeatEvery(repeatEvery);
		
		// creates temporal aggregator
		TemporalAggregator temporalAggregator = TemporalAggregatorFactory.create(periodicSchedule);
		
		ArrayList<Trigger> triggers = TriggerFactory.createTrigger("report-creation", businessTerm.getReporting().getReportId(), temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			assertTrue(TriggerUtils.computeFireTimes(trigger, null, 100).size() > 40);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								Daily trigger
	// ------------------------------------------------------------------------
	
	@Test
	public void testCreateDailyEndsOnTrigger() throws DatatypeConfigurationException, IOException {
		XMLGregorianCalendar startsOnXml = getXMLGregorianCalendar(getStartsOn().getTime());
		XMLGregorianCalendar endsOnXml = getXMLGregorianCalendar(nextWeek().getTime());
		int repeatEvery = 1;
		
		// load reporting policy from file 
		BusinessTerm businessTerm = getBusinessTerm(DAILY_ENDS_ON_REPORTING_POLICY);
		
		// set actual values
		RepeatDailyType periodicSchedule = businessTerm.getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatDaily();
		periodicSchedule.getTimePeriod().setStartsOn(startsOnXml);
		periodicSchedule.getTimePeriod().getEnds().getEndsOn().setDateTime(endsOnXml);
		periodicSchedule.setRepeatEvery(repeatEvery);
		
		// creates temporal aggregator
		TemporalAggregator temporalAggregator = TemporalAggregatorFactory.create(periodicSchedule);
		
		ArrayList<Trigger> triggers = TriggerFactory.createTrigger("report-creation", businessTerm.getReporting().getReportId(), temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			assertEquals(7, TriggerUtils.computeFireTimes(trigger, null, 100).size());
		}
	}
	
	@Test
	public void testCreateDailyEndsAfterOccurrencesTrigger() throws DatatypeConfigurationException, IOException {
		XMLGregorianCalendar startsOnXml = getXMLGregorianCalendar(getStartsOn().getTime());
		int repeatEvery = 1;
		int endsAfterOccurrences = 15;
		
		// load reporting policy from file 
		BusinessTerm businessTerm = getBusinessTerm(DAILY_ENDS_AFTER_OCCURRENCES_REPORTING_POLICY);
		
		// set actual values
		RepeatDailyType periodicSchedule = businessTerm.getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatDaily();
		periodicSchedule.getTimePeriod().setStartsOn(startsOnXml);
		periodicSchedule.getTimePeriod().getEnds().getEndsAfterOccurrences().setAfterOccurrences(endsAfterOccurrences);
		periodicSchedule.setRepeatEvery(repeatEvery);
		
		// creates temporal aggregator
		TemporalAggregator temporalAggregator = TemporalAggregatorFactory.create(periodicSchedule);
		
		ArrayList<Trigger> triggers = TriggerFactory.createTrigger("report-creation", businessTerm.getReporting().getReportId(), temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			assertEquals(14, TriggerUtils.computeFireTimes(trigger, null, 100).size());
		}
	}
	
	@Test
	public void testCreateDailyEndsNeverTrigger() throws Exception {
		// current date
		XMLGregorianCalendar startsOnXml = getXMLGregorianCalendar(getStartsOn().getTime());
		
		int repeatEvery = 1;
		
		// load reporting policy from file 
		BusinessTerm businessTerm = getBusinessTerm(DAILY_ENDS_NEVER_REPORTING_POLICY);
		
		// set actual values
		RepeatDailyType periodicSchedule = businessTerm.getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatDaily();
		periodicSchedule.getTimePeriod().setStartsOn(startsOnXml);
		periodicSchedule.setRepeatEvery(repeatEvery);
		
		// creates temporal aggregator
		TemporalAggregator temporalAggregator = TemporalAggregatorFactory.create(periodicSchedule);
		
		ArrayList<Trigger> triggers = TriggerFactory.createTrigger("report-creation", businessTerm.getReporting().getReportId(), temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			assertTrue(TriggerUtils.computeFireTimes(trigger, null, 100).size() > 40);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								Weekday trigger
	// ------------------------------------------------------------------------
	
	@Test
	public void testCreateWeekdayTrigger() throws Exception {
		TemporalAggregator temporalAggregator;
		ArrayList<Trigger> triggers;
		Date startsOn = getStartsOn();
		
		// Every weekday until next month
		logger.debug("Every weekday until next month");
		temporalAggregator = new Weekday(startsOn, new EndsOn(nextMonth()));
		triggers = TriggerFactory.createTrigger("trigger1", "group1", temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			List<Date> dates = TriggerUtils.computeFireTimes(trigger, null, 100);
			logger.debug("Starts on: " + trigger.getStartTime());
			logger.debug("Ends on: " + trigger.getEndTime());
			for (Date date : dates) {
				logger.debug(date);
			}
			
			assertEquals(21, dates.size());
		}
		
		// Every weekday until 15 occurrences
		logger.debug("Every weekday until after occurrences");
		temporalAggregator = new Weekday(startsOn, new EndsAfterOccurrences(12));
		triggers = TriggerFactory.createTrigger("trigger1", "group1", temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			List<Date> dates = TriggerUtils.computeFireTimes(trigger, null, 100);
			logger.debug("Starts on: " + trigger.getStartTime());
			logger.debug("Ends on: " + trigger.getEndTime());
			for (Date date : dates) {
				logger.debug(date);
			}
			
			assertEquals(12, dates.size());
		}
		
		// Every weekday until never
		logger.debug("Every weekday until never");
		temporalAggregator = new Weekday(startsOn, new EndsNever());
		triggers = TriggerFactory.createTrigger("trigger1", "group1", temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			List<Date> dates = TriggerUtils.computeFireTimes(trigger, null, 100);
			logger.debug("Starts on: " + trigger.getStartTime());
			logger.debug("Ends on: " + trigger.getEndTime());
			for (Date date : dates) {
				logger.debug(date);
			}
			
			assertTrue(dates.size() > 40);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								Weekend trigger
	// ------------------------------------------------------------------------
	
	@Test
	public void testCreateWeekendTrigger() throws Exception {
		TemporalAggregator temporalAggregator;
		ArrayList<Trigger> triggers;
		Date startsOn = getStartsOn();
		
		// Every weekend until next month
		logger.debug("Every weekend until next month");
		temporalAggregator = new Weekend(startsOn, new EndsOn(nextMonth()));
		triggers = TriggerFactory.createTrigger("trigger1", "group1", temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			List<Date> dates = TriggerUtils.computeFireTimes(trigger, null, 100);
			logger.debug("Starts on: " + trigger.getStartTime());
			logger.debug("Ends on: " + trigger.getEndTime());
			for (Date date : dates) {
				logger.debug(date);
			}
			
			assertEquals(8, dates.size());
		}
		
		// Every weekend until 15 occurrences
		logger.debug("Every weekend until after occurrences");
		temporalAggregator = new Weekend(startsOn, new EndsAfterOccurrences(12));
		triggers = TriggerFactory.createTrigger("trigger1", "group1", temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			List<Date> dates = TriggerUtils.computeFireTimes(trigger, null, 100);
			logger.debug("Starts on: " + trigger.getStartTime());
			logger.debug("Ends on: " + trigger.getEndTime());
			for (Date date : dates) {
				logger.debug(date);
			}
			
			assertEquals(12, dates.size());
		}
		
		// Every weekend until never
		logger.debug("Every weekday until never");
		temporalAggregator = new Weekday(startsOn, new EndsNever());
		triggers = TriggerFactory.createTrigger("trigger1", "group1", temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			List<Date> dates = TriggerUtils.computeFireTimes(trigger, null, 100);
			logger.debug("Starts on: " + trigger.getStartTime());
			logger.debug("Ends on: " + trigger.getEndTime());
			for (Date date : dates) {
				logger.debug(date);
			}
			
			assertTrue(dates.size() > 40);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								Weekly trigger
	// ------------------------------------------------------------------------
	
	@Test
	public void testCreateWeeklyEndsOnTrigger() throws DatatypeConfigurationException, IOException {
		XMLGregorianCalendar startsOnXml = getXMLGregorianCalendar(getStartsOn().getTime());
		XMLGregorianCalendar endsOnXml = getXMLGregorianCalendar(nextMonth().getTime());
		WeekdaysType[] weekdays = new WeekdaysType[] {WeekdaysType.MONDAY, WeekdaysType.THURSDAY};
		int repeatEvery = 1;
		
		// load reporting policy from file 
		BusinessTerm businessTerm = getBusinessTerm(WEEKLY_ENDS_ON_REPORTING_POLICY);
		
		// set actual values
		RepeatWeeklyType periodicSchedule = businessTerm.getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatWeekly();
		periodicSchedule.getTimePeriod().setStartsOn(startsOnXml);
		periodicSchedule.getTimePeriod().getEnds().getEndsOn().setDateTime(endsOnXml);
		periodicSchedule.getRepeatOn().addAll(Arrays.asList(weekdays));
		periodicSchedule.setRepeatEvery(repeatEvery);
		
		// creates temporal aggregator
		TemporalAggregator temporalAggregator = TemporalAggregatorFactory.create(periodicSchedule);
		
		ArrayList<Trigger> triggers = TriggerFactory.createTrigger("report-creation", businessTerm.getReporting().getReportId(), temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			assertEquals(9, TriggerUtils.computeFireTimes(trigger, null, 100).size());
		}
	}
	
	@Test
	public void testCreateWeeklyEndsAfterOccurrencesTrigger() throws DatatypeConfigurationException, IOException {
		// current date
		XMLGregorianCalendar startsOnXml = getXMLGregorianCalendar(getStartsOn().getTime());
		WeekdaysType[] weekdays = new WeekdaysType[] {WeekdaysType.MONDAY, WeekdaysType.THURSDAY};
		int repeatEvery = 1;
		int endsAfterOccurrences = 15;
		
		// load reporting policy from file 
		BusinessTerm businessTerm = getBusinessTerm(WEEKLY_ENDS_AFTER_OCCURRENCES_REPORTING_POLICY);
		
		// set actual values
		RepeatWeeklyType periodicSchedule = businessTerm.getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatWeekly();
		periodicSchedule.getTimePeriod().setStartsOn(startsOnXml);
		periodicSchedule.getTimePeriod().getEnds().getEndsAfterOccurrences().setAfterOccurrences(endsAfterOccurrences);
		periodicSchedule.setRepeatEvery(repeatEvery);
		
		// creates temporal aggregator
		TemporalAggregator temporalAggregator = TemporalAggregatorFactory.create(periodicSchedule);
		
		ArrayList<Trigger> triggers = TriggerFactory.createTrigger("report-creation", businessTerm.getReporting().getReportId(), temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			assertEquals(15, TriggerUtils.computeFireTimes(trigger, null, 100).size());
		}
	}
	
	@Test
	public void testCreateWeeklyEndsNeverTrigger() throws Exception {
		// current date
		XMLGregorianCalendar startsOnXml = getXMLGregorianCalendar(getStartsOn().getTime());
		
		int repeatEvery = 1;
		
		// load reporting policy from file 
		BusinessTerm businessTerm = getBusinessTerm(WEEKLY_ENDS_NEVER_REPORTING_POLICY);
		
		// set actual values
		RepeatWeeklyType periodicSchedule = businessTerm.getReporting().getReportCreationSchedule().getAutomaticSchedule().getPeriodicSchedule().getRepeatWeekly();
		periodicSchedule.getTimePeriod().setStartsOn(startsOnXml);
		periodicSchedule.setRepeatEvery(repeatEvery);
		
		// creates temporal aggregator
		TemporalAggregator temporalAggregator = TemporalAggregatorFactory.create(periodicSchedule);
		
		ArrayList<Trigger> triggers = TriggerFactory.createTrigger("report-creation", businessTerm.getReporting().getReportId(), temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			assertTrue(TriggerUtils.computeFireTimes(trigger, null, 100).size() > 40);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								Yearly trigger
	// ------------------------------------------------------------------------
	
	@Test
	public void testCreateYearlyTrigger() throws Exception {
		TemporalAggregator temporalAggregator;
		ArrayList<Trigger> triggers;
		Date startsOn = getStartsOn();
		
		// Every year until next month
		logger.debug("Every year until years");
		temporalAggregator = new Yearly(startsOn, new EndsOn(nextTenYear()), 1);
		triggers = TriggerFactory.createTrigger("trigger1", "group1", temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			List<Date> dates = TriggerUtils.computeFireTimes(trigger, null, 100);
			logger.debug("Starts on: " + trigger.getStartTime());
			logger.debug("Ends on: " + trigger.getEndTime());
			for (Date date : dates) {
				logger.debug(date);
			}
			
			assertEquals(10, dates.size());
		}
		
		// Every 2 years until next month
		logger.debug("Every 2 years until years");
		temporalAggregator = new Yearly(startsOn, new EndsOn(nextTenYear()), 2);
		triggers = TriggerFactory.createTrigger("trigger1", "group1", temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			List<Date> dates = TriggerUtils.computeFireTimes(trigger, null, 100);
			logger.debug("Starts on: " + trigger.getStartTime());
			logger.debug("Ends on: " + trigger.getEndTime());
			for (Date date : dates) {
				logger.debug(date);
			}
			
			assertEquals(5, dates.size());
		}
		
		// Every year until occurrences
		logger.debug("Every year until after occurrences");
		temporalAggregator = new Yearly(startsOn, new EndsAfterOccurrences(12), 1);
		triggers = TriggerFactory.createTrigger("trigger1", "group1", temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			List<Date> dates = TriggerUtils.computeFireTimes(trigger, null, 100);
			logger.debug("Starts on: " + trigger.getStartTime());
			logger.debug("Ends on: " + trigger.getEndTime());
			for (Date date : dates) {
				logger.debug(date);
			}
			
			assertEquals(11, dates.size());
		}
		
		// Every year until never
		logger.debug("Every year until never");
		temporalAggregator = new Yearly(startsOn, new EndsNever(), 1);
		triggers = TriggerFactory.createTrigger("trigger1", "group1", temporalAggregator);
		
		// check
		for (Trigger trigger : triggers) {
			List<Date> dates = TriggerUtils.computeFireTimes(trigger, null, 100);
			logger.debug("Starts on: " + trigger.getStartTime());
			logger.debug("Ends on: " + trigger.getEndTime());
			for (Date date : dates) {
				logger.debug(date);
			}
			
			assertTrue(dates.size() > 40);
		}
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	private BusinessTerm getBusinessTerm(String fileName) throws IOException {
		String reportinPolicyFileName = getClass().getClassLoader().getResource(fileName).getPath();
		String xmlReportingPolicy = XMLUtils.fileToString(reportinPolicyFileName);
		
		return ReportingPolicyParser.parseBusinessTerm(xmlReportingPolicy);
	}
	
	/**
	 * @param milliseconds
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private XMLGregorianCalendar getXMLGregorianCalendar(long milliseconds) throws DatatypeConfigurationException {
		// current date
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(milliseconds);
		XMLGregorianCalendar xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		
		return xmlCalendar;
	}
	
	// Temporal operators
	
	private Date getStartsOn() {
		Calendar date = Calendar.getInstance();
		date.set(Calendar.YEAR, 2010);
		date.set(Calendar.MONTH, Calendar.FEBRUARY);
		date.set(Calendar.DAY_OF_MONTH, 1);
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		return date.getTime();
	}

	private Date nextMinute() {
		Calendar date = Calendar.getInstance();
		date.setTime(getStartsOn());
		date.set(Calendar.MINUTE, (date.get(Calendar.MINUTE)+1)%60);
		return date.getTime();
	}
	
	private Date nextHour() {
		Calendar date = Calendar.getInstance();
		date.setTime(getStartsOn());
		date.set(Calendar.HOUR, (date.get(Calendar.HOUR)+1)%24);
		return date.getTime();
	}
	
	private Date nextDay() {
		Calendar date = Calendar.getInstance();
		date.set(Calendar.DAY_OF_YEAR, date.get(Calendar.DAY_OF_YEAR)+1);
		return date.getTime();
	}
	
	private Date nextWeek() {
		Calendar date = Calendar.getInstance();
		date.setTime(getStartsOn());
		date.set(Calendar.WEEK_OF_YEAR, date.get(Calendar.WEEK_OF_YEAR)+1);
		return date.getTime();
	}
	
	private Date nextMonth() {
		Calendar date = Calendar.getInstance();
		date.setTime(getStartsOn());
		date.set(Calendar.MONTH, (date.get(Calendar.MONTH)+1)%12);
		return date.getTime();
	}
	
	private Date nextYear() {
		Calendar date = Calendar.getInstance();
		date.setTime(getStartsOn());
		date.set(Calendar.YEAR, date.get(Calendar.YEAR)+1);
		return date.getTime();
	}
	
	private Date nextTenYear() {
		Calendar date = Calendar.getInstance();
		date.setTime(getStartsOn());
		date.set(Calendar.YEAR, date.get(Calendar.YEAR)+10);
		return date.getTime();
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
