/**
 * 
 */
package test.org.slasoi.businessManager.reporting.utils;

import java.io.IOException;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slasoi.businessManager.reporting.utils.XMLUtils;
import org.slasoi.slamodel.core.ConstraintExpr;
import org.slasoi.slamodel.core.EventExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.TIME;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.Guaranteed.Action;
import org.slasoi.slamodel.sla.Invocation;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.vocab.units;

/**
 * @author Davide Lorenzoli
 * 
 * @date Apr 20, 2011
 */
public class B6SLACreator {
	static {
		PropertyConfigurator.configure(ClassLoader.getSystemResource("./conf/log4j.properties").getPath());
	}
	// logger
	private Logger logger = Logger.getLogger(getClass());
	
	private static final String SECONDLY_REPORTING_POLICY = "ReportingPolicy-Secondly.xml";
	private static final String DAYLY_REPORTING_POLICY = "ReportingPolicy-Daily.xml";
	private static final String WEEKLY_REPORTING_POLICY = "ReportingPolicy-Weekly.xml";
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------
	
	/**
	 * @return
	 */
	public SLA createAperiodicSLA() {
		SLA sla = null;
		
		sla = getSLA();
		
		return sla;
	}
	
	/**
	 * @return
	 */
	public SLA createSecondlyReportingPolicySLA() {
		SLA sla = null;
		
		sla = getSLA();
		sla.setUuid(new UUID("B6-SLA-SecondlyReportingPolicy"));
		Action action = (Action) sla.getAgreementTerm("GuarActions").getGuaranteed("kpiReporting");
		try {
			action.getPostcondition().setPropertyValue(new STND("BusinessTermReporting"), getSecondlyReportingPolicy());
		} catch (IOException e) {
			logger.debug(e.getMessage(), e);
		}
		
		return sla;
	}
	
	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	private SLA getSLA() {
		SLA sla = new SLA();
		
		sla.setAgreedAt(new TIME(Calendar.getInstance()));
		sla.setAgreementTerms(getAgreementTerms());
		sla.setDescr("B6-SLA");
		sla.setEffectiveFrom(new TIME(Calendar.getInstance()));
		sla.setEffectiveUntil(new TIME(Calendar.getInstance()));
		sla.setInterfaceDeclrs(null);
		sla.setParties(null);
		sla.setPropertyValue(new STND("property-1"), "valie-1");
		sla.setTemplateId(new UUID("98989893310"));
		sla.setUuid(new UUID("98989893310"));
		sla.setVariableDeclrs(null);
		
		return sla;
	}

	/**
	 * @return
	 */
	private AgreementTerm[] getAgreementTerms() {
		AgreementTerm[] agreementTerms = new AgreementTerm[2];
		
		AgreementTerm agreementTerm = new AgreementTerm();
		agreementTerm.setId(new ID("GuarStates"));
		agreementTerm.setPrecondition(getPrecondition());
		agreementTerm.setGuarantees(getGuaranteedStates());
		
		agreementTerms[0] = agreementTerm;
		
		agreementTerm = new AgreementTerm();
		agreementTerm.setId(new ID("GuarActions"));
		agreementTerm.setGuarantees(getGuaranteedActions());
		
		agreementTerms[1] = agreementTerm;
		
		return agreementTerms;
	}

	/**
	 * @return
	 */
	private ConstraintExpr getPrecondition() {
		TypeConstraintExpr typeConstraintExpr = new TypeConstraintExpr();
		
		typeConstraintExpr.setValue(new ID("ar"));
		typeConstraintExpr.setDomain(new SimpleDomainExpr(new CONST("3000", new STND("http://www.slaatsoi.org/extension#calls_per_day")), new STND("http://www.slaatsoi.org/coremodel#less_than")));
		
		return typeConstraintExpr;
	}
	
	/**
	 * @return
	 */
	private Guaranteed[] getGuaranteedStates() {
		Guaranteed[] guaranteeds = new Guaranteed[1];
		
		TypeConstraintExpr typeConstraintExpr = new TypeConstraintExpr();
		typeConstraintExpr.setValue(new ID("mean_satisfaction_level"));
		typeConstraintExpr.setDomain(new SimpleDomainExpr(new CONST("3", units.month), new STND("http://www.slaatsoi.org/coremodel#greater_than_or_equals")));
		
		Guaranteed.State state = new Guaranteed.State(new ID("G1"), typeConstraintExpr);
		
		guaranteeds[0] = state;
		
		return guaranteeds;
	}
	
	/**
	 * @return
	 */
	private Guaranteed[] getGuaranteedActions() {
		Guaranteed[] guaranteeds = new Guaranteed[1];
		
		EventExpr precondition = new EventExpr();
		precondition.setOperator(new STND("http://www.slaatsoi.org/coremodel#periodic"));
		precondition.setParameters(new CONST[] {
				new CONST("1", new STND("http://www.slaatsoi.org/coremodel/units#month"))
		});
		
		Invocation postcondition = new Invocation();
		postcondition.setPropertyValue(new STND("BusinessTermReporting"), getAperiodicReportingPolicy());
		postcondition.setEndpointId(new ID("CSCReporting/REPORTING_ID"));
		postcondition.setOperationId(new ID("CSCReporting/report"));
		postcondition.setParameterValue(new ID("MEAN_OP_RESP_TIME"), new ID("mean_op_resp_time"));
		postcondition.setParameterValue(new ID("NUMB_LATE_ARRIVAL"), new ID("numb_late_arrival"));
		postcondition.setParameterValue(new ID("MEAN_HEALTH_BOOK_TIME"), new ID("mean_health_book_time"));
		postcondition.setParameterValue(new ID("NUMB_PROV_TREATMENTS"), new ID("numb_prov_treatments"));
		postcondition.setParameterValue(new ID("O"), new ID("percent_busy_lines"));
		
		Guaranteed.Action action = new Guaranteed.Action(
				new ID("kpiReporting"),
				new ID("http://www.slaatsoi.org/slamodel#provider"),
				new STND("http://www.slaatsoi.org/slamodel#mandatory"),
				precondition,
				postcondition
				);
		
		guaranteeds[0] = action;
		
		return guaranteeds;
	}

	private String getSecondlyReportingPolicy() throws IOException {
		String fileName = getClass().getClassLoader().getResource(SECONDLY_REPORTING_POLICY).getPath();
		return XMLUtils.fileToString(fileName);
	}
	
	private String getDailyReportingPolicy() throws IOException {
		String fileName = getClass().getClassLoader().getResource(DAYLY_REPORTING_POLICY).getPath();
		return XMLUtils.fileToString(fileName);
	}
	
	private String getWeeklyReportingPolicy() throws IOException {
		String fileName = getClass().getClassLoader().getResource(WEEKLY_REPORTING_POLICY).getPath();
		return XMLUtils.fileToString(fileName);
	}
	
	/**
	 * @return
	 */
	private String getAperiodicReportingPolicy() {
		StringBuilder reportingPolicy = new StringBuilder();
		
		reportingPolicy.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
		reportingPolicy.append("<bs:BusinessTerm ");
		reportingPolicy.append("	xmlns:bs=\"http://www.slaatsoi.org/BusinessSchema\"");
		reportingPolicy.append("	xmlns:coremodel=\"http://www.slaatsoi.org/coremodel\"");
		reportingPolicy.append("	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
		reportingPolicy.append("	xsi:schemaLocation=\"http://www.slaatsoi.org/BusinessSchema business-schema.xsd\"");
		reportingPolicy.append(">");
		reportingPolicy.append("	<Reporting>");
		reportingPolicy.append("		<ReportId>R1</ReportId>");
		reportingPolicy.append("		<ReportingTarget>");
		reportingPolicy.append("			<GuaranteedId>GT1.1.1</GuaranteedId>");
		reportingPolicy.append("		</ReportingTarget>");
		reportingPolicy.append("		<FunctionalAggregator>average</FunctionalAggregator>");
		reportingPolicy.append("		<Description>Description</Description>");
		reportingPolicy.append("		<Format>pdf</Format>");
		reportingPolicy.append("		<Type>");
		reportingPolicy.append("			<SLAStatusReport></SLAStatusReport>");
		reportingPolicy.append("		</Type>");
		reportingPolicy.append("		<DeliveryMethod>email</DeliveryMethod>");
		reportingPolicy.append("		<ReportCreationSchedule>");
		reportingPolicy.append("			<AutomaticSchedule>");
		reportingPolicy.append("				<AperiodicFrequency>");
		reportingPolicy.append("					<DateTime>2011-02-22T17:44:00.035Z</DateTime>");
		reportingPolicy.append("				</AperiodicFrequency>");
		reportingPolicy.append("			</AutomaticSchedule>");
		reportingPolicy.append("		</ReportCreationSchedule>");
		reportingPolicy.append("		<ReportDeliverySchedule>");
		reportingPolicy.append("			<AutomaticSchedule>");
		reportingPolicy.append("				<AperiodicFrequency>");
		reportingPolicy.append("					<DateTime>2011-02-22T17:44:00.035Z</DateTime>");
		reportingPolicy.append("				</AperiodicFrequency>");
		reportingPolicy.append("			</AutomaticSchedule>");
		reportingPolicy.append("		</ReportDeliverySchedule>");
		reportingPolicy.append("	</Reporting>");
		reportingPolicy.append("</bs:BusinessTerm>");
		
		return reportingPolicy.toString();
	}
	
	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
	
	public static void main(String[] args) {
		B6SLACreator b6slaCreator = new B6SLACreator();
		
		System.out.println(b6slaCreator.createAperiodicSLA());
	}
}
