CREATE DATABASE `slasoi-bmanager-postsale-reporting`;

CREATE TABLE `slasoi-bmanager-postsale-reporting`.`monitoring_result_events` (
  `sla_Id` varchar(255) NOT NULL,
  `agreement_term_id` varchar(255) NOT NULL,
  `guaranteed_id` varchar(255) NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `event_object` blob NOT NULL,
  PRIMARY KEY (`sla_Id`,`agreement_term_id`,`guaranteed_id`,`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `slasoi-bmanager-postsale-reporting`.`reporting_policies` (
  `sla_id` varchar(45) NOT NULL,
  `reporting_policy_id` varchar(45) NOT NULL,
  `reporting_target_type` varchar(45) DEFAULT NULL,
  `reporting_target_id` varchar(45) DEFAULT NULL,
  `creation_date` bigint(20) NOT NULL,
  `sla_object` blob NOT NULL,
  `business_term_object` blob NOT NULL,
  PRIMARY KEY (`sla_id`,`reporting_policy_id`,`creation_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `slasoi-bmanager-postsale-reporting`.`reports` (
  `report_id` varchar(45) NOT NULL,
  `sla_id` varchar(45) NOT NULL,
  `creation_date` bigint(20) NOT NULL,
  `format` varchar(45) NOT NULL,
  `report_object` blob NOT NULL,
  PRIMARY KEY (`report_id`,`sla_id`,`creation_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;