/**
 * Copyright (c) 2008-2011, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.businessManager.billingEngine.dao.impl;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.slasoi.businessManager.common.dao.impl.AbstractHibernateDAOImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.slasoi.businessManager.billingEngine.dao.PartyBankDAO;
import org.slasoi.businessManager.common.model.billing.EmPartyBank;
import org.slasoi.businessManager.billingEngine.util.BillingConstants;

@Repository(value="partyBankDAO")
public class PartyBankDAOImpl extends AbstractHibernateDAOImpl<EmPartyBank, Long> implements PartyBankDAO {
    private static final Logger log = Logger.getLogger( PartyBankDAOImpl.class );
    
    @Autowired
    public PartyBankDAOImpl(SessionFactory factory)
    {
        setSessionFactory(factory);
    }
    
    @Override
    protected Class<EmPartyBank> getDomainClass() {
            return EmPartyBank.class;
    }
    
    /**
     * Get the account the one party by its id.
     * @param party
     * @return
     */
    public EmPartyBank getPartyBankActiveByParty(Long party)throws Exception{
        log.debug("finding EmPartyBank By PartyID: " +party);
        try {                 
            Criteria criteria = this.getSession().createCriteria(EmPartyBank.class);                                
            criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);                                      
           //To filter by service
            criteria.createAlias("emParty","party",CriteriaSpecification.LEFT_JOIN);
            criteria.add(Restrictions.eq("party.nuPartyid", party));
            List<EmPartyBank> result = criteria.list();
            if(result!=null&&result.size()==1){
                return result.get(0);
            }else if(result!=null&&result.size()>1){
                for(EmPartyBank bank:result){
                    if(bank.getTcDefaultYn()== BillingConstants.CHAR_YES){
                        return bank;
                    }
                }
                return null;
            }else{
                return null; 
            }      
        
        } catch (RuntimeException re) {
            log.error("finding EmPartyBank By PartyID", re);
            throw re;       
        }
    }

}