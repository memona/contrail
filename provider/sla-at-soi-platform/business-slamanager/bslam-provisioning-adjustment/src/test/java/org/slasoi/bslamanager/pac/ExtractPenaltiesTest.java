/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/violation-penalty/src/test/java/org/slasoi/businessManager/violationPenalty/ExtractPenaltiesTest.java $
 */

/**
 * 
 */
package org.slasoi.bslamanager.pac;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Resource;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.model.pac.EmPenalty;
import org.slasoi.businessManager.common.service.SlaUtilService;
import org.slasoi.gslam.syntaxconverter.SLASOIParser;
import org.slasoi.slamodel.sla.SLA;
import org.slasoi.slamodel.vocab.bnf;
import org.springframework.test.AbstractTransactionalSpringContextTests;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */

public class ExtractPenaltiesTest extends AbstractTransactionalSpringContextTests {
    
    private static final Logger logger = Logger.getLogger(ExtractPenaltiesTest.class.getName());
    private static final String BUSINESS_SLA_FILE = "ORC_Business_SLA.xml";
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");
    
    private static final String GUARANTEE_TERM_ID = "ORC_CustomerConstraint_arrival_rate";
    private static final float VALUE = 1;
    private static final String DATATYPE = "EUR";
    private static final String PARTY = "http://www.slaatsoi.org/slamodel#provider";
    private static final double DELTA = 0.1;

    @Resource
    private SlaUtilService slaUtilService;
    
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bslam-pac-context.xml"};
    }
    
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ExtractPenaltiesTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ExtractPenaltiesTest.class );
    }
    
    public void testGetPenalty() {
        SLA sla = null;
        try {
            sla = createSLA();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        EmPenalty penalty = slaUtilService.getPenaltiesFromSLA(sla, GUARANTEE_TERM_ID);

        float value = penalty.getValue();
        // String datatype = penalty.getPrice().getDatatype().toString();

        assertEquals(value, VALUE, DELTA);
        assertEquals(penalty.getObligatedParty(), PARTY);
        // assertEquals(datatype, DATATYPE);
    }

    private SLA createSLA() throws IOException {
        // Read SLA from xml, and convert to Java object (using Syntax Converter)
        String slaXml = readFile("src"+FILE_SEPARATOR+"test"+FILE_SEPARATOR+"resources"+FILE_SEPARATOR+BUSINESS_SLA_FILE);

        SLA sla = null;
        try {
            SLASOIParser slasoiParser = new SLASOIParser();
            sla = slasoiParser.parseSLA(slaXml);

            System.out.println("After syntax converter...");
            System.out.println(sla.toString());
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println(bnf.render(sla, true));

        return sla;
    }

    private String readFile(String file) throws IOException {

        InputStream is = new FileInputStream(file);
        
        byte[] b = new byte[is.available()];
        is.read(b);
        is.close();

        return new String(b);
    }
    
    private String readFile(InputStream is) throws IOException {

        byte[] b = new byte[is.available()];
        is.read(b);
        is.close();

        return new String(b);
    }

}
