/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/violation-penalty/src/test/java/org/slasoi/businessManager/violationPenalty/HibernateUtilTest.java $
 */

package org.slasoi.bslamanager.pac;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.model.pac.EmPenalty;
import org.slasoi.businessManager.common.model.pac.EmSlaViolation;
import org.slasoi.businessManager.common.model.pac.EmSlaWarning;
import org.slasoi.businessManager.common.service.PenaltyManager;
import org.slasoi.businessManager.common.service.SlaViolationManager;
import org.slasoi.businessManager.common.service.SlaWarningManager;
import org.springframework.test.AbstractTransactionalSpringContextTests;

public class PersistenceTest extends AbstractTransactionalSpringContextTests{
 
    private static final Logger logger = Logger.getLogger(PersistenceTest.class.getName());

    private String violationId = "Test001";
    private String gtId = "MiGT";
    private String slaId = "MiSLA";
    private String notificationId = "Notif001";
    private Date   initialDate = new Date(111,3,4);
    
    private Date persistedDate;
    private long penaltyId;
    private static final String PENALTY_DESC = "Penalty";
    private static final String PENALTY_PARTY = "Provider";
    private static final float PENALTY_VALUE = 5;
    
    @Resource
    private SlaViolationManager slaViolationManager =null;
    
    @Resource
    private PenaltyManager penaltyManager =null;
    
    @Resource
    private SlaWarningManager slaWarningManager =null;
    
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bslam-pac-context.xml"};
    }
    
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public PersistenceTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( PersistenceTest.class );
    }
   
    public void testViolationService() {
       
        EmSlaViolation violation = createEmSlaViolation();

        slaViolationManager.saveOrUpdate(violation);
        
        logger.info("violation saved");
        
        List<EmSlaViolation> list = slaViolationManager.getViolationsById(violation.getViolationId());
        
        assertFalse(list.size()==0);
        
        violation = list.get(0);
        
        logger.info("violation read from DB");
        
        assertEquals(this.slaId,violation.getSlaId());
        assertEquals(this.gtId,violation.getGuaranteeTermId());

        logger.info("violation parameters ok");
        
        Format formatter = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z");
        assertEquals(formatter.format(this.initialDate),formatter.format(violation.getInitialTime()));

        logger.info("Deleting violation " + violationId);
    
        slaViolationManager.delete(violation);      
        
        logger.info("\t violation deleted");  

    }

    
    public void testPenaltyService(){

        EmSlaViolation violation = createEmSlaViolation();
        EmPenalty penalty=createEmPenalty(violation);
        
        slaViolationManager.saveOrUpdate(violation);
        penaltyManager.saveOrUpdate(penalty);
        
        penaltyId = penalty.getPenaltyId();
        persistedDate = penalty.getPenaltyDate();
        logger.info("penalty saved");
        
        List<EmPenalty> penalties = penaltyManager.getPenaltiesByViolationsSlatIdOrderedByDate(slaId, true);

        assertNotNull(penalties.size());

        penalty = penalties.get(0);
        
        assertEquals(PENALTY_DESC,penalty.getDescription());
        assertEquals(PENALTY_PARTY,penalty.getObligatedParty());
        assertEquals(penaltyId,penalty.getPenaltyId());
        assertEquals(PENALTY_VALUE,penalty.getValue(), 0.10);

        EmSlaViolation penaltyViolation = penalty.getViolation();
        
        assertEquals(violation.getViolationId(), penaltyViolation.getViolationId());
        assertEquals(violation.getSlaId(), penaltyViolation.getSlaId());
        assertEquals(violation.getGuaranteeTermId(), penaltyViolation.getGuaranteeTermId());

        Format formatter = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z");
        assertEquals(formatter.format(persistedDate),formatter.format(penalty.getPenaltyDate()));
        assertEquals(formatter.format(initialDate),formatter.format(violation.getInitialTime()));
        
        logger.info("Deleting penalty " + penalty.getPenaltyId());
        penaltyManager.delete(penalty);

        logger.info("Deleting violation " + penalty.getViolation().getViolationId());
        slaViolationManager.delete(penalty.getViolation());

    }
      
    public void testWarningService(){
        
        List<EmSlaWarning> warningList = null;
        EmSlaWarning warning = null;
        
        logger.info("Running test...");
       
        slaWarningManager.saveOrUpdate(createEmSlaWarning());
        logger.info("warning saved");
        
        warningList =  slaWarningManager.getWarningsByNotificationId(this.notificationId);
       
        assertNotNull(warningList.size());
        
        logger.info("warning list obtained");
        
        warning =  warningList.get(0);
        
        logger.info("Cheking warning parameters");
        
        assertEquals(this.slaId,warning.getSlaId());

        logger.info("\t slaid correct");
        
        Format formatter = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z");
       
        assertEquals(formatter.format(initialDate),formatter.format(warning.getWarningTime()));
    
        logger.info("\t warning time correct");
        
        slaWarningManager.delete(warning);
        
        logger.info("warning deleted");
    }
    
    private EmSlaWarning createEmSlaWarning(){
        
        EmSlaWarning warning = new EmSlaWarning();
        warning.setNotificationId(notificationId);
        warning.setSlaId(slaId);
        warning.setWarningTime(initialDate);
        
        return warning;
    }
  
    private EmPenalty createEmPenalty(EmSlaViolation violation){
        
        EmPenalty penalty= new EmPenalty();
        penalty.setDescription(PENALTY_DESC);
        penalty.setObligatedParty(PENALTY_PARTY);
        penalty.setPenaltyDate(new Date());
        penalty.setValue(PENALTY_VALUE);
        
        if (violation!=null){
            penalty.setViolation(violation);
        }
        
        return penalty;        
    }
    
    private EmSlaViolation createEmSlaViolation(){

        logger.info("Creating new violation");
        
        EmSlaViolation violation = null;
        violation = new EmSlaViolation();
        violation.setViolationId(this.violationId);
        violation.setSlaId(this.slaId);
        violation.setGuaranteeTermId(this.gtId);
        violation.setInitialTime(this.initialDate);
        
        return violation;
        
    }
    
}