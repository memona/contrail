/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/violation-penalty/src/test/java/org/slasoi/businessManager/violationPenalty/ViolationPenaltyTest.java $
 */

/**
 * 
 */
package org.slasoi.bslamanager.pac;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.log4j.Logger;
import org.slasoi.bslamanager.pac.impl.BusinessProvisioningAdjustmentImpl;
import org.slasoi.businessManager.common.ws.types.AdjustmentNotificationType;
import org.slasoi.businessManager.common.ws.types.EventsType;
import org.slasoi.businessManager.common.ws.types.NotificationType;
import org.slasoi.businessManager.common.ws.types.ViolationEventType;
import org.slasoi.businessManager.violationPenalty.AdjustmentNotificationTypeService;
import org.springframework.test.AbstractTransactionalSpringContextTests;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
public class ViolationPenaltyTest extends AbstractTransactionalSpringContextTests{
   
    private static final Logger logger = Logger.getLogger(ViolationPenaltyTest.class.getName());
    private static final String SLA_ID = "ORCSLA";
    private static final String NOTIF_ID = "Test001";
    private static final String NOTIFIER = "Myself";
    private static final String GUARANTEE_TERM = "ORC_CustomerConstraint_arrival_rate";

    @Resource
    private BusinessProvisioningAdjustmentImpl businessPAC;
    
    @Resource
    private AdjustmentNotificationTypeService adjustmentNotificationTypeService =null;

    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bslam-pac-context.xml"};
    }
    
    /**
     * Create the test case
     * 
     * @param testName
     *            name of the test case
     */
    public ViolationPenaltyTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ViolationPenaltyTest.class);
    }

    public void testViolation() {
        logger.info("testViolation...");
        businessPAC.trackEvent(createAdjustmentNotificationList(EventsType.VIOLATION));

        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private List<AdjustmentNotificationType> createAdjustmentNotificationList(EventsType type) {
        List<AdjustmentNotificationType> list = new ArrayList<AdjustmentNotificationType>();

        if (type == EventsType.VIOLATION){
            list.add(createViolation());
        }

        return list;
    }

    private AdjustmentNotificationType createViolation() {
        AdjustmentNotificationType violation = new AdjustmentNotificationType();
        violation.setNotificationId(NOTIF_ID);
        violation.setType(EventsType.VIOLATION);
        violation.setSlaID(SLA_ID);
        violation.setReportTime(new Date());
        violation.setNotifier(NOTIFIER);

        NotificationType notification = new NotificationType();
        ViolationEventType violationEvent = new ViolationEventType();
        violationEvent.setGuaranteeTerm(GUARANTEE_TERM);
        notification.setViolationEvent(violationEvent);
        violation.setNotification(notification);
        return violation;
    }

    public void testClearEventsDB() {
        AdjustmentNotificationType notif = new AdjustmentNotificationType();
        notif.setNotificationId(NOTIF_ID);
        notif.setType(EventsType.VIOLATION);
        notif.setSlaID(SLA_ID);
        notif.setNotifier(NOTIFIER);

        NotificationType notification = new NotificationType();
        ViolationEventType violationEvent = new ViolationEventType();
        violationEvent.setGuaranteeTerm(GUARANTEE_TERM);
        notification.setViolationEvent(violationEvent);
        notif.setNotification(notification);

        adjustmentNotificationTypeService.delete(notif);

    }

}
