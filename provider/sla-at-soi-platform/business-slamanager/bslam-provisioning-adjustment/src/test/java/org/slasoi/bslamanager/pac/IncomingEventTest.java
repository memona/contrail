/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/violation-penalty/src/test/java/org/slasoi/businessManager/violationPenalty/IncomeEventTest.java $
 */

/**
 * 
 */
package org.slasoi.bslamanager.pac;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.log4j.Logger;
import org.slasoi.bslamanager.pac.impl.BusinessProvisioningAdjustmentImpl;
import org.slasoi.businessManager.common.ws.types.AdjustmentNotificationType;
import org.slasoi.businessManager.common.ws.types.EventsType;
import org.slasoi.businessManager.common.ws.types.NotificationType;
import org.slasoi.businessManager.common.ws.types.RecoveryEventType;
import org.slasoi.businessManager.common.ws.types.ViolationEventType;
import org.slasoi.businessManager.violationPenalty.AdjustmentNotificationTypeService;
import org.springframework.test.AbstractTransactionalSpringContextTests;

/**
 * @author Beatriz Fuentes (TID)
 * 
 */
public class IncomingEventTest extends AbstractTransactionalSpringContextTests{
    
    private static final Logger logger = Logger.getLogger(IncomingEventTest.class);

    @Resource
    private BusinessProvisioningAdjustmentImpl businessPAC;
    
    @Resource
    private AdjustmentNotificationTypeService adjustmentNotificationTypeService =null;
    
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bslam-pac-context.xml"};
    }
    
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public IncomingEventTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite(IncomingEventTest.class );
    }
    
    
    public void testViolation() {
        logger.info("testViolation...");
        
        businessPAC.trackEvent(createAdjustmentNotificationList(EventsType.VIOLATION));

        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void testRecoveryEvent() {
        logger.info("testRecoveryEvent...");
        businessPAC.trackEvent(createAdjustmentNotificationList(EventsType.RECOVERY));

        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void testWarning() {
        logger.info("testWarningEvent...");
        businessPAC.trackEvent(createAdjustmentNotificationList(EventsType.NOTIFICATION));
    }

    private List<AdjustmentNotificationType> createAdjustmentNotificationList(EventsType type) {
        List<AdjustmentNotificationType> list = new ArrayList<AdjustmentNotificationType>();

        if (type == EventsType.VIOLATION)
            list.add(createViolation());
        else if (type == EventsType.RECOVERY)
            list.add(createRecoveryEvent());
        else
            list.add(createNotificationEvent());
        return list;
    }

    private AdjustmentNotificationType createViolation() {
        AdjustmentNotificationType violation = new AdjustmentNotificationType();
        violation.setNotificationId("Test001");
        violation.setType(EventsType.VIOLATION);
        violation.setSlaID("MySLA");
        violation.setReportTime(new Date());
        violation.setNotifier("Myself");

        NotificationType notification = new NotificationType();
        ViolationEventType violationEvent = new ViolationEventType();
        violationEvent.setGuaranteeTerm("MyGT");
        notification.setViolationEvent(violationEvent);
        violation.setNotification(notification);
        return violation;
    }

    private AdjustmentNotificationType createRecoveryEvent() {
        AdjustmentNotificationType recovery = new AdjustmentNotificationType();
        recovery.setNotificationId("Test001");
        recovery.setType(EventsType.RECOVERY);
        recovery.setSlaID("MySLA");
        recovery.setReportTime(new Date());
        recovery.setNotifier("Myself");

        NotificationType notification = new NotificationType();
        RecoveryEventType recoveryEvent = new RecoveryEventType();
        recoveryEvent.setViolationID("Test001");
        notification.setRecoveryEvent(recoveryEvent);
        recovery.setNotification(notification);
        return recovery;
    }

    private AdjustmentNotificationType createNotificationEvent() {
        AdjustmentNotificationType notif = new AdjustmentNotificationType();
        notif.setNotificationId("Test001");
        notif.setType(EventsType.NOTIFICATION);
        notif.setSlaID("MySLA");
        notif.setReportTime(new Date());
        notif.setNotifier("Myself");

        NotificationType notification = new NotificationType();
        List<String> list = new ArrayList<String>();
        list.add("Warning: CPU > 80%");
        notification.setNotificationEvent(list);
        notif.setNotification(notification);
        return notif;
    }

    public void clearEventsDB() {
        AdjustmentNotificationType notif = new AdjustmentNotificationType();
        notif.setNotificationId("Test001");
        notif.setType(EventsType.VIOLATION);
        notif.setSlaID("MySLA");
        notif.setNotifier("Myself");

        NotificationType notification = new NotificationType();
        ViolationEventType violationEvent = new ViolationEventType();
        violationEvent.setGuaranteeTerm("MyGT");
        notification.setViolationEvent(violationEvent);
        notif.setNotification(notification);
        
        
        adjustmentNotificationTypeService.delete(notif);

        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        notif = new AdjustmentNotificationType();
        notif.setNotificationId("Test001");
        notif.setSlaID("MySLA");
        notif.setType(EventsType.NOTIFICATION);
        
        adjustmentNotificationTypeService.delete(notif);

    }

}
