/**
 * Copyright (c) 2008-2010, Telefónica Investigación y Desarrollo, S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Telefónica Investigación y Desarrollo, S.A. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Telefónica Investigación y Desarrollo, S.A. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Juan Lambea - juanlr@tid.es
 * @version        $Rev: 304 $
 * @lastrevision   $Date: 2010-12-05 14:45:45 +0100 (Sun, 05 Dec 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/tags/trunk_bm_refactor_29032011/business-manager/violation-penalty/src/test/java/org/slasoi/businessManager/violationPenalty/AdjustmentNotificationTypeDBManagerTest.java $
 */

package org.slasoi.bslamanager.pac;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.log4j.Logger;
import org.slasoi.businessManager.common.model.pac.EmSlaViolation;
import org.slasoi.businessManager.common.ws.types.AdjustmentNotificationType;
import org.slasoi.businessManager.common.ws.types.EventsType;
import org.slasoi.businessManager.common.ws.types.NotificationType;
import org.slasoi.businessManager.common.ws.types.ViolationEventType;
import org.slasoi.businessManager.violationPenalty.AdjustmentNotificationTypeService;
import org.springframework.test.AbstractTransactionalSpringContextTests;

public class AdjustmentNotificationTypeServiceTest extends AbstractTransactionalSpringContextTests{
    
    private static final Logger logger = Logger.getLogger(AdjustmentNotificationTypeServiceTest.class.getName());

    @Resource
    private AdjustmentNotificationTypeService adjustmentNotificationTypeService =null;
   
   
    private String violationId = null;
    
    private String notificationId = "Test001";
    
    private EventsType type = EventsType.VIOLATION;
    
    private String slaID = "MySLA";
    
    private String gt = "MyGT";
    
    private Date reportTime = new Date(111,3,12,0,0,0);
    
    private String notifier = "Myself";
    
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "META-INF/spring/bslam-pac-context.xml"};
    }
    
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AdjustmentNotificationTypeServiceTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite(AdjustmentNotificationTypeServiceTest.class );
    }
    
    
    public void testSaveViolation() {
        logger.info("Running test...");

        logger.info("\t adjustment notification CREATED");
        AdjustmentNotificationType adjust = createAdjustmentNotificationType();
        
        adjustmentNotificationTypeService.saveOrUpdate(createAdjustmentNotificationType());
            
        logger.info("\t adjustment notification type SAVED");
        
        EmSlaViolation  violation = null;
        
        List<EmSlaViolation> list =
            adjustmentNotificationTypeService.getSLAViolationById(notificationId);

        assertNotNull(list.size());

        logger.info("\t adjustment notification list IS NOT NULL");
        
        violation = (EmSlaViolation) list.get(0);
        
        logger.info("\t checking EmSlaViolation arguments");
        
        assertEquals(notificationId, violation.getViolationId());
        
        logger.info("\t notificationId OK");
        
        assertEquals(slaID,violation.getSlaId());
        
        logger.info("\t slaID OK");
        
        assertEquals(gt, violation.getGuaranteeTermId());

        logger.info("\t gt OK");
        
        assertEquals(reportTime.getTime(),violation.getInitialTime().getTime());

        logger.info("\t reportTime OK");
        
        logger.info("Deleting violation " + adjust.getNotificationId());
        
        adjustmentNotificationTypeService.delete(adjust);
        
    }

    private AdjustmentNotificationType createAdjustmentNotificationType(){
        
            logger.info("Creating new AdjustmentNotificationType violation");
            
            AdjustmentNotificationType violation = new AdjustmentNotificationType();
            violation.setNotificationId("Test001");
            violation.setType(EventsType.VIOLATION);
            violation.setSlaID("MySLA");
            violation.setReportTime(reportTime);
            violation.setNotifier("Myself");

            logger.info("\t creating notification for violation");
            
            NotificationType notification = new NotificationType();
            ViolationEventType violationEvent = new ViolationEventType();
            violationEvent.setGuaranteeTerm(gt);
            notification.setViolationEvent(violationEvent);
            
            logger.info("\t adding notification to violation");
            
            violation.setNotification(notification);
            
            return violation;
    }
    
}
