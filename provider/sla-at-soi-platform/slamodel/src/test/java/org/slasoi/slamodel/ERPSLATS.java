/* 
SVN FILE: $Id: $ 
 
Copyright (c) 2008-2010, Engineering Ingegneria Informatica S.p.A.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Engineering Ingegneria Informatica S.p.A. nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL Engineering Ingegneria Informatica S.p.A. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author         $Author: $
@version        $Rev: $
@lastrevision   $Date: $
@filesource     $URL: $

*/

package org.slasoi.slamodel;

import org.slasoi.slamodel.core.CompoundDomainExpr;
import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.*;
import org.slasoi.slamodel.sla.*;
import org.slasoi.slamodel.service.*;
import org.slasoi.slamodel.vocab.*;

public class ERPSLATS {

	/*
	 * The top-SLAT for ERP hosting shall look as follows:
		1) The basic party is the provider SAP.
		   There is no need to specify the  customer - as we do not have customer agreed actions.
		   (Guarantee states implicitly specify whether they are provider/customer obligations.)
		2) The interface declaration contains 2 different interfaces which are both just described by abstract references 
		   (without detailed operation specification.
		
		The actual terms (guaranteed states) are the following ones:
		3a) A support term where the customer can choose from 2 options, "basic support" or "enterprise support".
		    Both options are just specified by a constant String.
		3b) A customer obligated term, where the customer specifies the load he is putting on the system.
		    This load can be specified via 2 different methods
		    A) specification of a maximal arrival rate (transactions per second)
		    B) specifiaction of maximum number of 'low', 'medium' and 'high' profile users
			==> for sake of simplicity we might model this in 2 different SLATs!
		3c) A provider obligated term of an average response time (90% percentile) of <= 2 seconds
		    measured on an minute basis.
		    This term is fixed and cannot be changed by a customer.
		3d) A provider obligated term of a price value (cost per month).
		    Default value of this might correspond with the default value of the customer load or might be left completely empty.
			
		Note:
		* Guaranteed actions are not yet modelled at all!
	 */
	public static SLATemplate erpSlat;
	
	public static void initialize () {
		erpSlat = new SLATemplate();
		erpSlat.setDescr("A top level SLA for the ERP Hosting use case.");
		// erpSlat.setPropertyValue(new STND("hello"), "world (an example of an annotation)");

        // SLA LEVEL
		/*
        GregorianCalendar cal = new GregorianCalendar(2010,Calendar.MARCH,12,12,0,0);
        this.setAgreedAt(new TIME(cal));
        cal.roll(Calendar.DATE, true);
        this.setEffectiveFrom(new TIME(cal));
        cal.add(Calendar.DATE, 2);
        this.setEffectiveUntil(new TIME(cal));
        this.setTemplateId(new UUID("http://UUID_OF_THE_TEMPLATE_ON_WHICH_THIS_SLA_WAS_BASED"));
        */
		
        // SLA-TEMPLATE LEVEL
		erpSlat.setUuid(new UUID("COM.SAP.RESEARCH.SLASOI.ERPSLAT1"));
        
        // PARTIES
        Party p = new Party(new ID("SAP"), sla.provider);
        /*p.setDescr("describes the 'provider' ...");
        Party.Operative op = new Party.Operative(new ID("OPERATIVE_ID"));
        op.setDescr("describes a party operative/agent ...");
        p.setOperatives(new Party.Operative[]{ op });
        */
        erpSlat.setParties(new Party[]{ p });
        
		
		Interface iface = new InterfaceRef(new UUID("www.sap.com/research/erp/sd"));
		InterfaceDeclr[] ifaces = {new InterfaceDeclr(new ID("ERP-SD"), new ID("SAP"), iface)};
		erpSlat.setInterfaceDeclrs(ifaces);


		// -----------------------------------------------------------------------
		// 2a) Interface specification (ERP-SD, ERP, MM)

		Interface.Specification ispec = new Interface.Specification("www.sap.com/research/erp/sd");
        InterfaceDeclr idec1 = new InterfaceDeclr(new ID("ERP-SD"), sla.provider, ispec);
        // idec1.setDescr("describes an interface offered by the provider ...");
        
        ispec = new Interface.Specification("www.sap.com/research/erp/mm");
        InterfaceDeclr idec2 = new InterfaceDeclr(new ID("ERP-MM"), sla.provider, ispec);
        // idec2.setDescr("describes an interface expected from the customer ...");
        
        erpSlat.setInterfaceDeclrs(new InterfaceDeclr[]{ idec1, idec2 });
        
		// -----------------------------------------------------------------------
        // 2b Service Bundle definition: ERP-All
        
        // BNF syntax: ERP-All is  service_ref { ERP-SD  ERP-MM }
        ServiceRef sref = new ServiceRef();
        sref.setInterfaceDeclrIds(new ID[]{ idec1.getId(), idec2.getId() });
        VariableDeclr service_ref_erpAll = new VariableDeclr(new ID("ERP-All"), sref);
        //service_ref_vdec.setDescr("'SERVICE-REF-1' denotes a collection of interfaces ...");

		// -----------------------------------------------------------------------
        /* 3) Variable declarations
         * 3a) Support term
		        BNF syntax: SupportModel is one-of ( [ standardSupport , enterpriseSupport ] ) <<standardSupport>>
       	 * 3b) Capacity term
        		BNF syntax: Capacity is ( >= 0 tx_per_s and <= 1000 tx_per_s) <<100 tx_per_s>>

        */
        LIST options = new LIST();
        CONST[] cs = new CONST[]{
        	new CONST("standardSupport", null),
        	new CONST("enterpriseSupport", null)
        	};
        for (CONST q : cs) options.add(q);
        Customisable customisable_support = new Customisable(
            new ID("SupportModel"), 
            new SimpleDomainExpr(options, core.member_of), cs[0]
        );
        customisable_support.setDescr("'SupportModel' denotes a customisable value; there are 2 options, with a <<default>> of standardSupport");

        SimpleDomainExpr[] de = new SimpleDomainExpr[] {
        	new SimpleDomainExpr(new CONST("0", units.tx_per_s), core.greater_than_or_equals),
        	new SimpleDomainExpr(new CONST("1000", units.tx_per_s), core.less_than_or_equals),
        };
        Customisable customisable_cap = new Customisable(
                new ID("Capacity"),
                new CompoundDomainExpr(core.and, de), new CONST("100", units.tx_per_s)
            );
        
        erpSlat.setVariableDeclrs(new VariableDeclr[]{ service_ref_erpAll, customisable_support, customisable_cap });
        
		// -----------------------------------------------------------------------
        // 4) AGREEMENT TERMS

        /* 4a Support term declaration
         (a) via a guaranteed state
        BNF syntax
    		agreement_term{id =  SupportTerm
    				   guaranteed_state{id =  SupportState
    									supportFunction ( ERP-All ) = SupportModel
                                }
    					}
    	*/
    					
        AgreementTerm supportTerm = new AgreementTerm(
            new ID("SupportTerm"),
            null, 
            null,
            new Guaranteed[]{
                new Guaranteed.State(
                    new ID("SupportState"),
                    new TypeConstraintExpr(
                    	new FunctionalExpr(new STND("supportFunction"), new ValueExpr[] {service_ref_erpAll.getVar()}),
                        new SimpleDomainExpr(customisable_support.getVar(), core.equals)
                    )
                )
            }
        );
        supportTerm.setDescr("This must be specified by the customer!");

        // (b) via a guaranteed actions => we skip that for the moment as sla/effectiveFrom, sla/effectiveUntil cannot be used in a SLAT!
        /*
        AgreementTerm supportAction = new AgreementTerm(
            new ID("StandardSupportActionTerm"),
            null,
            null,
            new Guaranteed[]{
                new Guaranteed.Action(
                    new ID("StandardSupportActionState"),
                    sla.provider,
                    sla.mandatory,
                    new EventExpr(
                        core.interval, 
                        new Expr[]{ sla.effectiveFrom, sla.effectiveUntil} // new CONST("1", units.month) }
                    ),
                    action
                )
            }
        );
        */

        
		// -----------------------------------------------------------------------
        // 4b Load declaration
        /* BNF syntax
				agreement_term{id =  CapacityTerm
						       guaranteed_state{id =  CapacityState
												arrival_rate ( ERP-All ) <= Capacity
								}
				}
		*/
        AgreementTerm loadTerm = new AgreementTerm(
            new ID("CapacityTerm"),
            null, /*new TypeConstraintExpr( // precondition
                new FunctionalExpr(core.current_day, new ValueExpr[]{}), 
                new SimpleDomainExpr(new CONST("FRIDAY", null), core.equals),
                null
            ),*/
            null,
            new Guaranteed[]{
                new Guaranteed.State(
                    new ID("CapacityState"),
                    new TypeConstraintExpr(
                    	new FunctionalExpr(common.arrival_rate, new ValueExpr[] {service_ref_erpAll.getVar()}),
                        new SimpleDomainExpr(customisable_cap.getVar(), core.less_than_or_equals)
                    )
                )
            }
        );
        loadTerm.setDescr("This must be specified by the customer!");
        
		// -----------------------------------------------------------------------
        // 4c Response time declaration
        /*
         BNF syntax
			agreement_term{id =  ResponseTimeTerm
					       guaranteed_state{id =  ResponseTimeState
											percent ( series ( completion_time ( ERP-All ) , periodic [ 1 min ] ) , <= 2 s ) >= 90%
							}
			}
         */
        AgreementTerm responseTerm = new AgreementTerm(
            new ID("ResponseTimeTerm"),
            null, 
            null,
            new Guaranteed[]{
                new Guaranteed.State(
                    new ID("ResponseTimeState"),
                    new TypeConstraintExpr(
                    	new FunctionalExpr(core.percent, new ValueExpr[] {
                    			new FunctionalExpr(core.series, new ValueExpr[] {
                    					new FunctionalExpr(common.completion_time, new ValueExpr[] {service_ref_erpAll.getVar()}),
                    					new FunctionalExpr(core.periodic, new ValueExpr[] {new CONST("1", units.min)})
                    			}),
                    			new SimpleDomainExpr(new CONST("2", units.s), core.less_than_or_equals) 
                    		}),
                        new SimpleDomainExpr(new CONST("90", units.percentage), core.greater_than_or_equals)
                    )
                )
            }
        );
        responseTerm.setDescr("This response time term is fixed by the provider!");

        
		// -----------------------------------------------------------------------
        // 4d Price term declaration (TBD)

  
        
        erpSlat.setAgreementTerms(new AgreementTerm[]{ supportTerm, loadTerm, responseTerm });

	}

}
