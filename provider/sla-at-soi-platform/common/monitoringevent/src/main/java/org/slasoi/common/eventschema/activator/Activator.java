/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Davide Lorenzoli - Davide.Lorenzoli.1@soi.city.ac.uk, George Spanoudakis - G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.common.eventschema.activator;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slasoi.common.eventschema.EventIdType;
import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.EventPayloadType;

public class Activator implements BundleActivator {

	/**
	// @Override
	public void start(BundleContext context) throws Exception {
		try {
			 Book event = new Book("East of Eden");
			 
			 System.out.println("The result of Book object serialization:");
			 
			 StringWriter sw = new StringWriter();
			 JAXBContext.newInstance(Book.class).createMarshaller().marshal(event, sw);
			 
			 System.out.println("First marshall");
			 System.out.println(sw);
			 
			 Book event2 = (Book) JAXBContext.newInstance(Book.class).createUnmarshaller().unmarshal(new StringReader(sw.toString()));
			
			 System.out.println("Title " + event2.getTitle());
			 System.out.println();
		 } catch (Throwable ex) {
		 ex.printStackTrace();
		 }
	 }
	*/
	
	// @Override
	public void start(BundleContext context) throws Exception {
		try {
			 EventInstance event = new EventInstance();
			 event.setEventID(new EventIdType());
			 event.setEventPayload(new EventPayloadType());
			 
			 System.out.println("The result of EventInstance object serialization:");
			 
			 StringWriter sw = new StringWriter();
			 JAXBContext.newInstance(EventInstance.class).createMarshaller().marshal(event, sw);
			 
			 System.out.println("First marshall");
			 System.out.println(sw);
			 
			 EventInstance event2 = (EventInstance) JAXBContext.newInstance(EventInstance.class).createUnmarshaller().unmarshal(new StringReader(sw.toString()));
			
			 System.out.println("EventIdType " + event2.getEventID() + " EventPayloadType " + event2.getEventPayload());
			 System.out.println();
		 } catch (Throwable ex) {
		 ex.printStackTrace();
		 }
	 }
	
	// @Override
	public void stop(BundleContext context) throws Exception {
	}
}