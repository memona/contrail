/**
 * Copyright (c) 2008-2010, City University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of City University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL City University BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Davide Lorenzoli - Davide.Lorenzoli.1@soi.city.ac.uk, George Spanoudakis - G.Spanoudakis@soi.city.ac.uk
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

/**
 * 
 */
package org.slasoi.common.eventschema.impl;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slasoi.common.eventschema.EventInstance;
import org.slasoi.common.eventschema.service.MonitoringEventService;

/**
 * @author Davide Lorenzoli
 * @email Davide.Lorenzoli.1@soi.city.ac.uk
 * @date Sep 9, 2010
 */
public class MonitoringEventServiceImpl implements MonitoringEventService {
	
	// ------------------------------------------------------------------------
	// 								PUBLIC METHODS
	// ------------------------------------------------------------------------

	/**
	 * @throws JAXBException 
	 * @see org.slasoi.common.eventschema.service.MonitoringEventService#unmarshall(java.lang.String)
	 */
	public EventInstance unmarshall(String eventInstanceXML) throws Exception {
        JAXBContext jc = JAXBContext.newInstance (EventInstance.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller ();

        StringReader stringReader = new StringReader(eventInstanceXML);
       
       EventInstance eventInstance = (EventInstance) ((JAXBElement) unmarshaller.unmarshal(stringReader)).getValue();
       
       return eventInstance;
	}

	/**
	 * @throws JAXBException 
	 * @see org.slasoi.common.eventschema.service.MonitoringEventService#marshall(org.slasoi.common.eventschema.EventInstance)
	 */
	public String marshall(EventInstance eventInstance) throws Exception {		
	    JAXBContext context = JAXBContext.newInstance(eventInstance.getClass());
	    
	    Marshaller marshaller = context.createMarshaller();
	    
	    StringWriter stringWriter = new StringWriter();
	    
	    marshaller.marshal(eventInstance, stringWriter);
	    
	    
	    return stringWriter.toString();
	}

	// ------------------------------------------------------------------------
	// 								PRIVATE METHODS
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// 								ABSTRACT METHODS
	// ------------------------------------------------------------------------
}
