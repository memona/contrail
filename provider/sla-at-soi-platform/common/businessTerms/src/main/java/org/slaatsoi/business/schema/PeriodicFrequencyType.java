//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2011.06.17 at 11:09:27 AM CEST 
//


package org.slaatsoi.business.schema;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PeriodicFrequencyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PeriodicFrequencyType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.slaatsoi.org/BusinessSchema}FrequencyType">
 *       &lt;sequence>
 *         &lt;element name="TimePeriod" type="{http://www.slaatsoi.org/BusinessSchema}TimePeriodType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PeriodicFrequencyType", propOrder = {
    "timePeriod"
})
@XmlSeeAlso({
    RepeatDailyType.class,
    RepeatMinutelyType.class,
    RepeatWeekendType.class,
    RepeatSecondlyType.class,
    RepeatHourlyType.class,
    RepeatMonthlyType.class,
    RepeatYearlyType.class,
    RepeatWeekdayType.class,
    RepeatWeeklyType.class
})
public class PeriodicFrequencyType
    extends FrequencyType
    implements Serializable
{

    @XmlElement(name = "TimePeriod", required = true)
    protected TimePeriodType timePeriod;

    /**
     * Gets the value of the timePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link TimePeriodType }
     *     
     */
    public TimePeriodType getTimePeriod() {
        return timePeriod;
    }

    /**
     * Sets the value of the timePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimePeriodType }
     *     
     */
    public void setTimePeriod(TimePeriodType value) {
        this.timePeriod = value;
    }

}
