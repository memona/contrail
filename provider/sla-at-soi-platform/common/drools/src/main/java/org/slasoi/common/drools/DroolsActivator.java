package org.slasoi.common.drools;

import org.joda.time.format.FormatUtils;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class DroolsActivator implements BundleActivator
{
    public void start( BundleContext context ) throws Exception
    {
        try
        {
            StringBuffer sb = new StringBuffer();
            FormatUtils.appendPaddedInteger( sb, 254742, 10 );
            
            System.out.println( "Drools4OSGi 5.0.1 CRC:  '" + sb.toString() + "' started." );
        }
        catch ( Exception e )
        {
            System.out.println( e );
        }
    }

    public void stop( BundleContext context ) throws Exception
    {
    }
}
