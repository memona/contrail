package org.slasoi.common.messaging.pointtopoint.xmpp;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.pointtopoint.*;
import org.slasoi.common.messaging.pointtopoint.Messaging;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class MessagingTest {
    final String MESSAGING1_CONF_FILE = "src/test/resources/point-to-point-1.properties";
    final String MESSAGING2_CONF_FILE = "src/test/resources/point-to-point-2.properties";
    private static Logger log = Logger.getLogger(MessagingTest.class);
    boolean msgReceived;
    String msgBody;

    @Test
    public void testSendMessage() throws InterruptedException, IOException, MessagingException {
        msgBody = "Some message body";
        msgReceived = false;

        Messaging test1 = MessagingFactory.createMessaging(MESSAGING1_CONF_FILE);
        Messaging test2 = MessagingFactory.createMessaging(MESSAGING2_CONF_FILE);

        test2.addMessageListener(new MessageListener() {
            public void processMessage(MessageEvent messageEvent) {
                log.debug(messageEvent.getMessage().getPayload());
                if (messageEvent.getMessage().getPayload().equals(msgBody)) {
                    msgReceived = true;
                }
            }
        });

        Message message = new Message(test2.getAddress(), msgBody);
        test1.sendMessage(message);

        Thread.sleep(1000);

        test1.close();
        test2.close();
        assertTrue("Message received", msgReceived);
    }
}
