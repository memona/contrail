package org.slasoi.common.messaging.pubsub;

import org.junit.Test;
import org.slasoi.common.messaging.MessagingException;

import java.io.IOException;
import java.util.Properties;

import static org.junit.Assert.assertNotNull;

public class PubSubManagerXmppTest extends PubSubManagerTestCommon {
    final String PUBSUBMANAGER1_CONF_FILE = "src/test/resources/test1.properties";
    final String PUBSUBMANAGER2_CONF_FILE = "src/test/resources/test2.properties";

    @Override
    Properties loadPubSubManager1Properties() throws IOException {
        Properties props = loadPropertiesFile(PUBSUBMANAGER1_CONF_FILE);
        props.setProperty("pubsub", "xmpp");
        return props;
    }

    @Override
    Properties loadPubSubManager2Properties() throws IOException {
        Properties props = loadPropertiesFile(PUBSUBMANAGER2_CONF_FILE);
        props.setProperty("pubsub", "xmpp");
        return props;
    }

    @Test
    public void testCreatePubSubManager() throws IOException, MessagingException {
        PubSubManager pubSubManager = PubSubFactory.createPubSubManager(PUBSUBMANAGER1_CONF_FILE);
        assertNotNull(pubSubManager.getId());
        pubSubManager.close();
    }
}
