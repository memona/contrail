package org.slasoi.common.messaging.pointtopoint;

public class Message extends org.slasoi.common.messaging.Message {
    private static final long serialVersionUID = 1L;

    private String from;
    private String to;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Message() {
        super();
    }

    public Message(String to, String payload) {
        super(payload);
        this.to = to;
    }
}
