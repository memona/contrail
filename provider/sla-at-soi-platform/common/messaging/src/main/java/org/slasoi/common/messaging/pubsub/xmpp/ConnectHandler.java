package org.slasoi.common.messaging.pubsub.xmpp;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;

import java.util.Observable;
import java.util.Observer;

public class ConnectHandler implements ConnectionCreationListener, Observer {
    private static final Logger log = Logger.getLogger(ConnectHandler.class);

    private XMPPConnection connection;
    private Settings settings;

    public ConnectHandler(XMPPConnection connection, Settings settings) {
        this.connection = connection;
        this.settings = settings;
    }

    public void connectionCreated(XMPPConnection connection) {

        if (connection.equals(this.connection)) {
            log.info("Connection created!");

            // Login to server, validate that nodeID/nodeAuth exists, if not
            // create
            log.info("Logging in.");
            login();

            // Verify that pubsub channels are present, setup monitoring
            log.info("Initialising connection.");
        }
    }

    private void login() {

        // Validate that credentials exist - if not create them
        CredentialValidator credentialValidator = new CredentialValidator(connection, settings);
        credentialValidator.addObserver(this);
        credentialValidator.checkCredentials(settings.getSetting(Setting.xmpp_username), settings
                .getSetting(Setting.xmpp_password));

        log.info("Logging in as " + settings.getSetting(Setting.xmpp_username));

        try {
            connection.login(settings.getSetting(Setting.xmpp_username), settings.getSetting(Setting.xmpp_password),
                    settings.getSetting(Setting.xmpp_resource));
        }
        catch (XMPPException loginError) {
            log.error(loginError.getMessage(), loginError);

            return;
        }
        log.info("Connected and logged in... ");
    }

    public void update(Observable observable, Object obj) {

        if (observable instanceof CredentialValidator) {
            settings.setSetting(Setting.xmpp_username, ((CredentialValidator) observable).getUser());
            settings.setSetting(Setting.xmpp_password, ((CredentialValidator) observable).getPassword());
        }
        else {
            log.info("Unrecognised observable event trapped");
        }
    }
}
