package org.slasoi.common.messaging;

import java.io.Serializable;

public abstract class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    private String payload;

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public Message(String payload) {
        this.payload = payload;
    }

    public Message() {
        this("");
    }
}
