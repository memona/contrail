package org.slasoi.common.messaging.pubsub;

public class MessageEvent extends org.slasoi.common.messaging.MessageEvent {

    private static final long serialVersionUID = 1L;

    private PubSubMessage message;

    @Override
    public PubSubMessage getMessage() {
        return message;
    }

    public MessageEvent(Object source, PubSubMessage message) {
        super(source);
        this.message = message;
    }
}
