package org.slasoi.common.messaging;

public class MessagingException extends Exception {

    private static final long serialVersionUID = -3485084987501112376L;

    public MessagingException() {
        super();
    }

    public MessagingException(String message) {
        super(message);
    }

    public MessagingException(Throwable cause) {
        super(cause);
    }

    public MessagingException(String message, Throwable cause) {
        super(message, cause);
    }

}
