package org.slasoi.common.messaging;

/**
 * Node settings.
 */
public enum Setting {
    messaging,
    pubsub,
    encryption,
    xmpp_username,
    xmpp_password,
    xmpp_host,
    xmpp_port,
    xmpp_service,
    xmpp_resource,
    xmpp_pubsubservice,
    xmpp_publish_echo_enabled,
    jms_host,
    jms_port,
    jms_username,
    jms_password,
    amqp_username,
    amqp_password,
    amqp_virtualhost,
    amqp_host,
    amqp_port,
    amqp_federation_forwarding_channel    
}
