package org.slasoi.common.messaging.pubsub;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;

public class PubSubFactory {

    public static PubSubManager createPubSubManager() throws MessagingException {
        Settings settings = new Settings();

        return createPubSubManager(settings);
    }

    public static PubSubManager createPubSubManager(String filePath) throws MessagingException, FileNotFoundException,
            IOException {
        return createPubSubManager(Settings.createSettings(filePath));
    }

    public static PubSubManager createPubSubManager(Properties properties) throws MessagingException {
        return createPubSubManager(Settings.createSettings(properties));
    }

    public static PubSubManager createPubSubManager(Settings settings) throws MessagingException {
        PubSubEngine pubsubEngine = null;
        PubSubEngine[] values = PubSubEngine.values();
        for (int i = 0; i < values.length; i++) {
            if (values[i].toString().toLowerCase().equals(settings.getSetting(Setting.pubsub).toLowerCase())) {
                pubsubEngine = values[i];
            }
        }

        switch (pubsubEngine) {
        case XMPP:
            return new org.slasoi.common.messaging.pubsub.xmpp.PubSubManager(settings);
        case AMQP:
            return new org.slasoi.common.messaging.pubsub.amqp.PubSubManager(settings);
        case JMS:
            return new org.slasoi.common.messaging.pubsub.jms.PubSubManager(settings);
        case IN_MEMORY:
            return new org.slasoi.common.messaging.pubsub.inmemory.PubSubManager(settings);
        }

        return null;
    }
}
