package org.slasoi.common.messaging.pointtopoint;

public class MessageEvent extends org.slasoi.common.messaging.MessageEvent {

    private static final long serialVersionUID = 1L;

    private Message message;

    @Override
    public Message getMessage() {
        return this.message;
    }

    public MessageEvent(Object source, Message message) {
        super(source);
        this.message = message;
    }
}
