package org.slasoi.common.messaging.pointtopoint;

public interface MessageListener extends org.slasoi.common.messaging.MessageListener {

    void processMessage(MessageEvent messageEvent);
}
