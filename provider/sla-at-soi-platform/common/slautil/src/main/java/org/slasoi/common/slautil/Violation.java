package org.slasoi.common.slautil;

import java.util.Date;

import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Guaranteed;

public class Violation
{
	private AgreementTerm agreementTerm;
	private FunctionalExpr functionalExpr;
	private Guaranteed.Action action;
	private Date date;
	private ID party;
	private int penalty;
	private int sumOfPenalties;
	
	public Violation(Environment environment, FunctionalExpr expr, Guaranteed.Action action, Date date, ID party, int penalty, int sumOfPenalties)
	{
		this.functionalExpr = expr;
		this.action = action;
		this.date = date;
		this.agreementTerm = environment.getTerm();
		this.party = party;
		this.penalty = penalty;
		this.sumOfPenalties = sumOfPenalties;
	}
	
	public Date getDate()
	{
		return this.date;
	}
	
	public Guaranteed.Action getAction()
	{
		return this.action;
	}
	
	public AgreementTerm getAgreementTerm()
	{
		return this.agreementTerm;
	}
	
	public ID getPartyID()
	{
		return this.party;
	}
	
	public int getPenalty()
	{
		return this.penalty;		
	}
	
	public int getPenaltySum()
	{
		return this.sumOfPenalties;		
	}
	
	public FunctionalExpr getFunctionalExpr()
	{
		return this.functionalExpr;
	}
	
	public String toString()
	{
		return Integer.toString(sumOfPenalties);
	}
}