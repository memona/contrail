SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `slasoi-everestplus-core` ;
CREATE SCHEMA IF NOT EXISTS `slasoi-everestplus-core` DEFAULT CHARACTER SET latin1 ;
USE `slasoi-everestplus-core` ;

-- -----------------------------------------------------
-- Table `slasoi-everestplus-core`.`distribution`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi-everestplus-core`.`distribution` ;

CREATE  TABLE IF NOT EXISTS `slasoi-everestplus-core`.`distribution` (
  `distribution_id` VARCHAR(255) NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `class_name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`distribution_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi-everestplus-core`.`event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi-everestplus-core`.`event` ;

CREATE  TABLE IF NOT EXISTS `slasoi-everestplus-core`.`event` (
  `event_PK` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `eventId` TEXT NOT NULL ,
  `timestamp` BIGINT(10) UNSIGNED NOT NULL ,
  `ecName` TEXT NOT NULL ,
  `prefix` TEXT NOT NULL ,
  `operationName` TEXT NOT NULL ,
  `partnerId` TEXT NOT NULL ,
  `negated` TINYINT(1) NOT NULL ,
  `abducible` TINYINT(1) NOT NULL ,
  `recordable` TINYINT(1) NOT NULL ,
  `eventObject` BLOB NOT NULL ,
  `eventString` TEXT NOT NULL ,
  PRIMARY KEY (`event_PK`) )
ENGINE = MyISAM
AUTO_INCREMENT = 20001
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi-everestplus-core`.`event_buffer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi-everestplus-core`.`event_buffer` ;

CREATE  TABLE IF NOT EXISTS `slasoi-everestplus-core`.`event_buffer` (
  `event_PK` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `eventId` TEXT NOT NULL ,
  `timestamp` BIGINT(10) UNSIGNED NOT NULL ,
  `ecName` TEXT NOT NULL ,
  `prefix` TEXT NOT NULL ,
  `operationName` TEXT NOT NULL ,
  `partnerId` TEXT NOT NULL ,
  `negated` TINYINT(1) NOT NULL ,
  `abducible` TINYINT(1) NOT NULL ,
  `recordable` TINYINT(1) NOT NULL ,
  `eventObject` BLOB NOT NULL ,
  `eventString` TEXT NOT NULL ,
  PRIMARY KEY (`event_PK`) )
ENGINE = MyISAM
AUTO_INCREMENT = 20001
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi-everestplus-core`.`prediction_policy`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi-everestplus-core`.`prediction_policy` ;

CREATE  TABLE IF NOT EXISTS `slasoi-everestplus-core`.`prediction_policy` (
  `prediction_policy_id` VARCHAR(255) NOT NULL ,
  `sla_id` VARCHAR(255) NOT NULL ,
  `agreement_term_id` VARCHAR(255) NOT NULL ,
  `guaranteed_id` VARCHAR(255) NOT NULL ,
  `qos_id` VARCHAR(255) NOT NULL ,
  `prediction_policy_object` BLOB NOT NULL ,
  PRIMARY KEY (`prediction_policy_id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi-everestplus-core`.`model_factory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi-everestplus-core`.`model_factory` ;

CREATE  TABLE IF NOT EXISTS `slasoi-everestplus-core`.`model_factory` (
  `model_factory_id` VARCHAR(255) NOT NULL ,
  `class_name` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`model_factory_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `slasoi-everestplus-core`.`inferred_distribution`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi-everestplus-core`.`inferred_distribution` ;

CREATE  TABLE IF NOT EXISTS `slasoi-everestplus-core`.`inferred_distribution` (
  `prediction_policy_id` VARCHAR(255) NOT NULL ,
  `distribution_id` VARCHAR(255) NOT NULL ,
  `qos_id` VARCHAR(255) NOT NULL ,
  `parameter_index` INT(11) NOT NULL ,
  `model_factory_id` VARCHAR(255) NOT NULL ,
  `parameter_value` DOUBLE NOT NULL ,
  `goodness_of_fit` DOUBLE NULL DEFAULT NULL ,
  `data_points` INT(11) NULL DEFAULT NULL ,
  CONSTRAINT `fk_inferred_distribution_prediction_policy`
    FOREIGN KEY (`prediction_policy_id` )
    REFERENCES `slasoi-everestplus-core`.`prediction_policy` (`prediction_policy_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_inferred_distribution_distribution1`
    FOREIGN KEY (`distribution_id` )
    REFERENCES `slasoi-everestplus-core`.`distribution` (`distribution_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_inferred_distribution_model_factory1`
    FOREIGN KEY (`model_factory_id` )
    REFERENCES `slasoi-everestplus-core`.`model_factory` (`model_factory_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `fk_inferred_distribution_prediction_policy` ON `slasoi-everestplus-core`.`inferred_distribution` (`prediction_policy_id` ASC) ;

CREATE INDEX `fk_inferred_distribution_distribution1` ON `slasoi-everestplus-core`.`inferred_distribution` (`distribution_id` ASC) ;

CREATE INDEX `fk_inferred_distribution_model_factory1` ON `slasoi-everestplus-core`.`inferred_distribution` (`model_factory_id` ASC) ;


-- -----------------------------------------------------
-- Table `slasoi-everestplus-core`.`prediction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi-everestplus-core`.`prediction` ;

CREATE  TABLE IF NOT EXISTS `slasoi-everestplus-core`.`prediction` (
  `prediction_policy_id` VARCHAR(255) NOT NULL ,
  `timestamp` BIGINT(20) UNSIGNED NOT NULL ,
  `value` DOUBLE NOT NULL ,
  `window` BIGINT(20) UNSIGNED NOT NULL ,
  CONSTRAINT `fk_prediction_prediction_policy1`
    FOREIGN KEY (`prediction_policy_id` )
    REFERENCES `slasoi-everestplus-core`.`prediction_policy` (`prediction_policy_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `fk_prediction_prediction_policy1` ON `slasoi-everestplus-core`.`prediction` (`prediction_policy_id` ASC) ;


-- -----------------------------------------------------
-- Table `slasoi-everestplus-core`.`qos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi-everestplus-core`.`qos` ;

CREATE  TABLE IF NOT EXISTS `slasoi-everestplus-core`.`qos` (
  `sla_id` VARCHAR(255) NOT NULL ,
  `agreement_term_id` VARCHAR(255) NOT NULL ,
  `guaranteed_id` VARCHAR(255) NOT NULL ,
  `qos_id` VARCHAR(255) NOT NULL ,
  `timestamp` BIGINT(20) UNSIGNED NOT NULL ,
  `value` DOUBLE NOT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE INDEX `QOS_specificationId_FK` ON `slasoi-everestplus-core`.`qos` (`sla_id` ASC) ;


-- -----------------------------------------------------
-- Table `slasoi-everestplus-core`.`statistics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `slasoi-everestplus-core`.`statistics` ;

CREATE  TABLE IF NOT EXISTS `slasoi-everestplus-core`.`statistics` (
  `statistics_PK` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `k` DOUBLE NULL DEFAULT NULL COMMENT 'The constant constraint in the constraint expression MTTR (< | <= | == | >= | >) k' ,
  `predictionWindow` DOUBLE NULL DEFAULT NULL COMMENT 'The prediction windows' ,
  `y` DOUBLE NULL DEFAULT NULL COMMENT 'The number of future failures' ,
  `mttrc` DOUBLE NULL DEFAULT NULL COMMENT 'The current MTTR value at when a prediction is computed' ,
  `mttry` DOUBLE NULL DEFAULT NULL COMMENT 'The MTTR value computed for a given value of y' ,
  `prY` DOUBLE NULL DEFAULT NULL COMMENT 'The probability of observing a number of failures euqal to y' ,
  `prMttrc` DOUBLE NULL DEFAULT NULL COMMENT 'The probability of observing an MTTR value euqal to MTTRc' ,
  `prMttry` DOUBLE NULL DEFAULT NULL COMMENT 'The probability of observing an MTTR value equal to MTTRy' ,
  `prediction` DOUBLE NULL DEFAULT NULL COMMENT 'The prediction result' ,
  `mttrf` DOUBLE NULL DEFAULT NULL ,
  `modelHistorySize` INT(11) NULL DEFAULT NULL ,
  `modelGoodness` DOUBLE NULL DEFAULT NULL ,
  `predictionTimestamp` BIGINT(20) NULL DEFAULT NULL ,
  `executionId` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`statistics_PK`) )
ENGINE = MyISAM
AUTO_INCREMENT = 11589
DEFAULT CHARACTER SET = latin1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
