/**
 *  SVN FILE: $Id: ServiceInfoManagerTest.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/test/java/org/slasoi/softwareservicemanager/impl/test/ServiceInfoManagerTest.java $
 */

package org.slasoi.softwareservicemanager.impl.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceConstructionModel;
import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.softwareservicemanager.impl.ServiceInfoManager;
import org.slasoi.softwareservicemanager.provisioning.ServiceState;

/**
 * This class provides unit tests for ServiceInfoManager class.
 * 
 * @author Alexander Wert
 * 
 */
public class ServiceInfoManagerTest {
    /**
     * ServiceInfoManager instance to be tested.
     */
    private static ServiceInfoManager manager;

    /**
     * Builder, which will be created by the ServiceInfoManager.
     */
    private static ServiceBuilder builder;

    /**
     * Creates the static ServiceInfoManager instance needed for all tests.
     */
    @BeforeClass
    public static void setUp() {
        manager = new ServiceInfoManager();
        builder = manager.createBuilder(null, null);
    }

    /**
     * Tests getBuilder function.
     */
    @Test
    public final void testGetBuilder() {
        assertEquals(builder, manager.getBuilder(builder.getUuid()));

        // try to get a builder unknown to the ServiceInfoManager
        // this try should throw an Exception
        boolean exception = false;
        try {
            manager.getBuilder("INVALID_BUILDER");
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);
    }

    /**
     * Tests setter and getter method for ManageabilityAgent.
     */
    @Test
    public final void testGetAndSetManageability() {
        DummyManageabilityAgent agent = new DummyManageabilityAgent();
        // try to get the manageabilityAgent for a builder for
        // which no manageabilityAgent is known to the ServiceInfoManager
        boolean exception = false;
        try {
            manager.getManageabilityAgent(builder.getUuid());
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);

        // try to get the manageabilityAgent for an unknown builder
        exception = false;
        try {
            manager.getManageabilityAgent("INVALID_BUILDER");
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);

        // try to set the manageabilityAgent for an unknown builder

        exception = false;
        try {
            manager.setManageabilityAgent("INVALID_BUILDER", agent);
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);

        manager.setManageabilityAgent(builder.getUuid(), agent);
        assertEquals(agent, manager.getManageabilityAgent(builder.getUuid()));

    }

    /**
     * Tests setter and getter methods for ServiceInstance.
     */
    @Test
    public final void testSetAndGetServiceInstance() {
        ServiceInstance instance = ServiceConstructionModel.getFactory().createServiceInstance();
        instance.setID(null);
        // try to set a service instance with empty id
        // an exception should be thrown
        boolean exception = false;
        try {
            manager.setServiceInstance(instance);
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);

        // try to set a service instance with unknown id to ServiceInfoManager
        // an exception should be thrown
        instance.setID("INVALID_BUILDER");
        exception = false;
        try {
            manager.setServiceInstance(instance);
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);

        // try to get the service instance
        // of a builder without an existing service instance
        // an exception should be thrown
        exception = false;
        try {
            manager.getServiceInstance(builder.getUuid());
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);

        // try to get the service instance of an unknown builder
        // an exception should be thrown
        exception = false;
        try {
            manager.getServiceInstance("INVALID_BUILDER");
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);

        // try to set a valid instance for a valid builder
        instance.setID(builder.getUuid());
        manager.setServiceInstance(instance);
        assertEquals(instance, manager.getServiceInstance(builder.getUuid()));
    }

    /**
     * Tests setter and getter methods for the ServiceStatus.
     */
    @Test
    public final void testGetAndSetServiceStatus() {
        // try to get the status of a invalid builder
        boolean exception = false;
        try {
            manager.getServiceStatus("INVALID_BUILDER");
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);

        // try to set the status of a invalid builder
        exception = false;
        try {
            manager.setServiceStatus("INVALID_BUILDER", ServiceState.eNONE);
        } catch (Exception e) {
            exception = true;
        }
        assertTrue(exception);

        manager.setServiceStatus(builder.getUuid(), ServiceState.eSERVICE_RUNNING);
        assertEquals(ServiceState.eSERVICE_RUNNING, manager.getServiceStatus(builder.getUuid()));

    }
}
