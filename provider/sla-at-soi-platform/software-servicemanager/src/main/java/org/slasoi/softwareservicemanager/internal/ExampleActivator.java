/**
 *  SVN FILE: $Id: ExampleActivator.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/internal/ExampleActivator.java $
 */

package org.slasoi.softwareservicemanager.internal;

import java.net.URL;
import java.util.Dictionary;
import java.util.Properties;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slasoi.softwareservicemanager.IManageSoftwareServices;
import org.slasoi.softwareservicemanager.IPrepareSoftwareService;
import org.slasoi.softwareservicemanager.IProvisioning;
import org.slasoi.softwareservicemanager.impl.SoftwareServiceManagerFacade;
import org.slasoi.softwareservicemanager.provisioning.ProvisionServiceStub;

/**
 * Extension of the default OSGi bundle activator.
 */
public final class ExampleActivator implements BundleActivator {
    /**
     * Called whenever the OSGi framework starts our bundle.
     * 
     * @param bc
     *            BundleContext which should be started.
     * @throws Exception
     *             Throws exception if starting the bundle fails.
     */
    public void start(final BundleContext bc) throws Exception {
        System.out.println("STARTING SoftwareServiceManager");

        // Adjust for your puposes
        String landscapePath = "ORC.scm";
        IProvisioning provisioningManager = new ProvisionServiceStub();

        Dictionary props = new Properties();
        URL url = SoftwareServiceManagerFacade.class.getClassLoader().getResource(landscapePath);
        SoftwareServiceManagerFacade ssm = SoftwareServiceManagerFacade.createSoftwareServiceManager(url.toString(),
                provisioningManager);

        // Register our example service implementation in the OSGi service registry
        System.out.println("REGISTER SoftwareServiceManager instance for the ORC.");
        bc.registerService(IManageSoftwareServices.class.getName(), (IManageSoftwareServices) ssm, props);
        bc.registerService(IPrepareSoftwareService.class.getName(), (IPrepareSoftwareService) ssm, props);
    }

    /**
     * Called whenever the OSGi framework stops our bundle.
     * 
     * @param bc
     *            BundleContext which should be stopped.
     * @throws Exception
     *             Throws exception if stpping the bundle fails.
     */
    public void stop(final BundleContext bc) throws Exception {
        System.out.println("STOPPING SoftwareServiceManager");
        // no need to unregister our service -
        // the OSGi framework handles it for us
    }
}
