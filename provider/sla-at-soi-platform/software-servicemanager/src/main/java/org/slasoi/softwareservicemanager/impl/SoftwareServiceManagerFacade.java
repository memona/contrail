/**
 *  SVN FILE: $Id: SoftwareServiceManagerFacade.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/impl/SoftwareServiceManagerFacade.java $
 */

package org.slasoi.softwareservicemanager.impl;

import java.util.Date;
import java.util.List;
import java.util.Timer;

import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Settings;
import org.slasoi.models.scm.ConfigurationDirective;
import org.slasoi.models.scm.ISoftwareServiceManager;
import org.slasoi.models.scm.Landscape;
import org.slasoi.models.scm.ServiceBinding;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceConstructionModel;
import org.slasoi.models.scm.ServiceConstructionModelFactory;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.models.scm.ServiceType;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.softwareservicemanager.IProvisioning;
import org.slasoi.softwareservicemanager.ISoftwareServiceManagerFacade;
import org.slasoi.softwareservicemanager.exceptions.BookingException;
import org.slasoi.softwareservicemanager.exceptions.ReservationException;
import org.slasoi.softwareservicemanager.exceptions.ServiceStartupException;
import org.slasoi.softwareservicemanager.management.IEffectorAction;
import org.slasoi.softwareservicemanager.management.IEffectorResult;
import org.slasoi.softwareservicemanager.management.IManageabilityAgentFacade;
import org.slasoi.softwareservicemanager.management.SensorSubscriptionData;
import org.slasoi.softwareservicemanager.provisioning.ServiceState;
import org.slasoi.softwareservicemanager.provisioning.StartServiceTask;
import org.slasoi.softwareservicemanager.provisioning.StopServiceTask;

/**
 * 
 * @author Alexander Wert
 * 
 */
public class SoftwareServiceManagerFacade implements ISoftwareServiceManagerFacade, ISoftwareServiceManager {

    /**
     * Booking manager instance booking tasks are delegated to.
     */
    private BookingManager bookingManager;
    /**
     * Landscape describing available ServiceTypes, ServiceImplementations and their features.
     */
    private Landscape landscape;
    /**
     * ServiceInfoManager manages all information for created ServiceBuilder objects.
     */
    private ServiceInfoManager serviceInfoManager;
    /**
     * ProvisioningManager is responsible for starting, stopping and updating/adjusting ServiceInstances for a service.
     */
    private IProvisioning provisioningManager;

    /**
     * Static Function for creating a SoftwareServiceManagerFacade.
     * 
     * @param landscapePath
     *            Path to the landscape file, representing a Landscape instance.
     * @param provisioningManager
     *            Instance of a provisioning manager.
     * @return New SoftwareServiceManagerFacade object.
     */
    public static SoftwareServiceManagerFacade createSoftwareServiceManager(final String landscapePath,
            final IProvisioning provisioningManager) {
        Landscape landscape = (Landscape) ServiceConstructionModel.loadFromXMI(landscapePath);
        return createSoftwareServiceManager(landscape, provisioningManager);
    }

    /**
     * Static Function for creating a SoftwareServiceManagerFacade.
     * 
     * @param landscape
     *            Landscape instance describing available ServiceTypes, ServiceImplementations and their features.
     * @param provisioningManager
     *            Instance of a provisioning manager.
     * @return New SoftwareServiceManagerFacade object.
     */
    public static SoftwareServiceManagerFacade createSoftwareServiceManager(final Landscape landscape,
            final IProvisioning provisioningManager) {
        return new SoftwareServiceManagerFacade(landscape, provisioningManager);
    }

    /**
     * Private constructor.
     * 
     * @param scmLandscape
     *            A Landscape instance the serviceManager has to know about.
     * @param provisioningManager
     *            A provisioningManager instance the serviceManager should use for provisioning tasks.
     */
    private SoftwareServiceManagerFacade(final Landscape scmLandscape, final IProvisioning provisioningManager) {
        this.landscape = scmLandscape;
        this.bookingManager = new BookingManager();
        this.serviceInfoManager = new ServiceInfoManager();
        this.provisioningManager = provisioningManager;
    }

    /**
     * Public Constructor for creating a dummy SoftwareServiceManagerFacade.
     */
    public SoftwareServiceManagerFacade() {
        this.bookingManager = new BookingManager();
        this.serviceInfoManager = new ServiceInfoManager();
    }

    /**
     * Getter for private BookingManager instance.
     * 
     * @return BookingManger instance the SoftwareServiceManagerfacade uses.
     */
    public final BookingManager getBookingManager() {
        return bookingManager;
    }

    /**
     * Setter for private BookingManager instance.
     * 
     * @param bookingManager
     *            New BookingManager for SoftwareServiceManagerFacade.
     */
    public final void setBookingManager(final BookingManager bookingManager) {
        this.bookingManager = bookingManager;
    }

    /**
     * Getter for private Landscape instance.
     * 
     * @return Landscape instance the SoftwareServiceManagerFacade uses.
     */
    public final Landscape getLandscape() {
        return landscape;
    }

    /**
     * Setter for private Landscape instance.
     * 
     * @param landscape
     *            New Landscape instance for SoftwareServiceManagerFacade.
     */
    public final void setLandscape(final Landscape landscape) {
        this.landscape = landscape;
    }

    /**
     * Getter for private IProvisioning instance.
     * 
     * @return IProvisioning instance the SoftwareServiceManagerFacade uses.
     */
    public final IProvisioning getProvisioningManager() {
        return provisioningManager;
    }

    /**
     * Setter for private IProvisioning instance.
     * 
     * @param provisioningManager
     *            New IProvisioning instance for SoftwareServiceManagerFacade.
     */
    public final void setProvisioningManager(final IProvisioning provisioningManager) {
        this.provisioningManager = provisioningManager;
    }

    /**
     * @param builder
     *            ServiceBuilder instance of interest.
     * @return Returns the state of the passed ServiceBuilder object.
     */
    public final ServiceState queryServiceStatus(final ServiceBuilder builder) {
        return queryServiceStatus(builder.getUuid());
    }

    /**
     * * @param serviceID Identifier of the service/ServiceBuilder instance of interest.
     * 
     * @return Returns the state of the passed ServiceBuilder object.
     */
    public final ServiceState queryServiceStatus(final String serviceID) {
        validate();
        return serviceInfoManager.getServiceStatus(serviceID);
    }

    /**
     * Books the service described by the passed builder.
     * 
     * @param builder
     *            ServiceBuilder instance describing the service.
     * @return Returns a list of endpoints the booked service can be accessed through.
     * @throws BookingException
     *             this exception is thrown if booking fails.
     */
    public final List<Endpoint> book(final ServiceBuilder builder) throws BookingException {
        validate();
        bookingManager.book(builder);
        this.serviceInfoManager.setServiceStatus(builder.getUuid(), ServiceState.eRESOURCES_BOOKED);
        return provisioningManager.getEndpoints(builder);
    }

    /**
     * Books the service described by the passed builder.
     * 
     * @param serviceID
     *            Identifier of the ServiceBuilder instance describing the service.
     * @return Returns a list of endpoints the booked service can be accessed through.
     * @throws BookingException
     *             this exception is thrown if booking fails.
     */
    public final List<Endpoint> book(final String serviceID) throws BookingException {
        return book(getBuilder(serviceID));
    }

    /**
     * Cancels the booking for the passed builder object.
     * 
     * @param builder
     *            ServiceBuilder instance the booking should be canceled for.
     * @return Returns true if canceling succeeds.
     * @throws BookingException
     *             Throws an BookingException if an error occurs during canceling the service booking.
     */
    public final boolean cancelBooking(final ServiceBuilder builder) throws BookingException {
        validate();
        checkState(builder.getUuid(), ServiceState.eRESOURCES_BOOKED);
        boolean result = bookingManager.cancelBooking(builder);
        if (result) {
            this.serviceInfoManager.setServiceStatus(builder.getUuid(), ServiceState.eBUILDER_CREATED);
        }
        return result;
    }

    /**
     * Cancels the booking for the passed builder object.
     * 
     * @param serviceID
     *            Identifier of the ServiceBuilder instance the booking should be canceled for.
     * @return Returns true if canceling succeeds.
     * @throws BookingException
     *             Throws an BookingException if an error occurs during canceling the service booking.
     */
    public final boolean cancelBooking(final String serviceID) throws BookingException {
        return cancelBooking(getBuilder(serviceID));
    }

    /**
     * Cancels reservation for passed ServiceBuilder instance.
     * 
     * @param builder
     *            ServiceBuilder instance of interest.
     * @return Returns true if canceling the reservation succeeds.
     * @throws ReservationException
     *             Throws an ReservationException if an error occurs during canceling the service reservation.
     */
    public final boolean cancelReservation(final ServiceBuilder builder) throws ReservationException {
        validate();
        checkState(builder.getUuid(), ServiceState.eRESOURCES_RESERVED);
        boolean result = bookingManager.cancelReservation(builder);
        if (result) {
            this.serviceInfoManager.setServiceStatus(builder.getUuid(), ServiceState.eBUILDER_CREATED);
        }
        return result;
    }

    /**
     * Cancels reservation for passed ServiceBuilder instance.
     * 
     * @param serviceID
     *            Identifier of the ServiceBuilder instance of interest.
     * @return Returns true if canceling the reservation succeeds.
     * @throws ReservationException
     *             Throws an ReservationException if an error occurs during canceling the service reservation.
     */
    public final boolean cancelReservation(final String serviceID) throws ReservationException {
        return cancelReservation(getBuilder(serviceID));
    }

    /**
     * @param implementation
     *            ServiceImplementation instance of interest.
     * @return Returns the capacity of available resources for the passed ServiceImplementation instance.
     */
    public final int capacityCheck(final ServiceImplementation implementation) {
        validate();
        return bookingManager.capacityCheck(implementation);
    }

    /**
     * Reserves resources for the service described by the passed ServiceBuilder object.
     * 
     * @param builder
     *            ServiceBuilder instance the reservation should be done for.
     * @param from
     *            Start time of reservation period.
     * @param until
     *            End time of the reservation period.
     * @return Returns true if reservation succeeds.
     * @throws ReservationException
     *             Throws a ReservationException if reservation fails.
     */
    public final boolean reserve(final ServiceBuilder builder, final Date from, final Date until)
            throws ReservationException {
        validate();
        checkState(builder.getUuid(), ServiceState.eBUILDER_CREATED);
        boolean reserved = bookingManager.reserve(builder, from, until);
        if (reserved) {
            this.serviceInfoManager.setServiceStatus(builder.getUuid(), ServiceState.eRESOURCES_RESERVED);
        }
        return reserved;
    }

    /**
     * Updates reservation information for the service described by the passed ServiceBuilder object.
     * 
     * @param builder
     *            ServiceBuilder instance the reservation should be done for.
     * @param from
     *            Start time of reservation period.
     * @param until
     *            End time of the reservation period.
     * @return Returns true if reservation update succeeds.
     * @throws ReservationException
     *             Throws a ReservationException if reservation update fails.
     */
    public final boolean updateReservation(final ServiceBuilder builder, final Date from, final Date until)
            throws ReservationException {
        validate();
        checkState(builder.getUuid(), ServiceState.eRESOURCES_RESERVED);
        return bookingManager.updateReservation(builder, from, until);
    }

    /**
     * Updates reservation information for the service described by the passed ServiceBuilder object.
     * 
     * @param serviceID
     *            Identifier of the ServiceBuilder instance the reservation should be done for.
     * @param from
     *            Start time of reservation period.
     * @param until
     *            End time of the reservation period.
     * @return Returns true if reservation update succeeds.
     * @throws ReservationException
     *             Throws a ReservationException if reservation update fails.
     */
    public final boolean updateReservation(final String serviceID, final Date from, final Date until)
            throws ReservationException {
        return updateReservation(getBuilder(serviceID), from, until);
    }

    /**
     * Creates a new ServiceBuidler object for the passed ServiceImplementation.
     * 
     * @param implementation
     *            Target ServiceImplementation instance.
     * @return New instance of a ServiceBuilder.
     * 
     * 
     */
    public final ServiceBuilder createBuilder(final ServiceImplementation implementation) {
        validate();
        boolean validImpl = false;
        for (ServiceType type : this.queryServiceTypes()) {
            for (ServiceImplementation impl : this.queryServiceImplementations(type)) {
                if (impl.getServiceImplementationName().equals(implementation.getServiceImplementationName())) {
                    validImpl = true;
                }
            }
        }

        if (!validImpl) {
            throw new IllegalArgumentException("The service implementation '" + implementation
                    + "' is not supported by this instance of SoftwareServiceManager!");
        }
        // System.out.println("*** " +
        // landscape.getProvidedTypes()[0].getServiceTypeName());
        return serviceInfoManager.createBuilder(implementation, this);
    }

    /**
     * Returns a list of service implementations for the passed service type.
     * 
     * @param type
     *            Specifies the type of the implementations.
     * @return Returns a list of service implementations.
     */
    public final List<ServiceImplementation> queryServiceImplementations(final ServiceType type) {
        validate();
        return landscape.queryServiceImplementations(type);
    }

    /**
     * @return Returns a list of all service types known to the landscape of the SoftwareServiceManagerFacade.
     */
    public final List<ServiceType> queryServiceTypes() {
        validate();
        return landscape.queryServiceTypes();
    }

    /**
     * Creates and starts provisioning of the service for the passed ServiceBuilder object.
     * 
     * @param builder
     *            ServiceBuilder instance describing the service which should be provisioned.
     * @param connectionSettings
     *            Settings containing information about messaging configuration.
     * @param notificationChannel
     *            Messaging channel which should be used to inform about completed provisioning, since this function is
     *            executed asynchronously in an own thread.
     * @throws ServiceStartupException
     *             ServiceStartupException is thrown if starting/provisioning the service fails.
     * @throws MessagingException
     *             MessagingException is thrown if notifying about provisioning raises errors.
     */
    public final void startServiceInstance(final ServiceBuilder builder, final Settings connectionSettings,
            final String notificationChannel) throws ServiceStartupException, MessagingException {
        startServiceInstance(builder, connectionSettings, notificationChannel, new Date());
    }

    /**
     * Creates a StartServiceTask which will be executed asynchronously.
     * 
     * @param builder
     *            ServiceBuilder object describing the service which should be started.
     * @param connectionSettings
     *            Settings containing information about messaging configuration.
     * @param notificationChannel
     *            Messaging channel which should be used to inform about completed provisioning, since this function is
     *            executed asynchronously in an own thread.
     * @param softwareServiceManagerFacade
     *            SoftwareServiceManagerFacade instance starting the service.
     * @return Returns a new StartServiceTask for the passed builder.
     */
    protected StartServiceTask createStartServiceTask(final ServiceBuilder builder, final Settings connectionSettings,
            final String notificationChannel, final SoftwareServiceManagerFacade softwareServiceManagerFacade) {
        return new StartServiceTask(provisioningManager, builder, connectionSettings, notificationChannel, this);
    }

    /**
     * Creates and starts provisioning of the service for the passed ServiceBuilder object.
     * 
     * @param builder
     *            ServiceBuilder instance describing the service which should be provisioned.
     * @param connectionSettings
     *            Settings containing information about messaging configuration.
     * @param notificationChannel
     *            Messaging channel which should be used to inform about completed provisioning, since this function is
     *            executed asynchronously in an own thread.
     * @param startTime
     *            StartTime of the service.
     * @throws ServiceStartupException
     *             ServiceStartupException is thrown if starting/provisioning the service fails.
     * @throws MessagingException
     *             MessagingException is thrown if notifying about provisioning raises errors.
     */
    public final void startServiceInstance(final ServiceBuilder builder, final Settings connectionSettings,
            final String notificationChannel, final Date startTime) throws ServiceStartupException, MessagingException {
        validate();
        StartServiceTask task = createStartServiceTask(builder, connectionSettings, notificationChannel, this);
        new Timer().schedule(task, startTime);
    }

    /**
     * Stops the service instance for the passed ServiceBuilder instance.
     * 
     * @param builder
     *            ServiceBuilder instance describing the service which should be stoped.
     */
    public final void stopServiceInstance(final ServiceBuilder builder) {
        stopServiceInstance(builder, new Date());
    }

    /**
     * Initializes a ServiceInstance for a started Service.
     * 
     * @param builder
     *            ServiceBuilder instance describing the service the instance should be initialized for.
     */
    public final void initializeServiceInstance(final ServiceBuilder builder) {
        serviceInfoManager.setServiceStatus(builder.getUuid(), ServiceState.eSERVICE_RUNNING);
        ServiceInstance instance = createServiceInstance(builder);
        serviceInfoManager.setServiceInstance(instance);
        System.out.println(instance);
        landscape.addBuilder(builder);
        landscape.addInstance(instance);
    }

    /**
     * Deinitializes data for a ServiceInstance.
     * 
     * @param builder
     *            ServiceBuilder instance describing the service of interest.
     */
    private void deinitializeServiceData(final ServiceBuilder builder) {
        serviceInfoManager.setServiceStatus(builder.getUuid(), ServiceState.eSERVICE_STOPPED);
        ServiceInstance instance = serviceInfoManager.getServiceInstance(builder.getUuid());
        landscape.getInstancesList().remove(instance);
        landscape.getBuildersList().remove(builder);
        serviceInfoManager.removeManageabilityAgent(builder.getUuid());
        serviceInfoManager.removeServiceInstance(builder.getUuid());
    }

    /**
     * Creates a serviceInstance for the passed ServiceBuilder instance.
     * 
     * @param builder
     *            ServiceBuilder instance describing the service the instance should be created for.
     * @return Returns a new ServiceInstance
     */
    private ServiceInstance createServiceInstance(final ServiceBuilder builder) {
        ServiceConstructionModelFactory factory = ServiceConstructionModel.getFactory();
        ServiceInstance instance = factory.createServiceInstance();

        instance.setID(builder.getUuid());
        instance.setImplementation(builder.getImplementation());
        instance.setServiceInstanceName(builder.getImplementation().getServiceImplementationName() + "_Instance_"
                + builder.getUuid());
        instance.setDescription(builder.getImplementation().getDescription());

        List<Endpoint> endpointList = provisioningManager.getEndpoints(builder);
        if (endpointList != null) {
            instance.getEndpointsList().addAll(endpointList);
        }
        return instance;
    }

    /**
     * Stops the service instance for the passed ServiceBuilder instance.
     * 
     * @param builder
     *            ServiceBuilder instance describing the service which should be stoped.
     * @param stopTime
     *            Time at which the service should be stopped.
     */
    public final void stopServiceInstance(final ServiceBuilder builder, final Date stopTime) {
        validate();
        checkState(builder.getUuid(), ServiceState.eSERVICE_RUNNING);
        StopServiceTask task = new StopServiceTask(provisioningManager, builder);
        new Timer().schedule(task, stopTime);
        deinitializeServiceData(builder);
    }

    /**
     * Configures Monitoring for the given ServiceBuilder object.
     * 
     * @param builder
     *            ServiceBuilder object of interest.
     */
    public final void configureMonitoringSystem(final ServiceBuilder builder) {
        validate();
        // TODO: Add final monitoring-configuration of Davide & Sam
    }

    /**
     * Deconfigures Monitoring for the given ServiceBuilder object.
     * 
     * @param serviceID
     *            Identifier of the ServiceBuilder object of interest.
     */
    public final void deconfigureMonitoring(final String serviceID) {
        validate();
        serviceInfoManager.getManageabilityAgent(serviceID).deconfigureMonitoring();
    }

    /**
     * Deconfigures Monitoring for the given ServiceBuilder object.
     * 
     * @param builder
     *            ServiceBuilder object of interest.
     */
    public final void deconfigureMonitoring(final ServiceBuilder builder) {
        deconfigureMonitoring(builder.getUuid());
    }

    /**
     * Executes a management action on the running service.
     * 
     * @param builder
     *            ServiceBuilder instance describing the service the action should be executed for.
     * @param action
     *            Action to be executed.
     * @return Returns an instance of IEffectorResult representing the result of the action.
     * 
     */
    public final IEffectorResult executeAction(final ServiceBuilder builder, final IEffectorAction action) {
        validate();
        return executeAction(builder.getUuid(), action);
    }

    /**
     * Executes a management action on the running service.
     * 
     * @param serviceID
     *            Identifier of the ServiceBuilder instance describing the service the action should be executed for.
     * @param action
     *            Action to be executed.
     * @return Returns an instance of IEffectorResult representing the result of the action.
     * 
     */
    public final IEffectorResult executeAction(final String serviceID, final IEffectorAction action) {
        validate();
        return serviceInfoManager.getManageabilityAgent(serviceID).executeAction(action);
    }

    /**
     * @param builder
     *            ServiceBuilder object of interest.
     * @return Returns a list of SensorSubscriptionData for the passed ServiceBuilder object.
     */
    public final List<SensorSubscriptionData> getSensorSubscriptionData(final ServiceBuilder builder) {
        return getSensorSubscriptionData(builder.getUuid());
    }

    /**
     * @param serviceID
     *            Identifier of the ServiceBuilder object of interest.
     * @return Returns a list of SensorSubscriptionData for the passed ServiceBuilder object.
     */
    public final List<SensorSubscriptionData> getSensorSubscriptionData(final String serviceID) {
        validate();
        return serviceInfoManager.getManageabilityAgent(serviceID).getSensorSubscriptionData();
    }

    /**
     * Checks whether the ServiceBuilder identified by the passed identifier has the expected state. Throws an
     * IllegalStateException if the actual state is not equal to the expected state.
     * 
     * @param serviceID
     *            Identifier for the ServiceBuilder instance of interest.
     * @param expectedState
     *            Expected state of the builder.
     */
    private void checkState(final String serviceID, final ServiceState expectedState) {
        if (serviceInfoManager.getServiceStatus(serviceID) != expectedState) {
            throw new IllegalStateException("Service is expected to be in state '" + expectedState
                    + "' but is in state '" + serviceInfoManager.getServiceStatus(serviceID) + "'.");
        }

    }

    /**
     * @param serviceID
     *            Identifier of the ServiceBuilder object of interest.
     * @return Returns the ServiceBuilder object for the passed identifier.
     */
    public final ServiceBuilder getBuilder(final String serviceID) {
        validate();
        return this.serviceInfoManager.getBuilder(serviceID);
    }

    /**
     * Checks whether the SSM has been instantiated properly.
     */
    public final void validate() {
        if (this.landscape == null) {
            throw new IllegalStateException("Software Landscape has not been initialised");
        }

        if (this.provisioningManager == null) {
            throw new IllegalStateException("ProvisioningManager has not been initialised");
        }

        if (this.bookingManager == null) {
            throw new IllegalStateException("Booking has not been initialised");
        }

    }

    /**
     * Creates a copy of the passed ServiceBuilder object. The only thing different to the original builder is the UUID.
     * 
     * @param b
     *            ServiceBuilder object to be cloned.
     * @return Returns a copy of the passed ServiceBuilder object.
     */
    public final ServiceBuilder cloneBuilder(final ServiceBuilder b) {
        ServiceBuilder newBuilder = this.createBuilder(b.getImplementation());

        Copier c = new Copier();

        ServiceBinding newBinding = null;
        for (ServiceBinding binding : b.getBindingsList()) {
            newBinding = (ServiceBinding) c.copy(binding);
            c.copyReferences();
            newBuilder.getBindingsList().add(newBinding);
        }

        ConfigurationDirective newDirective = null;
        for (ConfigurationDirective directive : b.getServiceConfigurationsList()) {
            newDirective = (ConfigurationDirective) c.copy(directive);
            c.copyReferences();
            newBuilder.getServiceConfigurationsList().add(newDirective);
        }

        if (b.getMonitoringSystemConfiguration() != null) {
            MonitoringSystemConfiguration msc = b.getMonitoringSystemConfiguration();
            MonitoringSystemConfiguration newMsc = (MonitoringSystemConfiguration) c.copy(msc);
            c.copyReferences();
            newBuilder.setMonitoringSystemConfiguration(newMsc);
        }

        return newBuilder;
    }

    /**
     * @param serviceID
     *            Identifier of the ServiceBuilder object of interest.
     * @return Returns true if a ServiceBuilder object with passed identifier exists/ is known to the
     *         SoftwareServiceManager.
     */
    public final boolean hasBuilder(final String serviceID) {
        return this.serviceInfoManager.hasBuilder(serviceID);
    }

    /**
     * Setter for the ManageabilityAgent of a ServiceBuilder object.
     * 
     * @param builder
     *            ServiceBuilder object the manageability agent should be set for.
     * @param agent
     *            Target IManageabilityAgentFacade.
     */
    public final void setManageabilityAgent(final ServiceBuilder builder, final IManageabilityAgentFacade agent) {
        this.serviceInfoManager.setManageabilityAgent(builder.getUuid(), agent);
    }

}
