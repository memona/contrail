/**
 *  SVN FILE: $Id: BookingManager.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/impl/BookingManager.java $
 */

package org.slasoi.softwareservicemanager.impl;

import java.util.Date;

import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.softwareservicemanager.exceptions.BookingException;
import org.slasoi.softwareservicemanager.exceptions.ReservationException;

/**
 * This is dummy implementation of a BookingManager.
 */
public class BookingManager {

    /**
     * Capacity of the BookingManager.
     */
    private int capacity;
    /**
     * Number of booked services.
     */
    private int booked;
    /**
     * Number of reserved services.
     */
    private int reserved;
    /**
     * Constant capacity.
     */
    private final int maxCAPACITY = 1000;

    /**
     * Constructor.
     */
    public BookingManager() {
        this.capacity = maxCAPACITY;
    }

    /**
     * 
     * @param implementation
     *            Implementation of interest.
     * @return Returns the capacity for a ServiceImplementation.
     */
    public final int capacityCheck(final ServiceImplementation implementation) {
        return capacity;
    }

    /**
     * Books a service for the passed builder object.
     * 
     * @param builder
     *            Builder, containing the information about the service.
     * @throws BookingException
     *             This exception is thrown if booking fails.
     */
    public final void book(final ServiceBuilder builder) throws BookingException {
        reserved--;
        booked++;
    }

    /**
     * Cancels a booking operation for the passed builder.
     * 
     * @param builder
     *            Builder of interest.
     * @return Returns a boolean value indicating whether the canceling was successful.
     * @throws BookingException
     *             Exception is thrown if canceling the booking fails.
     */
    public final boolean cancelBooking(final ServiceBuilder builder) throws BookingException {
        booked--;
        return true;
    }

    /**
     * Reserves resources for the service described by the passed builder object.
     * 
     * @param builder
     *            Builder containing information about the service of interest.
     * @param from
     *            Start time of the time period the resources should be reserved for.
     * @param until
     *            End time of the time period the resources should be reserved for.
     * @return Returns true if reservation succeeds.
     * @throws ReservationException
     *             This exception is thrown if reservation fails.
     */
    public final boolean reserve(final ServiceBuilder builder, final Date from, final Date until)
            throws ReservationException {
        if (booked + reserved < capacity) {
            reserved++;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Cancels reservation for passed builder.
     * 
     * @param builder
     *            Builder describing the service.
     * @return Returns true if canceling the reservation succeeds.
     * @throws ReservationException
     *             Is thrown if an error occurs during canceling the reservation.
     */
    public final boolean cancelReservation(final ServiceBuilder builder) throws ReservationException {
        reserved--;
        return true;
    }

    /**
     * Updates reservation information for a builder instance.
     * 
     * @param builder
     *            Builder describing the service.
     * @param from
     *            Start time of the time period the resources should be reserved for.
     * @param until
     *            End time of the time period the resources should be reserved for.
     * @return Returns true if update succeeds.
     * @throws ReservationException
     *             This exception is thrown if update fails.
     */
    public final boolean updateReservation(final ServiceBuilder builder, final Date from, final Date until)
            throws ReservationException {
        return true;
    }
}
