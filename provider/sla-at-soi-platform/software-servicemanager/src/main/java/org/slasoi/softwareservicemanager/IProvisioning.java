/**
 *  SVN FILE: $Id: IProvisioning.java 154 2010-11-18 07:31:00Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 154 $
 * @lastrevision   $Date: 2010-11-18 08:31:00 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/software-servicemanager/src/main/java/org/slasoi/softwareservicemanager/IProvisioning.java $
 */

package org.slasoi.softwareservicemanager;

import java.util.List;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Settings;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.softwareservicemanager.exceptions.ServiceStartupException;
import org.slasoi.softwareservicemanager.management.IManageabilityAgentFacade;

/**
 * Domain-specific interface that is to be implemented by all use cases employing a SoftwareServiceManager. This
 * interface encapsulates the basic functionality for starting and stopping service instances. Furthermore, it allows
 * the SoftwareServiceManager to access (or create) a domain-specific ManageabilityAgents for an existing service
 * instance. Finally, all implementations of this interface encapsulate the knowledge necessary to determine the
 * endpoints for their services.
 * 
 * @author Jens Happe
 * 
 */
public interface IProvisioning {

    /**
     * Start a new service instance according to the configuration given in the builder object. Once the service is
     * available a notification is sent over the channel specified.
     * 
     * @param builder
     *            The builder object used to set up the service instance. The builder must be fully specified, i.e., all
     *            dependencies to external services have to be resolved.
     * 
     * @param connectionSettings
     *            Connection settings for the message-oriented middleware hosting the notification channel.
     * 
     * @param notificationChannel
     *            The channel used to notify the responsible parties once the service's provisioning has been completed.
     * @throws ServiceStartupException
     *             Thrown if an error occurs during the initialization of the provisioning process.
     * @throws MessagingException
     *             Thrown if the messaging system cannot be reached.
     */
    void startServiceInstance(ServiceBuilder builder, Settings connectionSettings, String notificationChannel)
            throws ServiceStartupException, MessagingException;

    /**
     * Stops a running service instance.
     * 
     * @param builder
     *            Builder object that has been used to create the service instance.
     */
    void stopServiceInstance(ServiceBuilder builder);

    /**
     * Determines the endpoints of a service instance created by the given builder object.
     * 
     * @param builder
     *            Builder object used to create the service instance
     * @return Endpoint(s) of the service.
     */
    List<Endpoint> getEndpoints(ServiceBuilder builder);

    /**
     * If available, this methods returns a facade to the manageability system of the given, domain-specific service.
     * 
     * @param builder
     *            Builder object used to create the service instance.
     * @return ManageabilityAgent of a particular service instance.
     */
    IManageabilityAgentFacade getManagibilityAgent(ServiceBuilder builder);
}
