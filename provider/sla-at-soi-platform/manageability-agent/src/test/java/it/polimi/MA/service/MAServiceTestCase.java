package it.polimi.MA.service;


import it.polimi.MA.impl.MAServiceImpl;
import it.polimi.MA.impl.doe.DOESensorConfiguration;
import it.polimi.MA.impl.doe.UpdateBinding;
import it.polimi.MA.service.exceptions.ServiceStartupException;

import java.util.List;
import java.util.UUID;

import junit.framework.TestCase;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.extended.ServiceBuilderExtended;
import org.slasoi.monitoring.common.configuration.Component;
import org.slasoi.monitoring.common.configuration.ComponentConfiguration;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.configuration.OutputReceiver;
import org.slasoi.monitoring.common.configuration.impl.ConfigurationFactoryImpl;
import org.slasoi.monitoring.common.configuration.impl.OutputReceiverImpl;

public class MAServiceTestCase extends TestCase {
	
	boolean answerReceived = false;
	private String uuid = "paymentService";
	
	
	public void testMAServicestartServiceInstance() {
		

		MAService service = new MAServiceImpl();
		assertNotNull(service);
		
		ServiceBuilder builder = this.getBuilder();
		
		Settings connectionSettings = this.getPubSubSettings();
		assertNotNull(connectionSettings);
		
		String notificationChannel = "test-MA-Sam";	
		
		boolean result = false;
		try {
			service.startServiceInstance(builder, connectionSettings, notificationChannel);
			result = true;
		} catch (ServiceStartupException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(result);
		
		IManageabilityAgentFacade facade = service.getManagibilityAgentFacade(builder);
		assertNotNull(facade);
		
		//testing sensor configuration
		
		ConfigurationFactoryImpl factory = new ConfigurationFactoryImpl();

		MonitoringSystemConfiguration msc = factory.createMonitoringSystemConfiguration();
		
		msc.setUuid(UUID.randomUUID().toString());
		
		Component[] components = new Component[1];
		Component c = factory.createComponent();
		c.setType("Sensor");
		
		DOESensorConfiguration config = new DOESensorConfiguration();
		
		config.setConfigurationId(UUID.randomUUID().toString());
		
		String serviceID = "paymentService";
		config.setServiceID(serviceID);
		String operationID = "/process/flow/receive[@name=$$ReceivePaymentRequest$$]";
		config.setOperationID(operationID);
		String status = "input";
		config.setStatus(status);
		String correlationKey = "cardNumber";
		config.setCorrelationKey(correlationKey);
		String correlationValue = "7777";
		config.setCorrelationValue(correlationValue);
		
		OutputReceiver[] newOutputReceivers = new OutputReceiverImpl[1];
		OutputReceiver receiver = new ConfigurationFactoryImpl().createOutputReceiver();
		receiver.setEventType("event");
		receiver.setUuid("tcp:localhost:10000");
		newOutputReceivers[0] = receiver;
		config.setOutputReceivers(newOutputReceivers);
		
		ComponentConfiguration[] configs = new ComponentConfiguration[1];
		configs[0] = config;
		c.setConfigurations(configs );
		
		
		components[0] = c;
		msc.setComponents(components );
		
		facade.configureMonitoringSystem(msc);
		
		
		List<SensorSubscriptionData> list = facade.getSensorSubscriptionData();
		
		assertTrue(list.size()==1);
		
		for (SensorSubscriptionData d : list) {
			
			System.out.println("[DOE - facade] Sensor id: " + d.getSensorID());
			
		}
		
		facade.deconfigureMonitoring();
		
		list = facade.getSensorSubscriptionData();
		
		assertTrue(list.size()==0);
		
		for (SensorSubscriptionData d : list) {
			
			System.out.println("[DOE - facade] Sensor id: " + d.getSensorID());
			
		}
		
		
		
		IEffectorResult effectorResult = null;
		IEffectorAction action = new UpdateBinding(builder);
		try {
			effectorResult = facade.executeAction(action);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(effectorResult.isResult()); 
		

		
		
	}
	
	private ServiceBuilder getBuilder() {
		// TODO Auto-generated method stub
		ServiceBuilder builder = new ServiceBuilderExtended();
		builder.setUuid(uuid);

		
		//implement some way of obtaining real builder
		return builder;
	}

	private Settings getPubSubSettings() {
		Settings settings = new Settings();
		settings.setSetting(Setting.pubsub, "xmpp");
		settings.setSetting(Setting.xmpp_username, "primitive-ecf");
		settings.setSetting(Setting.xmpp_password, "primitive-ecf");
		settings.setSetting(Setting.xmpp_host, "testbed.sla-at-soi.eu");
		settings.setSetting(Setting.xmpp_port, "5222");
		settings.setSetting(Setting.messaging, "xmpp");
		settings.setSetting(Setting.pubsub, "xmpp");
		settings.setSetting(Setting.xmpp_service, "testbed.sla-at-soi.eu");
		settings.setSetting(Setting.xmpp_resource, "test");
		settings.setSetting(Setting.xmpp_pubsubservice, "pubsub.testbed.sla-at-soi.eu");	
		return settings;
	}
	
	

}
