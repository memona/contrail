
/**
 * ActiveBpelAdminCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

    package active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl;

    /**
     *  ActiveBpelAdminCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ActiveBpelAdminCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ActiveBpelAdminCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ActiveBpelAdminCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for setCorrelationSetData method
            * override this method for handling normal response from setCorrelationSetData operation
            */
           public void receiveResultsetCorrelationSetData(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from setCorrelationSetData operation
           */
            public void receiveErrorsetCorrelationSetData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for setConfiguration method
            * override this method for handling normal response from setConfiguration operation
            */
           public void receiveResultsetConfiguration(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from setConfiguration operation
           */
            public void receiveErrorsetConfiguration(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProcessDef method
            * override this method for handling normal response from getProcessDef operation
            */
           public void receiveResultgetProcessDef(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetProcessDefOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProcessDef operation
           */
            public void receiveErrorgetProcessDef(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for restartProcess method
            * override this method for handling normal response from restartProcess operation
            */
           public void receiveResultrestartProcess(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from restartProcess operation
           */
            public void receiveErrorrestartProcess(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSensorConfigurations method
            * override this method for handling normal response from getSensorConfigurations operation
            */
           public void receiveResultgetSensorConfigurations(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetSensorConfigurationsOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSensorConfigurations operation
           */
            public void receiveErrorgetSensorConfigurations(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAPIVersion method
            * override this method for handling normal response from getAPIVersion operation
            */
           public void receiveResultgetAPIVersion(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetAPIVersionOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAPIVersion operation
           */
            public void receiveErrorgetAPIVersion(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProcessList method
            * override this method for handling normal response from getProcessList operation
            */
           public void receiveResultgetProcessList(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetProcessListOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProcessList operation
           */
            public void receiveErrorgetProcessList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for resumeProcessObject method
            * override this method for handling normal response from resumeProcessObject operation
            */
           public void receiveResultresumeProcessObject(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from resumeProcessObject operation
           */
            public void receiveErrorresumeProcessObject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for suspendProcess method
            * override this method for handling normal response from suspendProcess operation
            */
           public void receiveResultsuspendProcess(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from suspendProcess operation
           */
            public void receiveErrorsuspendProcess(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVariable method
            * override this method for handling normal response from getVariable operation
            */
           public void receiveResultgetVariable(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetVariableDataOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVariable operation
           */
            public void receiveErrorgetVariable(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for removeProcessListener method
            * override this method for handling normal response from removeProcessListener operation
            */
           public void receiveResultremoveProcessListener(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from removeProcessListener operation
           */
            public void receiveErrorremoveProcessListener(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for configureSensor method
            * override this method for handling normal response from configureSensor operation
            */
           public void receiveResultconfigureSensor(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.ConfigureSensorOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from configureSensor operation
           */
            public void receiveErrorconfigureSensor(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPartnerRoleBindingRules method
            * override this method for handling normal response from getPartnerRoleBindingRules operation
            */
           public void receiveResultgetPartnerRoleBindingRules(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetPartnerRoleBindingRulesOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPartnerRoleBindingRules operation
           */
            public void receiveErrorgetPartnerRoleBindingRules(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for removeAttachments method
            * override this method for handling normal response from removeAttachments operation
            */
           public void receiveResultremoveAttachments(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.RemoveAttachmentDataOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from removeAttachments operation
           */
            public void receiveErrorremoveAttachments(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for resumeProcess method
            * override this method for handling normal response from resumeProcess operation
            */
           public void receiveResultresumeProcess(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from resumeProcess operation
           */
            public void receiveErrorresumeProcess(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteSensorConfiguration method
            * override this method for handling normal response from deleteSensorConfiguration operation
            */
           public void receiveResultdeleteSensorConfiguration(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.DeleteSensorConfigurationOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteSensorConfiguration operation
           */
            public void receiveErrordeleteSensorConfiguration(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProcessLog method
            * override this method for handling normal response from getProcessLog operation
            */
           public void receiveResultgetProcessLog(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetProcessLogOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProcessLog operation
           */
            public void receiveErrorgetProcessLog(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getConfiguration method
            * override this method for handling normal response from getConfiguration operation
            */
           public void receiveResultgetConfiguration(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetConfigurationOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getConfiguration operation
           */
            public void receiveErrorgetConfiguration(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addBreakpointListener method
            * override this method for handling normal response from addBreakpointListener operation
            */
           public void receiveResultaddBreakpointListener(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addBreakpointListener operation
           */
            public void receiveErroraddBreakpointListener(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addPartnerRoleAbstractBindingRule method
            * override this method for handling normal response from addPartnerRoleAbstractBindingRule operation
            */
           public void receiveResultaddPartnerRoleAbstractBindingRule(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addPartnerRoleAbstractBindingRule operation
           */
            public void receiveErroraddPartnerRoleAbstractBindingRule(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for completeActivity method
            * override this method for handling normal response from completeActivity operation
            */
           public void receiveResultcompleteActivity(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from completeActivity operation
           */
            public void receiveErrorcompleteActivity(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProcessDigest method
            * override this method for handling normal response from getProcessDigest operation
            */
           public void receiveResultgetProcessDigest(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetProcessDigestOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProcessDigest operation
           */
            public void receiveErrorgetProcessDigest(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for retryActivity method
            * override this method for handling normal response from retryActivity operation
            */
           public void receiveResultretryActivity(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from retryActivity operation
           */
            public void receiveErrorretryActivity(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProcessState method
            * override this method for handling normal response from getProcessState operation
            */
           public void receiveResultgetProcessState(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetProcessStateOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProcessState operation
           */
            public void receiveErrorgetProcessState(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addPartnerRoleBindingRule method
            * override this method for handling normal response from addPartnerRoleBindingRule operation
            */
           public void receiveResultaddPartnerRoleBindingRule(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addPartnerRoleBindingRule operation
           */
            public void receiveErroraddPartnerRoleBindingRule(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for terminateProcess method
            * override this method for handling normal response from terminateProcess operation
            */
           public void receiveResultterminateProcess(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from terminateProcess operation
           */
            public void receiveErrorterminateProcess(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for setVariable method
            * override this method for handling normal response from setVariable operation
            */
           public void receiveResultsetVariable(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.SetVariableDataOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from setVariable operation
           */
            public void receiveErrorsetVariable(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addProcessListener method
            * override this method for handling normal response from addProcessListener operation
            */
           public void receiveResultaddProcessListener(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addProcessListener operation
           */
            public void receiveErroraddProcessListener(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateBreakpointList method
            * override this method for handling normal response from updateBreakpointList operation
            */
           public void receiveResultupdateBreakpointList(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateBreakpointList operation
           */
            public void receiveErrorupdateBreakpointList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for setPartnerLinkData method
            * override this method for handling normal response from setPartnerLinkData operation
            */
           public void receiveResultsetPartnerLinkData(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from setPartnerLinkData operation
           */
            public void receiveErrorsetPartnerLinkData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for removeBreakpointListener method
            * override this method for handling normal response from removeBreakpointListener operation
            */
           public void receiveResultremoveBreakpointListener(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from removeBreakpointListener operation
           */
            public void receiveErrorremoveBreakpointListener(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPartnerRoleAbstractBindingRules method
            * override this method for handling normal response from getPartnerRoleAbstractBindingRules operation
            */
           public void receiveResultgetPartnerRoleAbstractBindingRules(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetPartnerRoleAbstractBindingRulesOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPartnerRoleAbstractBindingRules operation
           */
            public void receiveErrorgetPartnerRoleAbstractBindingRules(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for removePartnerRoleRule method
            * override this method for handling normal response from removePartnerRoleRule operation
            */
           public void receiveResultremovePartnerRoleRule(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from removePartnerRoleRule operation
           */
            public void receiveErrorremovePartnerRoleRule(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addEngineListener method
            * override this method for handling normal response from addEngineListener operation
            */
           public void receiveResultaddEngineListener(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addEngineListener operation
           */
            public void receiveErroraddEngineListener(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSensedServices method
            * override this method for handling normal response from getSensedServices operation
            */
           public void receiveResultgetSensedServices(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetSensedServicesOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSensedServices operation
           */
            public void receiveErrorgetSensedServices(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deployBpr method
            * override this method for handling normal response from deployBpr operation
            */
           public void receiveResultdeployBpr(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.DeployBprOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deployBpr operation
           */
            public void receiveErrordeployBpr(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProcessDetail method
            * override this method for handling normal response from getProcessDetail operation
            */
           public void receiveResultgetProcessDetail(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.GetProcessDetailOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProcessDetail operation
           */
            public void receiveErrorgetProcessDetail(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addAttachment method
            * override this method for handling normal response from addAttachment operation
            */
           public void receiveResultaddAttachment(
                    active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.AddAttachmentDataOutput result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addAttachment operation
           */
            public void receiveErroraddAttachment(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createSWS method
            * override this method for handling normal response from createSWS operation
            */
           public void receiveResultcreateSWS(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createSWS operation
           */
            public void receiveErrorcreateSWS(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for removeEngineListener method
            * override this method for handling normal response from removeEngineListener operation
            */
           public void receiveResultremoveEngineListener(
                    ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from removeEngineListener operation
           */
            public void receiveErrorremoveEngineListener(java.lang.Exception e) {
            }
                


    }
    