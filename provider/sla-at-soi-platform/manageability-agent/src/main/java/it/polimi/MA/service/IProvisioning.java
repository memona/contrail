package it.polimi.MA.service;

import it.polimi.MA.service.exceptions.ServiceStartupException;

import java.util.List;

import org.slasoi.common.messaging.MessagingException;
import org.slasoi.common.messaging.Settings;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.slamodel.sla.Endpoint;

public interface IProvisioning {

	public void startServiceInstance(ServiceBuilder builder,
			Settings connectionSettings, String notificationChannel)
			throws ServiceStartupException, MessagingException;

	public void stopServiceInstance(ServiceBuilder builder);

	public List<Endpoint> getEndpoints(ServiceBuilder builder);
	
	public IManageabilityAgentFacade getManagibilityAgentFacade(ServiceBuilder builder);
	
}
