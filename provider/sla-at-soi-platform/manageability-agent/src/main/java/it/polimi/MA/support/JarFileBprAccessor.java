package it.polimi.MA.support;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Collection;
import java.util.Iterator;
import java.util.zip.ZipEntry;

public class JarFileBprAccessor {

	JarReaderUtil jru = null;
	/** The pdd resource names. */
	private Collection mPddResources;

	public JarFileBprAccessor(URI bprLocation) throws IOException {
		jru = new JarReaderUtil(bprLocation);
		setPddResources(jru.getEntryNames(new NameFilter(".pdd")));
	}

	/**
	 * @param aPddResources
	 *            The pddResources to set.
	 */
	private void setPddResources(Collection aPddResources) {
		mPddResources = aPddResources;
	}

	public Collection getMPddResources() {
		return mPddResources;
	}
	
	public InputStream getPddInputStream() throws IOException{
		Iterator it = getMPddResources().iterator();
		String pdd = null;
		if(it.hasNext()){
			pdd = (String) it.next();
//			System.out.println("pdd.:"+pdd);
			return jru.getJar().getInputStream(new ZipEntry(pdd));
		}
		else
			throw new RuntimeException("Malformed BPR file.: pdd file does not exist!");
	}

	// ////// UTILITY CLASS///////

	/**
	 * Convience class - impl of FilenameFilter for building deployment
	 * descriptor object.
	 */
	static class NameFilter implements FilenameFilter {
		String mExt;

		/**
		 * Constructor
		 * 
		 * @param aExt
		 *            extension to filter on.
		 */
		public NameFilter(String aExt) {
			mExt = aExt;
		}

		/**
		 * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
		 */
		public boolean accept(File aFile, String aFilename) {
			return !isNullOrEmpty(aFilename)
					&& aFilename.toLowerCase().endsWith(mExt);
		}

		/**
		 * Returns true if the string object is null or doesn't contain at least
		 * one non-space character.
		 * 
		 * @param aString
		 *            The string to check.
		 * 
		 * @return boolean
		 */
		public static boolean isNullOrEmpty(String aString) {
			boolean result = true;
			if (aString != null)
				if (aString.trim().length() > 0)
					result = false;
			return result;
		}
	}

}
