package it.polimi.MA.impl.doe;

import it.polimi.MA.impl.ManageabilityAgentFacade;
import it.polimi.MA.service.IEffectorAction;
import it.polimi.MA.service.IEffectorResult;
import it.polimi.MA.service.SensorSubscriptionData;
import it.polimi.MA.service.IEffectorAction.Types;
import it.polimi.MA.support.JarFileBprAccessor;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.axis2.AxisFault;
import org.slasoi.common.messaging.Setting;
import org.slasoi.common.messaging.Settings;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.monitoring.common.configuration.Component;
import org.slasoi.monitoring.common.configuration.ComponentConfiguration;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;
import org.slasoi.monitoring.common.configuration.OutputReceiver;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub;
import active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.AddPartnerRoleBindingRuleInput;
import active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.AesAddBindingRuleRequestType;
import active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.AesBindingRule;
import active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.ConfigureSensorInput;
import active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.ConfigureSensorRequestType;
import active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.DeleteSensorConfigurationInput;
import active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.DeleteSensorConfigurationRequestType;
import active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.MapItem;


public class DOEManageabilityAgentFacade extends ManageabilityAgentFacade  {
	
	private ServiceBuilder builder;
	private String invokeHandler="java:it.eng.binding.impl.EngInvokeHandler";
	private URL adminServiceURL = null;
	private ArrayList<DOESensorConfiguration> currentSensorConfigurations;
	private String serviceID;
	private String sensorID;

	public DOEManageabilityAgentFacade(ServiceBuilder builder) throws MalformedURLException {
		// TODO Auto-generated constructor stub
		this.builder = builder;
		adminServiceURL = new URL("http://ORCServices:1030/active-bpel/services/ActiveBpelAdmin");
		this.currentSensorConfigurations = new ArrayList<DOESensorConfiguration>();
		this.serviceID = getServiceID(builder);
		
		/*
		 * 
		 * Verify that the BPEL process contained in the builder is deployed. If not proceed
		 * to deploy the process using the domain-specific interface.
		 * 
		 * Verify if builder contains information regarding binding rules. If it does
		 * proceed to configure binding using domain-specific interface. 
		 * 
		 * Verify if builder contains information regarding monitoring configurations. If it
		 * does proceed to configure monitoring using domain-specific interface.
		 * 
		 * 
		 */
		
		
	}



	private String getServiceID(ServiceBuilder builder) {
		return builder.getUuid();
	}

	public void configureMonitoringSystem(
			MonitoringSystemConfiguration configuration) {
		
		System.out.println("[MA-Facade] Starting configureMonitoringSystem");
		
		Component[] components = configuration.getComponents();
		
		for (int i = 0; i< components.length; i++){
			Component c = components[i];
			if (c.getType().equals("Sensor")) {
				ComponentConfiguration[] configs = c.getConfigurations();
				for (int j=0; j< configs.length; j++){
					if (configs[j] instanceof DOESensorConfiguration){
						this.sensorID = c.getUuid();
						addConfiguration2DOE(configs[j]);
						addConfiguration2LocalRegistry(configs[j]);
					}
				}
			}
		}


	}

	private void addConfiguration2LocalRegistry(
			ComponentConfiguration componentConfiguration) {
		this.currentSensorConfigurations.add((DOESensorConfiguration) componentConfiguration);	
	}
	

	private void addConfiguration2DOE(ComponentConfiguration componentConfiguration) {
		
		ActiveBpelAdminStub stub = null;
		try {
			stub = new ActiveBpelAdminStub("http://ORCServices:1030/active-bpel/services/ActiveBpelAdmin");
		} catch (AxisFault e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		System.out.println("\n[DOE - facade] Stub correctly initialized for sensor configuration: host.:"+adminServiceURL);
		
		DOESensorConfiguration config = (DOESensorConfiguration)componentConfiguration;
		
		/*
		
		ConfigureSensorInputDocument configureSensorInput = null;
		try {
			configureSensorInput = (ConfigureSensorInputDocument) getTestObject(ConfigureSensorInputDocument.class);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
		

		
		//configureSensorInput.addNewConfigureSensorInput();
		
		ConfigureSensorInput configureSensor = new ConfigureSensorInput();
		
		ConfigureSensorRequestType configureSensorInput = new ConfigureSensorRequestType();
			
		String serviceID = config.getServiceID();
		configureSensorInput.setServiceID(serviceID);
		
		String operationID = config.getOperationID();
		configureSensorInput.setOperationID(operationID);
		
		String status = config.getStatus();
		configureSensorInput.setStatus(status);
		
		String correlationKey = config.getCorrelationKey();
		configureSensorInput.setCorrelationKey(correlationKey);
		
		String correlationValue = config.getCorrelationValue();
		configureSensorInput.setCorrelationValue(correlationValue);
		
		String[] targetEPR = new String[config.getOutputReceiversLength()];
		
		for (int i=0; i< config.getOutputReceiversLength(); i++){
			targetEPR[i] = config.getOutputReceivers(i).getUuid();
		}
		
		configureSensorInput.setTargetEPR(targetEPR);
		
		configureSensor.setConfigureSensorInput(configureSensorInput);
		
		try {
			
			stub.configureSensor(configureSensor);
			System.out.println("[DOE - facade] - Added config to DOE");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	public void deconfigureMonitoring() {
		
		
		ActiveBpelAdminStub stub = null;
		try {
			stub = new ActiveBpelAdminStub("http://ORCServices:1030/active-bpel/services/ActiveBpelAdmin");
		} catch (AxisFault e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		System.out.println("\n[DOE - facade] Stub correctly initialized: host.:"+adminServiceURL);	
		
		for (DOESensorConfiguration config : this.currentSensorConfigurations) {		
			
			DeleteSensorConfigurationInput deleteConfigInput = new DeleteSensorConfigurationInput();
			
			DeleteSensorConfigurationRequestType deleteConfig = new DeleteSensorConfigurationRequestType();
			
			String processID = config.getServiceID();
			deleteConfig.setProcessID(processID);
			
			String operationID = config.getOperationID();
			deleteConfig.setOperationID(operationID);
			
			String status = config.getStatus();
			deleteConfig.setStatus(status);
			
			String correlationKey = config.getCorrelationKey();
			deleteConfig.setCorrelationKey(correlationKey);
			
			String correlationValue = config.getCorrelationValue();
			deleteConfig.setCorrelationValue(correlationValue);
			
			deleteConfigInput.setDeleteSensorConfigurationInput(deleteConfig);
			
			try {
				stub.deleteSensorConfiguration(deleteConfigInput);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("[DOE - facade] Deleted Sensor Configuration");
		} 
		
		currentSensorConfigurations = new ArrayList<DOESensorConfiguration>();
		
	}

	public IEffectorResult executeAction(IEffectorAction action) throws Exception {
		ActiveBpelAdminStub stub = new ActiveBpelAdminStub(adminServiceURL.toString());
		System.out.println("\n[DOE - facade] Stub correctly initialized: host.:"+adminServiceURL);
		
		if (action != null) {
			ServiceBuilder sb = action.getServiceBuilder();
		}
		//TODO : There are two possibilities to access the binding information via service builder.
		//1. By means of deployment file contained in the bpr archive (a special kind of software archive)
		//2. By means of ServiceBinding object owned by the SrviceBuilder. Each binding, in fact, refers the SLA templates
		//   from which the builder depends. After negotiation the SLA template is updated with binding information.
		
		// N.B: In both cases it is needed, to build a binding rule, the builder holds information identifying a specific customer
		
		
		//2. TODO: Approach using bindings from service builder...
//		ServiceBinding[] bindings = builder.getBindings();
//		for (ServiceBinding serviceBinding : bindings) {
//			SLATemplate slat = serviceBinding.getSlaTemplate();
//			InterfaceDeclr[] intDeclrs = slat.getInterfaceDeclrs();
//			for (InterfaceDeclr interfaceDeclr : intDeclrs) {
//				ID interfaceProvider = interfaceDeclr.getProviderRef();
//				if (slat.getParty(interfaceProvider.getValue())
//						.getAgreementRole().getValue().equals(
//								"http://www.slaatsoi.org/slamodel#provider")) {
//					Endpoint endpoint = interfaceDeclr.getEndpoints()[0];
//					String endpointID = endpoint.getId().getValue();
//					// I assume the endpointID as: serviceName:portName
//					int i = endpointID.indexOf(":");
//					String serviceName = endpointID.substring(0, i);
//					String portName = endpointID.substring(i + 1);
//					if (endpoint.getProtocol().getValue().equals(
//							"http://www.slaatsoi.org/slamodel#SOAP")) {
//						String address = endpoint.getLocation().getValue();
//						AesBindingRule br = new AesBindingRule(null, address,
//								serviceName, portName);
//						String partnerRole = interfaceProvider.getValue();
//						String processName = builder.getImplementation()
//								.getType().getInterfaces(1);
//						AesAddBindingRuleRequestType rt = new AesAddBindingRuleRequestType(
//								br, partnerRole, processName);
//						// Switching on the action type...
//						if (action.getActionType() == Types.UPDATE_BINDIG) {
//							try {
//								stub.addPartnerRoleBindingRule(rt);
//							} catch (RemoteException e) {
//								throw new Exception(e);
//							}
//						}
//					}
//				}
//			}
//		}
//		
//		IEffectorResult er1 = new IEffectorResult();
//		er1.setResult(true);
//		return er1;
		
		
		//1. TODO :Approach using pdd file from service builder...
		//   the bpr archive extraction form builder is commented since it does not yet exist in SCM...
//		ImplementationArtefact[] artefacts = builder.getImplementation().getArtefacts();
//		SoftwareArchive swArchive = null;
//		for (ImplementationArtefact implementationArtefact : artefacts) {
//			if (implementationArtefact instanceof SoftwareArchive)
//				swArchive = (SoftwareArchive)implementationArtefact;
//			break;
//		}
		
		//URL bprFileURL = new File("test_bprfiles/paymentService.bpr").toURL();
		URL bprFileURL = new File(System.getenv("SLASOI_ORC_HOME") + "/test_bprfiles/paymentService.bpr").toURL();
//		Map<String,Map<String,String>> pddData = _parsePDD(bprFileURL);
		Map<String,Map<String,String>> pddData = _parsePDDByJAXB(bprFileURL);
		System.out.println("[DOE - facade] pdd file retrieved from bpr owned by service builder...");
		active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.Map condition = new active_endpoints.docs.wsdl.activebpeladmin._2007._01.activebpeladmin_wsdl.ActiveBpelAdminStub.Map();
		//TODO : Access to the sla identifier via builder...
		MapItem[] items = new MapItem[1];
		items[0] = new MapItem();
		items[0].setKey("slaid");
		items[0].setValue("sla0001");
		condition.setItem(items);
		System.out.println("[DOE - facade] slaid retrieved from service builder...");
		for (Entry<String,Map<String,String>> plink_entry : pddData.entrySet()) {
			int index = plink_entry.getKey().indexOf(":");
			String processName = plink_entry.getKey().substring(0, index);
			String plinkName = plink_entry.getKey().substring(index+1);
			
			AddPartnerRoleBindingRuleInput addPartnerRoleBindingRuleInput = new AddPartnerRoleBindingRuleInput();
			AesAddBindingRuleRequestType addBindingRuleRequest = new AesAddBindingRuleRequestType();
			addBindingRuleRequest.setProcessID(processName);
			addBindingRuleRequest.setPartnerRoleName(plinkName);
			String address = plink_entry.getValue().get("address"); 
			String service = plink_entry.getValue().get("service");
			String port = plink_entry.getValue().get("port");
			AesBindingRule br = new AesBindingRule();
			br.setPortName(port);
			br.setServiceName(service);
			br.setWsdlAddress(address);
			br.setCondition(condition);
			addBindingRuleRequest.setBindingRule(br);
			addPartnerRoleBindingRuleInput.setAddPartnerRoleBindingRuleInput(addBindingRuleRequest );
			//Switching on the action type...
			if( (action == null) || (action.getActionType() == Types.UPDATE_BINDIG)){
				try {
					stub.addPartnerRoleBindingRule(addPartnerRoleBindingRuleInput);
				} catch (RemoteException e) {
					throw new Exception(e);
				}
			}
		}
		IEffectorResult er = new IEffectorResult();
		er.setResult(true);
		System.out.println("[DOE - facade] binding rules configured on DOE");
		return er;
	}
	
	private Map<String, Map<String,String>> _parsePDDByJAXB(URL bprFileURL) throws IOException, URISyntaxException, JAXBException {
		JAXBContext jc = JAXBContext.newInstance("it.polimi.MA.impl.doe.pdd");
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		Marshaller marshaller = jc.createMarshaller();
		
		JarFileBprAccessor bpra = new JarFileBprAccessor(bprFileURL.toURI());
		InputStream is = bpra.getPddInputStream(); 
		Object obj = unmarshaller.unmarshal(is);
		it.polimi.MA.impl.doe.pdd.Process process = (it.polimi.MA.impl.doe.pdd.Process)obj;

		String processName = process.getName();
		processName = processName.substring(processName.indexOf(":")+1);

		Map<String, Map<String,String>> pddData = new HashMap<String, Map<String,String>>();
		List<it.polimi.MA.impl.doe.pdd.PartnerLinkType> partnerLinksList = process.getPartnerLinks().getPartnerLink();
		for (it.polimi.MA.impl.doe.pdd.PartnerLinkType partnerLinkType : partnerLinksList) {
			if(partnerLinkType.getPartnerRole()!=null && partnerLinkType.getPartnerRole().getInvokeHandler().equals(invokeHandler)){
				String partnerLinkName = partnerLinkType.getName();
				String key = processName+":"+partnerLinkName;
//				System.out.println("partnerLinkName.: "+partnerLinkName);
				pddData.put(key, new HashMap<String, String>());
				it.polimi.MA.impl.doe.pdd.PartnerRoleType pddPartnerRole = partnerLinkType.getPartnerRole();
				List<Element> list = pddPartnerRole.getAny();
				Element wsaEndpointReferenceElement = list.get(0);
				Node wsaAddressElement = wsaEndpointReferenceElement.getFirstChild();
				String wsaAddressElementContent = wsaAddressElement.getFirstChild().getNodeValue();
//				System.out.println("address.:"+wsaAddressElementContent);
				pddData.get(key).put("address", wsaAddressElementContent);
				Node wsaMetadataElement = wsaAddressElement.getNextSibling();
				Node wsaServiceName = wsaMetadataElement.getFirstChild();
				String wsaServiceNameContent = wsaServiceName.getFirstChild().getNodeValue();				
//				System.out.println("wsaServiceNameContent.: "+wsaServiceNameContent);
				int index = 0;
				if (wsaServiceNameContent.contains(":")){
					index = wsaServiceNameContent.indexOf(":");
					wsaServiceNameContent = wsaServiceNameContent.substring(index+1);
				}
//				System.out.println("wsaServiceNameContent.: "+wsaServiceNameContent);
				NamedNodeMap nodes = wsaServiceName.getAttributes();
				String portNameNodeContent = nodes.getNamedItem("PortName").getNodeValue();
//				System.out.println("portNameNode.:"+portNameNodeContent);
				pddData.get(key).put("service", wsaServiceNameContent);
				pddData.get(key).put("port", portNameNodeContent);
			}
		}
		return pddData;
	}

//	private Map<String, Map<String,String>> _parsePDDByXMLBEAN(URL bprFileURL) throws IOException, XmlException, URISyntaxException {
//		JarFileBprAccessor bpra = new JarFileBprAccessor(bprFileURL.toURI());
//		InputStream is = bpra.getPddInputStream(); 
//		StringBuffer strBuffer = new StringBuffer();
//		BufferedReader br = new BufferedReader(new InputStreamReader(is));
//		int i;
//		do {
//			i = br.read();
//			if(i!=-1)
//				strBuffer.append((char)i);
//		} while (i != -1);
//		is.close();
//		//Copy pdd contents in a string...
//		String pddAsString = strBuffer.toString();
////		System.out.println("PDD content:\n");
////		System.out.println(strBuffer.toString()+"\n");
//		//Working with XmlBean classes...
//		ProcessDocument process = ProcessDocument.Factory.parse(pddAsString);
//		String processName = process.getProcess().getName().getLocalPart();
////		System.out.println("Process name.: "+processName);
//		//Access the partner links...
//		PartnerLinkType[] plinks = process.getProcess().getPartnerLinks().getPartnerLinkArray();
////		System.out.println("Numero partner link.: "+plinks.length+"\n");
//		Map<String, Map<String,String>> pddData = new HashMap<String, Map<String,String>>();
//		for (PartnerLinkType partnerLinkType : plinks) {
//			if(partnerLinkType.getPartnerRole()!=null && partnerLinkType.getPartnerRole().getInvokeHandler().equals(invokeHandler)){
////				System.out.println("invokeHandler.:"+partnerLinkType.getPartnerRole().getInvokeHandler());
////				System.out.println(partnerLinkType.getPartnerRole().toString());
//				String partnerLinkName = partnerLinkType.getName();
//				String key = processName+":"+partnerLinkName;
//				pddData.put(key, new HashMap<String, String>());
//				PartnerRoleType pddPartnerRole = partnerLinkType.getPartnerRole();
//				XmlCursor cursor = pddPartnerRole.newCursor();
//				cursor.toFirstChild();//endpoint reference element
//				cursor.push();
//				cursor.toChild(0);//Address
////				System.out.println("Address.:"+cursor.getTextValue());
//				pddData.get(key).put("address", cursor.getTextValue());
//				cursor.pop();
//				cursor.toChild(1);//Metadata
//				cursor.toChild(0);//ServiceName
//				String serviceName = cursor.getTextValue();
//				int index = 0;
//				if (serviceName.contains(":")){
//					index = serviceName.indexOf(":");
//					serviceName = serviceName.substring(index+1);
//				}
////				System.out.println("ServiceName.:"+serviceName);
//				pddData.get(key).put("service", serviceName);
//				String portName = cursor.getDomNode().getAttributes().getNamedItem("PortName").getNodeValue();
////				System.out.println("PortName.:"+portName);
//				pddData.get(key).put("port", portName);
//			}
//		}
//
//		return pddData;
//	}

	public ServiceInstance getInstance() {
		ServiceInstance retInstance = null;
		return retInstance ;
	}

	public List<SensorSubscriptionData> getSensorSubscriptionData() {

		
		/*ActiveBpelAdminStub stub = null;
		try {
			stub = new ActiveBpelAdminStub("http://ORCServices:8080/active-bpel/services/ActiveBpelAdmin");
		} catch (AxisFault e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		System.out.println("\n[DOE - facade] Stub correctly initialized: host.:"+adminServiceURL);	
		
		*/
		
		ArrayList<SensorSubscriptionData> subDatas = new ArrayList<SensorSubscriptionData>();
		
		ArrayList<String> channels = new ArrayList<String>();
		
		 
		/*GetSensorConfigurationsInput getSensorConfigurationsInput = new GetSensorConfigurationsInput();
		
		GetSensorConfigurationsRequestType getSensorsConfigs = new GetSensorConfigurationsRequestType();
		
		getSensorsConfigs.setServiceID(serviceID);
		
		getSensorConfigurationsInput.setGetSensorConfigurationsInput(getSensorsConfigs);
		
		GetSensorConfigurationsOutput response = null;
		
		try {
			response = stub.getSensorConfigurations(getSensorConfigurationsInput);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
		for (int i=0; i< currentSensorConfigurations.size(); i++) {
			
			DOESensorConfiguration doeConfig = currentSensorConfigurations.get(i);
			
			OutputReceiver[] receivers = doeConfig.getOutputReceivers();
			
			SensorSubscriptionData subData = new SensorSubscriptionData();
			
			subData.setSensorID(this.sensorID);
			subData.setBusConfiguration(this.getBusConfiguration(null));
			
			String[] arrayChannels = new String[receivers.length];

			for (int j=0; j< receivers.length; j++){
				arrayChannels[j] = receivers[j].getUuid();
			}
			
			subData.setChannels(arrayChannels);
			
			subDatas.add(subData);
			
		}
		
		
		
		/*ArrayOf_tns2_SensorConfiguration configs = response.getGetSensorConfigurationsOutput().getSensorConfigurations();
		 
		SensorConfiguration[]  arrayOfConfigs = configs.getItem();
		
		for (int i=0; i<arrayOfConfigs.length; i++) {
			
			SensorConfiguration config = arrayOfConfigs[i];
			
			String[] targets = config.getTargetEPR();
			
			for (int j=0; j< targets.length; j++) {
				
				channels.add(targets[j]);
				
			}
			
		}*/
		

		
		
		
		
		return subDatas;
		

		
	}

	private Settings getBusConfiguration(String string) {
		
		Settings settings = new Settings();
		
		settings.setSetting(Setting.pubsub, "xmpp");
		settings.setSetting(Setting.xmpp_username, "primitive-ecf");
		settings.setSetting(Setting.xmpp_password, "primitive-ecf");
		settings.setSetting(Setting.xmpp_host, "testbed.sla-at-soi.eu");
		settings.setSetting(Setting.xmpp_port, "5222");
		settings.setSetting(Setting.messaging, "xmpp");
		settings.setSetting(Setting.pubsub, "xmpp");
		settings.setSetting(Setting.xmpp_service, "testbed.sla-at-soi.eu");
		settings.setSetting(Setting.xmpp_resource, "test");
		settings.setSetting(Setting.xmpp_pubsubservice, "pubsub.testbed.sla-at-soi.eu");
		
		
		return null;
	}
	
	

}
