package it.polimi.MA.impl.doe;

import it.polimi.MA.service.IEffectorAction;

import org.slasoi.models.scm.ServiceBuilder;


/**
 * @author zampognaro
 *
 */
public class UpdateBinding implements IEffectorAction {
	
	private Types type = null;
	private ServiceBuilder builder = null;

	public UpdateBinding(ServiceBuilder builder) {
		this.type = IEffectorAction.Types.UPDATE_BINDIG;
		this.builder = builder;
	}

	public Types getActionType() {
		return type;
	}

	public ServiceBuilder getServiceBuilder() {
		return builder;
	}

}
