package it.polimi.MA.impl.doe;

import org.slasoi.monitoring.common.configuration.impl.SensorConfigurationImpl;



public class DOESensorConfiguration extends SensorConfigurationImpl {

	
	private String serviceID ;
	private String operationID;
	private String status;
	private String correlationKey;
	private String correlationValue;
	
	public String getServiceID() {
		return serviceID;
	}
	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}
	public String getOperationID() {
		return operationID;
	}
	public void setOperationID(String operationID) {
		this.operationID = operationID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCorrelationKey() {
		return correlationKey;
	}
	public void setCorrelationKey(String correlationKey) {
		this.correlationKey = correlationKey;
	}
	public String getCorrelationValue() {
		return correlationValue;
	}
	public void setCorrelationValue(String correlationValue) {
		this.correlationValue = correlationValue;
	}

	
	
	

}
