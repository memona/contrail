package it.polimi.MA.service;

import java.util.List;

import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.monitoring.common.configuration.MonitoringSystemConfiguration;

/**
 * This interface provides a generic access point for the SoftwareServiceManager
 * to use case specific ManageabilityAgents. To correctly implement this
 * interface, you need to consider the following:
 * 
 * There exists exactly (!) one IManageabilityAgentFacade per service instance.
 * If multiple service instances are managed by a single agent, the requests are
 * dispatched by the Facade.
 * 
 * The methods of the Facade are kept on a general level. Basically, all more
 * specific actions of a domain-specific ManageabilityAgent have to be realised
 * through the "executeAction" method.
 * 
 * @author Sam, Davide, Miha, Jens
 * 
 **/
public interface IManageabilityAgentFacade {

		
	/**
	 * @return Returns the ServiceInstance that is controlled by the
	 *         ManageabilityAgent.
	 */
	public ServiceInstance getInstance();

	/**
	 * @return Returns a list of available sensors and where to find their data.
	 */
	public List<SensorSubscriptionData> getSensorSubscriptionData();


	/**
	 * @return Return all sensors associated to the managed instance.
	 */
	//public List<ISensor> getSensors();


	/**
	 * Applies the given configuration to the monitoring system of the managed
	 * ServiceInstance. The given configuration replaces the previous one (if
	 * any).
	 * 
	 * @param configuration
	 *            Configuration to be applied.
	 */
	public void configureMonitoringSystem(MonitoringSystemConfiguration configuration);

	/**
	 * Disable the current configuration.
	 */
	public void deconfigureMonitoring();

	/**
	 * Execute domain-specific action for the adjustment of a ServiceInstance.
	 * 
	 * @param action
	 *            Domain-specific implementation of the IEffectorAction
	 *            interface that contains information about what action to
	 *            execute and its parameters.
	 * @return The result of the action in a domain-specific format.
	 * @throws Exception 
	 */
	public IEffectorResult executeAction(IEffectorAction action) throws Exception;
	



}
