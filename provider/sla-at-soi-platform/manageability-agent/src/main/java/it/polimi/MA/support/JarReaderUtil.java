package it.polimi.MA.support;

import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author paolo zampognaro
 * 
 */
public class JarReaderUtil {

	URI bprlLocation = null;
	/** jar file being read */
	private JarFile mJarFile;

	public JarReaderUtil(URI bprlLocation) throws IOException {
		 mJarFile = new JarFile( bprlLocation.toURL().getFile() );
	}

	/**
	 * Collection of JarEntry names acceptable by the filter.
	 * 
	 * @param aFilter
	 *            select entries based on filename
	 * @return matching jar entry file names
	 */
	public Collection getEntryNames(FilenameFilter aFilter) {
		List matches = new ArrayList();
		for (Enumeration e = mJarFile.entries(); e.hasMoreElements();) {
			JarEntry jarEntry = (JarEntry) e.nextElement();

			if (aFilter.accept(null, jarEntry.getName())) {
				matches.add(jarEntry.getName());
			}
		}
		return matches;
	}
	
	public JarFile getJar(){
		return mJarFile;
	}

}
