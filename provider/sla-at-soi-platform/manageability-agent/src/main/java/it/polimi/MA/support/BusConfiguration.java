package it.polimi.MA.support;

public class BusConfiguration {
	
	protected String resource;
	protected String username;
	protected String password;
	protected String host;
	protected String service;
	protected String pubsub;
	protected String xmppp_pubsubservice;
	protected String xmpp_port;
	
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	
	public String getPubsub() {
		return pubsub;
	}
	public void setPubsub(String pubsub) {
		this.pubsub = pubsub;
	}
	
	public String getXmppp_pubsubservice() {
		return xmppp_pubsubservice;
	}
	public void setXmppp_pubsubservice(String xmppp_pubsubservice) {
		this.xmppp_pubsubservice = xmppp_pubsubservice;
	}
	
	public String getXmpp_port() {
		return xmpp_port;
	}
	public void setXmpp_port(String xmpp_port) {
		this.xmpp_port = xmpp_port;
	}

}
