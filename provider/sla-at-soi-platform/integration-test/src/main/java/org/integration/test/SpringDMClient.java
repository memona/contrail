/**
 * Copyright (c) 2008-2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Nawaz Khurshid- khurshid@fbk.eu
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.integration.test;

import it.polimi.MA.service.MAService;

//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
import java.util.Hashtable;
//import java.util.Properties;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.slasoi.businessManager.common.service.PartyManager;
import org.slasoi.businessManager.common.service.PartyPartyRoleManager;
//import org.slasoi.gslam.core.context.GenericSLAManagerServices;
import org.slasoi.gslam.core.context.
GenericSLAManagerServices.CreateContextGenericSLAManagerException;
import org.slasoi.gslam.core.context.GenericSLAManagerUtils;
import org.slasoi.gslam.core.context.
GenericSLAManagerUtils.GenericSLAManagerUtilsException;
import org.slasoi.gslam.core.context.
SLAManagerContext.SLAManagerContextException;
import org.slasoi.gslam.core.monitoring.IMonitoringManager;
import org.slasoi.ism.occi.IsmOcciService;
import org.slasoi.seval.prediction.service.IServiceEvaluation;
import org.slasoi.seval.prediction.service.ISoftwareServiceEvaluation;
import org.slasoi.seval.prediction.exceptions.UnboundDependencyException;
import org.slasoi.seval.prediction.exceptions.UnsupportedTermException;
import org.slasoi.seval.repository.exceptions.ModelNotFoundException;
//import org.slasoi.softwareservicemanager.IManageSoftwareServices;
//import org.slasoi.softwareservicemanager.IPrepareSoftwareService;
import org.slasoi.softwareservicemanager.ISoftwareServiceManagerFacade;
import org.slasoi.monitoring.city.service.CityRCGService;
import org.slasoi.monitoring.fbk.service.FbkRCGService;

/**
 *
 * @author khurshid
 *
 */
public class SpringDMClient implements BundleActivator {

    //private GenericSLAManagerServices gslamServices = null;
    /** GSLAM Service. **/
    private GenericSLAManagerUtils gslamUtils = null;
    /** Bundle context variable. **/
    private BundleContext osgiContext = null;
    /** OSGI Service tracker variable. **/
    private ServiceTracker tracker = null;
    /** ISM Service. **/
    private IsmOcciService ismServices = null;
    /** software service manager 'Prepare' interaction  service. **/
    //private IPrepareSoftwareService iPrepareSWService = null;
    /** software service manager 'Manage' interaction  service. **/
    //private IManageSoftwareServices iManageSWService = null;
    /** software service manager service. **/
    private ISoftwareServiceManagerFacade iSSMFacade = null;
    /** service evaluation service. **/
    private ISoftwareServiceEvaluation sevalServices = null;
    /** manage ability agent service. **/
    private MAService manageabilityAgentService = null;
    /** business manager party role service. **/
    private PartyPartyRoleManager partyRoleService = null;
    /** business manager party manager service. **/
    private PartyManager partyManagerService = null;
    /** FBKRcg service. **/
    private FbkRCGService rcgFBKService = null;
    /** CityRcg service. **/
    private CityRCGService rcgCityService = null;
    /** IMonitoringManager service. **/
    private IMonitoringManager mmService = null;
    /** property variable. **/
    //private static Properties testProps = new Properties();
    /** scenarioId. **/
    private int scenarioId;
    /** negotiation scenario operation code. **/
    private int negotiationOpCode;
    /** provision scenario operation code. **/
    private int provisionOpCode;
    /** monitoring runtime scenario operation code. **/
    private int runtimeOpCode;

    /*
     * @ServiceReference public void setGslamServices(GenericSLAManagerServices
     * gslamServices) {
     * System.out.println("Generic-Slam injected successfully");
     * this.gslamServices = gslamServices; }
     */

    /*
     * @ServiceReference public void
     * setGslamManagerUtils(org.slasoi.gslam.core.context.GenericSLAManagerUtils
     * gslamUtilsServices) {
     * System.out.println("Generic-Slam Utility service injected successfully");
     * this.gslamUtils = gslamUtilsServices; }
     */

    /*
     * @ServiceReference public void setISMContext(IsmOcciService ismContext){
     * System.out.println("ISM Services injected successfully");
     * this.ismServices = ismContext; }
     * 
     * @ServiceReference public void
     * setISSMFacadeContext(org.slasoi.softwareservicemanager
     * .ISoftwareServiceManagerFacade iSSMFacade){
     * System.out.println("ISSMFacade Software Services injected successfully");
     * this.iSSMFacade = iSSMFacade; }
     * 
     * @ServiceReference public void setIEvaluateServices(IServiceEvaluation
     * sevalServices) {
     * System.out.println("Service Evaluation injected successfully");
     * this.sevalServices = sevalServices; }
     * 
     * @ServiceReference public void setLLMSIMDHServices(
     * IMonitoringDataAndHistory llmsIMDHService ) {
     * 
     * System.out.println(
     * "LLMS:IMonitoringDataAndHistory service injected successfully");
     * this.llmsIMDHService = llmsIMDHService; }
     * 
     * @ServiceReference public void setLLMSServices( IConfigureMonitoring
     * llmsICService ) {
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * System.out.println("LLMS:IConfigureMonitoring service injected successfully"
     * ); this.llmsICService = llmsICService; }
     * 
     * @ServiceReference public void
     * setLLMSInstanceServices(IServiceInstance2Monitoring llmsInstanceService )
     * {
     * 
     * System.out.println("LLMS:Instance service injected successfully");
     * this.llmsInstanceService = llmsInstanceService; }
     * 
     * @ServiceReference public void setMAServices(MAService
     * manageabilityAgentService ) {
     * 
     * System.out.println("Manageability Agent service injected successfully");
     * this.manageabilityAgentService = manageabilityAgentService; }
     * 
     * public void setBundleContext( BundleContext osgiContext ){
     * this.osgiContext = osgiContext; }
     */

    /**
     * Start method.
     @param context bundle context
     @throws Exception exception
     */
    public final void start(final BundleContext context) throws Exception {

        IntegrationListener il = new IntegrationListener();
        context.registerService(il.getClass().getName(), il, null);

        // GSLAM
        this.osgiContext = context;
        tracker = new ServiceTracker(context, GenericSLAManagerUtils.class
                .getName(), null);
        tracker.open();
        gslamUtils = (GenericSLAManagerUtils) tracker.waitForService(2000);
        if (gslamUtils != null) {
            System.out.println("GSLAM Services injected successfully");
        }

        // ISM
        tracker = new ServiceTracker(context, IsmOcciService.class.getName(),
                null);
        tracker.open();
        ismServices = (IsmOcciService) tracker.waitForService(2000);
        if (ismServices != null) {
            System.out.println("ISM Services injected successfully");
        }

        // SSM
        tracker = new ServiceTracker(context,
                ISoftwareServiceManagerFacade.class.getName(), null);
        tracker.open();
        iSSMFacade = (ISoftwareServiceManagerFacade) tracker
                .waitForService(2000);
        if (iSSMFacade != null) {
            System.out.println("ISSMFacade Software Services"
                    + " injected successfully");
        }

        // SE
        tracker = new ServiceTracker(context, IServiceEvaluation.class
                .getName(), null);
        tracker.open();
        sevalServices = (ISoftwareServiceEvaluation) tracker.waitForService(2000);
        if (sevalServices != null) {
            System.out.println("Service Evaluation injected successfully");
        }

        // MA
        tracker = new ServiceTracker(context, MAService.class.getName(), null);
        tracker.open();
        this.manageabilityAgentService = (MAService) tracker
                .waitForService(2000);
        if (manageabilityAgentService != null) {
            System.out.println("Manageability Agent service"
                    + " injected successfully");
        }

        // LLMS
        /*tracker = new ServiceTracker(context, IMonitoringDataAndHistory.class
                .getName(), null);
        tracker.open();
        llmsIMDHService = (IMonitoringDataAndHistory) tracker
                .waitForService(2000);
        if (llmsIMDHService != null) {
            System.out.println("LLMS:IMonitoringDataAndHistory service"
                    + " injected successfully");
        }

        tracker = new ServiceTracker(context, IConfigureMonitoring.class
                .getName(), null);
        tracker.open();
        llmsICService = (IConfigureMonitoring) tracker.waitForService(2000);
        if (llmsICService != null) {
            System.out.println("LLMS:IConfigureMonitoring service"
                    + " injected successfully");
        }

        tracker = new ServiceTracker(context, IServiceInstance2Monitoring.class
                .getName(), null);
        tracker.open();
        llmsInstanceService = (IServiceInstance2Monitoring) tracker
                .waitForService(2000);
        if (llmsInstanceService != null) {
            System.out.println("LLMS:Instance service injected successfully");
        }*/

        tracker = new ServiceTracker(context, PartyPartyRoleManager.class
                .getName(), null);
        tracker.open();
        partyRoleService = (PartyPartyRoleManager) tracker.waitForService(2000);
        if (partyRoleService != null) {
            System.out.println("BM:PartyPartyRoleManager service"
                    + " injected successfully");
        }

        tracker = new ServiceTracker(context, PartyManager.class.getName(),
                null);
        tracker.open();
        partyManagerService = (PartyManager) tracker.waitForService(2000);
        if (partyManagerService != null) {
            System.out.println("BM:PartyManager service injected successfully");
        }

        tracker = new ServiceTracker(context, FbkRCGService.class.getName(),
                null);
        tracker.open();
        rcgFBKService = (FbkRCGService) tracker.waitForService(2000);
        if (rcgFBKService != null) {
            System.out.println("FBKRcg service injected successfully");
        }

        tracker = new ServiceTracker(context, CityRCGService.class.getName(),
                null);
        tracker.open();
        rcgCityService = (CityRCGService) tracker.waitForService(2000);
        if (rcgCityService != null) {
            System.out.println("CityRcg service injected successfully.");
        }

        tracker = new ServiceTracker(context, IMonitoringManager.class.getName(),
                null);
        tracker.open();
        mmService = (IMonitoringManager) tracker.waitForService(2000);
        if (mmService != null) {
            System.out.println("IMonitoringManager service injected successfully.");
        }

    }

    /**
     * start scenario method.
     * @throws Exception exception
     */
    public final void startScenario() throws Exception {
        switch (scenarioId) {

        case 1:
            // Negotiation
            System.out.println("\n\n\n\n####################################"
                    + " NEGOTIATION SCENARIO RUN"
                    + " ########################\n\n\n\n");
            runNegotiation(negotiationOpCode);
            break;
        case 2:
            // Provision
            System.out.println("\n\n\n\n####################################"
                    + " PROVISIONING SCENARIO RUN "
                    + "########################\n\n\n\n");
            runProvision(provisionOpCode);
            break;
        case 3:
            // RunTime
            System.out.println("\n\n\n\n####################################"
                    + " MONITORING SCENARIO RUN"
                    + " ########################\n\n\n\n");
            runMonitoring(runtimeOpCode);
            break;
        case 4:
            runNegotiation(negotiationOpCode);
            runProvision(provisionOpCode);
            runMonitoring(runtimeOpCode);
            break;
        default:
            break;
        }
    }

    /**
     * Negotiation Scenario Manager.
     * @param opcode integer
     * @throws CreateContextGenericSLAManagerException exception
     * @throws GenericSLAManagerUtilsException exception
     * @throws SLAManagerContextException exception
     * @throws ModelNotFoundException e
     * @throws UnboundDependencyException e
     * @throws UnsupportedTermException e
     */
    private void runNegotiation(final int opcode)
            throws CreateContextGenericSLAManagerException,
            GenericSLAManagerUtilsException, SLAManagerContextException, UnsupportedTermException,
            UnboundDependencyException, ModelNotFoundException {

        NegotiationScenario negotiation = NegotiationScenario.getInstance(
                gslamUtils, partyRoleService, partyManagerService, osgiContext);

        switch (opcode) {

        case 0:
            negotiation.setPolicies();
            break;
        case 1:
            negotiation.register();
            break;
        case 2:
            negotiation.getProducts();
            break;
        case 3:
            negotiation.getTemplates();
            break;
        case 4:
            negotiation.querySLARegistry();
            break;
        case 5:
            negotiation.authenticateUser();
            break;
        case 6:
            negotiation.negotiateGSLAM();
            break;
        case 7:
            negotiation.checkCustomer();
            break;
        case 8:
            negotiation.querySoftwareSLATemplates();
            break;
        case 9:
            negotiation.negotiateSWSLAM();
            break;
        case 10:
            negotiation.querySSM(iSSMFacade);
            break;
        case 11:
            negotiation.queryISSLAM();
            break;
        case 12:
            negotiation.negotiateISSLAM();
            break;
        case 13:
            negotiation.queryISM(ismServices);
            break;
        case 14:
            negotiation.evaluate(sevalServices);
            break;
        case 15:
            negotiation.negotiateISSLAMSecond();
            break;
        case 16:
            negotiation.reserveISM(ismServices);
            break;
        case 17:
            negotiation.reserveSoftwareSM(iSSMFacade);
            break;
        case 18:
            negotiation.customize();
            break;
        case 19:
            negotiation.createAgreementGSLAM();
            break;
        case 20:
            negotiation.assess();
            break;
        case 21:
            negotiation.createAgreementSoftwareSLAM();
            break;
        case 22:
            negotiation.createAgreementInfrastructureSLAM();
            break;
        case 23:
            negotiation.commitInfrastructureSM(ismServices);
            break;
        case 24:
            negotiation.bookSoftwareSM(iSSMFacade);
            break;
        case 1500:
            negotiation.register();
            negotiation.getProducts();
            negotiation.getTemplates();
            negotiation.querySLARegistry();
            negotiation.authenticateUser();
            negotiation.negotiateGSLAM();
            negotiation.checkCustomer();
            negotiation.querySoftwareSLATemplates();
            negotiation.negotiateSWSLAM();
            negotiation.querySSM(iSSMFacade);
            negotiation.queryISSLAM();
            negotiation.negotiateISSLAM();
            negotiation.queryISM(ismServices);
            negotiation.evaluate(sevalServices);
            negotiation.negotiateISSLAMSecond();
            negotiation.reserveISM(ismServices);
            negotiation.reserveSoftwareSM(iSSMFacade);
            negotiation.customize();
            negotiation.createAgreementGSLAM();
            negotiation.assess();
            negotiation.createAgreementSoftwareSLAM();
            negotiation.createAgreementInfrastructureSLAM();
            negotiation.commitInfrastructureSM(ismServices);
            negotiation.bookSoftwareSM(iSSMFacade);
            break;
        case 1600: // ORC Run
            negotiation.runORC();
            break;
        default:
            break;
        }
    }

    /**
     * Provisioning scenario controller.
     * @param opcode integer
     * @throws CreateContextGenericSLAManagerException exception
     * @throws GenericSLAManagerUtilsException exception
     * @throws SLAManagerContextException exception
     */
    private void runProvision(final int opcode)
            throws CreateContextGenericSLAManagerException,
            GenericSLAManagerUtilsException, SLAManagerContextException {

        ProvisioningScenario provision = ProvisioningScenario.getInstance(
                gslamUtils, partyRoleService, partyManagerService, manageabilityAgentService, osgiContext);

        switch (opcode) {

        case 0:
            provision.register();
            break;
        case 1:
            provision.provisionGSLAM();
            break;
        case 2:
            provision.provisionSoftwareSLAM();
            break;
        case 3:
            provision.provisionInfrastructureSLAM();
            break;
        case 4:
            provision.provisionInfrastructureSM(ismServices);
            break;
        case 5:
            provision.startInstanceSoftwareSM();
            break;
        case 6:
            provision.createInfraService();
            break;
        case 7:
            provision.creationMonitorEventChannel();
            break;
        case 8:
            provision.subscribeInfraStructureSLAM();
            break;
        case 9:
            provision.reportSoftwareSLAM();
            break;
        case 10:
            provision.createSWServiceMA();
            break;
        case 11:
            provision.publishEventMonitoredEventChannel();
            break;
        case 12:
            provision.subscribeEeventMEToSWSLAM();
            break;
        case 13:
            provision.trackEvent();
            break;
        case 1500:
            provision.register();
            provision.provisionGSLAM();
            provision.provisionSoftwareSLAM();
            provision.provisionInfrastructureSLAM();
            provision.provisionInfrastructureSM(ismServices);
            provision.startInstanceSoftwareSM();
            provision.createInfraService();
            provision.creationMonitorEventChannel();
            provision.subscribeInfraStructureSLAM();
            provision.reportSoftwareSLAM();
            provision.createSWServiceMA();
            provision.publishEventMonitoredEventChannel();
            provision.subscribeEeventMEToSWSLAM();
            provision.trackEvent();
            break;
        case 1600:
            provision.runORC();
            break;
        default:
            break;
        }
    }

    /**
     * Monitoring scenario controller.
     *
     * @param opcode integer
     * @throws CreateContextGenericSLAManagerException exception
     * @throws GenericSLAManagerUtilsException exception
     * @throws SLAManagerContextException exception
     */
    private void runMonitoring(final int opcode)
            throws CreateContextGenericSLAManagerException,
            GenericSLAManagerUtilsException, SLAManagerContextException {

        RunTimeMonitoringScenario runtimeMonitoring = RunTimeMonitoringScenario
                .getInstance(gslamUtils, osgiContext, rcgFBKService, rcgCityService, mmService);

        switch (opcode) {

        case 1:
            runtimeMonitoring.getReport();
            break;
        case 2:
            break;
        case 3:
            runtimeMonitoring.slaViolation(rcgFBKService);
            break;
        case 4:
            runtimeMonitoring.reprovision(ismServices);
            break;
        case 5:
            runtimeMonitoring.validateUploadTemplate();
        case 6:
            runtimeMonitoring.generateEvents();
            break;
        case 7:
            runtimeMonitoring.executeAction(manageabilityAgentService);
            break;
        case 8:
            runtimeMonitoring.testSLAParsing();
            break;
        case 1500:
            break;
        case 1600:
            runtimeMonitoring.runORC();
            break;
        case 1700:
            runtimeMonitoring.orcRunWithEverest();
        default:
            break;

        }
    }

    /**
     * Stop bundle method.
     *
     * @param arg0
     *            context
     * @throws Exception
     *             e
     */
    public final void stop(final BundleContext arg0) throws Exception {
        // TODO Auto-generated method stub
        System.out.println("****************"
                + "Integration BUNDLE STOPPED ******************");
        tracker.close();
    }

    /**
     * IntegrationListener would be just invoked from osgi-console by 'invoke'
     * command.
     *
     * Usage: case1 : invoke integration-bundle-id run scenario=1 NO=1600 case2
     * : invoke integration-bundle-id run scenario=2 PO=1500 case3 : invoke
     * integration-bundle-id run scenario=3 RT=1600
     */
    public class IntegrationListener {
        /**
         * run.
         *
         * @param params table
         */
        public final void run(final Hashtable<String, String> params) {
            try {
                scenarioId = value(params, "scenario");
                negotiationOpCode = value(params, "NO");
                provisionOpCode = value(params, "PO");
                runtimeOpCode = value(params, "RT");

                System.out.println("Integration Bundle will be invoked :");
                System.out.println("\tscenarioID        = " + scenarioId);
                System.out
                        .println("\tnegotiationOpCode = " + negotiationOpCode);
                System.out.println("\tprovisionOpCode   = " + provisionOpCode);
                System.out.println("\truntimeOpCode     = " + runtimeOpCode);

                startScenario();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * Help information.
         */
        public final void help() {
            System.out.println("Usage:  osgi>invoke #bundleid run"
                    + " scenario=#id [NO=#id | PO=#id | RT=#id]");
        }

        /**
         * Return value from hash table.
         *
         * @param params
         *            table
         * @param key
         *            string
         * @return integer
         */
        protected final int value(final Hashtable<String, String> params,
                final String key) {
            String v = params.get(key);
            if (v != null) {
                return Integer.parseInt(v);
            }

            return -1;
        }
    }
}
