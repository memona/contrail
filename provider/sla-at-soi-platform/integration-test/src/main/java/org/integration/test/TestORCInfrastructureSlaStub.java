/**
 * Copyright (c) 2008-2010, FBK
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of FBK nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL FBK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Nawaz Khurshid- khurshid@fbk.eu
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.integration.test;

import org.slasoi.slamodel.core.FunctionalExpr;
import org.slasoi.slamodel.core.SimpleDomainExpr;
import org.slasoi.slamodel.core.TypeConstraintExpr;
import org.slasoi.slamodel.primitives.CONST;
import org.slasoi.slamodel.primitives.ID;
import org.slasoi.slamodel.primitives.STND;
import org.slasoi.slamodel.primitives.UUID;
import org.slasoi.slamodel.service.ResourceType;
import org.slasoi.slamodel.sla.AgreementTerm;
import org.slasoi.slamodel.sla.Endpoint;
import org.slasoi.slamodel.sla.Guaranteed;
import org.slasoi.slamodel.sla.InterfaceDeclr;
import org.slasoi.slamodel.sla.SLATemplate;
import org.slasoi.slamodel.vocab.core;
import org.slasoi.slamodel.vocab.resources;
import org.slasoi.slamodel.vocab.units;

/**
 * This is an ORC Infrastructure (resource) SLA template stub created
 * programmatically. For testing purposes only. Please refer to the XML document
 * instead when looking for the current and complete version.
 *
 * @author kuester
 */
@SuppressWarnings("serial")
public class TestORCInfrastructureSlaStub extends SLATemplate {

    /**
     * constructor.
     */
    public TestORCInfrastructureSlaStub() {
        super();
        this.setInterfaceDeclrs(generateInterfaceDeclrs());
        this.setAgreementTerms(generateQoSTerms());
    }

    /**
     * @return InterfaceDecl[]
     */
    private InterfaceDeclr[] generateInterfaceDeclrs() {
        InterfaceDeclr decl = new InterfaceDeclr(new ID("VM_TYPE_0"), new ID(
                "provider"), new ResourceType("Humba"));
        decl.setEndpoints(generateEndpoints());
        return new InterfaceDeclr[] {decl};
    }

    /**
     *
     * @return Array
     */
    private AgreementTerm[] generateQoSTerms() {
        AgreementTerm term = new AgreementTerm(new ID("VM_0_TERM"), null, null,
                generateCPUGuaranteed());
        return new AgreementTerm[] {term};
    }

     /**
     *
     * @return Array
     */
    private Endpoint[] generateEndpoints() {
        Endpoint point = new Endpoint(new ID("Endpoint0"), new STND("http"));
        point.setId(new ID("VM_0_IP"));
        point.setLocation(new UUID("IPOfRunningVM"));
        return new Endpoint[] {point};
    }

    /**
     * @return CPU_Speed(VM_TYPE_0) = 3.0 GHz && quantity(VM_0_IP) = 1
     */
    private Guaranteed[] generateCPUGuaranteed() {
        Guaranteed.State speedState = new Guaranteed.State(new ID(
                "VM_0_CPU_SPEED"), new TypeConstraintExpr(new FunctionalExpr(
                resources.cpu_speed, generateId("VM_TYPE_0")),
                new SimpleDomainExpr(
                        new CONST("3.0", units.GHz), core.equals)));
        Guaranteed.State quantityState = new Guaranteed.State(
                new ID("Quantity"),
                new TypeConstraintExpr(new FunctionalExpr(new STND("quantity"),
                        generateId("VM_0_IP")), new SimpleDomainExpr(new CONST(
                        "1", null), core.equals)));
        Guaranteed[] guarantees = {speedState, quantityState};
        return guarantees;
    }

    /**
     * transform strings to id's.
     *
     * @param s string
     * @return array ID
     */
    private ID[] generateId(final String s) {
        return new ID[] {new ID(s)};
    }

}
