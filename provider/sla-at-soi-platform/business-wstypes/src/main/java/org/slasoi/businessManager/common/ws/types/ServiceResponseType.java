package org.slasoi.businessManager.common.ws.types;


public class ServiceResponseType {

    private int responseCode;
    private String responseMessage;
    private Service [] serviceList;

    public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public Service[] getServiceList() {
		return serviceList;
	}
	public void setServiceList(Service[] serviceList) {
		this.serviceList = serviceList;
	}
	
}
