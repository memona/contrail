package org.slasoi.businessManager.common.ws.types;


public class RatingResponseType {

    private int responseCode;
    private String responseMessage;
    private RatingType rating;

    public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public RatingType getRating() {
		return rating;
	}
	public void setRating(RatingType rating) {
		this.rating = rating;
	}
	
}
