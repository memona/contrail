package org.slasoi.businessManager.common.ws.types;

public enum Type {    
    INDI("I"),
    ORGA("O");
    
    private String type;
    Type(String type){
        this.type=type;
    }
   
}
