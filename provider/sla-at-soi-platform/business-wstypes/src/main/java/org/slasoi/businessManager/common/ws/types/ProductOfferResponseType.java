package org.slasoi.businessManager.common.ws.types;

import org.slasoi.businessManager.common.ws.types.ParameterDataType;

public class ProductOfferResponseType {

    private int responseCode;
    private String responseMessage;
    private ProductOffer [] productOfferList;
	
    public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public ProductOffer[] getProductOfferList() {
		return productOfferList;
	}
	public void setProductOfferList(ProductOffer[] productOfferList) {
		this.productOfferList = productOfferList;
	}
	
}
