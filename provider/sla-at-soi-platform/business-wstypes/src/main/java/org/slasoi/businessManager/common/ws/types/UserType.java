package org.slasoi.businessManager.common.ws.types;

public class UserType {
    private String userLogin;
    private String passwd;
    
    public String getUserLogin() {
        return userLogin;
    }
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
    public String getPasswd() {
        return passwd;
    }
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    
}
