package org.slasoi.businessManager.common.ws.types;

public class ParameterType {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ParameterType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Country = "Country";
    public static final java.lang.String _Currency = "Currency";
    public static final java.lang.String _Language = "Language";
    public static final ParameterType Country = new ParameterType(_Country);
    public static final ParameterType Currency = new ParameterType(_Currency);
    public static final ParameterType Language = new ParameterType(_Language);
    public java.lang.String getValue() { return _value_;}
    public static ParameterType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ParameterType enumeration = (ParameterType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ParameterType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }



}
