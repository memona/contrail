package org.slasoi.businessManager.common.ws.types;

public class GetParameterListResponseType {
    private int responseCode;
    private String responseMessage;
    private ParameterDataType [] parameterList;
    
    
    public GetParameterListResponseType(){}
    
    public GetParameterListResponseType(int responseCode, String responseMessage) {
		super();
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
	}
	public int getResponseCode() {
        return responseCode;
    }
    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
    public String getResponseMessage() {
        return responseMessage;
    }
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
    public ParameterDataType[] getParameterList() {
        return parameterList;
    }
    public void setParameterList(ParameterDataType[] parameterList) {
        this.parameterList = parameterList;
    }
    
    
    
}
