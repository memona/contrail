package org.slasoi.businessManager.common.ws.types;


public class Service {

	private Long id;
	private String name;
	private String description;
	private String release;
    private RatingType rating;

    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRelease() {
		return release;
	}
	public void setRelease(String release) {
		this.release = release;
	}
	public RatingType getRating() {
		return rating;
	}
	public void setRating(RatingType rating) {
		this.rating = rating;
	}
  
}
