/**
 *  SVN FILE: $Id: ServiceTypeExtended.java 2722 2011-07-20 10:41:29Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 2722 $
 * @lastrevision   $Date: 2011-07-20 12:41:29 +0200 (sre, 20 jul 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/
 sla-at-soi/platform/trunk/scm/src/main/java/org/slasoi/models/scm/extended/ServiceTypeExtended.java $
 */

package org.slasoi.models.scm.extended;

import java.util.List;
import java.util.UUID;

import org.slasoi.models.scm.impl.ServiceTypeImpl;

/**
 * A ServiceType describe related set of functionality provided by a service. Related can, for example, mean that the
 * functionality is provided by the same component. In our case, a ServiceType always refers to a set of interfaces.
 *
 * @author Jens Happe
 *
 */
public class ServiceTypeExtended extends ServiceTypeImpl {

    /**
     * Creates a new service type for a given set of interface references.
     *
     */
    public ServiceTypeExtended() {
        super();
        this.id = UUID.randomUUID().toString();
    }

    /**
     * Compares the specified object with this service type for equality. Returns true if the specified object is also a
     * service type, the two service types refer to the same number of interfaces, and every interface of the specified
     * service type is contained in this service type (or equivalently, every interface of this service type is
     * contained in the specified service type).
     *
     * @param o
     *            Object to be compared with.
     * @return Returns true if this Objects is equal to Object o.
     */
    @Override
    public final boolean equals(final Object o) {
        if (o instanceof ServiceTypeExtended) {
            ServiceTypeExtended st = (ServiceTypeExtended) o;
            if (this.getInterfacesLength() == st.getInterfacesLength()) {
                for (String interfaceName : this.getInterfacesList()) {
                    if (!contains(st.getInterfacesList(), interfaceName)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Creates and returns the hashCode for a ServiceTypeExtended Object.
     *
     * @return Returns an integer representing the hash-code of this object..
     */
    public final int hashCode() {
        int result = 0;
        result += this.getInterfacesLength();
        for (String interfaceName : this.getInterfacesList()) {
            for (char c : interfaceName.toCharArray()) {
                result += c;
            }
        }
        return result;
    }

    /**
     * Checks whether a the passed interfaceList contains the passed interfaceName.
     *
     * @param interfaceList
     *            List which has to be scant.
     * @param interfaceName
     *            Name of the target interface
     * @return Returns true if the list contains the passed interface name.
     */
    private boolean contains(final List<String> interfaceList, final String interfaceName) {
        for (String currentName : interfaceList) {
            if (currentName.equals(interfaceName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Converts this ServiceTypeObject to a String.
     *
     * @return Returns a string.
     */
    @Override
    public final String toString() {
        return this.getInterfaces().toString();
    }

}