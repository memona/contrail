/**
 *  SVN FILE: $Id: ReasonerConfigurationImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/monitoring/common/configuration/impl/ReasonerConfigurationImpl.java $
 */

package org.slasoi.monitoring.common.configuration.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.slasoi.monitoring.common.configuration.ConfigurationPackage;
import org.slasoi.monitoring.common.configuration.OutputReceiver;
import org.slasoi.monitoring.common.configuration.ReasonerConfiguration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reasoner Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.monitoring.common.configuration.impl.ReasonerConfigurationImpl#getSpecification <em>Specification</em>}</li>
 *   <li>{@link org.slasoi.monitoring.common.configuration.impl.ReasonerConfigurationImpl#getOutputReceiversList <em>Output Receivers</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReasonerConfigurationImpl extends ComponentConfigurationImpl implements ReasonerConfiguration {
	/**
	 * The default value of the '{@link #getSpecification() <em>Specification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecification()
	 * @generated
	 * @ordered
	 */
	protected static final Object SPECIFICATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpecification() <em>Specification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecification()
	 * @generated
	 * @ordered
	 */
	protected Object specification = SPECIFICATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOutputReceiversList() <em>Output Receivers</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputReceiversList()
	 * @generated
	 * @ordered
	 */
	protected EList<OutputReceiver> outputReceivers;

	/**
	 * The empty value for the '{@link #getOutputReceivers() <em>Output Receivers</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputReceivers()
	 * @generated
	 * @ordered
	 */
	protected static final OutputReceiver[] OUTPUT_RECEIVERS_EEMPTY_ARRAY = new OutputReceiver [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReasonerConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConfigurationPackage.Literals.REASONER_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getSpecification() {
		return specification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecification(Object newSpecification) {
		Object oldSpecification = specification;
		specification = newSpecification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConfigurationPackage.REASONER_CONFIGURATION__SPECIFICATION, oldSpecification, specification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutputReceiver[] getOutputReceivers() {
		if (outputReceivers == null || outputReceivers.isEmpty()) return OUTPUT_RECEIVERS_EEMPTY_ARRAY;
		BasicEList<OutputReceiver> list = (BasicEList<OutputReceiver>)outputReceivers;
		list.shrink();
		return (OutputReceiver[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutputReceiver getOutputReceivers(int index) {
		return getOutputReceiversList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOutputReceiversLength() {
		return outputReceivers == null ? 0 : outputReceivers.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputReceivers(OutputReceiver[] newOutputReceivers) {
		((BasicEList<OutputReceiver>)getOutputReceiversList()).setData(newOutputReceivers.length, newOutputReceivers);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputReceivers(int index, OutputReceiver element) {
		getOutputReceiversList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutputReceiver> getOutputReceiversList() {
		if (outputReceivers == null) {
			outputReceivers = new EObjectResolvingEList<OutputReceiver>(OutputReceiver.class, this, ConfigurationPackage.REASONER_CONFIGURATION__OUTPUT_RECEIVERS);
		}
		return outputReceivers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConfigurationPackage.REASONER_CONFIGURATION__SPECIFICATION:
				return getSpecification();
			case ConfigurationPackage.REASONER_CONFIGURATION__OUTPUT_RECEIVERS:
				return getOutputReceiversList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConfigurationPackage.REASONER_CONFIGURATION__SPECIFICATION:
				setSpecification(newValue);
				return;
			case ConfigurationPackage.REASONER_CONFIGURATION__OUTPUT_RECEIVERS:
				getOutputReceiversList().clear();
				getOutputReceiversList().addAll((Collection<? extends OutputReceiver>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.REASONER_CONFIGURATION__SPECIFICATION:
				setSpecification(SPECIFICATION_EDEFAULT);
				return;
			case ConfigurationPackage.REASONER_CONFIGURATION__OUTPUT_RECEIVERS:
				getOutputReceiversList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConfigurationPackage.REASONER_CONFIGURATION__SPECIFICATION:
				return SPECIFICATION_EDEFAULT == null ? specification != null : !SPECIFICATION_EDEFAULT.equals(specification);
			case ConfigurationPackage.REASONER_CONFIGURATION__OUTPUT_RECEIVERS:
				return outputReceivers != null && !outputReceivers.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (specification: ");
		result.append(specification);
		result.append(')');
		return result.toString();
	}

} //ReasonerConfigurationImpl
