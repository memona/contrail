/**
 *  SVN FILE: $Id: ServiceInstanceImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/ServiceInstanceImpl.java $
 */

package org.slasoi.models.scm.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import org.slasoi.models.scm.ServiceConstructionModelPackage;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.models.scm.ServiceInstance;

import org.slasoi.slamodel.sla.Endpoint;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceInstanceImpl#getImplementation <em>Implementation</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceInstanceImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceInstanceImpl#getServiceInstanceName <em>Service Instance Name</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceInstanceImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceInstanceImpl#getInstancedOn <em>Instanced On</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ServiceInstanceImpl#getEndpointsList <em>Endpoints</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ServiceInstanceImpl extends EObjectImpl implements ServiceInstance {
	/**
	 * The cached value of the '{@link #getImplementation() <em>Implementation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementation()
	 * @generated
	 * @ordered
	 */
	protected ServiceImplementation implementation;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getServiceInstanceName() <em>Service Instance Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceInstanceName()
	 * @generated
	 * @ordered
	 */
	protected static final String SERVICE_INSTANCE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getServiceInstanceName() <em>Service Instance Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceInstanceName()
	 * @generated
	 * @ordered
	 */
	protected String serviceInstanceName = SERVICE_INSTANCE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getInstancedOn() <em>Instanced On</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstancedOn()
	 * @generated
	 * @ordered
	 */
	protected static final Date INSTANCED_ON_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInstancedOn() <em>Instanced On</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstancedOn()
	 * @generated
	 * @ordered
	 */
	protected Date instancedOn = INSTANCED_ON_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEndpointsList() <em>Endpoints</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndpointsList()
	 * @generated
	 * @ordered
	 */
	protected EList<Endpoint> endpoints;

	/**
	 * The empty value for the '{@link #getEndpoints() <em>Endpoints</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndpoints()
	 * @generated
	 * @ordered
	 */
	protected static final Endpoint[] ENDPOINTS_EEMPTY_ARRAY = new Endpoint [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServiceConstructionModelPackage.Literals.SERVICE_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceImplementation getImplementation() {
		if (implementation != null && implementation.eIsProxy()) {
			InternalEObject oldImplementation = (InternalEObject)implementation;
			implementation = (ServiceImplementation)eResolveProxy(oldImplementation);
			if (implementation != oldImplementation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ServiceConstructionModelPackage.SERVICE_INSTANCE__IMPLEMENTATION, oldImplementation, implementation));
			}
		}
		return implementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceImplementation basicGetImplementation() {
		return implementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplementation(ServiceImplementation newImplementation) {
		ServiceImplementation oldImplementation = implementation;
		implementation = newImplementation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_INSTANCE__IMPLEMENTATION, oldImplementation, implementation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		String oldID = id;
		id = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_INSTANCE__ID, oldID, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getServiceInstanceName() {
		return serviceInstanceName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServiceInstanceName(String newServiceInstanceName) {
		String oldServiceInstanceName = serviceInstanceName;
		serviceInstanceName = newServiceInstanceName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_INSTANCE__SERVICE_INSTANCE_NAME, oldServiceInstanceName, serviceInstanceName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_INSTANCE__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getInstancedOn() {
		return instancedOn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstancedOn(Date newInstancedOn) {
		Date oldInstancedOn = instancedOn;
		instancedOn = newInstancedOn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.SERVICE_INSTANCE__INSTANCED_ON, oldInstancedOn, instancedOn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Endpoint[] getEndpoints() {
		if (endpoints == null || endpoints.isEmpty()) return ENDPOINTS_EEMPTY_ARRAY;
		BasicEList<Endpoint> list = (BasicEList<Endpoint>)endpoints;
		list.shrink();
		return (Endpoint[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Endpoint getEndpoints(int index) {
		return getEndpointsList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEndpointsLength() {
		return endpoints == null ? 0 : endpoints.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndpoints(Endpoint[] newEndpoints) {
		((BasicEList<Endpoint>)getEndpointsList()).setData(newEndpoints.length, newEndpoints);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndpoints(int index, Endpoint element) {
		getEndpointsList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Endpoint> getEndpointsList() {
		if (endpoints == null) {
			endpoints = new EDataTypeUniqueEList<Endpoint>(Endpoint.class, this, ServiceConstructionModelPackage.SERVICE_INSTANCE__ENDPOINTS);
		}
		return endpoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__IMPLEMENTATION:
				if (resolve) return getImplementation();
				return basicGetImplementation();
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__ID:
				return getID();
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__SERVICE_INSTANCE_NAME:
				return getServiceInstanceName();
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__DESCRIPTION:
				return getDescription();
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__INSTANCED_ON:
				return getInstancedOn();
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__ENDPOINTS:
				return getEndpointsList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__IMPLEMENTATION:
				setImplementation((ServiceImplementation)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__ID:
				setID((String)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__SERVICE_INSTANCE_NAME:
				setServiceInstanceName((String)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__INSTANCED_ON:
				setInstancedOn((Date)newValue);
				return;
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__ENDPOINTS:
				getEndpointsList().clear();
				getEndpointsList().addAll((Collection<? extends Endpoint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__IMPLEMENTATION:
				setImplementation((ServiceImplementation)null);
				return;
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__ID:
				setID(ID_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__SERVICE_INSTANCE_NAME:
				setServiceInstanceName(SERVICE_INSTANCE_NAME_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__INSTANCED_ON:
				setInstancedOn(INSTANCED_ON_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__ENDPOINTS:
				getEndpointsList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__IMPLEMENTATION:
				return implementation != null;
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__SERVICE_INSTANCE_NAME:
				return SERVICE_INSTANCE_NAME_EDEFAULT == null ? serviceInstanceName != null : !SERVICE_INSTANCE_NAME_EDEFAULT.equals(serviceInstanceName);
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__INSTANCED_ON:
				return INSTANCED_ON_EDEFAULT == null ? instancedOn != null : !INSTANCED_ON_EDEFAULT.equals(instancedOn);
			case ServiceConstructionModelPackage.SERVICE_INSTANCE__ENDPOINTS:
				return endpoints != null && !endpoints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ID: ");
		result.append(id);
		result.append(", ServiceInstanceName: ");
		result.append(serviceInstanceName);
		result.append(", Description: ");
		result.append(description);
		result.append(", InstancedOn: ");
		result.append(instancedOn);
		result.append(", Endpoints: ");
		result.append(endpoints);
		result.append(')');
		return result.toString();
	}

} //ServiceInstanceImpl
