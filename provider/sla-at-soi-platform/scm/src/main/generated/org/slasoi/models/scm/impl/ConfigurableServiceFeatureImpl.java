/**
 *  SVN FILE: $Id: ConfigurableServiceFeatureImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/ConfigurableServiceFeatureImpl.java $
 */

package org.slasoi.models.scm.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.slasoi.models.scm.ConfigType;
import org.slasoi.models.scm.ConfigurableServiceFeature;
import org.slasoi.models.scm.ServiceConstructionModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configurable Service Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.impl.ConfigurableServiceFeatureImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ConfigurableServiceFeatureImpl#getConfigType <em>Config Type</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ConfigurableServiceFeatureImpl#getConfigFile <em>Config File</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ConfigurableServiceFeatureImpl#getParameterIdentifier <em>Parameter Identifier</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ConfigurableServiceFeatureImpl#getDefaultValue <em>Default Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConfigurableServiceFeatureImpl extends EObjectImpl implements ConfigurableServiceFeature {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getConfigType() <em>Config Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfigType()
	 * @generated
	 * @ordered
	 */
	protected static final ConfigType CONFIG_TYPE_EDEFAULT = ConfigType.ENV_VAR;

	/**
	 * The cached value of the '{@link #getConfigType() <em>Config Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfigType()
	 * @generated
	 * @ordered
	 */
	protected ConfigType configType = CONFIG_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getConfigFile() <em>Config File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfigFile()
	 * @generated
	 * @ordered
	 */
	protected static final String CONFIG_FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConfigFile() <em>Config File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfigFile()
	 * @generated
	 * @ordered
	 */
	protected String configFile = CONFIG_FILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getParameterIdentifier() <em>Parameter Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterIdentifier()
	 * @generated
	 * @ordered
	 */
	protected static final String PARAMETER_IDENTIFIER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getParameterIdentifier() <em>Parameter Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterIdentifier()
	 * @generated
	 * @ordered
	 */
	protected String parameterIdentifier = PARAMETER_IDENTIFIER_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefaultValue() <em>Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultValue()
	 * @generated
	 * @ordered
	 */
	protected static final String DEFAULT_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDefaultValue() <em>Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultValue()
	 * @generated
	 * @ordered
	 */
	protected String defaultValue = DEFAULT_VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfigurableServiceFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServiceConstructionModelPackage.Literals.CONFIGURABLE_SERVICE_FEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		String oldID = id;
		id = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__ID, oldID, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigType getConfigType() {
		return configType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfigType(ConfigType newConfigType) {
		ConfigType oldConfigType = configType;
		configType = newConfigType == null ? CONFIG_TYPE_EDEFAULT : newConfigType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__CONFIG_TYPE, oldConfigType, configType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConfigFile() {
		return configFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfigFile(String newConfigFile) {
		String oldConfigFile = configFile;
		configFile = newConfigFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__CONFIG_FILE, oldConfigFile, configFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getParameterIdentifier() {
		return parameterIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterIdentifier(String newParameterIdentifier) {
		String oldParameterIdentifier = parameterIdentifier;
		parameterIdentifier = newParameterIdentifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__PARAMETER_IDENTIFIER, oldParameterIdentifier, parameterIdentifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultValue(String newDefaultValue) {
		String oldDefaultValue = defaultValue;
		defaultValue = newDefaultValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__DEFAULT_VALUE, oldDefaultValue, defaultValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__ID:
				return getID();
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__CONFIG_TYPE:
				return getConfigType();
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__CONFIG_FILE:
				return getConfigFile();
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__PARAMETER_IDENTIFIER:
				return getParameterIdentifier();
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__DEFAULT_VALUE:
				return getDefaultValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__ID:
				setID((String)newValue);
				return;
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__CONFIG_TYPE:
				setConfigType((ConfigType)newValue);
				return;
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__CONFIG_FILE:
				setConfigFile((String)newValue);
				return;
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__PARAMETER_IDENTIFIER:
				setParameterIdentifier((String)newValue);
				return;
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__DEFAULT_VALUE:
				setDefaultValue((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__ID:
				setID(ID_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__CONFIG_TYPE:
				setConfigType(CONFIG_TYPE_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__CONFIG_FILE:
				setConfigFile(CONFIG_FILE_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__PARAMETER_IDENTIFIER:
				setParameterIdentifier(PARAMETER_IDENTIFIER_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__DEFAULT_VALUE:
				setDefaultValue(DEFAULT_VALUE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__CONFIG_TYPE:
				return configType != CONFIG_TYPE_EDEFAULT;
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__CONFIG_FILE:
				return CONFIG_FILE_EDEFAULT == null ? configFile != null : !CONFIG_FILE_EDEFAULT.equals(configFile);
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__PARAMETER_IDENTIFIER:
				return PARAMETER_IDENTIFIER_EDEFAULT == null ? parameterIdentifier != null : !PARAMETER_IDENTIFIER_EDEFAULT.equals(parameterIdentifier);
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE__DEFAULT_VALUE:
				return DEFAULT_VALUE_EDEFAULT == null ? defaultValue != null : !DEFAULT_VALUE_EDEFAULT.equals(defaultValue);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ID: ");
		result.append(id);
		result.append(", ConfigType: ");
		result.append(configType);
		result.append(", ConfigFile: ");
		result.append(configFile);
		result.append(", ParameterIdentifier: ");
		result.append(parameterIdentifier);
		result.append(", DefaultValue: ");
		result.append(defaultValue);
		result.append(')');
		return result.toString();
	}

} //ConfigurableServiceFeatureImpl
