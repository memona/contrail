/**
 *  SVN FILE: $Id: ComponentMonitoringFeaturesImpl.java 1630 2011-05-09 13:23:18Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 1630 $
 * @lastrevision   $Date: 2011-05-09 15:23:18 +0200 (pon, 09 maj 2011) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/monitoring/common/features/impl/ComponentMonitoringFeaturesImpl.java $
 */

package org.slasoi.monitoring.common.features.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.slasoi.monitoring.common.features.ComponentMonitoringFeatures;
import org.slasoi.monitoring.common.features.FeaturesPackage;
import org.slasoi.monitoring.common.features.MonitoringFeature;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Monitoring Features</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.monitoring.common.features.impl.ComponentMonitoringFeaturesImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.slasoi.monitoring.common.features.impl.ComponentMonitoringFeaturesImpl#getUuid <em>Uuid</em>}</li>
 *   <li>{@link org.slasoi.monitoring.common.features.impl.ComponentMonitoringFeaturesImpl#getMonitoringFeaturesList <em>Monitoring Features</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComponentMonitoringFeaturesImpl extends EObjectImpl implements ComponentMonitoringFeatures {
        /**
         * The default value of the '{@link #getType() <em>Type</em>}' attribute.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see #getType()
         * @generated
         * @ordered
         */
        protected static final String TYPE_EDEFAULT = null;

        /**
         * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see #getType()
         * @generated
         * @ordered
         */
        protected String type = TYPE_EDEFAULT;

        /**
         * The default value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see #getUuid()
         * @generated
         * @ordered
         */
        protected static final String UUID_EDEFAULT = null;

        /**
         * The cached value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see #getUuid()
         * @generated
         * @ordered
         */
        protected String uuid = UUID_EDEFAULT;

        /**
         * The cached value of the '{@link #getMonitoringFeaturesList() <em>Monitoring Features</em>}' containment reference list.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see #getMonitoringFeaturesList()
         * @generated
         * @ordered
         */
        protected EList<MonitoringFeature> monitoringFeatures;

        /**
         * The empty value for the '{@link #getMonitoringFeatures() <em>Monitoring Features</em>}' array accessor.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see #getMonitoringFeatures()
         * @generated
         * @ordered
         */
        protected static final MonitoringFeature[] MONITORING_FEATURES_EEMPTY_ARRAY = new MonitoringFeature [0];

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        protected ComponentMonitoringFeaturesImpl() {
                super();
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        @Override
        protected EClass eStaticClass() {
                return FeaturesPackage.Literals.COMPONENT_MONITORING_FEATURES;
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        public String getType() {
                return type;
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        public void setType(String newType) {
                String oldType = type;
                type = newType;
                if (eNotificationRequired())
                        eNotify(new ENotificationImpl(this, Notification.SET, FeaturesPackage.COMPONENT_MONITORING_FEATURES__TYPE, oldType, type));
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        public String getUuid() {
                return uuid;
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        public void setUuid(String newUuid) {
                String oldUuid = uuid;
                uuid = newUuid;
                if (eNotificationRequired())
                        eNotify(new ENotificationImpl(this, Notification.SET, FeaturesPackage.COMPONENT_MONITORING_FEATURES__UUID, oldUuid, uuid));
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        public MonitoringFeature[] getMonitoringFeatures() {
                if (monitoringFeatures == null || monitoringFeatures.isEmpty()) return MONITORING_FEATURES_EEMPTY_ARRAY;
                BasicEList<MonitoringFeature> list = (BasicEList<MonitoringFeature>)monitoringFeatures;
                list.shrink();
                return (MonitoringFeature[])list.toArray(new MonitoringFeature[list.size()]);
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        public MonitoringFeature getMonitoringFeatures(int index) {
                return getMonitoringFeaturesList().get(index);
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        public int getMonitoringFeaturesLength() {
                return monitoringFeatures == null ? 0 : monitoringFeatures.size();
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        public void setMonitoringFeatures(MonitoringFeature[] newMonitoringFeatures) {
                ((BasicEList<MonitoringFeature>)getMonitoringFeaturesList()).setData(newMonitoringFeatures.length, newMonitoringFeatures);
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        public void setMonitoringFeatures(int index, MonitoringFeature element) {
                getMonitoringFeaturesList().set(index, element);
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        public EList<MonitoringFeature> getMonitoringFeaturesList() {
                if (monitoringFeatures == null) {
                        monitoringFeatures = new EObjectContainmentEList<MonitoringFeature>(MonitoringFeature.class, this, FeaturesPackage.COMPONENT_MONITORING_FEATURES__MONITORING_FEATURES);
                }
                return monitoringFeatures;
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        @Override
        public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
                switch (featureID) {
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__MONITORING_FEATURES:
                                return ((InternalEList<?>)getMonitoringFeaturesList()).basicRemove(otherEnd, msgs);
                }
                return super.eInverseRemove(otherEnd, featureID, msgs);
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        @Override
        public Object eGet(int featureID, boolean resolve, boolean coreType) {
                switch (featureID) {
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__TYPE:
                                return getType();
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__UUID:
                                return getUuid();
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__MONITORING_FEATURES:
                                return getMonitoringFeaturesList();
                }
                return super.eGet(featureID, resolve, coreType);
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        @SuppressWarnings("unchecked")
        @Override
        public void eSet(int featureID, Object newValue) {
                switch (featureID) {
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__TYPE:
                                setType((String)newValue);
                                return;
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__UUID:
                                setUuid((String)newValue);
                                return;
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__MONITORING_FEATURES:
                                getMonitoringFeaturesList().clear();
                                getMonitoringFeaturesList().addAll((Collection<? extends MonitoringFeature>)newValue);
                                return;
                }
                super.eSet(featureID, newValue);
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        @Override
        public void eUnset(int featureID) {
                switch (featureID) {
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__TYPE:
                                setType(TYPE_EDEFAULT);
                                return;
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__UUID:
                                setUuid(UUID_EDEFAULT);
                                return;
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__MONITORING_FEATURES:
                                getMonitoringFeaturesList().clear();
                                return;
                }
                super.eUnset(featureID);
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        @Override
        public boolean eIsSet(int featureID) {
                switch (featureID) {
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__TYPE:
                                return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__UUID:
                                return UUID_EDEFAULT == null ? uuid != null : !UUID_EDEFAULT.equals(uuid);
                        case FeaturesPackage.COMPONENT_MONITORING_FEATURES__MONITORING_FEATURES:
                                return monitoringFeatures != null && !monitoringFeatures.isEmpty();
                }
                return super.eIsSet(featureID);
        }

        /**
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        @Override
        public String toString() {
                if (eIsProxy()) return super.toString();

                StringBuffer result = new StringBuffer(super.toString());
                result.append(" (type: ");
                result.append(type);
                result.append(", uuid: ");
                result.append(uuid);
                result.append(')');
                return result.toString();
        }

} //ComponentMonitoringFeaturesImpl