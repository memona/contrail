/**
 *  SVN FILE: $Id: LandscapeImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/LandscapeImpl.java $
 */

package org.slasoi.models.scm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.slasoi.models.scm.Landscape;
import org.slasoi.models.scm.ServiceBuilder;
import org.slasoi.models.scm.ServiceConstructionModelPackage;
import org.slasoi.models.scm.ServiceImplementation;
import org.slasoi.models.scm.ServiceInstance;
import org.slasoi.models.scm.ServiceTopology;
import org.slasoi.models.scm.ServiceType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Landscape</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.impl.LandscapeImpl#getInstancesList <em>Instances</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.LandscapeImpl#getProvidedTypesList <em>Provided Types</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.LandscapeImpl#getImplementationsList <em>Implementations</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.LandscapeImpl#getTopologiesList <em>Topologies</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.LandscapeImpl#getBuildersList <em>Builders</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.LandscapeImpl#getRequiredTypesList <em>Required Types</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LandscapeImpl extends EObjectImpl implements Landscape {
	/**
	 * The cached value of the '{@link #getInstancesList() <em>Instances</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstancesList()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceInstance> instances;

	/**
	 * The empty value for the '{@link #getInstances() <em>Instances</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstances()
	 * @generated
	 * @ordered
	 */
	protected static final ServiceInstance[] INSTANCES_EEMPTY_ARRAY = new ServiceInstance [0];

	/**
	 * The cached value of the '{@link #getProvidedTypesList() <em>Provided Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvidedTypesList()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceType> providedTypes;

	/**
	 * The empty value for the '{@link #getProvidedTypes() <em>Provided Types</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvidedTypes()
	 * @generated
	 * @ordered
	 */
	protected static final ServiceType[] PROVIDED_TYPES_EEMPTY_ARRAY = new ServiceType [0];

	/**
	 * The cached value of the '{@link #getImplementationsList() <em>Implementations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementationsList()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceImplementation> implementations;

	/**
	 * The empty value for the '{@link #getImplementations() <em>Implementations</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementations()
	 * @generated
	 * @ordered
	 */
	protected static final ServiceImplementation[] IMPLEMENTATIONS_EEMPTY_ARRAY = new ServiceImplementation [0];

	/**
	 * The cached value of the '{@link #getTopologiesList() <em>Topologies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopologiesList()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceTopology> topologies;

	/**
	 * The empty value for the '{@link #getTopologies() <em>Topologies</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopologies()
	 * @generated
	 * @ordered
	 */
	protected static final ServiceTopology[] TOPOLOGIES_EEMPTY_ARRAY = new ServiceTopology [0];

	/**
	 * The cached value of the '{@link #getBuildersList() <em>Builders</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBuildersList()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceBuilder> builders;

	/**
	 * The empty value for the '{@link #getBuilders() <em>Builders</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBuilders()
	 * @generated
	 * @ordered
	 */
	protected static final ServiceBuilder[] BUILDERS_EEMPTY_ARRAY = new ServiceBuilder [0];

	/**
	 * The cached value of the '{@link #getRequiredTypesList() <em>Required Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredTypesList()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceType> requiredTypes;

	/**
	 * The empty value for the '{@link #getRequiredTypes() <em>Required Types</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredTypes()
	 * @generated
	 * @ordered
	 */
	protected static final ServiceType[] REQUIRED_TYPES_EEMPTY_ARRAY = new ServiceType [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LandscapeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServiceConstructionModelPackage.Literals.LANDSCAPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceInstance[] getInstances() {
		if (instances == null || instances.isEmpty()) return INSTANCES_EEMPTY_ARRAY;
		BasicEList<ServiceInstance> list = (BasicEList<ServiceInstance>)instances;
		list.shrink();
		return (ServiceInstance[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceInstance getInstances(int index) {
		return getInstancesList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInstancesLength() {
		return instances == null ? 0 : instances.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstances(ServiceInstance[] newInstances) {
		((BasicEList<ServiceInstance>)getInstancesList()).setData(newInstances.length, newInstances);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstances(int index, ServiceInstance element) {
		getInstancesList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceInstance> getInstancesList() {
		if (instances == null) {
			instances = new EObjectContainmentEList<ServiceInstance>(ServiceInstance.class, this, ServiceConstructionModelPackage.LANDSCAPE__INSTANCES);
		}
		return instances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceType[] getProvidedTypes() {
		if (providedTypes == null || providedTypes.isEmpty()) return PROVIDED_TYPES_EEMPTY_ARRAY;
		BasicEList<ServiceType> list = (BasicEList<ServiceType>)providedTypes;
		list.shrink();
		return (ServiceType[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceType getProvidedTypes(int index) {
		return getProvidedTypesList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getProvidedTypesLength() {
		return providedTypes == null ? 0 : providedTypes.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvidedTypes(ServiceType[] newProvidedTypes) {
		((BasicEList<ServiceType>)getProvidedTypesList()).setData(newProvidedTypes.length, newProvidedTypes);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvidedTypes(int index, ServiceType element) {
		getProvidedTypesList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceType> getProvidedTypesList() {
		if (providedTypes == null) {
			providedTypes = new EObjectContainmentEList<ServiceType>(ServiceType.class, this, ServiceConstructionModelPackage.LANDSCAPE__PROVIDED_TYPES);
		}
		return providedTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceImplementation[] getImplementations() {
		if (implementations == null || implementations.isEmpty()) return IMPLEMENTATIONS_EEMPTY_ARRAY;
		BasicEList<ServiceImplementation> list = (BasicEList<ServiceImplementation>)implementations;
		list.shrink();
		return (ServiceImplementation[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceImplementation getImplementations(int index) {
		return getImplementationsList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getImplementationsLength() {
		return implementations == null ? 0 : implementations.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplementations(ServiceImplementation[] newImplementations) {
		((BasicEList<ServiceImplementation>)getImplementationsList()).setData(newImplementations.length, newImplementations);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplementations(int index, ServiceImplementation element) {
		getImplementationsList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceImplementation> getImplementationsList() {
		if (implementations == null) {
			implementations = new EObjectContainmentEList<ServiceImplementation>(ServiceImplementation.class, this, ServiceConstructionModelPackage.LANDSCAPE__IMPLEMENTATIONS);
		}
		return implementations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceTopology[] getTopologies() {
		if (topologies == null || topologies.isEmpty()) return TOPOLOGIES_EEMPTY_ARRAY;
		BasicEList<ServiceTopology> list = (BasicEList<ServiceTopology>)topologies;
		list.shrink();
		return (ServiceTopology[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceTopology getTopologies(int index) {
		return getTopologiesList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTopologiesLength() {
		return topologies == null ? 0 : topologies.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTopologies(ServiceTopology[] newTopologies) {
		((BasicEList<ServiceTopology>)getTopologiesList()).setData(newTopologies.length, newTopologies);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTopologies(int index, ServiceTopology element) {
		getTopologiesList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceTopology> getTopologiesList() {
		if (topologies == null) {
			topologies = new EObjectContainmentEList<ServiceTopology>(ServiceTopology.class, this, ServiceConstructionModelPackage.LANDSCAPE__TOPOLOGIES);
		}
		return topologies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceBuilder[] getBuilders() {
		if (builders == null || builders.isEmpty()) return BUILDERS_EEMPTY_ARRAY;
		BasicEList<ServiceBuilder> list = (BasicEList<ServiceBuilder>)builders;
		list.shrink();
		return (ServiceBuilder[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceBuilder getBuilders(int index) {
		return getBuildersList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBuildersLength() {
		return builders == null ? 0 : builders.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBuilders(ServiceBuilder[] newBuilders) {
		((BasicEList<ServiceBuilder>)getBuildersList()).setData(newBuilders.length, newBuilders);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBuilders(int index, ServiceBuilder element) {
		getBuildersList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceBuilder> getBuildersList() {
		if (builders == null) {
			builders = new EObjectContainmentEList<ServiceBuilder>(ServiceBuilder.class, this, ServiceConstructionModelPackage.LANDSCAPE__BUILDERS);
		}
		return builders;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceType[] getRequiredTypes() {
		if (requiredTypes == null || requiredTypes.isEmpty()) return REQUIRED_TYPES_EEMPTY_ARRAY;
		BasicEList<ServiceType> list = (BasicEList<ServiceType>)requiredTypes;
		list.shrink();
		return (ServiceType[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceType getRequiredTypes(int index) {
		return getRequiredTypesList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRequiredTypesLength() {
		return requiredTypes == null ? 0 : requiredTypes.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredTypes(ServiceType[] newRequiredTypes) {
		((BasicEList<ServiceType>)getRequiredTypesList()).setData(newRequiredTypes.length, newRequiredTypes);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredTypes(int index, ServiceType element) {
		getRequiredTypesList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceType> getRequiredTypesList() {
		if (requiredTypes == null) {
			requiredTypes = new EObjectContainmentEList<ServiceType>(ServiceType.class, this, ServiceConstructionModelPackage.LANDSCAPE__REQUIRED_TYPES);
		}
		return requiredTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addBuilder(ServiceBuilder builder) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addInstance(ServiceInstance instance) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceImplementation> queryServiceImplementations(ServiceType type) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceType> queryServiceTypes() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ServiceConstructionModelPackage.LANDSCAPE__INSTANCES:
				return ((InternalEList<?>)getInstancesList()).basicRemove(otherEnd, msgs);
			case ServiceConstructionModelPackage.LANDSCAPE__PROVIDED_TYPES:
				return ((InternalEList<?>)getProvidedTypesList()).basicRemove(otherEnd, msgs);
			case ServiceConstructionModelPackage.LANDSCAPE__IMPLEMENTATIONS:
				return ((InternalEList<?>)getImplementationsList()).basicRemove(otherEnd, msgs);
			case ServiceConstructionModelPackage.LANDSCAPE__TOPOLOGIES:
				return ((InternalEList<?>)getTopologiesList()).basicRemove(otherEnd, msgs);
			case ServiceConstructionModelPackage.LANDSCAPE__BUILDERS:
				return ((InternalEList<?>)getBuildersList()).basicRemove(otherEnd, msgs);
			case ServiceConstructionModelPackage.LANDSCAPE__REQUIRED_TYPES:
				return ((InternalEList<?>)getRequiredTypesList()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServiceConstructionModelPackage.LANDSCAPE__INSTANCES:
				return getInstancesList();
			case ServiceConstructionModelPackage.LANDSCAPE__PROVIDED_TYPES:
				return getProvidedTypesList();
			case ServiceConstructionModelPackage.LANDSCAPE__IMPLEMENTATIONS:
				return getImplementationsList();
			case ServiceConstructionModelPackage.LANDSCAPE__TOPOLOGIES:
				return getTopologiesList();
			case ServiceConstructionModelPackage.LANDSCAPE__BUILDERS:
				return getBuildersList();
			case ServiceConstructionModelPackage.LANDSCAPE__REQUIRED_TYPES:
				return getRequiredTypesList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServiceConstructionModelPackage.LANDSCAPE__INSTANCES:
				getInstancesList().clear();
				getInstancesList().addAll((Collection<? extends ServiceInstance>)newValue);
				return;
			case ServiceConstructionModelPackage.LANDSCAPE__PROVIDED_TYPES:
				getProvidedTypesList().clear();
				getProvidedTypesList().addAll((Collection<? extends ServiceType>)newValue);
				return;
			case ServiceConstructionModelPackage.LANDSCAPE__IMPLEMENTATIONS:
				getImplementationsList().clear();
				getImplementationsList().addAll((Collection<? extends ServiceImplementation>)newValue);
				return;
			case ServiceConstructionModelPackage.LANDSCAPE__TOPOLOGIES:
				getTopologiesList().clear();
				getTopologiesList().addAll((Collection<? extends ServiceTopology>)newValue);
				return;
			case ServiceConstructionModelPackage.LANDSCAPE__BUILDERS:
				getBuildersList().clear();
				getBuildersList().addAll((Collection<? extends ServiceBuilder>)newValue);
				return;
			case ServiceConstructionModelPackage.LANDSCAPE__REQUIRED_TYPES:
				getRequiredTypesList().clear();
				getRequiredTypesList().addAll((Collection<? extends ServiceType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.LANDSCAPE__INSTANCES:
				getInstancesList().clear();
				return;
			case ServiceConstructionModelPackage.LANDSCAPE__PROVIDED_TYPES:
				getProvidedTypesList().clear();
				return;
			case ServiceConstructionModelPackage.LANDSCAPE__IMPLEMENTATIONS:
				getImplementationsList().clear();
				return;
			case ServiceConstructionModelPackage.LANDSCAPE__TOPOLOGIES:
				getTopologiesList().clear();
				return;
			case ServiceConstructionModelPackage.LANDSCAPE__BUILDERS:
				getBuildersList().clear();
				return;
			case ServiceConstructionModelPackage.LANDSCAPE__REQUIRED_TYPES:
				getRequiredTypesList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.LANDSCAPE__INSTANCES:
				return instances != null && !instances.isEmpty();
			case ServiceConstructionModelPackage.LANDSCAPE__PROVIDED_TYPES:
				return providedTypes != null && !providedTypes.isEmpty();
			case ServiceConstructionModelPackage.LANDSCAPE__IMPLEMENTATIONS:
				return implementations != null && !implementations.isEmpty();
			case ServiceConstructionModelPackage.LANDSCAPE__TOPOLOGIES:
				return topologies != null && !topologies.isEmpty();
			case ServiceConstructionModelPackage.LANDSCAPE__BUILDERS:
				return builders != null && !builders.isEmpty();
			case ServiceConstructionModelPackage.LANDSCAPE__REQUIRED_TYPES:
				return requiredTypes != null && !requiredTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LandscapeImpl
