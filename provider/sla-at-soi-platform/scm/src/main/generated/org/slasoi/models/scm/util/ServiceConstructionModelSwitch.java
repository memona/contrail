/**
 *  SVN FILE: $Id: ServiceConstructionModelSwitch.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/util/ServiceConstructionModelSwitch.java $
 */

package org.slasoi.models.scm.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.slasoi.models.scm.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.slasoi.models.scm.ServiceConstructionModelPackage
 * @generated
 */
public class ServiceConstructionModelSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ServiceConstructionModelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceConstructionModelSwitch() {
		if (modelPackage == null) {
			modelPackage = ServiceConstructionModelPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ServiceConstructionModelPackage.SERVICE_TYPE: {
				ServiceType serviceType = (ServiceType)theEObject;
				T result = caseServiceType(serviceType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.SERVICE_IMPLEMENTATION: {
				ServiceImplementation serviceImplementation = (ServiceImplementation)theEObject;
				T result = caseServiceImplementation(serviceImplementation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.SERVICE_BUILDER: {
				ServiceBuilder serviceBuilder = (ServiceBuilder)theEObject;
				T result = caseServiceBuilder(serviceBuilder);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.SERVICE_BINDING: {
				ServiceBinding serviceBinding = (ServiceBinding)theEObject;
				T result = caseServiceBinding(serviceBinding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.SERVICE_INSTANCE: {
				ServiceInstance serviceInstance = (ServiceInstance)theEObject;
				T result = caseServiceInstance(serviceInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT: {
				ImplementationArtefact implementationArtefact = (ImplementationArtefact)theEObject;
				T result = caseImplementationArtefact(implementationArtefact);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.DEPENDENCY: {
				Dependency dependency = (Dependency)theEObject;
				T result = caseDependency(dependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.DEPLOYMENT_ARTEFACT: {
				DeploymentArtefact deploymentArtefact = (DeploymentArtefact)theEObject;
				T result = caseDeploymentArtefact(deploymentArtefact);
				if (result == null) result = caseImplementationArtefact(deploymentArtefact);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.DATA_ARTEFACT: {
				DataArtefact dataArtefact = (DataArtefact)theEObject;
				T result = caseDataArtefact(dataArtefact);
				if (result == null) result = caseImplementationArtefact(dataArtefact);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.VIRTUAL_APPLIANCE: {
				VirtualAppliance virtualAppliance = (VirtualAppliance)theEObject;
				T result = caseVirtualAppliance(virtualAppliance);
				if (result == null) result = caseDeploymentArtefact(virtualAppliance);
				if (result == null) result = caseImplementationArtefact(virtualAppliance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.SOFTWARE_ARCHIVE: {
				SoftwareArchive softwareArchive = (SoftwareArchive)theEObject;
				T result = caseSoftwareArchive(softwareArchive);
				if (result == null) result = caseDeploymentArtefact(softwareArchive);
				if (result == null) result = caseImplementationArtefact(softwareArchive);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.CONFIGURABLE_SERVICE_FEATURE: {
				ConfigurableServiceFeature configurableServiceFeature = (ConfigurableServiceFeature)theEObject;
				T result = caseConfigurableServiceFeature(configurableServiceFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.SERVICE_TOPOLOGY: {
				ServiceTopology serviceTopology = (ServiceTopology)theEObject;
				T result = caseServiceTopology(serviceTopology);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.SERVICE_LANDSCAPE_ELEMENT: {
				ServiceLandscapeElement serviceLandscapeElement = (ServiceLandscapeElement)theEObject;
				T result = caseServiceLandscapeElement(serviceLandscapeElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.SOFTWARE_ELEMENT: {
				SoftwareElement softwareElement = (SoftwareElement)theEObject;
				T result = caseSoftwareElement(softwareElement);
				if (result == null) result = caseServiceLandscapeElement(softwareElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.WEB_SERVICE_ELEMENT: {
				WebServiceElement webServiceElement = (WebServiceElement)theEObject;
				T result = caseWebServiceElement(webServiceElement);
				if (result == null) result = caseServiceLandscapeElement(webServiceElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.COMPOSITE_WEB_SERVICE: {
				CompositeWebService compositeWebService = (CompositeWebService)theEObject;
				T result = caseCompositeWebService(compositeWebService);
				if (result == null) result = caseWebServiceElement(compositeWebService);
				if (result == null) result = caseServiceLandscapeElement(compositeWebService);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.ATOMIC_WEB_SERVICE: {
				AtomicWebService atomicWebService = (AtomicWebService)theEObject;
				T result = caseAtomicWebService(atomicWebService);
				if (result == null) result = caseWebServiceElement(atomicWebService);
				if (result == null) result = caseServiceLandscapeElement(atomicWebService);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.APPLICATION_SOFTWARE: {
				ApplicationSoftware applicationSoftware = (ApplicationSoftware)theEObject;
				T result = caseApplicationSoftware(applicationSoftware);
				if (result == null) result = caseSoftwareElement(applicationSoftware);
				if (result == null) result = caseServiceLandscapeElement(applicationSoftware);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.EXECUTION_SOFTWARE: {
				ExecutionSoftware executionSoftware = (ExecutionSoftware)theEObject;
				T result = caseExecutionSoftware(executionSoftware);
				if (result == null) result = caseSoftwareElement(executionSoftware);
				if (result == null) result = caseServiceLandscapeElement(executionSoftware);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.CONFIGURATION_DIRECTIVE: {
				ConfigurationDirective configurationDirective = (ConfigurationDirective)theEObject;
				T result = caseConfigurationDirective(configurationDirective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.LANDSCAPE: {
				Landscape landscape = (Landscape)theEObject;
				T result = caseLandscape(landscape);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.PROVISIONING_INFORMATION: {
				ProvisioningInformation provisioningInformation = (ProvisioningInformation)theEObject;
				T result = caseProvisioningInformation(provisioningInformation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.RESOURCE_ARTEFACT: {
				ResourceArtefact resourceArtefact = (ResourceArtefact)theEObject;
				T result = caseResourceArtefact(resourceArtefact);
				if (result == null) result = caseImplementationArtefact(resourceArtefact);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ServiceConstructionModelPackage.VIRTUAL_MACHINE_ARTEFACT: {
				VirtualMachineArtefact virtualMachineArtefact = (VirtualMachineArtefact)theEObject;
				T result = caseVirtualMachineArtefact(virtualMachineArtefact);
				if (result == null) result = caseResourceArtefact(virtualMachineArtefact);
				if (result == null) result = caseImplementationArtefact(virtualMachineArtefact);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceType(ServiceType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Implementation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Implementation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceImplementation(ServiceImplementation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Builder</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Builder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceBuilder(ServiceBuilder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Binding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceBinding(ServiceBinding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceInstance(ServiceInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Implementation Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Implementation Artefact</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImplementationArtefact(ImplementationArtefact object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDependency(Dependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deployment Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deployment Artefact</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeploymentArtefact(DeploymentArtefact object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Artefact</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataArtefact(DataArtefact object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Virtual Appliance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Virtual Appliance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVirtualAppliance(VirtualAppliance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Software Archive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Software Archive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoftwareArchive(SoftwareArchive object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configurable Service Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configurable Service Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfigurableServiceFeature(ConfigurableServiceFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Topology</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Topology</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceTopology(ServiceTopology object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Landscape Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Landscape Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceLandscapeElement(ServiceLandscapeElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Software Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Software Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoftwareElement(SoftwareElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Web Service Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Web Service Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWebServiceElement(WebServiceElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Web Service</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Web Service</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompositeWebService(CompositeWebService object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atomic Web Service</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atomic Web Service</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtomicWebService(AtomicWebService object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Application Software</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Application Software</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseApplicationSoftware(ApplicationSoftware object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Execution Software</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Execution Software</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExecutionSoftware(ExecutionSoftware object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configuration Directive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configuration Directive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfigurationDirective(ConfigurationDirective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Landscape</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Landscape</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLandscape(Landscape object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Provisioning Information</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Provisioning Information</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProvisioningInformation(ProvisioningInformation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Virtual Machine Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Virtual Machine Artefact</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVirtualMachineArtefact(VirtualMachineArtefact object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Artefact</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceArtefact(ResourceArtefact object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //ServiceConstructionModelSwitch
