/**
 *  SVN FILE: $Id: ImplementationArtefactImpl.java 152 2010-11-18 07:22:11Z alexanderwert $
 *
 * Copyright (c) 2010, SAP AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SAP AG nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SAP AG BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         $Author: alexanderwert $
 * @version        $Rev: 152 $
 * @lastrevision   $Date: 2010-11-18 08:22:11 +0100 (čet, 18 nov 2010) $
 * @filesource     $URL: https://sla-at-soi.svn.sourceforge.net/svnroot/sla-at-soi/platform/branches/contrail-import-0.2/scm/src/main/generated/org/slasoi/models/scm/impl/ImplementationArtefactImpl.java $
 */

package org.slasoi.models.scm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.slasoi.models.scm.ConfigurableServiceFeature;
import org.slasoi.models.scm.Dependency;
import org.slasoi.models.scm.ImplementationArtefact;
import org.slasoi.models.scm.ServiceConstructionModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Implementation Artefact</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.slasoi.models.scm.impl.ImplementationArtefactImpl#getID <em>ID</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ImplementationArtefactImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ImplementationArtefactImpl#getDesciption <em>Desciption</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ImplementationArtefactImpl#getDependenciesList <em>Dependencies</em>}</li>
 *   <li>{@link org.slasoi.models.scm.impl.ImplementationArtefactImpl#getServiceFeaturesList <em>Service Features</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ImplementationArtefactImpl extends EObjectImpl implements ImplementationArtefact {
	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDesciption() <em>Desciption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDesciption()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDesciption() <em>Desciption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDesciption()
	 * @generated
	 * @ordered
	 */
	protected String desciption = DESCIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDependenciesList() <em>Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependenciesList()
	 * @generated
	 * @ordered
	 */
	protected EList<Dependency> dependencies;

	/**
	 * The empty value for the '{@link #getDependencies() <em>Dependencies</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencies()
	 * @generated
	 * @ordered
	 */
	protected static final Dependency[] DEPENDENCIES_EEMPTY_ARRAY = new Dependency [0];

	/**
	 * The cached value of the '{@link #getServiceFeaturesList() <em>Service Features</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceFeaturesList()
	 * @generated
	 * @ordered
	 */
	protected EList<ConfigurableServiceFeature> serviceFeatures;

	/**
	 * The empty value for the '{@link #getServiceFeatures() <em>Service Features</em>}' array accessor.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceFeatures()
	 * @generated
	 * @ordered
	 */
	protected static final ConfigurableServiceFeature[] SERVICE_FEATURES_EEMPTY_ARRAY = new ConfigurableServiceFeature [0];

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImplementationArtefactImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServiceConstructionModelPackage.Literals.IMPLEMENTATION_ARTEFACT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getID() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setID(String newID) {
		String oldID = id;
		id = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__ID, oldID, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDesciption() {
		return desciption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDesciption(String newDesciption) {
		String oldDesciption = desciption;
		desciption = newDesciption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__DESCIPTION, oldDesciption, desciption));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependency[] getDependencies() {
		if (dependencies == null || dependencies.isEmpty()) return DEPENDENCIES_EEMPTY_ARRAY;
		BasicEList<Dependency> list = (BasicEList<Dependency>)dependencies;
		list.shrink();
		return (Dependency[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dependency getDependencies(int index) {
		return getDependenciesList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDependenciesLength() {
		return dependencies == null ? 0 : dependencies.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependencies(Dependency[] newDependencies) {
		((BasicEList<Dependency>)getDependenciesList()).setData(newDependencies.length, newDependencies);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependencies(int index, Dependency element) {
		getDependenciesList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dependency> getDependenciesList() {
		if (dependencies == null) {
			dependencies = new EObjectContainmentEList<Dependency>(Dependency.class, this, ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__DEPENDENCIES);
		}
		return dependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurableServiceFeature[] getServiceFeatures() {
		if (serviceFeatures == null || serviceFeatures.isEmpty()) return SERVICE_FEATURES_EEMPTY_ARRAY;
		BasicEList<ConfigurableServiceFeature> list = (BasicEList<ConfigurableServiceFeature>)serviceFeatures;
		list.shrink();
		return (ConfigurableServiceFeature[])list.data();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurableServiceFeature getServiceFeatures(int index) {
		return getServiceFeaturesList().get(index);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getServiceFeaturesLength() {
		return serviceFeatures == null ? 0 : serviceFeatures.size();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServiceFeatures(ConfigurableServiceFeature[] newServiceFeatures) {
		((BasicEList<ConfigurableServiceFeature>)getServiceFeaturesList()).setData(newServiceFeatures.length, newServiceFeatures);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServiceFeatures(int index, ConfigurableServiceFeature element) {
		getServiceFeaturesList().set(index, element);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConfigurableServiceFeature> getServiceFeaturesList() {
		if (serviceFeatures == null) {
			serviceFeatures = new EObjectContainmentEList<ConfigurableServiceFeature>(ConfigurableServiceFeature.class, this, ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES);
		}
		return serviceFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__DEPENDENCIES:
				return ((InternalEList<?>)getDependenciesList()).basicRemove(otherEnd, msgs);
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES:
				return ((InternalEList<?>)getServiceFeaturesList()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__ID:
				return getID();
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__NAME:
				return getName();
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__DESCIPTION:
				return getDesciption();
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__DEPENDENCIES:
				return getDependenciesList();
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES:
				return getServiceFeaturesList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__ID:
				setID((String)newValue);
				return;
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__NAME:
				setName((String)newValue);
				return;
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__DESCIPTION:
				setDesciption((String)newValue);
				return;
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__DEPENDENCIES:
				getDependenciesList().clear();
				getDependenciesList().addAll((Collection<? extends Dependency>)newValue);
				return;
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES:
				getServiceFeaturesList().clear();
				getServiceFeaturesList().addAll((Collection<? extends ConfigurableServiceFeature>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__ID:
				setID(ID_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__DESCIPTION:
				setDesciption(DESCIPTION_EDEFAULT);
				return;
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__DEPENDENCIES:
				getDependenciesList().clear();
				return;
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES:
				getServiceFeaturesList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__DESCIPTION:
				return DESCIPTION_EDEFAULT == null ? desciption != null : !DESCIPTION_EDEFAULT.equals(desciption);
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__DEPENDENCIES:
				return dependencies != null && !dependencies.isEmpty();
			case ServiceConstructionModelPackage.IMPLEMENTATION_ARTEFACT__SERVICE_FEATURES:
				return serviceFeatures != null && !serviceFeatures.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ID: ");
		result.append(id);
		result.append(", Name: ");
		result.append(name);
		result.append(", Desciption: ");
		result.append(desciption);
		result.append(')');
		return result.toString();
	}

} //ImplementationArtefactImpl
