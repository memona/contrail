-------------------------------------------------------------------------------
# docs at http://paxrunner.ops4j.org/display/paxrunner/Advanced+profiles+topics
-------------------------------------------------------------------------------

# EQUINOX PLATFORM
--platform=equinox
--version=3.5.1

# BundleClassLoader 
#--debugClassLoading=true

# SYSTEM PROPERTIES
--vmo=-Dkey=value\
 -Dpax.log4j.logger.org.springframework=WARN\
 -Dpax.log4j.logger.org.apache.activemq.transport.InactivityMonitor=INFO\
 -Dverbose:class\
 -Dorg.osgi.service.http.port=8080\
# comments
# if debugging from eclipse uncomment next line
 -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005\
 -Dlog4j.configuration=../common/osgi-config/generic-slamanager/log4j.properties

# MAVEN    ::   ${maven.home} should be configured.  PaxRunner search automatically for ${maven.home}/conf/settings.xml
#
--repositories=+http://sla:Ix-bt3U@mammutbaum15.fzi.de:8081/artifactory/libs-releases-local,http://sla:Ix-bt3U@mammutbaum15.fzi.de:8081/artifactory/libs-snapshots-local@snapshots
# --settings=C:\apache-maven-2.2.1\conf\settings.xml
 
# ERROR | TRACE | DEBUG | INFO
#--log=INFO

## PROFILES REPOSITORIES
--profilesRepositories=file:../profiles@snapshots,http://svn.fzi.de/svn/sla_at_soi/project/Software/Y2/platform/trunk/profiles,http://repository.jboss.org/maven2/

--profiles=\
#
#-------------------------------------------------------
## SLA@SOI FRAMEWORK
#-------------------------------------------------------
#
#----------------------
### Common
#----------------------
common/eclipse-jetty/1.0,\
common/axis/1.0,\
common/spring-source/1.0,\
common/spring/2.5.6,\
common/spring.dm/1.2.0,\
models/slamodel/1.0.SNAPSHOT,\
common/misc/1.0,\
common/activemq/5.3,\
#
#-----------------
###  Generic-SLAM
#-----------------
gslam/pss4slam-client/1.0.SNAPSHOT,\
gslam/sla-registry/1.0.SNAPSHOT,\
gslam/sla-template-registry/0.1.SNAPSHOT,\
gslam/syntaxconverter/0.1.SNAPSHOT,\
gslam/protocol-engine/1.0.SNAPSHOT,\
gslam/gslam4osgi/1.0.SNAPSHOT,\
gslam/monitoring-manager/1.0.SNAPSHOT,\
gslam/provisioning-adjustment/0.1.SNAPSHOT,\
#
#----------------------
### Software-SLAManager
#----------------------
swslam/planning-optimization/1.0.SNAPSHOT,\
swslam/provisioning-adjustment/0.1.SNAPSHOT,\
swslam/swslam4osgi/1.0.SNAPSHOT,\
seval/prediction/1.0.SNAPSHOT,\
#
#-------------------------------------------------------
## OSGi Management
## Console ::  http://localhost:8080/system/console/list
#-------------------------------------------------------
common/webconsole/1.0.0,\
#----------------------------
### Software Service Manager
#----------------------------
swsm/swsm/0.1-SNAPSHOT
