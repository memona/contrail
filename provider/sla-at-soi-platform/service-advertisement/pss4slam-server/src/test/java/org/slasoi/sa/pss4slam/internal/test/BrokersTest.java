/**
 * Copyright (c) 2008-2010, SLASOI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of SLASOI nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL SLASOI BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author         Miguel Rojas - miguel.rojas@uni-dortmund.de
 * @version        $Rev$
 * @lastrevision   $Date$
 * @filesource     $URL$
 */

package org.slasoi.sa.pss4slam.internal.test;

import junit.framework.TestCase;

import org.slasoi.sa.pss4slam.server.internal.brokers.AMQStoreEmbeddedBroker;
import org.slasoi.sa.pss4slam.server.internal.brokers.MemoryStoreEmbeddedBroker;
import org.slasoi.sa.pss4slam.server.internal.brokers.PSS4SLAMBroker;
import org.slasoi.sa.pss4slam.server.internal.brokers.RetroactiveConsumer;

public class BrokersTest extends TestCase {

    public void testOne()
    {
        try {
            AMQStoreEmbeddedBroker broker01 = new AMQStoreEmbeddedBroker();
            broker01.createEmbeddedBroker();
        }
        catch (Exception e) {
        }
        
        try {
            MemoryStoreEmbeddedBroker broker01 = new MemoryStoreEmbeddedBroker();
            broker01.createEmbeddedBroker();
        }
        catch (Exception e) {
        }
        
        try {
            RetroactiveConsumer broker01 = new RetroactiveConsumer ();
            broker01.createRetroactiveConsumer();
        }
        catch (Exception e) {
        }
        
        try {
            PSS4SLAMBroker broker01 = new PSS4SLAMBroker();
            broker01.stop();
        }
        catch (Exception e) {
        }
        
        try {
            PSS4SLAMBroker.main( null );
        }
        catch (Exception e) {
        }
        
        assertTrue( true );
    }
}
