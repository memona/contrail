/*
 * Copyright 2012 Contrail Consortium.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.contrail.security;

import java.io.FileInputStream;
import java.security.KeyPair;
import java.security.cert.X509Certificate;
import java.security.Security;
import java.util.Properties;


import org.bouncycastle.jce.provider.BouncyCastleProvider;

import org.junit.*;

/**
 *
 * @author ianjohnson
 */


public class UserCertBasicAuthClientTest {
  
  private static SecurityCommons sc = new SecurityCommons();
  
 @Test
public void dummyTest() {
  System.out.println("dummy");
} 
 
  @BeforeClass
  public static void setUpClass() throws Exception {

     Security.addProvider(new BouncyCastleProvider());
 
  } 

// @Test
  public void testGetCert()
    throws Exception {
   
    System.out.println("get UserCertBasicAuth");
    
    
    /*
     * If the propsFile property isn't set, read the properties files from a hard-wired locationn
     * 
     */
     
    String propsFile = System.getProperty("propsFile", System.getProperty("user.home") + 
      "/SVN/ContrailCode/newtrunk/common/security-commons/src/test/resources/ucstest.properties");
       
    Properties props = null;
    
    try {

      props = new Properties();
      props.load(new FileInputStream(propsFile));  
      
    } catch (Exception ex){
      System.err.println(ex);     
    } 
   
    /*
     * If the targetUrl property isn't set, use a hard-wired URL
     * 
     */
    String uriSpec = props.getProperty("targetUrl", "https://one-test.contrail.rl.ac.uk:8080/ca/user");
   
    KeyPair keyPair = sc.generateKeyPair("RSA", 2048);
    
    String signatureAlgorithm = "SHA256withRSA";
    
    /*
     * Use a well-known username/password combination
     * 
     */
    
    
    
    
    String username = "admin";
    String password = "password";
    String actionPoint = "";
    String proxyHost = null;
    String proxyPortSpec = null;
    String proxyScheme = null;
    CertClient instance = new CertClient(uriSpec, true, 
      /* "/etc/ssl/certs/java/cacerts" */  "/Library/Java/Home/lib/security/cacerts" , "changeit");
 
    X509Certificate result = null;
    
    try {
      System.out.printf("Calling %s.%n", uriSpec);
      
      result = instance.getCert(keyPair, signatureAlgorithm, username, password, 
      actionPoint, true);
    
      if (result == null) { 
        
        throw new Exception(); // Throw an Exception to signal test has failed
 
      }
      
      System.err.println("User Private Key:");
      sc.writeKey(System.out, keyPair.getPrivate());
      
      System.out.println("User Certificate from CA Server:");
      
      sc.writeCertificate(System.out, result);
      
    } catch (IllegalArgumentException ex) {

      System.err.printf(ex.getLocalizedMessage());
      
    }
    
  }  
  


  

}
