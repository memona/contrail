package eu.contrail.security;

import eu.contrail.security.SecurityUtils;

import java.security.KeyPair;

import java.util.ArrayList;

import java.io.IOException;
import java.io.FileNotFoundException;

import java.io.File;

import java.io.InputStream;


import java.io.FileOutputStream;


import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.lang.reflect.Array;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

import org.apache.http.HttpEntity;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;

import org.apache.http.StatusLine;

import org.apache.http.HttpStatus;


import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;


/*
 * This program is used to fetch the trustroots for the Federation CA service
 *
 * These consist of the SSL certificate chain used to certify the CA service
 * endpoint, the CA root certificate (used to sign certificates), and
 * configuration and signing policy files
 *
 * This command needs to be run before any commands to fetch user or service
 * certificates are run
 *
 *
 * Command: gettrustroots rootCertsDir URI
 *
 * rootCertsDir specifies a directory to contain the trustroots for the Contrail
 * Federation CA URI speficies the endpoint for fetching trustroots from the
 * Federation CA service Example:
 * https://one-test.contrail.rl.ac.uk/online-ca/trustroots
 *
 *
 *
 */
public class GetTrustroots {

  /*
   *
   * @param httpClient the HttpClient object for the connection
   *
   *
   */
  public static void setProxy(final HttpClient httpClient) {

    final String proxyName = System.getProperty("http.proxyHost");

    if (proxyName != null) {
      final String proxyScheme = "http"; // Set to HTTP for now 

      int proxyPort = 8080;

      String proxyPortSpec = System.getProperty("http.proxyPort");

      if (proxyPortSpec != null) {

        try {
          proxyPort = Integer.valueOf(proxyPortSpec);
          if (proxyPort <= 0) {
            throw new NumberFormatException();
          }

        } catch (NumberFormatException ex) {
          System.err.println("Error: proxy port specifier '" + proxyPortSpec + "' is invalid");
          proxyPort = 8080;
        }

      }
      HttpHost proxy = new HttpHost(proxyName, proxyPort, proxyScheme);
      httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
    }

  }

  public final static boolean writeFile(final String filepath, final String fileContent)
          throws IOException, FileNotFoundException {

    final File fOut = new File(filepath);

    if (fOut.exists()) {
      boolean deletedOK = fOut.delete();
      if (!deletedOK) {
        return false;
      }
    } else {
      boolean createdOK = fOut.createNewFile();
      if (!createdOK) {
        return false;
      }
    }

    final FileOutputStream fOs = new FileOutputStream(filepath);
    fOs.write(fileContent.getBytes());
    fOs.close();
    return true;

  }
  
  /*
   * 
   * getCerts 
   * @param is InputStream containing 1 or more lines
   * 
   * format of each line is filename=filespec
   * 
   * filename
   * 
   * filespec ia a Base64-encoded file
   * 
   * @return certlist String[] containing an element for each valid filespec  in the input stream
   * 
   * 
   */
  
  public static String[] getCerts(final InputStream is, final String separator)
          throws IOException {

    final ArrayList<String> certList = new ArrayList<String>();
    
    BufferedReader br = null;

    try {
 
      br = new BufferedReader(new InputStreamReader(is));
      String line;

      while ((line = br.readLine()) != null) {
        certList.add(line);
      }

      
      /*
       * To-Do: Correct the code below to use ArrayList.toArray() with the approproate parameters
       * 
       */
      String[] result = new String[certList.size()];

      int i = 0;
      
      for (String s: certList) {
        result[i++] = s;  
      }
 
      return result;
      
    } finally {

      if (br != null) {
        try {
          br.close();
        } catch (Exception ex) {
          ; // Can;t do owt...
        }
      }

    }

  }
  
  public static int writeCertsToDir(final String[] certs, final String rootCertsDir, final String SEPARATOR) {
    
    Base64 base64 = new Base64();
    
    int nWritten = 0;
    int nFailed = 0;
    
    for (String cert: certs) {

      String[] fileSpec = cert.split(SEPARATOR); // Might be better to take substrings 
                                                 // before and after '=' seperator?

      if (fileSpec.length == 2) {

        byte[] contents = base64.decode(fileSpec[1]);

        try {

          writeFile(rootCertsDir + "/" + fileSpec[0], new String(contents));
          nWritten++;

        } catch (FileNotFoundException ex) {
          nFailed++;

        } catch (IOException ex) {
          nFailed++;
        }

      }

    }    
    return nWritten;
  }



  public final static void main(String[] args) throws Exception {

    if (args.length != 2) {
      System.err.println("Usage: GetTrustroots rootCertsDirectory URI");
      System.exit(-1);
    }

    final String rootCertsDir = args[0];

    final File certDir = new File(rootCertsDir);

    if (!certDir.canWrite()) {
      System.err.printf("Error: please ensure that directory %s exists and is writable%n", rootCertsDir);
      System.exit(-1);
    }

    final String uri = args[1];

    HttpClient httpClient = new DefaultHttpClient();

    setProxy(httpClient);

    /*
     * Example URI to fetch trustroots from:
     *
     * https://one-test.contrail.rl.ac.uk/online-ca/trustroots
     *
     */

    InputStream is = null;
    
    try {

      HttpGet httpGet = new HttpGet(uri);
      HttpResponse response = httpClient.execute(httpGet);

      final StatusLine statusLine = response.getStatusLine();

      final int statusCode = statusLine.getStatusCode();

      if (statusCode != HttpStatus.SC_OK) {

        System.err.println("Error code is " + statusCode);
        System.err.println("Status line is " + statusLine);

        httpClient.getConnectionManager().shutdown();
        System.exit(-1);

      }

      is = response.getEntity().getContent();
      
      final String SEPARATOR = "=";
      
      /*
       * Read the list of certificates/config info/policy files from the response stream
       */
      
      String[] certList = getCerts(is, SEPARATOR);
      
      int totalFiles = certList.length;
      
      /*
       * Write the certificate and other files to the directory specified as 
       * the first argument to the program
       */
      
      int nWritten = writeCertsToDir(certList, rootCertsDir, SEPARATOR);
          
      int exitCode = 0;
      System.out.printf("Wrote %d files to %s", nWritten, rootCertsDir);
      
      if (nWritten != totalFiles) {
        exitCode = -1;
        System.err.printf(", expected to output %d files.%n", totalFiles);
      } else {
        System.err.printf(".%n");
      }

      is.close();
      System.exit(exitCode);

    } catch (Exception ex) {

      System.err.println(ex);

    } finally {

      httpClient.getConnectionManager().shutdown();
      if (is != null) {
        try {
          is.close();
        } catch (Exception ex) {
          ; // Can't do owt...
        }
      }

    }

  }
}
