========================
=== Contrail Security Commons ===
========================

Author: Ian Johnson
Scientific Computing Department
STFC Rutherford Appleton Laboratory
Harwell Oxford
OX11 0QX
United Kingdom


1) DESCRIPTION

This component provides a library of common security-related methods for generating crypto-related objects 
(KeyPairs, CSRs, CRLs, X509 certificates, etc) and for retrieving information from objects such as certificates. 
This library is for use by Contrail services and user-level commands. This component also provides a client for 
the Contrail Federation Certification Authority service.

The library is contained in the JAR file /var/lib/contrail/security-commons/security-commons-1.0-SNAPSHOT.jar.
The user command, the CA client, is installed in /usr/bin/get-user-cert.


II) USAGE

    get-user-cert -u <username> -p <password> [-Q passphrase] http://<CA server:port>/ca/user

e.g.

   get-user-cert -u admin -p password -Q passwout http://one-test.contrail.rl.ac.uk:8080/ca/user


III) DEPENDENCIES

BouncyCastle hcprov-jdk16-146

BouncyCastle bcmail-jdk16-146

Junit        junit-4.10

GNU Getopt   java-getopt-1.0.13

Apache Commons Lang commons-lang-2.6

Apache HTTP Components   httpcore-4.1.4

Apache Logging   commons-logging-1.1.1

Apache Codec    commons-codec-1.6

Apache HTTP client commons-httpclient-3.1


IV) PROVIDES

JAR file: /var/lib/contrail/security-commons/security-commons-1.0-SNAPSHOT.jar

User command: /usr/bin/get-user-cert

