package org.ow2.contrail.common.dynamicca;

import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.contrail.common.dynamicca.jpa.EMF;
import org.ow2.contrail.common.dynamicca.utils.Conf;

import java.io.File;
import java.io.StringReader;
import java.security.Security;
import java.security.cert.X509Certificate;

import static org.junit.Assert.assertEquals;

@Ignore
public class VinTest extends JerseyTest {

    public VinTest() throws Exception {
        super(new WebAppDescriptor.Builder("org.ow2.contrail.common.dynamicca.rest")
                .contextPath("dynca").build());
    }

    @Before
    public void setUp() throws Exception {
        EMF.init("testPersistenceUnit");
        Conf.getInstance().load(new File("src/test/resources/dynamic-ca-server.properties"));
        if (Security.getProvider("BC") == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    @After
    public void tearDown() throws Exception {
       EMF.close();
       Utils.dropTestDatabase();
    }

    @Test
    public void testCreateCa() throws Exception {
        WebResource webResource = resource();
        String baseUri = webResource.getURI().toString();

        // create VIN controller
        JSONObject vinData = new JSONObject();
        vinData.put("uid", "myvin");
        webResource.path("/vins").post(vinData);

        // get all VIN controllers
        JSONArray vinsArray = webResource.path("/vins").get(JSONArray.class);
        assertEquals(vinsArray.length(), 1);
        assertEquals(vinsArray.get(0), baseUri + "/vins/myvin");

        // get myvin
        JSONObject vinInfo = webResource.path("/vins/myvin").get(JSONObject.class);
        assertEquals(vinInfo.getString("uid"), "myvin");

        // create new CA for myvin
        JSONObject caData = new JSONObject();
        caData.put("uid", "myca");
        webResource.path("/vins/myvin/cas").post(caData);

        // get all CAs for myvin
        JSONArray casArray = webResource.path("/vins/myvin/cas").get(JSONArray.class);
        assertEquals(casArray.length(), 1);
        assertEquals(casArray.get(0), baseUri + "/vins/myvin/cas/myca");

        // get myca
        JSONObject caInfo = webResource.path("/vins/myvin/cas/myca").get(JSONObject.class);
        assertEquals(caInfo.getString("uid"), "myca");

        // get myca certificate
        String certPem = webResource.path("/vins/myvin/cas/myca/cacert").get(String.class);
        assertEquals(caInfo.getString("uid"), "myca");
        PEMReader pemReader = new PEMReader(new StringReader(certPem));
        X509Certificate cert = (X509Certificate) pemReader.readObject();
        assertEquals(cert.getSubjectDN().getName(), Conf.getInstance().getCACertDN("myca"));
    }
}
