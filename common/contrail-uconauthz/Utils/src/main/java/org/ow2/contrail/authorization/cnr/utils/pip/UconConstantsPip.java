package org.ow2.contrail.authorization.cnr.utils.pip;

public class UconConstantsPip {
	//configuration file location for pip
	public static final String configFile = "/etc/contrail/authz/pip/pipconfig.properties";
	
	//Axis2 table entry for PIP
	public static final String IDENTITY_PROVIDER_URL = "identity_provider_url";
    public static final String OPENSAML_UTILS = "opensamlutils";
    public static final String SUBSCRIBERS_SET = "SUBSCRIBERS";

    //error message    
    public static final String errorIP_URL 	= "PIP ERROR: Malformed URL for Identity Provider ";
	public static final String errorIP 		= "PIP ERROR: Unable to reach Identity Provider";
	public static final String errorMSG 	= "PIP ERROR: Malformed request";    
}
