package org.ow2.contrail.authorization.cnr.utils.core;

import java.util.List;

import org.ow2.contrail.authorization.cnr.utils.XacmlSamlException;

public interface XacmlSamlCoreUtils {

	/**
	 * Extract the id from Start message assertion
	 * 
	 * @param start
	 * @return
	 * @throws XacmlSamlException 
	 */
	public UconRequestContext getIdFromStart(String message) throws XacmlSamlException;

	/**
	 * Extract the id from end message assertion
	 * 
	 * @param end
	 * @return
	 * @throws XacmlSamlException 
	 */
	public UconRequestContext getIdFromEnd(String message) throws XacmlSamlException;
		

	/**
	 * Add the PIP attributes to xacmlAuthzQuery
	 * 
	 * @param xacmlAuthzQuery
	 *            The xacmlAuthzQuery
	 * @param attributePIP
	 *            List of UconAttributes
	 * @return The new xacmlAuthzQuery
	 */
	public String formXacmlAuthzQueryAttribute(UconXacmlRequest xacmlAuthzQuery, List<UconAttribute> attribute) throws XacmlSamlException;
	public String convertXacmlAuthzQuery20to30(UconXacmlRequest xacmlAuthzQuery, List<UconAttribute> attributes) throws XacmlSamlException;
	/**
	 * Parse the response got by pdp
	 * 
	 * @param response
	 *            XACML response
	 * @return
	 */
	public boolean getAccessDecision(String xacmlResponse) throws XacmlSamlException;
	public boolean getAccessDecision30(String xacmlResponse) throws XacmlSamlException;
		
	/**
	 * Incapsulate a xacmlresponse (by a pdp) and sessionId in a saml response
	 * 
	 * @param xacmlResponse
	 * @param sessionId
	 * @return
	 * @throws XacmlSamlException 
	 */
	public String formResponse(String xacmlResponse, String sessionId) throws XacmlSamlException;
	
	/**
	 * Used in session manager with the pip response
	 * 
	 * @param response
	 * @param holder
	 * @return
	 * @throws XacmlSamlException
	 */
	public List<UconAttribute> getAttributeFromPipResponse(String response, String holder) throws XacmlSamlException;
	
	/**
	 * Used in session manager with pip update
	 * @param updateMessage
	 * @return
	 * @throws XacmlSamlException
	 */
	public List<UconAttribute> getAttributeFromPipUpdate(String updateMessage) throws XacmlSamlException;
	
	/**
	 * Extract every information from request context
	 * @param request
	 * @return
	 * @throws XacmlSamlException 
	 */
	public UconRequestContext getRequestContextFromAccessRequest(String request) throws XacmlSamlException;
		
}
