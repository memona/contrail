package org.ow2.contrail.authorization.cnr.utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.util.Date;

import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.xacml.ctx.DecisionType;
import org.opensaml.xacml.ctx.ResponseType;
import org.opensaml.xacml.ctx.ResultType;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.opensaml.xml.util.XMLHelper;
import org.opensaml.xml.util.XMLObjectHelper;

public class OpenSamlUtils {

	protected XMLObjectBuilderFactory builderFactory;

	/**
	 * Initialize OpenSaml Library
	 * 
	 * @throws ConfigurationException
	 *             If an error occurs during bootstrap (getMessage for more informations)
	 */
	public OpenSamlUtils() throws XacmlSamlException {
		try {
			DefaultBootstrap.bootstrap();
		} catch (ConfigurationException e) {
			throw new XacmlSamlException("There is a problem initializating OpenSaml library (message: "+e.getMessage()+")");
		}
		builderFactory = Configuration.getBuilderFactory();
	}

	/**
	 * Return a string representation of a XMLObject
	 * 
	 * @param obj
	 * @return
	 * @throws XacmlSamlException 
	 * 				If an error occur (getMessage for more informations)
	 */
	protected String marshalling(XMLObject obj) throws XacmlSamlException {
		try {
			return XMLHelper.prettyPrintXML(XMLObjectHelper.marshall(obj));
		} catch (MarshallingException e) {
			String message = "Marshalling error: unable to marshall the following obeject\n" + obj;
			throw new XacmlSamlException(message);
		}
	}

	/**
	 * Return a XMLObject representation of a XML string
	 * 
	 * @param str
	 * @return
	 * @throws XacmlSamlException
	 * 				If an error occur (getMessage for more informations)
	 */
	protected XMLObject unmarshalling(String str) throws XacmlSamlException {
		if (str == null || str.trim().equals("")) { throw new XacmlSamlException("Unmarshalling error: blank or null string"); }
		try {
			
			BasicParserPool parserPool = new BasicParserPool();
			Reader reader = new StringReader(str.trim());
			return XMLObjectHelper.unmarshallFromReader(parserPool, reader);
			
		} catch (Exception e) {
			String message = "Unmarshalling error: unable to unmarshall the following input string\n" + str;
			String log = "\n***\n(" + new Date() + ") "+message;
			String outputFile = System.getProperty("user.home") + "/unmarshallingError";
			PrintWriter out = null;
			try {
				out = new PrintWriter(new FileOutputStream(outputFile, true));
				out.println(log);
				out.println("***\n");
				out.flush();
			} catch (FileNotFoundException e1) {
				System.err.println("Unmarshalling error: cannot write on log file " + outputFile);
				System.err.println(log);
			} finally {
				out.close();
			}
			throw new XacmlSamlException(message);
		}
	}

	/**
	 * Parse a response got by pdp
	 * 
	 * @param response
	 *            XACML response
	 * @return
	 * @throws UnmarshallingException
	 * @throws XMLParserException
	 */
	protected boolean getAccessDecision(ResponseType response) {
		ResultType result = response.getResult();
		DecisionType decision = result.getDecision();
		boolean value = false;
		switch (decision.getDecision()) { // don't we care about the other decisions?
		case Deny:
		case Indeterminate:
		case NotApplicable:
			value = false;
			break;
		case Permit:
			value = true;
			break;
		}
		return value;
	}
}
