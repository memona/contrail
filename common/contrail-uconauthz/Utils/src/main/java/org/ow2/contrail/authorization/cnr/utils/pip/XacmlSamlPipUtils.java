package org.ow2.contrail.authorization.cnr.utils.pip;

import java.util.List;
import org.ow2.contrail.authorization.cnr.utils.XacmlSamlException;
import org.ow2.contrail.authorization.cnr.utils.UconConstants.Category;

public interface XacmlSamlPipUtils {
	
	/**
	 * Create a SAML AttributeQuery element without any attributes
	 * "if no attributes are specified, it indicates that all attribute allowed by policy are requested"
	 * 
	 * @param name
	 *            subject name
	 * @return the attribute query object
	 */
	public Object formSAMLAttributeQuery(String name);
	
	/**
	 * Create a SAML AttributeQuery element
	 * 
	 * @param name
	 *            subject name
	 * @param attributes
	 *            string list of attributes to query
	 * @return the attribute query object
	 */
	public Object formSAMLAttributeQuery(String name, List<String> attributes);
	
	/**
	 * Create a SAML attribute object
	 * 
	 * @param attributeName
	 * @return
	 */
	public Object createSAMLAttribute(String attributeName);
	
	/**
	 * Create a SOAP message with a body and an empty header
	 * 
	 * @param bodyContent
	 *            the body content
	 * @return
	 * @throws XacmlSamlException 
	 */
	public String formSOAPMessage(Object bodyContent) throws XacmlSamlException;
		
	/**
	 * Create the XACML response message from identity provider query response
	 * @param queryResponse
	 * @return
	 * @throws XacmlSamlException 
	 */
	public String formXACMLResponseMessage(String queryResponse, Category type) throws XacmlSamlException;
	
	/**
	 * Create the XACML response message from identity provider update
	 * @param updateMessage
	 * @return
	 * @throws XacmlSamlException 
	 */
	public String formXACMLUpdateMessage(String updateMessage) throws XacmlSamlException;
	
	/**
	 * Get a list of attribute names for subscription table
	 * @param queryResponse
	 * @return
	 * @throws XacmlSamlException
	 */
	public List<String> getAttributeNameList(String queryResponse) throws XacmlSamlException;
	
}