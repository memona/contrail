package org.ow2.contrail.authorization.cnr.utils.core;

import org.ow2.contrail.authorization.cnr.utils.UconConstants.Category;

public class UconAttribute {

	private int key; // primary key in db
	private String xacml_attribute_id;
	private String type;
	private String value;
	private String holder_id;
	private String issuer;
	//private boolean isMutable = true;
	// private int category = 3;
	private Category category;

	public UconAttribute(String xacmlid, String type, String issuer, String holder, Category category) {
		this(-1, xacmlid, type, "", issuer, holder, category); //without primary key and value
	}
	
	public UconAttribute(int key, String xacmlid, String type, String value, String issuer, String holder, Category category) {
		this.key = key;
		this.xacml_attribute_id = xacmlid;
		this.type = type;
		this.value = value;
		this.issuer = issuer;
		this.category = category;
		this.holder_id = holder;
	}
	
	public UconAttribute(UconAttribute attr) {
		this(attr.key, attr.xacml_attribute_id, attr.type, attr.value, attr.issuer, attr.holder_id, attr.category);
	}

	public int getAttributeKey() {
		return key;
	}
	
	public void setAttributeKey(int key) {
		this.key = key;
	}

	public String getXacmlId() {
		return xacml_attribute_id;
	}

	public String getType() {
		return type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public String getIssuer() {
		return issuer;
	}

	public Category getCategory() {
		return category;
	}

	public String getHolderId() {
		return holder_id;
	}
	
	public void setHolderId(String value) {
		this.holder_id = value;
	}
}
