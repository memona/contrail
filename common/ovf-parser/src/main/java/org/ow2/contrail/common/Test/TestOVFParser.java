package org.ow2.contrail.common.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import org.ow2.contrail.common.ParserManager;
import org.ow2.contrail.common.exceptions.MalformedOVFException;
import org.ow2.contrail.common.implementation.application.ApplianceDescriptor;
import org.ow2.contrail.common.implementation.application.ApplicationDescriptor;
import org.ow2.contrail.common.implementation.ovf.File;
import org.ow2.contrail.common.implementation.ovf.OVFParser;
import org.ow2.contrail.common.implementation.ovf.OVFVirtualHardware;
import org.ow2.contrail.common.implementation.ovf.OVFVirtualNetwork;
import org.ow2.contrail.common.implementation.ovf.OVFVirtualSystem;
import org.ow2.contrail.common.implementation.ovf.ovf_StAXParsing.OVFStAXParser;

/**
 * This class is intended for testing purpose only 
 * @author maestrelli
 *
 */
public class TestOVFParser 
{
	public static void main(String[] args) throws NumberFormatException, FileNotFoundException,
	XPathExpressionException, DOMException, ParserConfigurationException, SAXException, IOException,
	URISyntaxException 
{		
		org.ow2.contrail.common.ParserManager p = null;
		ApplicationDescriptor appDesc=null;
		ApplicationDescriptor appDesc2=null;
		long finish=0,finish2=0,start=0;
		try
		{
			
			start=System.currentTimeMillis();
//			for(int i=0;i<10;i++)
//			OVFParser.ParseOVF(URI.create("src/main/resources/contrail_petstore.xml"));
//			OVFParser.ParseOVF(URI.create("src/main/resources/official_source.xml"));
			finish2=System.currentTimeMillis()-start;
			//p = new org.ow2.contrail.common.ParserManager("src/main/resources/dsl-test-xlab.ovf");
			start=System.currentTimeMillis();
			//for(int i=0;i<10;i++)
			new OVFStAXParser().parseOVF(URI.create("src/main/resources/contrail_petstore.xml"));
			new OVFStAXParser().parseOVF(URI.create("src/main/resources/official_source.xml"));
			finish=System.currentTimeMillis()-start;
			if (false) throw new XMLStreamException("hi!");
			
			/*	
			start=System.currentTimeMillis();
			appDesc2=OVFParser.ParseOVF(URI.create("src/main/resources/contrail_petstore.xml"));
			finish2=System.currentTimeMillis()-start;
			*/	
		}
		catch (MalformedOVFException e)
		{
			e.printStackTrace();
		}
		catch (XMLStreamException e) {
//			e.printStackTrace();
		}
		System.out.println("completamento time STAX: "+ finish+" completamento time dom: "+finish2);
//		recursiveDesc(appDesc.getApplianceDescriptors(),"");
//		recursiveDesc(appDesc2.getApplianceDescriptors(),"");
		//org.ow2.contrail.common.ParserManager p = new org.ow2.contrail.common.ParserManager("src/main/resources/ovf.xml");

		//for (String appId : p.getAppliances())
		//{
		//	System.out.print("Appliance Id = ");
		//	System.out.println(appId);
		//	
		//	for (File f : p.getApplianceImages(appId))
		//	{	
		//		System.out.println("Image URI = " + f.getUri() + " " + "Image Size = " + f.getSize());
		//	}
		//	
		//	Collection<String> networks = p.getApplianceNetworks(appId);
		//	if (networks.size() > 0)
		//	{
		//		System.out.print("Associated Networks = ");
		//		for (String n : networks)
		//		{	
		//			System.out.println(n);
		//		}
		//	}
		//	System.out.println();
		//}
}

	private static void recursiveDesc(Collection<ApplianceDescriptor> cad,String indent){
		for(ApplianceDescriptor ad:cad){
			System.out.println(indent+ad.getID());
			System.out.println(indent+" Networks:");
			for(OVFVirtualNetwork vn:ad.getAssociatedVirtualNetworks()){
				System.out.println(indent+"  "+vn.getName());
			}
			recursiveDesc(ad.getAppliancesDescriptors(),"\t"+indent);
		}
	}
}
