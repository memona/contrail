===========================
Contrail Federation Certificate Authority Server
===========================

Author: Ian Johnson
Scientific Computing Department
STFC Rutherford Appleton Laboratory
Harwell Oxford
OX11 0QX
United Kingdom

1) DESCRIPTION

This component provides the the means of creating the configuration for a Contrail Federation 
Root CA, and a WAR file containing servlets for Tomcat to act as the CA server.

The Root CA creation is the scripts create-rootca-files script and add-trusted-ca.
The CA servlets are contained in the ca.war file.


II) DEPENDENCIES

Tomcat, version 6 or 7 (version 7 preferred)


contrail-security-commons



BouncyCastle bcprov-jdk16-146
BouncyCastle bcmail-jdk16-146


Apache Commons commons-codec-1.4
Apache Commons commons-httpclient-3.1
Apache Commons commons-lang-2.6
Apache Commons commons-logging-1.1.1
EclipseLink eclipselink-2.3.2
Contrail federation-db-0.3

Apache HTTP Commons httpclient-4.1.3
Apache HTTP Commons httpcore-4.1.4
EclipseLink javax.persistence-2.0.3
JSON json-20090211
MySQL mysql-connector-java-5.1.20
EclipseLink org.eclipse.persistence.jpa.modelgen.processor-2.3.0

Contrail security-commons-1.0-SNAPSHOT




III) PROVIDES

<TOMCAT DIR>/webapps/ca.war

/etc/contrail/ca-server/create-rootca-files.conf
/etc/contrail/ca-server/tomcat-connector-fragment.xml

/usr/bin/create-rootca-files
/usr/bin/add-trusted-ca

/usr/share/contrail/ca-server/LICENSE.txt
/usr/share/contrail/ca-server/README.txt

