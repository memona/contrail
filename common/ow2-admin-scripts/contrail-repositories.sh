#!/bin/bash
#
#    Manage artifacts repositories for the Contrail project
#
# Usage
# -----
#    contrail-repositories <source_tar> <target_dir>
#
# Versions
# --------
#    2012-06-25 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Fixed a variable name
#    2012-04-14 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Fix wget options to not download other repos
#        * Add functions to download files without recursive wget
#        * Suppressed rate limitation when mirroring
#    2012-03-28 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Added the tag command
#    2012-03-27 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Fixed a bug with extra / in the URL
#    2012-03-26 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Mirror only one repository instead of everything
#        * Do not use /tmp anymore to store downloaded files
#    2012-03-22 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Simplified command line usage
#    2012-02-27 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Now print started and finished time
#        * Added usage message
#    2012-02-21 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Added code to mirror repositories
#    2012-02-08 Sebastien Andre <sebastien.andre@gmx.fr>
#        * Initial version
#
set -o nounset -o errexit

declare -r NAME='contrail-repositories'
declare -r VERSION='2012-06-27'

declare -r OW2_DIR='/var/lib/gforge/chroot/home/groups/contrail/htdocs/repositories/binaries'
# original URL which replies HTTP 302 redirect
# declare -r OBS_URL='http://download.opensuse.org/repositories/home:/contrail:'
# mirrors 
declare -r OBS_URL='http://widehat.opensuse.org/repositories/home:/contrail:'
#declare -r OBS_URL='http://ftp5.gwdg.de/pub/opensuse/repositories/home:/contrail:'


declare -r USAGE="Usage: $NAME <command> [args...]

   create <source> <target>
       Convert a <source> directory with Maven 3rd party files to a Maven
       repository in <target> directory. All files in <source> must match
       '<groupId>/<ArtifactId>-<version>.<packaging>'.

   mirror <repo>...
       Create a mirror copy of the OBS subprojects <repo>.

   tag <revision> <version> [<login>]
       Create a tag copy of source files at <revision> for <version>.
       If <login> is provided, it is used to connect to SVN.

"


# Print error message $1 and exit with status $2
function fail () {
	echo "ERROR: ${1:-failed}" >&2
	exit ${2:-1}
}

function info () {
	echo ":: $(date '+%F %T') [$NAME] $*"
}

# Convert files directory $1 to maven repository $2rail:/staging/
function create_maven_repo () {
	local src="${1?}" dst="${2?}"
	local groupId artifactId version packaging dir file base

	for dir in "$src"/*
	do
		groupId="${dir##*/}"
		for file in "$dir"/*
		do
			base="${file##*/}"
			packaging="${base##*.}"
			artifactId="${base%-*}"
			version="${base#$artifactId-}"
			version="${version%.$packaging}"

			info "install $groupId:$artifactId:$version:$packaging"
			mvn install:install-file -DlocalRepositoryPath="$dst" \
				-Dfile="$dir/$base" -DgroupId="$groupId" \
				-DartifactId="$artifactId" -Dversion="$version" \
				-Dpackaging="$packaging" \
				-DgeneratePom=true -DcreateChecksum=true
		done
	done
}

# List files found in repo URL $1 or only directories if $2 is "DIR"
function list_repo_files () {
	local url="${1?}" type="${2:-   }"
	wget -q -O - "$url" \
		| sed -ne 's|^<img src=\"/icons/.*alt=\"\['"$type"'\]\".*href=\"\([^\"]*\)\".*|\1|p'
}


# Download files and dirs from http url $1 into directory $2
function download_http_repo () {
	local url="${1?}" dir="${2?}" item=

	rm -fr "$dir"
	mkdir -p "$dir"
	info "$dir: created"

	for item in $(list_repo_files "$url")
	do
		wget --no-verbose -O "$dir/$item" -- "$url/$item" &
		sleep 1
	done

	info "waiting for background downloads to terminate..."
	wait

	for item in $(list_repo_files "$url" DIR)
	do
		download_http_repo "$url/$item" "$dir/$item"
	done
}

# Create a mirror copy of URL $1 in directory $2 with optional rate limit $3
function mirror_binaries_repo () {
	local url="${1?}"
	local newdir="${2?}" bkpdir="${2%%/}.bkp" tmpdir="${2%%/}.tmp" 

	info "$url: testing if URL is online"
	wget --spider --quiet "$url"

	info "$url: download files from repository to backup dir"
	download_http_repo "$url" "$tmpdir"

	info "fix group and permission of files"
	chgrp -R contrail "$tmpdir"
	find "$tmpdir" -type d -exec chmod 2775 {} +
	find "$tmpdir" -type f -exec chmod 2664 {} +

	info "$newdir: setup new directory"
	if [ -e "$newdir" ]
	then
		rm -fr "$bkpdir"
		mv "$newdir" "$bkpdir"
		mv "$tmpdir" "$newdir"
		rm -fr "$bkpdir"
	else
		mv "$tmpdir" "$newdir"
	fi
}




if [ $# -eq 0 ]
then
	echo "$USAGE" >&2
	exit 2
fi

info "$@"

declare command="$1"
shift

case "$command" in
create)
	if [ $# -ne 2 ]
	then
		fail "$#: bad number of arguments"

	elif [ ! -d "$1" ]
	then
		fail "$1: source directory not found"

	elif [ -e "$2" ]
	then
		fail "$2: target directory already exists"

	elif ! create_maven_repo "$1" "$2"
	then
		fail
	fi
;;
mirror)
	declare name

	if [ $# -eq 0 ]
	then
		fail "$#: bad number of arguments"

	elif [ ! -d "$OW2_DIR" -o ! -w "$OW2_DIR" ]
	then
		fail "$OW2_DIR: unable to write in directory"
	fi

	for name in "$@"
	do
		mirror_binaries_repo "$OBS_URL/$name" "$OW2_DIR/$name"
	done

;;
tag)
	declare rev ver dev

	if [ $# -lt 2 -o $# -gt 3 ]
	then
		fail "$#: bad number of arguments"

	elif ! egrep -qw '[0-9]+' <<<"$1"
	then
		fail "$1: invalid SVN revision number"

	elif [ -z "$2" ]
	then
		fail "version name cannot be empty"

	else
		rev="$1"
		ver="$2"
		dev="${3:-$USER}"
	fi

	# TODO: deep copy of externals content
	if svn copy --ignore-externals --revision "$rev" \
		--message "[contrail-repositories] tag revision $rev for version $ver" \
		"svn+ssh://${dev}@svn.forge.objectweb.org/svnroot/contrail/trunk" \
		"svn+ssh://${dev}@svn.forge.objectweb.org/svnroot/contrail/tags/contrail-trunk-${ver}"
	then
		info "created tag contrail-trunk-$ver for revision $rev"
	else
		fail "failed to tag SVN revision $rev"
	fi
;;		
*)
	fail "$command: invalid command"
esac


info 'terminate'
exit 0

