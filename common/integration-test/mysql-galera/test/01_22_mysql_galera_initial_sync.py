# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def test_01_initial_sync_mysql_galera(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/upstream/mysql-galera/run-initial-sync-node1.sh 'head ' ")
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/upstream/mysql-galera/run-initial-sync-node2.sh 'federation ' ")
    # now, restart to normal state

def test_02_check_status(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/upstream/mysql-galera/check-status.sh")
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/upstream/mysql-galera/check-status.sh")

#
