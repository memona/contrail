#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# allow login from other hosts
mysql -e "use mysql; select Host, User, Password from user;"
mysql -e "use mysql; update user set Host='%' where User='root' and Host like 'n00%'; flush privileges;"
# additional users...
mysql -e "use mysql; select Host, User, Password from user;"

