#!/bin/bash

# $1 - dns name of peer node

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

MYIP=`dig +short $HOST_NAME`
echo "INFO: mysql-galera me: $HOST_NAME ($MYIP)"
PEERNAME=`eval echo '$'"HOST_NAME_$1"`
PEERIP=`dig +short $PEERNAME`
echo "INFO: mysql-galera peer: $PEERNAME ($1: $PEERIP)"

# Start mgmt node.
nohup mysqld --wsrep_cluster_address=gcomm://${PEERIP} 1>>/var/log/mysql/initial-sync-2.log 2>&1 &
sleep 10
cat /var/log/mysql/initial-sync-2.log
