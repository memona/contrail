#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Setup node with sql and ndbd deamon.

cd /usr/local/mysql/
scripts/mysql_install_db --user=mysql --basedir=/usr/local/mysql
chown -R root  .
chown -R mysql data
chgrp -R mysql .

cp /srv/contegrator-test/upstream/mysql-cluster/mysql.ndbd /etc/init.d/
chmod a+x /etc/init.d/mysql.ndbd
(cd /etc/rc2.d/ && ln -s ../init.d/mysql.ndbd S81mysql.ndbd )
cp support-files/mysql.server /etc/init.d/
chmod a+x /etc/init.d/mysql.server
(cd /etc/rc2.d/ && ln -s ../init.d/mysql.server S82mysql.server )


cat <<EOF > /etc/my.cnf
[mysql]
port=3306
socket=/var/run/mysqld/mysqld.sock
#
[mysqld]
# SQL node
ndbcluster
ndb-connectstring=${HOST_NAME_federation}
# basedir=/usr/local/mysql/
port=3306
socket=/var/run/mysqld/mysqld.sock
# socket=/tmp/mysql-cluster
bind-address=0.0.0.0
#
[mysql_cluster]
# NDB node
ndb-connectstring=${HOST_NAME_federation}
# basedir=/usr/local/mysql/
EOF

