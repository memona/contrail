#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Start mgmt node.
/etc/init.d/mysql.ndbd start
sleep 10
