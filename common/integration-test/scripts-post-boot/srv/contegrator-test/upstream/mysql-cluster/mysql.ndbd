#!/bin/sh

### BEGIN INIT INFO
# Provides:          mysql.ndbd
# Required-Start:    $network
# Required-Stop:     $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: MySQL ndbd
# Description:       MySQL ndbd
### END INIT INFO

# Source function library.
if [ -e /lib/lsb/init-functions ]
then
	. /lib/lsb/init-functions
else
	. /etc/init.d/functions
fi

# options for the daemon program
APP_NAME=ndbd
APP_NAME2="MySQL ndbd daemon"

MYSQL_USER=mysql

APP_BINARY="/usr/local/mysql/bin/ndbd"
APP_OPTIONS=""

pre_check() {
        exists=`grep -c $MYSQL_USER /etc/passwd`
        if [ $exists -eq 0 ]
        then
                echo "user $MYSQL_USER does not exist."
                exit 1
        fi
}

get_pid () {
        ps ax -o pid,comm | grep "$APP_BINARY" | awk '{print $1}'
}

start() {
    PID=`get_pid`
    if [ -n "$PID" ]
    then
	echo -n "$APP_NAME2 already started"
	echo
        return 0
    fi

    pre_check

    echo -n "starting $APP_NAME2..."
    exec $APP_BINARY $APP_OPTIONS
    sleep 3s
    PID=`get_pid`
    if [ -n "$PID" ]
    then
        echo " (PID $PID)"
    else
        echo ""
        return 1
    fi

    return 0
}

stop() {
    PID=`get_pid`
    if [ -n "$PID" ]
    then
        echo -n "stopping $APP_NAME2 (PID $PID)..."
        kill $PID
	echo
    fi

    return 0
}

status() {
    PID=`get_pid`
    if [ -n "$PID" ]
    then
	if [ ! -e /proc/$PID ]
	then
		echo "$APP_NAME2 has crashed (PID $PID)"
		return 1
	else
		echo "$APP_NAME2 is running (PID $PID)"
		return 0
	fi
    else
	echo "$APP_NAME2 is not running"
	return 3
    fi
}

# See how we were called.
case "$1" in
    start)
        start
        result=$?
        ;;
    start-initial)
        APP_OPTIONS="--initial"
        start
        result=$?
        ;;
    stop)
        stop
        result=$?
        ;;
    status)
        status
        result=$?
        ;;
    reload)
        result=0
        ;;
    restart)
        stop && sleep 1 && start
        result=$?
        ;;
    try-restart)
        ## Stop the service and if this succeeds (i.e. the
        ## service was running before), start it again.
        $0 status >/dev/null
        if [ $? -eq 0 ]; then
          $0 restart
          result=$?
        else
          result=0
        fi
        ;;
    *)
        echo -e "Usage: $0 {start|start-initial|stop|restart|reload|status|try-restart}\n"
        result=1
        ;;
esac

exit $result
