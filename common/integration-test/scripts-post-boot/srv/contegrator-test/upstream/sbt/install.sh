#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo "Install sbt..."
cd /usr/local/bin/
# Which sbt ?
# wget http://typesafe.artifactoryonline.com/typesafe/ivy-releases/org.scala-tools.sbt/sbt-launch/0.11.2/sbt-launch.jar
VER=0.7.4
VER=0.7.5
VER=0.7.7
wget http://simple-build-tool.googlecode.com/files/sbt-launch-$VER.jar
ln -sf sbt-launch-$VER.jar sbt-launch.jar
echo 'java -Xms512M -Xmx1536M -Xss1M -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=384M -jar `dirname $0`/sbt-launch.jar "$@"' > sbt
chmod a+x sbt

#
