#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# default file path
VEP_CONF=/root/.vep-gui/vep.properties
VEP_LOG_CONF=/root/.vep-gui/log4j.properties
VEP_KEYSTORE=/var/lib/contrail/vep/VEPKeyStore.jks
# currently, sqlite is used
VEP_SQLITE_DB=/var/lib/contrail/vep/vep.dp

do_binary() {
do_common
/srv/contegrator-test/edit-conf-file.sh $VEP_CONF \
	vepdb.choice=mysql \
	mysql.pass=contrail \
	one.ip=localhost \
	one.port=2633 \
	one.user=oneadmin \
	one.pass=310d133ccfaa2c1fbb0462c2beaa73cd \
	pdp.use=$CONTRAIL_ENV_USE_PDP
# one.user, one.pass, (one.ip, one.port)
#
#SQL="use vepdb;"
#SQL+="insert into ugroup (gname,uid) values ('admin', 1);"
#SQL+="insert into user (username,uid,vid,oneuser,onepass,oneid,role) values ('fedadmin',1,-1,0,'7bc8559a8fe509e680562b85c337f170956fcb06',-1,'admin');"
#mysql -u root -e "$SQL"
# ls /bla-2
}

do_targz() {
do_common
export DEBIAN_FRONTEND="noninteractive"
/usr/share/contrail/vep/mysql-setup.sh
/srv/contegrator-test/edit-conf-file.sh $VEP_CONF \
	vepdb.choice=mysql \
	mysql.pass=contrail \
	one.ip=localhost \
	one.port=2633 \
	one.user=oneadmin \
	one.pass=310d133ccfaa2c1fbb0462c2beaa73cd \
	pdp.use=$CONTRAIL_ENV_USE_PDP
#	vepdb.choice=sqlite \
#	mysql.pass=pass1234
}

do_common() {
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	VEP_USR_DIR=/usr/share/contrail/contrail-vep-gui
else
	VEP_USR_DIR=/usr/share/contrail/contrail-vep-gui
fi

# mysql already installed
mkdir -p `dirname $VEP_CONF`
mkdir -p `dirname $VEP_KEYSTORE`
[ ! -f $VEP_CONF ] && cp $VEP_USR_DIR/vep.properties $VEP_CONF
# [ ! -f $VEP_LOG_CONF ] && cp /usr/share/contrail/vep/log4j.properties $VEP_LOG_CONF
[ ! -f $VEP_LOG_CONF ] && touch $VEP_LOG_CONF
[ ! -f $VEP_KEYSTORE ] && cp /srv/contegrator-test/vep-cli/VEPKeyStore.jks $VEP_KEYSTORE


# mysql is on current node
/srv/contegrator-test/edit-conf-file.sh $VEP_CONF \
	vepdb.choice=sqlite \
	mysql.ip=localhost \
	mysql.port=3306 \
	mysql.user=vepuser \
	mysql.pass=pass1234 \
	sqlite.db=$VEP_SQLITE_DB

# ONE head is on node head
/srv/contegrator-test/edit-conf-file.sh $VEP_CONF \
	one.ip=$HOST_NAME_head \
	one.port=2633 \
	one.user=oneadmin \
	one.pass=310d133ccfaa2c1fbb0462c2beaa73cd \
	contrail.cluster=100 \
# TODO: contrail.cluster=1

# keystore
/srv/contegrator-test/edit-conf-file.sh $VEP_CONF \
	rest.keystore=$VEP_KEYSTORE

# pdp.use - true|false
/srv/contegrator-test/edit-conf-file.sh $VEP_CONF \
	pdp.use=$CONTRAIL_ENV_USE_PDP \
	pdp.endpoint=http://146.48.96.75:2000/contrailPDPwebApplication/contrailPDPsoap

echo "one.version=3.4" >> $VEP_CONF

# mysql should listen on all IPs
#/srv/contegrator-test/edit-conf-file.sh /etc/mysql/my.cnf \
#	bind-address=0.0.0.0 \
#service mysql restart


# sqlite setup
mkdir -p `dirname $VEP_SQLITE_DB`
cp /srv/contegrator-test/vep-cli/vep.db.n0006 $VEP_SQLITE_DB
chmod a+w $VEP_SQLITE_DB
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

#

