#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

VEP_CONF=/root/.vep-gui/vep.properties
VEP_LOG_CONF=/root/.vep-gui/log4j.properties
PARAM="-t -p $VEP_CONF -l $VEP_LOG_CONF"

do_binary() {
# answer yes to 'Do you want to reinitialize the database? (yes/no)'
nohup sh -c "echo yes | /usr/bin/contrail-vep-gui $PARAM" >> /var/log/contrail/vep.nohup.log &
sleep 10
# ls /hm
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_binary
fi

