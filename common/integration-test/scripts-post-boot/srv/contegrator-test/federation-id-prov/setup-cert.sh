#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

cd /srv/contegrator-test/federation-ca-server/files/
# Create truststore
cat cert/contrail-id-prov-server.key cert/contrail-id-prov-server.cert > cert/contrail-id-prov-server.certkey
openssl pkcs12 -export -in cert/contrail-id-prov-server.certkey -out cert/ks-id-prov.p12 -name contrail -noiter -nomaciter -passout pass:contrail

mkdir -p /var/lib/contrail/federation-id-prov
cd /srv/contegrator-test/federation-ca-server/files
cp cert/ks-id-prov.p12 /var/lib/contrail/federation-id-prov/
cp cert/contrailTrustStore /var/lib/contrail/federation-id-prov/
ls -la /var/lib/contrail/federation-id-prov/
