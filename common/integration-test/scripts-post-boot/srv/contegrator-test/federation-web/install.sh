#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

do_binary_ub1204() {
# ubuntu 12.04 prerequisites
#install_binary_always build-essential python-setuptools python-dev

# fails (dpkg configure stage), and then other packages fails too - kestrel etc
# gcc required to build python 2.7 eggs
#install_binary_always build-essential
# deb postinst expects /etc/contrail/contrail-federation-web
# mkdir -p /etc/contrail/contrail-federation-web-1204
# (cd /etc/contrail && ln -s contrail-federation-web-1204 contrail-federation-web)
# othervise:
#   File "/usr/lib/contrail/federation-web/eggs/Django-1.3.1-py2.7.egg/django/core/files/storage.py", line 223, in listdir
#    for entry in os.listdir(path):
#OSError: [Errno 2] No such file or directory: '/usr/lib/contrail/federation-web/src/federweb/settings/static'
#mkdir -p /usr/lib/contrail/federation-web/src/federweb/settings/static

#useradd -m contrail || true
#if [ ! -d '/home/contrail' ]
#then
#	mkdir /home/contrail
#	chown contrail:contrail /home/contrail
#fi
#apt-get install python-openssl

install_binary_always python-zookeeper # prerequisite, available in 12.04 only?
install_binary_always contrail-federation-web
}

do_binary() {
install_binary_always contrail-federation-web

# add user coordinator:password, user roles, and unix user contrail
#/usr/lib/contrail/federation-web/bin/django initial_data
# useradd -m contrail
#
# coordinator is FederationCoordinator
#curl http://localhost:8080/federation-api/users/4
#curl http://localhost:8080/federation-api/roles/2
# curl -X POST http://localhost:8080/federation-api/users/4/roles -H "Content-type: application/json" -d '{roleId:"2"}'
}

do_targz() {
echo "Get latest federation-web.tar.gz"
mkdir -p /var/tmp/feder-web
cd /var/tmp/feder-web
wget -O feder-web.a.html http://bamboo.ow2.org/artifact/CONTRAIL-TRUNK/JOB1/
MYLINE=`cat feder-web.a.html | grep '/artifact/CONTRAIL-TRUNK/JOB1/build-' | tail -n1`
echo "MYLINE = $MYLINE"
MYLINES=`echo $MYLINE | sed -e 's/<TD>/\n/g' -e 's/<\/TD>/\n/g' -e 's/<TR>/\n/g' -e 's/<\/TR>/\n/g'`
MYHREF=`echo "$MYLINES" | grep HREF | sed -e 's/^.*A HREF="//g' -e 's/">.*$//g'`
echo $MYHREF
wget http://bamboo.ow2.org/$MYHREF/Packages/federation/federation-web/target/federation-web.tar.gz

echo "Install federation-web, from tar.gz"
# python2.7 or python2.6 setuptools
# install_targz_dependency python-setuptools apache2 libapache2-mod-wsgi libzookeeper2
install_targz_dependency python-zookeeper python-chardet apache2 libapache2-mod-wsgi
useradd -m contrail
mkdir -p /var/lib/contrail/federation/federation-web/static
mkdir -p /usr/lib/contrail/federation/
mkdir -p /etc/contrail/federation

tar xvzpf /var/tmp/feder-web/federation-web.tar.gz
mv federation-web /usr/lib/contrail/federation-web
(cd /usr/lib/contrail/federation && ln -s ../federation-web federation-web)

pushd /usr/lib/contrail/federation-web
if [ "$CONF_OS_VERSION" == "12.04" ]
then
	install_binary_always build-essential
	make clean
	make build
fi
$PYTHON offline_bootstrap.py
./bin/buildout -No
popd

/usr/lib/contrail/federation-web/bin/django default_settings > /etc/contrail/federation/federation-web.conf
cp /usr/lib/contrail/federation-web/extra/apache-vhost /etc/apache2/sites-available/contrail-federation-web
sudo a2dissite default
sudo a2ensite contrail-federation-web

/usr/lib/contrail/federation-web/bin/django collectstatic --noinput
chown contrail:contrail /var/lib/contrail -R
chown contrail:contrail /usr/lib/contrail -R
chown contrail:contrail /etc/contrail/federation -R
}

# main
if [ "$CONF_OS_VERSION" == "12.04" ]
then
	PYTHON=python2.7
else
	PYTHON=python2.6
fi

if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	if [ "$CONF_OS_VERSION" == "12.04" ]
	then
		do_binary_ub1204
	else
		do_binary
	fi
else
	do_targz
fi
