#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

echo "Run federation-web"
/etc/init.d/apache2 stop
/etc/init.d/apache2 start

