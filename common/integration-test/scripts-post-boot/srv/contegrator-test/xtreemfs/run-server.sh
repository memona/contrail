#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo '-------------------------------------------------'
mv /etc/init.d/xtreemfs-dir{,.orig}
mv /etc/init.d/xtreemfs-mrc{,.orig}
mv /etc/init.d/xtreemfs-osd{,.orig}
cp -p /srv/contegrator-test/xtreemfs/tmp/* /etc/init.d/
echo '-------------------------------------------------'

/etc/init.d/xtreemfs-dir restart
/etc/init.d/xtreemfs-mrc restart
/etc/init.d/xtreemfs-osd restart

# wait for xtreemfs-dir to start
wget --retry-connrefused --tries=20 http://localhost:30638/
