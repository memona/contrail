#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Create a volume, for oneadmin:cloud user/group
# On federation node (server is now on ONE head)
#addgroup --gid=4000 cloud
#useradd --uid=4000 --gid=4000 oneadmin

# wait for xtreemfs-dir to start
#wget --retry-connrefused --tries=20 http://localhost:30638/
#sleep 10

/etc/init.d/xtreemfs-dir status
/etc/init.d/xtreemfs-mrc status
/etc/init.d/xtreemfs-osd status

mkfs.xtreemfs -u oneadmin -g cloud localhost/contrail-images
