#!/bin/sh

### BEGIN INIT INFO
# Provides:          xtreemfs-dir
# Required-Start:    $network $remote_fs
# Required-Stop:     $network $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: XtreemFS DIR
# Description:       XtreemFS DIR. http://www.xtreemfs.org/
### END INIT INFO

# Source function library.
if [ -e /lib/lsb/init-functions ]
then
	. /lib/lsb/init-functions
else
	. /etc/init.d/functions
fi

if [ -z $JAVA_HOME ]; then
	export JAVA_HOME=/usr
fi

# options for the daemon program
APP_NAME=xtreemfs-dir
APP_NAME2="XtreemFS Directory Service (DIR)"
CONFIG=/etc/xos/xtreemfs/dirconfig.properties
LOG=/var/log/xtreemfs/dir.log
XTREEMFS_USER=xtreemfs
# LOCKDIR=/var/lock/subsys
# LOCKFILE=$LOCKDIR/$APP_NAME
# /var/run/ is writable by root only, use subdir
PID=/var/run/xtreemfs/$APP_NAME.pid

JAVA_CALL="$JAVA_HOME/bin/java -cp /usr/share/java/XtreemFS.jar:/usr/share/java/BabuDB.jar:/usr/share/java/Flease.jar:/usr/share/java/protobuf-java-2.3.0.jar:/usr/share/java/Foundation.jar:/usr/share/java/jdmkrt.jar:/usr/share/java/jdmktk.jar"
JAVA_CALL="$JAVA_CALL org.xtreemfs.dir.DIR $CONFIG"

DAEMON=`which daemon`
daemon_args="--name $APP_NAME --pidfile $PID --user $XTREEMFS_USER --chdir / --output=$LOG"
# daemon_args="$daemon_args --respawn"
daemon_args="$daemon_args -v"

if [ -z "$DAEMON" ]
then
        echo "ERROR: daemon binary is not installed" 1>&2
        exit 1
fi
# change log file ownership
[ ! -f $LOG ] && touch $LOG
chown $XTREEMFS_USER $LOG

running() {
  $DAEMON $daemon_args --running
}

pre_check() {
        exists=`grep -c $XTREEMFS_USER /etc/passwd`
        if [ $exists -eq 0 ]
        then
                echo "user $XTREEMFS_USER does not exist."
                exit 1
        fi
        dir=`dirname $LOG`
        if [ ! -e $dir ]
        then
                echo "directory for logfiles $dir does not exist"
        fi

        [ ! -d /var/run/xtreemfs/ ] && mkdir /var/run/xtreemfs/
        chown $XTREEMFS_USER /var/run/xtreemfs/
        [ -f $LOG ] && chown $XTREEMFS_USER $LOG
}

start() {
    running
    if [ -f $PID ]
    then
	echo -n "$APP_NAME2 already started"
	echo
        return 0
    fi

    pre_check

    echo >> $LOG
    date >> $LOG
    echo -e "Starting $APP_NAME2...\n\n" >> $LOG

    echo -n "starting $APP_NAME2..."
    $DAEMON $daemon_args $daemon_start_args -- sh -c "exec $JAVA_CALL"
    # sudo -u $XTREEMFS_USER $JAVA_CALL org.xtreemfs.dir.DIR $CONFIG_DS >> $LOG_DS 2>&1 &
    # PROCPID=$!
    # echo $PROCPID > $PID_DS
    sleep 1s
    if [ -f $PID ]
    then
        echo " (PID `cat $PID`)"
    else
        echo ""
    fi

    if [ ! running ]
    then
	return 1
    fi

    return 0
}

stop() {
    if [ -f $PID ]
    then
        PROCPID=`cat $PID`
        echo -n "stopping $APP_NAME2 (PID $PROCPID)..."
        killproc -p $PID sudo
	echo
    fi

    return 0
}

status() {
    running
    if [ -f $PID ]
    then
        PROCPID=`cat $PID`
	if [ ! -e /proc/$PROCPID ]
	then
		echo "$APP_NAME2 has crashed (PID $PROCPID)"
		return 1
	else
		echo "$APP_NAME2 is running (PID $PROCPID)"
		return 0
	fi
    else
	echo "$APP_NAME2 is not running"
	return 3
    fi
}

# See how we were called.
case "$1" in
    start)
        start
        result=$?
        ;;
    stop)
        stop
        result=$?
        ;;
    status)
        status
        result=$?
        ;;
    reload)
        result=0
        ;;
    restart)
        stop && sleep 1 && start
        result=$?
        ;;
    try-restart)
        ## Stop the service and if this succeeds (i.e. the
        ## service was running before), start it again.
        $0 status >/dev/null
        if [ $? -eq 0 ]; then
          $0 restart
          result=$?
        else
          result=0
        fi
        ;;
    *)
        echo -e "Usage: $0 {start|stop|restart|reload|status|try-restart}\n"
        result=1
        ;;
esac

exit $result
