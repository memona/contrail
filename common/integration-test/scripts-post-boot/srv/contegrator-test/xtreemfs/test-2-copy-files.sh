#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Copy a few basic images to xtreemfs volume
echo "`date` asdf" > /srv/contrail/images/test-file

# we are on ONE head, local /srv/one-images contains some demo images
cp /srv/one-images/dsl-1.qcow2.z /srv/contrail/images/
cp /srv/one-images/ubu-11.10.qcow2.z /srv/contrail/images/
