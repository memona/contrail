#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# Backup and restore initrd. File created by update-initramfs does not work.
cp -f /boot/initrd.img-`uname -r` /boot/initrd.img-`uname -r`.backup
install_binary_always xtreemfs-client
cp -f /boot/initrd.img-`uname -r`.backup /boot/initrd.img-`uname -r`
