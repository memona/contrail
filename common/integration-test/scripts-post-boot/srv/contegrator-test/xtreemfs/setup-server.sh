#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# TODO:
cat >> /etc/xos/xtreemfs/mrcconfig.properties <<EOF
#
# added by $0
hostname = $HOST_NAME_head
EOF

cat >> /etc/xos/xtreemfs/osdconfig.properties <<EOF
#
# added by $0
hostname = $HOST_NAME_head
EOF
