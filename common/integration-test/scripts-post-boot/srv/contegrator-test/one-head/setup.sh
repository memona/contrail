#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

#
oneuser create contrail contrail

# TODO: ssh keys
# add onehost, test image is already present
# ssh_config - StrictHostKeyChecking no
# [ ! -z "$HOST_NAME_head" ] && onehost create $HOST_NAME_head im_kvm vmm_kvm tm_nfs
[ ! -z "$HOST_NAME_head" ] && onehost create $HOST_NAME_head --im im_kvm --vm vmm_kvm --net 802.1Q
[ ! -z "$HOST_NAME_worker" ] && onehost create $HOST_NAME_worker --im im_kvm --vm vmm_kvm --net 802.1Q
[ ! -z "$HOST_NAME_worker1" ] && onehost create $HOST_NAME_worker1 --im im_kvm --vm vmm_kvm --net 802.1Q
[ ! -z "$HOST_NAME_worker2" ] && onehost create $HOST_NAME_worker2 --im im_kvm --vm vmm_kvm --net 802.1Q
#
#onevnet publish private-lan
cd /srv/contegrator-test/one-head
onevnet create private-lan.onevnet-template
onevnet create public-lan.onevnet-template
# user contrail rights ... ?

oneimage create dsl-1.oneimage-template -d default
oneimage create ubuntu-stress.oneimage-template -d default
oneimage create ubu-11.10.oneimage-template -d default

oneacl create "@1 IMAGE+NET/* USE"

