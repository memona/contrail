#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

PATH=$PATH:/srv/pax-runner-1.7.1/bin
export PATH
export SLASOI_HOME="/srv/contrail-2/trunk/provider/sla-at-soi-platform/common/osgi-config/"

# ubuntu 11.x
# apt-get install python-software-properties
# add-apt-repository ppa:natecarlson/maven3
# apt-get update && sudo apt-get install maven3
apt-get update
apt-get install maven3
(cd /usr/bin && ln -s mvn3 mvn)
apt-get install openjdk-6-jdk
apt-get install subversion

rm -fr /srv/contrail-2/ /srv/pax-runner-1.7.1/
# killall java0
cd /srv
wget http://contrail.xlab.si/packages-targz/sla-soi.compiled.20121204.tar.gz
tar -xzf sla-soi.compiled.20121204.tar.gz


(cd /srv/contrail-2/trunk/provider/sla-at-soi-platform/generic-slamanager/syntax-converter && \
nohup ./syc-broker >> /var/log/contrail/sla-soi-syc-broker.nohup.log 2>&1 & )
# download takes ages... 10, 15 minutes...

(cd /srv/contrail-2/trunk/common/contrail-parent/ && \
mvn install )
(cd /srv/contrail-2/trunk/provider/sla/vepMock/ && \
nohup mvn exec:java -Dexec.mainClass="org.contrail.vep.VepContainerMock" >> /var/log/contrail/sla-soi-vep-mock.nohup.log 2>&1 & )

(cd /srv/contrail-2/trunk/provider/sla-at-soi-platform/pax-runner/ && \
nohup pax-run.sh >> /var/log/contrail/sla-soi-pax.nohup.log 2>&1 & )


#
