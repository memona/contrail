#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

cd /srv/contegrator-test/federation-id-prov-support/

# Set servername and alias.
sed -i 's/ServerName/## ServerName/' /etc/apache2/sites-enabled/simplesaml
sed -i '/## ServerName/ i\
ServerName contrail-idp.contrail.eu \
ServerAlias federation.contrail-idp.contrail.eu \
ServerAlias google.contrail-idp.contrail.eu \
' /etc/apache2/sites-enabled/simplesaml

# Will be configurable...
STR1='http://localhost:8080/federation-id-prov/users/authenticate'
STR2='http://localhost:8080/federation-api/users/authenticate'
sed -i "s|$STR1|$STR2|" /usr/share/simplesamlphp-1.9.0/modules/contrailmodule/lib/Auth/Source/CAuth.php

