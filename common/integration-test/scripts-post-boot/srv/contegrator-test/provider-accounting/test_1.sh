#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

curl -X POST http://localhost:8080/accounting/accounting/metric/cpu/load_one/history \
 --header "Content-Type: application/json" \
 -d '{"source":"host", "sid":"host001.test.com", "startTime":"2012-06-01T10:00:00.000+0200", "endTime":"2012-06-01T10:50:00.000+0200"}'

#
