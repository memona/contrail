#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo "Run provider-accounting"

do_binary() {
/etc/init.d/tomcat6 restart
}

do_targz() {
/etc/init.d/tomcat6 restart
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

#
