#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

# mongo
CONFIG=/etc/mongodb.conf
cp $CONFIG $CONFIG.orig
/srv/contegrator-test/edit-conf-file.sh $CONFIG \
	bind_ip=0.0.0.0


# CONFIG=/etc/contrail/contrail-provider-storage-manager/storage-manager.cfg
CONFIG=/etc/contrail/provider/storage-manager/storage-manager.cfg
cp $CONFIG $CONFIG.orig

/srv/contegrator-test/edit-conf-file.sh $CONFIG \
	RabbitMQListener.host=$HOST_NAME_head

#

