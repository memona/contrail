#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

# run script for VEP setup, and run VEP daemon
cd /srv/contegrator-test/vin/vep
./vep-setup.sh 0 1 2
# a test OVF should work now
/srv/contegrator-test/vep-cli/test-1-a.sh
# check for two running VMs
/srv/contegrator-test/vep-cli/test-1-b.sh

# and now VIN




./one-machine.sh two-on-host.vin &

# Copy log files after 350 sec
sleep 350 || true
# get IP of two VMs
ONEVM_ID=`onevm list | tail -n3 | head -n1 | awk '{print $1}'`
ONEVM_IP1=`onevm show $ONEVM_ID | grep '^[[:space:]].*IP=' | sed -e 's/^[[:space:]].*IP=//' -e 's/,//'`
ONEVM_IP1=`echo $ONEVM_IP1 | sed -e 's/^"//' -e 's/"$//'`
ONEVM_ID=`onevm list | tail -n2 | head -n1 | awk '{print $1}'`
ONEVM_IP2=`onevm show $ONEVM_ID | grep '^[[:space:]].*IP=' | sed -e 's/^[[:space:]].*IP=//' -e 's/,//'`
ONEVM_IP2=`echo $ONEVM_IP2 | sed -e 's/^"//' -e 's/"$//'`
ONEVM_ID=`onevm list | tail -n1 | head -n1 | awk '{print $1}'`
ONEVM_IP3=`onevm show $ONEVM_ID | grep '^[[:space:]].*IP=' | sed -e 's/^[[:space:]].*IP=//' -e 's/,//'`
ONEVM_IP3=`echo $ONEVM_IP3 | sed -e 's/^"//' -e 's/"$//'`
SCP="scp -i ./id_rsa_vin_test -o ConnectTimeout=5 -o PasswordAuthentication=no"
# don't fail no /var/log/contrail/ is found on VM 
$SCP -r root@${ONEVM_IP1}:/var/log/ /var/log/contrail/vin/onevm-${ONEVM_IP1} || true
$SCP -r root@${ONEVM_IP2}:/var/log/ /var/log/contrail/vin/onevm-${ONEVM_IP2} || true
$SCP -r root@${ONEVM_IP3}:/var/log/ /var/log/contrail/vin/onevm-${ONEVM_IP3} || true
#
#$SCP -r root@${ONEVM_IP1}:/var/log/context.log /var/log/contrail/vin/onevm-${ONEVM_IP1}-context.log || true
#$SCP -r root@${ONEVM_IP2}:/var/log/context.log /var/log/contrail/vin/onevm-${ONEVM_IP2}-context.log || true
#$SCP -r root@${ONEVM_IP3}:/var/log/context.log /var/log/contrail/vin/onevm-${ONEVM_IP3}-context.log || true
$SCP -r root@${ONEVM_IP1}:/etc/network/interfaces /var/log/contrail/vin/onevm-${ONEVM_IP1}-interfaces || true
$SCP -r root@${ONEVM_IP2}:/etc/network/interfaces /var/log/contrail/vin/onevm-${ONEVM_IP2}-interfaces || true
$SCP -r root@${ONEVM_IP3}:/etc/network/interfaces /var/log/contrail/vin/onevm-${ONEVM_IP3}-interfaces || true

#
