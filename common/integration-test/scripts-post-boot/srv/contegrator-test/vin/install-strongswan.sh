#!/bin/bash

source /srv/contegrator-test/env.sh

# exit on error
set -e
# echo cmd
set -x

export DEBIAN_FRONTEND=noninteractive
apt-get update
#apt-get -q -y --force-yes install contrail-strongswan
apt-get -q -y --force-yes install strongswan

# "whoami", get VNFS name
# "10.31.1.253:/usr/var/lib/perceus//vnfs/test-vnfs-vin-0.2-SNAPSHOT-head/rootfs on / type nfs ... "
# head, worker1 or worker2?
NODE_NAME=`mount | grep ' on / type' | sed "s|/|\n|g" | grep '^test-vnfs-' | sed "s/-/\n/g" | tail -n1`

# config certs
CERT_SRC="/srv/contegrator-test/vin/strongswan-ca"
cp ${CERT_SRC}/ca-cert.der /etc/ipsec.d/cacerts/
cp ${CERT_SRC}/one-${NODE_NAME}-cert.der /etc/ipsec.d/certs/
cp ${CERT_SRC}/one-${NODE_NAME}-key.der /etc/ipsec.d/private/
cp ${CERT_SRC}/one-${NODE_NAME}-cert.der /etc/ipsec.d/certs/dummy-cert.der
cp ${CERT_SRC}/one-${NODE_NAME}-key.der /etc/ipsec.d/private/dummy-key.der
# here should be some sample/test config ?
touch /var/lib/strongswan/ipsec.secrets.inc

# Include vin configuration files.
#echo "include /var/lib/contrail/vin/strongSwan/*.conf" >> /etc/ipsec.conf
#mkdir -p /var/lib/contrail/vin/strongSwan
#touch /var/lib/contrail/vin/strongSwan/blabla.conf
#chmod 0777 /var/lib/contrail/vin/strongSwan

service ipsec restart

#
