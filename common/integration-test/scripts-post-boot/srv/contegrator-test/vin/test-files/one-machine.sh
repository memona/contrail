#!/bin/sh
set -e -u
set -x
# untarred vin-support.tar.gz
VIN_HOME=/usr/share/contrail/support/vin
# where is controller.properties
TEST_FILES_DIR=/srv/contegrator-test/vin/test-files

hostname
hostname -I

# Jar-files from library.
LIBCLASSPATH="$TEST_FILES_DIR/logconfig"
add_to_libclasspath () {
    JARFILES=`cd "$1" && ls *.jar 2>/dev/null`
    for i in ${JARFILES} ; do
        LIBCLASSPATH="$LIBCLASSPATH:$1/$i"
    done
}

#add_to_libclasspath "${VIN_HOME}"/used-jars/ipl
#add_to_libclasspath "${VIN_HOME}"/used-jars
add_to_libclasspath "${VIN_HOME}"/jars

# And finally, run ...
echo "Starting VIN controller"
java \
    -server \
    -classpath bin:"${CLASSPATH-""}:$LIBCLASSPATH" \
    -enableassertions \
     org.ow2.contrail.resource.vin.controller.tester.CommandLine -p=$TEST_FILES_DIR/controller.properties $TEST_FILES_DIR/$*
echo "VIN controller has stopped"
