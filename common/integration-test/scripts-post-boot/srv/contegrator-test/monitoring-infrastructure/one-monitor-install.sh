#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

do_binary() {
# req for ubuntu 12.04 only?
install_binary_always rabbitmq-server tomcat6

install_binary contrail-one-monitor
}

do_targz() {
# ruby1.9 etc installed in chroot
# apt-get -q -y --force-yes install sun-java6-jre # partner repo
install_targz_dependency rabbitmq-server tomcat6
# ganglia-monitor (node), gmond ?
}

# main
if [ "$CONF_REPOSITORY_MODE" == "binary" ]
then
	do_binary
else
	do_targz
fi

#
