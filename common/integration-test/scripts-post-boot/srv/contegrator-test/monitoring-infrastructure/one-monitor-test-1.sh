#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo "ONE monitor minimal test"
# just check if someone is listening on correct port
netstat -plane | grep java | grep '1234[[:space:]]'

#
