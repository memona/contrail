#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo "Setup ONE sensor config"
CONF="/etc/contrail/contrail-one-sensor/one-sensor.config"
/srv/contegrator-test/edit-conf-file.sh $CONF \
	rabbit_mq_host=$HOST_NAME_head \
	log4j_log_level=info \
	host_properties_file=/etc/contrail/contrail-one-sensor/hostConfig \
#

#
