#!/bin/bash

# Called if test fails, to give some time for manually correcting problem.

# exit on error
set -e
# echo cmd
#set -x

WAIT_TIME=100
SLEEP_TIME=10

# optional param
FILE2="/tmp/wait_file"
[ $# -ge 1 ] && WAIT_TIME=$1
[ $# -ge 2 ] && FILE2=$2
FILE1="$FILE2.$$"

cat <<EOF

`date`
$0 $@
Waiting on someone to
rm $FILE1
Do
touch $FILE2
to wait longer than $WAIT_TIME seconds.


EOF

STATUS=10

touch $FILE1
while [ -f $FILE1 ] && [ $WAIT_TIME -gt 0 ]
do
	sleep $SLEEP_TIME || true
	WAIT_TIME=$(($WAIT_TIME - $SLEEP_TIME))
done
if [ -f $FILE1 ]
then
	# timeout, user did not interact
	STATUS=11
else
	STATUS=0
fi

if [ -f $FILE2 ]
then
	echo "File $FILE2 exists, waiting up to inf secondes..."
	STATUS=0
fi
while [ -f $FILE2 ]
do
	sleep 11
done

exit $STATUS
#
