#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

CONFIG=/etc/contrail/contrail-provisioning-manager/ProvisioningManager.settings

/srv/contegrator-test/edit-conf-file.sh $CONFIG \
	PROVISIONING_MANAGER_IP=$HOST_NAME_head \
	RABBITMQ_IP=$HOST_NAME_head \
	VEP_IP=$HOST_NAME_head

#

