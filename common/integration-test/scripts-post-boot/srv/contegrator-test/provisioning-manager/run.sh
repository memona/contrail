#!/bin/bash

source /srv/contegrator-test/env.sh
# exit on error
set -e
# echo cmd
set -x

echo "Run provisioning-manager"
CONFIG=/etc/contrail/contrail-provisioning-manager/ProvisioningManager.settings
LOG=/var/log/contrail/contrail-provisioning-manager.log

CP="`find /usr/share/contrail/provisioning-manager/lib/ -iname '*.jar' -exec echo -n {}\: \;`"
nohup java -classpath $CP org.ow2.contrail.provider.provisioningmanager.Main $CONFIG >> $LOG 2>&1 &
sleep 10 # nohup needs some time ?
echo "  done"
#

