# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


def test_02_install_monitoring(ct_node_list, ct_nodeman):
    # federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    worker = ct_node_list["worker"]
    worker1 = ct_node_list["worker1"]
    worker2 = ct_node_list["worker2"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/monitoring-infrastructure/one-monitor-install.sh")

    ct_nodeman.testrun("root", head, "/srv/contegrator-test/monitoring-infrastructure/one-sensor-install.sh")
    ct_nodeman.testrun("root", worker, "/srv/contegrator-test/monitoring-infrastructure/one-sensor-install.sh")
    ct_nodeman.testrun("root", worker1, "/srv/contegrator-test/monitoring-infrastructure/one-sensor-install.sh")
    ct_nodeman.testrun("root", worker2, "/srv/contegrator-test/monitoring-infrastructure/one-sensor-install.sh")

#

