# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes

def test_01_install(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    worker = ct_node_list["worker"]
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/upstream/rabbitmq/install.sh")
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/provisioning-manager/install.sh")

#

