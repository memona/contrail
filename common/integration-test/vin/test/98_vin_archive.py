# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes

# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def test_01_archive(ct_node_list, ct_nodeman):
    head = ct_node_list["head"]
    worker1 = ct_node_list["worker1"]
    worker2 = ct_node_list["worker2"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    # ct_nodeman.getfile(head, "/var/log/syslog", "./archive/head/syslog.txt" )
    ct_nodeman.getfile(head, "/var/log/", "./archive/head/var-log" )
    ct_nodeman.getfile(worker1, "/var/log/", "./archive/worker1/var-log" )
    ct_nodeman.getfile(worker2, "/var/log/", "./archive/worker2/var-log" )

#
