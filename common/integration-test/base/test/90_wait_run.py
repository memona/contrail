# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


# Test should run for 5 min.
# Then, logs are saved and nodes are shutdown.
def test_03_wait(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    #worker = ct_node_list["worker"]
    #worker1 = ct_node_list["worker1"]
    #worker2 = ct_node_list["worker2"]
    if federation:
        node = federation
    elif head:
        node = head
    else:
        node = None
    sec = 302
    print "Test will run for " + str(sec) + " secundes..."
    ct_nodeman.testrun("root", node, "sleep " + str(sec) + " || true")

#

