# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def test_99_wait(ct_node_list, ct_nodeman):
    worker = ct_node_list["worker"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    try:
        ct_nodeman.testrun("root", worker, "/srv/contegrator-test/wait-before-shutdown.sh 303")
        # ct_nodeman.sshrun("root", worker, "/usr/bin/one-test-run.sh")
    except:
        print "Test failed"
        raise
    finally:
        pass

def test_01_install(ct_node_list, ct_nodeman):
    worker = ct_node_list["worker"]
    try:
        ct_nodeman.testrun("root", worker, "date > /root/timestamp.txt")
        # test-2.sh comes from config/xyz.tar.gz
        ct_nodeman.testrun("root", worker, "/root/some-dir/test-2.sh || true")
        ct_nodeman.testrun("root", worker, "/srv/contegrator-test/monitoring-core/worker-install.sh")
    except:
        print "Test failed"
        raise
    finally:
        pass


def test_02_setup(ct_node_list, ct_nodeman):
    worker = ct_node_list["worker"]
    try:
        ct_nodeman.testrun("root", worker, "/srv/contegrator-test/monitoring-core/worker-setup.sh")
    except:
        print "Test failed"
        raise
    finally:
        pass

def test_03_run(ct_node_list, ct_nodeman):
    worker = ct_node_list["worker"]
    try:
        # will wait until monitoring-worker-install.sh finishes...
        ct_nodeman.testrun("root", worker, "/srv/contegrator-test/monitoring-core/worker-run.sh")
    except:
        print "Test failed"
        raise
    finally:
        pass

#

