# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins
def test_01_setup_mgmd(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/upstream/mysql-cluster/setup-mgmd.sh")

def test_02_setup_ndbd_sql(ct_node_list, ct_nodeman):
    federation = ct_node_list["federation"]
    head = ct_node_list["head"]
    # worker = ct_node_list["worker"]
    # ct_nodeman.sshrun("root", worker, "read wait_on_kill2")
    ct_nodeman.testrun("root", federation, "/srv/contegrator-test/upstream/mysql-cluster/setup-ndbd-sql.sh")
    ct_nodeman.testrun("root", head, "/srv/contegrator-test/upstream/mysql-cluster/setup-ndbd-sql.sh")
#

