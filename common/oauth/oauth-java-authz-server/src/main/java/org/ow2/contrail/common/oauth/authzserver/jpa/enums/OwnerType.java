package org.ow2.contrail.common.oauth.authzserver.jpa.enums;

public enum OwnerType {
    USER,
    SERVICE
}
