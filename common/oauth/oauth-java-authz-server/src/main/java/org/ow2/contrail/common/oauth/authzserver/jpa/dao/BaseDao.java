package org.ow2.contrail.common.oauth.authzserver.jpa.dao;

import javax.persistence.EntityManager;

public abstract class BaseDao {
    protected EntityManager em;

    protected BaseDao(EntityManager em) {
        this.em = em;
    }
}
