'''
Created on Aug 31, 2011

@author: jaka
'''

from perceus.perceus import Perceus
from image.imagedef import ImageDef
from image.imager import Imager

from testbedconfig import node_passwords, ct_perceus_util_node, ct_perceus_util_user
from conf import ct_conf

from fabric.api import env, get
from fabric.operations import run, _AttributeString
from fabric.sftp import SFTP

import logging
import time
import subprocess
import os
import socket

class ResourceError(Exception):
    def __init__(self, msg):
        self._msg = msg
    
    def get_message(self):
        return self._msg


class Node(object):
    def __init__(self, **nodeparams):
        self._name = nodeparams['name']
        self._host = nodeparams['host']
        self._image_name = nodeparams['image_name']
        self._image_type = nodeparams['image_type']
        
    def get_name(self):
        return self._name
        
    def get_host_name(self):
        return self._host
    
    def get_image_name(self):
        return self._image_name
    
    def get_image_type(self):
        return self._image_type
    
    def __str__(self, *args, **kwargs):
        return "(" + self._host + "," + self._image_type + ":" + self._image_name + ")" 


class NodeMan(object):
    def __init__(self, available_nodes = None,
                 perceus_util_cmd = "/srv/util/perceus-util.sh"):
        self._perceus_util_cmd = perceus_util_cmd
        self._perceus = Perceus(available_nodes)
        self._phy_nodes = []
    
    def setup_fabric_env(self):
        env.shell = "/bin/bash -l -i -c"
        env.passwords = node_passwords
        env.warn_only = True
        env.disable_known_hosts = True

    def _run_via_ssh(self, node, cmd, **kwargs):
        args = { "user": "root",
                 "timeout": 30 }
        args.update(kwargs)
        self.setup_fabric_env()
        env.host_string = args["user"] + "@" + node
        cmd_str = " ".join(cmd)
        logging.debug("Running [" + cmd_str + "] on " + env.host_string)
        try:
            rv = run(cmd_str)
        except SystemExit as e:
            rv = _AttributeString("")
            rv.succeeded = False
            rv.failed = True
            rv.return_code = -1
        except socket.error as e:
            print 'nodeman-socket-error'
            rv = _AttributeString("")
            rv.succeeded = False
            rv.failed = True
            rv.return_code = -1
        logging.debug("Success:     " + str(rv.succeeded))
        logging.debug("Return code: " + str(rv.return_code))
        logging.debug("Output:      " + rv)
        return rv
    
    def sshrun(self, user, host, cmd):
        self.setup_fabric_env()
        env.host_string = user + "@" + host
        try:
            rv = run(cmd)
        except SystemExit as e:
            rv = _AttributeString("")
            rv.succeeded = False
            rv.failed = True
            rv.return_code = -1
        logging.debug("Success:     " + str(rv.succeeded))
        logging.debug("Return code: " + str(rv.return_code))
        logging.debug("Output:      " + rv)
        return rv

    # Called from test.py files
    def testrun(self, user, host, cmd):
        if not host:
            return
        rv = self.sshrun(user, host, cmd)
        if rv.failed:
            raise Exception("sshrun() failed")

    # Get (log) file with scp.
    # Called from test.py files
    def getfile(self, node, src, dest):
        if not node:
            return
        args = { "user": "root",
                 "timeout": 30 }
        self.setup_fabric_env()
        env.host_string = args["user"] + "@" + node
        try:
            ftp=SFTP(env.host_string)
            if ftp.isdir(src):
                self._get_dir_safe(ftp, src, dest)
            else:
                self._get_file_safe(ftp, src, dest)
            logging.info("File copied: " + node + ":" + src + " -> " + dest)
        except:
            logging.exception("Error: copy file " + node + ":" + src + " -> " + dest)

    def _get_dir_safe(self, ftp, srcd, destd):
        srcd_dirname = os.path.dirname(srcd)   # '/root'
        srcd_basename = os.path.basename(srcd) # 'd4'
        # create destdir
        if not os.path.exists(destd):
            os.makedirs(destd)
        #print ('AA', srcd_dirname, srcd_basename)
        for entry in ftp.walk(srcd):
            #print entry
            entry_dirname = entry[0]
            entry_subdirs = entry[1]
            entry_files = entry[2]
            # create current subdir
            rel_subdir = entry_dirname[len(srcd_dirname)+1:] # 'tmp2/tmp3'
            dest_subdir = os.path.join(destd, rel_subdir)
            if not os.path.exists(dest_subdir):
                os.mkdir(dest_subdir)
            # copy files in current subdir
            for ff in entry_files:
                src_file = os.path.join(entry_dirname, ff)
                dest_file = os.path.join(dest_subdir, ff)
                #print ('BB', src_file, dest_file)
                self._get_file_safe(ftp, src_file, dest_file)
    
    # get only small files
    def _get_file_safe(self, ftp, src, dest):
        attr=ftp.stat(src)
        # CvR: Sorry, but I really need read a large file
        # max_size = 10*1024*1024
        max_size = 110*1024*1024
        if attr.st_size > max_size:
            logging.error("File '" + src + "' is too large (size is " + str(attr.st_size) + ", limit is " + str(max_size) + ")")
            return
        get(src, dest)

    def _log_cmd(self, cmd_list):
        logging.debug("Command: " + " ".join(cmd_list))

    def _ucmd(self, args):
        cmd = ["sudo", self._perceus_util_cmd]
        cmd.extend(args)
        self._log_cmd(cmd)
        return cmd

    def _pu_reboot_node(self, node):
        try:
            self.sshrun(ct_perceus_util_user, ct_perceus_util_node,
                        " ".join(self._ucmd(["node", "power", "of", node])))
            time.sleep(3)
            self.sshrun(ct_perceus_util_user, ct_perceus_util_node,
                        " ".join(self._ucmd(["node", "power", "on", node])))
            time.sleep(3)
            self.sshrun(ct_perceus_util_user, ct_perceus_util_node,
                        " ".join(self._ucmd(["node", "power", "wol", node])))
            return True
        except Exception, e:
            logging.exception("Failed to reboot node.")
            return False

    def _pu_shutdown_node(self, node):
        try:
            self.sshrun(ct_perceus_util_user, ct_perceus_util_node,
                        " ".join(self._ucmd(["node", "power", "of", node])))
            return True
        except Exception, e:
            logging.exception("Failed to shutdown node.")
            return False

    def _add_phy_node(self, name, vnfs):
        host = self._perceus.acquire_node(vnfs)
        if host is None:
            raise ResourceError("No more nodes left.")
        node = Node(name = name, host = host, image_name = vnfs, image_type = ImageDef.IMAGE_TYPE_VNFS)
        logging.debug("Added new physical node " + str(node))
        self._phy_nodes.append(node)
        return node

    def add_phy_nodes(self, images):
        nodes = {}
        for image in images:
            if images[image]:
                nodes[image] = self._add_phy_node(image, images[image].image_name)
            else:
                # node listed in ignore-nodes
                nodes[image] = None
        return nodes

    def _remove_phy_node(self, node):
        self._perceus.release_node(node.get_host_name())
        self._phy_nodes.remove(node)
        logging.debug("Removed physical node " + str(node) + ' (' + node.get_host_name() + ')')
                
    def get_phy_nodes(self):
        return self._phy_nodes

    def reboot_phy_node(self, node):
        self._pu_reboot_node(node.get_host_name())

    def shutdown_phy_node(self, node):
        self._pu_shutdown_node(node.get_host_name())

    def shutdown_phy_nodes(self):
        for node in self._phy_nodes:
            self.shutdown_phy_node(node)

    # shutdown with halt cmd, wait, do power off afterwards
    def shutdown_phy_nodes_halt(self, wait_time = 300):
        for node in self._phy_nodes:
            self._halt_phy_node(node)
        self._wait_for_phy_nodes_halt(wait_time)
        time.sleep(5)
        # power off
        self.shutdown_phy_nodes()
        for node in self._phy_nodes[:]:
            self._remove_phy_node(node)

    # execute halt cmd
    def _halt_phy_node(self, node):
        output = self._run_via_ssh(node.get_host_name(), [ "sh -c 'halt && exit'" ])
        return (output.succeeded == True and output.return_code == 0)

    # test with fping
    def _wait_for_phy_nodes_halt(self, wait_time = 300, sleep_time = 10):
        while wait_time >= 0:
            node_alive = 0
            for node in self._phy_nodes:
                ret = subprocess.call("fping -q -c 1 -t 100 " + node.get_host_name(), shell = True)
                logging.debug("Node " + node.get_host_name() + " alive check: " + str(ret))
                if ret == 0:
                    node_alive += 1
            if node_alive == 0:
                logging.debug("OK, all nodes dead")
                return True
            wait_time -= sleep_time
            time.sleep(sleep_time)
        logging.error("Timeout waiting for nodes shutdown")
        return False
            

    def is_phy_node_booted(self, node):
        output = self._run_via_ssh(node.get_host_name(), [ "cat", Imager.VNFS_ID_PATH ])
        return (output.succeeded == True and output.return_code == 0 and output == node.get_image_name())

    # return True/False and list of booted/not-booted node objects (get_name() - head, worker, federation)
    def wait_for_phy_nodes(self, retries = 0, sleep_time = 60):
        logging.debug("Waiting for physical nodes to boot.")
        while True:
            n = 0
            booted_nodes = []
            not_booted_nodes = []
            for node in self._phy_nodes:
                if self.is_phy_node_booted(node):
                    logging.debug("Node " + str(node) + " booted.")
                    booted_nodes.append(node)
                    n += 1
                else:
                    logging.debug("Node " + str(node) + " not booted yet.")
                    not_booted_nodes.append(node)
                    # break
            if n == len(self._phy_nodes):
                return True, not_booted_nodes
            logging.debug("Retries left: " + str(retries))
            retries -= 1
            if retries < 0:
                return False, not_booted_nodes
            time.sleep(sleep_time)
