'''
Created on Aug 31, 2011

@author: jaka
'''

from image.imagedef import ImageDef, ImageDefError
from image.repodef import RepoDef, RepoDefError
from perceus.perceus import Perceus
from image.installer import Installer

from conf import ct_conf

import tempfile
import logging
import os
import shutil
import urllib
import subprocess

usage = """ct-imager.py <release-tag> <nodes-dir>"""

class RepositoryMan(object):
    def __init__(self):
        pass
    
    # Take RepoDef, filter out suitable binary repositories (apt/deb, rpm), 
    # and insert additional configuration into node root FS to include the repositories.
    def add_repositories(self, repodef, mount_path):
        # TODO: use conf.py
        release = ct_conf['repository']['release']
        os_name = ct_conf['os']['name']
        os_version = ct_conf['os']['version']
        logging.info("Repository parameters : " + str((release, os_name, os_version)))
        repos = repodef.get_binary_repositories(release, os_name, os_version)
        self._add_repositories_deb(repos, mount_path)
            
    def _add_repositories_deb(self, repositories, mount_path):
        temp_file = '/var/tmp/contegrator.list'
        dest_file = '/etc/apt/sources.list.d/contegrator.list'
        fout = open(mount_path + temp_file, 'w')
        for repo in repositories:
            conf_line = 'deb ' + repo.url + ' ./'
            fout.write(conf_line + '\n')
            logging.debug("    Repository: " + conf_line)
        fout.close()
        cmd = "sudo chroot " + mount_path + ' /bin/sh -c "cp -f ' + temp_file + " " + dest_file + '"'
        subprocess.check_call(cmd, shell=True)
        
        
class Imager(object):
    
    VNFS_ID_FILE = "vnfs_id.txt"
    VNFS_ID_PATH = "/" + VNFS_ID_FILE
    
    def __init__(self, release_tag, nodes_cfg_dir, vnfsname_tag, node_list = None):
        self._release_tag = release_tag
        self._vnfsname_tag = vnfsname_tag
        self._nodes_cfg_dir = nodes_cfg_dir
        self._perceus = Perceus(node_list)
        self._images = []
        self._repodef = []

    def _get_test_vnfs_name(self, node_id):
        return "test-vnfs-" + self._vnfsname_tag + "-" + self._release_tag + "-" + node_id
    
    # copy scripts-post-boot directory to new VNFS
    def _copy_scripts_pb(self, scripts_pb_dir, mount_path, tmp_dir_rel, tmp_file):
        tmp_dir_abs = mount_path + "/" + tmp_dir_rel
        if os.path.isdir(scripts_pb_dir):
            try:
                # avoid directories, so that owner etc is not changed
                cmd = "find " + scripts_pb_dir + " -type f"
                file_list = subprocess.check_output(cmd, shell = True)
                if len(file_list) == 0:
                    logging.info("No files in " + scripts_pb_dir)
                else:
                    cmd = "cd " + scripts_pb_dir + " && tar cf " + tmp_dir_abs+"/"+tmp_file + " `find ./ -type f`"
                    logging.debug("tar copy-1 cmd: " + cmd)
                    subprocess.check_call(cmd, shell = True)
                    cmd = "sudo chroot " + mount_path + " tar xvf " + tmp_dir_rel+"/"+tmp_file
                    logging.debug("tar copy-2 cmd: " + cmd)
                    subprocess.check_call(cmd, shell = True)
            except:
                logging.exception("Failed to copy scripts-post-boot directory (" + scripts_pb_dir + " -> " + mount_path + ")")
                return False
        return True

    # Untar contrail tar.gz packages.
    def _prepare_node_install_contrail_targz(self, imagedef, mount_path, tmp_dir_rel, installer):
        # fetch contrail packages
        if len(imagedef.get_contrail_packages()) > 0:
            pkg_dir = tempfile.mkdtemp(".tmp", "contrail", dir = mount_path + tmp_dir_rel)
            pkg_dir_rel = os.path.relpath(pkg_dir, mount_path)
            cmd = "sudo chroot " + mount_path + " chmod 0777 " + pkg_dir_rel
            subprocess.check_call(cmd, shell = True)
            logging.debug("Fetching contrail packages to " + pkg_dir + ": " +
                          " ".join(imagedef.get_contrail_packages()) + " ...")
            local_pkg_paths_rel = []
            for pkg, pkg_url in zip(imagedef.get_contrail_packages(), imagedef.get_contrail_packages_url()):
                try:
                    #pkg_url = imagedef.get_repository() + "/" + self._release_tag + "/" + pkg
                    local_pkg_path = pkg_dir + "/" + pkg
                    logging.debug("Fetching " + pkg_url + " to " + local_pkg_path)
                    (fname, headers) = urllib.urlretrieve(pkg_url, local_pkg_path)
                    local_pkg_paths_rel.append(os.path.relpath(fname, mount_path))
                    logging.debug("Fetched package " + pkg_url + " to " + fname)
                except:
                    logging.exception("Failed to retrieve Contrail package: " + pkg)
                    return None
            logging.debug("Contrail packages fetched. Unpacking ...")
            # install contrail packages
            if not installer.unpack_bamboo(local_pkg_paths_rel):
                logging.error("Failed to unpack Contrail packages.")
                return None
            logging.debug("Contrail packages unpacked.")
            try:
                pass # shutil.rmtree(pkg_dir)
            except:
                logging.exception("Failed to remove temporary package dir: " + pkg_dir)
        else:
            logging.debug("No contrail packages to fetch.")
            
    # Untar non-contrail tar.gz packages.
    def _prepare_node_install_targz(self, imagedef, mount_path, tmp_dir_rel, installer):
        # fetch targz packages
        if len(imagedef.get_targz_packages()) > 0:
            pkg_dir = tempfile.mkdtemp(".tmp", "contrail-targz-", dir = mount_path + tmp_dir_rel)
            pkg_dir_rel = os.path.relpath(pkg_dir, mount_path)
            cmd = "sudo chroot " + mount_path + " chmod 0777 " + pkg_dir_rel
            subprocess.check_call(cmd, shell = True)
            logging.debug("Fetching targz packages to " + pkg_dir + ": " +
                          " ".join(imagedef.get_targz_packages()) + " ...")
            local_pkg_paths_rel = []
            for pkg, pkg_url in zip(imagedef.get_targz_packages(), imagedef.get_targz_packages_url()):
                try:
                    #pkg_url = imagedef.get_repository() + "/" + self._release_tag + "/" + pkg
                    local_pkg_path = pkg_dir + "/" + pkg
                    logging.debug("Fetching " + pkg_url + " to " + local_pkg_path)
                    (fname, headers) = urllib.urlretrieve(pkg_url, local_pkg_path)
                    local_pkg_paths_rel.append(os.path.relpath(fname, mount_path))
                    logging.debug("Fetched package " + pkg_url + " to " + fname)
                except:
                    logging.exception("Failed to retrieve targz package: " + pkg)
                    return None
            logging.debug("Targz packages fetched. Unpacking ...")
            # install targz packages
            if not installer.unpack(local_pkg_paths_rel):
                logging.error("Failed to unpack targz packages.")
                return None
            logging.debug("Targz packages unpacked.")
            try:
                pass # shutil.rmtree(pkg_dir)
            except:
                logging.exception("Failed to remove temporary package dir: " + pkg_dir)
        else:
            logging.debug("No contrail packages to fetch.")

    # Untar config packages.
    # Files could be over written by binary deb package
    def _prepare_node_install_config(self, imagedef, node_cfg_dir, mount_path, tmp_dir_rel, installer):
        # unpack config files
        cfg_dir = os.path.join(node_cfg_dir, "config")
        if os.path.isdir(cfg_dir):
            pkg_dir = tempfile.mkdtemp(".tmp", "config-", dir = mount_path + tmp_dir_rel)
            pkg_dir_rel = os.path.relpath(pkg_dir, mount_path)
            cmd = "sudo chroot " + mount_path + " chmod 0777 " + pkg_dir_rel
            subprocess.check_call(cmd, shell = True)
            tmp_cfg_files = os.listdir(cfg_dir)
            cfg_files = []
            for f in tmp_cfg_files:
                p = os.path.join(cfg_dir, f)
                if not f.startswith(".") and f.endswith(".tar.gz") and os.path.isfile(p):
                    shutil.copyfile(p, os.path.join(pkg_dir, f))
                    cfg_files.append(pkg_dir_rel + "/" + f)
            if len(cfg_files) > 0:
                logging.debug("Unpacking config files ...")
                if not installer.unpack(cfg_files):
                    logging.error("Failed to unpack configuration tarballs.")
                    return None
                logging.debug("Config files unpacked.")
            else:
                logging.debug("No config files to unpack.")

    def _prepare_node_vnfs(self, imagedef, node_id):
        node_cfg_dir = os.path.join(self._nodes_cfg_dir, node_id)
        logging.debug("Preparing VNFS node: " + node_cfg_dir + "," +
                      self._release_tag + "," + node_id) 
        test_vnfs = self._get_test_vnfs_name(node_id)
        # delete old VNFS if required
        if self._perceus.exist_vnfs(test_vnfs):
            logging.info("Old VNFS " + test_vnfs + " found, deleting...")
            self._perceus.umount_vnfs(test_vnfs)
            self._perceus.delete_vnfs(test_vnfs)
        # clone VNFS
        if not self._perceus.clone_vnfs(imagedef.get_base_image_name(), test_vnfs):
            logging.error("Failed to clone test VNFS.")
            return None
        logging.debug("VNFS cloned to: " + test_vnfs)
        mount_path = self._perceus.mount_vnfs(test_vnfs)
        if mount_path is None:
            logging.error("Failed to mount test VNFS.")
            return None
        logging.debug("VNFS mounted to: " + mount_path)
        # fix fstab in case it sports NFS mounts
        orig_fstab = os.path.join(mount_path, "etc", "fstab")
        bak_fstab  = os.path.join(mount_path, "etc", "fstab.bak")
        cmd = "sudo chroot " + mount_path + " cp /etc/fstab /etc/fstab.bak"
        subprocess.check_call(cmd, shell = True)
        cmd = "sudo chroot " + mount_path + " chmod o+w /etc/fstab"
        subprocess.check_call(cmd, shell = True)
        with open(bak_fstab, "r") as bak_f:
            with open(orig_fstab, "w") as orig_f:
                line = bak_f.readline()
                while(len(line) > 0):
                    if(line.find("nfs") != -1 and line.find(imagedef.get_base_image_name())):
                        line = line.replace(imagedef.get_base_image_name(), test_vnfs)
                    orig_f.write(line)
                    line = bak_f.readline()
        cmd = "sudo chroot " + mount_path + " chmod o-w /etc/fstab"
        subprocess.check_call(cmd, shell = True)
        # add additional binary repositories
        logging.debug("Adding binary repositories...")
        repo_man = RepositoryMan()
        repo_man.add_repositories(self._repodef, mount_path)
        
        # install additional system packages
        installer = Installer(mount_path)
        if not installer.install(imagedef.get_sys_packages()):
            logging.error("Failed to install system packages.")
            return None
        logging.debug("System packages installed.")

        # create packages dir in chroot-ed image
        tmp_dir_rel = "/var/tmp/contegrator"
        tmp_dir_abs = mount_path + tmp_dir_rel
        cmd = "mkdir -m 0777 " + tmp_dir_abs
        subprocess.check_call(cmd, shell = True)
        
        if ct_conf['repository']['mode'] == 'contrail':
            self._prepare_node_install_contrail_targz(imagedef, mount_path, tmp_dir_rel, installer)
        self._prepare_node_install_targz(imagedef, mount_path, tmp_dir_rel, installer)
        self._prepare_node_install_config(imagedef, node_cfg_dir, mount_path, tmp_dir_rel, installer)

        # copy scripts-post-boot directory
        scripts_pb_dir = os.path.join("./", "scripts-post-boot") # jenkins env variable WORKSPACE == './'
        self._copy_scripts_pb(scripts_pb_dir, mount_path, tmp_dir_rel, "scripts-pb-1.tar")
        scripts_pb_dir = os.path.join(self._nodes_cfg_dir, "scripts-post-boot")
        self._copy_scripts_pb(scripts_pb_dir, mount_path, tmp_dir_rel, "scripts-pb-2.tar")
        scripts_pb_dir = os.path.join(node_cfg_dir, "scripts-post-boot")
        self._copy_scripts_pb(scripts_pb_dir, mount_path, tmp_dir_rel, "scripts-pb-3.tar")

        # create a file used to identify the node once it comes up
        cmd = "sudo chroot " + mount_path + ' /bin/sh -c "echo ' + test_vnfs + ' > /' + Imager.VNFS_ID_FILE + '"'
        subprocess.check_call(cmd, shell = True)
        # leave vnfs mounted
        imagedef.image_name = test_vnfs
        imagedef.mount_path = mount_path
        return imagedef

    # prepare environmnet variables
    def _prepare_environment(self, node_id, rv, nodes):
        imagedef = rv[node_id]
        node = nodes[node_id]
        env = {}
        # settings from conf.py
        env.update({ 'CONF_OS_NAME': ct_conf['os']['name'] })
        env.update({ 'CONF_OS_VERSION': ct_conf['os']['version'] })
        env.update({ 'CONF_REPOSITORY_MODE': ct_conf['repository']['mode'] })
        env.update({ 'CONF_REPOSITORY_RELEASE': ct_conf['repository']['release'] })
        # settings from conf.py, ct_conf['env']
        for kk in ct_conf['env']:
            env.update({ kk: ct_conf['env'][kk] })
        # info about current image
        env.update({ 'NODE_ID': node_id })
        env.update({ 'HOST_NAME': node.get_host_name() })
        env.update({ 'VNFS_NAME': imagedef.image_name })
        env.update({ 'BASE_VNFS_NAME': imagedef.get_base_image_name() })
        # and for all other images
        for image2 in rv:
            if not rv[image2]:
                continue
            imagedef2 = rv[image2]
            node2 = nodes[image2]
            env.update({ 'NODE_ID_' + image2: image2 })
            env.update({ 'HOST_NAME_' + image2: node2.get_host_name() })
            env.update({ 'VNFS_NAME_' + image2: imagedef2.image_name })
            env.update({ 'BASE_VNFS_NAME_' + image2: imagedef2.get_base_image_name() })
        return env

    # for VNFS images only
    def _prepare_node_run_scripts(self, node_id, rv, nodes, prestaged = False):
        node_cfg_dir = os.path.join(self._nodes_cfg_dir, node_id)
        imagedef = rv[node_id]
        node = nodes[node_id]
        installer = Installer(imagedef.mount_path)
        # fix /etc/hostname
        cmd = "sudo chroot " + imagedef.mount_path + ' /bin/sh -c "echo ' + node.get_host_name() + ' > /etc/hostname "'
        subprocess.check_call(cmd, shell=True)
        # execute scripts, chrooted
        # TODO: what do we want to pass to these scripts via env var?
        # some ideas:
        # - original and cloned vnfs names
        # But - sudo resets environment
        env = self._prepare_environment(node_id, rv, nodes)
        # Write env to file, for scripts-post-boot
        env_file_tmp = tempfile.mkstemp(".tmp", "env.sh-")
        env_file_fd = env_file_tmp[0]
        env_file_name = env_file_tmp[1]
        # open(os.path.join(imagedef.mount_path, "root/contegrator-test/env.sh"), "w")
        for key in env:
            os.write(env_file_fd, key + "=" + str(env[key]) + "\n")
        os.close(env_file_fd)
        shutil.copyfile(env_file_name, os.path.join(imagedef.mount_path, "var/tmp/contegrator/env_var.sh"))
        #cmd = "sudo chroot " + imagedef.mount_path + " cp /var/tmp/contegrator/env.sh /root/contegrator-test/env.sh"
        #subprocess.check_call(cmd, shell=True)
        cmd = "sudo chroot " + imagedef.mount_path + " cp /var/tmp/contegrator/env_var.sh /srv/contegrator-test/common/env_var.sh"
        subprocess.check_call(cmd, shell=True)
        #
        scripts_dir = os.path.join(node_cfg_dir, "scripts")
        if os.path.isdir(scripts_dir):
            tmp_script_files = os.listdir(scripts_dir)
            script_files = []
            for f in tmp_script_files:
                p = os.path.join(scripts_dir, f)
                if not f.startswith(".") and os.path.isfile(p):
                    script_files.append(f)
            if len(script_files) > 0:
                tmp_dir = tempfile.mkdtemp(".tmp", "contrail",
                                           os.path.join(imagedef.mount_path, "tmp"))
                logging.debug("Temporary scripts dir: " + tmp_dir)
                (head, tail) = os.path.split(tmp_dir)
                if len(tail) == 0:
                    (head, tail) = os.path.split(head)
                chroot_tmp_dir = os.path.join("/tmp", tail)
                logging.debug("Temporary scripts dir (chroot): " + chroot_tmp_dir)
                for script in script_files:
                    try:
                        src_script_path = os.path.join(node_cfg_dir, "scripts", script)
                        dst_script_path = os.path.join(tmp_dir, script) 
                        logging.debug("Copying script file " + src_script_path + " to " + dst_script_path)
                        shutil.copyfile(src_script_path, dst_script_path)
                        os.chmod(dst_script_path, 0755)
                        logging.debug("Copied script file to: " + dst_script_path)
                    except:
                        logging.exception("Failed to copy script: " + script)
                        return None
                    chroot_script_path = os.path.join(chroot_tmp_dir, script)
                    logging.debug("Executing script (in chroot): " + chroot_script_path)
                    if not installer.chroot([chroot_script_path], env):
                        logging.error("Failed to execute script: " + script)
                        return None
                    logging.debug("Executed.")
                try:
                    shutil.rmtree(tmp_dir)
                except:
                    logging.exception("Failed to remove temporary scripts dir: " + tmp_dir)
                logging.debug("Scripts executed.")
            else:
                logging.debug("No scripts found.")
        # umount VNFS
        if not self._perceus.umount_vnfs(imagedef.image_name, force=True):
            logging.error("Failed to unmount test VNFS.")
            return None
        logging.debug("VNFS unmounted")
        return imagedef.image_name
    
    def _prepare_node(self, node_id, prestaged = False):
        node_cfg_dir = os.path.join(self._nodes_cfg_dir, node_id)
        logging.debug("Preparing node " + node_id + " for release " +
                      self._release_tag + " from config dir " + node_cfg_dir)
        # 1. parse image XML
        imagedef = ImageDef()
        # find all *.xml files for current node
        files = os.listdir(node_cfg_dir)
        install_xml_all = []
        for ff in files:
            filename = os.path.join(node_cfg_dir, ff)
            if filename.endswith('.xml') and os.path.isfile(filename):
                logging.debug("Adding xml file " + filename)
                install_xml_all.append(filename)
        if not install_xml_all:
            logging.exception("No .xml files found in dir " + node_cfg_dir)
            return None
        # parse install-*.xml files
        install_xml_all.sort()
        try:
            logging.debug("Parsing install configuration files: " + str(install_xml_all))
            imagedef.parse_files(install_xml_all, self._release_tag)
        except ImageDefError as error:
            logging.exception("Failed to parse node install file: " +
                              error.get_error_message())
            return None
        except Exception as ex:
            logging.exception("Failed to parse node install file.")
            return None
        if not prestaged:
            if imagedef.get_image_type() == ImageDef.IMAGE_TYPE_VNFS:
                if not prestaged:
                    return self._prepare_node_vnfs(imagedef, node_id)
            else:
                print "Unsupported image type:", imagedef.get_image_type()
                return None
        else:
            imagedef.image_name = "__prestaged__"
            return imagedef

    def _purge_vnfs(self, vnfs_name):
        # we might be using this after error, thus we do not know in what state the vnfs is:
        # thus unmount first (and silently fail if not mounted), then delete
        # .nfs files can be problem, thus additional rm -fr
        self._perceus.umount_vnfs(vnfs_name, force=True)
        self._perceus.delete_vnfs(vnfs_name)
        vnfs_dir = "/usr/var/lib/perceus/vnfs/" + vnfs_name
        vnfs_mount =  "/mnt/" + vnfs_name
        if os.path.isdir(vnfs_dir) and len(vnfs_name) > 0:
            logging.error("Directory " + vnfs_dir + " still exists, deleting manually")
            subprocess.call("umount " + vnfs_dir + "/rootfs/proc || true", shell = True)
            subprocess.call("[ -L " + vnfs_mount + " ] && rm -f " + vnfs_mount + " || true", shell = True)
            subprocess.call("rm -fr " + vnfs_dir + " || true", shell = True)
    
    # prepare_nodes, 1st step
    def prepare_nodes(self, prestaged = False):
        nodes_list = os.listdir(self._nodes_cfg_dir)
        rv = {}
        # parse configuration common for all nodes
        p = os.path.join(self._nodes_cfg_dir, 'repository.xml')
        repodef = RepoDef()
        try:
            repodef.parse_file(p)
        except RepoDefError as ex:
            logging.error("Failed parsing repository definition file " + p)
            return None
        self._repodef = repodef
        # prepare individual nodes        
        for node in nodes_list:
            p = os.path.join(self._nodes_cfg_dir, node)
            if node.startswith(".") or not os.path.isdir(p):
                continue
            if node in ct_conf['ignore-nodes']:
                logging.info('Skipping node ' + node + ', is listed in ignore-nodes')
                rv[node] = None
                continue
#            (image_name, image_type, imagedef) = self._prepare_node(node, prestaged)
            imagedef = self._prepare_node(node, prestaged)
            if imagedef: # and imagedef.image_name is not "":
                logging.info("Created node image " + node + " (" + 
                             imagedef.image_name + " , " + imagedef.get_image_type() + ")")
#                rv[node] = { "name": image_name, "type": image_type }
                rv[node] = imagedef
            else:
                logging.error("Failed to create node image for node: " + node)
                self.purge_nodes(rv, prestaged)
                return None
        return rv

    # prepare_nodes, 2nd step
    # run scripts in chroot environment
    def prepare_nodes_run_scripts(self, rv, nodes, prestaged = False):
        if prestaged:
            return rv
        for node_id in rv:
            if not rv[node_id]:
                continue
            imagedef = rv[node_id]
            node = nodes[node_id]
            logging.info("Running config scripts for node: " + node_id)
            ret = self._prepare_node_run_scripts(node_id, rv, nodes, prestaged)
            if ret:
                logging.info("Run config scripts for node - OK: " + node_id)
            else:
                logging.error("Failed to run config scripts for node: " + node_id)
                self.purge_nodes(rv, prestaged)
                return None
        return rv
    
    def purge_nodes(self, rv, prestaged = False):
        if prestaged:
            return
        if rv == None:
            return
        for node in rv:
            if not rv[node]:
                continue
            imagedef = rv[node]
            if imagedef.type == ImageDef.IMAGE_TYPE_VNFS:
                self._purge_vnfs(imagedef.image_name)
            elif imagedef.type == ImageDef.IMAGE_TYPE_VM:
                # TODO:
                pass
            else:
                logging.error("Unknown node image type: " + imagedef.get_image_type())

