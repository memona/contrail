# the following variables that are in the context when the test cases are run
# are of special importance to us:
# - ct_node_list: mapping node names (dir names of node definitions) to host names
# - nodeman:      a NodeManager instance that can be used to issue ssh commands to the nodes


# ct_nodeman.testrun - raises excetion, so that failure is detected by jenkins

def test_01_hello(ct_node_list, ct_nodeman):
    worker = ct_node_list["worker"]
    try:
        ct_nodeman.testrun("root", worker, "date > /root/timestamp.txt")
        ct_nodeman.testrun("root", worker, "/root/some-dir/test-2.sh || true")
    except:
        print "Test failed"
        raise
    finally:
        pass

def test_02_hello(ct_node_list, ct_nodeman):
    worker = ct_node_list["worker"]
    ct_nodeman.testrun("root", worker, "mkdir /var/log/contrail")
    ct_nodeman.testrun("root", worker, "echo 'aa asdf' > /var/log/contrail/aa.log")
    ct_nodeman.testrun("root", worker, "echo 'bb asdf' > /var/log/contrail/bb.log")

def test_03_hello(ct_node_list, ct_nodeman):
    worker = ct_node_list["worker"]
    dummy = ct_node_list["dummy"]
    ct_nodeman.testrun("root", dummy, "mkdir /var/log/contrail")

def test_04_hello(ct_node_list, ct_nodeman):
    worker = ct_node_list["worker"]
    dummy = ct_node_list["dummy"]
    ct_nodeman.testrun("root", worker, "dd bs=1M count=11 if=/dev/zero of=/root/bigfile-1")
    ct_nodeman.testrun("root", worker, "dd bs=1k count=2  if=/dev/zero of=/root/smallfile-1")
    ct_nodeman.testrun("root", worker, "dd bs=1M count=11 if=/dev/zero of=/var/log/contrail/bigfile-2")
    ct_nodeman.testrun("root", worker, "dd bs=1k count=3  if=/dev/zero of=/var/log/contrail/smallfile-2")

