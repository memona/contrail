#!/bin/bash

# In edited-xml file, replace everything between <TAG>,</TAG> with the second file.
# Use for jenkins job config.xml:
# ./replace-xml-block.sh JOB/config.xml config-param.xml hudson.model.ParametersDefinitionProperty

if [ $# -ne 3 ]
then
	echo "Usage: $0 <edited-xml> <xml-to-insert> <xml-tag-to-match>"
	exit 1
fi

FILE=$1
FILE2=$2
TAG=$3
echo "edited xml: $FILE"
echo "insert xml: $FILE2"
echo "xml tag: $TAG"

DATE=`date +%Y%m%d-%H%M%S`
cp -p $FILE $FILE.back-$DATE || exit 1
sed -i -e '/<'$TAG'>/,/<\/'$TAG'>/ c MY_MAGIC_MARKER' $FILE
sed -i -e "/^MY_MAGIC_MARKER\$/r $FILE2" -e '/^MY_MAGIC_MARKER$/d' $FILE

