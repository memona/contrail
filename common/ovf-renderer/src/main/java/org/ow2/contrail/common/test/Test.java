package org.ow2.contrail.common.test;

import org.ow2.contrail.common.implementation.Renderer;
import org.apache.log4j.*;

public class Test {

	private static Logger logger = Logger.getLogger(Test.class);
	private static String path = "src/main/resources";
	private static String name = "test";
	private static String source = "contrail_petstore.xml";

	public static void main(String[] args) throws Exception {

		logger.info("Test Renderer - starting test");
		logger.info("Test Renderer - creating document \""+name+".xml\" from document \""+source+"\"\n");
		Renderer.RenderOVF(path + "/contrail_petstore.xml", path, name);
		logger.info("Test Renderer - test completed, document \""+path+"/"+name+".xml\" created");
	}
}
