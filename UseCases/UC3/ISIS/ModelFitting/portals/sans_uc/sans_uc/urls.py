"""Top-level URL declarations for this Django project.

Official docs: https://docs.djangoproject.com/en/1.4/topics/http/urls/

"""

#===============================================================================
# Author: Adrian Coveney
# Contact: adrian.coveney@stfc.ac.uk, ian.johnson@stfc.ac.uk
# Copyright (c) 2012 The Science and Technology Facilities Council
#===============================================================================


from django.conf import settings
from django.contrib import admin
from django.contrib.auth.views import login, logout
from django.conf.urls import patterns, include, url
#from django.views.generic. import direct_to_template

from django.views.generic import TemplateView


admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', TemplateView.as_view(template_name='home.html')),

    url(r'^home$', TemplateView.as_view(template_name='home.html'), name='home'),

    url(r'^bot/', include('bag_of_tasks.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^accounts/login/$', login, {'template_name': 'login.html'}, 'login'),

    url(r'^accounts/logout/$', logout, {'template_name': 'logout.html'},
        'logout'),

    url(r'^xhr_test/', TemplateView.as_view(template_name='xhr_test.html')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': False}),

)

# Only serve uploaded media from Django if in DEBUG mode. (In most cases, Apache
# should be doing this job.)
if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': False}),
        )
