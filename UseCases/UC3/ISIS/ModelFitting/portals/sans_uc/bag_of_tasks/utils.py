"""Utility functions used in the views."""

#===============================================================================
# Authors: Adrian Coveney, Ian Johnson
# Contact: adrian.coveney@stfc.ac.uk, ian.johnson@stfc.ac.uk
# Copyright (c) 2012-2013 The Science and Technology Facilities Council
#===============================================================================

import json

from itertools import product
from string import Template

from django.forms.models import model_to_dict

import sans_uc.settings as settings

import os

import uuid

from cps import taskfarm

def get_user_models(request, model_list):
    """Return list of objects from differnt models that belong to user."""
    modelset = []
    for model in model_list:
        modelset += list(model.objects.filter(owner=request.user.id))
    return modelset


def create_bot(models, data_files):
    """Create bag-of-tasks file using supplied models and data."""

    # Keep spaces at end of each line.
    cmd_template = Template('python "${progpath}" fit '
                            '-dataset "${dataset}" '
                            '-model ${model} '
                            '-parameters \'${params}\' '
                            '-outpath "${outpath}" '
                            '-xml')

    cmd = ''

    for data_file in data_files:
        for model in models:
            parameters = []
            for key in model_to_dict(model).keys():
                if key in ('id', 'owner', 'fixed_fields'):
                    # Skip key if it's not a sans model parameter.
                    continue
                if key in model.fixed_fields:
                    fixed = True
                else:
                    fixed = False
                parameters.append({'paramname': key,
                                   'value': model_to_dict(model)[key],
                                   'fixed': fixed})

            # Make first letter lower case
            model_name = str(model)[0].lower() + str(model)[1:]

            cmd += cmd_template.substitute(progpath='modelling.py',
                                           dataset=data_file,
                                           model=model_name,
                                           params=parameters,
                                           outpath='test.xml') + '\n'
    return cmd


def frange(start, stop, step):
    """Produce range like xrange() for floats."""
    while start <= stop:
        yield start
        start += step

def get_product(values):
    
    for idx, value in enumerate(values):
        
        if len(value.split(',')) == 1:
            # Either provide a one element list for single values...
            values[idx] = (float(value),)
        else:
            # ...or a range.
            values[idx] = frange(*[float(x) for x in value.split(',')])

    return product(*values)    

def get_values(model):
    
    keys = []
    values = []
    for key in model_to_dict(model).keys():
    
        if settings.VARIABLE_DEBUG:
            print 'key = %s' % (key)
        if key in ('id', 'owner', 'fixed_fields'):
            # Skip key if it's not a sans model parameter.
            continue
        # Create list of keys (parameters) for later matching to values
        keys.append(key)
        values.append(model_to_dict(model)[key])
        
    return keys, values

def model_name_from_sweep(sweepName):
# Make first letter lower case and drop last 5 letters ('Sweep')
    return str(sweepName)[0].lower() + str(sweepName)[1:-len('Sweep')]


def create_bot_with_sweep(models, datadict, outdir=None, limit=None):
    """Create bag-of-tasks file using supplied models and data."""

    # Keep spaces at end of each line.
    cmd_template = \
    Template('${progpath} fit --dataset "${dataset}" '
                            '--model ${model} '
                            '--parameters \'${params}\' '
                            '--outpath "${outpath}" '
                            )
    cmd = ''
    
    count = 0
    
    for data_file in datadict:
        datapath = os.path.join( settings.MEDIA_ROOT, data_file)
        
        for model in models:

            model_name = model_name_from_sweep(model)           
            keys, values = get_values(model)                  
            enumerated = get_product(values)
            
 
            
            for single in enumerated:
                
                if count is not None and count == limit: # Check that BoT file is not too long, or is short enough for testing
                    break  
                                 
                parameters = list_to_JSON(get_parameters(model, single, keys))
                
                val = None
                try:
                    val = json.loads(parameters)
                except ValueError:
                    print 'Exception: val = %s' % val
                
                # Create a unique filename for each output file
                # Mostly relevant for case of local execution (i.e. w/o using the TaskFarm)
                
                outpath = "output_%d.xml" % count
                if outdir is not None:
                    outpath = os.path.join( outdir, outpath )
                    
                # Create the command for the model-fitting
                
                cmd += cmd_template.substitute(progpath=settings.MODELLING_CMD,
                                               dataset=datapath,
                                               model=model_name,
                                               params=parameters,
                                               outpath=outpath) + '\n'
                count += 1
    return cmd

def get_parameters(model, single, keys):   

    parameters = []
    
    for idx, value in enumerate(single):
        parameters.append({"paramname": keys[idx],
                           "value": value,
                           "fixed": "true" if keys[idx] in
                                    model.fixed_fields else "false"})   
    return parameters    
    
    
def list_to_JSON(aList):
    # Join the list elements by converting each one to a string and separating with commas
    joined = ', '.join(map(str, aList))
    # Now create a JSON-style list
    return '[' + joined.replace("'", '"') + ']'     

def updateCounter(filename, value):
    
    try:
        fo = open(filename, 'w+')
        fo.write( str(value) )
        fo.close()   
 
    except IOError as e:
        print 'IOError: %s' % e.strerror
   
def readCounter(filename):
    
    v = 0
    try:
        fo = open(filename, 'r')
        v = fo.read()
        fo.close()
        
    except IOError as e:
        print 'IOError: %s' % e.strerror
        
    return v
        
def destroyCounter(filename):
    
    try:
        os.unlink(filename)
    except IOError as e:
        print 'IOError: %s' % e.strerror
        
def getCompleted(dirname):
    
    return len( os.listdir(dirname))

def getOutputDir(uniqueifier):
    return os.path.join ( settings.OUTPUT_ROOT, 'output-%s' % str (uniqueifier) )

import StringIO

from django.core.files.uploadedfile import InMemoryUploadedFile

import bag_of_tasks



def createAndSaveDocument(data, filename, owner):
       
    sbuffer = StringIO.StringIO()        
    sbuffer.write(data)
       
    file_obj = InMemoryUploadedFile(sbuffer, None, filename,
                                   None, sbuffer.len, None)
    newdoc = bag_of_tasks.models.BoTFile(task_file=file_obj)
    newdoc.owner = owner
   
    newdoc.save()
    sbuffer.close()
    
    #===========================================================================
    # Probably don't need to return file_obj because we don't need to use it
    #===========================================================================
    return newdoc, file_obj
    
import stat 
   
def setExecutableBit(filename):
    
    perms = os.stat(filename).st_mode
    os.chmod(filename, perms | stat.S_IXUSR)


def get_OutputDir(pattern='output-%s'):
    
    uuidp = uuid.uuid1()
    
    if pattern is not None:
        dirname = pattern % str( uuidp )
    else:
        dirname = str( uuidp )
    
    return os.path.join ( settings.OUTPUT_ROOT, dirname )
    
    

def dictify(sequence, desired_keys=None):
    
    d = {}

    for k,v in sequence.iteritems():
        if k in desired_keys or desired_keys is None:
            d[k] = v                 

    return d


def getTasks(t, sid):
    data = t.info(sid)
   
    return  int(data['noCompletedTasks']), int(data['noTotalTasks'])    


            
    
    