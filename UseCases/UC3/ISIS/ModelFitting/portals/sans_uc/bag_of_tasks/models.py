"""Defines model classes for storage in db.

A model is the single, definitive source of data about your data. It contains
the essential fields and behaviors of the data you're storing. Generally, each
model maps to a single database table.

Official docs: https://docs.djangoproject.com/en/1.4/topics/db/models/

"""

#===============================================================================
# Authors: Adrian Coveney, Ian Johnson
# Contact: adrian.coveney@stfc.ac.uk, ian.johnson@stfc.ac.uk
# Copyright (c) 2012-2013 The Science and Technology Facilities Council
#===============================================================================


from django.contrib.auth.models import User
from django.db import models
from django.db.models.manager import Manager


class OwnedModel(models.Model):
    """Parent class for models with an owner.

    Use this snippet in the view before saving:

    if not OwnedModel.id:
        OwnedModel.owner = request.user

    """
    owner = models.ForeignKey(User, editable=False)
    

    # Explictly add objects methods so code completion works in views.py and so
    # pylint doesn't complain about missing methods. This is a bit hacky.
    objects = Manager()

    class Meta:
        """Optional model metadata."""
        # Stops table being created for this class as it'll be inherited from.
        abstract = True


class BoTFile(OwnedModel):
    """Model class for uploaded bag-of-tasks files"""
    task_file = models.FileField(upload_to='task_files')

    class Meta:
        """Optional model metadata."""
        verbose_name = 'BoT file'

    def __unicode__(self):
        return self.task_file.name


class DataFile(OwnedModel):
    """Model for uploaded data files.

    Owned by uploading user.

    """
    data_file = models.FileField(upload_to='data_files')

    def __unicode__(self):
        return self.data_file.name


class SansModel(OwnedModel):
    """Parent class for sans models.

    Contains fields applicable to all models.

    """
    # Save a list here of fields that will be fixed in the model simulation.
    fixed_fields = models.CharField(max_length=500, blank=True, editable=False)

    background = models.FloatField()
    scale = models.FloatField()

    def __unicode__(self):
        """Return just the name of the class rather than 'class-name object'"""
        return self.__class__.__name__

    class Meta:
        """Optional model metadata."""
        # Stops table being created for this class as it'll be inherited from.
        abstract = True


class Cylinder(SansModel):
    """SansView cylinder model."""
    radius = models.FloatField()
    length = models.FloatField()
    sldCyl = models.FloatField()
    sldSolv = models.FloatField()
    
    def prettyPrint(self):
        pp = 'Cylinder: \n\tlength %s, radius %s' % \
        (str(self.length), str(self.radius))
        
        return pp


class Sphere(SansModel):
    """SansView sphere model."""
    radius = models.FloatField()
    sldSph = models.FloatField()
    sldSolv = models.FloatField()


class Ellipse(SansModel):
    """SansView ellipse model."""
    radius_a = models.FloatField()
    radius_b = models.FloatField()
    sldEll = models.FloatField()
    sldSolv = models.FloatField()

# ijj
class CoreShellBicelle(SansModel):
    """SansView coreShellBicelle model."""
    radius = models.FloatField()
    rim_thick = models.FloatField()
    face_thick = models.FloatField()
    length = models.FloatField()
    core_sld = models.FloatField()
    face_sld = models.FloatField()
    rim_sld = models.FloatField()
    solvent_sld = models.FloatField()


# Models for parameter sweeps
class SansModelSweep(OwnedModel):
    """Parent class for sans model sweeps.

    Contains fields applicable to all models.

    """
    # Save a list here of fields that will be fixed in the model simulation.
    fixed_fields = models.CharField(max_length=500, blank=True, editable=False)

    # Stores number of commands that will be added to BoT by parameter sweep.
    runs = models.IntegerField(blank=True, editable=False)

    fields = ('background', 'scale')

    for field in fields:
        locals()[field] = models.CharField(max_length=100)

    def __unicode__(self):
        """Return just the name of the class rather than 'class-name object'"""
        return self.__class__.__name__

    class Meta:
        """Optional model metadata."""
        # Stops table being created for this class as it'll be inherited from.
        abstract = True


class CylinderSweep(SansModelSweep):
    """SansView cylinder sweep model."""

    fields = ('radius', 'length', 'sldCyl', 'sldSolv')

    for field in fields:
        locals()[field] = models.CharField(max_length=100)


class SphereSweep(SansModelSweep):
    """SansView sphere sweep model."""

    fields = ('radius', 'sldSph', 'sldSolv')

    for field in fields:
        locals()[field] = models.CharField(max_length=100)


class EllipseSweep(SansModelSweep):
    """SansView ellipse sweep model."""

    fields = ('radius_a', 'radius_b', 'sldEll', 'sldSolv')

    for field in fields:
        locals()[field] = models.CharField(max_length=100)


# ijj
class CoreShellBicelleSweep(SansModelSweep):
    """SansView coreShellBicelle sweep model."""
 
    fields = ('radius', 'rim_thick', 'face_thick', 'length',
              'core_sld', 'face_sld', 'rim_sld', 'solvent_sld')
 
    for field in fields:
        locals()[field] = models.CharField(max_length=100)
